## Troubleshooting tips and tricks


- `com.sun.org.apache.xerces.internal.impl.io.MalformedByteSequenceException: Invalid byte 1 of 1-byte UTF-8 sequence.`
   * Check in the stack trace if the problem is related to Xtext! 
   * If in Maven tests: see https://github.com/eclipse/xtext-extras/issues/108
   * Check which file causes the problem! If it is an `.xtextbin` file, then the problem is that EMF tries to load a binary file as an XMI. This is because the `.xtextbin` files are not correctly registered in the resource factory. Use the <grammarname>StandaloneSetup.createInjectorAndDoEMFRegistration() to register the proper factory for `.xtextbin` files (if not done before) and to get an injector.
   
- ```
   org.osgi.framework.BundleException: Exception in cern.plcverif.plc.step7.ui.Step7UiCustomActivator.start() of bundle cern.plcverif.plc.step7.ui.
   ...
   Caused by: java.lang.NoClassDefFoundError: org/eclipse/core/resources/IncrementalProjectBuilder
   Caused by: java.lang.ClassNotFoundException: An error occurred while automatically activating bundle org.eclipse.core.resources (110).
   Caused by: org.osgi.framework.BundleException: Exception in org.eclipse.core.resources.ResourcesPlugin.start() of bundle org.eclipse.core.resources.
   Caused by: org.eclipse.core.internal.dtree.ObjectNotFoundException: Tree element '/debug-anon-struct/stltest.awl' not found.
   ```
    * Delete  .metadata/.plugins/org.eclipse.core.resources/
	
- `java.lang.NullPointerException at org.eclipse.xtext.ui.shared.internal.ExecutableExtensionFactory.getBundle(ExecutableExtensionFactory.java:19)`
   * Start the application with `-clean` flag