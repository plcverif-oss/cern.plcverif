/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.preferences;

import static cern.plcverif.base.gui.preferences.RootPlcverifPreferencePage.CONTEXT_QUALIFIER;
import static cern.plcverif.base.gui.preferences.RootPlcverifPreferencePage.DEFAULT_VALUE_OUTPUT_FOLDER;
import static cern.plcverif.base.gui.preferences.RootPlcverifPreferencePage.DEFAULT_VALUE_SETTINGS_FOLDER;
import static cern.plcverif.base.gui.preferences.RootPlcverifPreferencePage.DEFAULT_VALUE_STRICT_ID_CHECK;
import static cern.plcverif.base.gui.preferences.RootPlcverifPreferencePage.KEY_OUTPUT_FOLDER;
import static cern.plcverif.base.gui.preferences.RootPlcverifPreferencePage.KEY_SETTINGS_FOLDER;
import static cern.plcverif.base.gui.preferences.RootPlcverifPreferencePage.KEY_STRICT_ID_CHECK;

import java.io.IOException;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;

import com.google.common.base.Preconditions;

/**
 * Utility class to simplify the access to PLCverif GUI preferences.
 */
public final class PlcverifPreferenceAccess {
	private PlcverifPreferenceAccess() {
		// Utility class.
	}

	/**
	 * Returns the output folder set up in PLCverif.
	 */
	public static String getOutputFolder() {
		IEclipsePreferences preferences = InstanceScope.INSTANCE.getNode(CONTEXT_QUALIFIER);
		return preferences.get(KEY_OUTPUT_FOLDER, DEFAULT_VALUE_OUTPUT_FOLDER);
	}

	/**
	 * Returns the settings folder set up in PLCverif (relative to PLCverif's
	 * installation folder).
	 */
	public static String getSettingsFolder() {
		IEclipsePreferences preferences = InstanceScope.INSTANCE.getNode(CONTEXT_QUALIFIER);
		return preferences.get(KEY_SETTINGS_FOLDER, DEFAULT_VALUE_SETTINGS_FOLDER);
	}

	/**
	 * Returns the {@link IFolder} representation of the output folder set up in
	 * PLCverif within the given project. If the folder does not exist, it will
	 * be automatically created (if possible).
	 * 
	 * @param parentProject
	 *            Project in which the output folder is to be acquired
	 * @param monitor
	 *            Progress monitor to monitor the folder creation.
	 * @return The {@link IFolder} as set in the settings. Guaranteed to exist
	 *         if the method does not throw an exception.
	 * @throws IOException
	 *             if it was not possible to create the folder
	 */
	public static IFolder getAndCreateOutputFolder(IProject parentProject, IProgressMonitor monitor)
			throws IOException {
		IPath outputPath = new org.eclipse.core.runtime.Path(getOutputFolder());
		IFolder outputFolder = parentProject.getFolder(outputPath);

		try {
			if (!outputFolder.exists()) {
				outputFolder.create(true, true, monitor);
			}
			Preconditions.checkState(outputFolder.exists(), "Assumption violated: output folder does not exist.");
			return outputFolder;
		} catch (CoreException e) {
			throw new IOException("Unable to create the output folder.", e);
		}
	}

	/**
	 * Returns whether strict ID check is required. If true, the GUI should make
	 * sure that the settings loaded from files uses the same ID as the name of
	 * the file.
	 * 
	 * @return Strict ID check required
	 */
	public static boolean isStrictIdCheck() {
		IEclipsePreferences preferences = InstanceScope.INSTANCE.getNode(CONTEXT_QUALIFIER);
		return preferences.getBoolean(KEY_STRICT_ID_CHECK, DEFAULT_VALUE_STRICT_ID_CHECK);
	}
}
