/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.preferences;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.preferences.ScopedPreferenceStore;

public class RootPlcverifPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	public static final String CONTEXT_QUALIFIER = "cern.plcverif.base.gui";
	public static final String KEY_OUTPUT_FOLDER = "outputfolder";
	public static final String DEFAULT_VALUE_OUTPUT_FOLDER = "output";
	public static final String KEY_SETTINGS_FOLDER = "settingsfolder";
	public static final String DEFAULT_VALUE_SETTINGS_FOLDER = "./settings";
	public static final String KEY_STRICT_ID_CHECK = "strict_id_check";
	public static final boolean DEFAULT_VALUE_STRICT_ID_CHECK = true;

	public RootPlcverifPreferencePage() {
		super(GRID);
	}

	@Override
	public void init(IWorkbench workbench) {
		setPreferenceStore(new ScopedPreferenceStore(InstanceScope.INSTANCE, CONTEXT_QUALIFIER));
		
		setDescription("General settings for PLCverif");
	}

	@Override
	protected void createFieldEditors() {
		final Composite parent = getFieldEditorParent();

		addField(new StringFieldEditor(KEY_OUTPUT_FOLDER, "Output folder (within a project):", parent));
		getPreferenceStore().setDefault(KEY_OUTPUT_FOLDER, DEFAULT_VALUE_OUTPUT_FOLDER);
		
		addField(new StringFieldEditor(KEY_SETTINGS_FOLDER, "Settings folder (relative to PLCverif installation folder):", parent));
		getPreferenceStore().setDefault(KEY_SETTINGS_FOLDER, DEFAULT_VALUE_SETTINGS_FOLDER);
		
		addField(new BooleanFieldEditor(KEY_STRICT_ID_CHECK, "Strict ID check", parent));
		getPreferenceStore().setDefault(KEY_STRICT_ID_CHECK, DEFAULT_VALUE_STRICT_ID_CHECK);
		
	}
}
