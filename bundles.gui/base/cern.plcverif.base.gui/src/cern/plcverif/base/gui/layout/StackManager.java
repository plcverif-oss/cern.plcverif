/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.layout;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;

import com.google.common.base.Preconditions;

/**
 * Manager class to efficiently switch between stacked composites. Each of the
 * composites shall have an attached unique ID that can be used to select the
 * currently visible composite.
 */
public class StackManager {
	/**
	 * Functional interface for stack page change listeners.
	 */
	@FunctionalInterface
	public static interface StackPageChangeListener {
		/**
		 * Method to be invoked when the listened page's visibility has changed.
		 * 
		 * @param visibleNow
		 *            The new state of visibility.
		 */
		void onPageChange(boolean visibleNow);
	}

	/**
	 * Error page, if defined. Shall never be {@code null}.
	 */
	private Optional<Composite> errorPage = Optional.empty();

	/**
	 * Pages in the stack with their attached string ID.
	 */
	private Map<String, Composite> pages = new HashMap<>();

	/**
	 * Stack page change listeners attached to the pages.
	 */
	private Map<String, StackPageChangeListener> onPageChange = new HashMap<>();

	/**
	 * Parent composite representing the stack.
	 */
	private Composite stackComposite;

	/**
	 * Layout created for the composite. It is used to change the currently
	 * visible page.
	 */
	private StackLayout stackLayout;

	/**
	 * Creates a new stack manager for the given composite. <br>
	 * A new layout will be created for the given composite.
	 * 
	 * @param stack
	 *            Composite that contains the stack
	 */
	public StackManager(Composite stack) {
		this.stackLayout = new CustomizedStackLayout();
		stack.setLayout(this.stackLayout);
		this.stackComposite = stack;
	}

	/**
	 * Adds a new page to the stack with the given key.
	 * 
	 * @param key
	 *            Key of the page
	 * @param page
	 *            Composite representing the page. Must be contained by the
	 *            composite that was given in the constructor of this object.
	 */
	public void addPage(String key, Composite page) {
		addPage(key, page, null);
	}

	/**
	 * Adds a new page to the stack with the given key and a listener for the
	 * visibility change.
	 * 
	 * @param key
	 *            Key of the page
	 * @param page
	 *            Composite representing the page. Must be contained by the
	 *            composite that was given in the constructor of this object.
	 * @param onPageChange
	 *            Function or object that is invoked when the visibility of the
	 *            currently added page changes
	 */
	public void addPage(String key, Composite page, StackPageChangeListener onPageChange) {
		Preconditions.checkNotNull(key, "Key shall not be null.");
		Preconditions.checkNotNull(page, "A null composite cannot be set as page.");
		Preconditions.checkArgument(page.getParent() == stackComposite,
				"The composite to be added is not contained by the managed stack.");
		Preconditions.checkArgument(!pages.containsKey(key), "A composite with the given key is already present.");

		pages.put(key, page);
		page.setVisible(false);
		if (onPageChange != null) {
			this.onPageChange.put(key, onPageChange);
			onPageChange.onPageChange(false);
		}
	}

	/**
	 * Adds an error page. It is to be shown when an incorrect page was
	 * requested to be shown.
	 * 
	 * @param errorPage
	 *            Composite representing the error page. Must be contained by
	 *            the composite that was given in the constructor of this
	 *            object.
	 */
	public void addErrorPage(Composite errorPage) {
		Preconditions.checkArgument(errorPage.getParent() == stackComposite,
				"The composite to be added is not contained by the managed stack.");
		Preconditions.checkNotNull(errorPage, "A null composite cannot be set as error page.");

		errorPage.setVisible(false);
		this.errorPage = Optional.of(errorPage);
	}

	/**
	 * Shows the page with the given key. If no page with such key exists and
	 * there is an error page defined, the error page will be displayed.
	 * 
	 * @param key
	 *            Key of the page to be shown. May be {@code null}.
	 */
	public void select(String key) {
		if (pages.containsKey(key)) {
			stackLayout.topControl = pages.get(key);
		} else if (errorPage.isPresent()) {
			stackLayout.topControl = errorPage.get();
		} else {
			stackLayout.topControl = null;
		}
		stackComposite.layout();
		stackComposite.getParent().requestLayout();

		for (Entry<String, StackPageChangeListener> pageChange : onPageChange.entrySet()) {
			boolean isVisible = pageChange.getKey().equals(key);
			pageChange.getValue().onPageChange(isVisible);
		}
	}

	private static class CustomizedStackLayout extends StackLayout {
		/**
		 * Modified version of compute size: if there is a top control, the rest
		 * will not be taken into account.
		 */
		@Override
		protected Point computeSize(Composite composite, int wHint, int hHint, boolean flushCache) {
			if (this.topControl == null) {
				return super.computeSize(composite, wHint, hHint, flushCache);
			} else {
				Point topControlSize = topControl.computeSize(wHint, hHint, flushCache);
				int width = topControlSize.x + 2 * marginWidth;
				int height = topControlSize.y + 2 * marginHeight;

				if (wHint != SWT.DEFAULT)
					width = wHint;
				if (hHint != SWT.DEFAULT)
					height = hHint;
				return new Point(width, height);
			}
		}
	}
}
