/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.component;

import org.eclipse.swt.widgets.Composite;

import cern.plcverif.base.gui.binding.PvDataBinding;
import cern.plcverif.base.gui.layout.PvLayoutUtils;

/**
 * Interface for composable GUI components (i.e., GUI parts).
 *
 * @param <T>
 *            Type of GUI context object
 */
public interface IPvGuiPart<T extends PvGuiContext> {
	/**
	 * Creates the GUI controls included in this GUI part.
	 * 
	 * @param composite
	 *            The composite that will represent this GUI part. The layout is
	 *            expected to be consistent with the layouts used by
	 *            {@link PvLayoutUtils}.
	 * @param parentBinding
	 *            Parent data binding element
	 */
	public void createPart(Composite composite, PvDataBinding parentBinding);

	/**
	 * Sets the enablement of the represented data binding elements.
	 * 
	 * @param enabled
	 *            The new value for enablement
	 */
	public void setEnabled(boolean enabled);

	/**
	 * Returns the context of the parent form of this GUI part.
	 * 
	 * @return Context
	 */
	T getContext();
}