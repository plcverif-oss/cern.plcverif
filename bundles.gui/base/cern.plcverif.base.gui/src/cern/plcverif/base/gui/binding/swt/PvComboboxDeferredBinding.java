/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.binding.swt;

import java.util.List;
import java.util.Optional;

import org.eclipse.swt.widgets.Combo;

import cern.plcverif.base.common.logging.PlatformLogger;
import cern.plcverif.base.gui.binding.IKeyLabelPair;
import cern.plcverif.base.gui.binding.PvDataBinding;
import cern.plcverif.base.gui.binding.exceptions.DataInvalidException;

/**
 * Data binding for an element representing a string/enumeration
 * {@link cern.plcverif.base.common.settings.SettingsElement} by a combobox
 * ({@link Combo}) SWT control. This implementation extends
 * {@link PvComboboxBinding} with deferred loading support, i.e., if the value
 * loaded is not in the list of known values yet, then it waits until it is
 * refreshed instead of throwing errors.
 */
public class PvComboboxDeferredBinding extends PvComboboxBinding {
	public PvComboboxDeferredBinding(Combo combobox, String settingsAttribute, PvDataBinding parent) {
		super(combobox, settingsAttribute, parent);
	}

	/**
	 * A key that was not possible to set as current value because it was not in
	 * the set of handled keys. At next refresh of handled set of keys
	 * ({@link PvComboboxDeferredBinding#setPermittedItems(List)}) it will be
	 * retried.
	 */
	private String deferredKey = null;

	@Override
	public void setSelectedItem(String key) throws DataInvalidException {
		if (handledKeys.contains(key)) {
			super.setSelectedItem(key);
		} else {
			// The given key cannot be loaded yet: don't do anything yet and
			// store it.
			deferredKey = key;
			getControl().setEnabled(false);
			fireChangeListeners();
		}
	}

	@Override
	public Optional<String> getSelectedItem() {
		if (deferredKey != null) {
			return Optional.of(deferredKey);
		} else {
			return super.getSelectedItem();
		}
	}

	@Override
	public void setPermittedItems(List<? extends IKeyLabelPair> items) {
		super.setPermittedItems(items);

		if (deferredKey != null && handledKeys.contains(deferredKey)) {
			// It is possible now to set the value to the previously loaded one
			try {
				setSelectedItem(deferredKey);
			} catch (DataInvalidException ex) {
				// Unexpected exception
				PlatformLogger.logWarning("Unexpected exception in PvComboboxDeferredBinding.", ex);
			}
		}

		// Don't keep any deferred keys any longer
		getControl().setEnabled(true);
		deferredKey = null;
	}

	@Override
	protected void setDirty() {
		if (deferredKey == null) {
			super.setDirty();
		}

		// prevent setting the dirty bit while there is a deferred value to be
		// treated
	}

	@Override
	protected boolean isThisValid() {
		if (deferredKey == null) {
			return super.isThisValid();
		} else {
			return validator.validate(deferredKey);
		}
	}
}
