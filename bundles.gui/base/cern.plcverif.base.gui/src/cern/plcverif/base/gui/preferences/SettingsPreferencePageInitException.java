/*******************************************************************************
 * (C) Copyright CERN 2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.preferences;

/**
 * Runtime exception to indicate that it was not possible to initialize a
 * settings preference page for some reason.
 */
public class SettingsPreferencePageInitException extends RuntimeException {
	private static final long serialVersionUID = 119123584285977874L;

	public SettingsPreferencePageInitException(Throwable throwable) {
		super(throwable);
	}

	public SettingsPreferencePageInitException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
