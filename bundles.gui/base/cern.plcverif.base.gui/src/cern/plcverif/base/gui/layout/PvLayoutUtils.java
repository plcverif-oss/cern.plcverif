/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.layout;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.google.common.base.Preconditions;

import cern.plcverif.base.gui.swt.ILayoutSettable;

/**
 * Utility class for the unified layout handling in PLCverif GUI forms.
 */
public final class PvLayoutUtils {
	/**
	 * Interface for any object that has a {@code getLayoutData} method.
	 */
	@FunctionalInterface
	public interface ILayoutDataGet {
		Object getLayoutData();
	}

	/**
	 * Number of columns in the default grid.
	 */
	public static final int NUM_COLS = 4;

	/**
	 * Width of the first column.
	 */
	public static final int DEFAULT_FIRST_COL_WIDTH = 110;

	/**
	 * Typical width for 'small' controls.
	 */
	public static final int CONTROL_WIDTH_SMALL = 100;

	/**
	 * Typical width for 'medium' controls.
	 */
	public static final int CONTROL_WIDTH_MEDIUM = 300;

	private PvLayoutUtils() {
		// Utility class.
	}

	/**
	 * Returns a grid layout object with the default settings for a PLCverif
	 * form.
	 * 
	 * @return Layout
	 */
	public static GridLayout defaultLayout() {
		GridLayout layout = new GridLayout(NUM_COLS, false);

		layout.horizontalSpacing = 15;
		layout.verticalSpacing = 5;
		layout.marginHeight = 5;
		layout.marginWidth = 10;

		return layout;
	}

	/**
	 * Returns a grid layout object with the default settings for a PLCverif
	 * part that is nested within a PLCverif form.
	 * 
	 * @return Layout
	 */
	public static GridLayout defaultNestedLayout() {
		GridLayout layout = defaultLayout();

		layout.marginHeight = 0;
		layout.marginWidth = 0;

		return layout;
	}

	/**
	 * Returns a layout data object that is to be used for labels in the first
	 * column of the default grid.
	 * 
	 * @return Layout data
	 */
	public static GridData firstColumnLabelGridData() {
		GridData ret = GridDataFactory.fillDefaults().minSize(DEFAULT_FIRST_COL_WIDTH, 1)
				.align(SWT.BEGINNING, SWT.BEGINNING).create();
		ret.widthHint = DEFAULT_FIRST_COL_WIDTH;
		return ret;
	}

	public static GridData fillAllButFirstColGridData() {
		return GridDataFactory.fillDefaults().grab(true, false).span(NUM_COLS - 1, 1).create();
	}

	public static void createLabeledRow(Composite parent, Label label, Control control) {
		createLabeledRow(parent, label, control::setLayoutData);
	}
	
	public static void createLabeledDropdownRow(Composite parent, Label label, Control control, Control dropdown) {
		label.setLayoutData(PvLayoutUtils.firstColumnLabelGridData());
		dropdown.setLayoutData(PvLayoutUtils.firstColumnLabelGridData());

		control.setLayoutData(GridDataFactory.fillDefaults().span(PvLayoutUtils.NUM_COLS - 1, 2).grab(PvLayoutUtils.NUM_COLS - 1 > 1, false)
				.minSize(50, SWT.DEFAULT).create());
	}

	public static void createLabeledRow(Composite parent, Label label, ILayoutSettable control) {
		createLabeledRow(parent, label, control, PvLayoutUtils.NUM_COLS - 1, false);
	}

	public static void createLabeledRow(Composite parent, Label label, Control control, int controlColSpan,
			boolean fillRest) {
		createLabeledRow(parent, label, control::setLayoutData, controlColSpan, fillRest);
	}

	public static void createLabeledRow(Composite parent, Label label, ILayoutSettable control, int controlColSpan,
			boolean fillRest) {
		label.setLayoutData(PvLayoutUtils.firstColumnLabelGridData());

		control.setLayoutData(GridDataFactory.fillDefaults().span(controlColSpan, 1).grab(controlColSpan > 1, false)
				.minSize(50, SWT.DEFAULT).create());
		if (fillRest) {
			for (int i = 0; i < PvLayoutUtils.NUM_COLS - controlColSpan - 1; i++) {
				new Label(parent, SWT.NONE);
			}
		}
	}

	public static void createRow(Composite parent, Control control, int controlColSpan, boolean fillRest) {
		Preconditions.checkArgument(controlColSpan >= 1);

		control.setLayoutData(GridDataFactory.fillDefaults().span(controlColSpan, 1).grab(controlColSpan > 1, false)
				.minSize(50, SWT.DEFAULT).create());

		if (fillRest) {
			for (int i = 0; i < PvLayoutUtils.NUM_COLS - controlColSpan; i++) {
				new Label(parent, SWT.NONE);
			}
		}
	}

	public static GridData fullRowLayout(int minHeight) {
		return GridDataFactory.fillDefaults().grab(true, false).minSize(SWT.DEFAULT, minHeight)
				.span(PvLayoutUtils.NUM_COLS, 1).create();
	}

	public static void applySmallWidthHint(Control control) {
		applySmallWidthHint(control::getLayoutData);
	}

	public static void applySmallWidthHint(ILayoutDataGet control) {
		Preconditions.checkArgument(control.getLayoutData() != null && control.getLayoutData() instanceof GridData,
				"Unsupported LayoutData.");
		((GridData) control.getLayoutData()).widthHint = CONTROL_WIDTH_SMALL;
	}

	public static void applyMediumWidthHint(Control control) {
		applyMediumWidthHint(control::getLayoutData);
	}

	public static void applyMediumWidthHint(ILayoutDataGet control) {
		Preconditions.checkArgument(control.getLayoutData() != null && control.getLayoutData() instanceof GridData,
				"Unsupported LayoutData.");
		((GridData) control.getLayoutData()).widthHint = CONTROL_WIDTH_MEDIUM;
	}

	public static void setMinHeight(Control control, int minHeight) {
		Preconditions.checkNotNull(control);
		Preconditions.checkArgument(minHeight > 0);
		Preconditions.checkArgument(control.getLayoutData() instanceof GridData);

		((GridData) control.getLayoutData()).minimumHeight = minHeight;
		((GridData) control.getLayoutData()).heightHint = minHeight;
	}

	public static void checkExpectedLayout(Composite composite) {
		Preconditions.checkArgument(composite.getLayout() instanceof GridLayout);
	}

	/**
	 * Creates an expandable composite together with a {@link Composite} client.
	 * 
	 * The client will have the PLCverif default layout
	 * ({@link PvLayoutUtils#defaultNestedLayout()}). However, the layout data
	 * of the returned expandable composite is not set by this method.
	 * 
	 * @param parent
	 *            The parent composite for the newly created control
	 * @param title
	 *            Title of the expandable composite
	 * @return Created expandable composite, that has a newly created client too
	 *         ({@link ExpandableComposite#getClient()})
	 */
	public static ExpandableComposite createExpandableComposite(Composite parent, String title) {
		FormToolkit toolkit = new FormToolkit(parent.getDisplay());
		ExpandableComposite ret = toolkit.createExpandableComposite(parent,
				ExpandableComposite.TREE_NODE | ExpandableComposite.CLIENT_INDENT);
		ret.setText(title);

		Composite clientComposite = new Composite(ret, SWT.NONE);
		clientComposite.setLayout(PvLayoutUtils.defaultNestedLayout());
		ret.setClient(clientComposite);

		toolkit.dispose();
		return ret;
	}

	/**
	 * Creates an expandable composite for the advanced settings. It creates the
	 * client (nested) composite too. It returns the newly created nested
	 * composite.
	 * 
	 * @param parent
	 *            Parent composite
	 * @return The client composite that is contained by the expandable
	 *         composite
	 */
	public static Composite createAdvancedSettingsExpandableComposite(Composite parent) {
		ExpandableComposite compAdvanced = PvLayoutUtils.createExpandableComposite(parent, "Advanced settings");
		PvLayoutUtils.createRow(parent, compAdvanced, PvLayoutUtils.NUM_COLS, false);
		return (Composite) compAdvanced.getClient();
	}

	public static void applyHeightHint(Control control, int height) {
		Preconditions.checkArgument(control.getLayoutData() instanceof GridData,
				"The given control's layout data does not match the one expected by PvLayoutUtils.");

		((GridData) control.getLayoutData()).heightHint = height;
	}

	public static void applyWidthHint(Control control, int width) {
		Preconditions.checkArgument(control.getLayoutData() instanceof GridData,
				"The given control's layout data does not match the one expected by PvLayoutUtils.");

		((GridData) control.getLayoutData()).widthHint = width;
	}

	/**
	 * Excludes or includes the given control from/in the parent's layout
	 * algorithm.
	 * 
	 * @param control
	 *            Control
	 * @param value
	 *            True if the control should be excluded, false if it should be
	 *            included
	 * @throws IllegalArgumentException
	 *             if the given control does not have a layout data with the
	 *             expected type (i.e., the layout of the control was not done
	 *             by {@link PvLayoutUtils})
	 */
	public static void setExcludeFromLayout(Control control, boolean value) {
		if (control.getLayoutData() instanceof GridData) {
			((GridData) control.getLayoutData()).exclude = value;
		} else {
			throw new IllegalArgumentException("The given control does not have the layout expected by PvLayoutUtils.");
		}
	}
}
