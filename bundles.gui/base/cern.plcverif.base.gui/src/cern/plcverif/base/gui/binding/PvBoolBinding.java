/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.binding;

import java.util.Optional;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsUtils;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;

/**
 * Data binding for an element representing a Boolean {@link SettingsElement}.
 */
public abstract class PvBoolBinding extends PvDataBinding {
	/**
	 * Creates a new data binding for a Boolean {@link SettingsElement}.
	 * 
	 * @param settingsAttribute
	 *            Name of the settings element attribute to be represented
	 * @param parent
	 *            Parent data binding element
	 */
	public PvBoolBinding(String settingsAttribute, PvDataBinding parent) {
		super(settingsAttribute, parent);
	}

	/**
	 * Returns current Boolean value of the binding.
	 * 
	 * @return Current value of binding
	 */
	public abstract boolean getValue();

	/**
	 * Sets the current Boolean value.
	 * 
	 * @param value New value
	 */
	public abstract void setValue(boolean value);

	/**
	 * {@inheritDoc}
	 * 
	 * If the given settings node is not a settings element, no operation is
	 * performed.
	 */
	@Override
	protected void loadThis(Optional<? extends Settings> settings) throws SettingsParserException {
		if (settings.isPresent() && settings.get() instanceof SettingsElement) {
			SettingsElement settingsElement = (SettingsElement) settings.get();
			if (SettingsUtils.isTrue(settingsElement.value())) {
				setValue(true);
			} else if (SettingsUtils.isFalse(settingsElement.value())) {
				setValue(false);
			} else {
				throw new SettingsParserException("Unable to parse as Boolean value: " + settingsElement.value());
			}
		} else {
			setValue(false);
		}
	}

	@Override
	public Optional<Settings> saveThis() {
		return Optional.of(SettingsElement.createRootElement(getSettingsAttribute(), Boolean.toString(getValue())));
	}
}
