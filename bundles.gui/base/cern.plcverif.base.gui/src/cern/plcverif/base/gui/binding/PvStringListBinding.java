/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.binding;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsKind;
import cern.plcverif.base.common.settings.SettingsList;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;

/**
 * Data binding for an element representing a String {@link SettingsList}.
 */
public abstract class PvStringListBinding extends PvDataBinding {
	/**
	 * Creates a new data binding for a String {@link SettingsList}.
	 * 
	 * @param settingsAttribute
	 *            Name of the settings element attribute to be represented
	 * @param parent
	 *            Parent data binding element
	 */
	public PvStringListBinding(String settingsAttribute, PvDataBinding parent) {
		super(settingsAttribute, parent);
	}

	/**
	 * Returns the list of values that are selected in the represented GUI
	 * element.
	 * 
	 * @return List of selected values. Shall never be {@code null}.
	 */
	public abstract List<String> getSelectedValues();

	/**
	 * Sets the list of values that are selected. <br>
	 * Implementations of this method need to set the dirty flag.
	 * 
	 * @param values
	 *            List of values to be selected. Shall never be {@code null}.
	 *            
	 * @throws NullPointerException
	 *             if the given parameter is null
	 */
	public abstract void setSelectedValues(List<String> values);

	/**
	 * Returns the list of permitted (selectable) values in the represented GUI
	 * element.
	 * 
	 * @return List of permitted values. Shall never be {@code null}.
	 */
	public abstract List<String> getPermittedValues();

	/**
	 * Sets the list of values that are permitted to be selected. <br>
	 * Implementations of this method need to set the dirty flag.
	 * 
	 * @param values
	 *            List of values that can be selected. Shall never be
	 *            {@code null}.
	 * 
	 * @throws NullPointerException
	 *             if the given parameter is null
	 * 
	 */
	public abstract void setPermittedValues(List<String> values);

	/**
	 * {@inheritDoc} If the given settings node is not a settings list, no
	 * operation is performed.
	 */
	@Override
	protected void loadThis(Optional<? extends Settings> settings) throws SettingsParserException {
		if (settings.isPresent()) {
			SettingsList settingsList = settings.get().toList();
			List<String> items = settingsList.elements().stream()
					.filter(it -> it != null && it.kind() == SettingsKind.ELEMENT)
					.map(it -> ((SettingsElement) it).value()).collect(Collectors.toList());
			setSelectedValues(items);
		} else {
			setSelectedValues(Collections.emptyList());
		}
	}

	@Override
	public Optional<Settings> saveThis() {
		SettingsList list = SettingsList.empty();
		for (String selectedItem : getSelectedValues()) {
			list.add(SettingsElement.createRootElement("", selectedItem));
		}
		return Optional.of(list);
	}

	@Override
	protected void addChild(PvDataBinding child) {
		throw new UnsupportedOperationException("A PvStringListBinding cannot have PvDataBinding children.");
	}
}
