/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.binding.listeners;

import cern.plcverif.base.gui.binding.PvDataBinding;

/**
 * Dirty state change functional interface.
 */
@FunctionalInterface
public interface IDirtyStateChangeListener {
	/**
	 * Method that is invoked when the dirty state of the binding it is listening to has changed.
	 * 
	 * @param binding
	 *            The binding whose dirty state has changed.
	 */
	void onDirtyChange(PvDataBinding binding);
}
