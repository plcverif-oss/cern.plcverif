/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.binding;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * An object having a key and a label (display value).
 */
public interface IKeyLabelPair {
	/**
	 * Returns the key of this object.
	 * 
	 * @return Key
	 */
	String getKey();

	/**
	 * Returns the display value (label) of this object.
	 * 
	 * @return Display value (label)
	 */
	String getLabel();

	/**
	 * Basic implementation of the {@link IKeyLabelPair} interface as a pair.
	 */
	public static class Impl implements IKeyLabelPair {
		private String key;
		private String label;

		/**
		 * Creates a mew
		 * 
		 * @param key
		 * @param label
		 */
		public Impl(String key, String label) {
			this.key = key;
			this.label = label;
		}

		@Override
		public String getKey() {
			return key;
		}

		@Override
		public String getLabel() {
			return label;
		}
		
		/**
		 * Returns a list of {@link IKeyLabelPair}s representing the given map.
		 * 
		 * Each map entry will be represented by a corresponding element in the returned list of key-label pairs.
		 * 
		 * @param map Map to represent
		 * @return Representation
		 */
		public static List<? extends IKeyLabelPair> createFromMap(Map<String, String> map) {
			return map.entrySet().stream().map(it -> new Impl(it.getKey(), it.getValue())).collect(Collectors.toList());
		}
	}
}
