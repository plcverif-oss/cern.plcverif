/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.swt;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.LabelProviderChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import com.google.common.base.Preconditions;

import cern.plcverif.base.gui.binding.IKeyLabelPair;

public class CheckboxList implements ILayoutSettable {
	private CheckboxTableViewer cbTblViewer;
	private StringListLabelProvider labelProvider;

	public CheckboxList(Composite parent) {
		createWidgets(parent);
	}

	private void createWidgets(Composite parent) {
		cbTblViewer = CheckboxTableViewer.newCheckList(parent, SWT.BORDER | SWT.FULL_SELECTION);
		cbTblViewer.setContentProvider(new StringListContentProvider());
		cbTblViewer.setInput(Collections.emptyList());
		labelProvider = new StringListLabelProvider();
		cbTblViewer.setLabelProvider(labelProvider);
	}

	public void setLayoutData(Object data) {
		cbTblViewer.getTable().setLayoutData(data);
	}

	/**
	 * Returns the list of items that can be chosen in this checkbox list.
	 * Returns null if it is not set correctly.
	 * 
	 * @return Items that can be chosen. Returns {@code null} if not set
	 *         correctly.
	 */
	@SuppressWarnings("unchecked")
	public List<String> getInput() {
		if (cbTblViewer.getInput() instanceof List<?>) {
			return Collections.unmodifiableList((List<String>) cbTblViewer.getInput());
		} else {
			return null;
		}
	}

	/**
	 * Sets the list of items that can be chosen in this checkbox list. The
	 * given items will be shown exactly as given.
	 * 
	 * @param content
	 *            Content to be shown in the checkbox list
	 */
	public void setInput(List<String> content) {
		cbTblViewer.setInput(content);
		labelProvider.setKeyLabelMap(null);
	}

	/**
	 * Sets the list of items that can be chosen in this checkbox list. For each
	 * item, the key will be used as data that can be fetched by
	 * {@link #getSelectedItems()}, the labels will be used as representations
	 * shown to the users.
	 * 
	 * @param content
	 *            Content to be shown in the checkbox list. Shall not be
	 *            {@code null}.
	 */
	public void setInputWitLabels(List<IKeyLabelPair> content) {
		Preconditions.checkNotNull(content);
		cbTblViewer.setInput(content.stream().map(it -> it.getKey()).collect(toList()));
		labelProvider
				.setKeyLabelMap(content.stream().collect(Collectors.toMap(it -> it.getKey(), it -> it.getLabel())));
	}

	/**
	 * Registers the given key-label pairs, <i>without</i> modifying the input.
	 * 
	 * @param content
	 *            Labels to be used in the checkbox list. Shall not be
	 *            {@code null}.
	 */
	public void setLabels(List<? extends IKeyLabelPair> content) {
		Preconditions.checkNotNull(content);
		labelProvider
				.setKeyLabelMap(content.stream().collect(Collectors.toMap(it -> it.getKey(), it -> it.getLabel())));
	}

	/**
	 * Returns the selected items.
	 * 
	 * @return List of selected items. Never {@code null}.
	 */
	public List<String> getSelectedItems() {
		Object[] selectedItems = cbTblViewer.getCheckedElements();
		if (selectedItems.length == 0) {
			return Collections.emptyList();
		} else {
			List<String> ret = new ArrayList<>();
			for (Object it : selectedItems) {
				ret.add(it.toString());
			}
			return ret;
		}
	}

	/**
	 * Makes the given items selected. Any item in the given list that is not a
	 * known item will be discarded.
	 * 
	 * @param selectedItems
	 *            Items to be selected.
	 */
	public void setSelectedItems(List<String> selectedItems) {
		cbTblViewer.setCheckedElements(selectedItems.toArray());
	}

	public void addCheckStateListener(ICheckStateListener listener) {
		cbTblViewer.addCheckStateListener(listener);
	}

	public Control getUnderlyingSwtControl() {
		return cbTblViewer.getControl();
	}

	private static class StringListContentProvider implements IStructuredContentProvider {
		@Override
		public Object[] getElements(Object inputElement) {
			if (inputElement instanceof List<?>) {
				return ((List<?>) inputElement).toArray();
			} else {
				return new Object[] {};
			}
		}
	}

	/**
	 * Label provider that returns the label corresponding to a key from a map
	 * if present, otherwise the label will be equivalent to the key.
	 */
	private static class StringListLabelProvider extends ColumnLabelProvider {
		private Map<String, String> keyLabelMap = null;

		/**
		 * Sets the key-label map. Null value is accepted, in that case every
		 * label will be equivalent to the keys.
		 */
		public void setKeyLabelMap(Map<String, String> keyLabelMap) {
			this.keyLabelMap = keyLabelMap;
			fireLabelProviderChanged(new LabelProviderChangedEvent(this));
		}

		@Override
		public String getText(Object element) {
			if (keyLabelMap != null && keyLabelMap.containsKey(element.toString())) {
				return keyLabelMap.get(element.toString());
			} else {
				return element.toString();
			}
		}
	}
}
