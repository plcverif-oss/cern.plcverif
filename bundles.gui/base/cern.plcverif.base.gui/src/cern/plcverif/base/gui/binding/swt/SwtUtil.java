/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.binding.swt;

import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Control;

/**
 * Utility methods for SWT-related data binding operations.
 * 
 * To be used only locally, within this package.
 */
final class SwtUtil {
	private SwtUtil() {
		// Utility class
	}

	public static ControlDecoration createInvalidityControlDecoration(Control control, String defaultText) {
		// Create decoration for invalidity
		ControlDecoration invalidityDecoration = new ControlDecoration(control, SWT.LEFT | SWT.TOP);
		invalidityDecoration.setDescriptionText(defaultText);
		FieldDecoration fieldDecoration = FieldDecorationRegistry.getDefault()
				.getFieldDecoration(FieldDecorationRegistry.DEC_ERROR);
		invalidityDecoration.setImage(fieldDecoration.getImage());

		return invalidityDecoration;
	}
}
