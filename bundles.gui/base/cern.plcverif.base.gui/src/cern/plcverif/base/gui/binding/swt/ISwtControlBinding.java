/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.binding.swt;

import org.eclipse.swt.widgets.Control;

import cern.plcverif.base.gui.binding.PvDataBinding;

/**
 * Interface through which the SWT control represented by a data binding element
 * may be accessed. It only makes sense for SWT control-backed data binding
 * elements. To be used on subclasses of {@link PvDataBinding}.
 */
public interface ISwtControlBinding {
	/**
	 * Returns the SWT control whose value is bound by this data binding
	 * element.
	 * 
	 * @return SWT control
	 */
	Control getControl();
}
