/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.binding;

import java.util.Optional;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsList;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.gui.binding.exceptions.DataInvalidException;

/**
 * Data binding for an element representing a {@link SettingsList} consisting of
 * other {@link Settings} nodes.
 * 
 * This is for special use cases only, where the list elements represent
 * "positional values", for example in case of requirement pattern parameters.
 */
public class PvElementListBinding extends PvDataBinding {
	/**
	 * Creates a new data binding for a String {@link SettingsList}.
	 * 
	 * @param settingsAttribute
	 *            Name of the settings element attribute to be represented
	 * @param parent
	 *            Parent data binding element
	 */
	public PvElementListBinding(String settingsAttribute, PvDataBinding parent) {
		super(settingsAttribute, parent);
	}

	/**
	 * {@inheritDoc} If the given settings node is not a settings list, no
	 * operation is performed.
	 */
	@Override
	protected void loadThis(Optional<? extends Settings> settings) throws SettingsParserException, DataInvalidException {
		if (settings.isPresent()) {
			SettingsList settingsList = settings.get().toList();

			if (settingsList.elements().size() > this.getChildren().size()) {
				throw new SettingsParserException(
						String.format("The settings '%s' contains too many children items.", settingsList.fqn()));
			}

			for (int i = 0; i < this.getChildren().size(); i++) {
				Settings settingToLoad = i < settingsList.elements().size() ? settingsList.elements().get(i) : null;
				PvDataBinding childBinding = this.getChildren().get(i);
				childBinding.load(Optional.ofNullable(settingToLoad));
			}
		} else {
			// Log at least?
		}
	}
	
	@Override
	protected void loadChildren(Optional<? extends Settings> settings) throws SettingsParserException {
		// Do nothing
	}

	@Override
	public Optional<Settings> saveThis() throws DataInvalidException {
		SettingsList list = SettingsList.empty();
		for (PvDataBinding childBinding : this.getChildren()) {
			list.add(childBinding.save().orElse(null));
		}
		return Optional.of(list);
	}
	
	@Override
	protected Optional<Settings> saveChildren(Optional<Settings> thisSaved) throws DataInvalidException {
		// Do nothing.
		return thisSaved;
	}

	@Override
	protected boolean isThisValid() {
		// The validity of children will be checked independently.
		return true;
	}
}
