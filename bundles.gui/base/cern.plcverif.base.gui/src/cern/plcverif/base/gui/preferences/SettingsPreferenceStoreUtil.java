/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.preferences;

import java.util.Optional;

import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.swt.widgets.Composite;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.logging.PlcverifSeverity;
import cern.plcverif.base.common.settings.EffectiveSettings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.exceptions.SettingsSerializerException;
import cern.plcverif.base.common.settings.specific.AbstractSpecificSettings;

public final class SettingsPreferenceStoreUtil {
	public static FieldEditor createSeverityFieldEditor(String name, String labelText, Composite parentComposite) {
		return new RadioGroupFieldEditor(name, labelText, 1,
				new String[][] { { "Error", PlcverifSeverity.Error.name() },
						{ "Warning", PlcverifSeverity.Warning.name() }, { "Info", PlcverifSeverity.Info.name() },
						{ "Debug", PlcverifSeverity.Debug.name() } },
				parentComposite);
	}
	
	/**
	 * Returns the union of the two settings. If an element is present in both, {@code resourceSettings} takes priority.
	 * @param hardcodedSettings The default settings hardcoded in a specific settings class.
	 * @param resourceSettings The settings loaded from the plug-ins resources.
	 * @return The union of the two settings.
	 */
	public static SettingsElement mergePluginSettings(AbstractSpecificSettings hardcodedSettings, SettingsElement resourceSettings) {
		Preconditions.checkNotNull(hardcodedSettings);
		Preconditions.checkNotNull(resourceSettings);
		
		try {
			return EffectiveSettings.pluginEffectiveSettings(hardcodedSettings, resourceSettings, Optional.empty()).toSingle();
		} catch (SettingsParserException | SettingsSerializerException e) {
			// TODO this error handling could be improved
			return resourceSettings;
		}
	}
	
	private SettingsPreferenceStoreUtil() {
		// Utility class.
	}
}
