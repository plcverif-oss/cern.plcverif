/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.eclipse;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.viewers.IStructuredSelection;

import com.google.common.base.Preconditions;
import com.google.common.io.CharStreams;

import cern.plcverif.base.common.logging.PlatformLogger;

/**
 * Utility class for input/output handling in Eclipse.
 */
public final class EclipseIoUtils {
	private EclipseIoUtils() {
		// Utility class.
	}

	/**
	 * Loads all lines of the given text file.
	 * 
	 * @param file
	 *            File to load
	 * @return Lines of the given file
	 * @throws IOException
	 *             if loading was not successful
	 */
	public static List<String> loadAllLines(IFile file) throws IOException {
		Preconditions.checkNotNull(file, "File to be loaded should not be null.");

		try (InputStreamReader reader = new InputStreamReader(file.getContents(true), file.getCharset())) {
			List<String> content;
			content = CharStreams.readLines(reader);
			return content;
		} catch (CoreException e) {
			throw new IOException(String.format("Unable to load '%s'.", file.getName()), e);
		}
	}

	/**
	 * Loads the content of the given text file.
	 * 
	 * @param file
	 *            File to load
	 * @return Content of the given file
	 * @throws IOException
	 *             if loading was not successful
	 */
	public static String loadText(IFile file) throws IOException {
		return String.join(System.lineSeparator(), loadAllLines(file));
	}

	/**
	 * Sets the content of the given output file to the given text. UTF-8
	 * encoding will be used. If the given file does not exist, this method will
	 * try to create it.
	 * 
	 * @param outFile
	 *            File to be written. Shall not be {@code null}.
	 * @param contentToSave
	 *            Textual content to be written. Shall not be {@code null}.
	 * @param monitor
	 *            Progress monitor. Shall not be {@code null}.
	 * @throws IOException
	 *             if saving was unsuccessful
	 */
	public static void setContents(IFile outFile, String contentToSave, IProgressMonitor monitor) throws IOException {
		Preconditions.checkNotNull(outFile, "outFile shall not be null");
		Preconditions.checkNotNull(contentToSave, "contentToSave shall not be null");
		Preconditions.checkNotNull(monitor, "monitor shall not be null");

		// Save text to file
		try (InputStream stream = new ByteArrayInputStream(contentToSave.getBytes(StandardCharsets.UTF_8))) {
			if (outFile.exists()) {
				outFile.setCharset("utf8", monitor);
				outFile.setContents(stream, IResource.KEEP_HISTORY, monitor);
			} else {
				outFile.create(stream, IResource.NONE, monitor);
				outFile.setCharset("utf8", monitor);
			}
		} catch (CoreException e) {
			throw new IOException(String.format("Unable to save the file '%s'.", outFile.getName()), e);
		}
	}

	/**
	 * Returns a list of all selected files. If the selection contains
	 * {@linkplain IContainer containers}, all files contained by them will be
	 * included.
	 * 
	 * @param selection
	 *            Selection
	 * @return Files in the selection
	 * @throws CoreException
	 */
	public static List<IFile> getAllSelectedFiles(IStructuredSelection selection) throws CoreException {
		List<IFile> files = new ArrayList<>();
		Iterator<?> iterator = selection.iterator();

		while (iterator.hasNext()) {
			final Object element = iterator.next();

			if (element instanceof IFile) {
				// IFile: add to collection
				files.add((IFile) element);
			} else if (element instanceof IContainer) {
				// IContainer: add all contents to collection recursively
				IContainer container = (IContainer) element;
				collectAllFilesOfContainer(container, files);
			}
		}

		return files;
	}

	/**
	 * Collects all files in the given container (and its subcontainers
	 * recursively) in the given accumulator.
	 * 
	 * @param container
	 *            Container in which files need to be collected
	 * @param files
	 *            Accumulator for the files
	 * @throws CoreException
	 */
	public static void collectAllFilesOfContainer(IContainer container, Collection<IFile> files) throws CoreException {
		if (container == null) {
			return;
		}

		for (final IResource res : container.members()) {
			if (res instanceof IFile) {
				IFile file = (IFile) res;
				files.add(file);
			} else if (res instanceof IContainer) {
				IContainer folder = (IContainer) res;
				collectAllFilesOfContainer(folder, files);
			}
		}
	}

	/**
	 * Returns the given project (base project) and all its referenced projects.
	 * The project references are not resolved recursively (i.e., the project
	 * referenced from a project referenced from the base project will not be
	 * included).
	 * 
	 * @param baseProject
	 *            Base project. Shall not be {@code null}.
	 * @return The list of projects containing the base project and its
	 *         referenced projects
	 */
	public static List<IProject> getProjectAndItsReferences(IProject baseProject) {
		Preconditions.checkNotNull(baseProject);

		List<IProject> projects = new ArrayList<>();
		projects.add(baseProject);

		try {
			projects.addAll(Arrays.asList(baseProject.getReferencedProjects()));
		} catch (CoreException e) {
			PlatformLogger.logInfo("Unable to load the referenced projects: " + e.getMessage());
		}

		return projects;
	}

	/**
	 * Returns the files in the given projects satisfying the given filter
	 * predicate.
	 * 
	 * @param projects
	 *            Collection of projects where files are searched for. Shall not
	 *            be {@code null}.
	 * @param filter
	 *            Filter predicate. Only the files that <b>satisfy</b> the
	 *            filter expression will be included in the result. Shall not be
	 *            {@code null}.
	 * @param processSubcontainers
	 *            If true, the files in the subcontainers (subfolders) will also
	 *            be included in the result. If false, only the direct members
	 *            of the given projects will be processed.
	 * @return An iterable of the files found in the given projects satisfying
	 *         the filter predicate
	 * @throws CoreException
	 *             if accessing the members of one of the projects was
	 *             unsuccessful
	 */
	public static Iterable<IFile> getFiles(Collection<IProject> projects, Predicate<IFile> filter,
			boolean processSubcontainers) throws CoreException {
		Preconditions.checkNotNull(projects);
		Preconditions.checkNotNull(filter);

		List<IFile> ret = new ArrayList<>();

		for (IProject project : projects) {
			collectFiles(project, filter, processSubcontainers, ret);
		}

		return ret;
	}

	private static void collectFiles(IContainer container, Predicate<IFile> filter, boolean processSubcontainers,
			List<IFile> accu) throws CoreException {
		Preconditions.checkNotNull(container);
		Preconditions.checkNotNull(filter);

		for (IResource res : container.members()) {
			if (res instanceof IFile) {
				// 'res' is guaranteed not to be null if instanceof returns
				// with true
				IFile file = (IFile) res;

				if (filter.test(file)) {
					accu.add(file);
				}
			} else if (processSubcontainers && res instanceof IContainer) {
				collectFiles((IContainer) res, filter, processSubcontainers, accu);
			}
		}
	}

	/**
	 * Returns the name of the given file without its extension. If the
	 * extension of the given file is empty or null, the returned value equals
	 * to the name of the file ({@link IFile#getName()}).
	 * 
	 * @param file
	 *            File
	 * @return Name of the file without extension
	 */
	public static String getFileNameWithoutExtension(IFile file) {
		Preconditions.checkNotNull(file, "Null file");

		String name = file.getName();
		Preconditions.checkNotNull(name, "The given file has null as name");

		if (file.getFileExtension() == null) {
			return name;
		} else {
			return name.substring(0, Math.max(0, name.length() - file.getFileExtension().length() - 1));
		}
	}
}
