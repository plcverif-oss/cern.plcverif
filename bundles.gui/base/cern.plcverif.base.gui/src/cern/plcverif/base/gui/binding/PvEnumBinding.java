/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.binding;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.gui.binding.exceptions.DataInvalidException;

/**
 * Data binding for an element representing an enumeration
 * {@link SettingsElement}, i.e., whose value should be one of a given list of
 * keys.
 */
public abstract class PvEnumBinding extends PvDataBinding {
	/**
	 * Sorted items handled by this data binding handler.
	 */
	protected List<? extends IKeyLabelPair> permittedItems;

	/**
	 * Unmodifiable set of keys handled by this data binding handler. This set
	 * contains the keys of items.
	 */
	protected Set<String> handledKeys;

	/**
	 * Creates a new data binding for a Boolean {@link SettingsElement}.
	 * 
	 * @param settingsAttribute
	 *            Name of the settings element attribute to be represented
	 * @param parent
	 *            Parent data binding element
	 * @param items
	 *            List of items that can be selected
	 */
	public PvEnumBinding(String settingsAttribute, PvDataBinding parent, List<? extends IKeyLabelPair> items) {
		super(settingsAttribute, parent);

		setPermittedItemsInternal(items);
	}

	protected final void setPermittedItemsInternal(List<? extends IKeyLabelPair> items) {
		// Refresh contents
		this.permittedItems = Preconditions.checkNotNull(items);
		this.handledKeys = Collections
				.unmodifiableSet(items.stream().map(it -> it.getKey()).collect(Collectors.toSet()));
	}

	/**
	 * Returns the key of the selected item.
	 * 
	 * @return Key of the item selected, or {@link Optional#empty()} if no item
	 *         is selected
	 */
	public abstract Optional<String> getSelectedItem();

	/**
	 * Modifies the selected item.
	 * 
	 * @param key
	 *            Key of the new item to be selected. It must be one of the keys
	 *            of the handled items. Shall not be {@code null}.
	 * @throws DataInvalidException if the given key is not known by this data binding element
	 */
	public abstract void setSelectedItem(String key) throws DataInvalidException;

	/**
	 * {@inheritDoc}
	 * 
	 * If the given settings node is not a settings element, no operation is
	 * performed.
	 */
	@Override
	protected void loadThis(Optional<? extends Settings> settings) throws SettingsParserException, DataInvalidException {
		if (settings.isPresent() && settings.get() instanceof SettingsElement) {
			SettingsElement settingsElement = (SettingsElement) settings.get();

			setSelectedItem(settingsElement.value());
			// Error handling is done in setValue
		} else {
			if (!permittedItems.isEmpty()) {
				setSelectedItem(permittedItems.get(0).getKey());
			}
		}
	}

	@Override
	public Optional<Settings> saveThis() {
		if (getSelectedItem().isPresent()) {
			return Optional.of(SettingsElement.createRootElement(getSettingsAttribute(), getSelectedItem().get()));
		} else {
			return Optional.empty();
		}
	}
}
