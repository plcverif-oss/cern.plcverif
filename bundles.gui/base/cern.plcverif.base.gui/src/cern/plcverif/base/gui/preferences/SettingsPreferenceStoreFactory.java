/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.preferences;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;

public final class SettingsPreferenceStoreFactory {
	private SettingsPreferenceStoreFactory() {
		// Utility class.
	}

	public static SettingsPreferenceStore createStore(String pluginId, SettingsElement defaultSettings)
			throws SettingsParserException, IOException {
		// Create the folder(s) if does not exist
		Path settingsFolder = Paths.get(PlcverifPreferenceAccess.getSettingsFolder());
		if (!Files.exists(settingsFolder)) {
			Files.createDirectories(settingsFolder);
		}
		
		Path settingsFile = Paths.get(PlcverifPreferenceAccess.getSettingsFolder(), pluginId + ".settings");

		return new SettingsPreferenceStore(settingsFile, defaultSettings);
	}
}
