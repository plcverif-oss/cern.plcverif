/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.binding.swt;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.swt.widgets.Control;

import com.google.common.base.Preconditions;

import cern.plcverif.base.gui.binding.IKeyLabelPair;
import cern.plcverif.base.gui.binding.PvDataBinding;
import cern.plcverif.base.gui.binding.PvStringListBinding;
import cern.plcverif.base.gui.swt.CheckboxList;

/**
 * Data binding for an element representing a String
 * {@link cern.plcverif.base.common.settings.SettingsList} by a checkbox list
 * ({@link CheckboxList}) GUI control.
 */
public class PvCheckboxListBinding extends PvStringListBinding implements ISwtControlBinding {
	@FunctionalInterface
	public interface Validator {
		/**
		 * Returns true iff the current selection in the control passed as
		 * parameter is valid.
		 * 
		 * @param binding
		 *            The checkbox list data binding to be validated.
		 * @return True if the selection in the checkbox list is valid.
		 */
		boolean isSelectionValid(PvCheckboxListBinding binding);
	}

	/**
	 * Represented checkbox list GUI control.
	 */
	private CheckboxList checkboxList;

	/**
	 * Control decoration representing invalidity of the value in the checkbox
	 * list.
	 */
	private ControlDecoration invalidityDecoration;

	/**
	 * Validator function. Returns true iff the text in the represented textbox
	 * is valid.
	 */
	private Validator validator = control -> true;

	/**
	 * Creates a new data binding for a checkbox list.
	 * 
	 * @param checkboxList
	 *            The GUI checkbox list control to be bound
	 * @param settingsAttribute
	 *            Name of the settings element attribute to be represented
	 * @param parent
	 *            Parent data binding element
	 */
	public PvCheckboxListBinding(CheckboxList checkboxList, String settingsAttribute, PvDataBinding parent) {
		super(settingsAttribute, parent);
		this.checkboxList = Preconditions.checkNotNull(checkboxList, "The represented CheckboxList cannot be null.");

		// Register for modification event
		this.checkboxList.addCheckStateListener(new ICheckStateListener() {
			@Override
			public void checkStateChanged(CheckStateChangedEvent event) {
				setDirty();
				fireChangeListeners();
				refresh();
			}
		});

		// Create decoration for invalidity
		invalidityDecoration = SwtUtil.createInvalidityControlDecoration(checkboxList.getUnderlyingSwtControl(),
				"The selection is invalid.");

		// no need to refresh the invalidity decoration: no validation rule is
		// given yet
	}

	@Override
	protected void refresh() {
		super.refresh();
		refreshInvalidityDecoration();
	}

	private void refreshInvalidityDecoration() {
		if (isThisValid()) {
			invalidityDecoration.hide();
		} else {
			invalidityDecoration.show();
		}
	}

	@Override
	public List<String> getPermittedValues() {
		return checkboxList.getInput();
	}

	@Override
	public void setPermittedValues(List<String> values) {
		Preconditions.checkNotNull(values);

		// Check if there is any selected value that cannot be selected after
		// this action.
		boolean modificationOfSelection = getSelectedValues().stream().anyMatch(it -> !values.contains(it));

		checkboxList.setInput(new ArrayList<>(values));
		if (modificationOfSelection) {
			setDirty();
		}

		refresh();
		getControl().requestLayout();
	}

	/**
	 * Sets the list of values that are permitted to be selected. The keys in
	 * the given key-label pairs will be used internally (e.g., in
	 * {@link #getSelectedValues()}, but the elements will be visualized using
	 * the corresponding labels.
	 * 
	 * @param values
	 *            List of values (and the corresponding labels) that can be
	 *            selected. Shall never be {@code null}.
	 * @throws NullPointerException
	 *             if the given parameter is null
	 */
	public void setPermittedValuesWithLabels(List<? extends IKeyLabelPair> values) {
		Preconditions.checkNotNull(values);
		
		setPermittedValues(values.stream().map(it -> it.getKey()).collect(toList()));
		checkboxList.setLabels(values);
	}

	@Override
	public List<String> getSelectedValues() {
		return checkboxList.getSelectedItems();
	}

	@Override
	public void setSelectedValues(List<String> values) {
		checkboxList.setSelectedItems(Preconditions.checkNotNull(new ArrayList<>(values)));
		setDirty();
		refresh();
	}

	@Override
	protected boolean isThisValid() {
		return validator.isSelectionValid(this);
	}

	/**
	 * Sets the validator function for the represented data.
	 * 
	 * @param validator
	 *            Checkbox list selection validator.
	 * @param errorMessage
	 *            Message to be shown if the validation fails.
	 */
	public void setValidator(Validator validator, String errorMessage) {
		this.validator = Preconditions.checkNotNull(validator, "Null validator is invalid.");
		this.invalidityDecoration.setDescriptionText(errorMessage);
		refresh();
	}

	@Override
	public Control getControl() {
		return checkboxList.getUnderlyingSwtControl();
	}

	@Override
	public Optional<String> getDiagnosisMessage() {
		return Optional.ofNullable(invalidityDecoration.getDescriptionText());
	}
}
