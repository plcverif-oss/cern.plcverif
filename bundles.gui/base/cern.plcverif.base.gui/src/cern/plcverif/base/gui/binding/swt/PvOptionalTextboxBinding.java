/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.binding.swt;

import java.util.Optional;

import org.eclipse.swt.widgets.Text;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.gui.binding.PvDataBinding;

/**
 * Data binding for an element representing a String
 * {@link cern.plcverif.base.common.settings.SettingsElement} by a textbox
 * ({@link Text}) SWT control.
 * The content of the textbox is not saved if it is empty (hence "optional").
 */
public class PvOptionalTextboxBinding extends PvTextboxBinding {
		public PvOptionalTextboxBinding(Text textbox, String settingsAttribute, PvDataBinding parent) {
			super(textbox, settingsAttribute, parent);
		}

		@Override
		protected Optional<Settings> saveThis() {
			if (getValue() == null || getValue().equals("")) {
				// don't save if empty
				return Optional.empty();
			} else {
				return super.saveThis();
			}
		}
	}