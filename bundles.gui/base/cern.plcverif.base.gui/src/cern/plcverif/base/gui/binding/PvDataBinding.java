/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.binding;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.gui.binding.exceptions.DataInvalidException;
import cern.plcverif.base.gui.binding.listeners.IDirtyStateChangeListener;
import cern.plcverif.base.gui.binding.listeners.IValueChangeListener;

/**
 * Data binding element. It handles the representation of a SettingsElement or
 * SettingsList node or subtree.
 */
public abstract class PvDataBinding {
	/**
	 * Attribute name of the settings node that this object represents. The full
	 * FQN of a settings element is given by this value and the similar values
	 * of all the parents.
	 * 
	 * This value is null only if this is a root control element.
	 */
	private String settingsAttribute = null;

	/**
	 * The parent data binding element. Null if and only if this is the root
	 * element.
	 */
	private PvDataBinding parent;

	/**
	 * List of children data binding elements.
	 */
	private List<PvDataBinding> children = new ArrayList<>();

	/**
	 * True iff the current data binding element is enabled. Only enabled
	 * elements are saved, however non-enabled elements are also loaded.
	 */
	private boolean enabled = true;

	/**
	 * True iff the current data binding element needs saving as it has changed
	 * its state since last loading.
	 */
	private boolean dirty = false;

	/**
	 * Listeners to the value change of the data that this data binding element
	 * describes. It is lazily initialized, thus it is null until the first
	 * listener is added.
	 */
	private List<IValueChangeListener> valueChangeListeners = null;

	/**
	 * Listeners to be executed when the dirty property changes.
	 */
	private List<IDirtyStateChangeListener> onDirtyStateChange = null;

	/**
	 * Creates a new data binding element. This element will represent the
	 * attribute with the given name within the given parent.
	 * 
	 * @param settingsAttribute
	 *            Name of the settings attribute to be represented
	 * @param parent
	 *            Parent data binding. Can be {@code null} if and only if this
	 *            is the root element.
	 */
	protected PvDataBinding(String settingsAttribute, PvDataBinding parent) {
		this.settingsAttribute = Preconditions.checkNotNull(settingsAttribute);
		this.parent = parent;
		if (this.parent != null) {
			this.parent.addChild(this);
		}
	}

	/**
	 * Parent data binding element.
	 * 
	 * @return Parent. Return value is {@code null} only if this is a root data
	 *         binding element (i.e., {@link #isRoot()} is true).
	 */
	public PvDataBinding getParent() {
		return parent;
	}

	/**
	 * Adds a new child to this data binding element. It is expected that the
	 * children already has this element set as parent.
	 * 
	 * @param child
	 *            Child to be added. Shall not be {@code null}.
	 * @throws IllegalArgumentException
	 *             if the given child does not have this controller element as
	 *             parent
	 */
	protected void addChild(PvDataBinding child) {
		Preconditions.checkNotNull(child);
		Preconditions.checkArgument(child.getParent() == this);
		this.children.add(child);
	}

	/**
	 * Unmodifiable list of children data binding elements of this controller.
	 * 
	 * @return Children. Never {@code null}.
	 */
	public List<PvDataBinding> getChildren() {
		return Collections.unmodifiableList(children);
	}

	/**
	 * Returns true iff this data binding element has at least one child.
	 * 
	 * @return True if this controller has children.
	 */
	public boolean hasChildren() {
		return !children.isEmpty();
	}

	/**
	 * Returns true iff this is a root data binding element, i.e., it does not
	 * have a parent.
	 * 
	 * @return True if this is a root data binding element.
	 */
	public boolean isRoot() {
		return parent == null;
	}

	/**
	 * Returns the root data binding element of this data binding tree.
	 * 
	 * @return Root data binding element. For the returned
	 *         {@link PvDataBinding}, {@link #isRoot()} is true.
	 */
	public PvDataBinding getRoot() {
		// PERF cache
		PvDataBinding current = this;
		while (!current.isRoot()) {
			current = current.getParent();
		}
		return current;
	}

	/**
	 * The name of the represented attribute.
	 * 
	 * @return Attribute name
	 */
	public String getSettingsAttribute() {
		return settingsAttribute;
	}

	/**
	 * Returns true iff this data binding element is enabled.
	 * 
	 * @return True if this is enabled.
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Changes whether this data binding element is enabled.
	 * 
	 * @param enabled
	 *            The new value. If true, the data binding element will be
	 *            enabled, otherwise disabled
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;

		if (enabled && isDirty()) {
			parent.setDirty();
		}
	}

	/**
	 * Returns true iff the current data binding element needs saving as it has
	 * changed its state since last loading.
	 * 
	 * @return True if needs saving
	 */
	public boolean isDirty() {
		return dirty;
	}

	/**
	 * Sets the dirty flag, i.e., registers that the represented control changed
	 * its value. To be set by the concrete implementations.
	 */
	protected void setDirty() {
		this.dirty = true;
		if (onDirtyStateChange != null) {
			onDirtyStateChange.forEach(it -> it.onDirtyChange(this));
		}

		if (!isRoot() && isEnabled()) {
			parent.setDirty();
		}
	}

	/**
	 * Clears the dirty flag for this data binding element and all its
	 * descendants.
	 */
	public void clearDirty() {
		this.dirty = false;
		for (PvDataBinding child : children) {
			child.clearDirty();
		}

		if (onDirtyStateChange != null) {
			onDirtyStateChange.forEach(it -> it.onDirtyChange(this));
		}
	}

	/**
	 * Loads the given settings subtree. Based on the contents of the given
	 * settings, the data represented by this data binding element and all its
	 * descendants will be updated. If this data binding element is not enabled,
	 * the settings will not be loaded, neither for the descendants.
	 * 
	 * @param settings
	 *            Settings subtree to be loaded. The root node of this subtree
	 *            is expected to be the node which is represented by this data
	 *            binding element.
	 * @throws SettingsParserException
	 *             if problem occurred while parsing the settings
	 * @throws DataInvalidException
	 *             if the data to be loaded is not acceptable for a given
	 *             binding
	 */
	public void load(Settings settings) throws SettingsParserException, DataInvalidException {
		load(Optional.of(settings));
	}

	/**
	 * Loads the given settings subtree, if present. Based on the contents of
	 * the given settings, the data represented by this data binding element and
	 * all its descendants will be updated. If this data binding element is not
	 * enabled, the settings will STILL BE loaded, ALSO for the descendants.
	 * 
	 * @param settings
	 *            Settings subtree to be loaded. The root node of this subtree
	 *            is expected to be the node which is represented by this data
	 *            binding element. If the given value is not present (is
	 *            {@link Optional#empty()}), no action is performed.
	 * @throws SettingsParserException
	 *             if problem occurred while parsing the settings
	 * @throws DataInvalidException
	 *             if the data to be loaded is not acceptable for a given
	 *             binding
	 */
	public void load(Optional<? extends Settings> settings) throws SettingsParserException, DataInvalidException {
		loadThis(settings);

		this.dirty = false;
		refresh();
		loadChildren(settings);
	}

	/**
	 * Loads the value represented by this data binding element from the given
	 * settings node, if present. <br>
	 * To be implemented by the concrete implementations.
	 * 
	 * @param settings
	 *            Settings node that contains data for this data binding
	 *            element.
	 * @throws SettingsParserException
	 *             if problem occurred while parsing the settings
	 * @throws DataInvalidException
	 *             if the data to be loaded is not acceptable for this binding
	 */
	protected abstract void loadThis(Optional<? extends Settings> settings)
			throws SettingsParserException, DataInvalidException;

	/**
	 * Loads the values represented by the children data binding element from
	 * the given settings node's children, if present.
	 * 
	 * @param settings
	 *            Settings node that contains data for this data binding
	 *            element.
	 * @throws SettingsParserException
	 *             if problem occurred while parsing the settings
	 * @throws DataInvalidException
	 *             if the data to be loaded is not acceptable for at least one
	 *             of the children of the given binding
	 */
	protected void loadChildren(Optional<? extends Settings> settings)
			throws SettingsParserException, DataInvalidException {
		if (settings.isPresent() && settings.get() instanceof SettingsElement) {
			// Distribute to children
			for (PvDataBinding child : getChildren()) {
				child.load(settings.get().toSingle().getAttribute(child.getSettingsAttribute()));
			}
		}
	}

	/**
	 * Serializes the data contained by this data binding element together with
	 * its descendants into a {@linkplain Settings settings} subtree. If this
	 * data binding element is disabled, the returned value is
	 * {@link Optional#empty()}.
	 * 
	 * @return The data contained by this and its descendants, as
	 *         {@link Settings} tree.
	 * @throws DataInvalidException
	 *             if an invalid (i.e., {@link #isThisAndChildrenValid()} is
	 *             false) data binding element is being serialized
	 */
	public Optional<Settings> save() throws DataInvalidException {
		if (isEnabled()) {
			Optional<Settings> ret = saveThis();
			if (!this.isThisValid()) {
				String errorMessage = getDiagnosisMessage().orElse(
						String.format("Invalid data found for the attribute '%s'.", this.getSettingsAttribute()));
				throw new DataInvalidException(errorMessage, this);
			}
			return saveChildren(ret);
		} else {
			return Optional.empty();
		}
	}

	/**
	 * Serializes the data contained by this data binding element into a
	 * {@linkplain Settings settings} node. If this data binding element is
	 * disabled, this function is not expected to be called.
	 * 
	 * @return The data contained by this, as {@link Settings} node.
	 * @throws DataInvalidException
	 *             if an invalid (i.e., {@link #isThisValid()} is false) data
	 *             binding element is being serialized
	 */
	protected abstract Optional<Settings> saveThis() throws DataInvalidException;

	/**
	 * Serializes the data contained by the descendants of this data binding
	 * element into a {@linkplain Settings settings} subtree. If the given value
	 * is not present, a new {@link SettingsElement} is created to hold the
	 * children, otherwise the children settings nodes will be attached to the
	 * given parameter. If this data binding element is disabled, this function
	 * is not expected to be called.
	 * 
	 * @return The given settings node, extended with the data contained by the
	 *         children of this.
	 */
	protected Optional<Settings> saveChildren(Optional<Settings> thisSaved) throws DataInvalidException {
		Optional<Settings> settingsToFill;

		if (!thisSaved.isPresent() && this.hasChildren()) {
			settingsToFill = Optional.of(SettingsElement.createRootElement(getSettingsAttribute()));
		} else {
			settingsToFill = thisSaved;
		}

		if (settingsToFill.isPresent() && settingsToFill.get() instanceof SettingsElement) {
			// Collect children data
			for (PvDataBinding child : getChildren()) {
				Optional<Settings> childSettings = child.save();
				if (childSettings.isPresent()) {
					// The value thisSaved.get is a SettingsElement for sure at
					// this point due to the "if" above.
					((SettingsElement) settingsToFill.get()).setAttribute(child.getSettingsAttribute(),
							childSettings.get());
				}
			}
		}
		return settingsToFill;
	}

	/**
	 * Returns true iff the data represented by this data binding element is
	 * valid. This should not take into account the validity of the descendants
	 * that is handled by the {@link #isThisAndChildrenValid()} method.
	 * 
	 * @return True iff this data is valid
	 */
	protected abstract boolean isThisValid();

	/**
	 * Returns true iff the data represented by <b>the subtree</b> of this data
	 * binding element is valid.
	 * 
	 * @return True iff this data is valid
	 */
	public boolean isThisAndChildrenValid() {
		if (!isThisValid()) {
			return false;
		}

		return getChildren().stream().allMatch(it -> it.isThisValid());
	}

	/**
	 * Refreshes this data binding element and all its descendants.
	 */
	public final void refreshAll() {
		this.refresh();
		children.forEach(it -> it.refresh());
	}

	/**
	 * Refreshes this data binding element if needed. <br>
	 * Concrete implementations based on GUI Controllers may want to refresh the
	 * validity checks and display the results here.
	 */
	protected void refresh() {
		fireChangeListeners();
	}

	/**
	 * Adds a listener to the change of the value represented by this data
	 * binding element. The given listener is fired only if the data of this
	 * binding is changed, but not if any of its descendants change.
	 * 
	 * @param listener
	 *            Value change listener to be added
	 */
	public final void addChangeListener(IValueChangeListener listener) {
		if (valueChangeListeners == null) {
			valueChangeListeners = new ArrayList<>();
		}
		this.valueChangeListeners.add(Preconditions.checkNotNull(listener));
	}

	/**
	 * Fires all registered change listeners. It is expected to be called by the
	 * concrete implementations if the represented value changes.
	 */
	public final void fireChangeListeners() {
		if (valueChangeListeners == null) {
			return;
		} else {
			for (IValueChangeListener listener : valueChangeListeners) {
				listener.onValueChange(this);
			}
		}
	}

	/**
	 * Adds a listener to be executed when the dirty state changes.
	 * 
	 * @param listener
	 *            Listener to be executed.
	 */
	public void addOnDirtyPropertyChange(IDirtyStateChangeListener listener) {
		if (this.onDirtyStateChange == null) {
			this.onDirtyStateChange = new ArrayList<>();
		}
		this.onDirtyStateChange.add(listener);
	}

	/**
	 * Returns detailed, human-readable information about the reason for
	 * non-validity, if available.
	 * 
	 * @return Human-readable information if not valid, otherwise it is expected
	 *         to return {@link Optional#empty()}.
	 */
	public Optional<String> getDiagnosisMessage() {
		return Optional.empty();
	}
	
	protected final String fqn() {
		if (this.getParent() == null) {
			return this.getSettingsAttribute();
		} else {
			String parentFqn = this.getParent().fqn();
			
			if (parentFqn != null && !parentFqn.isEmpty()) {
				return String.format("%s.%s", this.getParent().fqn(), this.getSettingsAttribute());
			} else {
				return this.getSettingsAttribute();
			}
		}
	}
}
