/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.binding.swt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.common.base.Preconditions;

import cern.plcverif.base.gui.DebugTracing;
import cern.plcverif.base.gui.binding.PvDataBinding;
import cern.plcverif.base.gui.swt.CheckboxList;

/**
 * Data binding for an element representing a String
 * {@link cern.plcverif.base.common.settings.SettingsList} by a checkbox list
 * ({@link CheckboxList}) GUI control. This implementation extends
 * {@link PvCheckboxListBinding} with deferred loading support, i.e., if the
 * value selection loaded is not in the list of known values yet, then it waits
 * until it is refreshed instead of throwing errors.
 */
public class PvCheckboxListDeferredBinding extends PvCheckboxListBinding {

	public PvCheckboxListDeferredBinding(CheckboxList checkboxList, String settingsAttribute, PvDataBinding parent) {
		super(checkboxList, settingsAttribute, parent);
	}

	/**
	 * A selection that was not possible to set as current selection because it
	 * was not in the set of handled keys. At next refresh of handled set of
	 * keys ({@link PvCheckboxListDeferredBinding#setPermittedItems(List)}) it
	 * will be retried. If {@link #keepDeferredValuesForever} is true, at all
	 * following update of the handled set of keys, deferred values tried to be
	 * set. {@code null} if there is no deferred value.
	 */
	private List<String> deferredValues = null;

	private boolean keepDeferredValuesForever = false;

	/**
	 * Alters the handling of deferred values.
	 * 
	 * @param newValue
	 *            If true, the deferred values will be tried to be loaded upon
	 *            every {@link #setPermittedValues(List)} call. Otherwise,
	 *            deferred values will be cleared after the next
	 *            {@link #setPermittedValues(List)} call.
	 */
	public void setKeepDeferredValuesForever(boolean newValue) {
		this.keepDeferredValuesForever = newValue;
	}

	@Override
	public void setSelectedValues(List<String> values) {
		Preconditions.checkNotNull(values);

		Set<String> permittedValues = new HashSet<>(getPermittedValues());

		// decide which values can be set now and which need to be deferred
		List<String> valuesToSetNow = new ArrayList<>();
		this.deferredValues = new ArrayList<>();

		for (String v : values) {
			if (permittedValues.contains(v)) {
				valuesToSetNow.add(v);
			} else {
				deferredValues.add(v);
				getControl().setEnabled(false);
			}
		}

		if (!valuesToSetNow.isEmpty()) {
			super.setSelectedValues(values);
		} else {
			super.setSelectedValues(Collections.emptyList());
		}
		if (deferredValues.isEmpty()) {
			clearDeferredValues();
		} else {
			if (DebugTracing.DATABINDING_DEBUG_TRACING) {
				System.out.println(
						"PvCheckboxList deferred values @ setSelectedValues: " + String.join(", ", deferredValues));
			}
		}
	}

	@Override
	public List<String> getSelectedValues() {
		if (deferredValues == null) {
			return super.getSelectedValues();
		} else {
			List<String> ret = new ArrayList<>(super.getSelectedValues());
			ret.addAll(deferredValues);
			return ret;
		}
	}

	@Override
	public void setPermittedValues(List<String> values) {
		super.setPermittedValues(values);
		if (DebugTracing.DATABINDING_DEBUG_TRACING) {
			System.out.println("PvCheckboxListDeferredBinding.setPermittedValues: " + String.join(", ", values));
		}

		if (deferredValues != null) {
			// If there are deferred values, we need to update the selection too
			List<String> currentPlusDeferred = new ArrayList<>(super.getSelectedValues());
			currentPlusDeferred.addAll(deferredValues);

			super.setSelectedValues(currentPlusDeferred);
			if (DebugTracing.DATABINDING_DEBUG_TRACING) {
				System.out.println("PvCheckboxListDeferredBinding.setPermittedValues changed selected values to: "
						+ String.join(", ", currentPlusDeferred));
			}
			
			deferredValues.removeAll(this.getPermittedValues());
			if (keepDeferredValuesForever && !deferredValues.isEmpty()) {
				if (DebugTracing.DATABINDING_DEBUG_TRACING) {
					System.out.println(
							"PvCheckboxListDeferredBinding.setPermittedValues: There are still some deferred values: "
									+ deferredValues);
				}
			} else {
				clearDeferredValues();
			}
		}
	}

	public void clearDeferredValues() {
		deferredValues = null;
		getControl().setEnabled(true);
	}

	@Override
	protected void setDirty() {
		if (deferredValues == null) {
			if (DebugTracing.DATABINDING_DEBUG_TRACING) {
				System.out.println("PvCheckboxListDeferredBinding.setDirty: deferredValues == null, permitted: "
						+ String.join(",", getPermittedValues()) + ", selected: "
						+ String.join(",", getSelectedValues()));
			}
			super.setDirty();
		}

		// prevent setting the dirty bit while there is a deferred value to be
		// treated
	}
}
