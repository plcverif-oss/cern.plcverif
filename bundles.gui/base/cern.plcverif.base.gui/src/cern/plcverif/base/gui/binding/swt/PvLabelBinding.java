/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.binding.swt;

import java.util.Optional;
import java.util.function.Function;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Label;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsKind;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.gui.binding.PvDataBinding;
import cern.plcverif.base.gui.binding.PvTextBinding;

/**
 * Data binding for an element representing a String
 * {@link cern.plcverif.base.common.settings.SettingsElement} by a label
 * ({@link Label}) SWT control (thus inherently read-only).
 */
public class PvLabelBinding extends PvTextBinding implements ISwtControlBinding {
	/**
	 * Represented label GUI control.
	 */
	private Label label;

	private String value = null;

	private Function<String, String> labelProvider = it -> it;
	private Function<String, Color> colorProvider;

	/**
	 * Creates a new data binding for a label.
	 * 
	 * @param label
	 *            The SWT label control to be bound
	 * @param settingsAttribute
	 *            Name of the settings element attribute to be represented
	 * @param parent
	 *            Parent data binding element
	 */
	public PvLabelBinding(Label label, String settingsAttribute, PvDataBinding parent) {
		super(settingsAttribute, parent);
		this.label = Preconditions.checkNotNull(label, "The represented SWT Label cannot be null.");

		// Create default color provider
		colorProvider = it -> label.getForeground();

		refresh();
	}

	/**
	 * Returns the value in the represented label.
	 * 
	 * @return Text in the label. May be {@code null} if it has not been loaded
	 *         yet.
	 */
	public String getValue() {
		return this.value;
	}

	/**
	 * Sets the value in the represented label.
	 * 
	 * @param value
	 *            New text for the label
	 */
	public void setValue(String value) {
		this.value = value;

		if (value == null) {
			label.setText("");
			label.setForeground(colorProvider.apply(""));
		} else {
			label.setText(labelProvider.apply(value));
			label.setForeground(colorProvider.apply(value));
		}
	}

	@Override
	protected boolean isThisValid() {
		return true;
	}

	@Override
	public Label getControl() {
		return label;
	}

	@Override
	protected void loadThis(Optional<? extends Settings> settings) throws SettingsParserException {
		if (settings.isPresent() && settings.get().kind() == SettingsKind.ELEMENT) {
			setValue(settings.get().toSingle().value());
		}
	}

	public void setLabelProvider(Function<String, String> labelProvider) {
		this.labelProvider = Preconditions.checkNotNull(labelProvider);
	}

	public void setColorProvider(Function<String, Color> colorProvider) {
		this.colorProvider = Preconditions.checkNotNull(colorProvider);
	}
}
