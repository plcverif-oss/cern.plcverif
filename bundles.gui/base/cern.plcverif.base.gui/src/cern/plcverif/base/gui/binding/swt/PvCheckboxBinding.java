/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.binding.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;

import com.google.common.base.Preconditions;

import cern.plcverif.base.gui.binding.PvBoolBinding;
import cern.plcverif.base.gui.binding.PvDataBinding;

/**
 * Data binding for an element representing a Boolean
 * {@link cern.plcverif.base.common.settings.SettingsElement} by a checkbox
 * ({@link Button}) SWT control.
 */
public final class PvCheckboxBinding extends PvBoolBinding implements ISwtControlBinding {
	/**
	 * Represented checkbox GUI control.
	 */
	private Button checkbox;

	/**
	 * Creates a new data binding for a checkbox.
	 * 
	 * @param checkbox
	 *            The SWT checkbox control to be bound
	 * @param settingsAttribute
	 *            Name of the settings element attribute to be represented
	 * @param parent
	 *            Parent data binding element
	 */
	public PvCheckboxBinding(Button checkbox, String settingsAttribute, PvDataBinding parent) {
		super(settingsAttribute, parent);
		this.checkbox = Preconditions.checkNotNull(checkbox, "The represented SWT Button cannot be null.");
		Preconditions.checkArgument((checkbox.getStyle() & (SWT.RADIO | SWT.CHECK)) != 0,
				"The represented SWT Button should be SWT.CHECK or SWT.RADIO.");

		// Register for modification event
		this.checkbox.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				setDirty();
				fireChangeListeners();
				refresh();
			}
		});

		refresh();
	}

	@Override
	protected void refresh() {
		super.refresh();
		// nothing for the moment
	}

	/**
	 * Returns the value in the represented checkbox.
	 * 
	 * @return Value of the checkbox
	 */
	public boolean getValue() {
		return checkbox.getSelection();
	}

	/**
	 * Sets the value in the represented checkbox.
	 * 
	 * @param value
	 *            New value for the checkbox
	 */
	public void setValue(boolean value) {
		checkbox.setSelection(value);
	}

	@Override
	protected boolean isThisValid() {
		return true;
	}

	@Override
	public Button getControl() {
		return checkbox;
	}
}
