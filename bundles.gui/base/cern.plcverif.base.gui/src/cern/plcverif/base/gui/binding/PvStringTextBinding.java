/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.binding;

import java.util.Optional;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsKind;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;

/**
 * Data binding for an element representing a String
 * {@link cern.plcverif.base.common.settings.SettingsElement} value without any
 * attached GUI element.
 */
public final class PvStringTextBinding extends PvTextBinding {
	private String value = null;
	private boolean allowLoading = true;

	/**
	 * Creates a new <b>read-only</b> data binding for a String
	 * {@link cern.plcverif.base.common.settings.SettingsElement} without any
	 * attached GUI control.
	 * 
	 * @param settingsAttribute
	 *            Name of the settings element attribute to be represented
	 * @param parent
	 *            Parent data binding element
	 */
	public PvStringTextBinding(String settingsAttribute, PvDataBinding parent) {
		super(settingsAttribute, parent);
	}

	/**
	 * Creates a new data binding for a String
	 * {@link cern.plcverif.base.common.settings.SettingsElement} without any
	 * attached GUI control. Depending on {@code allowLoading} the data binding
	 * may or may not be read-only.
	 * 
	 * @param settingsAttribute
	 *            Name of the settings element attribute to be represented
	 * @param parent
	 *            Parent data binding element
	 * @param allowLoading
	 *            If false, the value set cannot be modified at loading, making
	 *            the data binding read-only.
	 */
	public PvStringTextBinding(String settingsAttribute, PvDataBinding parent, boolean allowLoading) {
		super(settingsAttribute, parent);
		this.allowLoading = allowLoading;
	}

	@Override
	public String getValue() {
		return value;
	}

	@Override
	public void setValue(String value) {
		this.value = value;
		fireChangeListeners();
		this.setDirty();
	}

	@Override
	protected boolean isThisValid() {
		return true;
	}

	@Override
	protected void loadThis(Optional<? extends Settings> settings) throws SettingsParserException {
		if (allowLoading) {
			super.loadThis(settings);
		} else if (settings.isPresent() && settings.get().kind() == SettingsKind.ELEMENT) {
			// consume the value if identical to what is the current value
			if (value != null && value.equals(settings.get().toSingle().valueWithoutConsuming())) {
				settings.get().toSingle().value();
			}
		}
	}
}
