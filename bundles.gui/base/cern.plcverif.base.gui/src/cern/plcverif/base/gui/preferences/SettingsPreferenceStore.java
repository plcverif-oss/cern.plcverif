/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.preferences;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Optional;

import org.eclipse.core.commands.common.EventManager;
import org.eclipse.jface.preference.IPersistentPreferenceStore;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsSerializer;
import cern.plcverif.base.common.settings.SettingsUtils;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;

/**
 * Preference store that is backed by a PLCverif settings file. 
 */
public class SettingsPreferenceStore extends EventManager implements IPreferenceStore, IPersistentPreferenceStore {
	private Path settingsPath = null;
	private SettingsElement rootElement;
	private SettingsElement defaultRootElement;
	private boolean dirty = false;

	public SettingsPreferenceStore(SettingsElement rootElement, SettingsElement defaultRootElement) {
		this.rootElement = rootElement;
		this.defaultRootElement = defaultRootElement;
	}

	public SettingsPreferenceStore(Path settingsPath, SettingsElement defaultRootElement)
			throws SettingsParserException, IOException {
		this.settingsPath = Preconditions.checkNotNull(settingsPath);
		if (Files.exists(settingsPath)) {
			this.rootElement = SettingsSerializer.parseSettingsFromFile(Files.readAllLines(settingsPath));
		} else {
			this.rootElement = SettingsElement.empty();
		}
		this.defaultRootElement = defaultRootElement;
	}
	
	@Override
	public boolean needsSaving() {
		return this.dirty;
	}

	@Override
	public void save() throws IOException {
		this.dirty = false;

		if (settingsPath != null) {
			Files.write(settingsPath, Collections.singletonList(SettingsSerializer.serialize(rootElement)));
		}
	}
	
	public SettingsElement getSettingsRootElement() {
		return this.rootElement;
	}
	
	// Fetch values

	@Override
	public boolean contains(String name) {
		return SettingsUtils.tryGetNode(rootElement, name, false).isPresent();
	}


	@Override
	public boolean getBoolean(String name) {
		Optional<Settings> attributeNode = orElseOptional(getNode(name, false), getDefaultNode(name, false));
		return getBoolean(attributeNode);
	}

	@Override
	public boolean getDefaultBoolean(String name) {
		return getBoolean(getDefaultNode(name, false));
	}

	private static boolean getBoolean(Optional<Settings> attributeNode) {
		if (attributeNode.isPresent() && attributeNode.get() instanceof SettingsElement) {
			return Boolean.valueOf(((SettingsElement) attributeNode.get()).value());
		} else {
			return BOOLEAN_DEFAULT_DEFAULT;
		}
	}

	@Override
	public double getDouble(String name) {
		Optional<Settings> attributeNode = orElseOptional(getNode(name, false), getDefaultNode(name, false));
		return getDouble(attributeNode);
	}

	@Override
	public double getDefaultDouble(String name) {
		return getDouble(getDefaultNode(name, false));
	}

	private static double getDouble(Optional<Settings> attributeNode) {
		if (attributeNode.isPresent() && attributeNode.get() instanceof SettingsElement) {
			return Double.valueOf(((SettingsElement) attributeNode.get()).value());
		} else {
			return DOUBLE_DEFAULT_DEFAULT;
		}
	}

	@Override
	public float getFloat(String name) {
		Optional<Settings> attributeNode = orElseOptional(getNode(name, false), getDefaultNode(name, false));
		return getFloat(attributeNode);
	}

	@Override
	public float getDefaultFloat(String name) {
		return getFloat(getDefaultNode(name, false));
	}

	private static float getFloat(Optional<Settings> attributeNode) {
		if (attributeNode.isPresent() && attributeNode.get() instanceof SettingsElement) {
			return Float.valueOf(((SettingsElement) attributeNode.get()).value());
		} else {
			return FLOAT_DEFAULT_DEFAULT;
		}
	}

	@Override
	public int getInt(String name) {
		Optional<Settings> attributeNode = orElseOptional(getNode(name, false), getDefaultNode(name, false));
		return getInt(attributeNode);
	}

	@Override
	public int getDefaultInt(String name) {
		return getInt(getDefaultNode(name, false));
	}

	private static int getInt(Optional<Settings> attributeNode) {
		if (attributeNode.isPresent() && attributeNode.get() instanceof SettingsElement) {
			return Integer.valueOf(((SettingsElement) attributeNode.get()).value());
		} else {
			return INT_DEFAULT_DEFAULT;
		}
	}

	@Override
	public long getLong(String name) {
		Optional<Settings> attributeNode = orElseOptional(getNode(name, false), getDefaultNode(name, false));
		return getLong(attributeNode);
	}

	@Override
	public long getDefaultLong(String name) {
		return getLong(getDefaultNode(name, false));
	}

	private static long getLong(Optional<Settings> attributeNode) {
		if (attributeNode.isPresent() && attributeNode.get() instanceof SettingsElement) {
			return Long.valueOf(((SettingsElement) attributeNode.get()).value());
		} else {
			return LONG_DEFAULT_DEFAULT;
		}
	}

	@Override
	public String getString(String name) {
		Optional<Settings> attributeNode = orElseOptional(getNode(name, false), getDefaultNode(name, false));
		return getString(attributeNode);
	}

	@Override
	public String getDefaultString(String name) {
		return getString(getDefaultNode(name, false));
	}

	private static String getString(Optional<Settings> attributeNode) {
		if (attributeNode.isPresent() && attributeNode.get() instanceof SettingsElement) {
			return ((SettingsElement) attributeNode.get()).value();
		} else {
			return STRING_DEFAULT_DEFAULT;
		}
	}

	@Override
	public boolean isDefault(String name) {
		return getDefaultString(name).equals(getString(name));
	}

	// Set values
	
	@Override
	public void putValue(String name, String value) {
		firePropertyChangeEvent(name, getString(name), value);
		this.dirty = true;

		Optional<Settings> node = getNode(name, true);
		if (node.isPresent() && node.get() instanceof SettingsElement) {
			((SettingsElement) node.get()).setValue(value);
		} else {
			throw new UnsupportedOperationException("Impossible to get the settings node to be filled.");
		}
	}

	@Override
	public void setDefault(String name, double value) {
		// NOP
	}

	@Override
	public void setDefault(String name, float value) {
		// NOP
	}

	@Override
	public void setDefault(String name, int value) {
		// NOP
	}

	@Override
	public void setDefault(String name, long value) {
		// NOP
	}

	@Override
	public void setDefault(String name, String defaultObject) {
		// NOP
	}

	@Override
	public void setDefault(String name, boolean value) {
		// NOP
	}

	@Override
	public void setToDefault(String name) {
		// NOP
	}

	@Override
	public void setValue(String name, double value) {
		putValue(name, Double.toString(value));
	}

	@Override
	public void setValue(String name, float value) {
		putValue(name, Float.toString(value));

	}

	@Override
	public void setValue(String name, int value) {
		putValue(name, Integer.toString(value));
	}

	@Override
	public void setValue(String name, long value) {
		putValue(name, Long.toString(value));
	}

	@Override
	public void setValue(String name, String value) {
		putValue(name, value);
	}

	@Override
	public void setValue(String name, boolean value) {
		putValue(name, Boolean.toString(value));
	}
	
	
	// Listeners
	
	@Override
	public void addPropertyChangeListener(IPropertyChangeListener listener) {
		addListenerObject(listener);
	}

	@Override
	public void firePropertyChangeEvent(String name, Object oldValue, Object newValue) {
		if (oldValue == null || !oldValue.equals(newValue)) {
			PropertyChangeEvent event = new PropertyChangeEvent(this, name, oldValue, newValue);
			for (Object listener : getListeners()) {
				if (listener instanceof IPropertyChangeListener) {
					((IPropertyChangeListener) listener).propertyChange(event);
				}
			}
		}
	}

	@Override
	public void removePropertyChangeListener(IPropertyChangeListener listener) {
		removeListenerObject(listener);
	}

	
	// Helpers
	
	private static <T> Optional<T> orElseOptional(Optional<T> alt1, Optional<T> alt2) {
		if (alt1.isPresent()) {
			return alt1;
		} else {
			return alt2;
		}
	}
	
	public Optional<Settings> getNode(String fqn, boolean createIfNotExists) {
		return SettingsUtils.tryGetNode(this.rootElement, fqn, createIfNotExists);
	}

	public Optional<Settings> getDefaultNode(String fqn, boolean createIfNotExists) {
		return SettingsUtils.tryGetNode(this.defaultRootElement, fqn, createIfNotExists);
	}
}
