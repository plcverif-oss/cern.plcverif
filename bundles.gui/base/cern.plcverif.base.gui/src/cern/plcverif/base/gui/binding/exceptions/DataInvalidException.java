/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.binding.exceptions;

import cern.plcverif.base.gui.binding.PvDataBinding;

/**
 * Exception expressing that the data validation of a given the data binding has
 * been violated.
 */
public class DataInvalidException extends Exception {
	private static final long serialVersionUID = -8966576315770961803L;

	/**
	 * Origin data binding of the exception. {@code null} if not known.
	 */
	private final PvDataBinding origin;

	/**
	 * Constructor for a new data validation failure exception with the given
	 * message.
	 * 
	 * @param message
	 *            Detailed description
	 */
	public DataInvalidException(String message) {
		super(message);
		this.origin = null;
	}

	/**
	 * Constructor for a new data validation failure exception with the given
	 * message and origin.
	 * 
	 * @param message
	 *            Detailed description
	 * @param origin
	 *            Data binding causing the exception
	 */
	public DataInvalidException(String message, PvDataBinding origin) {
		super(message);
		this.origin = origin;
	}

	/**
	 * The originating data binding. May be {@code null}.
	 */
	public PvDataBinding getOrigin() {
		return origin;
	}
}
