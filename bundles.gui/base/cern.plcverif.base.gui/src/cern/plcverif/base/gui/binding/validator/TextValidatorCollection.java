/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.binding.validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.google.common.base.Preconditions;

/**
 * Class to validate text against a list of registered text validators.
 */
public class TextValidatorCollection {
	private static class ValidatorMessagePair {
		private ITextValidator validator;
		private String errorMessage;

		public ValidatorMessagePair(ITextValidator validator, String errorMessage) {
			this.validator = validator;
			this.errorMessage = errorMessage;
		}
	}

	/**
	 * Separator to be used between error messages if there are at least two.
	 */
	private static final String MESSAGE_SEPARATOR = System.lineSeparator();

	/**
	 * List of validators, together with the error messages to be shown in case
	 * of validation error.
	 */
	private List<ValidatorMessagePair> validators = null;

	/**
	 * Registers a new text validator.
	 * 
	 * @param validator
	 *            Validator to be registered
	 * @param errorMessage
	 *            Error message if the validator reports invalidity
	 */
	public void add(ITextValidator validator, String errorMessage) {
		Preconditions.checkNotNull(validator);
		Preconditions.checkNotNull(errorMessage);

		if (validators == null) {
			validators = new ArrayList<ValidatorMessagePair>();
		}

		validators.add(new ValidatorMessagePair(validator, errorMessage));
	}

	/**
	 * Validates the given text against the registered validators.
	 * 
	 * If no validator is registered to this, the result is always 'valid'.
	 * 
	 * @param textToValidate
	 *            Text to validate
	 * @return {@link Optional#empty()} if there is no validation error (i.e.,
	 *         the given text is valid). Otherwise the returned string contains
	 *         all error messages concatenated.
	 */
	public Optional<String> validate(String textToValidate) {
		if (validators == null) {
			return Optional.empty();
		}

		StringBuilder result = new StringBuilder();

		boolean valid = true;
		for (ValidatorMessagePair item : validators) {
			if (item.validator.validate(textToValidate) == false) {
				valid = false;
				if (result.length() > 0) {
					result.append(MESSAGE_SEPARATOR);
				}
				result.append(item.errorMessage);
			}
		}

		if (valid) {
			return Optional.empty();
		} else {
			return Optional.of(result.toString());
		}
	}
	
	/**
	 * Validates the given text against the registered validators.
	 * 
	 * If no validator is registered to this, the result is always 'valid' (i.e., true).
	 * 
	 * @param textToValidate
	 *            Text to validate
	 * @return True iff valid, i.e., if none of the validators returned with 'invalid'.
	 */
	public boolean isValid(String textToValidate) {
		if (validators == null) {
			return true;
		}
		
		for (ValidatorMessagePair item : validators) {
			if (item.validator.validate(textToValidate) == false) {
				return false;
			}
		}
		
		return true;
	}
}
