/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.component;

import java.util.ArrayList;
import java.util.Collection;

import com.google.common.base.Preconditions;

import cern.plcverif.base.gui.binding.PvDataBinding;

/**
 * Abstract superclass for {@link IPvGuiPart} implementations that implements
 * the enablement mechanism. <br>
 * Subclasses are required to register the created {@link PvDataBinding} objects
 * using the {@link #registerBindingElement(PvDataBinding)} method.
 */
public abstract class AbstractPvGuiPart<T extends PvGuiContext> implements IPvGuiPart<T> {
	private final Collection<PvDataBinding> registeredBindingElements = new ArrayList<>();
	private T context;

	public AbstractPvGuiPart(T context) {
		this.context = context;
	}

	public void registerBindingElement(PvDataBinding binding) {
		Preconditions.checkNotNull(binding);
		registeredBindingElements.add(binding);
	}

	/**
	 * Sets all data binding elements registered (using
	 * {@link #registerBindingElement(PvDataBinding)}) to enabled/disabled based
	 * on the given parameter.
	 * @param enabled Enablement value to be set for all registered binding elements.
	 */
	public void setEnabled(boolean enabled) {
		registeredBindingElements.forEach(it -> it.setEnabled(enabled));
	}

	@Override
	public T getContext() {
		return this.context;
	}
}
