/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.binding;

import java.util.Optional;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.gui.DebugTracing;
import cern.plcverif.base.gui.binding.validator.ITextValidator;
import cern.plcverif.base.gui.binding.validator.TextValidatorCollection;

/**
 * Data binding for an element representing a String {@link SettingsElement}.
 */
public abstract class PvTextBinding extends PvDataBinding {
	/**
	 * Text validators. May be {@code null} if there is none.
	 */
	private TextValidatorCollection validators = null;

	/**
	 * Creates a new data binding for a String {@link SettingsElement}.
	 * 
	 * @param settingsAttribute
	 *            Name of the settings element attribute to be represented
	 * @param parent
	 *            Parent data binding element
	 */
	public PvTextBinding(String settingsAttribute, PvDataBinding parent) {
		super(settingsAttribute, parent);
	}

	/**
	 * Returns the represented textual value. Shall never be {@code null}.
	 * 
	 * @return Represented textual value
	 */
	public abstract String getValue();

	/**
	 * Sets the represented textual value. May be {@code null}, in which case
	 * empty text will be set as value.
	 * 
	 * @param value
	 *            New represented textual value
	 */
	public abstract void setValue(String value);

	/**
	 * {@inheritDoc} If the given settings node is not a settings element, no
	 * operation is performed.
	 */
	@Override
	protected void loadThis(Optional<? extends Settings> settings) throws SettingsParserException {
		if (settings.isPresent() && settings.get() instanceof SettingsElement) {
			SettingsElement settingsElement = (SettingsElement) settings.get();
			setValue(settingsElement.value());
		} else {
			setValue(null);
		}
	}

	@Override
	protected Optional<Settings> saveThis() {
		if (getValue() == null) {
			return Optional.empty();
		} else {
			if (DebugTracing.DATABINDING_DEBUG_TRACING) {
				System.out.printf("PvTextBinding.saveThis(): attribute='%s' value='%s', enabled=%s%n",
						getSettingsAttribute(), getValue(), isEnabled());
			}
			return Optional.of(SettingsElement.createRootElement(getSettingsAttribute(), getValue()));
		}
	}

	/**
	 * Registers a new text validator for this data binding
	 * 
	 * @param validator
	 *            Validator to be registered
	 * @param errorMessage
	 *            Error message to be shown when the validation fails
	 */
	public void addValidator(ITextValidator validator, String errorMessage) {
		Preconditions.checkNotNull(validator);
		Preconditions.checkNotNull(errorMessage);

		if (this.validators == null) {
			this.validators = new TextValidatorCollection();
		}

		validators.add(validator, errorMessage);
		refresh();
	}

	@Override
	protected boolean isThisValid() {
		if (this.validators == null) {
			return true;
		} else {
			return this.validators.isValid(getValue());
		}
	}

	/**
	 * Returns the validation error message, if any.
	 * 
	 * @return Error message. Never {@code null}. It is {@link Optional#empty()}
	 *         if there is no validation error.
	 */
	protected Optional<String> getValidationErrorMessage() {
		if (this.validators == null) {
			return Optional.empty();
		} else {
			return this.validators.validate(getValue());
		}
	}
}
