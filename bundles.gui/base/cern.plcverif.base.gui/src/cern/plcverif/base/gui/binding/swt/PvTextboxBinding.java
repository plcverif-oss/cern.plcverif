/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.binding.swt;

import java.util.Optional;

import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Text;

import com.google.common.base.Preconditions;

import cern.plcverif.base.gui.binding.PvDataBinding;
import cern.plcverif.base.gui.binding.PvTextBinding;
import cern.plcverif.base.gui.binding.validator.ITextValidator;

/**
 * Data binding for an element representing a String
 * {@link cern.plcverif.base.common.settings.SettingsElement} by a textbox
 * ({@link Text}) SWT control.
 */
public class PvTextboxBinding extends PvTextBinding implements ISwtControlBinding {
	/**
	 * Represented textbox GUI control.
	 */
	private Text textbox;

	/**
	 * Control decoration representing invalidity of the value in the textbox.
	 */
	private ControlDecoration invalidityDecoration;

	/**
	 * Creates a new data binding for a textbox.
	 * 
	 * @param textbox
	 *            The SWT textbox control to be bound
	 * @param settingsAttribute
	 *            Name of the settings element attribute to be represented
	 * @param parent
	 *            Parent data binding element
	 */
	public PvTextboxBinding(Text textbox, String settingsAttribute, PvDataBinding parent) {
		super(settingsAttribute, parent);
		this.textbox = Preconditions.checkNotNull(textbox, "The represented SWT Text cannot be null.");

		// Register for modification event
		this.textbox.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				setDirty();
				fireChangeListeners();
				refresh();
			}
		});

		// Create decoration for invalidity
		invalidityDecoration = SwtUtil.createInvalidityControlDecoration(textbox, "The given value is invalid.");
		
		// no need for refreshInvalidityDecoration() yet
	}

	@Override
	protected void refresh() {
		super.refresh();
		refreshInvalidityDecoration();
	}
	
	private void refreshInvalidityDecoration() {
		if (isThisValid()) {
			invalidityDecoration.hide();
		} else {
			invalidityDecoration.setDescriptionText(getValidationErrorMessage().orElse("Unknown error."));
			invalidityDecoration.show();
		}
	}

	/**
	 * Returns the value in the represented textbox.
	 * 
	 * @return Text in the textbox
	 */
	public String getValue() {
		return textbox.getText();
	}

	/**
	 * Sets the value in the represented textbox.
	 * 
	 * @param value
	 *            New text for the textbox
	 */
	public void setValue(String value) {
		if (value == null) {
			textbox.setText("");
		} else {
			textbox.setText(value);
		}
	}

	@Override
	public Text getControl() {
		return textbox;
	}
	
	@Override
	public Optional<String> getDiagnosisMessage() {
		return Optional.ofNullable(invalidityDecoration.getDescriptionText());
	}
	
	
	/**
	 * Registers a new text validator for this data binding and returns this.
	 * 
	 * @param validator
	 *             Validator to be registered
	 * @param errorMessage
	 *            Error message to be shown when the validation fails
	 * @return This
	 */
	public PvTextboxBinding withValidator(ITextValidator validator, String errorMessage) {
		this.addValidator(validator, errorMessage);
		return this;
	}
}
