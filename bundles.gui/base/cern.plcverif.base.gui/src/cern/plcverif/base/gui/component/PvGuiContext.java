/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.component;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Preconditions;

/**
 * Abstract superclass for GUI contexts, to be used to connect various
 * composable GUI components.
 */
public abstract class PvGuiContext {

	@FunctionalInterface
	public static interface ContextUpdatedListener {
		/**
		 * Method that is invoked when a GUI context has been updated.
		 * @param updatedFieldId The ID of the field that was updated.
		 */
		void updated(String updatedFieldId);
	}

	private List<ContextUpdatedListener> listeners = new ArrayList<>();

	/**
	 * Adds a new listener to the "context updated" event.
	 * 
	 * @param listener
	 *            Listener to be added
	 * @throws NullPointerException
	 *             if the given listener is null
	 */
	public void addContextUpdatedListener(ContextUpdatedListener listener) {
		this.listeners.add(Preconditions.checkNotNull(listener));
	}

	protected void fireUpdated(String updatedField) {
		this.listeners.forEach(
				it -> it.updated(updatedField)
			);
	}
}
