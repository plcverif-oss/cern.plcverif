/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.binding.swt;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Combo;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.logging.PlatformLogger;
import cern.plcverif.base.gui.binding.IKeyLabelPair;
import cern.plcverif.base.gui.binding.PvDataBinding;
import cern.plcverif.base.gui.binding.PvEnumBinding;
import cern.plcverif.base.gui.binding.exceptions.DataInvalidException;
import cern.plcverif.base.gui.binding.validator.ITextValidator;

/**
 * Data binding for an element representing a string/enumeration
 * {@link cern.plcverif.base.common.settings.SettingsElement} by a combobox
 * ({@link Combo}) SWT control.
 */
public class PvComboboxBinding extends PvEnumBinding implements ISwtControlBinding {
	/**
	 * Represented combobox GUI control.
	 */
	private Combo combobox;

	/**
	 * Control decoration representing invalidity of the value in the combobox.
	 */
	private ControlDecoration invalidityDecoration;

	/**
	 * Validator function. Returns true iff the key selected in the represented
	 * combobox is valid.
	 */
	protected ITextValidator validator = it -> true;

	private boolean prohibitSetDirty = false;

	/**
	 * Creates a new data binding for a combobox without any item.
	 * 
	 * @param combobox
	 *            The SWT combobox control to be bound
	 * @param settingsAttribute
	 *            Name of the settings element attribute to be represented
	 * @param parent
	 *            Parent data binding element
	 */
	public PvComboboxBinding(Combo combobox, String settingsAttribute, PvDataBinding parent) {
		this(combobox, settingsAttribute, parent, Collections.emptyList());
	}

	/**
	 * Creates a new data binding for a combobox.
	 * 
	 * @param combobox
	 *            The SWT combobox control to be bound
	 * @param settingsAttribute
	 *            Name of the settings element attribute to be represented
	 * @param parent
	 *            Parent data binding element
	 * @param items
	 *            List of items that can be selected
	 */
	public PvComboboxBinding(Combo combobox, String settingsAttribute, PvDataBinding parent,
			List<? extends IKeyLabelPair> items) {
		super(settingsAttribute, parent, items);

		this.combobox = Preconditions.checkNotNull(combobox, "The represented SWT Combo cannot be null.");
		Preconditions.checkArgument((combobox.getStyle() & (SWT.READ_ONLY)) != 0,
				"The represented SWT Combo should be SWT.READ_ONLY.");

		initPermittedItems(items);

		// Register for modification event
		this.combobox.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				if (!prohibitSetDirty) {
					setDirty();
				}
				fireChangeListeners();
				refresh();
			}
		});

		// Disable scroll event for the combobox
		this.combobox.addListener(SWT.MouseVerticalWheel, event -> {
			event.doit = false;
		});

		// Create decoration for invalidity
		invalidityDecoration = SwtUtil.createInvalidityControlDecoration(combobox, "The selection is invalid.");
		if (items.isEmpty()) {
			invalidityDecoration.show();
		} else {
			invalidityDecoration.hide();
		}
	}

	@Override
	protected void refresh() {
		super.refresh();

		refreshInvalidityDecoration();
	}

	private void refreshInvalidityDecoration() {
		if (invalidityDecoration != null) {
			if (isThisValid()) {
				invalidityDecoration.hide();
			} else {
				invalidityDecoration.show();
			}
		}
	}

	/**
	 * Returns the key of the value in the represented combobox.
	 * 
	 * @return Key of the item selected in the combobox. Returns
	 *         {@link Optional#empty()} if no item is selected or if an item
	 *         with key {@code null} is selected.
	 */
	@Override
	public Optional<String> getSelectedItem() {
		int idx = combobox.getSelectionIndex();
		if (idx < 0) {
			return Optional.empty();
		} else {
			return Optional.ofNullable(permittedItems.get(idx).getKey());
		}
	}

	/**
	 * Modifies the selected item in the represented combobox.
	 * 
	 * @param key
	 *            Key of the new item to be selected for the combobox. It must
	 *            be one of the keys of the handled items. It can be
	 *            {@code null} if there is a permitted item with the key
	 *            {@code null}. (If the selected item's key is {@code null}, the
	 *            attribute won't be serialized.)
	 *            The key comparison is case insensitive.
	 * @throws DataInvalidException
	 *             if the given key is not known by this data binding element
	 */
	public void setSelectedItem(String key) throws DataInvalidException {
		for (int i = 0; i < permittedItems.size(); i++) {
			String candidateKey = permittedItems.get(i).getKey();
			if (equals(key, candidateKey)) {
				combobox.select(i);
				refresh();
				return;
			}
		}

		throw new DataInvalidException(String.format("The value '%s' is not acceptable for the setting '%s'.", key, fqn()), this);
	}

	private static boolean equals(String str1, String str2) {
		return (str1 == null && str2 == null) || (str1 != null && str1.equalsIgnoreCase(str2));
	}

	private void initPermittedItems(List<? extends IKeyLabelPair> items) {
		// It can be assumed that at this point there are no event handlers
		// registered yet.

		// Change binding's list of possible values
		super.setPermittedItemsInternal(items);

		// Refresh combobox contents
		this.combobox.removeAll();
		items.forEach(it -> this.combobox.add(it.getLabel()));

		combobox.select(0);
	}

	public void setPermittedItems(List<? extends IKeyLabelPair> items) {
		// Store current value
		Optional<String> prevSelection = getSelectedItem(); // may be empty

		// no need to set the dirty flag if we can restore the current value
		prohibitSetDirty = true;

		// Change binding's list of possible values
		super.setPermittedItemsInternal(items);

		// Refresh combobox contents
		this.combobox.removeAll();
		items.forEach(it -> this.combobox.add(it.getLabel()));

		// Restore previous selection if possible
		if (prevSelection.isPresent() && handledKeys.contains(prevSelection.get())) {
			try {
				setSelectedItem(prevSelection.get());
			} catch (DataInvalidException ex) {
				// Unexpected exception
				PlatformLogger.logWarning("Unexpected exception in PvComboboxBinding.", ex);
				// We have no better option than select the first item.
				prohibitSetDirty = false;
				combobox.select(0);
			}
			prohibitSetDirty = false;
		} else {
			// We have no better option than select the first item.
			prohibitSetDirty = false;
			combobox.select(0);
		}
	}

	@Override
	protected boolean isThisValid() {
		Optional<String> selectedItem = getSelectedItem();
		boolean result = combobox.getSelectionIndex() > -1 && 
				(validator.validate(selectedItem.orElse(null)));
		return result;
	}

	/**
	 * Sets the validator function for the represented data.
	 * 
	 * @param validator
	 *            Text validator object or function. Should return true iff the
	 *            string given as parameter is a valid key for the represented
	 *            data.
	 * @param errorMessage
	 *            Message to be shown if the validation fails.
	 */
	public void setValidator(ITextValidator validator, String errorMessage) {
		this.validator = Preconditions.checkNotNull(validator, "Null validator is invalid.");
		this.invalidityDecoration.setDescriptionText(errorMessage);
		refresh();
	}

	@Override
	public Combo getControl() {
		return combobox;
	}
}
