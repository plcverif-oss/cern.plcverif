/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.binding.validator;

import com.google.common.base.Preconditions;

/**
 * Functional interface for text validators.
 */
@FunctionalInterface
public interface ITextValidator {
	/**
	 * Common utility methods related to text validation.
	 */
	public static interface Utils {
		/**
		 * Returns true iff the given string is a non-negative integer or empty.
		 * 
		 * @param str
		 *            String to be validated. Shall not be {@code null}.
		 * @return True iff unsigned integer or empty.
		 */
		public static boolean isNonNegativeIntegerValidator(String str) {
			Preconditions.checkNotNull(str);
			return str.matches("\\d*");
		}

		/**
		 * Returns true iff the given string is a positive integer or empty.
		 * 
		 * @param str
		 *            String to be validated. Shall not be {@code null}.
		 * @return True iff positive integer or empty.
		 */
		public static boolean isPositiveIntegerValidator(String str) {
			Preconditions.checkNotNull(str);
			return !str.equals("0") && isNonNegativeIntegerValidator(str);
		}
	}
	
	/**
	 * Returns true iff the given text is valid.
	 * 
	 * @param text
	 *            Text to validate. May be {@code null}.
	 * @return True if the text is valid
	 */
	boolean validate(String text);
}
