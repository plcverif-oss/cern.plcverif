/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.binding;

import java.util.Optional;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;

/**
 * Data binding element, representing an empty {@link SettingsElement} without
 * any attached element. To be used for grouping other {@link PvDataBinding}
 * objects.
 */
public final class PvDataBindingGroup extends PvDataBinding {
	/**
	 * Creates a new data binding element, representing an empty
	 * {@link SettingsElement} without any attached element. To be used for
	 * grouping other {@link PvDataBinding} objects.
	 * 
	 * @param settingsAttribute
	 *            Name of the settings attribute to be represented
	 * @param parent
	 *            Parent data binding. Can be {@code null} if and only if this
	 *            is the root element.
	 */
	public PvDataBindingGroup(String settingsAttribute, PvDataBinding parent) {
		super(Preconditions.checkNotNull(settingsAttribute), Preconditions.checkNotNull(parent));
	}

	private PvDataBindingGroup() {
		super("", null);
	}

	public static PvDataBindingGroup createRoot() {
		return new PvDataBindingGroup();
	}

	@Override
	protected void loadThis(Optional<? extends Settings> settings) throws SettingsParserException {
		// Nothing to do for this node.
	}

	@Override
	protected Optional<Settings> saveThis() {
		SettingsElement ret = SettingsElement.createRootElement(this.getSettingsAttribute());
		return Optional.of(ret);
	}

	@Override
	protected boolean isThisValid() {
		return true;
	}
}
