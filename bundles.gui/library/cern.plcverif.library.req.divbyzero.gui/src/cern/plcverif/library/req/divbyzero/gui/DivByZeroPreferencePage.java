/*******************************************************************************
 * (C) Copyright CERN 2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jean-Charles Tournier - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.req.divbyzero.gui;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

public class DivByZeroPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	public DivByZeroPreferencePage() {
		super(GRID);
	}

	@Override
	public void init(IWorkbench workbench) {
		setDescription("No customization available");
	}

	@Override
	protected void createFieldEditors() {

	}

}
