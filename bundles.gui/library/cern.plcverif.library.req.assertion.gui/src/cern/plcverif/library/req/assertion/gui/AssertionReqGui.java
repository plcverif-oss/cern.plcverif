/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.req.assertion.gui;

import static cern.plcverif.base.gui.layout.PvLayoutUtils.createLabeledRow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import cern.plcverif.base.gui.binding.IKeyLabelPair;
import cern.plcverif.base.gui.binding.PvDataBinding;
import cern.plcverif.base.gui.binding.swt.PvCheckboxListDeferredBinding;
import cern.plcverif.base.gui.binding.swt.PvComboboxBinding;
import cern.plcverif.base.gui.component.AbstractPvGuiPart;
import cern.plcverif.base.gui.layout.PvLayoutUtils;
import cern.plcverif.base.gui.swt.CheckboxList;
import cern.plcverif.library.requirement.assertion.AssertRepresentationStrategy;
import cern.plcverif.library.requirement.assertion.AssertionRequirementSettings;
import cern.plcverif.verif.extensions.gui.context.VerificationCaseContext;

public class AssertionReqGui extends AbstractPvGuiPart<VerificationCaseContext> {
	private PvCheckboxListDeferredBinding bndAssToCheck = null;
	private static final String ALL_ASSERTIONS = "*";

	public AssertionReqGui(VerificationCaseContext context) {
		super(context);
	}

	public void createPart(Composite composite, PvDataBinding bindingParent) {
		PvLayoutUtils.checkExpectedLayout(composite);

		// Create GUI elements for "Assertions to check" row
		Label lblAssToCheck = new Label(composite, SWT.NONE);
		lblAssToCheck.setText("Assertions to check:");
		CheckboxList cblAssToCheck = new CheckboxList(composite);
		createLabeledRow(composite, lblAssToCheck, cblAssToCheck);
		PvLayoutUtils.applyHeightHint(cblAssToCheck.getUnderlyingSwtControl(), 100);
		
		// Advanced settings
		Composite advancedSettings = PvLayoutUtils.createAdvancedSettingsExpandableComposite(composite);
		
		// "Error field representation" GUI objects
		Label lblErrorField = new Label(advancedSettings, SWT.NONE);
		lblErrorField.setText("Error field repres.:");
		Combo cbErrorField = new Combo(advancedSettings, SWT.READ_ONLY);
		createLabeledRow(composite, lblErrorField, cbErrorField);
		
		synchronized (this) {
			// Create data binding for "Assertions to check" row
			bndAssToCheck = new PvCheckboxListDeferredBinding(cblAssToCheck,
					AssertionRequirementSettings.ASSERTION_TO_CHECK, bindingParent);
			bndAssToCheck.setValidator(binding -> !bndAssToCheck.getSelectedValues().isEmpty(),
					"At least one assertion needs to be selected.");
			bndAssToCheck.setKeepDeferredValuesForever(true);
			bndAssToCheck.setSelectedValues(Collections.singletonList(AssertionRequirementSettings.ALL_ASSERTIONS)); // to have a sensible default
			registerBindingElement(bndAssToCheck);

			// "Algorithm" data binding
			new PvComboboxBinding(cbErrorField, AssertionRequirementSettings.ASSERT_REPRESENTATION_STRATEGY,
					bindingParent,
					Arrays.asList(new IKeyLabelPair.Impl(null, "(default)"),
							new IKeyLabelPair.Impl(AssertRepresentationStrategy.INTEGER.name(), "Integer"),
							new IKeyLabelPair.Impl(AssertRepresentationStrategy.BOOLEAN.name(), "Boolean")));
		}

		subscribeForContextModification();
	}

	private void subscribeForContextModification() {
		getContext().addContextUpdatedListener(id -> {
			synchronized (this) {
				if (VerificationCaseContext.ASSERTIONS_ID.equals(id) && bndAssToCheck != null) {
					List<String> assertions = new ArrayList<>();
					assertions.add(ALL_ASSERTIONS);
					assertions.addAll(getContext().getAssertions());

					if (!(assertions.size() == 1 && bndAssToCheck.getPermittedValues().size() == 1
							&& bndAssToCheck.getPermittedValues().get(0).equals(ALL_ASSERTIONS))) {
						// avoid refreshing if the assertions are not available
						// --
						// this could cause the deferred value to be erased
						bndAssToCheck.setPermittedValues(assertions);
					} else {
						// no need to refresh, but make sure that it is enabled
						bndAssToCheck.getControl().setEnabled(true);
					}
				}
			}
		});
	}
}
