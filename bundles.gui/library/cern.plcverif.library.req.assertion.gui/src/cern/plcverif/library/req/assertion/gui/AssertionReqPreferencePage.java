/*******************************************************************************
 * (C) Copyright CERN 2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.req.assertion.gui;

import java.io.IOException;

import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.exceptions.SettingsSerializerException;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.gui.preferences.SettingsPreferencePageInitException;
import cern.plcverif.base.gui.preferences.SettingsPreferenceStoreFactory;
import cern.plcverif.library.requirement.assertion.AssertRepresentationStrategy;
import cern.plcverif.library.requirement.assertion.AssertionRequirementExtension;
import cern.plcverif.library.requirement.assertion.AssertionRequirementSettings;

public class AssertionReqPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	public AssertionReqPreferencePage() {
		super(GRID);
	}

	@Override
	public void init(IWorkbench workbench) {
		try {
			Settings defaultSettings = SpecificSettingsSerializer.toGenericSettings(new AssertionRequirementSettings());
			// As there is no default settings file included as resource, there
			// is no need for merging.

			Preconditions.checkState(defaultSettings instanceof SettingsElement,
					"Default settings does not have appropriate root element for assertion requirement.");
			setPreferenceStore(SettingsPreferenceStoreFactory.createStore(AssertionRequirementExtension.CMD_ID,
					defaultSettings.toSingle()));
		} catch (IOException | SettingsParserException | SettingsSerializerException e) {
			throw new SettingsPreferencePageInitException(e);
		}
	}

	@Override
	protected void createFieldEditors() {
		final Composite parent = getFieldEditorParent();

		addField(createStrategyFieldEditor(AssertionRequirementSettings.ASSERT_REPRESENTATION_STRATEGY,
				"Representation strategy: ", parent));
	}

	public static FieldEditor createStrategyFieldEditor(String name, String labelText, Composite parentComposite) {
		return new RadioGroupFieldEditor(name, labelText, 1,
				//@formatter:off
				new String[][] { 
						{ "Integer (larger state space)", AssertRepresentationStrategy.INTEGER.name() },
						{ "Boolean (cannot be determined which assertion is violated)", AssertRepresentationStrategy.BOOLEAN.name() }	
					},
				//@formatter:on
				parentComposite);
	}
}
