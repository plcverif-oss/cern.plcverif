/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.cbmc.gui;

import java.io.IOException;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.gui.preferences.SettingsPreferencePageInitException;
import cern.plcverif.base.gui.preferences.SettingsPreferenceStoreFactory;
import cern.plcverif.base.gui.preferences.SettingsPreferenceStoreUtil;
import cern.plcverif.library.backend.cbmc.CbmcBackendExtension;
import cern.plcverif.library.backend.cbmc.CbmcSettings;
import cern.plcverif.library.backend.cbmc.CbmcSettings.CbmcModelVariant;

public class CbmcBackendPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	public CbmcBackendPreferencePage() {
		super(GRID);
	}

	@Override
	public void init(IWorkbench workbench) {
		SettingsElement defaultSettings = SettingsPreferenceStoreUtil.mergePluginSettings(new CbmcSettings(),
				new CbmcBackendExtension().loadDefaultSettings());

		try {
			setPreferenceStore(
					SettingsPreferenceStoreFactory.createStore(CbmcBackendExtension.CMD_ID, defaultSettings));
		} catch (IOException | SettingsParserException e) {
			throw new SettingsPreferencePageInitException(e);
		}
	}

	@Override
	protected void createFieldEditors() {
		final Composite parent = getFieldEditorParent();

		//@formatter:off
		addField(new FileFieldEditor(CbmcSettings.BINARY_PATH,
				"Path of CBMC binary:", parent));
		addField(new FileFieldEditor(CbmcSettings.TIMEOUT_EXECUTOR_PATH,
				"Path of timeout executor (TimeoutExecutor.dll, only on Windows):", parent));
		addField(new FileFieldEditor(CbmcSettings.VS_INIT_BATCH,
				"Visual Studio initialization batch file (vcvars64.bat, only on Windows):", parent));
		addField(new IntegerFieldEditor(CbmcSettings.TIMEOUT,
				"Timeout (in sec):", parent));
		addField(new IntegerFieldEditor(CbmcSettings.UNWIND, "Loop unwind:", parent));
		addField(new RadioGroupFieldEditor(CbmcSettings.MODEL_VARIANT, "Model variant:", 1,
				new String[][] {
					{ "CFA declaration", CbmcModelVariant.CFD.name() }, 
					{ "CFA instance", CbmcModelVariant.CFI.name() },
					{ "Structured CFA declaration", CbmcModelVariant.CFD_STRUCTURED.name() }
				},
				parent));
		addField(new IntegerFieldEditor(CbmcSettings.VERBOSITY,
				"Output verbosity level (4..10, 10 is the most verbose):", parent));
		addField(new BooleanFieldEditor(CbmcSettings.PRINT_STDOUT_TO_FILE,
				"Write the console output of CBMC to a file (next to the generated C file)", parent));
		//@formatter:on
	}
}
