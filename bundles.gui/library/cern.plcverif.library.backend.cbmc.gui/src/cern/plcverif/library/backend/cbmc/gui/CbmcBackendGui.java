/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.cbmc.gui;

import static cern.plcverif.base.gui.layout.PvLayoutUtils.createLabeledRow;

import java.util.Arrays;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.ExpandableComposite;

import cern.plcverif.base.gui.binding.IKeyLabelPair;
import cern.plcverif.base.gui.binding.PvDataBinding;
import cern.plcverif.base.gui.binding.swt.PvComboboxBinding;
import cern.plcverif.base.gui.binding.swt.PvOptionalTextboxBinding;
import cern.plcverif.base.gui.binding.swt.PvTextboxBinding;
import cern.plcverif.base.gui.binding.validator.ITextValidator;
import cern.plcverif.base.gui.component.AbstractPvGuiPart;
import cern.plcverif.base.gui.layout.PvLayoutUtils;
import cern.plcverif.library.backend.cbmc.CbmcSettings;
import cern.plcverif.verif.extensions.gui.context.VerificationCaseContext;

public class CbmcBackendGui extends AbstractPvGuiPart<VerificationCaseContext> {
	public CbmcBackendGui(VerificationCaseContext context) {
		super(context);
	}

	public void createPart(Composite composite, PvDataBinding bindingParent) {
		PvLayoutUtils.checkExpectedLayout(composite);

		createModelVariantPart(composite, bindingParent);

		// Advanced part
		Composite compAdvancedClient = createAdvancedBox(composite);

		createUnwindPart(compAdvancedClient, bindingParent);
		createIncrementalPart(compAdvancedClient, bindingParent);
		createTimeoutPart(compAdvancedClient, bindingParent);
	}

	private Composite createAdvancedBox(Composite parentComposite) {
		ExpandableComposite compAdvanced = PvLayoutUtils.createExpandableComposite(parentComposite, "Advanced settings");
		PvLayoutUtils.createRow(parentComposite, compAdvanced, PvLayoutUtils.NUM_COLS, false);
		return (Composite) compAdvanced.getClient();
	}

	private void createModelVariantPart(Composite composite, PvDataBinding bindingParent) {
		PvDataBinding bndDomain = createEnumOptionRow("Model variant:", CbmcSettings.MODEL_VARIANT,
				Arrays.asList(
						new IKeyLabelPair.Impl(null, "(default)"),
						new IKeyLabelPair.Impl(CbmcSettings.CbmcModelVariant.CFD.name(), "CFA declaration"),
						new IKeyLabelPair.Impl(CbmcSettings.CbmcModelVariant.CFI.name(), "CFA instance"),
						new IKeyLabelPair.Impl(CbmcSettings.CbmcModelVariant.CFD_STRUCTURED.name(), "Structured CFA declaration")
				),
				composite, bindingParent);
		registerBindingElement(bndDomain);
	}

	private void createUnwindPart(Composite composite, PvDataBinding bindingParent) {
		// GUI objects
		Label label = new Label(composite, SWT.NONE);
		label.setText("Loop unwinding:");
		Text textbox = new Text(composite, SWT.BORDER);
		createLabeledRow(composite, label, textbox, 1, true);
		PvLayoutUtils.applySmallWidthHint(textbox);

		// Data binding
		PvTextboxBinding bndUnwinding = new PvOptionalTextboxBinding(textbox, CbmcSettings.UNWIND, bindingParent)
				.withValidator(ITextValidator.Utils::isPositiveIntegerValidator,
				"Only positive integers are accepted. Leave empty for default.");
		registerBindingElement(bndUnwinding);
	}
	
	private void createIncrementalPart(Composite composite, PvDataBinding bindingParent) {
		// "Forward search" GUI objects
		Label lblDf = new Label(composite, SWT.NONE);
		lblDf.setText("Unwinding variant:");
		Combo cbDf = new Combo(composite, SWT.READ_ONLY);
		createLabeledRow(composite, lblDf, cbDf, 2, true);
		PvLayoutUtils.applySmallWidthHint(cbDf);

		// "Forward search" data binding
		PvComboboxBinding bndDf = new PvComboboxBinding(cbDf, CbmcSettings.INCREMENTAL_BMC, bindingParent,
				Arrays.asList(
						new IKeyLabelPair.Impl(null, "(default)"),
						new IKeyLabelPair.Impl("true", "Incremental"),
						new IKeyLabelPair.Impl("false", "Fixed")));
		registerBindingElement(bndDf);
	}

	private void createTimeoutPart(Composite composite, PvDataBinding bindingParent) {
		// "Timeout" GUI objects
		Label lblTimeout = new Label(composite, SWT.NONE);
		lblTimeout.setText("Timeout (sec):");

		Text tbTimeout = new Text(composite, SWT.BORDER);
		tbTimeout.setText("");
		createLabeledRow(composite, lblTimeout, tbTimeout, 1, true);
		PvLayoutUtils.applySmallWidthHint(tbTimeout);

		// "Timeout" data binding
		PvTextboxBinding bndTimeout = new PvOptionalTextboxBinding(tbTimeout, CbmcSettings.TIMEOUT, bindingParent);
		bndTimeout.addValidator(ITextValidator.Utils::isNonNegativeIntegerValidator,
				"Numeric value is expected or leave empty for default.");
		registerBindingElement(bndTimeout);
	}

	private PvComboboxBinding createEnumOptionRow(String labelText, String attributeName,
			List<? extends IKeyLabelPair> options, Composite composite, PvDataBinding bindingParent) {
		// GUI objects
		Label label = new Label(composite, SWT.NONE);
		label.setText(labelText);
		Combo combo = new Combo(composite, SWT.READ_ONLY);
		createLabeledRow(composite, label, combo);

		// data binding
		PvComboboxBinding binding = new PvComboboxBinding(combo, attributeName, bindingParent, options);
		registerBindingElement(binding);

		return binding;
	}
}