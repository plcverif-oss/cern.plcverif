/*******************************************************************************
 * (C) Copyright CERN 2017-2021. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Mihaly Dobos-Kovacs - integer representation, array representation support
 *******************************************************************************/
package cern.plcverif.library.backend.theta.gui;

import static cern.plcverif.base.gui.layout.PvLayoutUtils.createLabeledRow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.logging.PlatformLogger;
import cern.plcverif.base.common.settings.EffectiveSettings;
import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsUtils;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.exceptions.SettingsSerializerException;
import cern.plcverif.base.gui.binding.IKeyLabelPair;
import cern.plcverif.base.gui.binding.PvDataBinding;
import cern.plcverif.base.gui.binding.swt.PvComboboxBinding;
import cern.plcverif.base.gui.binding.swt.PvOptionalTextboxBinding;
import cern.plcverif.base.gui.binding.swt.PvTextboxBinding;
import cern.plcverif.base.gui.binding.validator.ITextValidator;
import cern.plcverif.base.gui.component.AbstractPvGuiPart;
import cern.plcverif.base.gui.layout.PvLayoutUtils;
import cern.plcverif.library.backend.theta.ThetaBackendExtension;
import cern.plcverif.library.backend.theta.ThetaSettings;
import cern.plcverif.verif.extensions.gui.context.VerificationCaseContext;

public class ThetaBackendGui extends AbstractPvGuiPart<VerificationCaseContext> {
	// Bindings needed
	private PvComboboxBinding bndDomain;
	private PvComboboxBinding bndRefinement;
	private PvComboboxBinding bndIntRep;
	@SuppressWarnings("unused")
	private PvComboboxBinding bndArrRep;
	private PvComboboxBinding bndPredsplit;
	private PvComboboxBinding bndInitprec;
	private PvTextboxBinding bndMaxenum;
	
	private Optional<? extends SettingsElement> installationSettings;
	private SettingsElement defaultSettings = null;

	public ThetaBackendGui(VerificationCaseContext context, Optional<? extends SettingsElement> installationSettings) {
		super(context);
		this.installationSettings = Preconditions.checkNotNull(installationSettings);
	}

	public void createPart(Composite composite, PvDataBinding bindingParent) {
		PvLayoutUtils.checkExpectedLayout(composite);
		
		createIntRepPart(composite, bindingParent);
		createArrRepPart(composite, bindingParent);

		FormToolkit toolkit = new FormToolkit(composite.getDisplay());
		ExpandableComposite compAdvanced = toolkit.createExpandableComposite(composite,
				ExpandableComposite.TREE_NODE | ExpandableComposite.CLIENT_INDENT);
		PvLayoutUtils.createRow(composite, compAdvanced, PvLayoutUtils.NUM_COLS, false);
		compAdvanced.setText("Advanced settings");

		Composite compAdvancedClient = new Composite(compAdvanced, SWT.NONE);
		compAdvancedClient.setLayout(PvLayoutUtils.defaultNestedLayout());
		compAdvanced.setClient(compAdvancedClient);

		createDomainPart(compAdvancedClient, bindingParent);
		createRefinementPart(compAdvancedClient, bindingParent);
		createSearchPart(compAdvancedClient, bindingParent);
		createGranularityPart(compAdvancedClient, bindingParent);
		createPredSplitPart(compAdvancedClient, bindingParent);
		createEncodingPart(compAdvancedClient, bindingParent);
		createMaxenumPart(compAdvancedClient, bindingParent);
		createInitprecPart(compAdvancedClient, bindingParent);

		createDataConstraints();

		// ----

		createTimeoutPart(compAdvancedClient, bindingParent);
	}

	private void createDataConstraints() {
		// domain=EXPL and refinement=UNSAT_CORE are incompatible
		bndRefinement.setValidator(it -> {
			return it == null || !bndDomain.getSelectedItem().isPresent() || (!(it.equalsIgnoreCase("UNSAT_CORE") && !bndDomain.getSelectedItem().orElse("").equalsIgnoreCase("EXPL")));
		}, "UNSAT_CORE is only valid with EXPL domain.");
		bndDomain.addChangeListener(bnd -> bndRefinement.refreshAll());
		
		// bitvectors are only supported with Newton refinement
		bndIntRep.setValidator(it -> {
			return it == null || !it.equalsIgnoreCase("BITVECTOR") || bndRefinement.getSelectedItem().orElse("").toUpperCase().startsWith("NWT");
		}, "Bitvectors are only usable via Newton refinement");
		bndRefinement.addChangeListener(bnd -> bndIntRep.refreshAll());

		// domain=EXPL and initprec=ALLVARS are incompatible
		bndInitprec.setValidator(it -> {
			return it == null || !bndDomain.getSelectedItem().isPresent() || (!(it.equalsIgnoreCase("ALLVARS") && !bndDomain.getSelectedItem().orElse("").equalsIgnoreCase("EXPL")));
		}, "ALLVARS is only valid with EXPL domain.");
		bndDomain.addChangeListener(bnd -> bndInitprec.refreshAll());

		// predsplit is not relevant if domain=EXPL,
		// maxenum is not relevant if domain!=EXPL
		bndDomain.addChangeListener(bnd -> {
			boolean isExplSelected = bndDomain.getSelectedItem().isPresent() && bndDomain.getSelectedItem().orElse("").equalsIgnoreCase("EXPL");
			bndPredsplit.setEnabled(!bndDomain.getSelectedItem().isPresent() || !isExplSelected);
			bndPredsplit.getControl().setEnabled(!bndDomain.getSelectedItem().isPresent() || !isExplSelected);

			bndMaxenum.setEnabled(!bndDomain.getSelectedItem().isPresent() || isExplSelected);
			bndMaxenum.getControl().setEnabled(!bndDomain.getSelectedItem().isPresent() || isExplSelected);
		});
	}

	private void createIntRepPart(Composite composite, PvDataBinding bindingParent) {
		bndIntRep = createEnumOptionRow("Integer representation:", ThetaSettings.INTEGER_REPRESENTATION,
				Arrays.asList(new IKeyLabelPair.Impl("UNBOUNDED", "Unbounded integers"),
						new IKeyLabelPair.Impl("MODULO", "Bounded integers with modulo arithmetics"),
						new IKeyLabelPair.Impl("BITVECTOR", "Bounded integers with bitvectors")),
				composite, bindingParent);
	}

	private void createArrRepPart(Composite composite, PvDataBinding bindingParent) {
		bndArrRep = createEnumOptionRow("Array representation:", ThetaSettings.ARRAY_REPRESENTATION,
				Arrays.asList(new IKeyLabelPair.Impl("ENUMERATED", "The arrays are enumerated (dynamic indexing not supported)"),
						new IKeyLabelPair.Impl("DYNAMIC", "The arrays are left as is (dynamic indexing supported)")),
				composite, bindingParent);
	}

	private void createDomainPart(Composite composite, PvDataBinding bindingParent) {
		bndDomain = createEnumOptionRow("Abstract domain:", ThetaSettings.DOMAIN,
				Arrays.asList(new IKeyLabelPair.Impl("PRED_BOOL", "Boolean predicate abstraction (PRED_BOOL)"),
						new IKeyLabelPair.Impl("PRED_CART", "Cartesian predicate abstraction (PRED_CART)"),
						new IKeyLabelPair.Impl("PRED_SPLIT",
								"Boolean predicate abstraction with splitting disjunctions (PRED_SPLIT)"),
						new IKeyLabelPair.Impl("EXPL", "Explicit value abstraction (EXPL)")),
				composite, bindingParent);
	}

	private void createRefinementPart(Composite composite, PvDataBinding bindingParent) {
		bndRefinement = createEnumOptionRow("Refinement strategy:", ThetaSettings.REFINEMENT,
				Arrays.asList(new IKeyLabelPair.Impl("BW_BIN_ITP", "Backward binary interpolation (suggested) (BW_BIN_ITP)"),
						new IKeyLabelPair.Impl("FW_BIN_ITP", "Forward binary interpolation (FW_BIN_ITP)"),
						new IKeyLabelPair.Impl("SEQ_ITP", "Sequence interpolation (suggested) (SEQ_ITP)"),
						new IKeyLabelPair.Impl("UNSAT_CORE", "Unsat cores, only for EXPL (UNSAT_CORE)"),
						new IKeyLabelPair.Impl("NWT_IT_WP", "Newton - abstract trace, weakest (suggested) (NWT_IT_WP)"),
						new IKeyLabelPair.Impl("NWT_IT_SP", "Newton - abstract trace, strongest (NWT_IT_SP)"),
						new IKeyLabelPair.Impl("NWT_WP", "Newton - weakest (NWT_WP)"),
						new IKeyLabelPair.Impl("NWT_SP", "Newton - strongest (NWT_SP)"),
						new IKeyLabelPair.Impl("NWT_IT_WP_LV", "Newton - abstract trace, weakest, live variables (NWT_IT_WP_LV)"),
						new IKeyLabelPair.Impl("NWT_IT_SP_LV", "Newton - abstract trace, strongest, live variables (NWT_IT_SP_LV)"),
						new IKeyLabelPair.Impl("NWT_WP_LV", "Newton - weakest, live variables (NWT_WP_LV)"),
						new IKeyLabelPair.Impl("NWT_SP_LV", "Newton - strongest, live variables (NWT_SP_LV)")),
				composite, bindingParent);
	}

	private void createSearchPart(Composite composite, PvDataBinding bindingParent) {
		createEnumOptionRow("Search strategy:", ThetaSettings.SEARCH,
				Arrays.asList(new IKeyLabelPair.Impl("BFS", "Breadth-first search (BFS)"),
						new IKeyLabelPair.Impl("DFS", "Depth-first search (DFS)"),
						new IKeyLabelPair.Impl("ERR", "Error location distance-based search (ERR)")),
				composite, bindingParent);
	}

	private void createGranularityPart(Composite composite, PvDataBinding bindingParent) {
		createEnumOptionRow("Abs. prec. granularity:", ThetaSettings.PREC_GRANULARITY,
				Arrays.asList(new IKeyLabelPair.Impl("LOCAL", "Tracking separate precisions in each location (LOCAL)"),
						new IKeyLabelPair.Impl("GLOBAL", "Single, global precision (GLOBAL)")),
				composite, bindingParent);
	}

	private void createPredSplitPart(Composite composite, PvDataBinding bindingParent) {
		bndPredsplit = createEnumOptionRow("Predicate splitting:", ThetaSettings.PRED_SPLIT,
				Arrays.asList(new IKeyLabelPair.Impl("WHOLE", "Keep expressions as they are (WHOLE)"),
						new IKeyLabelPair.Impl("ATOMS", "Split into atoms (ATOMS)"),
						new IKeyLabelPair.Impl("CONJUNCTS", "Split into conjuncts (CONJUNCTS)")),
				composite, bindingParent);
	}

	private void createEncodingPart(Composite composite, PvDataBinding bindingParent) {
		createEnumOptionRow("CFA encoding:", ThetaSettings.CFA_ENCODING,
				Arrays.asList(new IKeyLabelPair.Impl("SBE", "Single block encoding (SBE)"),
						new IKeyLabelPair.Impl("LBE", "Large block encoding (LBE)")),
				composite, bindingParent);
	}

	private void createInitprecPart(Composite composite, PvDataBinding bindingParent) {
		bndInitprec = createEnumOptionRow("Init. precision:", ThetaSettings.INIT_PRECISION,
				Arrays.asList(new IKeyLabelPair.Impl("EMPTY", "Empty (EMPTY)"),
						new IKeyLabelPair.Impl("ALLVARS", "Track all variables from beginning (ALLVARS)")),
				composite, bindingParent);
	}

	private void createMaxenumPart(Composite composite, PvDataBinding bindingParent) {
		// GUI objects
		Label label = new Label(composite, SWT.NONE);
		label.setText("Max. succ. enum.:");
		Text textbox = new Text(composite, SWT.BORDER);
		createLabeledRow(composite, label, textbox, 1, true);

		// Data binding
		bndMaxenum = new PvOptionalTextboxBinding(textbox, ThetaSettings.MAX_ENUM, bindingParent);
		bndMaxenum.addValidator(ITextValidator.Utils::isNonNegativeIntegerValidator,
				"Positive integers and 0 (representing infinity) are accepted only. Leave blank for using the default settings.");
		registerBindingElement(bndMaxenum);
	}

	private void createTimeoutPart(Composite composite, PvDataBinding bindingParent) {
		// "Timeout" GUI objects
		Label lblTimeout = new Label(composite, SWT.NONE);
		lblTimeout.setText("Timeout (sec):");

		Text tbTimeout = new Text(composite, SWT.BORDER);
		tbTimeout.setText("");
		createLabeledRow(composite, lblTimeout, tbTimeout, 1, true);
		PvLayoutUtils.applySmallWidthHint(tbTimeout);
		
		Label lblPlaceholder = new Label(composite, SWT.NONE);
		Label lblTimeoutDefault = new Label(composite, SWT.NONE);
		lblTimeoutDefault.setText(String.format("If not set, the following timeout will be used: %s sec.", fetchDefaultTimeoutValue("N/A")));
		createLabeledRow(composite, lblPlaceholder, lblTimeoutDefault);

		// "Timeout" data binding
		PvTextboxBinding bndTimeout = new PvOptionalTextboxBinding(tbTimeout, ThetaSettings.TIMEOUT, bindingParent).withValidator(
				ITextValidator.Utils::isPositiveIntegerValidator,
				"Numeric value is expected or leave empty for default.");
		registerBindingElement(bndTimeout);
	}

	private PvComboboxBinding createEnumOptionRow(String labelText, String attributeName,
			List<? extends IKeyLabelPair> options, Composite composite, PvDataBinding bindingParent) {
		// GUI objects
		Label label = new Label(composite, SWT.NONE);
		label.setText(labelText);
		Combo combo = new Combo(composite, SWT.READ_ONLY);
		createLabeledRow(composite, label, combo);
		
		// Add a "default" option
		Optional<String> defSettingsKey = SettingsUtils.tryGetAttributeValue(fetchDefaultSettings(), attributeName);
		String defSettingsString = "unknown";
		if (defSettingsKey.isPresent()) {
			for (IKeyLabelPair option : options) {
				if (option.getKey().equals(defSettingsKey.get())) {
					defSettingsString = option.getLabel();
					break;
				}
			}
		}
		IKeyLabelPair.Impl defaultOption = new IKeyLabelPair.Impl(null, String.format("default (currently: %s)", defSettingsString));

		// data binding
		List<IKeyLabelPair> optionsWithDefault = new ArrayList<>();
		optionsWithDefault.add(defaultOption);
		optionsWithDefault.addAll(options);
		
		PvComboboxBinding binding = new PvComboboxBinding(combo, attributeName, bindingParent, optionsWithDefault);
		registerBindingElement(binding);

		return binding;
	}

	private SettingsElement fetchDefaultSettings() {
		if (defaultSettings == null) {
			try {
				SettingsElement resourceDefaults = new ThetaBackendExtension().loadDefaultSettings();
				this.defaultSettings = EffectiveSettings
						.pluginEffectiveSettings(new ThetaSettings(), resourceDefaults, installationSettings)
						.toSingle();
			} catch (SettingsParserException | SettingsSerializerException e) {
				PlatformLogger.logError("Unable to fetch the default settings for the Theta backend.", e);
			}
		}

		return this.defaultSettings;
	}

	private String fetchDefaultTimeoutValue(String orElse) {
		if (fetchDefaultSettings() == null) {
			return orElse;
		}
		
		Optional<Settings> timeoutNode = fetchDefaultSettings().getAttribute(ThetaSettings.TIMEOUT);
		if (timeoutNode.isPresent() && timeoutNode.get() instanceof SettingsElement) {
			return ((SettingsElement)timeoutNode.get() ).value();
		}
		return orElse;
	}
}