/*******************************************************************************
 * (C) Copyright CERN 2017-2021. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Mihaly Dobos-Kovacs - integer representation, array representation support
 *******************************************************************************/
package cern.plcverif.library.backend.theta.gui;

import java.io.IOException;

import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.gui.preferences.SettingsPreferencePageInitException;
import cern.plcverif.base.gui.preferences.SettingsPreferenceStoreFactory;
import cern.plcverif.base.gui.preferences.SettingsPreferenceStoreUtil;
import cern.plcverif.library.backend.theta.ThetaBackendExtension;
import cern.plcverif.library.backend.theta.ThetaSettings;

public class ThetaBackendPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	public ThetaBackendPreferencePage() {
		super(GRID);
	}

	@Override
	public void init(IWorkbench workbench) {
		SettingsElement defaultSettings = SettingsPreferenceStoreUtil.mergePluginSettings(new ThetaSettings(),
				new ThetaBackendExtension().loadDefaultSettings());
		try {
			setPreferenceStore(
					SettingsPreferenceStoreFactory.createStore(ThetaBackendExtension.CMD_ID, defaultSettings));
		} catch (IOException | SettingsParserException e) {
			throw new SettingsPreferencePageInitException(e);
		}
	}

	@Override
	protected void createFieldEditors() {
		final Composite parent = getFieldEditorParent();

		addField(new FileFieldEditor(ThetaSettings.BINARY_PATH, "Path of Theta binary (theta-cfa-cli.jar):", parent));
		addField(new DirectoryFieldEditor(ThetaSettings.LIBRARY_PATH, "Directory of Theta libraries:", parent));
		addField(new IntegerFieldEditor(ThetaSettings.TIMEOUT, "Timeout (in sec):", parent));

		addField(new StringFieldEditor(ThetaSettings.DOMAIN, "Abstract domain:", parent));
		addField(new StringFieldEditor(ThetaSettings.REFINEMENT, "Refinement strategy:", parent));
		addField(new StringFieldEditor(ThetaSettings.INTEGER_REPRESENTATION, "Integer representation:", parent));
		addField(new StringFieldEditor(ThetaSettings.ARRAY_REPRESENTATION, "Array representation:", parent));
		addField(new StringFieldEditor(ThetaSettings.SEARCH, "Search strategy:", parent));
		addField(new StringFieldEditor(ThetaSettings.PREC_GRANULARITY, "Abs. prec. granularity:", parent));
		addField(new StringFieldEditor(ThetaSettings.PRED_SPLIT, "Predicate splitting:", parent));
		addField(new StringFieldEditor(ThetaSettings.CFA_ENCODING, "CFA encoding:", parent));
		addField(new StringFieldEditor(ThetaSettings.MAX_ENUM, "Max. succ. enum.:", parent));
		addField(new StringFieldEditor(ThetaSettings.INIT_PRECISION, "Initial precision:", parent));
	}
}
