/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.req.pattern.gui;

import static cern.plcverif.base.gui.layout.PvLayoutUtils.createLabeledRow;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.xml.transform.TransformerException;

import org.eclipse.jface.resource.ColorRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.widgets.FormText;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.google.common.html.HtmlEscapers;

import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.logging.PlatformLogger;
import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsKind;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.base.gui.binding.IKeyLabelPair;
import cern.plcverif.base.gui.binding.PvDataBinding;
import cern.plcverif.base.gui.binding.PvElementListBinding;
import cern.plcverif.base.gui.binding.PvStringTextBinding;
import cern.plcverif.base.gui.binding.swt.PvComboboxBinding;
import cern.plcverif.base.gui.component.AbstractPvGuiPart;
import cern.plcverif.base.gui.layout.PvLayoutUtils;
import cern.plcverif.base.models.exprparser.ui.embedded.ExpressionEditor;
import cern.plcverif.base.models.exprparser.ui.embedded.PvExpressionBinding;
import cern.plcverif.library.requirement.pattern.PatternRequirementSettings;
import cern.plcverif.library.requirement.pattern.configuration.PatternRegistry;
import cern.plcverif.library.requirement.pattern.configuration.PatternSerialization;
import cern.plcverif.library.requirement.pattern.configuration.PatternSerialization.PatternLoadingException;
import cern.plcverif.library.requirement.pattern.configuration.PatternUtils;
import cern.plcverif.library.requirement.pattern.configuration.jaxb.Parameter;
import cern.plcverif.library.requirement.pattern.configuration.jaxb.Pattern;
import cern.plcverif.verif.extensions.gui.context.VerificationCaseContext;
import cern.plcverif.verif.job.gui.common.BrowserUtils;
import cern.plcverif.verif.job.gui.common.BrowserUtils.BrowserStyle;

public final class PatternReqGui extends AbstractPvGuiPart<VerificationCaseContext> {
	private static class ParameterRow {
		private int index;
		private Label label;
		private ExpressionEditor editor;
		private PvExpressionBinding binding;

		public ParameterRow(Composite parent, PvDataBinding bndParams, int index) {
			this.index = index;

			label = new Label(parent, SWT.NONE);
			label.setText(index + ":");
			editor = new ExpressionEditor(parent);
			createLabeledRow(parent, label, editor.getSwtControl());

			binding = new PvExpressionBinding(editor, Integer.toString(index), bndParams);
			// don't register, we'll take care of enabling them ourselves

			// by default disabled/invisible
			setEnabled(false);
		}

		/**
		 * Sets the visibility and enablement of the represented values.
		 * 
		 * @param enabled
		 *            If false, controls will be hidden and data binding
		 *            disabled.
		 */
		public final void setEnabled(boolean enabled) {
			label.setVisible(enabled);
			editor.getSwtControl().setVisible(enabled);
			binding.setEnabled(enabled);

			if (label.getLayoutData() instanceof GridData) {
				((GridData) label.getLayoutData()).exclude = !enabled;
			}
			if (editor.getSwtControl().getLayoutData() instanceof GridData) {
				((GridData) editor.getSwtControl().getLayoutData()).exclude = !enabled;
			}

			label.requestLayout();
			editor.getSwtControl().requestLayout();
		}

		/**
		 * Sets the label and tooltip texts attached to this parameter row.
		 * 
		 * @param labelText
		 * @param tooltipText
		 */
		public void setLabel(String labelText, String tooltipText) {
			label.setText(labelText);
			label.setToolTipText(tooltipText);
		}

		public int getIndex() {
			return index;
		}

		public String getExprEditorText() {
			return editor.getText();
		}
	}

	private static final String REQ_PREVIEW_BGCOLOR_KEY = "REQ_PREVIEW_BGCOLOR";
	private static final RGB REQ_PREVIEW_BGCOLOR = new RGB(230, 230, 230);
	
	/**
	 * Registry of available patterns.
	 */
	private PatternRegistry patterns;

	private List<ParameterRow> parameters = new ArrayList<>();

	private Composite composite;
	private FormText reqPreview;
	private PvComboboxBinding bndPattern;

	public PatternReqGui(VerificationCaseContext context, Optional<? extends SettingsElement> installationSettings) {
		super(context);
		
		// Load patterns
		String patternFilePath = "N/A";
		try {
			if (installationSettings.isPresent()) {
				Optional<Settings> patternFileAttribute = installationSettings.get().getAttribute(PatternRequirementSettings.PATTERN_FILE);
				if (patternFileAttribute.isPresent() && patternFileAttribute.get().kind() == SettingsKind.ELEMENT) {
					patternFilePath = patternFileAttribute.get().toSingle().value();
					if (patternFilePath != null && !patternFilePath.isEmpty()) {
						Path path = Paths.get(patternFilePath);
						this.patterns = PatternRegistry.create(IoUtils.readFile(path));
						
					}
				}
			} 
			
			if (this.patterns == null) {
				// If it was unsuccessful to get the pattern file location from the settings
				this.patterns = PatternRegistry.getDefaultPatternRegistry();
			}
		} catch (IOException e) {
			throw new PlcverifPlatformException(String.format("Unable to load the selected patterns definition file (%s).", patternFilePath), e);
		} catch (PatternLoadingException e) {
			throw new PlcverifPlatformException("Unable to load the default patterns definitions.", e);
		} catch (SettingsParserException e) {
			throw new PlcverifPlatformException("Unable to load the installation settings to determine the pattern definition file path.", e);
		}
	}

	public void createPart(Composite composite, PvDataBinding bindingParent) {
		PvLayoutUtils.checkExpectedLayout(composite);

		this.composite = composite;
		FormToolkit toolkit = new FormToolkit(composite.getDisplay());

		// Create GUI elements for "Pattern" row
		Label lblPattern = new Label(composite, SWT.NONE);
		lblPattern.setText("Pattern:");
		Combo cbPattern = new Combo(composite, SWT.READ_ONLY);
		createLabeledRow(composite, lblPattern, cbPattern, PvLayoutUtils.NUM_COLS - 2, false);
		PvLayoutUtils.applyMediumWidthHint(cbPattern);
		
		Button btnHelp = new Button(composite, SWT.PUSH);
		btnHelp.setText("?");
		btnHelp.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				super.widgetSelected(e);
				onClickHelpButton();
			}
		});
		
		// Create data binding for "Pattern" row
		List<IKeyLabelPair> patternList = new ArrayList<>();
		for (Pattern p : patterns.getAllPatterns()) {
			patternList.add(new IKeyLabelPair.Impl(p.getId(), p.getHumanreadable()));
		}

		bndPattern = new PvComboboxBinding(cbPattern, PatternRequirementSettings.PATTERN_ID,
				bindingParent, patternList);
		registerBindingElement(bndPattern);
		bndPattern.addChangeListener(e -> onChangeSelectedPattern(patterns.getPattern(bndPattern.getSelectedItem())));

		// Prepare for the maximum pattern number with pattern rows
		PvElementListBinding bndParams = new PvElementListBinding(PatternRequirementSettings.PATTERN_PARAMS,
				bindingParent);
		registerBindingElement(bndParams);

		// to make indexing one-based for pattern params:
		PvStringTextBinding bndParam0 = new PvStringTextBinding("0", bndParams, false);
		bndParam0.setValue(null);
		registerBindingElement(bndParam0);

		// create pattern rows
		for (int i = 1; i <= patterns.getAllPatterns().stream().mapToInt(it -> it.getParameter().size()).max()
				.getAsInt(); i++) {
			parameters.add(new ParameterRow(composite, bndParams, i));
		}

		// Human-readable pattern representation (preview)
		reqPreview = toolkit.createFormText(composite, false);
		reqPreview.setLayoutData(PvLayoutUtils.fullRowLayout(80));
		PvLayoutUtils.applyMediumWidthHint(reqPreview);
		PvLayoutUtils.applyHeightHint(reqPreview, 40);
		parameters.forEach(p -> p.binding.addChangeListener(e -> {
			Optional<Pattern> selectedPattern = patterns.getPattern(bndPattern.getSelectedItem());
			updateReqPreview(selectedPattern);
		}));
		reqPreview.setBackground(getColor(REQ_PREVIEW_BGCOLOR_KEY, REQ_PREVIEW_BGCOLOR));

		// When the list of variables change in context, update the content
		// assists
		getContext().addContextUpdatedListener(changedId -> {
			if (VerificationCaseContext.VARIABLES_ID.equals(changedId)) {
				parameters.forEach(p -> p.editor.setVariablesForContentAssist(getContext().getVariables()));
			}
		});
		
		// Initialize the visible pattern list
		onChangeSelectedPattern(patterns.getPattern(bndPattern.getSelectedItem()));
		
		composite.layout();
	}
	
	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		onChangeSelectedPattern(patterns.getPattern(bndPattern.getSelectedItem()));
	}

	private void onChangeSelectedPattern(Optional<Pattern> selectedPattern) {
		if (selectedPattern.isPresent()) {
			// Show/hide the appropriate number of parameter rows
			for (int i = 0; i < parameters.size(); i++) {
				if (i < selectedPattern.get().getParameter().size()) {
					Parameter representedParameter = selectedPattern.get().getParameter().get(i);
					parameters.get(i).setEnabled(true);
					parameters.get(i).setLabel("      " + representedParameter.getKey() + ":",
							representedParameter.getDescription());
				} else {
					parameters.get(i).setEnabled(false);
				}
			}

			updateReqPreview(selectedPattern);
		} else {
			parameters.forEach(it -> it.setEnabled(false));
		}
		composite.requestLayout();
	}
	
	private void onClickHelpButton() {
		try {
			String htmlContent = PatternSerialization.asHtml(patterns);
			Path target = Files.createTempFile("patterns", ".html");
			IoUtils.writeAllContent(target, htmlContent);
			
			String anchor = "";
			if (bndPattern != null) {
				anchor = "#" + bndPattern.getSelectedItem().orElse("");
			}
			
			String urlToOpen = target.toAbsolutePath().toUri().toString() + anchor;
			BrowserUtils.tryOpenUrlInBrowser(urlToOpen, BrowserStyle.NO_BUTTONS_NO_LOCATION);
		} catch (IOException | TransformerException ex) {
			PlatformLogger.logError("Exception when trying to create the pattern requirement help file.");
			// Silent failure
		}
	}

	private void updateReqPreview(Optional<Pattern> selectedPattern) {
		if (selectedPattern.isPresent()) {
			Map<String, String> formattedParams = getCurrentParamValues();
			for (String key : new ArrayList<>(formattedParams.keySet())) {
				formattedParams.put(key,
						String.format("<b>%s</b>", HtmlEscapers.htmlEscaper().escape(formattedParams.get(key))));
			}

			String content = String.format("<form><p>%s</p></form>",
					PatternUtils.fill(selectedPattern.get().getHumanreadable(), formattedParams));
			try {
				reqPreview.setText(content, true, false);
			} catch (IllegalArgumentException e) {
				reqPreview.setText(e.getMessage(), false, false);
			}
		} else {
			reqPreview.setText("", false, false);
		}
	}

	private Map<String, String> getCurrentParamValues() {
		return parameters.stream()
				.collect(Collectors.toMap(p -> Integer.toString(p.getIndex()), p -> p.getExprEditorText()));
	}

	private static Color getColor(String key, RGB defaultValue) {
		ColorRegistry colorRegistry = JFaceResources.getColorRegistry();
		if (!colorRegistry.hasValueFor(key)) {
			colorRegistry.put(key, defaultValue);
		}
		return colorRegistry.get(key);
	}
}