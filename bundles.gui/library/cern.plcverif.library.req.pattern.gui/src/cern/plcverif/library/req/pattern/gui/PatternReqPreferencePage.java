/*******************************************************************************
 * (C) Copyright CERN 2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.req.pattern.gui;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.exceptions.SettingsSerializerException;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.base.gui.preferences.SettingsPreferencePageInitException;
import cern.plcverif.base.gui.preferences.SettingsPreferenceStoreFactory;
import cern.plcverif.library.requirement.pattern.PatternRequirementExtension;
import cern.plcverif.library.requirement.pattern.PatternRequirementSettings;
import cern.plcverif.library.requirement.pattern.configuration.PatternRegistry;
import cern.plcverif.library.requirement.pattern.configuration.PatternSerialization.PatternLoadingException;
import cern.plcverif.library.requirement.pattern.configuration.PatternUtils;

public class PatternReqPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	public PatternReqPreferencePage() {
		super(GRID);
	}

	@Override
	public void init(IWorkbench workbench) {
		try {
			Settings defaultSettings = SpecificSettingsSerializer.toGenericSettings(new PatternRequirementSettings(),
					Optional.empty(), false);
			// As there is no default settings file included as resource, there
			// is no need for merging.

			Preconditions.checkState(defaultSettings instanceof SettingsElement,
					"Default settings does not have appropriate root element for assertion requirement.");
			setPreferenceStore(SettingsPreferenceStoreFactory.createStore(PatternRequirementExtension.CMD_ID,
					defaultSettings.toSingle()));
		} catch (IOException | SettingsParserException | SettingsSerializerException e) {
			throw new SettingsPreferencePageInitException(e);
		}
	}

	@Override
	protected void createFieldEditors() {
		final Composite parent = getFieldEditorParent();

		createPatternFileEditors(parent);
	}

	private void createPatternFileEditors(Composite parent) {
		FileFieldEditor fileField = new PatternFileFieldEditor(PatternRequirementSettings.PATTERN_FILE,
				"Pattern definition XML file:", true, parent);
		addField(fileField);

		Button cbDefaultPatterns = new Button(parent, SWT.CHECK);
		cbDefaultPatterns.setText("Use the default (built-in) requirement patterns");
		GridDataFactory.fillDefaults().span(2, 1).applyTo(cbDefaultPatterns);
		cbDefaultPatterns.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				super.widgetSelected(e);
				if (cbDefaultPatterns.getSelection()) {
					fileField.setStringValue(null);
					fileField.setEnabled(false, parent);
				} else {
					fileField.setEnabled(true, parent);
				}
			}
		});

		// Workaround to gray out the file field in case empty value is loaded initially
		fileField.getTextControl(parent).addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				if (fileField.getStringValue() == null || fileField.getStringValue().isEmpty()) {
					cbDefaultPatterns.setSelection(true);
					fileField.setEnabled(false, parent);
				}
			}
		});
	}

	/**
	 * Editor specifically for pattern definition files. It extends the
	 * {@link FileFieldEditor} with a <I>Test</I> button that can be used to
	 * check the validity of the given pattern definition file. The user is
	 * notified in a message box about the result of the check.
	 */
	private static class PatternFileFieldEditor extends FileFieldEditor {
		private Button testButton;
		private Label fillerLabel;

		public PatternFileFieldEditor(String name, String labelText, boolean enforceAbsolute, Composite parent) {
			super(name, labelText, enforceAbsolute, parent);
			super.setEmptyStringAllowed(true);
		}

		@Override
		protected void doFillIntoGrid(Composite parent, int numColumns) {
			super.doFillIntoGrid(parent, numColumns);
			createTestButtonIfNeeded(parent);

			GridDataFactory.fillDefaults().span(numColumns - 1, 1).applyTo(fillerLabel);
			GridDataFactory.fillDefaults().applyTo(testButton);
		}

		private void createTestButtonIfNeeded(Composite parent) {
			if (testButton == null) {
				fillerLabel = new Label(parent, SWT.NONE);
				testButton = new Button(parent, SWT.PUSH);
				testButton.setText("Test loading");

				testButton.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						super.widgetSelected(e);

						try {
							String xml = IoUtils.readFile(Paths.get(getTextControl(parent).getText()));
							PatternRegistry registry = PatternRegistry.create(xml);
							registry.validate();
							MessageDialog.openInformation(parent.getShell(), "Patterns successfully loaded",
									String.format("The following %s patterns have been successfully loaded: %s.",
											registry.getAllPatterns().size(),
											String.join(", ", registry.getAllPatterns().stream().map(it -> it.getId())
													.collect(Collectors.toList()))));
						} catch (IOException ex) {
							MessageDialog.openError(parent.getShell(), "Pattern loading error",
									"Unable to load the given XML file. " + ex.getMessage());
						} catch (PatternLoadingException ex1) {
							MessageDialog.openError(parent.getShell(), "Pattern loading error",
									"Unable to parse the given XML file. " + ex1.getMessage());
						} catch (PatternUtils.PatternValidationException ex2) {
							MessageDialog.openError(parent.getShell(), "Pattern validation error",
									"Incorrect pattern in the loaded XML file. " + ex2.getMessage());
						}
					}
				});
			}
		}

		@Override
		public void setEnabled(boolean enabled, Composite parent) {
			super.setEnabled(enabled, parent);
			if (testButton != null) {
				testButton.setEnabled(enabled);
			}
		}
	}
}
