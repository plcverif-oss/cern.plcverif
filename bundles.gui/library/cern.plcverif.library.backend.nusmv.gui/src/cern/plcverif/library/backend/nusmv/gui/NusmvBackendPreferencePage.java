/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.nusmv.gui;

import java.io.IOException;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.gui.preferences.SettingsPreferencePageInitException;
import cern.plcverif.base.gui.preferences.SettingsPreferenceStoreFactory;
import cern.plcverif.base.gui.preferences.SettingsPreferenceStoreUtil;
import cern.plcverif.library.backend.nusmv.NusmvBackendExtension;
import cern.plcverif.library.backend.nusmv.NusmvSettings;
import cern.plcverif.library.backend.nusmv.NusmvSettings.NusmvAlgorithm;

public class NusmvBackendPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	public NusmvBackendPreferencePage() {
		super(GRID);
	}

	@Override
	public void init(IWorkbench workbench) {
		SettingsElement defaultSettings = SettingsPreferenceStoreUtil.mergePluginSettings(new NusmvSettings(),
				new NusmvBackendExtension().loadDefaultSettings());
		try {
			setPreferenceStore(
					SettingsPreferenceStoreFactory.createStore(NusmvBackendExtension.CMD_ID, defaultSettings));
		} catch (IOException | SettingsParserException e) {
			throw new SettingsPreferencePageInitException(e);
		}
	}

	@Override
	protected void createFieldEditors() {
		final Composite parent = getFieldEditorParent();

		addField(new FileFieldEditor(NusmvSettings.BINARY_PATH, "Path of NuSMV/nuXmv binary:", parent));
		addField(new IntegerFieldEditor(NusmvSettings.TIMEOUT, "Timeout (in sec):", parent));
		addField(new RadioGroupFieldEditor(NusmvSettings.ALGORITHM, "Verification algorithm:", 1,
				new String[][] { { "Classic", NusmvAlgorithm.Classic.name() }, { "IC3", NusmvAlgorithm.Ic3.name() }, { "BMC", NusmvAlgorithm.Bmc.name() } },
				parent));
		addField(new BooleanFieldEditor(NusmvSettings.DF, "Disable forward search (-df)", parent));
		addField(new BooleanFieldEditor(NusmvSettings.DYNAMIC, "Allow dynamic variable reordering (-dynamic)", parent));
		addField(new BooleanFieldEditor(NusmvSettings.REQ_AS_INVAR, "Represent requirements as invariants, if possible",
				parent));
	}
}
