/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.nusmv.gui;

import static cern.plcverif.base.gui.layout.PvLayoutUtils.createLabeledRow;

import java.util.Arrays;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import cern.plcverif.base.gui.binding.IKeyLabelPair;
import cern.plcverif.base.gui.binding.PvDataBinding;
import cern.plcverif.base.gui.binding.swt.PvComboboxBinding;
import cern.plcverif.base.gui.binding.swt.PvOptionalTextboxBinding;
import cern.plcverif.base.gui.binding.swt.PvTextboxBinding;
import cern.plcverif.base.gui.binding.validator.ITextValidator;
import cern.plcverif.base.gui.component.AbstractPvGuiPart;
import cern.plcverif.base.gui.layout.PvLayoutUtils;
import cern.plcverif.library.backend.nusmv.NusmvSettings;
import cern.plcverif.verif.extensions.gui.context.VerificationCaseContext;

/**
 * GUI part to represent settings related to the NuSMV backend
 * ({@code job.backend=nusmv}) on the verification job description form.
 */
public final class NusmvBackendGui extends AbstractPvGuiPart<VerificationCaseContext> {
	public NusmvBackendGui(VerificationCaseContext context) {
		super(context);
	}

	@Override
	public void createPart(Composite composite, PvDataBinding bindingParent) {
		PvLayoutUtils.checkExpectedLayout(composite);

		createAlgorithmPart(composite, bindingParent);

		Composite compAdvancedClient = PvLayoutUtils.createAdvancedSettingsExpandableComposite(composite);

		// Checkboxes for dynamic, df
		createDynamicPart(compAdvancedClient, bindingParent);
		createFowardSearchPart(compAdvancedClient, bindingParent);
		
		// Timeout textbox
		createTimeoutPart(compAdvancedClient, bindingParent);
	}

	private void createAlgorithmPart(Composite composite, PvDataBinding bindingParent) {
		// "Algorithm" GUI objects
		Label lblAlgo = new Label(composite, SWT.NONE);
		lblAlgo.setText("Algorithm:");
		Combo cbAlgo = new Combo(composite, SWT.READ_ONLY);
		createLabeledRow(composite, lblAlgo, cbAlgo);

		// "Algorithm" data binding
		PvComboboxBinding bndAlgo = new PvComboboxBinding(cbAlgo, NusmvSettings.ALGORITHM, bindingParent,
				Arrays.asList(
						new IKeyLabelPair.Impl(null, "(default)"),
						new IKeyLabelPair.Impl(NusmvSettings.NusmvAlgorithm.Classic.name(), "Classic (NuSMV/nuXmv)"),
						new IKeyLabelPair.Impl(NusmvSettings.NusmvAlgorithm.Ic3.name(), "IC3 (nuXmv only)"),
						new IKeyLabelPair.Impl(NusmvSettings.NusmvAlgorithm.Bmc.name(), "BMC (NuSMV/nuXmv)")));
		registerBindingElement(bndAlgo);
	}
	
	private void createDynamicPart(Composite composite, PvDataBinding bindingParent) {
		// "Dynamic" GUI objects
		Label lblDynamic = new Label(composite, SWT.NONE);
		lblDynamic.setText("Variable reordering:");
		Combo cbDynamic = new Combo(composite, SWT.READ_ONLY);
		createLabeledRow(composite, lblDynamic, cbDynamic, 2, true);
		PvLayoutUtils.applySmallWidthHint(cbDynamic);

		// "Dynamic" data binding
		PvComboboxBinding bndDynamic = new PvComboboxBinding(cbDynamic, NusmvSettings.DYNAMIC, bindingParent,
				Arrays.asList(
						new IKeyLabelPair.Impl(null, "(default)"),
						new IKeyLabelPair.Impl("true", "Dynamic variable reordering (-dynamic)"),
						new IKeyLabelPair.Impl("false", "No variable reordering")));
		registerBindingElement(bndDynamic);
	}
	
	private void createFowardSearchPart(Composite composite, PvDataBinding bindingParent) {
		// "Forward search" GUI objects
		Label lblDf = new Label(composite, SWT.NONE);
		lblDf.setText("Forward search:");
		Combo cbDf = new Combo(composite, SWT.READ_ONLY);
		createLabeledRow(composite, lblDf, cbDf, 2, true);
		PvLayoutUtils.applySmallWidthHint(cbDf);

		// "Forward search" data binding
		PvComboboxBinding bndDf = new PvComboboxBinding(cbDf, NusmvSettings.DF, bindingParent,
				Arrays.asList(
						new IKeyLabelPair.Impl(null, "(default)"),
						new IKeyLabelPair.Impl("true", "Disable forward search (-df)"),
						new IKeyLabelPair.Impl("false", "Enable forward search (-f)")));
		registerBindingElement(bndDf);
	}

	private void createTimeoutPart(Composite compAdvancedClient, PvDataBinding bindingParent) {
		// "Timeout" GUI objects
		Label lblTimeout = new Label(compAdvancedClient, SWT.NONE);
		lblTimeout.setText("Timeout (sec):");

		Text tbTimeout = new Text(compAdvancedClient, SWT.BORDER);
		tbTimeout.setText("");
		createLabeledRow(compAdvancedClient, lblTimeout, tbTimeout, 1, true);
		PvLayoutUtils.applySmallWidthHint(tbTimeout);

		// "Timeout" data binding
		PvTextboxBinding bndTimeout = new PvOptionalTextboxBinding(tbTimeout, NusmvSettings.TIMEOUT, bindingParent);
		bndTimeout.addValidator(ITextValidator.Utils::isPositiveIntegerValidator, "Numeric value is expected or leave empty for default.");

		registerBindingElement(bndTimeout);
	}
}