/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Zsofia Adam - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.req.fret.gui;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import cern.plcverif.base.models.expr.utils.TypedAstVariable;
import cern.plcverif.library.req.fret.gui.exceptions.FretJsonException;
import cern.plcverif.verif.extensions.gui.context.VerificationCaseContext;

/**
 * This class is responsible for the generation of JSON texts
 * that are used to communicate inbetween PLCVerif and FRET.
 * It generate a JSON describing the current requirement
 * and another JSON describing our variables
 * (which will be used as a glossary in FRET)
 * @author zadam
 *
 */
public class FretReqJsonGenerator {	
	private enum DataType {
		BOOLEAN("BOOL", "boolean", 1),
		INTEGER("INT", "integer", 16),
		U_INTEGER("UINT", "unsigned integer", 16),
		SHORT_INTEGER("SINT", "integer", 8),
		U_SHORT_INTEGER("USINT", "unsigned integer", 8),
		DOUBLE_INTEGER("DINT", "integer", 32),
		U_DOUBLE_INTEGER("UDINT", "unsigned integer", 32),
		REAL("REAL", "double", 32),
		DOUBLE_WORD("DWORD", "integer", 32), // is broken up to bits below
		WORD("WORD", "integer", 16), // is broken up to bits below
		BYTE("BYTE", "integer", 8); // is broken up to bits below // TODO .%X index is not implemented in PLCVerif (in STEP7 frontend)
		
		private final static HashMap<String, DataType> TYPEMAP = new HashMap<String, DataType>();

		private final String plcType;
		private final int width;
		private final String fretType;
		private DataType(String plcType, String fretType, int width) {
			this.plcType = plcType;
			this.fretType = fretType;
			this.width = width;
		}
		
		public String getFretType() {
			return fretType;
		}

		public int getWidth() {
			return width;
		}
		
		static {
			for (DataType dt : EnumSet.allOf(DataType.class))
	        {
				TYPEMAP.put(dt.plcType, dt);	            
	        }
		}
		
		public static DataType getDataType(String plcType) {
			return TYPEMAP.get(plcType);
		}
	}

	private VerificationCaseContext context;
	
	public FretReqJsonGenerator(VerificationCaseContext context) {
		this.context = context;
	}
	
	public String generateFretInputJson(FretReqGui fretGui) throws FretJsonException {
		JsonObject jobj = new JsonObject();
		jobj.add("requirement", generateRequirementJson(fretGui));
		jobj.add("variables", generateVariableJson());
		return jobj.toString();
	}
	
	/**
	 * Generate a JSON in the format FRET accepts requirements
	 * The requirement might be completely empty or might even be
	 * an already existing requirement (created earlier with FRET), 
	 * which just needs to be modified in FRET
	 * @return JSON of requirement of verification case in FRET format
	 * @throws FretJsonException
	 */
	private JsonObject generateRequirementJson(FretReqGui fretGui) {
		JsonObject jobj = new JsonObject();
		jobj.addProperty("reqid", context.getReqId());
		jobj.addProperty("comments", context.getReqDescription());
		jobj.addProperty("rationale", context.getReqName());
		jobj.addProperty("fulltext", fretGui.getFretishReq());

		jobj.addProperty("parent_reqid", "");
		jobj.addProperty("project", "");
		
		jobj.add("semantics", new JsonObject());
		jobj.getAsJsonObject("semantics").addProperty("ftExpanded_inf", fretGui.getTlReq());
		return jobj;
	}
	
	/**
	 * Generate a JSON in the format FRET accepts variables (glossary)
	 * @return JSON of variables in FRET format
	 * @throws FretJsonException
	 */
	private JsonObject generateVariableJson() {
		List<TypedAstVariable> variables = context.getVariables();
		JsonObject variablesObj = new JsonObject();		
		JsonArray jarr = new JsonArray();
		variablesObj.add("docs", jarr);
		
		for(TypedAstVariable var : variables) {
			JsonObject jobj = new JsonObject();
			if(DataType.getDataType(var.getPlcType()) != null) {
				if(var.getPlcType() == "WORD" || var.getPlcType() == "DWORD" || var.getPlcType() == "BYTE") {
					int width = DataType.getDataType(var.getPlcType()).getWidth();
					for(int i = 0; i < width; i++) {
						jobj = new JsonObject();
						jobj.addProperty("component_name", context.getEntryBlock());
						jobj.addProperty("variableType", ""); // this stays empty (for now), as it is hard to find out here
						jobj.addProperty("dataType", "boolean");
						jobj.addProperty("project", "");
						jobj.addProperty("idType", "");
						jobj.addProperty("variable_name", var.getVariableName()+".%X"+i);
						jobj.addProperty("width", 1);
						jobj.addProperty("tool", "PLCVerif");
						jarr.add(jobj);
					}
				} else {
					jobj.addProperty("component_name", context.getEntryBlock());
					jobj.addProperty("variableType", ""); // this stays empty (for now), as it is hard to find out here
					jobj.addProperty("dataType", DataType.getDataType(var.getPlcType()).getFretType());
					jobj.addProperty("project", "");
					jobj.addProperty("idType", "");
					jobj.addProperty("variable_name", var.getVariableName());
					jobj.addProperty("width", DataType.getDataType(var.getPlcType()).getWidth());
					jobj.addProperty("tool", "PLCVerif");
					jarr.add(jobj);
				}
			}
		}
		return variablesObj;
	}
}
