/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Zsofia Adam - initial API and implementation
 *   Xaver Fink - additions
 *******************************************************************************/
package cern.plcverif.library.req.fret.gui;

import static cern.plcverif.base.gui.layout.PvLayoutUtils.createLabeledRow;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsKind;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.gui.binding.IKeyLabelPair;
import cern.plcverif.base.gui.binding.PvDataBinding;
import cern.plcverif.base.gui.binding.PvDataBindingGroup;
import cern.plcverif.base.gui.binding.swt.PvComboboxBinding;
import cern.plcverif.base.gui.binding.swt.PvTextboxBinding;
import cern.plcverif.base.gui.component.AbstractPvGuiPart;
import cern.plcverif.base.gui.layout.PvLayoutUtils;
import cern.plcverif.library.req.fret.gui.exceptions.FretJsonException;
import cern.plcverif.library.req.fret.gui.exceptions.FretPathException;
import cern.plcverif.library.requirement.fret.FretExecutor;
import cern.plcverif.library.requirement.fret.FretRequirementSettings;
import cern.plcverif.verif.extensions.gui.context.VerificationCaseContext;

public final class FretReqGui extends AbstractPvGuiPart<VerificationCaseContext> {
	private PvTextboxBinding bndFretishReq;
	private PvTextboxBinding bndTlReq;
	private PvComboboxBinding bndTlBox;
	private PvComboboxBinding bndTimeButtonBox;
	private Optional<? extends SettingsElement> settings;
	private String fretPath;
	private String cygwinPath;
	private JsonObject currentFretSemantics;
	
	public enum LtlType {
		PT_LTL("ptExpanded", "ptLTL"),
		FT_LTL_INF("ftInfAUExpanded", "LTL"),
		FT_LTL_FIN("ftExpanded", "finite LTL");
		
		private String fretStringRepresentation;
		private String humanReadable;
		
		LtlType(String fretStringRepresentation, String humanReadable) {
	        this.fretStringRepresentation = fretStringRepresentation;
	        this.humanReadable = humanReadable;
	    }
	 
	    public String getFretStringRepresentation() {
	        return fretStringRepresentation;
	    }
	    
	    public String getHumanReadable() {
			return humanReadable;
		}
	}
	
	public enum TimeTypes {
		CYCLES(),
		MS();
	}
	
	public FretReqGui(VerificationCaseContext context, Optional<? extends SettingsElement> settings) {
		super(context);
		this.settings = settings;
		
		if (settings.isPresent()) {
			Optional<Settings> fretPathAttribute = settings.get().getAttribute(FretRequirementSettings.FRET_PATH);
			if (fretPathAttribute.isPresent() && fretPathAttribute.get().kind() == SettingsKind.ELEMENT) {
				try {
					fretPath = fretPathAttribute.get().toSingle().value();
				} catch (SettingsParserException e) {
					throw new PlcverifPlatformException("PLCVerif was unable to set FRET path: " + e.getMessage());
				}
			}  else {
				throw new PlcverifPlatformException("PLCVerif was unable to set FRET path (setting is missing, try under Preferences)");			
			}

			Optional<Settings> cygwinPathAttribute = settings.get().getAttribute(FretRequirementSettings.CYGWIN_PATH);
			if (cygwinPathAttribute.isPresent() && cygwinPathAttribute.get().kind() == SettingsKind.ELEMENT) {
				try {
					cygwinPath = cygwinPathAttribute.get().toSingle().value();
				} catch (SettingsParserException e) {
					throw new PlcverifPlatformException("PLCVerif was unable to set Cygwin path: " + e.getMessage());
				}
			}  else {
				throw new PlcverifPlatformException("PLCVerif was unable to set Cygwin path (setting is missing, try under Preferences)");			
			}
		} 
//		else {
//			throw new PlcverifPlatformException("PLCVerif was unable to configure Fret (setting is missing, try under Preferences)");			
//		}
	}
	
	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
	}
	
	@Override
	public void createPart(org.eclipse.swt.widgets.Composite composite, PvDataBinding parentBinding) {
		PvLayoutUtils.checkExpectedLayout(composite);

		FormToolkit toolkit = new FormToolkit(composite.getDisplay());
		
		Button btnEdit = toolkit.createButton(composite, "Edit in FRET", SWT.PUSH);
		PvLayoutUtils.createRow(composite, btnEdit, 2, true);
		PvDataBindingGroup bind = PvDataBindingGroup.createRoot();
		
		bind.addOnDirtyPropertyChange(it -> {});

		Text txtFretish = toolkit.createText(composite, "N/A", SWT.READ_ONLY | SWT.MULTI | SWT.WRAP);
		Label lblFretishTitle = toolkit.createLabel(composite, "Fretish requirement:");
		
		Combo timeButton = new Combo(composite, SWT.READ_ONLY);
		
        bndFretishReq = new PvTextboxBinding(txtFretish, FretRequirementSettings.FRETISH_REQUIREMENT, parentBinding);
		registerBindingElement(bndFretishReq);
		PvLayoutUtils.createLabeledDropdownRow(composite, lblFretishTitle, txtFretish, timeButton);
		PvLayoutUtils.setMinHeight(txtFretish, 40);
		
		Text txtTl = toolkit.createText(composite, "N/A", SWT.READ_ONLY | SWT.MULTI | SWT.WRAP);
		Label lblTlTitle = toolkit.createLabel(composite, "TL requirement:");
		
		Combo cbTl = new Combo(composite, SWT.READ_ONLY);
		
		bndTlReq = new PvTextboxBinding(txtTl, FretRequirementSettings.TL_REQUIREMENT, parentBinding);
		registerBindingElement(bndTlReq);
		PvLayoutUtils.createLabeledDropdownRow(composite, lblTlTitle, txtTl, cbTl);
		PvLayoutUtils.setMinHeight(txtTl, 40);
		
		// Create data binding for "Pattern" row
		List<IKeyLabelPair> patternList = new ArrayList<>();
		for (LtlType type : LtlType.values()) {
			patternList.add(new IKeyLabelPair.Impl(type.name(), type.humanReadable));
		}
		// Create data binding for timing Dropdown button
		List<IKeyLabelPair> timeTypeList = new ArrayList<>();
		for (TimeTypes type : TimeTypes.values()) {
			timeTypeList.add(new IKeyLabelPair.Impl(type.name(), type.name()));
		}


		bndTlBox = new PvComboboxBinding(cbTl, FretRequirementSettings.TL_REQUIREMENT_FORMAT,
				parentBinding, patternList);
		bndTimeButtonBox = new PvComboboxBinding(timeButton, FretRequirementSettings.TL_TIME_FORMAT,
				parentBinding, timeTypeList);
		registerBindingElement(bndTlBox);
		registerBindingElement(bndTimeButtonBox);
		bndTlBox.addChangeListener(e -> {
			if (currentFretSemantics != null) {
				bndTlReq.setValue(getTlStringFromSemantics(currentFretSemantics, LtlType.valueOf(bndTlBox.getSelectedItem().get())));
			}else {
				VerificationCaseContext context = super.getContext();
				File fretDirectory = context.getVerifCaseFile().getParent().getLocation().append(context.getReqId()).toFile();
				String fretRequirementPath = fretDirectory.getAbsolutePath() + File.separator + "requirement.json";
				File reqJsonFile = new File(fretRequirementPath);
				try {
		            String content = new String(Files.readAllBytes(Paths.get(reqJsonFile.toURI())));
		            Gson gson = new Gson();
		            JsonObject requirements = gson.fromJson(content, JsonObject.class);
		            JsonObject requirement = requirements.getAsJsonObject("requirement");
		            JsonObject semantics = requirement.getAsJsonObject("semantics");
		            currentFretSemantics = semantics;
				} catch (Exception ex) {
					// TODO: handle exception
				}
			}
			//TODO: If user tries to change representation after the GUI is initialized throw error with hint
		});
		
		VerificationCaseContext context = super.getContext();
		FretReqGui fretGui = this;
		btnEdit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {				
				super.widgetSelected(e);
				
				try {
					checkFretPath();
				} catch (FretPathException e1) {
					Display.getDefault().asyncExec(
							() -> MessageDialog.openError(Display.getDefault().getActiveShell(),
							"FRET binary path not setup correctly", "FRET binary path not setup correctly: " + e1.getMessage()));
					throw new PlcverifPlatformException("FRET binary path not setup correctly: " + e1.getMessage());
				}
				
				btnEdit.setEnabled(false);
				btnEdit.setText("Loading FRET...");
				
				File fretDirectory = context.getVerifCaseFile().getParent().getLocation().append(context.getReqId()).toFile();
				if(!fretDirectory.exists()) fretDirectory.mkdir();
				FretReqJsonGenerator jsonGenerator = new FretReqJsonGenerator(context);
				String fretGlossaryPath = fretDirectory.getAbsolutePath()+File.separator+"glossary.json";
				File glossaryFile = new File(fretGlossaryPath);
				try {
					String fretJson = jsonGenerator.generateFretInputJson(fretGui);
					FileWriter fw = new FileWriter(glossaryFile);
					fw.write(fretJson);
					fw.close();
				} catch (FretJsonException e1) {
					btnEdit.setText("Edit in FRET");
					btnEdit.setEnabled(true);
					Display.getDefault().asyncExec(
							() -> MessageDialog.openError(Display.getDefault().getActiveShell(),
							"FRET JSON generation error", "PLCVerif was unable to generate the proper FRET JSON: " + e1.getMessage()));
					throw new PlcverifPlatformException("PLCVerif was unable to generate the proper FRET JSON: " + e1.getMessage());
				} catch (IOException e1) {
					btnEdit.setText("Edit in FRET");
					btnEdit.setEnabled(true);
					Display.getDefault().asyncExec(
							() -> MessageDialog.openError(Display.getDefault().getActiveShell(),
							"FRET JSON generation error", "PLCVerif was unable to generate the proper FRET JSON: " + e1.getMessage()));
					throw new PlcverifPlatformException("PLCVerif was unable to generate the proper FRET JSON: " + e1.getMessage());
				}
				
				String fretRequirementPath = fretDirectory.getAbsolutePath() + File.separator + "requirement.json";
				try {
					new Thread(() -> openFret(fretGlossaryPath, fretRequirementPath, btnEdit)).start();
					Display.getDefault().asyncExec(
							() -> MessageDialog.openInformation(Display.getDefault().getActiveShell(),
							"FRET is starting up", "FRET is currently starting up or running. Changes in PLCverif are not synced with FRET."));
				} catch (PlcverifPlatformException e1) {
					Display.getDefault().asyncExec(
							() -> MessageDialog.openError(Display.getDefault().getActiveShell(),
							"FRET launch error", "Unable to launch FRET. Make sure it is installed and correctly configured: " + e1.getMessage()));
					throw new PlcverifPlatformException("Unable to launch FRET. Make sure it is installed and correctly configured: " + e1.getMessage());
				}

			}
		});
	}
	
	private void checkFretPath() throws FretPathException {
		Path path = Paths.get(fretPath, "package.json");
		if (!Files.exists(path)) {
			throw new FretPathException("Path " + fretPath + " does not contain an electron package.");
		}
	}
	
	String getFretishReq() {
		return bndFretishReq.getValue();
	}
	
	String getTlReq() {
		return bndTlReq.getValue();
	}
	
	static boolean isWindows() {
		return System.getProperty("os.name").toLowerCase().contains("win");
	}

	private void openFret(String fretGlossaryPath, String fretRequirementPath, Button btnEdit) {
		String fretElectronDir = fretPath;
		ProcessBuilder pb;

		FretExecutor.execute(fretGlossaryPath, fretRequirementPath, fretElectronDir, cygwinPath);
		
		Display.getDefault().asyncExec(() -> {
			File reqJsonFile = new File(fretRequirementPath);
			try {
	            String content = new String(Files.readAllBytes(Paths.get(reqJsonFile.toURI())));
	            Gson gson = new Gson();
	            JsonObject requirements = gson.fromJson(content, JsonObject.class);
	            JsonObject requirement = requirements.getAsJsonObject("requirement");
	            
	            String fretishReq = requirement.get("fulltext").getAsString();
	            
	            JsonObject semantics = requirement.getAsJsonObject("semantics");
	            
	            String ftlReq = getTlStringFromSemantics(semantics, LtlType.valueOf(bndTlBox.getSelectedItem().get()));
	            
	            getContext().setReqId(requirement.get("reqid").getAsString());
	            getContext().setReqName(requirement.get("rationale").getAsString());
	            getContext().setReqDescription(requirement.get("comments").getAsString());
	            
	            currentFretSemantics = semantics;
	            bndFretishReq.setValue(fretishReq);
	            bndTlReq.setValue(ftlReq);
	            currentFretSemantics = semantics;
	            
	            btnEdit.setText("Edit in FRET");
				btnEdit.setEnabled(true);
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		});
	}
	
	private String getTlStringFromSemantics(JsonObject semantics, LtlType type) {
		return transformFromFretNuxmv(semantics.get(type.fretStringRepresentation).getAsString());
	}

	private String transformFromFretNuxmv(String tlExprStr) {
		String tlExprStrSanitized = tlExprStr;

		
		
		// if there is a simple (G/F/X varname) expression, we change it to (G (varname))
		Pattern unaryOpParentheses = Pattern.compile("\\(([GFXHOYZ])(\\[\\d+,\\d+\\])? ([^()]*)\\)");
		Matcher unaryOpParenthesesMatcher = unaryOpParentheses.matcher(tlExprStrSanitized);		
		if(unaryOpParenthesesMatcher.find()) {
			tlExprStrSanitized = unaryOpParenthesesMatcher.replaceAll("\\($1$2 \\($3\\)\\)");
		}
		
		// TODO what to do with R/V
		// replace "V" operator with "R"
//		Pattern binaryVOp = Pattern.compile("(.*)( V )(.*)");
//		Matcher binaryVOpMatcher = binaryVOp.matcher(tlExprStrSanitized);
//		if(binaryVOpMatcher.find()) {
//			tlExprStrSanitized = binaryVOpMatcher.replaceAll("$1 R $3");
//		}
		
		// U and V/R operations shall be in [] instead of () - issue is with having to find the matching parentheses, no regex for that
		tlExprStrSanitized = replaceUVRParentheses(tlExprStrSanitized);
		
		tlExprStrSanitized = tlExprStrSanitized.replace("->", "-->")
				.replace("&", "and")
				.replace("|", "or")
				.replace("PLC_END", "{PLC_END}")
				.replace("PLC_START", "{PLC_START}");
		return mapSpecialCharacters(tlExprStrSanitized); // these map the special characters in variable names - has to happen after the above mapping of ops
	}
	
	// U and V/R operations shall be in [] instead of () - issue is with having to find the matching parentheses, no regex for that
	private String replaceUVRParentheses(String tlExprStr) {
		String tlExprStrSanitized = tlExprStr;
		ArrayList<Integer> binOpIndices = new ArrayList<Integer>();
		String operator = " U ";
		int index = tlExprStrSanitized.indexOf(operator);
		while (index >= 0) {
			binOpIndices.add(index+1); // index of operator character itself
			index = tlExprStrSanitized.indexOf(operator, index + 1);
		}
		
		operator = " R ";
		index = tlExprStrSanitized.indexOf(operator);
		while (index >= 0) {
			binOpIndices.add(index+1); // index of operator character itself
			index = tlExprStrSanitized.indexOf(operator, index + 1);
		}
		
		operator = " S ";
		index = tlExprStrSanitized.indexOf(operator);
		while (index >= 0) {
			binOpIndices.add(index+1); // index of operator character itself
			index = tlExprStrSanitized.indexOf(operator, index + 1);
		}

		operator = " V ";
		index = tlExprStrSanitized.indexOf(operator);
		while (index >= 0) {
			binOpIndices.add(index+1); // index of operator character itself
			index = tlExprStrSanitized.indexOf(operator, index + 1);
		}
		
		for(Integer i : binOpIndices) {
			Integer[] indices = findBinaryOpParentheses(i, tlExprStrSanitized);
		    StringBuilder sb = new StringBuilder(tlExprStrSanitized);
		    sb.setCharAt(indices[0], '[');
		    sb.setCharAt(indices[1], ']');
			tlExprStrSanitized = sb.toString();
		}
		
		return tlExprStrSanitized;
	}

	private Integer[] findBinaryOpParentheses(Integer opIndex, String tlExprStr) {
		int openingIndex = -1;
		int closingIndex = -1;
		
		// find opening parentheses
		int numOfClosing = 0;
		for(int i = opIndex; i>=0; i--) {
			if(tlExprStr.charAt(i)==')') {
				numOfClosing++;
			} else if(tlExprStr.charAt(i)=='(') {
				if(numOfClosing==0) {
					openingIndex = i;
					break;
				} else {
					numOfClosing--;
				}
			}
		}
		if(openingIndex==-1) {
			throw new PlcverifPlatformException("LTL Parsing error: Couldn't find opening parenthesis around binary operation");
		}
		
		// find closing parentheses
		int numOfOpening = 0;
		for(int i = opIndex; i<tlExprStr.length(); i++) {
			if(tlExprStr.charAt(i)=='(') {
				numOfOpening++;
			} else if(tlExprStr.charAt(i)==')') {
				if(numOfOpening==0) {
					closingIndex = i;
					break;
				} else {
					numOfOpening--;
				}
			}
		}
		if(closingIndex==-1) {
			throw new PlcverifPlatformException("LTL Parsing error: Couldn't find closing parenthesis around binary operation");
		}
		Integer[] result = {openingIndex, closingIndex};
		return result;
	}
	
	private String mapSpecialCharacters(String str) {
		return str.replace("_DOT_", ".")
        		.replace("_PRC_","%")
        		.replace("_HSH_","#")
        		.replace("_DOL_","$")
        		// This broke a variable in ELISA
        		//.replace("_AMP_","&")
        		.replace("_ATS_","@")
        		.replace("_BNG_","!")
        		.replace("_QUS_","?")
        		.replace("_HYP_","-")
        		.replace("_AST_","*")
        		.replace("_EQU_","=")
        		.replace("_CRT_","^")
        		.replace("_SPC_"," ")
        		.replace("_PLS_","+")
        		.replace("_SEM_",";")
        		.replace("_CLN_",":")
        		.replace("_CMA_",",")
        		.replace("_FSL_","/")
        		.replace("_BSL_","\\")
        		.replace("_VBR_","|")
        		.replace("_TLD_","~")
        		.replace("_LAN_","<")
        		.replace("_RAN_",">")
        		.replace("_LPR_","(")
        		.replace("_RPR_",")")
				.replace("_LCB_","{")
				.replace("_RCB_","}")
				.replace("_LSQ_","[")
				.replace("_RSQ_","]")
				.replace("_SQT_","'")
				.replace("_DQT_","\"")
				.replace("_BQT_","`");
		}
}
