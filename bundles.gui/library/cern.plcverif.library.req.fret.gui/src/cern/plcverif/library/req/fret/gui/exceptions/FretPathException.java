package cern.plcverif.library.req.fret.gui.exceptions;

public class FretPathException extends Exception {
	private static final long serialVersionUID = -1395220514758235872L;

	public FretPathException(String message) {
		super(message);
	}
	
	public FretPathException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
