package cern.plcverif.library.req.fret.gui.exceptions;

public class FretJsonException extends Exception {
	private static final long serialVersionUID = -1395220514758235873L;

	public FretJsonException(String message) {
		super(message);
	}
	
	public FretJsonException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
