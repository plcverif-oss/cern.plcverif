/*******************************************************************************
 * (C) Copyright CERN 2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.req.fret.gui;

import java.io.IOException;
import java.util.Optional;

import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.exceptions.SettingsSerializerException;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.gui.preferences.SettingsPreferencePageInitException;
import cern.plcverif.base.gui.preferences.SettingsPreferenceStoreFactory;
import cern.plcverif.library.requirement.fret.FretRequirementExtension;
import cern.plcverif.library.requirement.fret.FretRequirementSettings;

public class FretReqPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	public FretReqPreferencePage() {
		super(GRID);
	}

	@Override
	public void init(IWorkbench workbench) {
		Settings defaultSettings;

		try {
			defaultSettings = SpecificSettingsSerializer.toGenericSettings(new FretRequirementSettings(),
					Optional.empty(), false);
			setPreferenceStore(
					SettingsPreferenceStoreFactory.createStore(FretRequirementExtension.CMD_ID, defaultSettings.toSingle()));
		} catch (IOException | SettingsParserException | SettingsSerializerException e) {
			throw new SettingsPreferencePageInitException(e);
		}
	}

	@Override
	protected void createFieldEditors() {
		final Composite parent = getFieldEditorParent();

		//@formatter:off
		addField(new DirectoryFieldEditor(FretRequirementSettings.FRET_PATH,
				"Path to fret-electron directory:", parent));
		
		addField(new DirectoryFieldEditor( FretRequirementSettings.CYGWIN_PATH,
				"Cygwin bin directory (windows only):", parent));
		//@formatter:on
	}
}
