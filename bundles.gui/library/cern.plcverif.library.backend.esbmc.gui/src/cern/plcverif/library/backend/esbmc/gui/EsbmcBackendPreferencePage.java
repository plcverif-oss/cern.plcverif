/*******************************************************************************
 * (C) Copyright CERN 2017-2023. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Xaver Fink	- modifications for ESBMC
 *******************************************************************************/
package cern.plcverif.library.backend.esbmc.gui;

import java.io.IOException;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.specific.AbstractSpecificSettings;
import cern.plcverif.base.gui.preferences.SettingsPreferencePageInitException;
import cern.plcverif.base.gui.preferences.SettingsPreferenceStore;
import cern.plcverif.base.gui.preferences.SettingsPreferenceStoreFactory;
import cern.plcverif.base.gui.preferences.SettingsPreferenceStoreUtil;
import cern.plcverif.library.backend.esbmc.EsbmcBackendExtension;
import cern.plcverif.library.backend.esbmc.EsbmcSettings;
import cern.plcverif.library.backend.esbmc.EsbmcSettings.EsbmcModelVariant;
import cern.plcverif.library.backend.esbmc.EsbmcSettings.EsbmcVerifyCommand;

public class EsbmcBackendPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	public EsbmcBackendPreferencePage() {
		super(GRID);
	}

	@Override
	public void init(IWorkbench workbench) {
		EsbmcBackendExtension backendExtension = new EsbmcBackendExtension();
		SettingsElement defaults = backendExtension.loadDefaultSettings();
		EsbmcSettings esbmcSettings = new EsbmcSettings();
		SettingsElement defaultSettings = SettingsPreferenceStoreUtil.mergePluginSettings(esbmcSettings, defaults);

		try {
			SettingsPreferenceStore store = SettingsPreferenceStoreFactory.createStore(EsbmcBackendExtension.CMD_ID, defaultSettings);
			setPreferenceStore(store);
		} catch (IOException | SettingsParserException e) {
			throw new SettingsPreferencePageInitException(e);
		}
	}

	@Override
	protected void createFieldEditors() {
		final Composite parent = getFieldEditorParent();

		//@formatter:off
		addField(new FileFieldEditor(EsbmcSettings.BINARY_PATH,
				"Path of ESBMC binary:", parent));
		addField(new IntegerFieldEditor(EsbmcSettings.TIMEOUT,
				"Timeout (in sec):", parent));
		addField(new RadioGroupFieldEditor(EsbmcSettings.VERIFY_COMMAND, "Verification command:", 1,
				new String[][] {
					{ "k-induction", EsbmcVerifyCommand.K_INDUCTION.name() }, 
					{ "falsification", EsbmcVerifyCommand.FALSIFICATION.name() }
				},
				parent));
		addField(new BooleanFieldEditor(EsbmcSettings.PARALLELIZE_COMPUTATION,
				"Acitvate the k-induction parallelization feature:", parent));
		addField(new RadioGroupFieldEditor(EsbmcSettings.MODEL_VARIANT, "Model variant:", 1,
				new String[][] {
					{ "CFA declaration", EsbmcModelVariant.CFD.name() }, 
					{ "CFA instance", EsbmcModelVariant.CFI.name() },
					{ "Structured CFA declaration", EsbmcModelVariant.CFD_STRUCTURED.name() }
				},
				parent));
		addField(new IntegerFieldEditor(EsbmcSettings.VERBOSITY,
				"Output verbosity level (4..10, 10 is the most verbose):", parent));
		addField(new BooleanFieldEditor(EsbmcSettings.PRINT_STDOUT_TO_FILE,
				"Write the console output of ESBMC to a file (next to the generated C file)", parent));
		//@formatter:on
	}
}
