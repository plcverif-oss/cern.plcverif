/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reporter.plaintext.gui;

import static cern.plcverif.base.gui.preferences.SettingsPreferenceStoreUtil.createSeverityFieldEditor;

import java.io.IOException;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.exceptions.SettingsSerializerException;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.gui.preferences.SettingsPreferencePageInitException;
import cern.plcverif.base.gui.preferences.SettingsPreferenceStoreFactory;
import cern.plcverif.library.reporter.plaintext.PlaintextReporterExtension;
import cern.plcverif.library.reporter.plaintext.PlaintextReporterSettings;
import cern.plcverif.verif.interfaces.AbstractHumanReporterSettings;

public class PlaintextReporterPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	public PlaintextReporterPreferencePage() {
		super(GRID);
	}

	@Override
	public void init(IWorkbench workbench) {
		try {
			Settings defaultSettings = SpecificSettingsSerializer.toGenericSettings(new PlaintextReporterSettings());
			// As there is no default settings file included as resource, there is no need for merging. 
			
			Preconditions.checkState(defaultSettings instanceof SettingsElement, "Default settings does not have appropriate root element for plain text verification reporter.");
			setPreferenceStore(
					SettingsPreferenceStoreFactory.createStore(PlaintextReporterExtension.CMD_ID, defaultSettings.toSingle()));
		} catch (IOException | SettingsParserException | SettingsSerializerException e) {
			throw new SettingsPreferencePageInitException(e);
		}
	}

	@Override
	protected void createFieldEditors() {
		final Composite parent = getFieldEditorParent();

		addField(createSeverityFieldEditor(AbstractHumanReporterSettings.MIN_LOG_LEVEL, "Minimum log message severity to report: ", parent));
		addField(new BooleanFieldEditor(AbstractHumanReporterSettings.INCLUDE_SETTINGS, "The effective settings should be included in the report", parent));
		addField(new IntegerFieldEditor(PlaintextReporterSettings.MAX_OUTPUT_LENGTH, "Maximum console output length:", parent));
	}
}
