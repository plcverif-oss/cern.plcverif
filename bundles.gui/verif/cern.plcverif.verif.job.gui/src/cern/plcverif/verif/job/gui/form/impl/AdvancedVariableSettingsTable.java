/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.job.gui.form.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider;
import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider.IStyledLabelProvider;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.StyledString.Styler;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.TextStyle;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsKind;
import cern.plcverif.base.common.settings.SettingsList;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.gui.binding.PvDataBinding;
import cern.plcverif.base.gui.binding.exceptions.DataInvalidException;
import cern.plcverif.base.models.expr.utils.TypedAstVariable;

public class AdvancedVariableSettingsTable extends Composite {
	private static class Line {
		private final String variableName;
		private String astType = "";
		private boolean input = false;
		private boolean parameter = false;
		private String assignedValue = "";
		private String lowerBound = "";
		private String upperBound = "";

		public Line(String variableName, String astType) {
			this.variableName = variableName;
			this.astType = astType;
		}

		public String getVariableName() {
			return variableName;
		}
		
		public String getAstType() {
			return astType;
		}

		public void setInput(boolean input) {
			this.input = input;
			if (this.input) {
				this.parameter = false;
				this.assignedValue = "";
			}
		}

		public boolean isInput() {
			return input;
		}

		public void setParameter(boolean parameter) {
			this.parameter = parameter;
			if (this.parameter) {
				this.input = false;
				this.assignedValue = "";
			}
		}

		public boolean isParameter() {
			return parameter;
		}

		public void setAssignedValue(String assignedValue) {
			this.assignedValue = assignedValue;
			if (assignedValue != null && !assignedValue.trim().isEmpty()) {
				this.input = false;
				this.parameter = false;
			}
		}

		public String getAssignedValue() {
			return assignedValue;
		}
		
		
		public void setLowerBound(String lowerBound) {
			this.lowerBound = lowerBound;
		}

		public String getLowerBound() {
			return lowerBound;
		}
		
		public void setUpperBound(String upperBound) {
			this.upperBound = upperBound;
		}

		public String getUpperBound() {
			return upperBound;
		}

		public boolean isDefault() {
			return !input && !parameter && "".equals(assignedValue) && "".equals(lowerBound) && "".equals(upperBound);
		}
	}

	private static class TableContentProvider implements IStructuredContentProvider {
		@Override
		public Object[] getElements(Object inputElement) {
			Preconditions.checkArgument(inputElement instanceof List<?>);
			return ((List<?>) inputElement).toArray();
		}
	}

	/**
	 * Class handling the data binding for the input variables ('req.inputs').
	 */
	private class PvInputsBinding extends PvDataBinding {
		protected PvInputsBinding(String settingsFqnElement, PvDataBinding parent) {
			super(settingsFqnElement, parent);
		}

		@Override
		protected void loadThis(Optional<? extends Settings> settings) throws SettingsParserException {
			if (settings.isPresent()) {
				Preconditions.checkArgument(settings.get().kind() == SettingsKind.LIST);

				SettingsList inputVars = settings.get().toList();
				for (Settings inputVarSetting : inputVars.elements()) {
					Preconditions.checkArgument(inputVarSetting.kind() == SettingsKind.ELEMENT);
					String varName = inputVarSetting.toSingle().value();
					if (variableLookup.containsKey(varName)) {
						variableLookup.get(varName).setInput(true);
					} else {
						addVariableLine(varName).setInput(true);
					}
				}
			}
		}

		@Override
		protected Optional<Settings> saveThis() throws DataInvalidException {
			SettingsList ret = SettingsList.empty();
			for (Line line : variables) {
				if (line.isInput()) {
					ret.add(SettingsElement.createRootElement("", line.getVariableName()));
				}
			}

			if (ret.elements().isEmpty()) {
				return Optional.empty();
			} else {
				return Optional.of(ret);
			}
		}

		@Override
		protected boolean isThisValid() {
			return true;
		}
		
		@Override
		protected void setDirty() {
			// Override to make the setDirty() method visible
			super.setDirty();
		}
	}

	/**
	 * Class handling the data binding for the parameter variables
	 * ('req.parameters').
	 */
	private class PvParamsBinding extends PvDataBinding {
		protected PvParamsBinding(String settingsFqnElement, PvDataBinding parent) {
			super(settingsFqnElement, parent);
		}

		@Override
		protected void loadThis(Optional<? extends Settings> settings) throws SettingsParserException {
			if (settings.isPresent()) {
				Preconditions.checkArgument(settings.get().kind() == SettingsKind.LIST);

				SettingsList paramVars = settings.get().toList();
				for (Settings paramVarSetting : paramVars.elements()) {
					Preconditions.checkArgument(paramVarSetting.kind() == SettingsKind.ELEMENT);
					String varName = paramVarSetting.toSingle().value();
					if (variableLookup.containsKey(varName)) {
						variableLookup.get(varName).setParameter(true);
					} else {
						addVariableLine(varName).setParameter(true);
					}
				}
			}
		}

		@Override
		protected Optional<Settings> saveThis() throws DataInvalidException {
			SettingsList ret = SettingsList.empty();
			for (Line line : variables) {
				if (line.isParameter()) {
					ret.add(SettingsElement.createRootElement("", line.getVariableName()));
				}
			}

			if (ret.elements().isEmpty()) {
				return Optional.empty();
			} else {
				return Optional.of(ret);
			}
		}

		@Override
		protected boolean isThisValid() {
			return true;
		}
		
		@Override
		protected void setDirty() {
			// Override to make the setDirty() method visible
			super.setDirty();
		}
	}

	/**
	 * Class handling the data binding for the variable value bindings
	 * ('req.bindings.variable' and 'req.bindings.value').
	 */
	private class PvValueBindingBinding extends PvDataBinding {
		protected PvValueBindingBinding(String settingsFqnElement, PvDataBinding parent) {
			super(settingsFqnElement, parent);
		}

		@Override
		protected void loadThis(Optional<? extends Settings> settings) throws SettingsParserException {
			if (settings.isPresent()) {
				Preconditions.checkArgument(settings.get().kind() == SettingsKind.ELEMENT);

				Optional<Settings> vars = settings.get().toSingle().getAttribute("variable");
				Optional<Settings> values = settings.get().toSingle().getAttribute("value");

				Preconditions.checkState(vars.isPresent() && vars.get().kind() == SettingsKind.LIST);
				Preconditions.checkState(values.isPresent() && values.get().kind() == SettingsKind.LIST);
				Preconditions
						.checkState(vars.get().toList().elements().size() == values.get().toList().elements().size());

				for (int i = 0; i < vars.get().toList().elements().size(); i++) {
					String varName = vars.get().toList().elements().get(i).toSingle().value();
					String value = values.get().toList().elements().get(i).toSingle().value();

					if (variableLookup.containsKey(varName)) {
						variableLookup.get(varName).setAssignedValue(value);
					} else {
						addVariableLine(varName).setAssignedValue(value);
					}
				}
			}
		}

		@Override
		protected Optional<Settings> saveThis() throws DataInvalidException {
			SettingsElement bindingsNode = SettingsElement.createRootElement("bindings");
			SettingsList varNamesNode = SettingsList.empty();
			bindingsNode.setAttribute("variable", varNamesNode);
			SettingsList valuesNode = SettingsList.empty();
			bindingsNode.setAttribute("value", valuesNode);

			for (Line line : variables) {
				if (line.getAssignedValue() != null && !line.getAssignedValue().isEmpty()) {
					varNamesNode.add(SettingsElement.createRootElement("", line.getVariableName()));
					valuesNode.add(SettingsElement.createRootElement("", line.getAssignedValue()));
				}
			}

			if (varNamesNode.elements().isEmpty()) {
				return Optional.empty();
			} else {
				return Optional.of(bindingsNode);
			}
		}

		@Override
		protected boolean isThisValid() {
			return true;
		}

		@Override
		protected void setDirty() {
			// Override to make the setDirty() method visible
			super.setDirty();
		}
	}
	
	/**
	 * Class handling the data binding for the variable value bindings
	 * ('req.bindings.variable' and 'req.bindings.value').
	 */
	private class PvLowerBoundBinding extends PvDataBinding {
		protected PvLowerBoundBinding(String settingsFqnElement, PvDataBinding parent) {
			super(settingsFqnElement, parent);
		}

		@Override
		protected void loadThis(Optional<? extends Settings> settings) throws SettingsParserException {
			if (settings.isPresent()) {
				Preconditions.checkArgument(settings.get().kind() == SettingsKind.ELEMENT);

				Optional<Settings> vars = settings.get().toSingle().getAttribute("variable");
				Optional<Settings> values = settings.get().toSingle().getAttribute("value");

				Preconditions.checkState(vars.isPresent() && vars.get().kind() == SettingsKind.LIST);
				Preconditions.checkState(values.isPresent() && values.get().kind() == SettingsKind.LIST);
				Preconditions
						.checkState(vars.get().toList().elements().size() == values.get().toList().elements().size());

				for (int i = 0; i < vars.get().toList().elements().size(); i++) {
					String varName = vars.get().toList().elements().get(i).toSingle().value();
					String value = values.get().toList().elements().get(i).toSingle().value();

					if (variableLookup.containsKey(varName)) {
						variableLookup.get(varName).setLowerBound(value);
					} else {
						addVariableLine(varName).setLowerBound(value);
					}
				}
			}
		}

		@Override
		protected Optional<Settings> saveThis() throws DataInvalidException {
			SettingsElement bindingsNode = SettingsElement.createRootElement("lowerBounds");
			SettingsList varNamesNode = SettingsList.empty();
			bindingsNode.setAttribute("variable", varNamesNode);
			SettingsList valuesNode = SettingsList.empty();
			bindingsNode.setAttribute("value", valuesNode);

			for (Line line : variables) {
				if (line.getLowerBound() != null && !line.getLowerBound().isEmpty()) {
					varNamesNode.add(SettingsElement.createRootElement("", line.getVariableName()));
					valuesNode.add(SettingsElement.createRootElement("", line.getLowerBound()));
				}
			}

			if (varNamesNode.elements().isEmpty()) {
				return Optional.empty();
			} else {
				return Optional.of(bindingsNode);
			}
		}

		@Override
		protected boolean isThisValid() {
			return true;
		}

		@Override
		protected void setDirty() {
			// Override to make the setDirty() method visible
			super.setDirty();
		}
	}
	
	/**
	 * Class handling the data binding for the variable value bindings
	 * ('req.bindings.variable' and 'req.bindings.value').
	 */
	private class PvUpperBoundBinding extends PvDataBinding {
		protected PvUpperBoundBinding(String settingsFqnElement, PvDataBinding parent) {
			super(settingsFqnElement, parent);
		}

		@Override
		protected void loadThis(Optional<? extends Settings> settings) throws SettingsParserException {
			if (settings.isPresent()) {
				Preconditions.checkArgument(settings.get().kind() == SettingsKind.ELEMENT);

				Optional<Settings> vars = settings.get().toSingle().getAttribute("variable");
				Optional<Settings> values = settings.get().toSingle().getAttribute("value");

				Preconditions.checkState(vars.isPresent() && vars.get().kind() == SettingsKind.LIST);
				Preconditions.checkState(values.isPresent() && values.get().kind() == SettingsKind.LIST);
				Preconditions
						.checkState(vars.get().toList().elements().size() == values.get().toList().elements().size());

				for (int i = 0; i < vars.get().toList().elements().size(); i++) {
					String varName = vars.get().toList().elements().get(i).toSingle().value();
					String value = values.get().toList().elements().get(i).toSingle().value();

					if (variableLookup.containsKey(varName)) {
						variableLookup.get(varName).setUpperBound(value);
					} else {
						addVariableLine(varName).setUpperBound(value);
					}
				}
			}
		}

		@Override
		protected Optional<Settings> saveThis() throws DataInvalidException {
			SettingsElement bindingsNode = SettingsElement.createRootElement("upperBounds");
			SettingsList varNamesNode = SettingsList.empty();
			bindingsNode.setAttribute("variable", varNamesNode);
			SettingsList valuesNode = SettingsList.empty();
			bindingsNode.setAttribute("value", valuesNode);

			for (Line line : variables) {
				if (line.getUpperBound() != null && !line.getUpperBound().isEmpty()) {
					varNamesNode.add(SettingsElement.createRootElement("", line.getVariableName()));
					valuesNode.add(SettingsElement.createRootElement("", line.getUpperBound()));
				}
			}

			if (varNamesNode.elements().isEmpty()) {
				return Optional.empty();
			} else {
				return Optional.of(bindingsNode);
			}
		}

		@Override
		protected boolean isThisValid() {
			return true;
		}

		@Override
		protected void setDirty() {
			// Override to make the setDirty() method visible
			super.setDirty();
		}
	}

	private static class VariableNameFilter extends ViewerFilter {
		/**
		 * Regex pattern to be used for filtering. If null, all lines are to be
		 * shown.
		 */
		private String filterText = null;

		@Override
		public boolean select(final Viewer viewer, final Object parentElement, final Object element) {
			if (this.filterText == null) {
				return true;
			}
			Line line = (Line) element;
			return line.getVariableName().matches(this.filterText);
		}

		/**
		 * Sets the filter text. The filter text may contain {@code ?} and
		 * {@code *} wildcards. Everything else will be escaped.
		 * 
		 * @param filterText
		 *            Filter text
		 */
		public void setFilterText(String filterText) {
			if (filterText == null || filterText.isEmpty()) {
				this.filterText = null;
			} else {
				this.filterText = String.format("(?i).*(%s).*",
						Pattern.quote(filterText).replace("*", "\\E.*\\Q").replace("?", "\\E.\\Q"));
			}
		}

		public String getFilterText() {
			return filterText;
		}
	}

	private class VariableNameStyledLabelProvider implements IStyledLabelProvider {
		private final Styler matchStyler = new Styler() {
			@Override
			public void applyStyles(TextStyle textStyle) {
				textStyle.underline = true;
			}
		};

		@Override
		public StyledString getStyledText(Object element) {
			if (element instanceof Line) {
				String varName = ((Line) element).getVariableName();
				if (varNameFilter.getFilterText() == null) {
					// no filtering, don't even bother
					return new StyledString(varName);
				}

				StyledString ret = new StyledString(varName);
				Matcher m = Pattern.compile(varNameFilter.getFilterText(), Pattern.CASE_INSENSITIVE).matcher(varName);
				while (m.find()) {
					ret.setStyle(m.start(1), m.end(1) - m.start(1), matchStyler);
				}
				
				return ret;
			} else {
				return new StyledString(element.toString());
			}
		}

		@Override
		public void addListener(ILabelProviderListener listener) {
			// No need for this.
		}

		@Override
		public void dispose() {
			// No need for this.
		}

		@Override
		public boolean isLabelProperty(Object element, String property) {
			return false;
		}

		@Override
		public void removeListener(ILabelProviderListener listener) {
			// No need for this.
		}

		@Override
		public Image getImage(Object element) {
			return null;
		}
	}

	private static final int COL_WIDTH = 60;

	private final List<Line> variables = new ArrayList<>();
	private final Map<String, Line> variableLookup = new HashMap<>();
	private TableViewer viewer = null;

	private PvInputsBinding bndInputs;
	private PvParamsBinding bndParams;
	private PvValueBindingBinding bndValueBindings;
	private PvLowerBoundBinding lowerBoundValueBindings;
	private PvUpperBoundBinding upperBoundValueBindings;

	private VariableNameFilter varNameFilter;

	public AdvancedVariableSettingsTable(Composite parent, PvDataBinding bindingParent) {
		super(parent, SWT.NONE);

		createParts();

		bndInputs = new PvInputsBinding("inputs", bindingParent);
		bndParams = new PvParamsBinding("params", bindingParent);
		bndValueBindings = new PvValueBindingBinding("bindings", bindingParent);
		lowerBoundValueBindings = new PvLowerBoundBinding("lowerBounds", bindingParent);
		upperBoundValueBindings = new PvUpperBoundBinding("upperBounds", bindingParent);
	}
	
	private Line addVariableLine(String varName) {
		return addVariableLine(varName, "");
	}

	private Line addVariableLine(String varName, String astType) {
		Line line = new Line(varName, astType);
		this.variables.add(line);
		this.variableLookup.put(varName, line);
		return line;
	}

	public void setKnownVariables(List<TypedAstVariable> variableNames) {
		Set<String> oldVarNames = variables.stream().map(it -> it.getVariableName()).collect(Collectors.toSet());
		Set<String> newVarNames = variableNames.stream().map(it -> it.getVariableName()).collect(Collectors.toSet());

		// Remove lines for variables that are not in known variables anymore
		this.variables.removeIf(it -> it.isDefault() && !newVarNames.contains(it.getVariableName()));

		// Add variable lines that were not present
		variableNames.forEach(it -> {
			if (!oldVarNames.contains(it.getVariableName())) {
				addVariableLine(it.getVariableName(), it.getPlcType());
			}
		});

		// Rebuild variableLookup
		variableLookup.clear();
		variables.forEach(it -> variableLookup.put(it.getVariableName(), it));
		
		variableNames.forEach(it -> { 
			if (variableLookup.containsKey(it.getVariableName())) {
				variableLookup.get(it.getVariableName()).astType = it.getPlcType();
			}
		});

		viewer.refresh();
	}

	/**
	 * Sets a filter to be used for the variable settings table.
	 * 
	 * @param filterText
	 *            Filter text to be used. The table will only display variables
	 *            containing the given filter text as fragment. Exact match is
	 *            not required and the comparison is case-insensitive. If the
	 *            parameter is {@code null}, all variables will be shown.
	 */
	public void setVariableNameFilter(String filterText) {
		this.varNameFilter.setFilterText(filterText);
		this.viewer.refresh();
	}

	private void createParts() {
		this.setLayout(new GridLayout(1, false));

		viewer = new TableViewer(this, SWT.FULL_SELECTION | SWT.BORDER | SWT.V_SCROLL);

		createColumns(viewer);

		viewer.setContentProvider(new TableContentProvider());
		viewer.setInput(variables);
		viewer.getTable().setHeaderVisible(true);
		viewer.getTable().setLinesVisible(true);

		// Filters
		this.varNameFilter = new VariableNameFilter();
		viewer.addFilter(varNameFilter);

		GridDataFactory.fillDefaults().grab(true, true).hint(SWT.DEFAULT, 120).applyTo(viewer.getTable());
	}

	private void setDirty() {
		this.bndInputs.setDirty();
		this.bndParams.setDirty();
		this.bndValueBindings.setDirty();
		this.lowerBoundValueBindings.setDirty();
		this.upperBoundValueBindings.setDirty();
	}

	private void createColumns(TableViewer viewer) {
		// Variable name column
		TableViewerColumn colVarName = new TableViewerColumn(viewer, SWT.NONE);
		colVarName.getColumn().setWidth(150);
		viewer.getTable().addControlListener(new ControlAdapter() {
			@Override
			public void controlResized(ControlEvent e) {
				colVarName.getColumn()
						.setWidth(Math.max(COL_WIDTH, viewer.getTable().getSize().x - 4 * (COL_WIDTH + 2) - 25));
			}
		});

		colVarName.getColumn().setText("Variable");
		colVarName.setLabelProvider(new DelegatingStyledCellLabelProvider(new VariableNameStyledLabelProvider()));
		
		// Type column
		TableViewerColumn colAstType = new TableViewerColumn(viewer, SWT.CHECK);
		colAstType.getColumn().setWidth(COL_WIDTH);
		colAstType.getColumn().setText("Type");
		colAstType.getColumn().setResizable(true);
		colAstType.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Line line = (Line) element;
				return line.getAstType();
			}
		});

		// Input column
		TableViewerColumn colIsInput = new TableViewerColumn(viewer, SWT.CHECK);
		colIsInput.getColumn().setWidth(COL_WIDTH);
		colIsInput.getColumn().setText("Input");
		colIsInput.getColumn().setResizable(false);
		colIsInput.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Line line = (Line) element;
				return line.isInput() ? "true" : "";
			}
		});
		colIsInput.setEditingSupport(new IsInputEditingSupport(viewer));

		// Parameter column
		TableViewerColumn colIsParam = new TableViewerColumn(viewer, SWT.CHECK);
		colIsParam.getColumn().setWidth(COL_WIDTH);
		colIsParam.getColumn().setText("Parameter");
		colIsParam.getColumn().setResizable(false);
		colIsParam.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Line line = (Line) element;
				return line.isParameter() ? "true" : "";
			}
		});
		colIsParam.setEditingSupport(new IsParameterEditingSupport(viewer));

		// Value binding column
		TableViewerColumn colAssignedValue = new TableViewerColumn(viewer, SWT.NONE);
		colAssignedValue.getColumn().setWidth(COL_WIDTH);
		colAssignedValue.getColumn().setText("Assigned value");
		colAssignedValue.getColumn().setResizable(true);
		colAssignedValue.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Line line = (Line) element;
				return line.getAssignedValue();
			}
		});
		colAssignedValue.setEditingSupport(new AssignValueEditingSupport(viewer));

		// Lower bound column
		TableViewerColumn colLowerBound = new TableViewerColumn(viewer, SWT.NONE);
		colLowerBound.getColumn().setWidth(COL_WIDTH);
		colLowerBound.getColumn().setText("Bound low");
		colLowerBound.getColumn().setResizable(true);
		colLowerBound.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Line line = (Line) element;
				return line.getLowerBound();
			}
		});
		colLowerBound.setEditingSupport(new LowerBoundEditingSupport(viewer));

		// Upper bound column
		TableViewerColumn colUpperBound = new TableViewerColumn(viewer, SWT.NONE);
		colUpperBound.getColumn().setWidth(COL_WIDTH);
		colUpperBound.getColumn().setText("Bound high");
		colUpperBound.getColumn().setResizable(true);
		colUpperBound.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Line line = (Line) element;
				return line.getUpperBound();
			}
		});
		colUpperBound.setEditingSupport(new UpperBoundEditingSupport(viewer));
	}

	private class AssignValueEditingSupport extends EditingSupport {
		private final TableViewer viewer;
		private final CellEditor editor;

		public AssignValueEditingSupport(TableViewer viewer) {
			super(viewer);
			this.viewer = viewer;
			this.editor = new TextCellEditor(viewer.getTable());
		}

		@Override
		protected CellEditor getCellEditor(Object element) {
			return editor;
		}

		@Override
		protected boolean canEdit(Object element) {
			return true;
		}

		@Override
		protected Object getValue(Object element) {
			return ((Line) element).getAssignedValue();
		}

		@Override
		protected void setValue(Object element, Object value) {
			((Line) element).setAssignedValue(value.toString());
			viewer.update(element, null);
			setDirty();
		}
	}
	
	private class LowerBoundEditingSupport extends EditingSupport {
		private final TableViewer viewer;
		private final CellEditor editor;

		public LowerBoundEditingSupport(TableViewer viewer) {
			super(viewer);
			this.viewer = viewer;
			this.editor = new TextCellEditor(viewer.getTable());
		}

		@Override
		protected CellEditor getCellEditor(Object element) {
			return editor;
		}

		@Override
		protected boolean canEdit(Object element) {
			return true;
		}

		@Override
		protected Object getValue(Object element) {
			return ((Line) element).getLowerBound();
		}

		@Override
		protected void setValue(Object element, Object value) {
			((Line) element).setLowerBound(value.toString());
			viewer.update(element, null);
			setDirty();
		}
	}
	
	
	private class UpperBoundEditingSupport extends EditingSupport {
		private final TableViewer viewer;
		private final CellEditor editor;

		public UpperBoundEditingSupport(TableViewer viewer) {
			super(viewer);
			this.viewer = viewer;
			this.editor = new TextCellEditor(viewer.getTable());
		}

		@Override
		protected CellEditor getCellEditor(Object element) {
			return editor;
		}

		@Override
		protected boolean canEdit(Object element) {
			return true;
		}

		@Override
		protected Object getValue(Object element) {
			return ((Line) element).getUpperBound();
		}

		@Override
		protected void setValue(Object element, Object value) {
			((Line) element).setUpperBound(value.toString());
			viewer.update(element, null);
			setDirty();
		}
	}

	private abstract class BooleanEditingSupport extends EditingSupport {
		private final TableViewer viewer;
		private final CellEditor editor;

		public BooleanEditingSupport(TableViewer viewer) {
			super(viewer);
			this.viewer = viewer;
			this.editor = new CheckboxCellEditor(viewer.getTable(), SWT.CHECK | SWT.READ_ONLY);
		}

		@Override
		protected CellEditor getCellEditor(Object element) {
			return editor;
		}

		@Override
		protected boolean canEdit(Object element) {
			return true;
		}

		@Override
		protected Object getValue(Object element) {
			return getValueSpecific((Line) element);
		}

		@Override
		protected void setValue(Object element, Object value) {
			setValueSpecific((Line) element, Boolean.valueOf(value.toString()));
			viewer.update(element, null);
			setDirty();
		}

		protected abstract boolean getValueSpecific(Line line);

		protected abstract void setValueSpecific(Line line, boolean newVal);
	}

	private class IsInputEditingSupport extends BooleanEditingSupport {
		public IsInputEditingSupport(TableViewer viewer) {
			super(viewer);
		}

		@Override
		protected boolean getValueSpecific(Line line) {
			return line.isInput();
		}

		@Override
		protected void setValueSpecific(Line line, boolean newVal) {
			line.setInput(newVal);
		}
	}

	private class IsParameterEditingSupport extends BooleanEditingSupport {
		public IsParameterEditingSupport(TableViewer viewer) {
			super(viewer);
		}

		@Override
		protected boolean getValueSpecific(Line line) {
			return line.isParameter();
		}

		@Override
		protected void setValueSpecific(Line line, boolean newVal) {
			line.setParameter(newVal);
		}
	}

}
