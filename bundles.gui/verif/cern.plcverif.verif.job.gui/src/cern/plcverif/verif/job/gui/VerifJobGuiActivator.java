/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.job.gui;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

import com.google.common.base.Preconditions;

import cern.plcverif.verif.job.gui.decorators.VerifJobDecoratorConstants;

public class VerifJobGuiActivator extends AbstractUIPlugin {
	public static final String PLUGIN_ID = "cern.plcverif.verif.job.gui";
	
	private static VerifJobGuiActivator plugin;

	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this; // Not so nice, but typical workaround.
	}
	
	@Override
	protected void initializeImageRegistry(ImageRegistry reg) {
		super.initializeImageRegistry(reg);
		
		putNewImage(reg, VerifJobDecoratorConstants.SATISFIED_DECORATOR_ICON_ID, "icons/icon_accept_8x8.gif");
		putNewImage(reg, VerifJobDecoratorConstants.VIOLATED_DECORATOR_ICON_ID, "icons/icon_stop_8x8.gif");
	}
	
	private void putNewImage(ImageRegistry reg, String imageID, String relativePath) {
		Bundle bundle = Platform.getBundle(PLUGIN_ID);
		reg.put(imageID, ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path(relativePath), null)));
	}

	public static ImageDescriptor getImageDescriptorByID(String imageID) {
		Preconditions.checkState(plugin != null, "Reference to this plugin is not found (plugin=null). Unable to fetch the image.");
		return ImageDescriptor.createFromImage(plugin.getImageRegistry().get(imageID));
	}
}
