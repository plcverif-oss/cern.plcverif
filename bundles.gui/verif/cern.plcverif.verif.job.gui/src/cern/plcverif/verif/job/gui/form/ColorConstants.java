/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.job.gui.form;

import org.eclipse.swt.graphics.RGB;

final class ColorConstants {
	public static final RGB SATISFIED_RESULT_COLOR = new RGB(34, 177, 76);
	public static final RGB VIOLATED_RESULT_COLOR = new RGB(255, 0, 0);
	public static final RGB UNKNOWN_RESULT_COLOR = new RGB(128, 128, 128);
	
	private ColorConstants() {
		// Utility class.
	}
}
