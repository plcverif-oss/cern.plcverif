/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.job.gui.form.impl;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.settings.SettingsSerializer;

public class AdditionalSettingsTable extends Composite implements IDirty {
	public static class Line {
		private String key = "...";
		private String value = "";

		public Line() {
		}

		public Line(String key, String value) {
			this.key = key;
			this.value = value;
		}
	}

	private static class TableContentProvider implements IStructuredContentProvider {
		@Override
		public Object[] getElements(Object inputElement) {
			Preconditions.checkArgument(inputElement instanceof List<?>);
			return ((List<?>) inputElement).toArray();
		}
	}

	private static final int DEFAULT_COL_WIDTH = 150;

	private final List<Line> additionSettingsLines = new ArrayList<>();
	private TableViewer viewer = null;

	// dirty state handling
	private boolean dirty = false;
	private Runnable onDirtyListener = () -> {
	};

	public AdditionalSettingsTable(Composite parent) {
		super(parent, SWT.NONE);

		createParts();
	}

	private void createParts() {
		this.setLayout(new GridLayout(2, false));

		viewer = new TableViewer(this, SWT.FULL_SELECTION | SWT.BORDER | SWT.V_SCROLL);

		createColumns(viewer);

		viewer.setContentProvider(new TableContentProvider());
		viewer.setInput(additionSettingsLines);
		viewer.getTable().setHeaderVisible(true);
		viewer.getTable().setLinesVisible(true);
		GridDataFactory.fillDefaults().grab(true, true).hint(SWT.DEFAULT, 120).span(1, 4).applyTo(viewer.getTable());

		Button btnAdd = new Button(this, SWT.PUSH);
		btnAdd.setText("Add new line");
		btnAdd.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				additionSettingsLines.add(new Line());
				viewer.refresh();
			}
		});
		GridDataFactory.fillDefaults().applyTo(btnAdd);
		
		Button btnAddWizard = new Button(this, SWT.PUSH);
		btnAddWizard.setText("Add...");
		btnAddWizard.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				AdditionalSettingDialog dialog = new AdditionalSettingDialog(getShell());
				dialog.create();
				if (dialog.open() == org.eclipse.jface.window.Window.OK && dialog.getLine().isPresent()) {
					additionSettingsLines.add(dialog.getLine().get());
					viewer.refresh();
				}
			}
		});
		GridDataFactory.fillDefaults().applyTo(btnAddWizard);

		Button btnRemove = new Button(this, SWT.PUSH);
		btnRemove.setText("Remove selected");
		btnRemove.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Line selectedLine = getSelectedLine();
				if (selectedLine != null) {
					additionSettingsLines.remove(selectedLine);
					setDirty();
				}
				viewer.refresh();
			}
		});
		GridDataFactory.fillDefaults().grab(false, false).applyTo(btnRemove);
	}

	/**
	 * Adds a new line to the table of additional settings.
	 * 
	 * @param key
	 *            Key to be added. Shall not be {@code null}.
	 * @param value
	 *            Value to be added. If null, it will be replaced with the empty
	 *            string,.
	 */
	public void addLine(String key, String value) {
		Line line = new Line();
		line.key = Preconditions.checkNotNull(key);
		line.value = value == null ? "" : value;
		additionSettingsLines.add(line);
		viewer.refresh();
		onDirtyListener.run();
	}
	
	/**
	 * Returns the number of lines with additional settings in this table.
	 * @return Number of lines
	 */
	public int getLineCount() {
		return additionSettingsLines.size();
	}

	/**
	 * Returns the additional settings contained by the table. The result is in
	 * the settings file format, i.e., one setting (key-value pair) is present
	 * per line.
	 * 
	 * @return Contents serialized
	 */
	public String getContents() {
		StringBuilder ret = new StringBuilder();
		for (Line line : additionSettingsLines) {
			ret.append("-");
			ret.append(line.key);
			ret.append(" = ");
			ret.append(valueWithQuotesIfNeeded(line.value));
			ret.append(System.lineSeparator());
		}
		return ret.toString();
	}
	
	@Override
	public boolean isDirty() {
		return dirty;
	}

	@Override
	public void clearDirty() {
		this.dirty = false;
		onDirtyListener.run();
	}

	protected void setDirty() {
		this.dirty = true;
		onDirtyListener.run();
	}

	@Override
	public void setOnDirtyListener(Runnable listener) {
		this.onDirtyListener = Preconditions.checkNotNull(listener);
	}
	
	private static String valueWithQuotesIfNeeded(String value) {
		String sanitizedValue = SettingsSerializer.sanitize(value);
		if (sanitizedValue == null || sanitizedValue.isEmpty()) {
			return "\"\"";
		} else if (sanitizedValue.startsWith("{") && sanitizedValue.endsWith("}")) {
			return sanitizedValue;
		} else {
			return String.format("\"%s\"", sanitizedValue);
		}
	}

	private Line getSelectedLine() {
		IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
		if (selection.getFirstElement() instanceof Line) {
			return (Line) selection.getFirstElement();
		}
		return null;
	}

	private void createColumns(TableViewer viewer) {
		// Key column
		TableViewerColumn colKey = new TableViewerColumn(viewer, SWT.NONE);
		colKey.getColumn().setWidth(DEFAULT_COL_WIDTH);

		colKey.getColumn().setText("Key");
		colKey.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Line line = (Line) element;
				return "-" + line.key;
			}
		});
		colKey.setEditingSupport(new KeyEditingSupport(viewer));

		// Value column
		TableViewerColumn colValue = new TableViewerColumn(viewer, SWT.CHECK);
		colValue.getColumn().setWidth(DEFAULT_COL_WIDTH);
		colValue.getColumn().setText("Value");
		colValue.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Line line = (Line) element;
				return valueWithQuotesIfNeeded(line.value);
			}
		});
		colValue.setEditingSupport(new ValueEditingSupport(viewer));
	}

	private class KeyEditingSupport extends EditingSupport {
		private final TableViewer viewer;
		private final CellEditor editor;

		public KeyEditingSupport(TableViewer viewer) {
			super(viewer);
			this.viewer = viewer;
			this.editor = new TextCellEditor(viewer.getTable());
		}

		@Override
		protected CellEditor getCellEditor(Object element) {
			return editor;
		}

		@Override
		protected boolean canEdit(Object element) {
			return true;
		}

		@Override
		protected Object getValue(Object element) {
			return ((Line) element).key;
		}

		@Override
		protected void setValue(Object element, Object value) {
			((Line) element).key = value.toString();
			viewer.update(element, null);
			setDirty();
		}
	}

	private class ValueEditingSupport extends EditingSupport {
		private final TableViewer viewer;
		private final CellEditor editor;

		public ValueEditingSupport(TableViewer viewer) {
			super(viewer);
			this.viewer = viewer;
			this.editor = new TextCellEditor(viewer.getTable());
		}

		@Override
		protected CellEditor getCellEditor(Object element) {
			return editor;
		}

		@Override
		protected boolean canEdit(Object element) {
			return true;
		}

		@Override
		protected Object getValue(Object element) {
			return ((Line) element).value;
		}

		@Override
		protected void setValue(Object element, Object valueToSet) {
			if (element instanceof Line) {
				Line line = (Line) element;
				line.value = valueToSet.toString();
				this.viewer.update(element, null);
				setDirty();
			}
		}
	}

}
