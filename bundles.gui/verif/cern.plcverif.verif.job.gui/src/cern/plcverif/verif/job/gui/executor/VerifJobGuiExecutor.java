/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.job.gui.executor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Semaphore;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.logging.PlatformLogger;
import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsKind;
import cern.plcverif.base.common.settings.SettingsList;
import cern.plcverif.base.common.settings.SettingsSerializer;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.gui.eclipse.EclipseIoUtils;
import cern.plcverif.base.gui.preferences.PlcverifPreferenceAccess;
import cern.plcverif.base.interfaces.IBasicJob;
import cern.plcverif.base.interfaces.data.CfaJobSettings;
import cern.plcverif.base.interfaces.data.PlatformConfig;
import cern.plcverif.base.interfaces.data.result.OutputFileToSave;
import cern.plcverif.base.platform.Platform;
import cern.plcverif.library.reporter.html.individual.HtmlReporter;
import cern.plcverif.library.reporter.html.individual.HtmlReporterExtension;
import cern.plcverif.library.reporter.summary.SummaryReporter;
import cern.plcverif.library.reporter.summary.SummaryReporterExtension;
import cern.plcverif.verif.interfaces.IVerificationReporter;
import cern.plcverif.verif.interfaces.data.VerificationResult;
import cern.plcverif.verif.job.VerificationJob;
import cern.plcverif.verif.job.VerificationJobExtension;
import cern.plcverif.verif.job.gui.form.EclipseProgressReporter;

public class VerifJobGuiExecutor {
	private static final int MAX_CONCURRENT_MODEL_CHECKER_EXECUTIONS = 2;
	private static final Semaphore VERIF_JOB_SEMAPHORE = new Semaphore(MAX_CONCURRENT_MODEL_CHECKER_EXECUTIONS);
	
	private static final PlatformConfig PLATFORM_CONFIG = new PlatformConfig(Paths.get("."));

	private HtmlReporterExtension htmlReporterExtension = new HtmlReporterExtension();
	private SummaryReporterExtension summaryReporterExtension = new SummaryReporterExtension();
	private SummaryReporter summaryReporter;

	/**
	 * Function (on a functional interface) that is going to be called once the
	 * verification job execution is finished.
	 */
	@FunctionalInterface
	public static interface VerifJobExecutionFinisher {
		/**
		 * Function to be called once the execution of the verification job has 
		 * been finished.
		 * 
		 * It is not guaranteed that the function is called on the GUI thread!
		 * 
		 * @param job
		 *            The job which calls the listener. May be
		 *            {@link Optional#empty()} if the creation of the job has
		 *            failed.
		 * @param summary
		 *            The root settings node of the verification summary
		 */
		void onExecutionFinished(Optional<ExecutionJob> job, Optional<SettingsElement> summary);
	}

	/**
	 * Returns the platform configuration that is used for GUI verification jobs.
	 * @return Platform configuration
	 */
	public static PlatformConfig getPlatformConfig() {
		return PLATFORM_CONFIG;
	}
	
	/**
	 * Executes the given verification job.
	 * 
	 * @param settingsString
	 *            Textual representation of verification job settings, in settings
	 *            file format (key-value pairs separated by line breaks)
	 * @param project
	 * @param monitor
	 * @param onFinish
	 *            Method or functional interface to be executed once the job
	 *            finishes. It is not guaranteed to be called from the GUI
	 *            thread.
	 * @return The job that has been created. It is already scheduled, does not
	 *         need scheduling.
	 * @throws SettingsParserException
	 * @throws IOException
	 */
	public Job execute(String settingsString, IProject project, IProgressMonitor monitor,
			VerifJobExecutionFinisher onFinish) throws SettingsParserException, IOException {
		VerificationJob verifJob;
		IFolder artifactFolder;

		try {
			SettingsElement jobSettings = SettingsSerializer.parseSettingsFromFile(settingsString);

			// Make source file paths absolute (if not yet)
			makeSourcePathsAbsolute(jobSettings.getAttribute(CfaJobSettings.SOURCE_FILES), project);

			Platform platform = Platform.createPlatform(PLATFORM_CONFIG);

			IBasicJob job = platform.parseJob(jobSettings);
			Preconditions.checkState(job instanceof VerificationJob, "");
			verifJob = (VerificationJob) job;

			// Additional settings
			verifJob.setOutputDirectory(Files.createTempDirectory("PLCverif-output"));
			addConfiguredHtmlReporter(verifJob.getReporters());
			addConfiguredSummaryReporter(verifJob.getReporters());

			// Create artifact folder
			artifactFolder = createArtifactFolder(project, monitor);
		} catch (SettingsParserException | IOException e) {
			onFinish.onExecutionFinished(null, Optional.empty());
			throw e;
		}

		// Execution
		Job backgroundExecution = new ExecutionJob("Verification job", verifJob, artifactFolder);
		backgroundExecution.addJobChangeListener(new JobChangeAdapter() {
			@Override
			public void done(IJobChangeEvent event) {
				super.done(event);

				ExecutionJob job = null;
				SettingsElement summaryMemento = null;

				try {
					// Fetch and pass data
					job = (ExecutionJob) event.getJob();
					Optional<VerificationResult> verificationResult = job.getVerifResult();
					Preconditions.checkState(verificationResult.isPresent(),
							"Illegal state: the execution job has finished, but there is no available result.");

					try {
						summaryMemento = SettingsSerializer.parseSettingsFromFile(summaryReporter.getResult());
					} catch (SettingsParserException e) {
						PlatformLogger.logError(
								"Error while parsing the summary memento after GUI verification job execution.", e);
						summaryMemento = null;
					}

					// Remove listener
					backgroundExecution.removeJobChangeListener(this);
				} finally {
					// No matter what happens, call the listener
					onFinish.onExecutionFinished(Optional.ofNullable(job), Optional.ofNullable(summaryMemento));
				}
			}
		});

		backgroundExecution.schedule();
		return backgroundExecution;
	}

	private void addConfiguredHtmlReporter(List<IVerificationReporter> reporters) {
		try {
			// HTML reporter
			// As we are not setting them via the settings, we need to manually
			// configure the reporters with the installation settings
			Optional<SettingsElement> htmlInstallationSettings = SettingsSerializer.loadInstallationPluginSettings(
					HtmlReporterExtension.CMD_ID, PLATFORM_CONFIG.getProgramSettingsDirectory());
			IVerificationReporter htmlReporter;
			if (htmlInstallationSettings.isPresent()) {
				htmlReporter = htmlReporterExtension.createReporter(htmlInstallationSettings.get());
			} else {
				htmlReporter = htmlReporterExtension.createReporter();
			}
			
			// Try to find existing HTML reporter. It will be overwritten if found. 
			int idx = -1;
			for (int i = 0; i < reporters.size(); i++) {
				if (reporters.get(i) instanceof HtmlReporter) {
					idx = i;
					break;
				}
			}
				
			if (idx >= 0) {
				reporters.set(idx, htmlReporter);
			} else {
				reporters.add(htmlReporter);
			}
		} catch (SettingsParserException e) {
			PlatformLogger.logError("Unable to create HTML verification reporter.", e);
		}
	}
	
	private void addConfiguredSummaryReporter(List<IVerificationReporter> reporters) {
		try {
			// Summary reporter
			// As we are not setting them via the settings, we need to manually
			// configure the reporters with the installation settings
			// (This will be used to get the short overview of the verification result)
			Optional<SettingsElement> summaryInstallationSettings = SettingsSerializer.loadInstallationPluginSettings(
					SummaryReporterExtension.CMD_ID, PLATFORM_CONFIG.getProgramSettingsDirectory());
			if (summaryInstallationSettings.isPresent()) {
				this.summaryReporter = summaryReporterExtension.createReporter(summaryInstallationSettings.get());
			} else {
				this.summaryReporter = summaryReporterExtension.createReporter();
			}
			
			// Try to find existing summary reporter. It will be overwritten if found. 
			int idx = -1;
			for (int i = 0; i < reporters.size(); i++) {
				if (reporters.get(i) instanceof SummaryReporter) {
					idx = i;
					break;
				}
			}
				
			if (idx >= 0) {
				reporters.set(idx, summaryReporter);
			} else {
				reporters.add(summaryReporter);
			}
		} catch (SettingsParserException e) {
			PlatformLogger.logError("Unable to create summary verification reporter.", e);
		}
	}

	private static void makeSourcePathsAbsolute(Optional<Settings> sourceFilesNode, IProject parentProject)
			throws SettingsParserException {
		if (sourceFilesNode.isPresent()) {
			SettingsList sourceFiles = sourceFilesNode.get().toList();
			
			for (Settings srcFile : new ArrayList<>(sourceFiles.elements())) {
				if (srcFile.kind() == SettingsKind.ELEMENT) {
					
					String srcFileItemString = srcFile.toSingle().value();
					if (srcFileItemString.contains("*")) {
						// Contains wildcard 
						for (IProject project : EclipseIoUtils.getProjectAndItsReferences(parentProject)) {
							String absolute = project.getLocation().append(new Path(srcFileItemString)).toOSString();
							
							SettingsElement newElement = SettingsElement.empty();
							newElement.setValue(absolute);
							sourceFiles.add(newElement);
						}
						
						// Remove previous
						sourceFiles.removeElement(srcFile);
					} else {
						// No wildcard
						String absolute = parentProject.getLocation().append(new Path(srcFileItemString)).toOSString();
						srcFile.toSingle().setValue(absolute);
					}
				}
			}
		}
	}

	private static IFolder createArtifactFolder(IProject parentProject, IProgressMonitor monitor)
			throws IOException {
		return PlcverifPreferenceAccess.getAndCreateOutputFolder(parentProject, monitor);
	}

	public static class ExecutionJob extends Job {
		private VerificationJob verifJob;
		private IFolder artifactFolder;
		private VerificationResult verifResult = null;
		private IFile htmlReport = null;

		public ExecutionJob(String name, VerificationJob verifJob, IFolder artifactFolder) {
			super(name);
			this.verifJob = verifJob;
			this.artifactFolder = artifactFolder;
		}

		@Override
		protected synchronized IStatus run(IProgressMonitor monitor) {
			// Execute job
			try {
				monitor.subTask("Waiting for free slot to start the verification job...");
				VERIF_JOB_SEMAPHORE.acquireUninterruptibly();
				this.verifResult = verifJob.execute(new EclipseProgressReporter(monitor, "PLC code verification"));
			} finally {
				VERIF_JOB_SEMAPHORE.release();
			}

			// Write outputs
			try {
				NeverCanceledMonitorWrapper wrappedMonitor = new NeverCanceledMonitorWrapper(monitor);
				for (OutputFileToSave output : verifResult.getOutputFilesToSave()) {
					IFile outFile = artifactFolder.getFile(output.getFilename());
					// Even if the task is cancelled, save the artifacts!
					EclipseIoUtils.setContents(outFile, output.getContent(), wrappedMonitor);

					if (outFile.getName().endsWith(HtmlReporter.HTML_REPORT_FILE_SUFFIX)) {
						htmlReport = outFile;
					}
				}

				return monitor.isCanceled() ? Status.CANCEL_STATUS : Status.OK_STATUS;
			} catch (IOException e) {
				PlatformLogger.logError("Unable to write outputs of a verification job to the workspace. " + e.getMessage(), e);
				return Status.CANCEL_STATUS;
			}
		}

		public synchronized Optional<VerificationResult> getVerifResult() {
			return Optional.ofNullable(verifResult);
		}

		public synchronized Optional<IFile> getHtmlReportFile() {
			return Optional.ofNullable(htmlReport);
		}
	}

	/**
	 * Returns true iff the given file content looks like a verification job
	 * settings file. This method will do text-based check to look for a line
	 * similar to {@code job} = {@code verif} ({@link CfaJobSettings#JOB} =
	 * {@link VerificationJobExtension#CMD_ID})
	 * 
	 * @param fileContent
	 *            Content of the settings file to check
	 * @return True iff the settings file seems to be a verification job
	 *         descriptor
	 */
	public static boolean isVerifJobSettingsFile(String fileContent) {
		return fileContent.matches(
				String.format("(?s).*-%s\\s*=\\s*(\"?)%s.*", CfaJobSettings.JOB, VerificationJobExtension.CMD_ID));
	}

	private static class NeverCanceledMonitorWrapper implements IProgressMonitor {
		private IProgressMonitor wrappedMonitor;

		public NeverCanceledMonitorWrapper(IProgressMonitor wrappedMonitor) {
			this.wrappedMonitor = Preconditions.checkNotNull(wrappedMonitor);
		}

		@Override
		public void beginTask(String name, int totalWork) {
			wrappedMonitor.beginTask(name, totalWork);
		}

		@Override
		public void done() {
			wrappedMonitor.done();
		}

		@Override
		public void internalWorked(double work) {
			wrappedMonitor.internalWorked(work);
		}

		@Override
		public boolean isCanceled() {
			return false;
		}

		@Override
		public void setCanceled(boolean value) {
			wrappedMonitor.setCanceled(value);
		}

		@Override
		public void setTaskName(String name) {
			wrappedMonitor.setTaskName(name);
		}

		@Override
		public void subTask(String name) {
			wrappedMonitor.subTask(name);
		}

		@Override
		public void worked(int work) {
			wrappedMonitor.worked(work);
		}
	}
}
