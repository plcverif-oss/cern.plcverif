/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.job.gui.form;

import static cern.plcverif.base.common.settings.SettingsUtils.toFqn;
import static cern.plcverif.base.gui.eclipse.EclipseIoUtils.getFiles;
import static cern.plcverif.base.gui.eclipse.EclipseIoUtils.getProjectAndItsReferences;
import static cern.plcverif.base.gui.layout.PvLayoutUtils.createLabeledRow;
import static cern.plcverif.base.gui.layout.PvLayoutUtils.fullRowLayout;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.forms.widgets.ColumnLayout;
import org.eclipse.ui.forms.widgets.ColumnLayoutData;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.part.EditorPart;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.extension.ExtensionHelper;
import cern.plcverif.base.common.logging.PlatformLogger;
import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsList;
import cern.plcverif.base.common.settings.SettingsSerializer;
import cern.plcverif.base.common.settings.SettingsUtils;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.gui.binding.IKeyLabelPair;
import cern.plcverif.base.gui.binding.PvDataBinding;
import cern.plcverif.base.gui.binding.PvDataBindingGroup;
import cern.plcverif.base.gui.binding.PvStringTextBinding;
import cern.plcverif.base.gui.binding.PvTextBinding;
import cern.plcverif.base.gui.binding.exceptions.DataInvalidException;
import cern.plcverif.base.gui.binding.swt.PvCheckboxListBinding;
import cern.plcverif.base.gui.binding.swt.PvCheckboxListDeferredBinding;
import cern.plcverif.base.gui.binding.swt.PvComboboxBinding;
import cern.plcverif.base.gui.binding.swt.PvTextboxBinding;
import cern.plcverif.base.gui.component.IPvGuiPart;
import cern.plcverif.base.gui.eclipse.EclipseIoUtils;
import cern.plcverif.base.gui.layout.PvLayoutUtils;
import cern.plcverif.base.gui.layout.StackManager;
import cern.plcverif.base.gui.preferences.PlcverifPreferenceAccess;
import cern.plcverif.base.gui.swt.CheckboxList;
import cern.plcverif.base.gui.swt.FileListSelection;
import cern.plcverif.base.interfaces.IParserExtension;
import cern.plcverif.base.interfaces.data.CfaJobSettings;
import cern.plcverif.verif.extensions.gui.IVerifGuiPartExtension;
import cern.plcverif.verif.extensions.gui.context.VerificationCaseContext;
import cern.plcverif.verif.interfaces.IToolAgentExtension;
import cern.plcverif.verif.interfaces.IVerificationReporterExtension;
import cern.plcverif.verif.job.VerificationJobExtension;
import cern.plcverif.verif.job.VerificationJobOptions;
import cern.plcverif.verif.job.gui.executor.VerifJobGuiExecutor;
import cern.plcverif.verif.job.gui.form.VerifCaseEditorExtensions.VerifCaseEditorExtension;
import cern.plcverif.verif.job.gui.form.impl.AdditionalSettingsTable;
import cern.plcverif.verif.job.gui.form.impl.AdvancedVariableSettingsTable;

public final class VerifCaseEditor extends EditorPart {
	public static final String ID = "cern.plcverif.verif.job.gui.form";

	// TODO_LOWPRI This should be unified with
	// cern.plcverif.gui.frontend.wizards.NewPlcverifProjectWizardPage.BUILTIN_PROJECT_PREFIX
	private static final String BUILTIN_PROJECT_PREFIX = ".builtin_";

	/**
	 * Set of extensions to be ignored from the input file selection. All item
	 * should be lower case and should not be preceded by dot.
	 */
	private static final HashSet<String> EXTENSIONS_TO_IGNORE = new HashSet<>(Arrays.asList("vc", "vc3", "txt", "log"));

	private VerifCaseEditorExtensions guiPartExtensions = new VerifCaseEditorExtensions();

	private IFile inputFile = null;

	// Common GUI
	private FormToolkit toolkit = null;
	private ScrolledForm form = null;
	private ResourceManager resManager = null;

	// GUI components that need direct access
	private Text tbDiag = null;
	private AdditionalSettingsTable additionalSettingsTable = null;

	// Bindings
	private PvDataBinding rootSettingsBinding = null;
	private PvTextBinding bndJob = null;
	private PvTextBinding bndId = null;
	private PvComboboxBinding bndRequirement = null;
	private VerificationCaseContext context = null;

	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		if (!(input instanceof IFileEditorInput)) {
			throw new PartInitException("Unexpected input type");
		} else {
			inputFile = ((IFileEditorInput) input).getFile();
			context = new VerificationCaseContext(inputFile);
		}

		setSite(site);
		setInput(input);
		setPartName(String.format("%s (verification case)", inputFile.getName()));

		rootSettingsBinding = PvDataBindingGroup.createRoot();
		bndJob = new PvStringTextBinding(CfaJobSettings.JOB, rootSettingsBinding, false);
		bndJob.setValue(VerificationJobExtension.CMD_ID);

		// Contents will only be loaded once the `createPartControl` has been
		// executed!

		// Set dirty listener
		rootSettingsBinding.addOnDirtyPropertyChange(e -> firePropertyChange(IEditorPart.PROP_DIRTY));
		rootSettingsBinding.addOnDirtyPropertyChange(e -> updateDiagnosticContent());
	}

	private boolean load() {
		try {
			// Load file contents
			List<String> content = EclipseIoUtils.loadAllLines(inputFile);

			// Load using data binding
			Settings loadedSettings = SettingsSerializer.parseSettingsFromFile(content);
			rootSettingsBinding.load(loadedSettings);
			rootSettingsBinding.clearDirty(); // just in case

			// If the ID is not filled after loading, put the file name there
			if (bndId.getValue().isEmpty()) {
				bndId.setValue(EclipseIoUtils.getFileNameWithoutExtension(inputFile));
			}

			// Load the rest to the 'additionalSettingsTable'
			for (SettingsElement nonconsumed : SettingsUtils.nonConsumedSettingsElements(loadedSettings)) {
				if (nonconsumed.getParent() != null && nonconsumed.fqn() != null) {
					additionalSettingsTable.addLine(nonconsumed.fqn(), nonconsumed.valueWithoutConsuming());
				}
			}

			// Load empty lists to the 'additionalSettingsTable' too
			for (Settings emptySettingsList : SettingsUtils.collectAllSettings(loadedSettings,
					it -> it instanceof SettingsList && ((SettingsList) it).isEmpty())) {
				additionalSettingsTable.addLine(emptySettingsList.fqn(), "{}");
			}

			updateDiagnosticContent();
			checkId();

			return true;
		} catch (SettingsParserException e) {
			showErrorMessage(null, String.format("Unable to parse the settings file '%s'.", inputFile.getName()), e);
		} catch (IOException e) {
			showErrorMessage(null,
					String.format("Unable to load the settings file '%s' (I/O error).", inputFile.getName()), e);
		} catch (DataInvalidException e) {
			String originAttribute = e.getOrigin() == null ? "unknwon" : e.getOrigin().getSettingsAttribute();
			showErrorMessage(null, String.format("Unable to load the settings file '%s', invalid data found (%s).",
					inputFile.getName(), originAttribute), e);
		}

		return false;
	}

	String getContentsAsSettingsFile() throws DataInvalidException {
		Optional<Settings> savedSettings = rootSettingsBinding.save();
		Preconditions.checkState(savedSettings.isPresent() && savedSettings.get() instanceof SettingsElement);

		StringBuilder contentToSave = new StringBuilder();
		contentToSave.append("// Verification case created by PLCverif3 GUI.");
		contentToSave.append(System.lineSeparator());
		contentToSave.append(SettingsSerializer.serialize((SettingsElement) savedSettings.get(), false));
		contentToSave.append(System.lineSeparator());
		contentToSave.append(additionalSettingsTable.getContents());

		return contentToSave.toString();
	}

	IFile getInputFile() {
		return inputFile;
	}

	PvDataBinding getRootSettingsBinding() {
		return rootSettingsBinding;
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		String contentToSave = null;
		try {
			contentToSave = getContentsAsSettingsFile();
		} catch (DataInvalidException e) {
			showErrorMessage(null, String.format("Unable to serialize the settings file '%s'.", inputFile.getName()),
					e);
			return; // There is no point in continuing
		}

		// Serialize settings to text
		try {
			Optional<Settings> savedSettings = rootSettingsBinding.save();
			Preconditions.checkState(savedSettings.isPresent() && savedSettings.get() instanceof SettingsElement);
			contentToSave = SettingsSerializer.serialize((SettingsElement) savedSettings.get(), false);
			contentToSave += System.lineSeparator() + additionalSettingsTable.getContents();
		} catch (DataInvalidException e) {
			showErrorMessage("Unable to save",
					"Unable to save the current verification case. Correct the errors on the form and try again.", e);
			return; // unsuccessful
		}

		checkValitidyAndShowError(contentToSave);

		// Save text to file
		try (InputStream stream = new ByteArrayInputStream(contentToSave.getBytes(StandardCharsets.UTF_8))) {
			inputFile.setCharset("utf8", monitor);
			inputFile.setContents(stream, IResource.KEEP_HISTORY, monitor);
		} catch (CoreException | IOException e) {
			showErrorMessage("Unable to save", "Unable to save the current verification case (I/O error).", e);
			return;
		}

		// Not dirty anymore
		clearDirty();
	}

	/**
	 * Checks whether the given string is a valid settings file content and can
	 * be parsed by {@link SettingsSerializer}. If it cannot be parsed, error
	 * message is shown to the user.
	 * 
	 * @param settingsFileContent
	 *            Content of the settings file
	 * @return True if the given settings file content is valid
	 */
	private boolean checkValitidyAndShowError(String settingsFileContent) {
		List<String> settingsFileLines = Arrays.asList(settingsFileContent.split("\r?\n"));
		try {
			SettingsSerializer.parseSettingsFromFile(settingsFileLines);
		} catch (SettingsParserException e) {
			Status status = new Status(IStatus.ERROR, "cern.plcverif.verif.job.gui", e.getMessage(), e);
			// show error dialog
			ErrorDialog.openError(form.getShell(), "Error",
					"Unable to save correctly, the result is corrupted and cannot be read back.", status);
			return false;
		}
		return true;
	}

	private void checkId() {
		if (!PlcverifPreferenceAccess.isStrictIdCheck()) {
			// ID checking is disabled
			return;
		}

		String loadedId = bndId.getValue();
		String fileId = EclipseIoUtils.getFileNameWithoutExtension(inputFile);

		if (loadedId != null && loadedId.equals(fileId)) {
			// nothing to do, matching ID
			return;
		}

		Shell shell = Display.getCurrent().getActiveShell();
		boolean dialogResult = MessageDialog.openQuestion(shell, "Strict ID check",
				String.format("This verification case file is named '%s' but the ID set in it is '%s'. "
						+ "PLCverif will use the latter to name the output files. This mismatch may cause confusion or may overwrite "
						+ "the output of other verification cases. You can disable this warning in the preferences.%n%n"
						+ "Would you like to update the ID in the verification case file to '%s'?", inputFile.getName(),
						loadedId, fileId));

		if (dialogResult) {
			bndId.setValue(fileId);
		}
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public void doSaveAs() {
		// Not required as Save As is not allowed
	}

	@Override
	public boolean isDirty() {
		boolean isDirty = rootSettingsBinding.isDirty() || additionalSettingsTable.isDirty();
		updateDiagnosticContent();
		return isDirty;
	}

	private void clearDirty() {
		rootSettingsBinding.clearDirty();
		additionalSettingsTable.clearDirty();
		firePropertyChange(IEditorPart.PROP_DIRTY);
	}

	@Override
	public void setFocus() {
		form.setFocus();
	}

	// GUI construction
	@Override
	public void createPartControl(Composite parent) {
		toolkit = new FormToolkit(parent.getDisplay());

		// Create form
		form = createForm(parent);
		resManager = new LocalResourceManager(JFaceResources.getResources(), form.getBody());

		// Create sections
		createSectionControls(form.getBody());

		// Layout
		parent.requestLayout();

		// Load contents
		boolean success = load();
		if (!success) {
			form.setEnabled(false);
			form.setVisible(false);
			clearDirty();
		}

		parent.requestLayout();
		form.reflow(true);
		setFocus();
	}

	private ScrolledForm createForm(Composite parent) {
		ScrolledForm createdForm = toolkit.createScrolledForm(parent);

		// Layout
		ColumnLayout colLayout = new ColumnLayout();
		colLayout.minNumColumns = 1;
		colLayout.maxNumColumns = 2;
		colLayout.horizontalSpacing = 20;
		createdForm.getBody().setLayout(colLayout);

		// Title
		createdForm.setText("Verification case");

		return createdForm;
	}

	private void createSectionControls(Composite parent) {
		Section secMetadata = createSection(parent, "Metadata", "General description of the verification case.");
		createMetadataGui((Composite) secMetadata.getClient(), rootSettingsBinding);

		Section secInput = createSection(parent, "Source files",
				"Here the scope of the verification (i.e., the included source files) needs to be selected.");
		createInputGui((Composite) secInput.getClient(), rootSettingsBinding);

		Section secBackend = createSection(parent, "Verification backend",
				"Selection and configuration of the external verification tool to be used.");
		createBackendGui((Composite) secBackend.getClient(), bndJob);

		Section secRequirement = createSection(parent, "Requirement", "Description of the requirement to be verified.");
		createRequirementGui((Composite) secRequirement.getClient(), bndJob);

		Section secReqAdvanced = createSection(parent, "Requirement \u2013 advanced",
				"Advanced settings for the variables.");
		secReqAdvanced.setExpanded(false);
		createVariableSettingsGui((Composite) secReqAdvanced.getClient(), bndRequirement);

		Section secReporters = createSection(parent, "Reporters",
				"Verification reporters to be used to give feedback about the result of the execution.");
		secReporters.setExpanded(false);
		createReportersGui((Composite) secReporters.getClient(), bndJob);
		
		Section secToolAgents = createSection(parent, "Misc. Tools",
				"Other misc. tools that may be applied during the verification run.");
		secToolAgents.setExpanded(false);
		createMiscToolsGui((Composite) secToolAgents.getClient(), bndJob);

		Section secAdditionalSettings = createSection(parent, "Advanced settings",
				"Additional lines for the settings file.");
		secAdditionalSettings.setExpanded(false);
		createAdditionalSettingsPart((Composite) secAdditionalSettings.getClient(), secAdditionalSettings);

		Section secVerification = createSection(parent, "Verify",
				"Everything is ready? Buckle up and hit the 'Verify' button!");
		createVerificationPart((Composite) secVerification.getClient());

		Section secDiagnostics = createSection(parent, "Diagnostics", "Diagnostic information.");
		secDiagnostics.setExpanded(false);
		createDiagnosticsGui((Composite) secDiagnostics.getClient());

	}

	private static void setAdditionalSettingsTitle(Section section, int settingsCount) {
		section.setText(String.format("Advanced settings (%s)", settingsCount));
	}

	private void createMetadataGui(Composite guiParent, PvDataBinding bindingParent) {
		Label lblId = toolkit.createLabel(guiParent, "ID:");
		Text tbId = toolkit.createText(guiParent, "", SWT.BORDER);
		createLabeledRow(guiParent, lblId, tbId);
		bndId = new PvTextboxBinding(tbId, CfaJobSettings.ID, bindingParent)
				.withValidator(it -> !it.isEmpty(), "ID must not be empty.")
				.withValidator(it -> it.length() < 100, "ID is too long.");

		Label lblName = toolkit.createLabel(guiParent, "Name:");
		Text tbName = toolkit.createText(guiParent, "", SWT.BORDER);
		createLabeledRow(guiParent, lblName, tbName);
		new PvTextboxBinding(tbName, CfaJobSettings.NAME, bindingParent);

		Label lblDescription = toolkit.createLabel(guiParent, "Description:");
		Text tbDescription = toolkit.createText(guiParent, "", SWT.MULTI | SWT.WRAP | SWT.BORDER);
		createLabeledRow(guiParent, lblDescription, tbDescription);
		PvLayoutUtils.setMinHeight(tbDescription, 80);
		PvLayoutUtils.applyWidthHint(tbDescription, 100);

		context.setRequirementMetadata(tbId, tbName, tbDescription);
		
		new PvTextboxBinding(tbDescription, CfaJobSettings.DESCRIPTION, bindingParent);
	}

	private void createInputGui(Composite guiParent, PvDataBinding bindingParent) {
		Label lblSources = toolkit.createLabel(guiParent, "Source files:");
		FileListSelection flsSources = new FileListSelection(guiParent);
		createLabeledRow(guiParent, lblSources, flsSources);
		PvLayoutUtils.setMinHeight(flsSources.getUnderlyingSwtControl(), 100);
		PvCheckboxListBinding bndSources = new PvCheckboxListDeferredBinding(flsSources, CfaJobSettings.SOURCE_FILES,
				bindingParent);
		bndSources.setValidator(it -> !it.getSelectedValues().isEmpty(),
				"At least one source file needs to be selected.");
		context.setFileSelector(bndSources, inputFile.getParent());

		loadSourceFileList(bndSources);

		Label lblReload = toolkit.createLabel(guiParent, "");
		Button btnReload = toolkit.createButton(guiParent, "Reload source files", SWT.PUSH);
		createLabeledRow(guiParent, lblReload, btnReload);
		btnReload.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				super.widgetSelected(e);
				bndSources.fireChangeListeners();
			}
		});

		Label lblLf = toolkit.createLabel(guiParent, "Language frontend:");
		Combo cbLf = new Combo(guiParent, SWT.READ_ONLY);
		List<VerifCaseEditorExtension> lfExtensions = guiPartExtensions.getExtensions(CfaJobSettings.LANGUAGE_FRONTEND);
		PvComboboxBinding bndLf = new PvComboboxBinding(cbLf, CfaJobSettings.LANGUAGE_FRONTEND, bindingParent,
				lfExtensions);
		createLabeledRow(guiParent, lblLf, cbLf, 1, true);
		PvLayoutUtils.applySmallWidthHint(cbLf);

		createStackCompositeWithManager(guiParent, bndLf, lfExtensions);
	}

	/**
	 * Fills the given binding with the list of available files in the container
	 * of the open settings file. Files with extension vc3, vc and txt; also
	 * file names starting with '.' are discarded automatically.
	 */
	private void loadSourceFileList(PvCheckboxListBinding bndSources) {
		IContainer container = inputFile.getParent();
		IPath basePath = container.getFullPath();

		List<IProject> projects = getProjectAndItsReferences(container.getProject());

		List<IKeyLabelPair> fileList = new ArrayList<>();

		/** Extensions which have been seen. Case sensitive. */
		Set<String> foundExtensions = new HashSet<>();
		/**
		 * Extensions which are supported by at least one parser. Case
		 * insensitive (lowercase). If empty, all files are potentially inputs.
		 */
		Set<String> supportedExtensions = collectSupportedExtensions();

		//@formatter:off
		Predicate<IFile> filter = file ->
				file != null &&
					!file.getName().startsWith(".") &&
					file.getFileExtension() != null &&
					(
						(supportedExtensions.isEmpty() && !EXTENSIONS_TO_IGNORE.contains(file.getFileExtension().toLowerCase()))
						||
						supportedExtensions.contains(file.getFileExtension().toLowerCase())
					);
		//@formatter:on

		try {
			for (IFile file : getFiles(projects, filter, true)) {
				IPath relativePath = file.getFullPath().makeRelativeTo(basePath);

				String key = relativePath.toString();
				// Beautification for built-in projects
				String label = key.replaceAll("^.+" + Pattern.quote(BUILTIN_PROJECT_PREFIX) + "([^/\\\\]+[/\\\\])",
						"Built-in $1");

				fileList.add(new IKeyLabelPair.Impl(key, label));
				foundExtensions.add(file.getFileExtension());
			}
		} catch (CoreException e) {
			showErrorMessage(null, "Unable to load the list of available files.", e);
		}

		// Add also wildcards for the present file extensions
		for (String extension : foundExtensions.stream().sorted().collect(Collectors.toList())) {
			String key = String.format("*.%s", extension);
			String label = String.format("%s   (all %s files in this project's root)", key, extension);
			fileList.add(new IKeyLabelPair.Impl(key, label));
		}

		bndSources.setPermittedValuesWithLabels(fileList);
	}

	/**
	 * Collects and returns the supported extensions of all known parser
	 * extensions. The returned extensions will be converted to lowercase.
	 * 
	 * @return Extensions supported by parsers.
	 */
	private static Set<String> collectSupportedExtensions() {
		Set<String> ret = new HashSet<>();

		for (IParserExtension parser : ExtensionHelper.getExtensions(IParserExtension.class).values()) {
			Preconditions.checkState(parser.getSupportedExtensions() != null,
					"One of the language frontend plug-ins returned null as supported extensions that is illegal.");

			if (parser.getSupportedExtensions().isEmpty()) {
				// We don't know which extensions are supported by one of the
				// parsers. Return empty set.
				ret.clear();
				return ret;
			}

			for (String ext : parser.getSupportedExtensions()) {
				Preconditions.checkState(ext != null);
				ret.add(ext.toLowerCase());
			}
		}

		return ret;
	}

	private void createBackendGui(Composite guiParent, PvDataBinding bndJob) {
		Label lblBackend = toolkit.createLabel(guiParent, "Backend:");
		Combo cbBackend = new Combo(guiParent, SWT.READ_ONLY);
		List<VerifCaseEditorExtension> backendExtensions = guiPartExtensions
				.getExtensions(toFqn(CfaJobSettings.JOB, VerificationJobOptions.BACKEND));
		PvComboboxBinding bndBackend = new PvComboboxBinding(cbBackend, VerificationJobOptions.BACKEND, bndJob,
				backendExtensions);
		createLabeledRow(guiParent, lblBackend, cbBackend, 1, true);
		PvLayoutUtils.applySmallWidthHint(cbBackend);

		createStackCompositeWithManager(guiParent, bndBackend, backendExtensions);
	}

	private void createRequirementGui(Composite guiParent, PvDataBinding bndJob) {
		Label lblReqType = toolkit.createLabel(guiParent, "Requirement type:");
		Combo cbReqType = new Combo(guiParent, SWT.READ_ONLY);
		List<VerifCaseEditorExtension> reqExtensions = guiPartExtensions
				.getExtensions(toFqn(CfaJobSettings.JOB, VerificationJobOptions.REQUIREMENT));
		bndRequirement = new PvComboboxBinding(cbReqType, VerificationJobOptions.REQUIREMENT, bndJob, reqExtensions);
		createLabeledRow(guiParent, lblReqType, cbReqType);

		createStackCompositeWithManager(guiParent, bndRequirement, reqExtensions);
	}

	private void createReportersGui(Composite guiParent, PvDataBinding bndJob) {
		Label lblReporters = toolkit.createLabel(guiParent, "Reporters:");
		CheckboxList lstReporters = new CheckboxList(guiParent);
		PvCheckboxListBinding bndReporters = new PvCheckboxListBinding(lstReporters, VerificationJobOptions.REPORTERS,
				bndJob);
		createLabeledRow(guiParent, lblReporters, lstReporters);

		// Fill contents
		Map<String, String> names = ExtensionHelper.getExtensionNames(IVerificationReporterExtension.class);
		bndReporters.setPermittedValuesWithLabels(IKeyLabelPair.Impl.createFromMap(names));
	}
	
	private void createMiscToolsGui(Composite guiParent, PvDataBinding bndJob) {
		Label lblToolAgents = toolkit.createLabel(guiParent, "Misc. Tools:");
		CheckboxList lstToolAgents = new CheckboxList(guiParent);
		PvCheckboxListBinding bndToolAgents = new PvCheckboxListBinding(lstToolAgents, VerificationJobOptions.TOOL_AGENTS,
				bndJob);
		createLabeledRow(guiParent, lblToolAgents, lstToolAgents);

		// Fill contents
		Map<String, String> names = ExtensionHelper.getExtensionNames(IToolAgentExtension.class);
		bndToolAgents.setPermittedValuesWithLabels(IKeyLabelPair.Impl.createFromMap(names));
	}

	private void createVariableSettingsGui(Composite guiParent, PvComboboxBinding bindingParent) {
		PvLayoutUtils.checkExpectedLayout(guiParent);

		Label lblFilter = toolkit.createLabel(guiParent, "Filter:");
		final Text tbFilter = toolkit.createText(guiParent, "", SWT.BORDER);
		PvLayoutUtils.createLabeledRow(guiParent, lblFilter, tbFilter);

		AdvancedVariableSettingsTable table = new AdvancedVariableSettingsTable(guiParent, bindingParent);
		table.setLayoutData(PvLayoutUtils.fullRowLayout(100));
		tbFilter.addModifyListener(it -> table.setVariableNameFilter(tbFilter.getText()));

		context.addContextUpdatedListener(id -> {
			if (VerificationCaseContext.VARIABLES_ID.equals(id)) {
				table.setKnownVariables(
						context.getVariables().stream().filter(it -> it.isLeaf()).collect(Collectors.toList()));
			}
		});
	}

	private void createAdditionalSettingsPart(Composite parent, Section parentSection) {
		parent.setLayout(new FillLayout());
		additionalSettingsTable = new AdditionalSettingsTable(parent);
		additionalSettingsTable.setOnDirtyListener(() -> {
			setAdditionalSettingsTitle(parentSection, additionalSettingsTable.getLineCount());
			firePropertyChange(PROP_DIRTY);
		});
	}

	private void createVerificationPart(Composite parent) {
		VerifCaseExecutorPart.createPart(parent, this);
	}

	private void createDiagnosticsGui(Composite guiParent) {
		GridLayoutFactory.fillDefaults().applyTo(guiParent);
		tbDiag = toolkit.createText(guiParent, "", SWT.READ_ONLY | SWT.MULTI | SWT.V_SCROLL | SWT.WRAP);
		GridDataFactory.fillDefaults().grab(true, true).hint(100, 150).applyTo(tbDiag);
	}

	// Helper methods
	private void updateDiagnosticContent() {
		if (tbDiag != null) {
			try {
				// Diagnostics: show the settings
				tbDiag.setText(getContentsAsSettingsFile());
			} catch (DataInvalidException e) {
				tbDiag.setText("ERROR: " + e.getMessage());
			}
		}
	}

	/**
	 * Displays a new error message.
	 * 
	 * All parameters are optional. The {@code null} parameters will be
	 * substituted with sensible generic text.
	 * 
	 * The message will be logged to the platform log as well
	 * ({@link PlatformLogger}).
	 * 
	 * @param title
	 *            Title of the error message.
	 * @param message
	 *            Description of the error message.
	 * @param exception
	 *            Exception causing the error message. Only the message part
	 *            will be used, the stack trace is not shown on the GUI.
	 */
	void showErrorMessage(String title, String message, Exception exception) {
		String finalTitle = title == null ? "Error" : title;
		String finalMessage = message == null ? "Error occured." : message;
		if (exception != null) {
			finalMessage += " " + exception.getMessage();
		}

		PlatformLogger.logError(
				String.format("PLCverif GUI exception on the verification case form: %s. %s.", title, message),
				exception);

		MessageBox errorMessage = new MessageBox(form.getShell(), SWT.ICON_ERROR | SWT.OK);
		errorMessage.setMessage(finalMessage);
		errorMessage.setText(finalTitle);
		errorMessage.open();
	}

	/**
	 * Creates a new composite within the 'parentComposite' that will behave as
	 * a stack. Its own stack manager will be created. The page selection will
	 * be bound to the value of the given selection binding. Pages will be
	 * created based on the given extensions, plus an error page in addition.
	 */
	private void createStackCompositeWithManager(Composite parentComposite, PvComboboxBinding selectionBinding,
			List<VerifCaseEditorExtension> stackPageExtensions) {
		// Create composite for the stack
		Composite stackedComposite = toolkit.createComposite(parentComposite);
		stackedComposite.setLayoutData(fullRowLayout(150));

		StackManager stackManager = new StackManager(stackedComposite);

		// Add stack pages for each extension
		for (VerifCaseEditorExtension extension : stackPageExtensions) {
			createStackPage(stackedComposite, extension.getSettingsValue(), stackManager, extension.getPart(),
					selectionBinding);
		}

		// Add error page
		Composite compError = new Composite(stackedComposite, SWT.NONE);
		compError.setBackground(parentComposite.getDisplay().getSystemColor(SWT.COLOR_RED));
		Label lblErrorMessage = toolkit.createLabel(compError, "Error: page not found.");
		lblErrorMessage.setLayoutData(PvLayoutUtils.fullRowLayout(10));
		stackManager.addErrorPage(compError);

		// Make sure that the initial selection is correct
		stackManager.select(selectionBinding.getSelectedItem().orElse(null));

		// Subscribe for modification of the selectionBinding
		selectionBinding.addChangeListener(e -> stackManager.select(selectionBinding.getSelectedItem().orElse(null)));
	}

	private Composite createStackPage(Composite parentStack, String key, StackManager stackManager,
			IVerifGuiPartExtension guiPartExtension, PvDataBinding bindingParent) {
		// Create new composite
		Composite composite = new Composite(parentStack, SWT.NONE);
		composite.setLayout(PvLayoutUtils.defaultNestedLayout());

		// Try to load the installation-specific settings
		Optional<SettingsElement> installationDefaults;
		try {
			installationDefaults = SettingsSerializer.loadInstallationPluginSettings(key,
					VerifJobGuiExecutor.getPlatformConfig().getProgramSettingsDirectory());
		} catch (SettingsParserException e) {
			PlatformLogger.logWarning(String.format(
					"Unexpected error while trying to load the installation settings for the plug-in '%s'.", key), e);
			installationDefaults = Optional.empty();
		}

		// Create contents of composite
		IPvGuiPart<VerificationCaseContext> guiPart = guiPartExtension.createPart(composite, bindingParent, context,
				installationDefaults);

		// Register in stack manager
		stackManager.addPage(key, composite, guiPart::setEnabled);

		return composite;
	}

	private Section createSection(Composite parent, String name, String description) {
		int sectionStyle = Section.DESCRIPTION | Section.TITLE_BAR | Section.EXPANDED | Section.TWISTIE;

		Section section = toolkit.createSection(parent, sectionStyle);
		section.setText(name);
		section.setDescription(description);

		Composite sectionBody = toolkit.createComposite(section);
		sectionBody.setLayout(PvLayoutUtils.defaultLayout());
		section.setClient(sectionBody);

		section.setLayoutData(new ColumnLayoutData());
		section.setBackground(resManager.createColor(new RGB(100, 140, 200)));

		return section;
	}
}
