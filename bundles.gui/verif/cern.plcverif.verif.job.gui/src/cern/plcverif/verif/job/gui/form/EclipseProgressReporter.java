/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.job.gui.form;

import org.eclipse.core.runtime.IProgressMonitor;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.progress.ICancelingProgressReporter;

/**
 * Wrapper for {@link IProgressMonitor} instances to be accepted as {@link ICancelingProgressReporter}s.
 */
public class EclipseProgressReporter implements ICancelingProgressReporter {
	private IProgressMonitor monitor;
	private int lastPercentage = 0;

	public EclipseProgressReporter(IProgressMonitor monitor, String taskName) {
		this.monitor = Preconditions.checkNotNull(monitor);
		monitor.beginTask(taskName, 100);
	}
	
	@Override
	public boolean isCanceled() {
		return monitor.isCanceled();
	}

	@Override
	public void reportTask(String currentTask) {
		monitor.subTask(currentTask);
	}

	@Override
	public void reportProgress(int percentage) {
		monitor.worked(percentage - lastPercentage);
		lastPercentage = percentage;
	}

	@Override
	public final void report(String currentTask, int percentage) {
		reportTask(currentTask);
		reportProgress(percentage);
	}

}
