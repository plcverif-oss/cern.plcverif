/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.job.gui.form;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.logging.PlatformLogger;
import cern.plcverif.base.gui.binding.IKeyLabelPair;
import cern.plcverif.verif.extensions.gui.IVerifGuiPartExtension;

public final class VerifCaseEditorExtensions {
	public static class VerifCaseEditorExtension implements IKeyLabelPair {
		private String settingsNode;
		private String settingsValue;
		private String label;
		private int priority = 100;
		private IVerifGuiPartExtension part;

		public String getSettingsNode() {
			return settingsNode;
		}

		public String getSettingsValue() {
			return settingsValue;
		}

		@Override
		public String getKey() {
			return getSettingsValue();
		}

		@Override
		public String getLabel() {
			return label;
		}

		public IVerifGuiPartExtension getPart() {
			return part;
		}

		public int getPriority() {
			return priority;
		}
	}

	private List<VerifCaseEditorExtension> extensions;

	public VerifCaseEditorExtensions() {
		this.extensions = collectAllExtensions();
	}

	/**
	 * Returns all extensions for the given settings node. The returned
	 * extensions will be ordered by their priorities, if specified.
	 * 
	 * @param settingsNodeFqn
	 *            Settings node FQN. Shall not be {@code null}.
	 * @return The list of verification case editor extensions matching the
	 *         given settings node.
	 */
	public List<VerifCaseEditorExtension> getExtensions(String settingsNodeFqn) {
		Preconditions.checkNotNull(settingsNodeFqn);
		return extensions.stream()
				// Find the extension points for the given settings node
				.filter(it -> settingsNodeFqn.equals(it.getSettingsNode()))
				// Sort by priority, then by name
				.sorted(Comparator.comparingInt((VerifCaseEditorExtension it) -> it.getPriority())
						.thenComparing((VerifCaseEditorExtension it) -> it.getSettingsValue()))
				// Collect to list
				.collect(Collectors.toList());
	}

	private List<VerifCaseEditorExtension> collectAllExtensions() {
		IConfigurationElement[] config = Platform.getExtensionRegistry()
				.getConfigurationElementsFor(IVerifGuiPartExtension.EXTENSION_POINT_ID);
		try {
			List<VerifCaseEditorExtension> ret = new ArrayList<>();

			for (IConfigurationElement e : config) {
				VerifCaseEditorExtension extension = new VerifCaseEditorExtension();
				extension.settingsNode = e.getAttribute("settings_node");
				extension.settingsValue = e.getAttribute("settings_value");
				extension.label = e.getAttribute("label");
				extension.priority = tryParseInt(e.getAttribute("priority"), 100);

				Object o = e.createExecutableExtension("class");
				if (IVerifGuiPartExtension.class.isInstance(o)) {
					extension.part = IVerifGuiPartExtension.class.cast(o);
					ret.add(extension);
					PlatformLogger.logDebug(String.format("Verification case GUI part loaded: %s for %s=%s.", e,
							extension.settingsNode, extension.settingsValue));
				} else {
					PlatformLogger.logWarning("Unknown extension: " + e);
				}
			}
			return ret;
		} catch (Exception ex) {
			throw new PlcverifPlatformException(
					"Problem occurred while loading the verification case editor GUI extensions.", ex);
		}
	}

	/**
	 * Returns the parsed integer value of the given string, or the default
	 * value if parsing was unsuccessful. This method never throws an exception.
	 * 
	 * @param str
	 *            String to be parsed as integer
	 * @param defaultValue
	 *            Default integer value to be used if parsing is not successful
	 * @return String parsed as integer or default value
	 */
	private int tryParseInt(String str, int defaultValue) {
		try {
			return Integer.parseInt(str);
		} catch (NumberFormatException e) {
			return defaultValue;
		}
	}
}
