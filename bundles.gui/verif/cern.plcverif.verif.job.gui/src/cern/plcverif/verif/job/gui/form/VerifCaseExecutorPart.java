/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.job.gui.form;

import static cern.plcverif.base.gui.layout.PvLayoutUtils.createLabeledRow;
import static cern.plcverif.verif.job.gui.form.ColorConstants.SATISFIED_RESULT_COLOR;
import static cern.plcverif.verif.job.gui.form.ColorConstants.UNKNOWN_RESULT_COLOR;
import static cern.plcverif.verif.job.gui.form.ColorConstants.VIOLATED_RESULT_COLOR;

import java.io.IOException;
import java.util.Optional;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.jface.resource.ResourceManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.logging.PlcverifSeverity;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.utils.UiUtils;
import cern.plcverif.base.gui.binding.PvDataBinding;
import cern.plcverif.base.gui.binding.PvDataBindingGroup;
import cern.plcverif.base.gui.binding.exceptions.DataInvalidException;
import cern.plcverif.base.gui.binding.swt.PvLabelBinding;
import cern.plcverif.base.gui.layout.PvLayoutUtils;
import cern.plcverif.base.interfaces.data.result.JobStage;
import cern.plcverif.base.interfaces.data.result.LogItem;
import cern.plcverif.verif.interfaces.data.ResultEnum;
import cern.plcverif.verif.interfaces.data.VerificationResult;
import cern.plcverif.verif.job.gui.common.BrowserUtils;
import cern.plcverif.verif.job.gui.decorators.VerifResultDecorator;
import cern.plcverif.verif.job.gui.executor.VerifJobGuiExecutor;
import cern.plcverif.verif.job.gui.executor.VerifJobGuiExecutor.ExecutionJob;
import cern.plcverif.verif.job.gui.form.impl.VerifInProgressSpinner;
import cern.plcverif.verif.summary.extensions.parsernodes.ResultNode;
import cern.plcverif.verif.summary.extensions.parsernodes.VerificationMementoNode;

/**
 * Class that represents the executor part of the verification case GUI. It
 * contains the 'Verify!' button, the 'Open report button' and the labels
 * showing the results. It also takes care of the event handler registrations
 * and showing the progress.
 */
public final class VerifCaseExecutorPart {
	// Necessary data of verification form
	private IFile inputFile;
	private VerifCaseEditor editor;
	private PvDataBinding rootDataBinding;

	// Stack of composites
	private StackLayout stackLayout;
	private Composite verifyComposite;
	private Composite inProgressComposite;

	// Important SWT controls
	private FormToolkit toolkit;
	private ResourceManager resManager;

	// Result
	private PvDataBinding bndSummary;
	private PvLabelBinding bndResultNode;

	private VerifCaseExecutorPart(Composite parentComposite, VerifCaseEditor editor) {
		this.editor = editor;
		this.inputFile = editor.getInputFile();
		this.rootDataBinding = editor.getRootSettingsBinding();

		this.toolkit = new FormToolkit(Display.getCurrent());
		this.resManager = new LocalResourceManager(JFaceResources.getResources(), parentComposite);

	}

	public static VerifCaseExecutorPart createPart(Composite parentComposite, VerifCaseEditor editor) {
		VerifCaseExecutorPart ret = new VerifCaseExecutorPart(parentComposite, editor);
		ret.createPart(parentComposite);
		return ret;
	}

	private void createPart(Composite parentComposite) {
		Composite innerComposite = new Composite(parentComposite, SWT.NONE);
		innerComposite.setLayoutData(PvLayoutUtils.fullRowLayout(1));
		this.stackLayout = new StackLayout();
		innerComposite.setLayout(stackLayout);

		this.verifyComposite = createVerifyPart(innerComposite);
		this.inProgressComposite = createInProgressComposite(innerComposite);
		switchToVerifyPage();
	}

	private Composite createVerifyPart(Composite parentComposite) {
		Composite ret = new Composite(parentComposite, SWT.NONE);
		ret.setLayout(PvLayoutUtils.defaultNestedLayout());

		// Verify button
		Button btnVerify = toolkit.createButton(ret, "Verify!", SWT.PUSH);
		PvLayoutUtils.createRow(ret, btnVerify, 2, true);
		this.bndSummary = PvDataBindingGroup.createRoot();

		rootDataBinding.addOnDirtyPropertyChange(it -> {
			// Verify button is enabled iff the form is valid
			btnVerify.setEnabled(it.isThisAndChildrenValid());

			// Dirty root binding makes this obsolete
			setObsolete();
		});

		// Last result
		Label lblResult = toolkit.createLabel(ret, "Last result:");
		Label lblResultVal = toolkit.createLabel(ret, "N/A");
		createLabeledRow(ret, lblResult, lblResultVal, 1, true);
		bndResultNode = new PvLabelBinding(lblResultVal, VerificationMementoNode.RESULT_NODE, bndSummary);
		bndResultNode.setColorProvider(it -> {
			if (it != null && it.equalsIgnoreCase(ResultEnum.Satisfied.name())) {
				return resManager.createColor(SATISFIED_RESULT_COLOR);
			} else if (it != null && it.equalsIgnoreCase(ResultEnum.Violated.name())) {
				return resManager.createColor(VIOLATED_RESULT_COLOR);
			} else {
				return resManager.createColor(UNKNOWN_RESULT_COLOR);
			}
		});

		// Last time
		Label lblExecDate = toolkit.createLabel(ret, "Last execution:");
		Label lblExecDateVal = toolkit.createLabel(ret, "N/A");
		createLabeledRow(ret, lblExecDate, lblExecDateVal, 1, true);
		new PvLabelBinding(lblExecDateVal, ResultNode.EXEC_DATE, bndResultNode);

		// Last duration
		Label lblDuration = toolkit.createLabel(ret, "Last duration:");
		Label lblDurationVal = toolkit.createLabel(ret, "N/A");
		createLabeledRow(ret, lblDuration, lblDurationVal, 1, true);
		PvLabelBinding bndDuration = new PvLabelBinding(lblDurationVal, ResultNode.RUNTIME_MS, bndResultNode);
		bndDuration.setLabelProvider(it -> {
			try {
				return UiUtils.displayRuntime(Long.parseLong(it));
			} catch (NumberFormatException ex) {
				return String.format("%s ms", it);
			}
		});

		// Open report button
		Button btnReport = toolkit.createButton(ret, "Open report", SWT.PUSH);
		PvLayoutUtils.createRow(ret, btnReport, 1, true);
		PvLayoutUtils.applySmallWidthHint(btnReport);

		// Register event handlers
		final ReportOpenHandler reportOpenHandler = new ReportOpenHandler(btnReport);
		btnVerify.addSelectionListener(new VerifyButtonClick(reportOpenHandler));

		return ret;
	}

	private static Composite createInProgressComposite(Composite parentComposite) {
		Composite ret = new Composite(parentComposite, SWT.NONE);
		GridData layoutData = PvLayoutUtils.fullRowLayout(SWT.DEFAULT);
		layoutData.grabExcessVerticalSpace = true;
		ret.setLayoutData(layoutData);

		ret.setLayout(PvLayoutUtils.defaultLayout());
		ret.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));

		Label label = new Label(ret, SWT.NONE);
		label.setText("Verification is in progress...");
		GridData labelLayoutData = PvLayoutUtils.fullRowLayout(SWT.DEFAULT);
		labelLayoutData.horizontalAlignment = SWT.CENTER;
		label.setLayoutData(labelLayoutData);

		VerifInProgressSpinner inProgressSpinner = new VerifInProgressSpinner(ret);
		ret.addListener(SWT.Show, event -> inProgressSpinner.start());
		ret.addListener(SWT.Hide, event -> inProgressSpinner.stop());
		GridDataFactory.fillDefaults().align(SWT.CENTER, SWT.CENTER).span(PvLayoutUtils.NUM_COLS, 1).minSize(128, 64)
				.hint(128, 64).applyTo(inProgressSpinner.getSwtControl());
		ret.layout();

		return ret;
	}

	// Helper methods
	private void switchToInProgressPage() {
		this.stackLayout.topControl = this.inProgressComposite;
		this.inProgressComposite.getParent().layout();
	}

	private void switchToVerifyPage() {
		this.stackLayout.topControl = this.verifyComposite;
		this.verifyComposite.getParent().layout();
	}

	/**
	 * Notifies this object about a change which makes the previously shown
	 * verification result potentially obsolete.
	 */
	private void setObsolete() {
		final String obsolete = " (may be obsolete)";

		if (bndResultNode.getValue() != null && !bndResultNode.getValue().endsWith(obsolete)) {
			bndResultNode.setValue(bndResultNode.getValue() + obsolete);
		}
	}

	// Event handlers

	private class VerifyButtonClick extends SelectionAdapter {
		private final ReportOpenHandler reportOpenHandler;

		public VerifyButtonClick(ReportOpenHandler reportOpenHandler) {
			super();
			this.reportOpenHandler = reportOpenHandler;
		}

		@Override
		public void widgetSelected(SelectionEvent e) {
			super.widgetSelected(e);

			// Try to fetch the contents of the form
			String settingsForVerification;
			try {
				settingsForVerification = editor.getContentsAsSettingsFile();
			} catch (DataInvalidException ex) {
				editor.showErrorMessage(null,
						"The form contains invalid data, therefore it is not possible to execute the verification.",
						ex);
				return;
			}

			// Switch to "pending" screen
			switchToInProgressPage();

			try {
				new VerifJobGuiExecutor().execute(settingsForVerification, inputFile.getProject(),
						new NullProgressMonitor(), (Optional<ExecutionJob> job, Optional<SettingsElement> summary) -> {
							onVerifExecutionFinished(reportOpenHandler, job, summary);
						});
			} catch (SettingsParserException | IOException | RuntimeException ex) {
				editor.showErrorMessage(null, "Error occurred during the execution of the verification.", ex);
				switchToVerifyPage();
			}
		}
	}

	private void onVerifExecutionFinished(ReportOpenHandler reportOpenHandler, Optional<ExecutionJob> job,
			Optional<SettingsElement> summary) {
		verifyComposite.getDisplay().syncExec(() -> {
			switchToVerifyPage();
			try {
				// Set the decorator
				if (job.isPresent() && job.get().getVerifResult().isPresent()) {
					VerifResultDecorator.decorateFile(inputFile, job.get().getVerifResult().get().getResult());
				} else {
					VerifResultDecorator.decorateFile(inputFile, ResultEnum.Unknown);
				}

				// Set verification result on the form
				bndSummary.load(summary);

				if (job.isPresent()) {
					// Set the correct report file to be opened
					reportOpenHandler.setReportFile(job.get().getHtmlReportFile().orElse(null));
					
					// Show error message if verification failed
					showGuiErrorMessageIfNeeded(job.get());
				}
			} catch (SettingsParserException | DataInvalidException ex) {
				editor.showErrorMessage(null, "Impossible to parse the result of the verification.", ex);
			}
		});
	}

	private static void showGuiErrorMessageIfNeeded(ExecutionJob job) {
		if (job.getVerifResult().isPresent()) {
			VerificationResult verifResult = job.getVerifResult().get();
			if (verifResult.getResult() == ResultEnum.Error) {
				Display.getDefault().asyncExec(
						() -> MessageDialog.openError(Display.getDefault().getActiveShell(),
						"Error during verification", createErrorMessage(verifResult)));
				System.err.println(createErrorMessage(verifResult));
			}
		}
	}

	private static String createErrorMessage(VerificationResult verifResult) {
		// We assume that there is at least one error.
		StringBuilder ret = new StringBuilder(String.format("The verification of '%s' ended with error(s):%n%n", verifResult.getJobMetadata().getId()));
		
		for (JobStage stage : verifResult.getAllStages()) {
			for (LogItem errorLogItem : stage.getLogItems(PlcverifSeverity.Error)) {
				ret.append("  - ");
				// Append message and fix eventual indentation
				ret.append(errorLogItem.getMessage().replace("\n", "\n      "));
				ret.append(System.lineSeparator());
			}
		}
		
		return ret.toString();
	}

	private static class ReportOpenHandler {
		private Button button;
		private IFile reportFile = null;
		private Object reportFileSync = new Object();

		public ReportOpenHandler(Button openReportButton) {
			this.button = Preconditions.checkNotNull(openReportButton);

			this.button.setEnabled(false);
			this.button.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					super.widgetSelected(e);
					synchronized (reportFileSync) {
						if (reportFile != null) {
							BrowserUtils.tryOpenInBrowser(reportFile);
						}
					}
				}
			});
		}

		public void setReportFile(IFile reportFile) {
			if (reportFile == null) {
				// setting to null is equivalent to unsetting
				unsetReportFile();
			} else {
				synchronized (reportFileSync) {
					this.reportFile = Preconditions.checkNotNull(reportFile);
					button.setEnabled(true);
				}
			}
		}

		public void unsetReportFile() {
			synchronized (reportFileSync) {
				this.reportFile = null;
				button.setEnabled(false);
			}
		}
	}
}
