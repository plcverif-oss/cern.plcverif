/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.job.gui.decorators;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ILightweightLabelDecorator;
import org.eclipse.ui.PlatformUI;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.logging.PlatformLogger;
import cern.plcverif.verif.interfaces.data.ResultEnum;
import cern.plcverif.verif.job.gui.VerifJobGuiActivator;

public class VerifResultDecorator implements ILightweightLabelDecorator {
	public static final String ID = "cern.plcverif.verif.job.gui.decorators.VerifResultDecorator";

	/**
	 * Refreshes the decorator manager. Must be called from GUI thread.
	 */
	public static void refresh() {
		PlatformUI.getWorkbench().getDecoratorManager().update(ID);
	}

	@Override
	public void addListener(ILabelProviderListener listener) {
		// Not needed here.
	}

	@Override
	public void dispose() {
		// Not needed here.
	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {
		// Not needed here.
	}

	@Override
	public void decorate(Object element, IDecoration decoration) {
		if (element instanceof IFile) {
			IFile file = (IFile) element;
			ResultEnum resultEnum = ResultEnum.Unknown;
			try {
				Object result = file.getSessionProperty(new QualifiedName(VerifJobGuiActivator.PLUGIN_ID,
						VerifJobDecoratorConstants.VERIFRESULT_PROPERTY_ID));
				if (result instanceof ResultEnum) {
					resultEnum = (ResultEnum) result;
				}
			} catch (CoreException e) { // best effort
			}

			if (resultEnum != ResultEnum.Unknown) {
				ImageDescriptor imgDesc = null;
				if (resultEnum == ResultEnum.Satisfied) {
					imgDesc = VerifJobGuiActivator
							.getImageDescriptorByID(VerifJobDecoratorConstants.SATISFIED_DECORATOR_ICON_ID);
				}
				if (resultEnum == ResultEnum.Violated) {
					imgDesc = VerifJobGuiActivator
							.getImageDescriptorByID(VerifJobDecoratorConstants.VIOLATED_DECORATOR_ICON_ID);
				}

				if (imgDesc != null) {
					decoration.addOverlay(imgDesc, IDecoration.BOTTOM_RIGHT);
				}
			}
		}
	}

	/**
	 * Sets a result decorator for the given file. Expected to be called from
	 * the GUI thread.
	 * 
	 * @param file
	 *            File to be decorated
	 * @param result
	 *            Result that is used to determine the decorator image to be
	 *            used
	 */
	public static void decorateFile(IFile file, ResultEnum result) {
		decorateFile(file, result, true);
	}
	
	/**
	 * Sets a result decorator for the given file. Expected to be called from
	 * the GUI thread if refresh is forced.
	 * 
	 * @param file
	 *            File to be decorated
	 * @param result
	 *            Result that is used to determine the decorator image to be
	 *            used
	 * @param forceRefresh
	 *            If true, it refreshes the decorators in the workspace. If
	 *            true, the method must be called from the GUI thread.
	 */
	public static void decorateFile(IFile file, ResultEnum result, boolean forceRefresh) {
		Preconditions.checkNotNull(file);
		Preconditions.checkNotNull(result);

		try {
			file.setSessionProperty(new QualifiedName(VerifJobGuiActivator.PLUGIN_ID,
					VerifJobDecoratorConstants.VERIFRESULT_PROPERTY_ID), result);
			if (forceRefresh) {
				VerifResultDecorator.refresh();
			}
		} catch (CoreException e) {
			PlatformLogger.logInfo(String.format("Unable to set result decorator for '%s'.", file.toString()));
		}
	}
}
