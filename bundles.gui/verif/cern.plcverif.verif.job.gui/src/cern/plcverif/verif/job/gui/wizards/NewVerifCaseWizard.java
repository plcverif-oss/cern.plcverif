/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.job.gui.wizards;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.internal.Workbench;
import org.eclipse.ui.internal.progress.ProgressManagerUtil;

@SuppressWarnings("restriction")
public class NewVerifCaseWizard extends Wizard implements INewWizard {

	private NewVerifCaseWizardPage newVerifCaseWizardPage;

	public static final String ID = "cern.plcverif.verif.job.gui.wizards.NewVerifCaseWizard";

	@Override
	public final void addPages() {
		addPage(newVerifCaseWizardPage);
	}

	@Override
	public final boolean performFinish() {
		// Create the file.
		IFile file = newVerifCaseWizardPage.createNewFile();

		if (file != null) {
			// Open the newly created file.
			try {
				IDE.openEditor(Workbench.getInstance().getActiveWorkbenchWindow().getActivePage(), file);
			} catch (PartInitException e) {
				MessageDialog.openError(ProgressManagerUtil.getDefaultParent(), "Error", e.getMessage());
			}
			return true;
		} else {
			return false;
		}
	}

	@Override
	public final void init(final IWorkbench workbench, final IStructuredSelection selection) {
		setWindowTitle("New verification case");
		this.newVerifCaseWizardPage = new NewVerifCaseWizardPage(selection);
	}

}
