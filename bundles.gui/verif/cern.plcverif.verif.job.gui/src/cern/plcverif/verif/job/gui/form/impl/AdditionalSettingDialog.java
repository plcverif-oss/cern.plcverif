/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.job.gui.form.impl;

import java.util.Collections;
import java.util.Optional;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import cern.plcverif.base.common.settings.help.SettingsHelp;
import cern.plcverif.base.common.settings.help.SettingsHelp.Field;
import cern.plcverif.base.interfaces.data.PlatformConfig;
import cern.plcverif.base.platform.Platform;
import cern.plcverif.verif.job.VerificationJobExtension;
import cern.plcverif.verif.job.gui.form.impl.AdditionalSettingsTable.Line;

public class AdditionalSettingDialog extends TitleAreaDialog {
	private SettingsHelp help = null;
	private Optional<Line> result = Optional.empty();

	private Combo cbPlugin;
	private Combo cbKey;
	private Combo cbValue;

	public AdditionalSettingDialog(Shell parentShell) {
		super(parentShell);

		this.help = new SettingsHelp();
		Platform.createPlatform(new PlatformConfig()).fillSettingsHelpForJobs(this.help,
				Collections.singletonList(new VerificationJobExtension()));
	}

	public Optional<Line> getLine() {
		return result;
	}

	@Override
	public void create() {
		super.create();
		setTitle("Add new custom setting");
		setMessage("All known settings are listed here together with their brief description.",
				IMessageProvider.INFORMATION);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite dialogArea = (Composite) super.createDialogArea(parent);
		Composite container = new Composite(dialogArea, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(container);
		GridLayoutFactory.swtDefaults().numColumns(2).applyTo(container);

		cbPlugin = createPluginRow(container);
		cbKey = createKeyRow(container);
		Label lblDescriptionVal = createDescriptionRow(container);
		cbValue = createValueRow(container);

		refreshKeysOnPluginChange();
		refreshPermittedValuesOnChange();
		refreshDescriptionOnKeyChange(lblDescriptionVal);
		setupValidation();

		return container;
	}
	
	@Override
	protected Control createContents(Composite parent) {
		Control ret = super.createContents(parent);
		validateAndEnableOk();
		return ret;
	}

	private Combo createPluginRow(Composite container) {
		// Plug-in row
		Label lblPlugin = new Label(container, SWT.NONE);
		lblPlugin.setText("Plug-in:");

		Combo combo = new Combo(container, SWT.READ_ONLY);
		applySecondColumnLayout(combo);
		fillPluginCombo(combo);
		return combo;
	}

	private static Combo createKeyRow(Composite container) {
		// Key row
		Label lblKey = new Label(container, SWT.NONE);
		lblKey.setText("Key:");

		Combo cbKey = new Combo(container, SWT.READ_ONLY);
		applySecondColumnLayout(cbKey);
		return cbKey;
	}

	private static Label createDescriptionRow(Composite container) {
		// Description row
		Label lblDescription = new Label(container, SWT.NONE);
		lblDescription.setText("Description:");
		GridDataFactory.swtDefaults().align(SWT.BEGINNING, SWT.BEGINNING).applyTo(lblDescription);

		Label lblDescriptionVal = new Label(container, SWT.WRAP);
		lblDescriptionVal.setText("");
		lblDescriptionVal.setFont(JFaceResources.getFontRegistry().getItalic(JFaceResources.DEFAULT_FONT));
		GridDataFactory.swtDefaults().grab(true, false).minSize(SWT.DEFAULT, 70).hint(SWT.DEFAULT, 70)
				.align(SWT.FILL, SWT.FILL).applyTo(lblDescriptionVal);
		return lblDescriptionVal;
	}

	private static Combo createValueRow(Composite container) {
		// Value row
		Label lblValue = new Label(container, SWT.NONE);
		lblValue.setText("Value:");

		Combo cbValue = new Combo(container, SWT.BORDER | SWT.DROP_DOWN);
		applySecondColumnLayout(cbValue);

		return cbValue;
	}

	private void refreshKeysOnPluginChange() {
		cbPlugin.addModifyListener(e -> refreshKeys());
	}

	private void refreshKeys() {
		cbKey.removeAll();
		//@formatter:off
		help.getFields().stream()
			.filter(it -> isThisPluginSelected(cbPlugin, it.getPlugin()))
			.map(it -> it.getAttributeFqn())
			.distinct()
			.forEach(it -> cbKey.add(it));
		//@formatter:on
	}
	
	private void refreshPermittedValuesOnChange() {
		cbPlugin.addModifyListener(e -> refreshPermittedValues());
		cbKey.addModifyListener(e -> refreshPermittedValues());
	}
	
	private void refreshPermittedValues() {
		cbValue.removeAll();
		//@formatter:off
		help.getFields().stream()
			.filter(it -> isThisPluginSelected(cbPlugin, it.getPlugin()))
			.filter(it -> cbKey.getText().equals(it.getAttributeFqn()))
			.map(it -> it.getPermittedValues())
			.forEach(it -> it.forEach(val -> cbValue.add(val)));
		//@formatter:on
	}

	private void refreshDescriptionOnKeyChange(Label lblDescription) {
		cbKey.addModifyListener(e -> refreshDescription(cbPlugin.getText(), cbKey.getText(), lblDescription));
	}

	private void refreshDescription(String plugin, String key, Label lblDescription) {
		Optional<Field> selected = help.getFields().stream().filter(
				(Field it) -> equalsOrBothNull(it.getPlugin(), plugin) && equalsOrBothNull(it.getAttributeFqn(), key))
				.findFirst();

		if (selected.isPresent()) {
			lblDescription.setText(selected.get().getDescription());
		} else {
			lblDescription.setText("N/A");
		}
	}

	@Override
	protected void okPressed() {
		saveResult();
		super.okPressed();
	}

	private void setupValidation() {
		cbKey.addModifyListener(e -> validateAndEnableOk());
		cbValue.addModifyListener(e -> validateAndEnableOk());
	}

	private void validateAndEnableOk() {
		boolean valid = cbKey.getSelectionIndex() >= 0 && !cbValue.getText().isEmpty();
		getButton(IDialogConstants.OK_ID).setEnabled(valid);
	}

	private boolean isThisPluginSelected(Combo cbPlugin, String pluginToCompare) {
		// Selected plugin
		int selectedIndex = cbPlugin.getSelectionIndex();
		if (selectedIndex < 0) {
			return false;
		}

		String selectedPlugin = cbPlugin.getItem(selectedIndex);
		return equalsOrBothNull(selectedPlugin, pluginToCompare);
	}

	private String getSelectedKey() {
		// Selected key
		int selectedIndex = cbKey.getSelectionIndex();
		if (selectedIndex < 0) {
			return "?";
		} else {
			String selectedKey = cbKey.getItem(selectedIndex);
			return selectedKey;
		}
	}

	private void saveResult() {
		this.result = Optional.of(new Line(getSelectedKey(), cbValue.getText()));
	}

	private void fillPluginCombo(Combo cbPlugin) {
		//@formatter:off
		help.getFields().stream()
			.map(it -> it.getPlugin())
			//.map(it -> it == null ? "(default)" : it)
			.distinct()
			.forEach(it -> cbPlugin.add(it));
		if (cbPlugin.getItemCount() > 0) {
			cbPlugin.select(0);
		}
		//@formatter:on
	}

	private static boolean equalsOrBothNull(String str1, String str2) {
		if (str1 == null && str2 == null) {
			return true;
		}

		return str1 != null && str1.equals(str2);
	}

	private static void applySecondColumnLayout(Control control) {
		GridDataFactory.swtDefaults().grab(true, false).align(SWT.FILL, SWT.BEGINNING).applyTo(control);
	}
}
