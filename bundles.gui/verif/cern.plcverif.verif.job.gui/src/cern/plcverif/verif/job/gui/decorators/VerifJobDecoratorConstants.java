/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.job.gui.decorators;

public final class VerifJobDecoratorConstants {
	public static final String SATISFIED_DECORATOR_ICON_ID = "icon_satisfied_8x8";
	public static final String VIOLATED_DECORATOR_ICON_ID = "icon_violated_8x8";
	
	public static final String VERIFRESULT_PROPERTY_ID = "verification_result"; 
	
	private VerifJobDecoratorConstants() {
	}
}
