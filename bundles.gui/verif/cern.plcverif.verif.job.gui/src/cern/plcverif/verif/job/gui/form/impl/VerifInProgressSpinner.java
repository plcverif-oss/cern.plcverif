/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.job.gui.form.impl;

import java.util.Random;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

public class VerifInProgressSpinner {
	private Canvas canvas;
	private AnimationThread animationThread = null;
	private Object animationThreadLock = new Object();

	private Image baseImage;
	private Image overlayImage;

	public VerifInProgressSpinner(Composite parent) {
		ImageLoader loader = new ImageLoader();
		this.canvas = new Canvas(parent, SWT.NONE);
		this.canvas.setBackground(parent.getBackground());

		loader.load(getClass().getResourceAsStream("spinner-base.png"));
		this.baseImage = new Image(parent.getDisplay(), loader.data[0]);
		this.canvas.setSize(loader.data[0].width, loader.data[0].height);

		loader.load(getClass().getResourceAsStream("spinner-overlay.png"));
		this.overlayImage = new Image(parent.getDisplay(), loader.data[0]);

		this.canvas.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent event) {
				event.gc.drawImage(baseImage, 0, 0);
			}
		});

		this.animationThread = new AnimationThread();
		this.animationThread.start();
	}

	public Control getSwtControl() {
		return canvas;
	}

	public void start() {
		if (animationThread != null) {
			synchronized (animationThreadLock) {
				animationThread.suspended = false;
				animationThreadLock.notifyAll();
			}
		}
	}

	public void stop() {
		if (animationThread != null) {
			animationThread.suspended = true;
		}
	}

	public void dispose() {
		if (baseImage != null) {
			baseImage.dispose();
		}

		if (overlayImage != null) {
			overlayImage.dispose();
		}

		if (animationThread != null) {
			animationThread.interrupt();
		}
	}

	private class AnimationThread extends Thread {
		private boolean suspended = true;
		private Random random = new Random();

		public AnimationThread() {
			super("Animation thread");
		}

		@Override
		public void run() {
			try {
				while (true) {
					// Handling suspended threads
					synchronized (animationThreadLock) {
						while (suspended) {
							animationThreadLock.wait();
						}
					}

					redrawOverlay();
					waitUntilRedrawNeeded();
				}
			} catch (InterruptedException e) {
				// Not much to do here.
			}
		}

		private void redrawOverlay() {
			canvas.getDisplay().asyncExec(() -> {
				GC localGc = new GC(canvas);
				try {
					localGc.setBackground(canvas.getBackground());
					localGc.fillRectangle(canvas.getClientArea());
					localGc.drawImage(baseImage, 0, 0);
					localGc.drawImage(overlayImage, random.nextInt(80), random.nextInt(50));
				} finally {
					localGc.dispose();
				}
			});
		}

		private void waitUntilRedrawNeeded() {
			try {
				Thread.sleep(1500);
			} catch (InterruptedException e) {
				// Nothing to do.
			}
		}
	}
}
