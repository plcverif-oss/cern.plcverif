/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.job.gui.common;

import org.eclipse.core.resources.IFile;
import org.eclipse.swt.SWT;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.browser.BrowserViewer;
import org.eclipse.ui.internal.browser.WebBrowserEditor;
import org.eclipse.ui.internal.browser.WebBrowserUtil;
import org.eclipse.ui.internal.browser.WebBrowserView;
import org.eclipse.ui.part.FileEditorInput;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.logging.PlatformLogger;

@SuppressWarnings("restriction")
public final class BrowserUtils {
	public enum BrowserStyle {
		NO_BUTTONS_NO_LOCATION(SWT.NONE),
		BUTTONS(BrowserViewer.BUTTON_BAR),
		LOCATION(BrowserViewer.LOCATION_BAR),
		BUTTONS_AND_LOCATION(BrowserViewer.BUTTON_BAR | BrowserViewer.LOCATION_BAR);
		
		private int swtStyle;
		
		private BrowserStyle(int style) {
			this.swtStyle = style;
		}
		
		int getSwtStyle() {
			return swtStyle;
		}
	}
	
	private BrowserUtils() {
		// Utility class.
	}

	/**
	 * Tries to open the given file in a web browser <b>editor</b> inside Eclipse.
	 * 
	 * @param file File (in the workspace) to open
	 */
	public static void tryOpenInBrowser(IFile file) {
		Preconditions.checkNotNull(file, "File to open cannot be null");

		// from: http://www.eclipse-tips.com/tips/9-open-editor-programmatically
		IEditorInput editorInput = new FileEditorInput(file);

		PlatformUI.getWorkbench().getDisplay().syncExec(() -> {
			IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
			// The window needs to be fetched from GUI thread!
			if (window == null) {
				return;
			}
			IWorkbenchPage page = window.getActivePage();
			try {
				// If it was opened, close it first
				IEditorPart oldEditor = page.findEditor(editorInput);
				if (oldEditor != null) {
					page.closeEditor(oldEditor, false);
				}

				page.openEditor(editorInput, WebBrowserEditor.WEB_BROWSER_EDITOR_ID);
			} catch (final PartInitException e) {
				// best effort
				PlatformLogger.logError(String.format("Unable to open the browser view for the file '%s'.", file), e);
			}
		});
	}

	/**
	 * Tries to open the given file in a web browser <b>view</b> inside Eclipse.
	 * 
	 * @param url URL to open
	 * @param browserStyle Style for the browser view (whether URL box and buttons should be shown)
	 */
	public static void tryOpenUrlInBrowser(String url, BrowserStyle browserStyle) {
		Preconditions.checkNotNull(url, "URL to open cannot be null");
		Preconditions.checkNotNull(browserStyle, "Browser style cannot be null");

		PlatformUI.getWorkbench().getDisplay().syncExec(() -> {
			IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
			// The window needs to be fetched from GUI thread!
			if (window == null) {
				return;
			}
			IWorkbenchPage page = window.getActivePage();
			try {
				IViewPart viewPart = page.showView(WebBrowserView.WEB_BROWSER_VIEW_ID, 
						WebBrowserUtil.encodeStyle("pattern-reqs", browserStyle.getSwtStyle()), IWorkbenchPage.VIEW_ACTIVATE);
				if (viewPart instanceof WebBrowserView) {
					WebBrowserView browser = (WebBrowserView) viewPart;
					browser.setURL(url);
					browser.setBrowserViewName("Requirement Patterns");
				} else {
					throw new IllegalStateException("Unexpected view type.");
				}
			} catch (final PartInitException e) {
				// best effort
				PlatformLogger.logError(String.format("Unable to open the browser view for the URL '%s'.", url), e);
			}
		});
	}
}
