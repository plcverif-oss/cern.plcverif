/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.extensions.gui;

import java.util.Optional;

import org.eclipse.swt.widgets.Composite;

import cern.plcverif.base.common.extension.ExtensionPoint;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.gui.binding.PvDataBinding;
import cern.plcverif.base.gui.component.IPvGuiPart;
import cern.plcverif.base.gui.layout.PvLayoutUtils;
import cern.plcverif.verif.extensions.gui.context.VerificationCaseContext;

/**
 * Interface for a GUI part extending the verification job GUI.
 */
@ExtensionPoint(extensionPointId = IVerifGuiPartExtension.EXTENSION_POINT_ID)
public interface IVerifGuiPartExtension {
	public static final String EXTENSION_POINT_ID = "cern.plcverif.verif.extensions.gui.part";

	// Interface defined only to remove the type parameter.

	/**
	 * Creates the GUI controls included in this GUI part. <br>
	 * The extension point's client will be a single instance, while the form's
	 * GUI part should be a unique instance for each form. Therefore the
	 * extension point shall implement a factory method to create the GUI part
	 * descriptor.
	 * 
	 * @param composite
	 *            The composite that will represent this GUI part. The layout is
	 *            expected to be consistent with the layouts used by
	 *            {@link PvLayoutUtils}.
	 * @param parentBinding
	 *            Parent data binding element, i.e., the one that represents the
	 *            settings element described in the extension point's
	 *            {@code settings_node} attribute.
	 * @param context
	 *            Context of the parent verification case form
	 * @param installationSettings
	 *            The installation-specific settings relevant to the settings
	 *            subtree represented by this plug-in, if such settings are
	 *            present. Shall never be {@code null}, but can be
	 *            {@link Optional#empty()}.
	 */
	public IPvGuiPart<VerificationCaseContext> createPart(Composite composite, PvDataBinding parentBinding,
			VerificationCaseContext context, Optional<? extends SettingsElement> installationSettings);
}
