/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.extensions.gui.context;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.Path;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Text;

import com.google.common.base.Preconditions;

import cern.plcverif.base.gui.binding.PvStringListBinding;
import cern.plcverif.base.gui.component.PvGuiContext;
import cern.plcverif.base.models.expr.utils.TypedAstVariable;

/**
 * Class to represent a context for a verification job editor.
 */
public class VerificationCaseContext extends PvGuiContext {
	private IContainer basePath = null;
	private IFile verifCaseFile;
	
	// Requirement metadata text boxes
	private Text tbId;
	private Text tbName;
	private Text tbDescription;
	
	/**
	 * 
	 * @param verifCaseFile The verification case file opened for editing.
	 */
	public VerificationCaseContext(IFile verifCaseFile) {
		this.verifCaseFile = Preconditions.checkNotNull(verifCaseFile);
	}
	
	public IFile getVerifCaseFile() {
		return verifCaseFile;
	}
	
	// Selected source files
	private PvStringListBinding fileSelector = null;
	public static final String FILES_ID = "files";

	public void setFileSelector(PvStringListBinding fileSelector, IContainer baseFolder) {
		this.fileSelector = fileSelector;
		this.basePath = baseFolder;
		fileSelector.addChangeListener(event -> fireUpdated(FILES_ID));
	}

	/**
	 * Returns the list of selected source file names. It is indented only to be
	 * used as display values, not to access the selected files.
	 * 
	 * @return List of selected source file names.
	 */
	public List<String> getSelectedSourceFileNames() {
		if (fileSelector == null) {
			return Collections.emptyList();
		} else {
			return fileSelector.getSelectedValues();
		}
	}

	/**
	 * Returns the list of selected source files as {@code IFile}s. They will be
	 * relative to the base folder that was set up previously.
	 * 
	 * @return List of selected source files.
	 */
	public List<IFile> getSelectedSourceFiles() {
		if (fileSelector == null) {
			return Collections.emptyList();
		} else {
			List<IFile> ret = new ArrayList<>();
			for (String selectedValue : fileSelector.getSelectedValues()) {
				ret.add(basePath.getFile(new Path(selectedValue)));
			}
			
			return ret;
		}
	}

	// Variables
	private List<TypedAstVariable> variables = Collections.emptyList();
	private List<String> variableNames = Collections.emptyList();
	public static final String VARIABLES_ID = "variables";

	/**
	 * Sets the list of known variables with their type information.
	 * This method needs to be called from UI thread.
	 */
	public void setVariables(List<TypedAstVariable> variables) {
		this.variables = Preconditions.checkNotNull(variables);
		this.variableNames = variables.stream().map(it -> it.getVariableName()).collect(Collectors.toList());
		fireUpdated(VARIABLES_ID);
	}

	/**
	 * Returns the list of known variables with their type information.
	 */
	public List<TypedAstVariable> getVariables() {
		return Collections.unmodifiableList(variables);
	}
	
	/**
	 * Returns the list of known variable names.
	 * The names may be hierarchical names.
	 */
	public List<String> getVariableNames() {
		return Collections.unmodifiableList(variableNames);
	}

	// Assertions
	private List<String> assertions = Collections.emptyList();
	private Combo entryBlock;
	public static final String ASSERTIONS_ID = "assertions";

	/**
	 * Sets the list of know assertions.
	 * This method needs to be called from UI thread.
	 */
	public void setAssertions(List<String> assertions) {
		this.assertions = Preconditions.checkNotNull(assertions);
		fireUpdated(ASSERTIONS_ID);
	}

	public List<String> getAssertions() {
		return Collections.unmodifiableList(assertions);
	}

	public String getReqId() {
		return tbId.getText();
	}
	
	public String getReqName() {
		return tbName.getText();
	}
	
	public String getReqDescription() {
		return tbDescription.getText();
	}

	public void setReqId(String reqId) {
		tbId.setText(reqId);
	}
	
	public void setReqName(String reqName) {
		tbName.setText(reqName);
	}
	
	public void setReqDescription(String reqDescription) {
		tbDescription.setText(reqDescription);
	}

	public void setRequirementMetadata(Text tbId, Text tbName, Text tbDescription) {
		this.tbId = tbId;
		this.tbDescription = tbDescription;
		this.tbName = tbName;
	}
	
	public String getEntryBlock() {
		return entryBlock.getItem(entryBlock.getSelectionIndex());
	}
	
	public void setEntryBlock(Combo entryBlock) {
		this.entryBlock = entryBlock;
	}
}
