/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.summary.job.gui.handlers;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.gui.eclipse.EclipseIoUtils;
import cern.plcverif.base.gui.preferences.PlcverifPreferenceAccess;
import cern.plcverif.library.reporter.html.common.CommonHtmlReportDesign;
import cern.plcverif.library.reporter.html.individual.HtmlReporterExtension;
import cern.plcverif.library.reporter.html.summary.HtmlSummaryReporterSettings;
import cern.plcverif.library.reporter.html.summary.impl.HtmlSummaryGenerator;
import cern.plcverif.verif.interfaces.data.ResultEnum;
import cern.plcverif.verif.job.gui.common.BrowserUtils;
import cern.plcverif.verif.job.gui.decorators.VerifResultDecorator;
import cern.plcverif.verif.job.gui.executor.VerifJobGuiExecutor;
import cern.plcverif.verif.summary.extensions.parsernodes.VerificationMementoNode;

/**
 * Handler to execute all selected verification jobs.
 */
public class ExecuteAllVerifJobsHandler extends AbstractHandler implements IHandler {
	public static final String COMMAND_ID = "cern.plcverif.verif.summary.job.gui.handlers.ExecuteAllVerifJobsHandler";
	private static final String REPORT_FILENAME_DATE_FORMAT_PATTERN = "yyyy-MM-dd_HH-mm-ss";

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		// source:
		// http://www.programcreek.com/2013/02/eclipse-rcp-tutorial-add-a-menu-item-to-the-popup-menu-when-right-click-a-file/
		Shell shell = HandlerUtil.getActiveShell(event);

		try {
			List<IFile> selectedFiles = getAllSelectedFiles(event);
			if (selectedFiles.isEmpty()) {
				// Nothing to do if no files are selected
				return null;
			}

			// Get the output folder / create summarizer
			IFolder outputFolder = getOrCreateOutputFolder(selectedFiles.get(0).getProject());
			final VerifJobSummarizer summarizer = new VerifJobSummarizer(shell, outputFolder);

			// Start each verification job
			for (IFile verifFile : selectedFiles) {
				VerifJobGuiExecutor executor = new VerifJobGuiExecutor();

				String verifFileContent = EclipseIoUtils.loadText(verifFile);
				if (VerifJobGuiExecutor.isVerifJobSettingsFile(verifFileContent)) {
					Job verifJob = executor.execute(verifFileContent, verifFile.getProject(), new NullProgressMonitor(),
							(job, summary) -> {
								try {
									// Include the verification result summary
									// in the report generator
									summarizer.addSummaryMemento(summary);

									// If possible, set the file decorator
									Optional<ResultEnum> result = getResult(summary);
									if (result.isPresent()) {
										VerifResultDecorator.decorateFile(verifFile, result.get(), false);
									}
								} catch (SettingsParserException e) {
									summarizer.addErrorMessage(
											"Unable to parse the verification summary generated for '%s'. %s",
											verifFile.getName(), e.getMessage());
								}
							});
					summarizer.registerJobToWaitFor(verifJob);
				} else {
					System.err.printf(
							"The file '%s' does not look like a verification case file. It is skipped from execution.%n",
							verifFile.getName());
				}
			}

			summarizer.schedule();
		} catch (CoreException e) {
			MessageDialog.openError(shell, "Error", "Error happened while handling the resources. " + e.getMessage());
		} catch (IOException e) {
			MessageDialog.openError(shell, "Error", "I/O error happened. " + e.getMessage());
		} catch (SettingsParserException e) {
			MessageDialog.openError(shell, "Error",
					"Error happened while parsing one of the verification job descriptors. " + e.getMessage());
		}

		return null;
	}

	/**
	 * Extracts the verification result (i.e., if it was satisfied or violated)
	 * if possible.
	 * 
	 * @param summarySettingRootNode
	 *            Summary setting root node
	 * @return Result if possible to get
	 */
	private static Optional<ResultEnum> getResult(Optional<SettingsElement> summarySettingRootNode) {
		if (summarySettingRootNode.isPresent()) {
			Optional<Settings> resultAttribute = summarySettingRootNode.get()
					.getAttribute(VerificationMementoNode.RESULT_NODE);
			if (resultAttribute.isPresent() && resultAttribute.get() instanceof SettingsElement) {
				return Optional.of(ResultEnum.valueOf(((SettingsElement) resultAttribute.get()).value()));
			}
		}

		return Optional.empty();
	}

	/**
	 * Returns the output folder to be used for the summary report. If the
	 * folder does not exist yet, it will be created by this method.
	 * 
	 * @param project Parent project
	 * @return Output folder. It guaranteed to exist.
	 * @throws IOException
	 *             if creating the folder was not successful
	 */
	private static IFolder getOrCreateOutputFolder(IProject project) throws IOException {
		return PlcverifPreferenceAccess.getAndCreateOutputFolder(project, new NullProgressMonitor());
	}

	private static List<IFile> getAllSelectedFiles(ExecutionEvent event) throws ExecutionException, CoreException {
		// get the selection
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		ISelectionService service = window.getSelectionService();
		IStructuredSelection selectedFiles = (IStructuredSelection) service.getSelection();

		return EclipseIoUtils.getAllSelectedFiles(selectedFiles);
	}

	/**
	 * Job that will wait for the verification execution jobs to finish, then
	 * create and open the summary report.
	 * 
	 * @author ddarvas
	 *
	 */
	private static class VerifJobSummarizer extends Job {
		private List<Job> jobsToWaitFor = new ArrayList<>();
		private List<VerificationMementoNode> mementoNodes = new ArrayList<>();
		private IFolder outputFolder;
		private List<String> errorMessages = new ArrayList<>();

		private boolean started = false;
		private final Shell shell;

		public VerifJobSummarizer(Shell shell, IFolder outputFolder) {
			super("Summary report generator");
			this.shell = shell;
			this.outputFolder = outputFolder;
		}

		public synchronized void registerJobToWaitFor(Job job) {
			Preconditions.checkState(!started,
					"It is forbidden to register additional jobs once the job has been started.");
			this.jobsToWaitFor.add(job);
		}

		public synchronized void addSummaryMemento(Optional<SettingsElement> memento) throws SettingsParserException {
			Preconditions.checkNotNull(memento);
			if (memento.isPresent()) {
				mementoNodes.add(SpecificSettingsSerializer.parse(memento.get(), VerificationMementoNode.class));
			}
		}

		public synchronized void addErrorMessage(String message, Object... args) {
			this.errorMessages.add(String.format(message, args));
		}

		@Override
		protected IStatus run(IProgressMonitor monitor) {
			started = true;

			// Wait for all jobs to finish
			for (Job job : jobsToWaitFor) {
				try {
					job.join();
				} catch (InterruptedException e) {
					// Not much to do here
				}
			}

			// Hopefully all mementos are collected now.
			if (jobsToWaitFor.size() > mementoNodes.size()) {
				System.err.printf("Executed jobs: %s, collected mementos: %s%n", jobsToWaitFor.size(),
						mementoNodes.size());
			}

			// If there was only a single verification case executed, try to
			// open its individual report instead of generating a summary
			if (mementoNodes.size() == 1) {
				Optional<String> individualReportLocation = mementoNodes.get(0).getInfoNode().getPluginKeyValueStore()
						.getValue(HtmlReporterExtension.CMD_ID, CommonHtmlReportDesign.REPORT_FILENAME_KEY);
				if (individualReportLocation.isPresent()) {
					IFile individualReport = outputFolder.getFile(individualReportLocation.get());
					if (individualReport.exists()) {
						BrowserUtils.tryOpenInBrowser(individualReport);
						showErrorMessagesIfAny();
						return Status.OK_STATUS;
					}
				}
			}

			try {
				// Generate summary report
				CharSequence reportContent = HtmlSummaryGenerator.represent(mementoNodes,
						new HtmlSummaryReporterSettings());

				// Save report
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern(REPORT_FILENAME_DATE_FORMAT_PATTERN);
				String reportFileDate = formatter.format(LocalDateTime.now());
				IFile reportFile = outputFolder.getFile(String.format("summary-%s.html", reportFileDate));
				EclipseIoUtils.setContents(reportFile, reportContent.toString(), monitor);

				// Open report
				BrowserUtils.tryOpenInBrowser(reportFile);

				// Refresh result decorators
				PlatformUI.getWorkbench().getDisplay().syncExec(() -> VerifResultDecorator.refresh());

				showErrorMessagesIfAny();

				return Status.OK_STATUS;
			} catch (IOException e) {
				addErrorMessage("I/O error occurred while generating the summary report. %s", e.getMessage());

				showErrorMessagesIfAny();
				return Status.CANCEL_STATUS;
			}
		}

		private void showErrorMessagesIfAny() {
			if (!errorMessages.isEmpty()) {
				Display.getDefault().asyncExec(new Runnable() {
					public void run() {
						MessageDialog.openError(shell, "Error during verification",
								"The following error(s) happened during verification:\n"
										+ String.join("\n", errorMessages));
					}
				});
			}
		}
	}
}
