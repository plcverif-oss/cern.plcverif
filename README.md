# cern.plcverif -- PLCverif core

[p2 repository](https://plcverif-oss.gitlab.io/cern.plcverif/p2/p2.index)

[Javadoc](https://plcverif-oss.gitlab.io/cern.plcverif/javadoc)

[Coverge report](https://plcverif-oss.gitlab.io/cern.plcverif/jacoco-aggregate)
