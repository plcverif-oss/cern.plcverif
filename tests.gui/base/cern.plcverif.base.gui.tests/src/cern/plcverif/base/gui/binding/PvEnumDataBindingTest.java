package cern.plcverif.base.gui.binding;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsSerializer;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.gui.binding.exceptions.DataInvalidException;

public class PvEnumDataBindingTest {
	private PvDataBinding bndRoot;
	private PvEnumBinding bndEnum;

	private enum TestEnum {
		NORTH, SOUTH, EAST, WEST
	}

	@Before
	public void setUp() {
		bndRoot = PvDataBindingGroup.createRoot();

		bndEnum = new PvEnumBinding("enum", bndRoot, Arrays.asList(TestEnum.values()).stream()
				.map(it -> new IKeyLabelPair.Impl(it.name(), it.name())).collect(Collectors.toList())) {
			private TestEnum selectedItem = null;

			@Override
			protected boolean isThisValid() {
				return selectedItem != null;
			}

			@Override
			public void setSelectedItem(String key) throws DataInvalidException {
				if (handledKeys.contains(key)) {
					selectedItem = TestEnum.valueOf(TestEnum.class, key);
				} else {
					throw new DataInvalidException("Unknown key");
				}
			}

			@Override
			public Optional<String> getSelectedItem() {
				if (selectedItem == null) {
					return Optional.empty();
				} else {
					return Optional.of(selectedItem.name());
				}
			}
		};
	}

	@Test
	public void successfulParsingTest() throws SettingsParserException, DataInvalidException {
		Settings settings = SettingsSerializer.parseSettingsFromString("-enum=NORTH");
		bndRoot.load(settings);

		assertTrue(bndEnum.getSelectedItem().isPresent());
		assertEquals("NORTH", bndEnum.getSelectedItem().get());
	}

	@Test(expected = DataInvalidException.class)
	public void unsuccessfulParsingTest() throws SettingsParserException, DataInvalidException {
		Settings settings = SettingsSerializer.parseSettingsFromString("-enum=LEFT");
		bndRoot.load(settings);
	}
	
	@Test(expected = DataInvalidException.class)
	public void unsuccessfulParsingTest2() throws SettingsParserException, DataInvalidException {
		Settings settings = SettingsSerializer.parseSettingsFromString("-enum");
		bndRoot.load(settings);
	}
}
