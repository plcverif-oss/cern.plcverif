/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.binding.validator;

import org.junit.Assert;
import org.junit.Test;

public class TextValidatorTest {
	@Test
	public void test() {
		TextValidatorCollection tvc = new TextValidatorCollection();
		tvc.add(it -> !it.startsWith("a"), "A"); // invalid if starts with 'a'
		tvc.add(it -> !it.endsWith("b"), "B"); // invalid if ends with 'b'

		Assert.assertFalse(tvc.validate("c").isPresent());

		Assert.assertTrue(tvc.validate("a").isPresent());
		Assert.assertEquals(tvc.validate("a").get(), "A");

		Assert.assertTrue(tvc.validate("b").isPresent());
		Assert.assertEquals(tvc.validate("b").get(), "B");

		Assert.assertTrue(tvc.validate("ab").isPresent());
		Assert.assertEquals(tvc.validate("ab").get(), "A" + System.lineSeparator() + "B");
	}
}
