/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.binding;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsList;
import cern.plcverif.base.common.settings.SettingsSerializer;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.gui.binding.exceptions.DataInvalidException;

public class PvElementListBindingTest {
	private static final String SETTINGS_TEXT = "-a.0=zero -a.1=one -a.2=two";

	private PvDataBinding bndRoot;
	private PvDataBinding bndA;
	private PvStringTextBinding bndZero;
	private PvStringTextBinding bndOne;
	private PvStringTextBinding bndTwo;

	@Before
	public void setUp() {
		bndRoot = PvDataBindingGroup.createRoot();
		bndA = new PvElementListBinding("a", bndRoot);
		bndZero = new PvStringTextBinding("does not matter", bndA);
		bndOne = new PvStringTextBinding("does not matter", bndA);
		bndTwo = new PvStringTextBinding("does not matter", bndA);
	}

	@Test
	public void parsingTest() throws SettingsParserException, DataInvalidException {
		Settings settings = SettingsSerializer.parseSettingsFromString(SETTINGS_TEXT);
		bndRoot.load(settings);

		assertEquals("zero", bndZero.getValue());
		assertEquals("one", bndOne.getValue());
		assertEquals("two", bndTwo.getValue());
	}

	@Test
	public void serializationTest() throws SettingsParserException, DataInvalidException {
		// bndZero is not set -- stays null, won't be serialized

		bndOne.setValue("une");
		assertTrue(bndRoot.isDirty());
		assertTrue(bndA.isDirty());
		assertTrue(bndOne.isDirty());
		assertFalse(bndTwo.isDirty());

		bndTwo.setValue("deux");

		Optional<Settings> result = bndRoot.save();

		assertTrue(result.isPresent());
		SettingsList list = result.get().toSingle().getAttribute("a").get().toList();
		assertEquals(null, list.elements().get(0));
		assertEquals("une", list.elements().get(1).toSingle().value());
		assertEquals("deux", list.elements().get(2).toSingle().value());
	}

	@Test
	public void validityTest() {
		PvDataBinding root = PvDataBindingGroup.createRoot();
		PvElementListBinding parent = new PvElementListBinding("attrib", root);
		new PvTextBinding("does not matter", parent) {
			@Override
			public String getValue() {
				return "x";
			}

			@Override
			public void setValue(String value) {
				;
			}

			@Override
			protected boolean isThisValid() {
				return false;
			}
		};
		
		assertFalse(parent.isThisAndChildrenValid());
	}
}
