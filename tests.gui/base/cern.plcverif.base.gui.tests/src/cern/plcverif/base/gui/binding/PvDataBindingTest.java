/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.gui.binding;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsSerializer;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.gui.binding.exceptions.DataInvalidException;

public class PvDataBindingTest {
	private static final String SETTINGS_TEXT = "-id=ID123 -job=verif -job.backend=nusmv";

	private PvDataBinding bndRoot;
	private PvStringTextBinding bndId;
	private PvStringTextBinding bndJob;
	private PvStringTextBinding bndBackend;

	@Before
	public void setUp() {
		bndRoot = PvDataBindingGroup.createRoot();

		bndId = new PvStringTextBinding("id", bndRoot);
		bndJob = new PvStringTextBinding("job", bndRoot);
		bndBackend = new PvStringTextBinding("backend", bndJob);
	}

	@Test
	public void checkRootTest() {
		assertEquals(bndRoot, bndRoot.getRoot());
		assertEquals(bndRoot, bndId.getRoot());
		assertEquals(bndRoot, bndJob.getRoot());
		assertEquals(bndRoot, bndBackend.getRoot());
	}

	@Test
	public void checkIsRootTest() {
		assertTrue(bndRoot.isRoot());
		assertFalse(bndId.isRoot());
		assertFalse(bndJob.isRoot());
		assertFalse(bndBackend.isRoot());
	}

	@Test
	public void parsingTest() throws SettingsParserException, DataInvalidException {
		Settings settings = SettingsSerializer.parseSettingsFromString(SETTINGS_TEXT);
		bndRoot.load(settings);

		assertEquals("ID123", bndId.getValue());
		assertEquals("verif", bndJob.getValue());
		assertEquals("nusmv", bndBackend.getValue());
	}

	@Test
	public void serializationTest() throws SettingsParserException, DataInvalidException {
		bndId.setValue("ID456");
		assertTrue(bndRoot.isDirty());
		assertTrue(bndId.isDirty());
		assertFalse(bndJob.isDirty());

		bndJob.setValue("test");
		bndBackend.setValue("theta");

		Optional<Settings> result = bndRoot.save();

		assertTrue(result.isPresent());
		assertEquals("ID456", result.get().toSingle().getAttribute("id").get().toSingle().value());
		assertEquals("test", result.get().toSingle().getAttribute("job").get().toSingle().value());
		assertEquals("theta", result.get().toSingle().getAttribute("job").get().toSingle().getAttribute("backend").get()
				.toSingle().value());
	}
}
