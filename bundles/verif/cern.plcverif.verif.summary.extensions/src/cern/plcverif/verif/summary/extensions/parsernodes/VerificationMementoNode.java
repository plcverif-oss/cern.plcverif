/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.summary.extensions.parsernodes;

import static cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory.OPTIONAL;

import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsComposite;
import cern.plcverif.base.interfaces.data.CfaJobSettings;
import cern.plcverif.verif.interfaces.data.ResultEnum;

public class VerificationMementoNode extends CfaJobSettings {
	public static final String INFO_NODE = "info";
	@PlcverifSettingsComposite(name = INFO_NODE, description = "Contains metadata about the verification.", mandatory = OPTIONAL)
	private InfoNode infoNode = new InfoNode();
	
	public static final String RESULT_NODE = "result";
	@PlcverifSettingsComposite(name = RESULT_NODE, description = "Contains data about the result of the verification.", mandatory = OPTIONAL)
	private ResultNode resultNode = new ResultNode();
	
	@PlcverifSettingsComposite(name = JOB, mandatory = OPTIONAL)
	private VerifJobNode verifJobNode = new VerifJobNode();
	
	/**
	 * Returns the information node that contains additional information for report generation.
	 * @return Info node. Never {@code null}.
	 */
	public InfoNode getInfoNode() {
		return infoNode;
	}
	
	public ResultEnum getResult() {
		String resultString = resultNode.getValue();
		if (resultString == null) {
			return ResultEnum.Unknown;
		}
		
		try {
			return ResultEnum.valueOf(resultString);
		} catch (IllegalArgumentException e) {
			return ResultEnum.Error;
		}
	}
	
	/**
	 * Returns the result node. Never {@code null}.
	 * May be a {@link ResultNode} with default values if it was not found in the loaded memento.
	 */
	public ResultNode getResultNode() {
		return resultNode;
	}
	
	public String getRequirementType() {
		return getRequirementNode().getValue();
	}
	
	public void setRequirementType(String requirementTypeId) {
		getRequirementNode().setValue(requirementTypeId);
	}
	
	/**
	 * Returns the verification job node. Never {@code null}.
	 * May be a {@link VerifJobNode} with default values if it was not found in the loaded memento.
	 */
	public VerifJobNode getVerifJobNode() {
		return verifJobNode;
	}
	
	/**
	 * Returns the requirement node. Never {@code null}.
	 * May be a {@link RequirementNode} with default values if it was not found in the loaded memento.
	 */
	public RequirementNode getRequirementNode() {
		return verifJobNode.getRequirementNode();
	}
}
