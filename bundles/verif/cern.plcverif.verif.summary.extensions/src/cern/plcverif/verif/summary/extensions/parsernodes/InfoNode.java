/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.summary.extensions.parsernodes;

import static cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory.OPTIONAL;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import cern.plcverif.base.common.logging.PlatformLogger;
import cern.plcverif.base.common.settings.specific.AbstractSpecificSettings;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsElement;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsList;
import cern.plcverif.base.interfaces.data.PluginKeyValueStore;
import cern.plcverif.base.interfaces.data.PluginKeyValueStore.StoreKey;

public class InfoNode extends AbstractSpecificSettings {
	@PlcverifSettingsElement(name = "backend_name", description = "Name of the backend used",  mandatory = OPTIONAL)
	private String backendName = null; 
	
	@PlcverifSettingsElement(name = "backend_algorithm", description = "Name of the backend algorithm used",  mandatory = OPTIONAL)
	private String backendAlgo = null; 
	
	@PlcverifSettingsElement(name = "requirement_text", description = "Human-readable textual representation of the requirement",  mandatory = OPTIONAL)
	private String requirementText = null;
	
	@PlcverifSettingsList(name = "plugin_store_keys", mandatory = OPTIONAL)
	private List<String> pluginStoreKeys = null;
	
	@PlcverifSettingsList(name = "plugin_store_values", mandatory = OPTIONAL)
	private List<String> pluginStoreValues = null;
	
	private PluginKeyValueStore cachedPluginKvStore = null;
	
	private static final String PLUGIN_STORE_SEPARATOR = "$$";
	
	public String getBackendName() {
		return backendName;
	}
	
	public void setBackendName(String backendName) {
		this.backendName = backendName;
	}
	
	public String getBackendAlgo() {
		return backendAlgo;
	}
	
	public void setBackendAlgo(String backendAlgo) {
		this.backendAlgo = backendAlgo;
	}
	
	public String getRequirementText() {
		return requirementText;
	}
	
	public void setRequirementText(String requirementText) {
		this.requirementText = requirementText;
	}
	
	public void setPluginKeyValueStore(PluginKeyValueStore store) {
		this.pluginStoreKeys = new ArrayList<>();
		this.pluginStoreValues = new ArrayList<>();
		
		for (Entry<StoreKey, String> entry : store.getEntrySet()) {
			String plugin = entry.getKey().getPlugin();
			String key = entry.getKey().getKey();
			if (plugin.contains(PLUGIN_STORE_SEPARATOR)) {
				PlatformLogger.logWarning(String.format("The plug-in key '%s' contains the internally used separator '%s' that may lead to issues.", plugin, PLUGIN_STORE_SEPARATOR));
			}
			if (key.contains(PLUGIN_STORE_SEPARATOR)) {
				PlatformLogger.logWarning(String.format("The key '%s' contains the internally used separator '%s' that may lead to issues.", key, PLUGIN_STORE_SEPARATOR));
			}
			pluginStoreKeys.add(String.format("%s%s%s", plugin, PLUGIN_STORE_SEPARATOR, key));
			pluginStoreValues.add(entry.getValue());
		}
		
		this.cachedPluginKvStore = null;
	}
	
	public PluginKeyValueStore getPluginKeyValueStore() {
		if (this.cachedPluginKvStore == null) {
			PluginKeyValueStore result = new PluginKeyValueStore();
			
			for (int i = 0; i < Math.min(pluginStoreKeys.size(), pluginStoreValues.size()); i++) {
				String storedKey = pluginStoreKeys.get(i);
				String[] storedKeySplit = storedKey.split(Pattern.quote(PLUGIN_STORE_SEPARATOR));
				
				if (storedKeySplit.length == 2) {
					String plugin = storedKeySplit[0];
					String key = storedKeySplit[1];
					String value = pluginStoreValues.get(i);
					result.addValue(plugin, key, value);
				} else {
					PlatformLogger.logWarning(String.format(
							"Unable to load plugin store item from memento node: '%s'. After splitting at %s, it has %s items instead of 2.",
							storedKey, PLUGIN_STORE_SEPARATOR, storedKeySplit.length));
				}
			}
			this.cachedPluginKvStore = result;
		}
		
		return this.cachedPluginKvStore;
	}
 }
