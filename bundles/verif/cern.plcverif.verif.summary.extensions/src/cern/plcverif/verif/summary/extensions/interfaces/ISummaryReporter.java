/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.summary.extensions.interfaces;

import java.util.List;

import cern.plcverif.base.common.settings.ISettingsProvider;
import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.verif.summary.extensions.interfaces.data.SummaryJobResult;
import cern.plcverif.verif.summary.extensions.parsernodes.VerificationMementoNode;

/**
 * A summary reporter is a component that is capable of presenting the results
 * of multiple verification job executions, represented by summary mementos.
 */
public interface ISummaryReporter extends ISettingsProvider {
	/**
	 * Should return a <i>new</i> {@link Settings} instance with enough content
	 * to perfectly reproduce the configuration of the current instance even
	 * when the default settings are changed.
	 */
	public Settings retrieveSettings();

	/**
	 * Should produce the summary report based on the given verification summary
	 * mementos. The generated file contents shall be registered in the given
	 * {@code result}, see {@link SummaryJobResult#getOutputFilesToSave()}.
	 */
	public void execute(List<VerificationMementoNode> input, SummaryJobResult result);
}
