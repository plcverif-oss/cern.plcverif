/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.summary.extensions.parsernodes;

import static cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory.OPTIONAL;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.settings.specific.AbstractSpecificSettings;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsElement;
import cern.plcverif.verif.interfaces.data.ResultEnum;

public class ResultNode extends AbstractSpecificSettings {
	public static final String RUNTIME_MS = "runtime_ms";
	@PlcverifSettingsElement(name = RUNTIME_MS, description = "Total verification run time [ms].",  mandatory = OPTIONAL)
	private long runtimeMs = -1; 
	
	public static final String EXEC_DATE = "exec_date";
	public static final String EXEC_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	@PlcverifSettingsElement(name = EXEC_DATE, description = "Date and time of execution.",  mandatory = OPTIONAL)
	private String executionDate = "N/A"; 
	
	@PlcverifSettingsElement(name = "backend_exectime_ms", description = "Backend execution time [ms].",  mandatory = OPTIONAL)
	private long backendExecTimeMs = -1; 
	
	@PlcverifSettingsElement(name = "cex_exists", description = "True if counterexample/witness is available.",  mandatory = OPTIONAL)
	private boolean cexExists = false; 
	
	public ResultEnum getResult() {
		return ResultEnum.valueOf(this.getValue());
	}
	
	public void setResult(ResultEnum result) {
		this.setValue(result.name());
	}
	
	public long getRuntimeMs() {
		return runtimeMs;
	}
	
	public void setRuntimeMs(long runtimeMs) {
		this.runtimeMs = runtimeMs;
	}
	
	public long getBackendExecTimeMs() {
		return backendExecTimeMs;
	}
	
	public void setBackendExecTimeMs(long backendExecTimeMs) {
		this.backendExecTimeMs = backendExecTimeMs;
	}
	
	public boolean isCexExists() {
		return cexExists;
	}
	
	public void setCexExists(boolean cexExists) {
		this.cexExists = cexExists;
	}
	
	public String getExecutionDate() {
		return executionDate;
	}
	
	public void setExecutionDate(LocalDateTime executionDate) {
		Preconditions.checkNotNull(executionDate);
		this.executionDate = executionDate.format(DateTimeFormatter.ofPattern(EXEC_DATE_FORMAT));
	}
}
