/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.summary.extensions.parsernodes;

import static cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory.OPTIONAL;

import cern.plcverif.base.common.settings.specific.AbstractSpecificSettings;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsComposite;
import cern.plcverif.verif.job.VerificationJobOptions;

public class VerifJobNode extends AbstractSpecificSettings {
	@PlcverifSettingsComposite(name = VerificationJobOptions.REQUIREMENT, mandatory = OPTIONAL)
	private RequirementNode reqNode = new RequirementNode();
	
	public RequirementNode getRequirementNode() {
		return reqNode;
	}
}
