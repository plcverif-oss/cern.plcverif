/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.summary.extensions.interfaces.data;

import java.util.Optional;

import cern.plcverif.base.interfaces.data.BasicJobResult;
import cern.plcverif.base.interfaces.data.JobSettings;

public class SummaryJobResult extends BasicJobResult {
	private String summaryName = "summary";
	
	@Override
	protected Optional<JobSettings> getSpecificSettings() {
		return Optional.empty();
	}
	
	public String getSummaryName() {
		return summaryName;
	}
}
