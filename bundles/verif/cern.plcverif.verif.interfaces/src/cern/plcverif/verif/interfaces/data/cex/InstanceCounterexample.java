/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions, refactoring
 *****************************************************************************/
package cern.plcverif.verif.interfaces.data.cex;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import cern.plcverif.base.models.cfa.cfainstance.AbstractVariableRef;

public class InstanceCounterexample implements ICounterexample {
	private final List<InstanceCexStep> steps = new ArrayList<>();

	public void addStep(InstanceCexStep value) {
		this.steps.add(value);
	}

	public List<InstanceCexStep> getSteps() {
		return Collections.unmodifiableList(this.steps);
	}

	public void removeStep(InstanceCexStep step) {
		this.steps.remove(step);
	}

	public int getStepsCount() {
		return this.steps.size();
	}

	public Collection<AbstractVariableRef> collectAllVariables() {
		HashSet<AbstractVariableRef> variables = new HashSet<>();

		for (InstanceCexStep step : this.steps) {
			variables.addAll(step.getVariables());
		}
		return variables;
	}
}
