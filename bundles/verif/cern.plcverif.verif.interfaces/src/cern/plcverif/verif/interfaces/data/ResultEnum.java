/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - add 'Error' enum
 *****************************************************************************/
package cern.plcverif.verif.interfaces.data;

public enum ResultEnum {
	/**
	 * The backend was able to determine the satisfaction of the requirement and
	 * it is satisfied.
	 */
	Satisfied,

	/**
	 * The backend was able to determine the satisfaction of the requirement and
	 * it is violated.
	 */
	Violated,

	/**
	 * The backend was not able to determine the satisfaction of the
	 * requirement.
	 * <p>
	 * This literal is to be used when the verification job has finished without
	 * any error. This literal is to be used for backend timeout too.
	 */
	Unknown,

	/**
	 * The backend was not able to determine the satisfaction of the requirement
	 * due to an irrecoverable error during the verification job.
	 */
	Error
}
