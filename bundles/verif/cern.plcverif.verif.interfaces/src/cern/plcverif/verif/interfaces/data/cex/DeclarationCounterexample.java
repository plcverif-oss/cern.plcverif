/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions, refactoring
 *****************************************************************************/
package cern.plcverif.verif.interfaces.data.cex;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EObjectSet;
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef;
import cern.plcverif.base.models.expr.Literal;

public class DeclarationCounterexample implements ICounterexample {
	private final List<DeclarationCexStep> steps = new ArrayList<>();

	public void addStep(DeclarationCexStep value) {
		this.steps.add(value);
	}

	public List<DeclarationCexStep> getSteps() {
		return Collections.unmodifiableList(this.steps);
	}
	
	public DeclarationCexStep getStep(int i) {
		Preconditions.checkArgument(i >= 0 && i < getStepsCount(), "Invalid step number.");
		return this.steps.get(i);
	}

	public void removeStep(DeclarationCexStep step) {
		this.steps.remove(step);
	}

	public int getStepsCount() {
		return this.steps.size();
	}

	public Collection<DataRef> collectAllVariables() {
		Set<DataRef> variables = new EObjectSet<>();

		for (DeclarationCexStep step : this.steps) {
			variables.addAll(step.getVariables());
		}
		return variables;
	}

	/**
	 * Returns the list of values for the given data element. Each element in
	 * the returned list represents a value of the given data element in a step.
	 * 
	 * @param dataElement
	 *            Data element whose values to be determined
	 * @return Values for the given data element in each of the steps. If no
	 *         value is available for a given step, the corresponding element in
	 *         the list will be {@code null}. The returned object will never be
	 *         {@code null}.
	 */
	public List<Literal> valuesFor(DataRef dataElement) {
		List<Literal> ret = new ArrayList<>();
		for (DeclarationCexStep step : this.steps) {
			ret.add(step.getValue(dataElement));
		}
		return ret;
	}
}
