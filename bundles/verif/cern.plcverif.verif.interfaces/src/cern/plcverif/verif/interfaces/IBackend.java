/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions, refactoring
 *****************************************************************************/
package cern.plcverif.verif.interfaces;

import cern.plcverif.base.common.logging.PlcverifSeverity;
import cern.plcverif.base.common.progress.ICanceling;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.cfa.transformation.CfaTransformationGoal;
import cern.plcverif.base.models.expr.utils.TlExprUtils;
import cern.plcverif.verif.interfaces.data.RequirementRepresentationStrategy;
import cern.plcverif.verif.interfaces.data.ResultEnum;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.VerificationResult;
import cern.plcverif.verif.interfaces.data.cex.DeclarationCounterexample;
import cern.plcverif.verif.interfaces.data.cex.InstanceCounterexample;

/**
 * A (verification) backend is a component that performs the formal verification
 * of a requirement on a CFA model (possible with the help of an external tool),
 * also parsing the results of the analysis.
 */
public interface IBackend {
	/**
	 * Should return a <i>new</i> {@link SettingsElement} instance with enough
	 * content to perfectly reproduce the configuration of the current instance
	 * even when the default settings are changed. The returned root element's
	 * value is expected to be set to the command ID of this backend.
	 */
	public SettingsElement retrieveSettings();

	/**
	 * Should execute the verification (possibly invoking an external tool). The
	 * model to verify is in {@code input}
	 * ({@link VerificationProblem#getModel()}), as well as the requirement to
	 * check ({@link VerificationProblem#getRequirement()}). The model may be a
	 * {@link CfaNetworkDeclaration} or a {@link CfaNetworkInstance} based on
	 * the goal specified in {@link #getPreferredModelFormat()}, and may or may
	 * not contain function calls based on {@link #isInliningRequired()} and the
	 * user settings. The requirement can be a CTL or LTL expression, that
	 * should be checked by the backend. The methods
	 * {@link TlExprUtils#containsOnlyCtl(cern.plcverif.base.models.expr.Expression)}
	 * and
	 * {@link TlExprUtils#containsOnlyLtl(cern.plcverif.base.models.expr.Expression)}
	 * can be used to determine whether the requirement is purely CTL or purely
	 * LTL.
	 * <p>
	 * The result of the verification should be logged in {@code result}. Fields
	 * to be filled:
	 *
	 * <ul>
	 * <li>{@link VerificationResult#setBackendName(String)}</li>
	 * <li>{@link VerificationResult#setBackendAlgorithmId(String)}</li>
	 * <li>{@link VerificationResult#setBackendStdout(CharSequence)}
	 * (optional)</li>
	 * <li>{@link VerificationResult#setBackendStderr(CharSequence)}
	 * (optional)</li>
	 * <li>{@link VerificationResult#setPotentialStateSpaceSize(double)}
	 * (optional)</li>
	 * <li>{@link VerificationResult#setResult(ResultEnum)}</li>
	 * <li>{@link VerificationResult#setDeclarationCounterexample(DeclarationCounterexample)}
	 * if the model is a CFA declaration and a counterexample/witness is
	 * available</li>
	 * <li>{@link VerificationResult#setInstanceCounterexample(InstanceCounterexample)}
	 * if the model is a CFA instance and a counterexample/witness is available
	 * ({@link VerificationResult#setDeclarationCounterexample(DeclarationCounterexample)}
	 * will be computed automatically <b>at the time of setting the
	 * counterexample</b>)</li>
	 * </ul>
	 * <p>
	 * Note: the verification backend does not have to determine whether the
	 * result ({@link VerificationResult#setResult(ResultEnum)}) has to be an
	 * {@link ResultEnum#Error}. If it was not possible to determine the
	 * satisfaction for whatever reason, it is OK to use the
	 * {@link ResultEnum#Unknown} literal. It will be changed to
	 * {@link ResultEnum#Error} by the {@code VerificationJob} if needed based
	 * on the stages. Be aware that any item with {@link PlcverifSeverity#Error}
	 * in any of the stages will change an {@link ResultEnum#Unknown} result to
	 * an {@link ResultEnum#Error} result.
	 * 
	 * @param canceler
	 *            Object through which the caller can request the cancellation
	 *            of the ongoing backend execution.
	 */
	public void execute(VerificationProblem input, VerificationResult result, ICanceling canceler);

	/**
	 * Returns the preferred format of the CFA model. See
	 * {@link CfaTransformationGoal} for a description of the options.
	 */
	public CfaTransformationGoal getPreferredModelFormat();

	/**
	 * Returns the preferred format of the requirement. See
	 * {@link RequirementRepresentationStrategy} for a description of the
	 * options.
	 */
	public RequirementRepresentationStrategy getPreferredRequirementFormat();

	/**
	 * Should return true if the backend cannot handle the function call
	 * notation. Returning false does not guarantee that the model will not be
	 * inlined.
	 */
	public boolean isInliningRequired();

	/**
	 * Should return true if the backend requires an annotated CFA.
	 */
	public boolean areCfaAnnotationsRequired();
}
