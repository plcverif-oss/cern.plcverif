/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions, refactoring
 *****************************************************************************/
package cern.plcverif.verif.interfaces.data;

import java.nio.file.Path;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EObjectSet;
import cern.plcverif.base.common.formattedstring.BasicFormattedString;
import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.interfaces.data.JobSettings;
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef;
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariableRef;
import cern.plcverif.base.models.cfa.trace.IVariableTraceModel;
import cern.plcverif.base.models.expr.ComparisonOperator;
import cern.plcverif.base.models.expr.InitialValue;
import cern.plcverif.verif.interfaces.IBackend;
import cern.plcverif.verif.interfaces.IRequirement;
import cern.plcverif.verif.interfaces.data.cex.DeclarationCexStep;
import cern.plcverif.verif.interfaces.data.cex.DeclarationCounterexample;
import cern.plcverif.verif.interfaces.data.cex.InstanceCexStep;
import cern.plcverif.verif.interfaces.data.cex.InstanceCexStep.VariableValuePair;
import cern.plcverif.verif.interfaces.data.cex.InstanceCounterexample;

/**
 * This class is an extension of {@link JobResult} that aggregates the results
 * of the operations related to the verification job. Included data (for
 * details, see the corresponding getter):
 * <p>
 * <ul>
 * <li>The verification problem, including the requirement to be checked (see
 * {@link #getVerifProblem()}</li>
 * <li>A set of additional variables introduced by the {@link IRequirement} or
 * other plugins (see {@link #getAdditionalVariables()})</li>
 * <li>The name of the {@link IBackend} (see {@link #getBackendName()})</li>
 * <li>The potential size of the state space (see
 * {@link #getPotentialStateSpaceSize()})</li>
 * <li>The potential counterexample using {@link DataRef} instances (see
 * {@link #getDeclarationCounterexample()})</li>
 * <li>The potential counterexample using {@link AbstractVariableRef} instances
 * (see {@link #getInstanceCounterexample()})</li>
 * <li>The result of the verification (see {@link #getResult()})</li>
 * </ul>
 * <p>
 *
 * @see JobResult
 */
public class VerificationResult extends JobResult {

	// Requirement
	private Optional<VerificationProblem> verifProblem = Optional.empty();
	private Optional<JobSettings> jobSettings = Optional.empty();
	// TODO: maybe this could be a map with some ID to identify the purpose of
	// the variable (or any enum?)
	private EObjectSet<DataRef> additionalVariables = new EObjectSet<>();

	private List<DataRef> inputFields = Collections.emptyList();
	private List<DataRef> paramFields = Collections.emptyList();
	private Map<DataRef, InitialValue> bindings = Collections.emptyMap();
	private Map<DataRef, AbstractMap.SimpleEntry<InitialValue, ComparisonOperator>> bounds = Collections.emptyMap();

	// Verification
	private String backendName = "unknown";
	private String backendAlgorithmId = "N/A";
	private double pss = -1;
	private Optional<DeclarationCounterexample> declarationCounterexample = Optional.empty();
	private Optional<InstanceCounterexample> instanceCounterexample = Optional.empty();
	private ResultEnum result = ResultEnum.Unknown;
	private Optional<CharSequence> backendStdout = Optional.empty();
	private Optional<CharSequence> backendStderr = Optional.empty();

	// Diagnosis
	private Optional<BasicFormattedString> resultDiagnosis = Optional.empty();
	
	
	// Tools
	private List<Path> savedGraphs = new ArrayList<>();

	@Override
	protected Optional<JobSettings> getSpecificSettings() {
		return jobSettings;
	}

	public void setSpecificSettings(JobSettings jobSettings) {
		this.jobSettings = Optional.ofNullable(jobSettings);
	}

	// Requirement
	// -----------
	/**
	 * Returns the verification problem to be checked.
	 * 
	 * @return Verification problem. Never {@code null}, may be
	 *         {@link Optional#empty()} if not set yet.
	 */
	public Optional<VerificationProblem> getVerifProblem() {
		return verifProblem;
	}

	/**
	 * Stores the verification problem to be checked.
	 * 
	 * @param verifProblem
	 *            Verification problem. Shall not be {@code null}.
	 * @throws NullPointerException
	 *             if the given verification problem is null.
	 */
	public void setVerifProblem(VerificationProblem verifProblem) {
		this.verifProblem = Optional.of(Preconditions.checkNotNull(verifProblem));
	}

	/**
	 * Returns the set of additional variables introduced by the
	 * {@link IRequirement} or other plugins.
	 */
	public EObjectSet<DataRef> getAdditionalVariables() {
		return additionalVariables;
	}

	// Verification
	// ------------
	/**
	 * Returns the human-readable name of the {@link IBackend}.
	 */
	public String getBackendName() {
		return backendName;
	}

	/**
	 * Stores the human-readable name of the {@link IBackend}.
	 * 
	 * @param backendName
	 *            The name of the verification backend. Shall not be
	 *            {@code null}.
	 * @throws NullPointerException
	 *             if the given name is null.
	 */
	public void setBackendName(String backendName) {
		this.backendName = Preconditions.checkNotNull(backendName);
	}

	/**
	 * Returns the size of the potential state space. It is negative iff it is
	 * not set.
	 * 
	 * @return The potential state space size.
	 */
	public double getPotentialStateSpaceSize() {
		return pss;
	}

	/**
	 * Stores the size of the potential state space (PSS).
	 * 
	 * @param pss
	 *            The potential state space size.
	 * @throws IllegalArgumentException
	 *             if the given PSS is negative.
	 */
	public void setPotentialStateSpaceSize(double pss) {
		Preconditions.checkArgument(pss >= 0, "The potential state space cannot be negative.");
		this.pss = pss;
	}

	/**
	 * Returns the backend's output written to the standard output.
	 * 
	 * @return Content of the standard output (or {@link Optional#empty()}.
	 *         Never {@code null}.
	 */
	public Optional<CharSequence> getBackendStdout() {
		return Preconditions.checkNotNull(backendStdout);
	}

	/**
	 * Stores the backend's output written to the standard output.
	 * 
	 * @param backendStdout
	 *            Content of the standard output, if known. Shall not be
	 *            {@code null}.
	 * @throws NullPointerException
	 *             if the given content is null.
	 */
	public void setBackendStdout(CharSequence backendStdout) {
		this.backendStdout = Optional.of(Preconditions.checkNotNull(backendStdout));
	}

	/**
	 * Returns the backend's output written to the standard error output.
	 * 
	 * @return Content of the standard error output (or
	 *         {@link Optional#empty()}. Never {@code null}.
	 */
	public Optional<CharSequence> getBackendStderr() {
		return Preconditions.checkNotNull(backendStderr);
	}

	/**
	 * Stores the backend's output written to the standard error output.
	 * 
	 * @param backendStderr
	 *            Content of the standard error output, if known. Shall not be
	 *            {@code null}.
	 * @throws NullPointerException
	 *             if the given content is null.
	 */
	public void setBackendStderr(CharSequence backendStderr) {
		this.backendStderr = Optional.of(Preconditions.checkNotNull(backendStderr));
	}

	/**
	 * Returns a string that identifies the algorithm used by the backend.
	 * 
	 * @return Identifier of the algorithm/configuration used by the backend.
	 *         Never {@code null}.
	 */
	public String getBackendAlgorithmId() {
		Preconditions.checkNotNull(backendAlgorithmId);
		return backendAlgorithmId;
	}

	/**
	 * Stores the identifier of the algorithm/configuration used by the backend.
	 * 
	 * @param backendAlgorithmId
	 *            Identifier of the algorithm/configuration used by the backend.
	 *            Shall not be {@code null}.
	 * @throws NullPointerException
	 *             if the given identifier is null.
	 */
	public void setBackendAlgorithmId(String backendAlgorithmId) {
		Preconditions.checkNotNull(backendAlgorithmId, "The backendAlgorithmId cannot be set to null.");
		this.backendAlgorithmId = backendAlgorithmId;
	}

	/**
	 * Returns the result of the verification (see {@link ResultEnum}).
	 * 
	 * @return Verification result. Never {@code null}.
	 */
	public ResultEnum getResult() {
		return Preconditions.checkNotNull(result);
	}

	/**
	 * Stores the result of the verification.
	 * 
	 * @param result
	 *            Result of verification.
	 * @see ResultEnum
	 * @throws NullPointerException
	 *             if the given result is null.
	 */
	public void setResult(ResultEnum result) {
		Preconditions.checkNotNull(result, "The given result should not be null in setResult.");
		this.result = result;
	}

	/**
	 * Returns the eventual counterexample that uses {@link DataRef} instances
	 * to the declaration CFA.
	 * 
	 * @return The declaration counterexample if known, otherwise
	 *         {@link Optional#empty}. Never {@code null}.
	 */
	public Optional<DeclarationCounterexample> getDeclarationCounterexample() {
		return Preconditions.checkNotNull(declarationCounterexample);
	}

	/**
	 * Stores a declaration counterexample, i.e., a counterexample that uses
	 * {@link DataRef} instances to the declaration CFA.
	 * 
	 * @param declarationCounterexample
	 *            The declaration counterexample. Shall not be {@code null}.
	 * @throws NullPointerException
	 *             if the given counterexample is null.
	 */
	public void setDeclarationCounterexample(DeclarationCounterexample declarationCounterexample) {
		Preconditions.checkNotNull(declarationCounterexample, "The given counterexample is null.");
		this.declarationCounterexample = Optional.of(declarationCounterexample);
	}

	/**
	 * Returns the potential counterexample using {@link AbstractVariableRef}
	 * instances.
	 * <p>
	 * <b>May be {@code null} if there was no counterexample or the model was
	 * not instantiated.</b>
	 */
	public Optional<InstanceCounterexample> getInstanceCounterexample() {
		return instanceCounterexample;
	}

	/**
	 * Stores the counterexample with {@link AbstractVariableRef} instances. If
	 * the variable trace ({@link VerificationResult#getVariableTrace()}) is
	 * available, it will transform the received counterexample to use
	 * {@link DataRef} instances and stores the result through
	 * {@link #setDeclarationCounterexample(DeclarationCounterexample)}.
	 * <p>
	 * <b>Do not set this field before finishing the construction of the
	 * counterexample, or it will not be transformed into the declaration
	 * version.</b>
	 * 
	 * @param instanceCounterexample
	 *            The instance counterexample. Shall not be {@code null}.
	 * @throws NullPointerException
	 *             if the given counterexample is null.
	 */
	public void setInstanceCounterexample(InstanceCounterexample instanceCounterexample) {
		Preconditions.checkNotNull(instanceCounterexample, "The given counterexample is null.");
		this.instanceCounterexample = Optional.of(instanceCounterexample);

		// Back-annotate to declaration counterexample
		Optional<IVariableTraceModel> variableTrace = getVariableTrace();
		if (variableTrace.isPresent()) {
			this.declarationCounterexample = Optional.of(new DeclarationCounterexample());
			for (InstanceCexStep instStep : instanceCounterexample.getSteps()) {
				DeclarationCexStep declStep = new DeclarationCexStep(instStep.getName(),
						declarationCounterexample.get());
				for (VariableValuePair valuation : instStep.getValuations()) {
					DataRef ref = variableTrace.get().recoverHierarchicalReference(valuation.variable);
					declStep.setValue(ref, valuation.value);
				}
			}
		}
	}

	/**
	 * Returns the list of fields treated as inputs.
	 * 
	 * @return Input fields. Never {@code null}.
	 */
	public List<DataRef> getInputFields() {
		return this.inputFields;
	}

	/**
	 * Stores the list of fields treated as inputs.
	 * 
	 * @param inputFields
	 *            Input fields. Shall not be {@code null}.
	 * @throws NullPointerException
	 *             if the given parameter is null.
	 */
	public void setInputFields(List<DataRef> inputFields) {
		this.inputFields = Preconditions.checkNotNull(inputFields);
	}

	/**
	 * Returns the list of fields treated as parameters.
	 * 
	 * @return Parameter fields. Never {@code null}.
	 */
	public List<DataRef> getParamFields() {
		return paramFields;
	}

	/**
	 * Stores the list of fields treated as parameters.
	 * 
	 * @param paramFields
	 *            Parameter fields. Shall not be {@code null}.
	 * @throws NullPointerException
	 *             if the given parameter is null.
	 */
	public void setParamFields(List<DataRef> paramFields) {
		this.paramFields = Preconditions.checkNotNull(paramFields);
	}

	/**
	 * Returns the bindings used in the requirement representation.
	 * 
	 * @return Map representing the bindings. Never {@code null}.
	 */
	public Map<DataRef, InitialValue> getBindings() {
		return bindings;
	}

	/**
	 * Stores bindings used in the requirement representation.
	 * 
	 * @param bindings
	 *            Map representing the bindings. Shall not be {@code null}.
	 * @throws NullPointerException
	 *             if the given parameter is null.
	 */
	public void setBindings(Map<DataRef, InitialValue> bindings) {
		this.bindings = Preconditions.checkNotNull(bindings);
	}
	
	/**
	 * Returns the bounds used in the requirement representation.
	 * 
	 * @return Map representing the bounds. Never {@code null}.
	 */
	public Map<DataRef, AbstractMap.SimpleEntry<InitialValue, ComparisonOperator>> getBounds() {
		return bounds;
	}
	
	/**
	 * Stores bounds used in the requirement representation.
	 * 
	 * @param bounds
	 *            Map representing the bounds. Shall not be {@code null}.
	 * @throws NullPointerException
	 *             if the given parameter is null.
	 */
	public void setBounds(Map<DataRef, AbstractMap.SimpleEntry<InitialValue, ComparisonOperator>> bounds) {
		this.bounds = Preconditions.checkNotNull(bounds);
	}

	/**
	 * Returns the diagnosis created by the requirement plug-in based on the
	 * result of the verification.
	 * 
	 * @return Result diagnosis. Never {@code null}, but can be
	 *         {@link Optional#empty()}.
	 */
	public Optional<BasicFormattedString> getResultDiagnosis() {
		return resultDiagnosis;
	}

	/**
	 * Stores the diagnosis created by the requirement plug-in based on the
	 * result of the verification.
	 * 
	 * @param resultDiagnosis
	 *            Result diagnosis. Shall not be {@code null}.
	 */
	public void setResultDiagnosis(BasicFormattedString resultDiagnosis) {
		Preconditions.checkNotNull(resultDiagnosis, "null is not an accepted value for result diagnosis");
		this.resultDiagnosis = Optional.of(resultDiagnosis);
	}

	/**
	 * @return the paths to the dot graphs that were saved
	 */
	public List<Path> getSavedGraphs() {
		return savedGraphs;
	}
}
