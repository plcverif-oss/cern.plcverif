/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - refactoring
 *****************************************************************************/
package cern.plcverif.verif.interfaces.data.cex;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

import cern.plcverif.base.common.emf.EObjectMap;
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariableRef;
import cern.plcverif.base.models.expr.Literal;

public class InstanceCexStep {
	public static class VariableValuePair {
		public final AbstractVariableRef variable;
		public final Literal value;

		public VariableValuePair(AbstractVariableRef variable, Literal value) {
			this.variable = variable;
			this.value = value;
		}
	}

	private final String name;
	private final Map<AbstractVariableRef, Literal> values = new EObjectMap<AbstractVariableRef, Literal>();
	private boolean startOfLoop = false;

	public InstanceCexStep(final String name, InstanceCounterexample parent) {
		this.name = name;
		parent.addStep(this);
	}

	public String getName() {
		return this.name;
	}

	public void setValue(final AbstractVariableRef variable, Literal value) {
		this.values.put(variable, value);
	}

	public Literal getValue(final AbstractVariableRef variable) {
		return this.values.get(variable);
	}

	public Collection<AbstractVariableRef> getVariables() {
		return values.keySet();
	}

	public Collection<VariableValuePair> getValuations() {
		return values.entrySet().stream().map(kvp -> new VariableValuePair(kvp.getKey(), kvp.getValue()))
				.collect(Collectors.toList());
	}

	public boolean isStartOfLoop() {
		return this.startOfLoop;
	}

	public void setStartOfLoop(final boolean startOfLoop) {
		this.startOfLoop = startOfLoop;
	}
}
