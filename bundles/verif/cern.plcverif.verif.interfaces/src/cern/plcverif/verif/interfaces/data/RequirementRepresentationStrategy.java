/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.interfaces.data;

import cern.plcverif.base.models.expr.EndOfCycle;

/**
 * Defines a requirement representation strategy.
 * 
 * In PLCverif it is assumed that the requirements will be evaluated at the end
 * of the PLC cycles. However, there are different ways to represent this.
 * 
 * <p>Example: if the requirement is to check whether &alpha; is satisfied at the
 * end of each cycle, the temporal logic requirement will be the following with
 * the different strategies:
 * <ul>
 * <li>{@code IMPLICIT}: AG(&alpha;) (We expect the verification backend to make
 * sure that the requirement is only checked at the end of the PLC cycle.)
 * <li>{@code EXPLICIT_WITH_VARIABLE}: AG({@code EoC} -> &alpha;), where
 * {@code EoC} is a variable in the CFA.
 * <li>{@code EXPLICIT_WITH_LOCATIONREF}: AG({@code EoC_ref} -> &alpha;), where
 * {@code EoC_ref} is a special expression in the CFA ({@link EndOfCycle}), which is true iff the
 * currently active location in the CFA is the one representing the end of
 * cycle. The {@link VerificationProblem#getEocLocation()} determines the
 * location which represents the end of the cycle.
 * </ul>
 */
public enum RequirementRepresentationStrategy {
	/**
	 * Implicit strategy: the temporal logic requirement does not have reference
	 * to the end of the cycle. It is expected that the verification backend
	 * will ensure that the requirement is only checked at the end of the cycle.
	 * 
	 * <p>For example, if the verification backend represents the requirements
	 * using error locations, this may be a good approach, as the dependence on
	 * the 'end of cycle' location will be encoded in the structure of the
	 * model.
	 */
	IMPLICIT,

	/**
	 * Explicit strategy with variable: the temporal logic requirement itself
	 * ensures that the states where the current state does not represent the
	 * end of the cycle will not influence its satisfaction. For this reason, a
	 * new variable {@code EoC} is introduced which has a 'true' value if and
	 * only if the current state represents the end of the cycle, where the
	 * requirement can be checked. The name of this variable is not defined.
	 * 
	 * <p>For example, if the verification backend is checking the satisfaction of
	 * the given temporal logic requirement globally, and the backend's language
	 * does not support references to the active location in the requirement,
	 * this strategy can be adequate.
	 */
	EXPLICIT_WITH_VARIABLE,

	/**
	 * Explicit strategy with location reference: the temporal logic requirement
	 * itself ensures that the states where the current state does not represent
	 * the end of the cycle will not influence its satisfaction. For this
	 * reason, the requirement contains reference to the location representing
	 * the end of cycle, where the requirement can be checked.
	 * 
	 * <p>For example, if the verification backend is checking the satisfaction of
	 * the given temporal logic requirement globally, and the backend's language
	 * does support references to the active location in the requirement, this
	 * strategy can be adequate.
	 */
	EXPLICIT_WITH_LOCATIONREF
}
