/******************************************************************************
 * (C) Copyright CERN 2024. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Xaver Fink
 *****************************************************************************/
package cern.plcverif.verif.interfaces;

import cern.plcverif.base.common.extension.ExtensionPoint;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.help.SettingsHelp;

/**
 * Implementors of this interface are extension points serving as a factory for
 * {@link IToolAgent} instances. 
 * <p>
 * Implementors must realize an Extension for the extension point
 * cern.plcverif.verif.extensions.reporter.
 */
@ExtensionPoint(extensionPointId = "cern.plcverif.verif.extensions.toolagent")
public interface IToolAgentExtension {
	/**
	 * Should create an {@link IToolAgent} instance with the default
	 * configuration. The result is expected to be used by client code to
	 * programmatically configure a job.
	 */
	public IToolAgent createAgent();

	/**
	 * Should create an {@link createAgent} preconfigured according to
	 * the contents of the provided settings description.
	 * <p>
	 * The result is expected to be used as returned, so every setting has to be
	 * properly set. If this is not possible based on the provided settings
	 * description, the method should throw a {@link SettingsParserException}.
	 */
	public IToolAgent createAgent(SettingsElement settings) throws SettingsParserException;
	

	/**
	 * Fills the given {@link SettingsHelp} object with the settings accepted by
	 * this extension (recursively) and their description.
	 */
	public void fillSettingsHelp(SettingsHelp help);
}
