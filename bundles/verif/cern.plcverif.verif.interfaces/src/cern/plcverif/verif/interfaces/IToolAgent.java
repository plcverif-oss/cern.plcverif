/******************************************************************************
 * (C) Copyright CERN 2024. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Xaver Fink
 *****************************************************************************/
package cern.plcverif.verif.interfaces;

import cern.plcverif.base.common.settings.ISettingsProvider;
import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.verif.interfaces.data.VerificationResult;

/**
 * A tool agent is a generic third party tool that transforms some result.
 */
public interface IToolAgent extends ISettingsProvider {
	/**
	 * Should return a <i>new</i> {@link Settings} instance with enough content
	 * to perfectly reproduce the configuration of the current instance even
	 * when the default settings are changed.
	 */
	public SettingsElement retrieveSettings();
	
	/**
	 * Executes the agent.
	 */
	public void execute(VerificationResult result);

}
