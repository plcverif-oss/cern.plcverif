/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.interfaces.data;

import static cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory.OPTIONAL;

import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsElement;
import cern.plcverif.base.interfaces.data.JobSettings;

public class VerificationJobSettings extends JobSettings {
	public static final String STRICT_CHECKS = "strict";
	@PlcverifSettingsElement(name = STRICT_CHECKS, description = "Strictness of the various checks (assertions and validations). If true, more assertions are used for the intermediate states of the CFAs to help diagnosing problems, but it has a strong impact on the performance.", mandatory = OPTIONAL)
	private boolean strict = false;

	public static final String DIAGNOSTIC_OUTPUTS = "diagnostic_outputs";
	@PlcverifSettingsElement(name = DIAGNOSTIC_OUTPUTS, description = "Enable diagnostic outputs (such as intermediary models, CFA visualization).", mandatory = OPTIONAL)
	private boolean diagnosticOutputs = true;
	
	public static final String MAX_REDUCTION_ITERATIONS = "max_reduction_iterations";
	@PlcverifSettingsElement(name = MAX_REDUCTION_ITERATIONS, description = "Maximum number of reduction iterations at the verification job level. (This setting will not limit the inner reduction iterations of a given reduction plug-in.)", mandatory = OPTIONAL)
	private int maxReductionIterations = 5;

	public boolean isStrict() {
		return strict;
	}

	public boolean isDiagnosticOutputEnabled() {
		return diagnosticOutputs;
	}
	
	public int getMaxReductionIterations() {
		return maxReductionIterations;
	}
}
