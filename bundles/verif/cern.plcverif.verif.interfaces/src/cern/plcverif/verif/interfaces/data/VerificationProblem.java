/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions, refactoring
 *****************************************************************************/
package cern.plcverif.verif.interfaces.data;

import java.util.Optional;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.formattedstring.BasicFormattedString;
import cern.plcverif.base.models.cfa.cfabase.CfaNetworkBase;
import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.verif.interfaces.IBackend;

/**
 * Represents a formal verification problem, specifying the model and the
 * property to check. Used as the input of {@link IBackend} implementations.
 */
public class VerificationProblem {
	private CfaNetworkBase model;
	private Expression requirement;
	private BasicFormattedString requirementDescription;
	private RequirementRepresentationStrategy requirementRepresentation;
	private Location eocLocation;
	private Location bocLocation;
	private DataRef entryBlockContext;

	/**
	 * Returns the model to verify. The model may be a
	 * {@link CfaNetworkDeclaration} or {@link CfaNetworkInstance} instance,
	 * which should be handled by the {@link IBackend} implementation.
	 */
	public CfaNetworkBase getModel() {
		return model;
	}

	public void setModel(CfaNetworkBase model) {
		this.model = model;
	}

	/**
	 * Returns the requirement to check, which may be a CTL or LTL expression.
	 */
	public Expression getRequirement() {
		return requirement;
	}

	/**
	 * Returns the requirement representation strategy which was used for the
	 * described requirement. This influences the semantics to be uses by the
	 * verification backend.
	 */
	public RequirementRepresentationStrategy getRequirementRepresentation() {
		return requirementRepresentation;
	}

	public void setRequirement(Expression requirement, RequirementRepresentationStrategy requirementRepresentation) {
		this.requirement = requirement;
		this.requirementRepresentation = requirementRepresentation;
	}

	/**
	 * Returns a human-readable description of the requirement.
	 * May be {@code null}.
	 */
	public BasicFormattedString getRequirementDescription() {
		return requirementDescription;
	}

	/**
	 * Sets a human-readable description of the requirement.
	 */
	public void setRequirementDescription(BasicFormattedString requirementDescription) {
		this.requirementDescription = requirementDescription;
	}

	/**
	 * Returns the location where the requirement will be checked (i.e., end of
	 * PLC cycle).
	 */
	public Location getEocLocation() {
		return eocLocation;
	}

	public void setEocLocation(Location eocLocation) {
		this.eocLocation = eocLocation;
	}
	
	/**
	 * Returns the beginning of PLC cycle location.
	 */
	public Location getBocLocation() {
		return bocLocation;
	}

	public void setBocLocation(Location bocLocation) {
		this.bocLocation = bocLocation;
	}
	
	/**
	 * Returns the context of the entry block. May be {@link Optional#empty()} if not set.
	 */
	public Optional<DataRef> getEntryBlockContext() {
		return Optional.ofNullable(this.entryBlockContext); 
	}
	
	/**
	 * Sets the context corresponding to the entry block.
	 */
	public void setEntryBlockContext(DataRef entryBlockContext) {
		this.entryBlockContext = Preconditions.checkNotNull(entryBlockContext);
	}
}
