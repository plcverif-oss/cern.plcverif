/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions, refactoring
 *****************************************************************************/
package cern.plcverif.verif.interfaces.data.cex;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import cern.plcverif.base.common.emf.EObjectMap;
import cern.plcverif.base.common.emf.EObjectMap.IMapFactory;
import cern.plcverif.base.common.emf.EquatableEObject;
import cern.plcverif.base.common.emf.IEObjectMap;
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef;
import cern.plcverif.base.models.cfa.utils.EquatableDataRef;
import cern.plcverif.base.models.expr.Literal;

public class DeclarationCexStep {
	public static class VariableValuePair {
		public final DataRef variable;
		public final Literal value;

		public VariableValuePair(DataRef variable, Literal value) {
			this.variable = variable;
			this.value = value;
		}
	}

	private final String name;
	private final IEObjectMap<DataRef, Literal> values;
	private boolean startOfLoop = false;

	public DeclarationCexStep(final String name, DeclarationCounterexample parent) {
		IMapFactory<Literal> mapFactory = () -> new HashMap<EquatableEObject, Literal>();
		this.values = new EObjectMap<DataRef, Literal>(mapFactory, EquatableDataRef.FACTORY);

		this.name = name;
		parent.addStep(this);
	}

	public String getName() {
		return this.name;
	}

	public void setValue(final DataRef variable, Literal value) {
		this.values.put(variable, value);
	}

	public Literal getValue(final DataRef variable) {
		return this.values.get(variable);
	}

	public Collection<DataRef> getVariables() {
		return values.keySet();
	}

	public Collection<VariableValuePair> getValuations() {
		return values.entrySet().stream().map(kvp -> new VariableValuePair(kvp.getKey(), kvp.getValue()))
				.collect(Collectors.toList());
	}
	
	public Map<DataRef, Literal> getValuationsAsMap() {
		return Collections.unmodifiableMap(this.values);
	}

	public boolean isStartOfLoop() {
		return this.startOfLoop;
	}

	public void setStartOfLoop(final boolean startOfLoop) {
		this.startOfLoop = startOfLoop;
	}
}
