/*******************************************************************************
 * (C) Copyright CERN 2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.interfaces;

import cern.plcverif.base.common.logging.PlcverifSeverity;
import cern.plcverif.base.common.settings.specific.AbstractSpecificSettings;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsElement;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory;

/**
 * Abstract superclass for the settings of verification reporters providing
 * human-readable verification reports.
 */
public abstract class AbstractHumanReporterSettings extends AbstractSpecificSettings {
	public static final String MIN_LOG_LEVEL = "min_log_level";
	@PlcverifSettingsElement(name = MIN_LOG_LEVEL, description = "Minimum log message severity to report.", mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private PlcverifSeverity minLogLevel = PlcverifSeverity.Warning;
	
	public final PlcverifSeverity getMinLogLevel() {
		return minLogLevel;
	}
	
	public static final String INCLUDE_SETTINGS = "include_settings";
	@PlcverifSettingsElement(name = INCLUDE_SETTINGS, description = "Include the effective settings in the report.", mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private boolean includeSettings = true;
	
	/**
	 * If true, the effective settings should be included in the generated verification report.
	 */
	public final boolean isIncludeSettings() {
		return includeSettings;
	}
	
	public static final String SHOW_LOGITEM_TIMESTAMPS = "show_logitem_timestapms";
	@PlcverifSettingsElement(name = SHOW_LOGITEM_TIMESTAMPS, description = "Show timestamps for the log items.", mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private boolean showLogitemTimestamps = false;
	
	/**
	 * If true, the timestamp of the log items should be included in the generated verification report.
	 */
	public final boolean isShowLogitemTimestamps() {
		return showLogitemTimestamps;
	}
}
