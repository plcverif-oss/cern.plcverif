/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.interfaces.data;

import cern.plcverif.base.interfaces.data.result.StageTag;

public final class VerificationStageTags {
	private static class VerificationStageTag extends StageTag {
		public VerificationStageTag(String name) {
			super(name);
		}
	}
	
	/**
	 * Tag to indicate the stage(s) which represents the execution of the verification backend.
	 * To be used to compute the time spent on external backend execution. 
	 */
	public static final StageTag BACKEND_EXECUTION = new VerificationStageTag("Backend execution");
	
	private VerificationStageTags() {
		// Utility class.
	}
}
