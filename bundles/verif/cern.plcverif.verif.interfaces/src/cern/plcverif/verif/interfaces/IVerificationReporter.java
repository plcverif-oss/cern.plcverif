/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions, refactoring
 *****************************************************************************/
package cern.plcverif.verif.interfaces;

import cern.plcverif.base.common.settings.ISettingsProvider;
import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.verif.interfaces.data.VerificationResult;

/**
 * A verification reporter is a component that transforms a
 * {@link VerificationResult} into some other artifact that can serve as a
 * processable report.
 * <p>
 * A reporter may log to the console but <b>should not create files</b>, as it
 * is up to the client code invoking the job to decide what to do with the
 * generated artifacts (it is possible that the results are only displayed in a
 * window). Any artifact that is supposed to be persisted should be added to the
 * result itself in {@link JobResult#getOutputFilesToSave()} by specifying the
 * desired <b>relative</b> file path (relative to the specified output
 * directory) and the content of the file.
 */
public interface IVerificationReporter extends ISettingsProvider {
	/**
	 * Should return a <i>new</i> {@link Settings} instance with enough content
	 * to perfectly reproduce the configuration of the current instance even
	 * when the default settings are changed. It is expected that the reporter
	 * sets the value of the returned settings element too (with its command
	 * ID).
	 */
	public SettingsElement retrieveSettings();

	/**
	 * Should perform the transformation of the log. A reporter may log to the
	 * console but <b>should not create files</b>, as it is up to the client
	 * code invoking the job to decide what to do with the generated artifacts
	 * (it is possible that the results are only displayed in a window). Any
	 * artifact that is supposed to be persisted should be added to the result
	 * itself in {@link JobResult#addFileToSave(String, String, String)} by
	 * specifying the desired <b>relative</b> file path (relative to the
	 * specified output directory) and the content of the file.
	 */
	public void generateReport(VerificationResult result);
}
