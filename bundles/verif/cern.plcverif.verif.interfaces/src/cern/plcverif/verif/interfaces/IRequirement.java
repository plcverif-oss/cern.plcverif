/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions, refactoring
 *****************************************************************************/
package cern.plcverif.verif.interfaces;

import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.interfaces.IParserLazyResult;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.verif.interfaces.data.RequirementRepresentationStrategy;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.VerificationResult;

/**
 * A requirement should provide a way to specify formal properties subject to
 * verification. It is also responsible for generating the full
 * {@link VerificationProblem}.
 */
public interface IRequirement {
	/**
	 * Should return a <i>new</i> {@link SettingsElement} instance with enough
	 * content to perfectly reproduce the configuration of the current instance
	 * even when the default settings are changed. The returned root element's
	 * value is expected to be set to the command ID of this requirement.
	 */
	public SettingsElement retrieveSettings();

	/**
	 * This method should use the CFA declaration model specified in
	 * {@code data} ({@link VerificationProblem#getModel()}) to create the final
	 * verification model (in case of PLC programs, this should involve
	 * enclosing the main automaton in an infinite loop). The resulting model
	 * should replace the original in {@code data}
	 * ({@link VerificationProblem#setModel(cern.plcverif.base.models.cfa.cfabase.CfaNetworkBase)}).
	 * <p>
	 * The method should also create an {@link Expression} representing the
	 * described property as a temporal logic expression. The provided
	 * {@link IParserLazyResult} can be used to parse the property. The
	 * {@link Expression} should be added to the {@link VerificationProblem}
	 * through
	 * {@link VerificationProblem#setRequirement(Expression, RequirementRepresentationStrategy)}.
	 * The requirement should be represented using the given representation
	 * strategy ({@code requirementRepresentation}).
	 * <p>
	 * The results of the method should be added to the {@code result} object.
	 * Fields to be filled:
	 *
	 * <ul>
	 * <li>add auxiliary variables introduced to handle the requirement to
	 * {@link VerificationResult#getAdditionalVariables()}</li>
	 * </ul>
	 */
	public void fillVerificationProblem(VerificationProblem data, IParserLazyResult model, VerificationResult result,
			RequirementRepresentationStrategy requirementRepresentation);

	/**
	 * This method is called after the verification backend execution has been
	 * finished. The requirement representation may analyse the results (e.g.,
	 * {@link VerificationResult#getResult()} and
	 * {@link VerificationResult#getDeclarationCounterexample()}) and provide a
	 * diagnosis based on the original requirement.
	 * 
	 * This method is permitted to set the result diagnosis in verification
	 * result
	 * {@link VerificationResult#setResultDiagnosis(cern.plcverif.base.common.formattedstring.BasicFormattedString)}
	 * and to modify the counterexamples
	 * ({@link VerificationResult#getDeclarationCounterexample()}} and
	 * {@link VerificationResult#getInstanceCounterexample()}}).
	 * 
	 * It can be assumed that the
	 * {@link #diagnoseResult(VerificationProblem, VerificationResult)} method
	 * will be called on the same instance as the
	 * {@link #fillVerificationProblem(VerificationProblem, IParserLazyResult, VerificationResult, RequirementRepresentationStrategy)}
	 * and that the instance will not be reused.
	 * 
	 * @param verifProblem
	 *            Verification problem.
	 * @param result
	 *            Verification result to be modified.
	 */
	public void diagnoseResult(VerificationProblem verifProblem, VerificationResult result);
}
