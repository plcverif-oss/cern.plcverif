/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.summary.job;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.progress.ICancelingProgressReporter;
import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsSerializer;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.base.interfaces.IBasicJob;
import cern.plcverif.base.interfaces.data.AbstractBasicJob;
import cern.plcverif.base.interfaces.data.result.JobStage;
import cern.plcverif.verif.summary.extensions.interfaces.ISummaryReporter;
import cern.plcverif.verif.summary.extensions.interfaces.data.SummaryJobResult;
import cern.plcverif.verif.summary.extensions.parsernodes.VerificationMementoNode;

public class SummaryJob extends AbstractBasicJob implements IBasicJob {
	private SummaryJobSettings settings;
	
	/**
	 * List of summary reporters to be executed. Never {@code null}.
	 */
	private List<ISummaryReporter> summaryReporters = Collections.emptyList();

	public SummaryJob() {
		this(new SummaryJobSettings());
	}

	public SummaryJob(SummaryJobSettings specificSettings) {
		this.settings = specificSettings;
	}

	public void setReporters(List<ISummaryReporter> summaryReporters) {
		this.summaryReporters = Preconditions.checkNotNull(summaryReporters);
		// Note retrieveSettings will return the summary reporters based on this collection, correctly.
	}

	@Override
	public Settings retrieveSettings() {
		SettingsElement ret = SettingsElement.empty();
		ret.addStringListAttribute(SummaryJobSettings.SUMMARY_FILES, settings.getSummaryMementoFiles());
		ret.addListAttribute(SummaryJobSettings.REPORTERS, summaryReporters);
		return ret;
	}

	@Override
	public SummaryJobResult execute(ICancelingProgressReporter progressReporter) {
		// Create result object
		SummaryJobResult result = new SummaryJobResult();
		result.setOutputDirectory(getOutputDirectory());
		result.setSettings(retrieveSettings());
		result.switchToStage("Loading summaries");

		// Load needed mementos
		List<VerificationMementoNode> mementos = loadAllMementos(settings.getSummaryMementoFiles(), result.currentStage());

		result.switchToStage("Generating summary reports");
		// Execute all reporters
		for (ISummaryReporter reporter: summaryReporters) {
			reporter.execute(mementos, result);
		}

		return result;
	}

	private List<VerificationMementoNode> loadAllMementos(List<String> filePatterns, JobStage stage) {
		List<VerificationMementoNode> mementos = new ArrayList<>();
		for (String filePattern : filePatterns) {
			try {
				mementos.addAll(loadMementos(filePattern, stage));
			} catch (SettingsParserException | IOException e) {
				throw new PlcverifPlatformException(String.format(
						"Unable to load the summaries based on pattern '%s': %s.", filePattern, e.getMessage()), e);
			}
		}

		return mementos;
	}

	private static Collection<? extends VerificationMementoNode> loadMementos(String filePattern, JobStage stage)
			throws SettingsParserException, IOException {
		List<Path> files = IoUtils.resolvePathsMatchingPattern(filePattern);
		if (files.isEmpty()) {
			stage.logWarning("No summary file has been found based on the pattern '%s'.", filePattern);
			return Collections.emptyList();
		}

		List<VerificationMementoNode> ret = new ArrayList<>();
		for (Path mementoFile : files) {
			String mementoContent = IoUtils.readFile(mementoFile);
			Settings mementoAsSettings = SettingsSerializer.parseSettingsFromString(mementoContent);
			VerificationMementoNode memento = SpecificSettingsSerializer.parse(mementoAsSettings.toSingle(), VerificationMementoNode.class);
			ret.add(memento);
		}
		return ret;
	}
}
