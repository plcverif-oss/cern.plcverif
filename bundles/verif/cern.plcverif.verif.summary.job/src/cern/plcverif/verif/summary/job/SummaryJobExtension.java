/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.summary.job;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.extension.ExtensionHelper;
import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsList;
import cern.plcverif.base.common.settings.SettingsSerializer;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.help.SettingsHelp;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.interfaces.IBasicJob;
import cern.plcverif.base.interfaces.IJobExtension;
import cern.plcverif.base.interfaces.data.PlatformConfig;
import cern.plcverif.verif.summary.extensions.interfaces.ISummaryReporter;
import cern.plcverif.verif.summary.extensions.interfaces.ISummaryReporterExtension;

public class SummaryJobExtension implements IJobExtension {
	public static final String CMD_ID = "summaryreport";

	public static final String REPORTERS_SETTINGS_NODE = "reporters";

	public Map<String, ISummaryReporterExtension> getSummaryReporters() {
		return ExtensionHelper.getExtensions(ISummaryReporterExtension.class);
	}

	/**
	 * Returns the summary reporter with the given ID. If it is not found
	 * {@link PlcverifPlatformException} is thrown
	 * 
	 * @param reporterId
	 *            ID of the reporter that is to be returned.
	 * @return The summary reporter with the given ID.
	 * @throws PlcverifPlatformException
	 *             if there is no such summary reporter.
	 */
	public ISummaryReporterExtension getSummaryReporterExtension(String reporterId) {
		ISummaryReporterExtension ret = getSummaryReporters().getOrDefault(reporterId, null);
		if (ret == null) {
			throw new PlcverifPlatformException(
					String.format("Summary reporter with ID '%s' has not been found.", reporterId));
		}
		return ret;
	}

	@Override
	public IBasicJob createJob() {
		return new SummaryJob();
	}

	@Override
	public IBasicJob createJob(SettingsElement settings, PlatformConfig platformConfig) throws SettingsParserException {
		Preconditions.checkNotNull(settings, "Error while creating SummaryJob: the given settings is null.");
		Preconditions.checkNotNull(platformConfig, "Error while creating SummaryJob: the given platform configuration is null.");
		
		SummaryJobSettings specificSettings = SpecificSettingsSerializer.parse(settings, SummaryJobSettings.class);
		SummaryJob job = new SummaryJob(specificSettings);
		job.setReporters(parseReporters(settings, platformConfig));
		return job;
	}

	private List<ISummaryReporter> parseReporters(SettingsElement settings, PlatformConfig platformConfig) {
		Optional<Settings> reportersSettings = settings.getAttribute(REPORTERS_SETTINGS_NODE);
		if (!reportersSettings.isPresent()) {
			throw new PlcverifPlatformException(
					String.format("The settings node '%s' is not found.", REPORTERS_SETTINGS_NODE));
		}

		List<ISummaryReporter> ret = new ArrayList<>();
		try {
			SettingsList reportersList = reportersSettings.get().toList();
			for (Settings elem : reportersList.elements()) {
				if (elem == null) {
					continue;
				}

				String reporterId = elem.toSingle().value();
				ISummaryReporterExtension reporterExtension = getSummaryReporterExtension(reporterId);

				// Load installation-specific settings from
				// '<reporter_id>.settings'
				SettingsElement resultantSettings = SettingsSerializer.extendWithInstallationSettings(elem.toSingle(),
						reporterId, platformConfig.getProgramSettingsDirectory());

				ISummaryReporter reporter = reporterExtension.createSummaryReporter(resultantSettings);
				Preconditions.checkNotNull(reporter, "reporter");
				ret.add(reporter);
			}
		} catch (SettingsParserException e) {
			throw new PlcverifPlatformException("Unable to parse settings: " + e.getMessage(), e);
		}

		return ret;
	}

	@Override
	public void fillSettingsHelp(SettingsHelp help) {
		help.addPluginDetails(CMD_ID, "Summary report generation job",
				"Generates a machine-readable representation of the verification results, which can be collected and summarized by the summary job (job=summary). Only applicable if job=verif.",
				SummaryJobExtension.class);
		SpecificSettingsSerializer.fillSettingsHelpSingleParameter(help, SummaryJobSettings.class,
				SummaryJobSettings.SUMMARY_FILES, null);
		SpecificSettingsSerializer.fillSettingsHelpSingleParameter(help, SummaryJobSettings.class,
				REPORTERS_SETTINGS_NODE, new ArrayList<>(getSummaryReporters().keySet()));
		
		help.startPrefixList(REPORTERS_SETTINGS_NODE);
		for (ISummaryReporterExtension reporter : getSummaryReporters().values()) {
			reporter.fillSettingsHelp(help);
		}
		help.endPrefix();
	}
}
