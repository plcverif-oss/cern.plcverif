/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.summary.job;

import static cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory.MANDATORY;

import java.util.List;

import cern.plcverif.base.common.settings.specific.AbstractSpecificSettings;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsList;

public class SummaryJobSettings extends AbstractSpecificSettings {
	public static final String SUMMARY_FILES = "summary_files";
	public static final String REPORTERS = "reporters";
	
	@PlcverifSettingsList(name=SUMMARY_FILES, description="Summary memento files to be compiled into a summary report. They may contain wildcards (* or ?).",
			mandatory = MANDATORY)
	private List<String> summaryMementoFiles;
	
	@PlcverifSettingsList(name=REPORTERS, description="IDs of summary reporters to be used.",
			mandatory = MANDATORY)
	private List<String> reporters;
	
	
	public List<String> getSummaryMementoFiles() {
		return summaryMementoFiles;
	}
	
	public void setSummaryMementoFiles(List<String> summaryMementoFiles) {
		this.summaryMementoFiles = summaryMementoFiles;
	}
	
	public List<String> getReporters() {
		return reporters;
	}
	
	public void setReporters(List<String> reporters) {
		this.reporters = reporters;
	}
}
