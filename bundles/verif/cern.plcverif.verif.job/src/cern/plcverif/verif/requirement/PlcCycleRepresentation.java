/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Xaver Fink - additions
 *****************************************************************************/
package cern.plcverif.verif.requirement;

import static cern.plcverif.base.models.cfa.textual.CfaToString.toDiagString;
import static cern.plcverif.base.models.cfa.utils.CfaDeclarationUtils.getReferredField;
import static cern.plcverif.base.models.expr.utils.TypeUtils.hasFloatType;
import static cern.plcverif.base.models.expr.utils.TypeUtils.hasIntType;
import static cern.plcverif.base.models.expr.utils.TypeUtils.hasUnknownType;
import static org.eclipse.emf.ecore.util.EcoreUtil.copy;

import java.nio.Buffer;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EObjectSet;
import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.common.emf.textual.EmfModelPrinter;
import cern.plcverif.base.common.emf.textual.SimpleEObjectToText;
import cern.plcverif.base.common.logging.IPlcverifLogger;
import cern.plcverif.base.interfaces.IParserLazyResult;
import cern.plcverif.base.interfaces.exceptions.AtomParsingException;
import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory;
import cern.plcverif.base.models.cfa.builder.TransitionBuilder;
import cern.plcverif.base.models.cfa.builder.VariableAssignmentSkeleton;
import cern.plcverif.base.models.cfa.cfabase.DataDirection;
import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfabase.Transition;
import cern.plcverif.base.models.cfa.cfadeclaration.AssignmentTransition;
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.CallTransition;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef;
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructure;
import cern.plcverif.base.models.cfa.cfadeclaration.DirectionFieldAnnotation;
import cern.plcverif.base.models.cfa.cfadeclaration.Field;
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef;
import cern.plcverif.base.models.cfa.cfadeclaration.VariableAssignment;
import cern.plcverif.base.models.cfa.cfadeclaration.VariableComparison;
import cern.plcverif.base.models.cfa.textual.CfaToString;
import cern.plcverif.base.models.cfa.utils.CfaCollectionDeleter;
import cern.plcverif.base.models.cfa.utils.CfaDeclarationUtils;
import cern.plcverif.base.models.cfa.utils.CfaTypeUtils;
import cern.plcverif.base.models.cfa.utils.CfaUtils;
import cern.plcverif.base.models.expr.AtomicExpression;
import cern.plcverif.base.models.expr.ComparisonExpression;
import cern.plcverif.base.models.expr.ComparisonOperator;
import cern.plcverif.base.models.expr.ElementaryType;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.FloatLiteral;
import cern.plcverif.base.models.expr.InitialValue;
import cern.plcverif.base.models.expr.IntLiteral;
import cern.plcverif.base.models.expr.IntType;
import cern.plcverif.base.models.expr.Literal;
import cern.plcverif.base.models.expr.Type;
import cern.plcverif.base.models.expr.utils.ExprUtils;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.VerificationResult;

/**
 * Representing the PLC's scan cycle and the requirement to be checked is the
 * responsibility of the requirement plug-ins. This is because for example an
 * equivalence checking may have special needs in terms of scan cycle or input
 * variable assignment. However, most requirements will simply create a new
 * automaton where the user code is called in an infinite loop, with
 * nondeterministic input variable initialization before each call. This class
 * provides helper methods to achieve this representation. It is not mandatory
 * to rely on these methods.
 *
 */
public final class PlcCycleRepresentation {
	public static final String EOC_FIELD_NAME = "EoC";
	public static final String BOC_FIELD_NAME = "BoC";
	private static CfaDeclarationSafeFactory factory = CfaDeclarationSafeFactory.INSTANCE;

	public static class InputBindingDesc {
		public final String input;
		public final String value;

		public InputBindingDesc(String input, String value) {
			this.input = input;
			this.value = value;
		}
	}

	public static class InputBinding {
		public final DataRef leftValue;
		public final InitialValue rightValue;

		public InputBinding(DataRef leftValue, InitialValue rightValue) {
			this.leftValue = leftValue;
			this.rightValue = rightValue;

			// Try fix unknown types (quickfix)
			if (hasUnknownType(rightValue)) {
				if ((hasIntType(leftValue) && rightValue instanceof IntLiteral)
						|| (hasFloatType(leftValue) && rightValue instanceof FloatLiteral)) {
					((Literal) rightValue).setType(copy(leftValue.getType()));
				}
			}
		}
	}

	public static class FieldsInCategories {
		List<DataRef> inputFields;
		List<DataRef> paramFields;
		List<InputBinding> fixedFields;
		List<InputBinding> lowerBounds;
		List<InputBinding> upperBounds;

		public List<DataRef> getInputFields() {
			return Collections.unmodifiableList(inputFields);
		}

		public List<DataRef> getParamFields() {
			return Collections.unmodifiableList(paramFields);
		}

		public List<InputBinding> getFixedFields() {
			return Collections.unmodifiableList(fixedFields);
		}
		
		public List<InputBinding> getLowerBounds() {
			return Collections.unmodifiableList(lowerBounds);
		}
		
		public List<InputBinding> getUpperBounds() {
			return Collections.unmodifiableList(upperBounds);
		}
	}

	private PlcCycleRepresentation() {
		// Utility class.
	}

	/**
	 * Determines the list of fields based on the requirement settings belonging
	 * to the following categories: input fields, parameter fields, input
	 * bindings.
	 * <p>
	 * More precisely, it will perform the following tasks:
	 * <ul>
	 * <li>Parses the field names in the settings (String to {@link DataRef});
	 * <li>Includes unmentioned inputs (see
	 * {@link #includeUnmentionedInputs(List, List, List, CfaNetworkDeclaration)}),
	 * i.e., fields which are marked as input in the CFA, but not specified as
	 * either input, parameter or fixed field;
	 * <li>In case a field is marked as both input and parameter, they are
	 * removed from the list of input fields;
	 * <li>The input and parameter fields are sorted to obtain a deterministic
	 * result.
	 * </ul>
	 * 
	 * @param cfa
	 *            The control-flow automata declaration that contains the
	 *            fields. Used to determine the "unmentioned" inputs
	 * @param parserResult
	 *            Parser object to be used to parse the field names
	 * @param settings
	 *            Requirement settings containing the field names to be parsed.
	 * @param logger
	 *            Logger to be used.
	 * @return The list of input, parameter and fixed fields, in a deterministic
	 *         order.
	 * @throws AtomParsingException
	 *             if it is not possible to parse any of the field names.
	 */
	public static FieldsInCategories categorizeFields(CfaNetworkDeclaration cfa, IParserLazyResult parserResult,
			AbstractRequirementSettings settings, IPlcverifLogger logger) throws AtomParsingException {
		Preconditions.checkNotNull(cfa, "cfa");
		Preconditions.checkNotNull(parserResult, "parserResult");
		Preconditions.checkNotNull(settings, "settings");
		Preconditions.checkNotNull(logger, "logger");

		FieldsInCategories ret = new FieldsInCategories();
		ret.inputFields = PlcCycleRepresentation.parseFields(parserResult, settings.getInputAstVars());
		ret.paramFields = PlcCycleRepresentation.parseFields(parserResult, settings.getParamAstVars());
		ret.fixedFields = PlcCycleRepresentation.parseAssignments(parserResult, settings.getInputBindings(), logger);
		ret.lowerBounds = PlcCycleRepresentation.parseAssignments(parserResult, settings.getLowerBounds(), logger);
		ret.upperBounds = PlcCycleRepresentation.parseAssignments(parserResult, settings.getUpperBounds(), logger);

		includeUnmentionedInputs(ret.inputFields, ret.paramFields, ret.fixedFields, cfa);
		removeParamsFromInputs(ret.inputFields, ret.paramFields);

		// sort to have deterministic order
		ret.inputFields.sort((DataRef ref1, DataRef ref2) -> CfaToString.toDiagString(ref1)
				.compareTo(CfaToString.toDiagString(ref2)));
		ret.paramFields.sort((DataRef ref1, DataRef ref2) -> CfaToString.toDiagString(ref1)
				.compareTo(CfaToString.toDiagString(ref2)));

		return ret;
	}

	/**
	 * This method modifies the CFA stored in the given
	 * {@link VerificationProblem} such that it will represent the PLC cycle. It
	 * will:
	 * <ul>
	 * <li>Create a new main automaton,
	 * <li>Create an infinite loop in the automaton, which
	 * <ul>
	 * <li>Calls the previous main automaton (representing the program under
	 * verification),
	 * <li>Sets the input fields to non-deterministic values (before each call),
	 * <li>Sets the input fields with bound values to their given values (before
	 * each call).
	 * </ul>
	 * <li>Set the parameter fields to non-deterministic values (before the PLC
	 * cycle)
	 * <li>Create the location that represents the <i>end of PLC cycle</i>,
	 * where the requirements shall be evaluated.
	 * </ul>
	 * 
	 * @param problem
	 *            Verification problem.
	 * @param result
	 *            Verification result.
	 * @param fields
	 *            Categorized fields (produced by
	 *            {@link #categorizeFields(CfaNetworkDeclaration, IParserLazyResult, AbstractRequirementSettings, IPlcverifLogger)}).
	 */
	public static void representPlcCycle(VerificationProblem problem, VerificationResult result,
			FieldsInCategories fields) {
		representPlcCycle(problem, result, fields.inputFields, fields.paramFields, fields.fixedFields, fields.lowerBounds, fields.upperBounds);
	}

	/**
	 * This method modifies the CFA stored in the given
	 * {@link VerificationProblem} such that it will represent the PLC cycle. It
	 * will:
	 * <ul>
	 * <li>Create a new main automaton,
	 * <li>Create an infinite loop in the automaton, which
	 * <ul>
	 * <li>Calls the previous main automaton (representing the program under
	 * verification),
	 * <li>Sets the input fields to non-deterministic values (before each call),
	 * <li>Sets the input fields with bound values to their given values (before
	 * each call).
	 * </ul>
	 * <li>Set the parameter fields to non-deterministic values (before the PLC
	 * cycle)
	 * <li>Create the location that represents the <i>end of PLC cycle</i>,
	 * where the requirements shall be evaluated.
	 * </ul>
	 * 
	 * @param problem
	 *            Verification problem.
	 * @param result
	 *            Verification result.
	 * @param inputFields
	 *            Fields to be represented as input fields.
	 * @param paramFields
	 *            Fields to be represented as parameter fields.
	 * @param fixedFields
	 *            List of input bindings to be enforced at the beginning of each
	 *            cycle.
	 */
	public static void representPlcCycle(VerificationProblem problem, VerificationResult result,
			List<DataRef> inputFields, List<DataRef> paramFields, List<InputBinding> fixedFields, List<InputBinding> lowerBounds, List<InputBinding> upperBounds) {
		Preconditions.checkArgument(problem.getModel() instanceof CfaNetworkDeclaration,
				"Requirement expected a CFA network declaration but instead got " + problem.getModel());
		Preconditions.checkNotNull(inputFields, "inputFields");
		Preconditions.checkNotNull(paramFields, "paramFields");
		Preconditions.checkNotNull(fixedFields, "fixedFields");
		Preconditions.checkNotNull(lowerBounds, "lowerBounds");
		Preconditions.checkNotNull(upperBounds, "upperBounds");

		result.setInputFields(Collections.unmodifiableList(inputFields));
		result.setParamFields(Collections.unmodifiableList(paramFields));
		result.setBindings(fixedFields.stream().collect(Collectors.toMap(it -> it.leftValue, it -> it.rightValue)));
		Map<DataRef, AbstractMap.SimpleEntry<InitialValue, ComparisonOperator>> bounds = lowerBounds.stream().collect(Collectors.toMap(it -> it.leftValue, it -> new AbstractMap.SimpleEntry<InitialValue, ComparisonOperator>(it.rightValue, ComparisonOperator.LESS_EQ)));
		bounds.putAll(lowerBounds.stream().collect(Collectors.toMap(it -> it.leftValue, it -> new AbstractMap.SimpleEntry<InitialValue, ComparisonOperator>(it.rightValue, ComparisonOperator.GREATER_EQ))));
		result.setBounds(bounds);
		
		CfaNetworkDeclaration cfaNetwork = (CfaNetworkDeclaration) problem.getModel();
		AutomatonDeclaration oldMainAutomaton = cfaNetwork.getMainAutomaton();
		DataRef oldMainContext = cfaNetwork.getMainContext();
		problem.setEntryBlockContext(oldMainContext);

		// Create new main automaton
		DataStructure loopDs = factory.createDataStructure("VerificationLoopDS", cfaNetwork.getRootDataStructure());
		AutomatonDeclaration loopAutomaton = factory.createAutomatonDeclaration("VerificationLoop", cfaNetwork, loopDs);
		Field loopLocalField = factory.createField("verificationLoop", cfaNetwork.getRootDataStructure(),
				factory.createDataStructureRef(loopDs));

		// Set new main automaton
		FieldRef loopContext = factory.createFieldRef(loopLocalField);
		cfaNetwork.setMainAutomaton(loopAutomaton);
		cfaNetwork.setMainContext(loopContext);

		// Create initial and end locations
		Location init = factory.createInitialLocation("init", loopAutomaton);
		Location end = factory.createEndLocation("end", loopAutomaton);

		// Create initial assignments for parameters
		Location loopStartLoc = factory.createLocation("loop_start", loopAutomaton);
		AssignmentTransition paramTr = factory.createAssignmentTransition("t_params", loopAutomaton, init, loopStartLoc,
				factory.trueLiteral());

		for (DataRef param : paramFields) {
			factory.createAssignment(paramTr, copy(param), factory.createNondeterministic(copy(param.getType())));
		}
		result.currentStage().logInfo("Fields treated as parameters in the PLC cycle: %s",
				paramFields.stream().map(it -> CfaToString.toDiagString(it)).collect(Collectors.joining(", ")));

		// Create BoC location
		Location bocLoc = factory.createLocation("prepare_BoC", loopAutomaton);
		bocLoc.setFrozen(true);
		problem.setBocLocation(bocLoc);

		// Start loop and assign inputs and fixed values
		Location callStartLoc = factory.createLocation("l_main_call", loopAutomaton);
		AssignmentTransition setBoCTr = factory.createAssignmentTransition("set_BoC", loopAutomaton, loopStartLoc, 
				bocLoc, factory.trueLiteral());

		for (DataRef input : inputFields) {
			factory.createAssignment(setBoCTr, copy(input), factory.createNondeterministic(copy(input.getType())));
			if (input instanceof FieldRef) {
				for (VariableComparison comparison : ((FieldRef)input).getField().getBounds()) {
					factory.createAssumeBound(setBoCTr, copy(input), copy(comparison));
				}
			} else {
				// TODO: not implemented yet
			}
		}
		result.currentStage().logInfo("Fields treated as inputs in the PLC cycle: %s",
				inputFields.stream().map(it -> CfaToString.toDiagString(it)).collect(Collectors.joining(", ")));
		
		factory.createAssignmentTransition("t_inputs", loopAutomaton, bocLoc, callStartLoc, factory.trueLiteral());

		representInputBindings(setBoCTr, fixedFields, cfaNetwork, result, oldMainAutomaton, oldMainContext);
		
		representInputBounds(setBoCTr, bounds, cfaNetwork, result, oldMainAutomaton, oldMainContext);

		// Call main automaton
		Location callEndLoc = factory.createLocation("callEnd", loopAutomaton);
		CallTransition mainCallTr = factory.createCallTransition("main_call", loopAutomaton, callStartLoc, callEndLoc,
				factory.trueLiteral());

		factory.createCall(mainCallTr, oldMainAutomaton, oldMainContext);
		
		// Create EoC location
		Location eocLoc = factory.createLocation("prepare_EoC", loopAutomaton);
		eocLoc.setFrozen(true);
		problem.setEocLocation(eocLoc);
		AssignmentTransition setEoCTr = factory.createAssignmentTransition("set_EoC", loopAutomaton, callEndLoc, eocLoc, factory.trueLiteral());
		
		createGlobalTimeIncrease(problem, result, setEoCTr, cfaNetwork);

		// Restart loop
		AssignmentTransition restartTr = factory.createAssignmentTransition("restart", loopAutomaton, eocLoc,
				loopStartLoc, factory.trueLiteral());
		restartTr.setFrozen(true);

		// Dummy transition to end location
		AssignmentTransition loopExit = factory.createAssignmentTransition("end", loopAutomaton, loopStartLoc, end,
				factory.falseLiteral());

		// Add WHILE loop annotation
		factory.createWhileLoopLocationAnnotation(loopStartLoc, setBoCTr, restartTr, loopExit);
	}
	
	private static void createGlobalTimeIncrease(VerificationProblem problem, VerificationResult result, AssignmentTransition setEoCTr, CfaNetworkDeclaration cfaNetwork) {
		Preconditions.checkArgument(problem.getEocLocation() != null,
				"The verification result should already contain the reference to the EoC location to create the EoC field.");
		Preconditions.checkArgument(problem.getModel() instanceof CfaNetworkDeclaration,
				"Requirement expected a CFA network declaration but instead got " + problem.getModel());
		CfaDeclarationSafeFactory factory = CfaDeclarationSafeFactory.INSTANCE;
		
		final String T_CYCLE_NAME = "T_CYCLE";
		final String GLOBAL_TIME_NAME = "__GLOBAL_TIME";
		Optional<Field> tCycle = findGlobalField(cfaNetwork, T_CYCLE_NAME);
		Optional<Field> globalTime = findGlobalField(cfaNetwork, GLOBAL_TIME_NAME);
		
		
		if (!tCycle.isEmpty() && !globalTime.isEmpty()){
			FieldRef globalTimeAssigneeFieldRef = factory.createFieldRef(globalTime.get());
			FieldRef globalTimeAssignmentFieldRef = factory.createFieldRef(globalTime.get());
			FieldRef tCycleAssignmentFieldRef = factory.createFieldRef(tCycle.get());
			
			for (VariableAssignment assignment : factory.createAssignment(
					setEoCTr,
					globalTimeAssigneeFieldRef,
					factory.plus(
							globalTimeAssignmentFieldRef,
							factory.withTypeConversionIfNeeded(tCycleAssignmentFieldRef, globalTime.get().getType())
					)
				)) {
				assignment.setFrozen(false);
			}
			
			result.getAdditionalVariables().addAll(Set.of(globalTimeAssigneeFieldRef, globalTimeAssignmentFieldRef, tCycleAssignmentFieldRef));
		}
	}
	
	
	public static void addAssumptionAtBoC(VerificationProblem problem, Expression assumption) {
		Preconditions.checkArgument(problem.getBocLocation().getOutgoing().get(0) instanceof AssignmentTransition, "This should be an assignment transition.");
		
		AssignmentTransition setBoCTr = (AssignmentTransition) problem.getBocLocation().getIncoming().get(0);		
		factory.createAssumption(setBoCTr, assumption);
	}
	
	
	
	public static void insertPropertyAutomaton(AutomatonDeclaration parent, AutomatonDeclaration insert, DataRef insertContext, Transition insertPlace) {
		// Call property automaton
		Location propertyCallStartLoc = factory.createLocation("propertyCallStart", parent);
		Location propertyCallEndLoc = factory.createLocation("propertyCallEnd", parent);
		Preconditions.checkArgument(insertPlace instanceof AssignmentTransition,
				"Transition is expected to be an Assignment.");
		
		CfaCollectionDeleter transitionDeleter = new CfaCollectionDeleter();
		transitionDeleter.registerForDeletion(insertPlace);
		
		AssignmentTransition replacerTransitionIncoming = (AssignmentTransition) TransitionBuilder.builderBetween(insertPlace.getSource(), propertyCallStartLoc).condition(copy(insertPlace.getCondition())).build();
		replacerTransitionIncoming.getAssignments().addAll(((AssignmentTransition)insertPlace).getAssignments());
		replacerTransitionIncoming.getAnnotations().addAll(insertPlace.getAnnotations());
		
		CallTransition propertyCallTr = factory.createCallTransition("property_call", parent, propertyCallStartLoc, propertyCallEndLoc, factory.trueLiteral());
		factory.createCall(propertyCallTr, insert, copy(insertContext));
		
		TransitionBuilder.builderBetween(propertyCallEndLoc, insertPlace.getTarget()).build();
		
		transitionDeleter.deleteAll();		
	}
	
	
	public static void insertMonitorBeforeLoop(VerificationProblem problem, List<VariableAssignmentSkeleton> assignments) {
		// Call property automaton
		Location monitorCallStart = factory.createLocation("startMonitorInit", problem.getModel().getMainAutomaton());
		Location monitorCallEnd = factory.createLocation("endMonitorInit", problem.getModel().getMainAutomaton());
		
		Location initialLocation =  problem.getModel().getMainAutomaton().getInitialLocation();
		CfaCollectionDeleter transitionDeleter = new CfaCollectionDeleter();
		for (Transition transition : initialLocation.getOutgoing()) {
			transitionDeleter.registerForDeletion(transition);
			Transition replacerTransitionIncoming = TransitionBuilder.builderBetween(monitorCallEnd, transition.getTarget()).condition(copy(transition.getCondition())).build();
			if (transition instanceof AssignmentTransition) { 
				((AssignmentTransition)replacerTransitionIncoming).getAssignments().addAll(((AssignmentTransition)transition).getAssignments());
			}
			replacerTransitionIncoming.getAnnotations().addAll(transition.getAnnotations());
		}
		
		
		int i = 0;
		Location currentStart = monitorCallStart;
		for (VariableAssignmentSkeleton assignment : assignments) {
			Location loc = factory.createLocation("monitorInit_"+i, problem.getModel().getMainAutomaton());
			TransitionBuilder.builderBetween(currentStart, loc).addAssignment(assignment).name("monitorAssignments"+i).build();
			currentStart = loc;
		}
		TransitionBuilder.builderBetween(currentStart, monitorCallEnd).name("monitorAssignmentsEnd").build();
		
		TransitionBuilder.builderBetween(initialLocation, monitorCallStart).name("initToMonitor").build();
		
		transitionDeleter.deleteAll();
	}
	
	public static void insertMonitorAtEoC(VerificationProblem problem, List<VariableAssignmentSkeleton> assignments) {		
		// Call property automaton
		Location monitorCallStart = factory.createLocation("monitorCallStartEoC", problem.getModel().getMainAutomaton());
		Location monitorCallEnd = factory.createLocation("monitorCallEndEoC", problem.getModel().getMainAutomaton());
		
		
		CfaCollectionDeleter transitionDeleter = new CfaCollectionDeleter();
		for (Transition transition : problem.getEocLocation().getIncoming()) {
			transitionDeleter.registerForDeletion(transition);
			Transition replacerTransitionIncoming = TransitionBuilder.builderBetween(transition.getSource(), monitorCallStart).condition(copy(transition.getCondition())).build();
			if (transition instanceof AssignmentTransition) { 
				((AssignmentTransition)replacerTransitionIncoming).getAssignments().addAll(((AssignmentTransition)transition).getAssignments());
			}
			replacerTransitionIncoming.getAnnotations().addAll(transition.getAnnotations());
		}
		
		int i = 0;
		Location currentStart = monitorCallStart;
		for (VariableAssignmentSkeleton assignment : assignments) {
			Location loc = factory.createLocation("monitorBoC_"+i, problem.getModel().getMainAutomaton());
			TransitionBuilder.builderBetween(currentStart, loc).addAssignment(assignment).name("monitorAssignments"+i).build();
			currentStart = loc;
		}
		TransitionBuilder.builderBetween(currentStart, monitorCallEnd).name("monitorAssignmentsEnd").build();
		
		TransitionBuilder.builderBetween(monitorCallEnd, problem.getEocLocation()).name("monitorToEoC").build();
		
		transitionDeleter.deleteAll();
	}
	
	public static void insertMonitorAtBoC(VerificationProblem problem, List<VariableAssignmentSkeleton> assignments) {
		// Call property automaton
		Location monitorCallStart = factory.createLocation("monitorCallStartBoC", problem.getModel().getMainAutomaton());
		Location monitorCallEnd = factory.createLocation("monitorCallEndBoC", problem.getModel().getMainAutomaton());
		
		
		CfaCollectionDeleter transitionDeleter = new CfaCollectionDeleter();
		for (Transition transition : problem.getBocLocation().getOutgoing()) {
			transitionDeleter.registerForDeletion(transition);
			Transition replacerTransitionIncoming = TransitionBuilder.builderBetween(monitorCallEnd, transition.getTarget()).condition(copy(transition.getCondition())).build();
			if (transition instanceof AssignmentTransition) { 
				((AssignmentTransition)replacerTransitionIncoming).getAssignments().addAll(((AssignmentTransition)transition).getAssignments());
			}
			replacerTransitionIncoming.getAnnotations().addAll(transition.getAnnotations());
		}
		
		
		int i = 0;
		Location currentStart = monitorCallStart;
		for (VariableAssignmentSkeleton assignment : assignments) {
			Location loc = factory.createLocation("monitorBoC_"+i, problem.getModel().getMainAutomaton());
			TransitionBuilder.builderBetween(currentStart, loc).addAssignment(assignment).name("monitorAssignments"+i).build();
			currentStart = loc;
		}
		TransitionBuilder.builderBetween(currentStart, monitorCallEnd).name("monitorAssignmentsEnd").build();
		
		TransitionBuilder.builderBetween(problem.getBocLocation(), monitorCallStart).name("boCToMonitor").build();
		
		transitionDeleter.deleteAll();
	}
	
	

	private static void representInputBindings(AssignmentTransition inputTr, List<InputBinding> fixedFields,
			CfaNetworkDeclaration cfaNetwork, VerificationResult result, AutomatonDeclaration oldMainAutomaton,
			DataRef oldMainContext) {
		for (InputBinding binding : fixedFields) {
			// Try to determine if this is an input variable. If not, we need to
			// remove the assignments to it (from the main automaton at least,
			// it would be difficult from other places as the model is not
			// instantiated yet) and warn the user.
			Field boundField = getReferredField(binding.leftValue);
			List<DirectionFieldAnnotation> dirAnnotations = CfaUtils.getAllAnnotationsOfType(boundField,
					DirectionFieldAnnotation.class);
			if (dirAnnotations.size() == 1) {
				DataDirection direction = dirAnnotations.get(0).getDirection();
				if (direction != DataDirection.INPUT && direction != DataDirection.INOUT) {
					List<VariableAssignment> amts = EmfHelper.getAllContentsOfType(oldMainAutomaton,
							VariableAssignment.class, false, it -> {
								return it.eContainer() instanceof Transition
										&& getReferredField(it.getLeftValue()) == boundField
										&& CfaUtils.dataRefEquals(
												factory.prependDataRef(copy(oldMainContext), copy(it.getLeftValue())),
												binding.leftValue);
							});
					int removedAmts = amts.size();

					if (removedAmts > 0) {
						// If there is anything to remove:
						result.currentStage().logWarning(
								"The '%s=%s' binding restricts the value of a non-input variable. Therefore assignments to '%s' (%s assignments) have been removed from the complete model, however, this may result in an incorrect model. Check the model and your assumptions.",
								CfaToString.toDiagString(binding.leftValue),
								CfaToString.toDiagString(binding.rightValue),
								CfaToString.toDiagString(binding.leftValue), removedAmts);

						for (VariableAssignment amt : amts) {
							result.currentStage().logInfo("Removed assignment due to the '%s=%s' binding: %s := %s.",
									CfaToString.toDiagString(binding.leftValue),
									CfaToString.toDiagString(binding.rightValue),
									CfaToString.toDiagString(amt.getLeftValue()),
									CfaToString.toDiagString(amt.getRightValue()));
						}

						CfaUtils.deleteEachElement(amts);
					}
				}
			}

			// Creation of the assignment that represents the binding
			factory.createAssignment(inputTr, copy(binding.leftValue),
					factory.createTypeConversionIfNeeded(copy(binding.rightValue), binding.leftValue.getType()));
		}

		result.currentStage().logInfo(
				"Fixed fields (re-initialized to the given value at the beginning of each cycle): %s",
				fixedFields.stream().map(
						it -> CfaToString.toDiagString(it.leftValue) + "=" + CfaToString.toDiagString(it.rightValue))
						.collect(Collectors.joining(", ")));
	}
	
	
	private static void representInputBounds(AssignmentTransition inputTr, Map<DataRef, AbstractMap.SimpleEntry<InitialValue, ComparisonOperator>> bounds,
			CfaNetworkDeclaration cfaNetwork, VerificationResult result, AutomatonDeclaration oldMainAutomaton,
			DataRef oldMainContext) {
		for (var entry : bounds.entrySet()) {
			factory.createAssumeBound(inputTr, copy(entry.getKey()), factory.createTypeConversionIfNeeded(copy(entry.getValue().getKey()), entry.getKey().getType()), entry.getValue().getValue());
		}

		result.currentStage().logInfo(
				"Bounded fields (bounding information given to model checking engine at the beginning of each cycle): %s",
				bounds.entrySet().stream().map(it -> CfaToString.toDiagString(it.getKey()) + "" + CfaToString.toDiagString(it.getValue().getKey())).collect(Collectors.joining(", ")));
	}

	/**
	 * Creates and returns a field that is true if and only if the currently
	 * active location is the one representing the end of PLC cycle
	 * ({@link VerificationProblem#getEocLocation()}). The method creates the
	 * necessary assignments too.
	 * 
	 * @param problem
	 *            Verification problem.
	 * @param result
	 *            Verification result.
	 * @return The EoC field.
	 */
	public static Field createEocField(VerificationProblem problem, VerificationResult result) {
		Preconditions.checkArgument(problem.getEocLocation() != null,
				"The verification result should already contain the reference to the EoC location to create the EoC field.");
		Preconditions.checkArgument(problem.getModel() instanceof CfaNetworkDeclaration,
				"Requirement expected a CFA network declaration but instead got " + problem.getModel());
		CfaNetworkDeclaration cfaNetwork = (CfaNetworkDeclaration) problem.getModel();
		AutomatonDeclaration loopAutomaton = cfaNetwork.getMainAutomaton();
		CfaDeclarationSafeFactory factory = CfaDeclarationSafeFactory.INSTANCE;

		// Create EoC variable
		Field eocField = factory.createField(EOC_FIELD_NAME, cfaNetwork.getRootDataStructure(),
				factory.createBoolType());
		eocField.setFrozen(true);
		factory.createInternalGeneratedFieldAnnotation(eocField);
		factory.createInitialAssignment(eocField, factory.falseLiteral());
		FieldRef eocFieldRef = factory.createFieldRef(eocField);
		eocFieldRef.setFrozen(true);

		// Set EoC to true just before the 'EoC' location
		AssignmentTransition setEoCTr = findOrCreateSetEocTransition(problem.getEocLocation());
		setEoCTr.setFrozen(true);
		for (VariableAssignment assignment : factory.createAssignment(setEoCTr, copy(eocFieldRef),
				factory.trueLiteral())) {
			assignment.setFrozen(true);
		}

		// Set EoC to false just after the 'EoC' location
		AssignmentTransition clearEoCTr = findOrCreateClearEocTransition(loopAutomaton, problem.getEocLocation());
		clearEoCTr.setFrozen(true);
		for (VariableAssignment assignment : factory.createAssignment(clearEoCTr, copy(eocFieldRef),
				factory.falseLiteral())) {
			assignment.setFrozen(true);
		}
		
		result.getAdditionalVariables().add(eocFieldRef);
		return eocField;
	}

	/**
	 * Creates and returns a field that is true if and only if the currently
	 * active location is the one representing the beginning of PLC cycle
	 * ({@link VerificationProblem#getBocLocation()}). The method creates the
	 * necessary assignments too.
	 * 
	 * @param problem
	 *            Verification problem.
	 * @param result
	 *            Verification result.
	 * @return The BoC field.
	 */
	public static Field createBocField(VerificationProblem problem, VerificationResult result) {
		Preconditions.checkArgument(problem.getBocLocation() != null,
				"The verification result should already contain the reference to the BoC location to create the BoC field.");
		Preconditions.checkArgument(problem.getModel() instanceof CfaNetworkDeclaration,
				"Requirement expected a CFA network declaration but instead got " + problem.getModel());
		CfaNetworkDeclaration cfaNetwork = (CfaNetworkDeclaration) problem.getModel();
		AutomatonDeclaration loopAutomaton = cfaNetwork.getMainAutomaton();
		CfaDeclarationSafeFactory factory = CfaDeclarationSafeFactory.INSTANCE;

		// Create BoC variable
		Field bocField = factory.createField(BOC_FIELD_NAME, cfaNetwork.getRootDataStructure(),
				factory.createBoolType());
		bocField.setFrozen(true);
		factory.createInternalGeneratedFieldAnnotation(bocField);
		factory.createInitialAssignment(bocField, factory.falseLiteral());
		FieldRef bocFieldRef = factory.createFieldRef(bocField);
		bocFieldRef.setFrozen(true);

		// Set BoC to true just before the 'BoC' location
		AssignmentTransition setBoCTr = findOrCreateSetBocTransition(problem.getBocLocation());
		setBoCTr.setFrozen(true);
		for (VariableAssignment assignment : factory.createAssignment(setBoCTr, copy(bocFieldRef), factory.trueLiteral())) {
			assignment.setFrozen(true);
		}

		// Set BoC to false just after the 'BoC' location
		AssignmentTransition clearBoCTr = findOrCreateClearBocTransition(loopAutomaton, problem.getBocLocation());
		clearBoCTr.setFrozen(true);
		for (VariableAssignment assignment : factory.createAssignment(clearBoCTr, copy(bocFieldRef),
				factory.falseLiteral())) {
			assignment.setFrozen(true);
		}

		result.getAdditionalVariables().add(bocFieldRef);
		return bocField;
		
	}
	
	
	/**
	 * Creates and returns a field that is true if and only if the currently
	 * active location is the one representing the beginning of PLC cycle
	 * ({@link VerificationProblem#getBocLocation()}). The method creates the
	 * necessary assignments too.
	 * 
	 * @param problem
	 *            Verification problem.
	 * @param result
	 *            Verification result.
	 * @return The BoC field.
	 */
	public static Field createInternalField(VerificationProblem problem, String fieldName, Type fieldType) {
		Preconditions.checkArgument(problem.getModel() instanceof CfaNetworkDeclaration,
				"Requirement expected a CFA network declaration but instead got " + problem.getModel());
		CfaNetworkDeclaration cfaNetwork = (CfaNetworkDeclaration) problem.getModel();
		CfaDeclarationSafeFactory factory = CfaDeclarationSafeFactory.INSTANCE;

		// Create BoC variable
		Field timerField = factory.createField(fieldName, cfaNetwork.getRootDataStructure(),
				copy(fieldType));
		// TODO: we probably do not want this frozen but to be reviewed
		//timerField.setFrozen(true);
		factory.createInternalGeneratedFieldAnnotation(timerField);
		factory.createInitialAssignment(timerField, factory.createDefaultLiteral(copy(fieldType)));
		return timerField;
	}
	
	public static Optional<Field> findGlobalField(CfaNetworkDeclaration cfaNetwork, String fieldName) {
		List<Field> tCycleFields = cfaNetwork.getRootDataStructure().getFields().stream()
				.filter(it -> it.getName().equalsIgnoreCase(fieldName)).collect(Collectors.toList());
		if (tCycleFields.isEmpty()) {
			return Optional.empty();
//		} else if (tCycleFields.size() > 1) {
//			throw new Step7AstTransformationException(
//					String.format("Multiple '%s' variables have been found.", fieldName));
		} else {
			Preconditions.checkState(tCycleFields.size() == 1);
			return Optional.of(tCycleFields.get(0));
		}
	}

	
	
	/**
	 * Creates a timer tree
	 * 
	 *
	 *        [cond]
	 * +----+ time +=incr(1) +----+
	 * |    +--------------->|    |
	 * |    |                |    |
	 * +-+--+       +--------+--+-+
	 *   |          |           |
	 *   |[!cond]   |[!time]    |[time]
	 *   |time := 0 |           |trigger := true (3)
	 *   |trig := f |           |
	 *   |  (2)     |           |
	 *   v          |           v
	 * +----+<------+        +----+
	 * |    |                |    |
	 * |    |<---------------+    |
	 * +----+     [true]     +----+
	 * 
	 * @param problem
	 *            Verification problem.
	 * @param result
	 *            Verification result.
	 * @return The BoC field.
	 */
	public static AutomatonDeclaration createTimerAutomaton(VerificationProblem problem, DataStructure loopDs, Expression incrementCondition, Expression goalCondition) {
		Preconditions.checkArgument(problem.getModel() instanceof CfaNetworkDeclaration,
				"Requirement expected a CFA network declaration but instead got " + problem.getModel());
		CfaNetworkDeclaration cfaNetwork = (CfaNetworkDeclaration) problem.getModel();
		CfaDeclarationSafeFactory factory = CfaDeclarationSafeFactory.INSTANCE;
		
		Optional<Field> tCycle = findGlobalField(cfaNetwork, "T_CYCLE");
		if(tCycle.isEmpty()) {
			tCycle = Optional.of(createInternalField(problem, "T_CYCLE", factory.createIntType(true, 32)));
			factory.createDirectionFieldAnnotation(tCycle.get(), DataDirection.INPUT);
		}
		Preconditions.checkArgument(tCycle.isPresent(),
				"For the pattern it is required to set the T-CYCLE in advanced requirement settings.");
		
		AutomatonDeclaration timeAutomaton = factory.createAutomatonDeclaration("PropertyTree", cfaNetwork, loopDs);

		// Create BoC variable
		Field timeField = createInternalField(problem, "timer_time", factory.createIntType(true, 32));
		Field triggerField = createInternalField(problem, "TIMER_TRIGGERED", factory.createBoolType());
		
		
		FieldRef timeFieldRef1 = factory.createFieldRef(timeField);
		FieldRef timeFieldRef2 = factory.createFieldRef(timeField);
		FieldRef triggerFieldRef1 = factory.createFieldRef(triggerField);
		FieldRef triggerFieldRef2 = factory.createFieldRef(triggerField);
		//timeFieldRef1.setFrozen(true);
		//timeFieldRef2.setFrozen(true);
		//triggerFieldRef.setFrozen(true);
		
		
		// Set BoC to true just before the 'BoC' location
		Location timerLoc2 = factory.createLocation("timer_input_true", timeAutomaton);
		Location timerLoc3 = factory.createLocation("timer_count_true", timeAutomaton);
		
		Location timerLoc1 = factory.createInitialLocation("timer_start", timeAutomaton);
		Location timerLoc4 = factory.createEndLocation("timer_end", timeAutomaton);
		
		VariableAssignmentSkeleton assignment1 = VariableAssignmentSkeleton.create(timeFieldRef1, factory.plus(factory.createFieldRef(timeField), factory.createTypeConversionIfNeeded(factory.createFieldRef(tCycle.get()), timeFieldRef1.getType())));
		VariableAssignmentSkeleton assignment2 = VariableAssignmentSkeleton.create(timeFieldRef2, factory.createTypeConversionIfNeeded(factory.createIntLiteral(0, (IntType)copy(timeField.getType())), timeFieldRef2.getType()));
		VariableAssignmentSkeleton assignment3 = VariableAssignmentSkeleton.create(triggerFieldRef1, factory.createTypeConversionIfNeeded(factory.createBoolLiteral(true), triggerFieldRef1.getType()));
		VariableAssignmentSkeleton assignment4 = VariableAssignmentSkeleton.create(triggerFieldRef2, factory.createTypeConversionIfNeeded(factory.createBoolLiteral(false), triggerFieldRef2.getType()));
		
		TransitionBuilder.builderBetween(timerLoc1, timerLoc2).condition(incrementCondition).addAssignment(assignment1).build();
		TransitionBuilder.builderBetween(timerLoc1, timerLoc4).condition(factory.neg(incrementCondition)).addAssignment(assignment2).addAssignment(assignment4).build();
		TransitionBuilder.builderBetween(timerLoc2, timerLoc3).condition(factory.geq(factory.createFieldRef(timeField), factory.createTypeConversionIfNeeded(copy(goalCondition), timeField.getType()))).addAssignment(assignment3).build();
		TransitionBuilder.builderBetween(timerLoc2, timerLoc4).condition(factory.neg(factory.geq(factory.createFieldRef(timeField), factory.createTypeConversionIfNeeded(copy(goalCondition), timeField.getType())))).build();
		TransitionBuilder.builderBetween(timerLoc3, timerLoc4).condition(factory.trueLiteral()).build();
				
		return timeAutomaton;
	}
	
	/**
	 * Finds or creates a transition which is appropriate for the
	 * {@code BoC := TRUE;} assignment. Returns an assignment transition which
	 * is the only assignment transition entering the given {@code bocLocation}.
	 * If there is already one, it returns that transition. If there is no such
	 * transition, it creates a new location and a new transition.
	 */
	private static AssignmentTransition findOrCreateSetBocTransition(Location bocLocation) {
		Preconditions.checkNotNull(bocLocation, "bocLocation");
		if (bocLocation.getIncoming().size() == 1 && bocLocation.getIncoming().get(0) instanceof AssignmentTransition) {
			return (AssignmentTransition) bocLocation.getIncoming().get(0);
		} else {
			throw new UnsupportedOperationException("Not implemented yet.");
			// we need to create a new location and a new transition

		}
	}

	/**
	 * Finds or creates a transition which is appropriate for the
	 * {@code BoC := FALSE;} assignment. Returns an assignment transition which
	 * is the only assignment transition leaving the given {@code bocLocation}.
	 * If there is already one, it returns that transition. If there is no such
	 * transition, it creates a new location and a new transition.
	 */
	private static AssignmentTransition findOrCreateClearBocTransition(AutomatonDeclaration loopAutomaton,
			Location bocLocation) {
		Preconditions.checkNotNull(bocLocation, "bocLocation");
		// We discard the transitions where the condition is `[FALSE]`
		List<Transition> outgoing = bocLocation.getOutgoing().stream()
				.filter(it -> !ExprUtils.isFalseLiteral(it.getCondition())).collect(Collectors.toList());
		if (outgoing.size() == 1 && outgoing.get(0) instanceof AssignmentTransition) {
			return (AssignmentTransition) outgoing.get(0);
		} else {
			throw new UnsupportedOperationException("Not implemented yet.");
			// we need to create a new location and a new transition
		}
	}
	
	/**
	 * Finds or creates a transition which is appropriate for the
	 * {@code EoC := TRUE;} assignment. Returns an assignment transition which
	 * is the only assignment transition entering the given {@code eocLocation}.
	 * If there is already one, it returns that transition. If there is no such
	 * transition, it creates a new location and a new transition.
	 */
	private static AssignmentTransition findOrCreateSetEocTransition(Location eocLocation) {
		Preconditions.checkNotNull(eocLocation, "eocLocation");
		if (eocLocation.getIncoming().size() == 1 && eocLocation.getIncoming().get(0) instanceof AssignmentTransition) {
			return (AssignmentTransition) eocLocation.getIncoming().get(0);
		} else {
			throw new UnsupportedOperationException("Not implemented yet.");
			// we need to create a new location and a new transition

		}
	}

	/**
	 * Finds or creates a transition which is appropriate for the
	 * {@code EoC := FALSE;} assignment. Returns an assignment transition which
	 * is the only assignment transition leaving the given {@code eocLocation}.
	 * If there is already one, it returns that transition. If there is no such
	 * transition, it creates a new location and a new transition.
	 */
	private static AssignmentTransition findOrCreateClearEocTransition(AutomatonDeclaration loopAutomaton,
			Location eocLocation) {
		Preconditions.checkNotNull(eocLocation, "eocLocation");
		// We discard the transitions where the condition is `[FALSE]`
		List<Transition> outgoing = eocLocation.getOutgoing().stream()
				.filter(it -> !ExprUtils.isFalseLiteral(it.getCondition())).collect(Collectors.toList());
		if (outgoing.size() == 1 && outgoing.get(0) instanceof AssignmentTransition) {
			return (AssignmentTransition) eocLocation.getOutgoing().get(0);
		} else {
			throw new UnsupportedOperationException("Not implemented yet.");
			// we need to create a new location and a new transition
		}
	}

	public static List<DataRef> parseFields(IParserLazyResult model, List<String> fields) throws AtomParsingException {
		List<DataRef> ret = new ArrayList<>();
		for (String field : fields) {
			AtomicExpression atom = model.parseAtom(field);
			Preconditions.checkArgument(atom instanceof DataRef,
					String.format("Expected a reference to field %s but received %s.", field, toDiagString(atom)));
			Preconditions.checkState(atom.getType() instanceof ElementaryType,
					"Parameter and input fields must be of elementary types.");
			ret.add((DataRef) atom);
		}
		return ret;
	}

	public static List<InputBinding> parseAssignments(IParserLazyResult model, List<InputBindingDesc> assignments,
			IPlcverifLogger logger) throws AtomParsingException {
		Preconditions.checkNotNull(model, "model");
		Preconditions.checkNotNull(assignments, "assignments");
		Preconditions.checkNotNull(logger, "logger");

		List<InputBinding> ret = new ArrayList<>();
		for (InputBindingDesc binding : assignments) {
			AtomicExpression leftValue;
			try {
				leftValue = model.parseAtom(binding.input);
			} catch (AtomParsingException e) {
				logger.logError(String.format(
						"Unable to set binding for '%s'. Variable is not found or maybe it is out of the scope of the verification (especially if this is a global variable).",
						binding.input), e);
				continue;
			}

			Preconditions.checkArgument(leftValue instanceof DataRef, String.format(
					"Expected a reference to field '%s' but received %s.", binding.input, toDiagString(leftValue)));
			try {
				AtomicExpression rightValue = model.parseAtom(binding.value);
				Preconditions.checkArgument(rightValue instanceof Literal, String.format(
						"Expected a literal from %s but received %s.", binding.value, toDiagString(rightValue)));
				CfaTypeUtils.checkArgumentsHaveSameType(leftValue, rightValue);

				ret.add(new InputBinding((DataRef) leftValue, (InitialValue) rightValue));
			} catch (AtomParsingException e) {
				logger.logError(String.format("Unable to set binding for '%s'. The value '%s' cannot be parsed.",
								binding.input, binding.value), e);
				throw new IllegalArgumentException(String.format("Unable to set binding for '%s'. The value '%s' cannot be parsed.",
						binding.input, binding.value), e);
			}
		}
		return ret;
	}

	public static void includeUnmentionedInputs(List<DataRef> inputFields, List<DataRef> paramFields,
			List<InputBinding> fixedFields, CfaNetworkDeclaration model) {
		EObjectSet<DataRef> unmentionedInputs = new EObjectSet<>();

		// Add all fields marked in the model
		Collection<DataRef> modelInputFields = CfaDeclarationUtils.getAllInputFields(model);
		for (DataRef modelInputField : modelInputFields) {
			unmentionedInputs.addAll(CfaDeclarationUtils.getAllElementaryDataElements(modelInputField));
		}

		// Remove those that are already specified
		unmentionedInputs.removeAll(inputFields);
		unmentionedInputs.removeAll(paramFields);
		unmentionedInputs
				.removeAll(fixedFields.stream().map(binding -> binding.leftValue).collect(Collectors.toList()));

		// Add remaining to inputFields
		inputFields.addAll(unmentionedInputs);
	}

	public static void removeParamsFromInputs(List<DataRef> inputFields, List<DataRef> paramFields) {
		// PERF improve efficiency if needed
		List<DataRef> toBeRemoved = new ArrayList<>();
		for (DataRef input : inputFields) {
			innerLoop: for (DataRef param : paramFields) {
				if (EcoreUtil.equals(param, input)) {
					toBeRemoved.add(input); // to avoid modification during
											// iteration
					break innerLoop;
				} else if (CfaToString.toDiagString(param).equals(CfaToString.toDiagString(input))) {
					System.err.println("Strange: EcoreUtil.equals=false but diagnostic strings are identical for "
							+ CfaToString.toDiagString(input));
					System.err.println("--- Input: ---");
					System.err.println(EmfModelPrinter.print(input, SimpleEObjectToText.INSTANCE));
					System.err.println("--- Param: ---");
					System.err.println(EmfModelPrinter.print(param, SimpleEObjectToText.INSTANCE));
				}
			}
		}

		inputFields.removeAll(toBeRemoved);
	}
}
