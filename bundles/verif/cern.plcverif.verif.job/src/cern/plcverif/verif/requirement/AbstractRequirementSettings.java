/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.verif.requirement;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.settings.specific.AbstractSpecificSettings;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsComposite;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsList;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory;
import cern.plcverif.verif.requirement.PlcCycleRepresentation.InputBindingDesc;

public abstract class AbstractRequirementSettings extends AbstractSpecificSettings {
	public static class RequirementBoundsSettings extends AbstractSpecificSettings {
		@PlcverifSettingsList(name = "variable", description = "Variables to be bound.")
		List<String> astVar = new ArrayList<>();
		
		@PlcverifSettingsList(name = "value", description = "Values to be used in binding.")
		List<String> value = new ArrayList<>();
		
		public List<InputBindingDesc> getBindingsAsList() {
			List<InputBindingDesc> ret = new ArrayList<>();
			
			Preconditions.checkNotNull(this.astVar, "inputBindings.astVar");
			Preconditions.checkNotNull(this.value, "inputBindings.value");
			for (int i = 0; i < Math.min(this.astVar.size(), this.value.size()); i++) {
				if (this.astVar.get(i) != null && this.value.get(i) != null) {
					ret.add(new InputBindingDesc(this.astVar.get(i), this.value.get(i)));
				}
			}
			
			return ret;
		}
		
	}
		
	@PlcverifSettingsList(name = "inputs", description = "List of (PLC) variable names to be treated as inputs.", mandatory = PlcverifSettingsMandatory.OPTIONAL, serializeIfEmpty = true)
	private List<String> inputAstVars = new ArrayList<>();
	
	@PlcverifSettingsList(name = "params", description = "List of (PLC) variable names to be treated as parameters.", mandatory = PlcverifSettingsMandatory.OPTIONAL, serializeIfEmpty = true)
	private List<String> paramAstVars = new ArrayList<>();
	
	@PlcverifSettingsComposite(name = "bindings", description = "PLC variable value bindings.", mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private RequirementBoundsSettings inputBindings = null;
	
	@PlcverifSettingsComposite(name = "lowerBounds", description = "PLC variable value lower bindings.", mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private RequirementBoundsSettings lowerBounds = null;
	
	@PlcverifSettingsComposite(name = "upperBounds", description = "PLC variable value upper bindings.", mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private RequirementBoundsSettings upperBounds = null;
	
	public List<String> getInputAstVars() {
		return inputAstVars;
	}
	
	public List<String> getParamAstVars() {
		return paramAstVars;
	}
	
	/**
	 * Returns a list of input binding descriptions, without resolving their textual representation.
	 * @return List of input bindings. Never {@code null}. May be empty list.
	 */
	public List<InputBindingDesc> getInputBindings() {
		List<InputBindingDesc> ret = new ArrayList<>();
		
		if (inputBindings != null) {
			ret = inputBindings.getBindingsAsList();
		}
		
		return ret;
	}
	
	public List<InputBindingDesc> getLowerBounds() {
		List<InputBindingDesc> ret = new ArrayList<>();
		
		if (lowerBounds != null) {
			ret = lowerBounds.getBindingsAsList();
		}
		
		return ret;
	}
	
	public List<InputBindingDesc> getUpperBounds() {
		List<InputBindingDesc> ret = new ArrayList<>();
		
		if (upperBounds != null) {
			ret = upperBounds.getBindingsAsList();
		}
		
		return ret;
	}
}
