/******************************************************************************
 * (C) Copyright CERN 2018-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.verif.utils.diagnoser;

import java.util.Optional;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.formattedstring.BasicFormattedString;
import cern.plcverif.base.common.logging.IPlcverifLogger;
import cern.plcverif.base.models.cfa.transformation.assertion.AssertInliningResult;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.VerificationResult;
import cern.plcverif.verif.interfaces.data.cex.DeclarationCounterexample;

/**
 * Class that determines in which cycle were the inlined assertions violated and
 * which one(s), if available. The classes inheriting from this assertion
 * diagnoser are supposed to provide an assertion requirement
 * representation-specific way to determine the violation, if possible. The
 * human-readable diagnosis will be produced by the given
 * {@link AssertDiagnosisPrinter} instance.
 */
public abstract class AssertDiagnoser {
	protected final AssertInliningResult assertionInliningResult;
	protected final AssertDiagnosisPrinter printer;

	/**
	 * Creates a new assertion diagnosis object.
	 * 
	 * @param assertionInliningResult
	 *            Result of the assertion inlining
	 * @param printer
	 *            Diagnosis printer to be used to produce the human-readable
	 *            result
	 */
	public AssertDiagnoser(AssertInliningResult assertionInliningResult, AssertDiagnosisPrinter printer) {
		this.assertionInliningResult = Preconditions.checkNotNull(assertionInliningResult);
		this.printer = Preconditions.checkNotNull(printer);
	}

	/**
	 * Diagnoses the result corresponding to the given verification problem.
	 * 
	 * @param verifProblem
	 *            Verification problem
	 * @param result
	 *            Verification result
	 */
	public final void diagnoseResult(VerificationProblem verifProblem, VerificationResult result) {
		result.setResultDiagnosis(printer.noDiagnosisAvailable());
		IPlcverifLogger log = result.currentStage();

		// Log if it is not possible to provide a diagnosis
		if (assertionInliningResult == null) {
			log.logInfo(
					"Assertion diagnosis: Assertion diagnosis cannot be performed as the descriptors are not available.");
			return;
		}

		Optional<DeclarationCounterexample> counterexample = result.getDeclarationCounterexample();
		if (!counterexample.isPresent()) {
			log.logInfo(
					"Assertion diagnosis: Assertion diagnosis cannot be performed as the declaration counterexample is not available.");
			return;
		} else {
			BasicFormattedString diagnosis = diagnoseResultInternal(verifProblem, counterexample.get(), log);
			if (!diagnosis.isEmpty()) {
				result.setResultDiagnosis(diagnosis);
			}
		}
	}

	/**
	 * Diagnoses the result corresponding to the given verification problem. It
	 * should return the diagnosis as a basic formatted string. The
	 * implementation does not need to set the verification result's diagnosis
	 * field as it will be set by the caller.
	 * 
	 * @param verifProblem
	 *            Verification problem. Shall never be {@code null}.
	 * @param declarationCounterexample
	 *            Counterexample. Shall never be {@code null}.
	 * @param log
	 *            Log to be used.
	 * @return Diagnosis string.
	 */
	protected abstract BasicFormattedString diagnoseResultInternal(VerificationProblem verifProblem,
			DeclarationCounterexample declarationCounterexample, IPlcverifLogger log);
}
