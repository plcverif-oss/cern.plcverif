/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.utils.backend;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.logging.IPlcverifLogger;
import cern.plcverif.base.common.logging.PlatformLogger;
import cern.plcverif.base.common.process.ConsoleOutputGobbler;
import cern.plcverif.base.common.process.TimeoutExecutor;
import cern.plcverif.base.common.progress.ICanceling;

/**
 * Backend executor, that can execute a verification backend with the given
 * arguments, enforcing a given timeout. First, the executor shall be build
 * using the builder pattern. Methods are available to create a new backend
 * executor ({@link #create(List, int)}), to set a logger
 * ({@link #logger(IPlcverifLogger)}), to set the working directory for the
 * backend execution ({@link #execDirectory(File)}), and to define exclusion
 * patterns for the output gobblers ({@link #stdoutCommentLinePattern(String)}
 * and {@link #stderrCommentLinePattern(String)}) If the execution is
 * successful, the console output of the backend is made available
 * ({@link BackendExecutorResult#getStdout()},
 * {@link BackendExecutorResult#getStderr()}). If the execution of this program
 * is interrupted, a shutdown hook will make sure to shut down the spawned
 * process.
 *
 */
public final class BackendExecutor {
	/**
	 * The status of an execution.
	 */
	public enum BackendExecutionStatus {
		/**
		 * The backend execution ended successfully. This does not mean that the
		 * requirement was satisfied.
		 */
		Successful,
		/**
		 * The backend execution was prematurely terminated due to a timeout.
		 */
		Timeout,
		/**
		 * The backend execution was prematurely terminated due to user
		 * cancellation.
		 */
		Canceled,
		/**
		 * The backend execution was prematurely terminated due to an error. Not
		 * used for the moment.
		 */
		Error
	}

	/**
	 * Result of a backend execution, including the standard and error console
	 * output, and the status (success) of the execution.
	 */
	public static class BackendExecutorResult {
		private CharSequence stdout;
		private CharSequence stderr;
		private BackendExecutionStatus status;

		public BackendExecutorResult(CharSequence stdout, CharSequence stderr, BackendExecutionStatus status) {
			this.stdout = stdout;
			this.stderr = stderr;
			this.status = status;
		}

		/**
		 * Returns the standard output of the backend.
		 * 
		 * @return Content of standard output. Never {@code null}.
		 */
		public CharSequence getStdout() {
			return stdout == null ? "" : stdout;
		}

		/**
		 * Returns the error output of the backend.
		 * 
		 * @return Content of error output. Never {@code null}.
		 */
		public CharSequence getStderr() {
			return stderr == null ? "" : stderr;
		}

		public BackendExecutionStatus getStatus() {
			return status;
		}
	}

	public static final class BackendExecutionException extends Exception {
		private static final long serialVersionUID = 3067840551099408833L;

		private BackendExecutionException(String message) {
			super(message);
		}

		private BackendExecutionException(String message, Throwable throwable) {
			super(message, throwable);
		}
	}

	// Mandatory parameters
	private List<String> arguments;
	private int timeoutSec;

	// Optional parameters
	private IPlcverifLogger logger = IPlcverifLogger.NULL_LOGGER;
	private Optional<String> stdoutCommentLinePattern = Optional.empty();
	private Optional<String> stderrCommentLinePattern = Optional.empty();
	private Optional<File> execDirectory = Optional.empty();
	private Optional<ICanceling> canceler = Optional.empty();
	private boolean verbose = true;
	
	private boolean killForcibly = false; 

	private BackendExecutor(List<String> arguments, int timeoutSec) {
		this.arguments = arguments;
		this.timeoutSec = timeoutSec;
	}
	
	private BackendExecutor(List<String> arguments, int timeoutSec, boolean verbose) {
		this.arguments = arguments;
		this.timeoutSec = timeoutSec;
		this.verbose = verbose;
	}

	// Builder methods
	/**
	 * Creates a new backend executor builder.
	 * 
	 * Note: this does not create or start any process yet! See
	 * {@link #execute()}.
	 * 
	 * @param arguments
	 *            Arguments to be passed on to the verification backend. The
	 *            first element of the list must be the String representation of
	 *            the absolute path to the verification engine to be executed.
	 * @param timeoutSec
	 *            Timeout value to be enforced, in seconds.
	 */
	public static BackendExecutor create(List<String> arguments, int timeoutSec) {
		return new BackendExecutor(arguments, timeoutSec);
	}
	
	public static BackendExecutor create(List<String> arguments, int timeoutSec, boolean verbose) {
		return new BackendExecutor(arguments, timeoutSec, verbose);
	}

	/**
	 * Sets the logger for the backend process to be created.
	 * 
	 * @param logger
	 *            Logger to be used. Shall not be {@code null}.
	 */
	public BackendExecutor logger(IPlcverifLogger logger) {
		this.logger = Preconditions.checkNotNull(logger);
		return this;
	}

	/**
	 * Sets the canceler (that can request the cancellation of the backend
	 * execution) for the backend process to be created.
	 * 
	 * @param canceler
	 *            Canceller. Shall not be {@code null}.
	 */
	public BackendExecutor canceler(Optional<ICanceling> canceler) {
		this.canceler = Preconditions.checkNotNull(canceler);
		return this;
	}

	/**
	 * Sets the canceler (that can request the cancellation of the backend
	 * execution) for the backend process to be created.
	 * 
	 * @param canceler
	 *            Canceller. Shall not be {@code null}.
	 */
	public BackendExecutor canceler(ICanceling canceler) {
		this.canceler = Optional.of(Preconditions.checkNotNull(canceler));
		return this;
	}

	/**
	 * Sets the working directory for the backend process to be created.
	 * 
	 * @param execDirectory
	 *            Working directory to be used. Shall not be {@code null}.
	 */
	public BackendExecutor execDirectory(File execDirectory) {
		this.execDirectory = Optional.of(execDirectory);
		return this;
	}

	/**
	 * Sets the exclusion regex pattern for the standard output of the backend
	 * process to be created. Any text matching the given pattern will be
	 * excluded from {@link BackendExecutorResult#getStdout()}.
	 * 
	 * @param commentPattern
	 *            Exclusion regex pattern. Shall not be {@code null}.
	 */
	public BackendExecutor stdoutCommentLinePattern(String commentPattern) {
		this.stdoutCommentLinePattern = Optional.of(commentPattern);
		return this;
	}

	/**
	 * Sets the exclusion regex pattern for the error output of the backend
	 * process to be created. Any text matching the given pattern will be
	 * excluded from {@link BackendExecutorResult#getStderr()}.
	 * 
	 * @param commentPattern
	 *            Exclusion regex pattern. Shall not be {@code null}.
	 */
	public BackendExecutor stderrCommentLinePattern(String commentPattern) {
		this.stderrCommentLinePattern = Optional.of(commentPattern);
		return this;
	}

	/**
	 * Sets whether the created process needs to destroyed forcibly upon
	 * cancellation or timeout. Its default value is false.
	 * 
	 * @param value
	 *            Desired value.
	 */
	public BackendExecutor killForcibly(boolean value) {
		this.killForcibly = value;
		return this;
	}	
	
	/**
	 * Creates, starts and supervises the backend process, creates based on the
	 * settings.
	 * 
	 * @return The result of the execution.
	 * @throws BackendExecutionException
	 *             if the backend execution found an irrecoverable error during
	 *             execution (e.g., {@link IOException} was thrown by the
	 *             process).
	 */
	public BackendExecutorResult execute() throws BackendExecutionException {
		Process backendProcess = null;
		try {
			if (arguments.isEmpty()) {
				throw new BackendExecutionException("Unknown parameters to execute the backend.");
			}
			
			final ProcessBuilder processBuilder = new ProcessBuilder(arguments);
			processBuilder.directory(execDirectory.orElse(null));

			PlatformLogger
					.logInfo(String.format("Executing the verification backend... (timeout: %s sec)", timeoutSec));
			logger.logInfo(
					"Verification backend started with the following parameter list: " + String.join(" ", arguments));
			final Process process = processBuilder.start();
			backendProcess = process;
			
			TimeoutExecutor executor = TimeoutExecutor.execute(backendProcess, timeoutSec,
					canceler.orElse(ICanceling.NEVER_CANCELING_INSTANCE), killForcibly);

			Thread shutdownHook = new Thread() {
				@Override
				public void run() {
					if (process != null) {
						process.destroyForcibly();
					}
				}
			};
			Runtime.getRuntime().addShutdownHook(shutdownHook);

			ConsoleOutputGobbler stdOutput = new ConsoleOutputGobbler(backendProcess.getInputStream(), verbose);
			if (stdoutCommentLinePattern.isPresent()) {
				stdOutput.setCommentLineRegex(stdoutCommentLinePattern.get());
			}
			stdOutput.start();

			ConsoleOutputGobbler stdErr = new ConsoleOutputGobbler(backendProcess.getErrorStream(), verbose);
			stdErr.setPromtChar("#");
			if (stderrCommentLinePattern.isPresent()) {
				stdErr.setCommentLineRegex(stderrCommentLinePattern.get());
			}
			stdErr.start();
			backendProcess.waitFor();

			String stdOutputContent = stdOutput.getReadString();
			String stdErrContent = stdErr.getReadString();
			BackendExecutionStatus status = BackendExecutionStatus.Successful;

			if (executor.isTimeout()) {
				logger.logWarning(
						String.format("Timeout during the execution of verification backend. (%s sec)", timeoutSec));
				status = BackendExecutionStatus.Timeout;
			} else if (executor.isCanceled()) {
				logger.logWarning("The execution of verification backend was canceled.");
				status = BackendExecutionStatus.Canceled;
			}

			Runtime.getRuntime().removeShutdownHook(shutdownHook);
			// PERF Note: if there is an error, the shutdown hook will not be
			// removed, but it should not cause any problem.
			return new BackendExecutorResult(stdOutputContent, stdErrContent, status);
		} catch (final IOException e) {
			logger.logWarning("Error on verification backend execution. " + e.getMessage(), e);
			throw new BackendExecutionException("Error on verification backend execution.", e);
		} catch (final InterruptedException e) {
			logger.logWarning("The execution of the verification backend was interrupted.");
			throw new BackendExecutionException("The execution of the verification backend was interrupted.", e);
		} finally {
			if (backendProcess != null && backendProcess.isAlive()) {
				PlatformLogger.logInfo("Trying to destroy the backend process that is still alive...");
				backendProcess.destroyForcibly();
			}
		}
	}
}
