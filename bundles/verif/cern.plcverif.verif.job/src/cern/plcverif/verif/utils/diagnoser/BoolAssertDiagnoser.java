/******************************************************************************
 * (C) Copyright CERN 2018-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.verif.utils.diagnoser;

import java.util.List;
import java.util.Optional;

import org.eclipse.emf.ecore.util.EcoreUtil;

import cern.plcverif.base.common.formattedstring.BasicFormattedString;
import cern.plcverif.base.common.logging.IPlcverifLogger;
import cern.plcverif.base.models.cfa.transformation.assertion.AssertInliningResult;
import cern.plcverif.base.models.cfa.transformation.assertion.AssertInliningStrategy;
import cern.plcverif.base.models.expr.Literal;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.cex.DeclarationCounterexample;

/**
 * Class that provides diagnosis for a verification job that used the
 * {@link AssertInliningStrategy#BOOL_STRATEGY} assertion inlining strategy to
 * inline assertions.
 * 
 * It can determine in which step of the counterexample did a violation occur.
 */
public class BoolAssertDiagnoser extends AssertDiagnoser {

	/**
	 * Creates a new diagnosis object for assertions inlined using the Boolean strategy.
	 * 
	 * @param assertionInliningResult
	 *            Result of the assertion inlining
	 * @param printer
	 *            Diagnosis printer to be used to produce the human-readable
	 *            result
	 */
	public BoolAssertDiagnoser(AssertInliningResult assertionInliningResult, AssertDiagnosisPrinter printer) {
		super(assertionInliningResult, printer);
	}

	protected BasicFormattedString diagnoseResultInternal(VerificationProblem verifProblem,
			DeclarationCounterexample declarationCounterexample, IPlcverifLogger log) {
		BasicFormattedString diagnosis = new BasicFormattedString();
		List<Literal> errorFieldValues = declarationCounterexample.valuesFor(assertionInliningResult.getErrorField());

		Literal noneViolatedValue = AssertInliningStrategy.BOOL_STRATEGY.createNoAssertionViolatedValue();
		for (int i = 0; i < errorFieldValues.size(); i++) {
			String currentCexStepName = declarationCounterexample.getStep(i).getName();

			if (errorFieldValues.get(i) == null) {
				log.logInfo("Assertion diagnosis: The value for the error field cannot be found for step %s.",
						currentCexStepName);
				continue;
			}

			if (!EcoreUtil.equals(errorFieldValues.get(i), noneViolatedValue)) {
				printer.appendAssertionViolatedMessage(Optional.empty(), i, diagnosis);
			}
		}
		return diagnosis;
	}
}
