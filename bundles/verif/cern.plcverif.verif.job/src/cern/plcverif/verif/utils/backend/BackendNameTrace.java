/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.utils.backend;

import java.util.function.BiFunction;
import java.util.function.Function;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import cern.plcverif.base.models.cfa.cfabase.NamedElement;

public class BackendNameTrace<T extends NamedElement> {
	private BiMap<T, String> modelElementToName = HashBiMap.create();
	private Function<String, String> nameSanitizer;
	private BiFunction<String, Integer, String> nameAlternation; 
	private static final int MAX_NAME_TRIALS = 1_000_000;
	
	/**
	 * Basic name sanitizer that replaces all characters with '{@code _}',
	 * except A-Z, a-z, 0-9, _.
	 * @param name The name to be sanitized.
	 * @return The sanitized name.
	 */
	public static String basicNameSanitizer(String name) {
		return name.replaceAll("[^A-Za-z0-9_]", "_");
	}
	
	public BackendNameTrace() {
		this(Function.identity());
	}
	
	public BackendNameTrace(Function<String, String> nameSanitizer) {
		this(nameSanitizer, (baseName, seqNum) -> baseName + seqNum);
	}
	
	public BackendNameTrace(Function<String, String> nameSanitizer, BiFunction<String, Integer, String> nameAlternation) {
		this.nameSanitizer = nameSanitizer;
		this.nameAlternation = nameAlternation;
	}
	
	public String toName(T v) {
		if (modelElementToName.containsKey(v)) {
			// it already has an assigned name
			return modelElementToName.get(v);
		}
		
		String sanitizedName = nameSanitizer.apply(v.getName());
		String uniqueName = null;
		if (existingName(sanitizedName)) {
			
			for (int id = 1; id < MAX_NAME_TRIALS; id++) {
				String proposal = nameAlternation.apply(sanitizedName, id);
				if (!proposal.equals(nameSanitizer.apply(proposal))) {
					throw new IllegalStateException("The 'nameAlternation' returned a name proposal that is not valid according to the 'nameSanitizer'.");
				}
				if (!existingName(proposal)) {
					uniqueName = proposal;
					break;
				}
			}
			
			if (uniqueName == null) {
				throw new IllegalStateException(String.format("Impossible to find a unique name for '%s'. This may happen if the model is huge and there are lots of similar names to be made unique.", v.getName()));
			}
		} else {
			uniqueName = sanitizedName;
		}
		
		modelElementToName.put(v, uniqueName);
		return uniqueName;
	}
	
	public T toModelElement(String name) {
		return modelElementToName.inverse().get(name);
	}

	private boolean existingName(String name) {
		return isReservedWord(name) || modelElementToName.inverse().containsKey(name);
	}
	
	/**
	 * Returns true if the given name is a reserved word in the output language,
	 * thus should not be used for naming.
	 */
	protected boolean isReservedWord(String name) {
		return false;
	}
}
