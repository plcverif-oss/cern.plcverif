/*******************************************************************************
 * (C) Copyright CERN 2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.utils.diagnoser;

import java.util.Optional;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.formattedstring.BasicFormattedString;
import cern.plcverif.base.models.cfa.transformation.assertion.AssertInliningResult;
import cern.plcverif.base.models.cfa.transformation.assertion.AssertInliningResult.InlinedAssertion;
import cern.plcverif.verif.interfaces.data.cex.DeclarationCounterexample;

/**
 * Class that produces human-readable diagnosis for a verification execution
 * that checked the violation of inlined assertions.
 */
public abstract class AssertDiagnosisPrinter {
	/** Result of the assertion inlining. */
	protected final AssertInliningResult assertionInliningResult;

	/**
	 * Creates a new assertion diagnosis printer.
	 * 
	 * @param assertionInliningResult
	 *            Result of the assertion inlining.
	 */
	public AssertDiagnosisPrinter(AssertInliningResult assertionInliningResult) {
		this.assertionInliningResult = Preconditions.checkNotNull(assertionInliningResult);
	}

	/**
	 * Appends a message to the given diagnosis representing that the given
	 * assertion was violated in the given step.
	 * 
	 * @param assertion
	 *            The assertion that was violated. Shall not be {@code null}.
	 *            Can be {@link Optional#empty()} if it was not possible to
	 *            determine which assertion is violated.
	 * @param cexStep
	 *            The number of the counterexample step in which the violation
	 *            occurs. It can be used as argument in
	 *            {@link DeclarationCounterexample#getStep(int)}.
	 * @param previousDiagnosis
	 *            The previous diagnosis message (as formatted string) to be
	 *            appended. Shall not be {@code null}.
	 */
	public abstract void appendAssertionViolatedMessage(Optional<InlinedAssertion> assertion, int cexStep,
			BasicFormattedString previousDiagnosis);

	/**
	 * Returns a formatted string representing that no diagnosis is available.
	 * 
	 * @return 'No diagnosis available' representation
	 */
	public abstract BasicFormattedString noDiagnosisAvailable();
}
