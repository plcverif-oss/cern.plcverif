/******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions, refactoring
 *****************************************************************************/
package cern.plcverif.verif.job;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.textual.EmfModelPrinter;
import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.logging.IPlcverifLogger;
import cern.plcverif.base.common.logging.PlatformLogger;
import cern.plcverif.base.common.progress.ICanceling;
import cern.plcverif.base.common.progress.ICancelingProgressReporter;
import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsList;
import cern.plcverif.base.common.settings.SettingsUtils;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.base.interfaces.ICfaJob;
import cern.plcverif.base.interfaces.IParserLazyResult;
import cern.plcverif.base.interfaces.IReduction;
import cern.plcverif.base.interfaces.data.AbstractBasicJob;
import cern.plcverif.base.interfaces.data.CfaJobSettings;
import cern.plcverif.base.interfaces.data.JobMetadata;
import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.interfaces.data.ModelTasks;
import cern.plcverif.base.interfaces.data.result.JobStageTags;
import cern.plcverif.base.interfaces.exceptions.AstParsingException;
import cern.plcverif.base.interfaces.exceptions.CfaGenerationException;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.cfa.serialization.CfaToXmi;
import cern.plcverif.base.models.cfa.textual.CfaEObjectToText;
import cern.plcverif.base.models.cfa.trace.CfaInstantiationTrace;
import cern.plcverif.base.models.cfa.transformation.CallInliner;
import cern.plcverif.base.models.cfa.transformation.CfaInstantiator;
import cern.plcverif.base.models.cfa.transformation.CfaTransformationGoal;
import cern.plcverif.base.models.cfa.utils.CfaInstanceStatistics;
import cern.plcverif.base.models.cfa.utils.CfaUtils;
import cern.plcverif.base.models.cfa.validation.CfaValidation;
import cern.plcverif.base.models.cfa.visualization.CfaToDot;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.platform.utils.PlatformSettingsUtils;
import cern.plcverif.verif.interfaces.IBackend;
import cern.plcverif.verif.interfaces.IRequirement;
import cern.plcverif.verif.interfaces.IToolAgent;
import cern.plcverif.verif.interfaces.IVerificationReporter;
import cern.plcverif.verif.interfaces.data.ResultEnum;
import cern.plcverif.verif.interfaces.data.VerificationJobSettings;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.VerificationResult;

public class VerificationJob extends AbstractBasicJob implements ICfaJob {
	private JobMetadata metadata;
	private VerificationJobSettings settings = new VerificationJobSettings();
	private ModelTasks modelTasks;

	private IBackend backend;
	private IRequirement requirement;
	private List<IVerificationReporter> reporters = new ArrayList<>();
	private List<IToolAgent> toolAgents = new ArrayList<>();

	@Override
	public JobMetadata getMetadata() {
		return metadata;
	}

	@Override
	public void setMetadata(JobMetadata metadata) {
		this.metadata = metadata;
	}

	public VerificationJobSettings getSettings() {
		return settings;
	}

	public void setSettings(VerificationJobSettings settings) {
		this.settings = settings;
	}

	public ModelTasks getModelTasks() {
		return modelTasks;
	}

	@Override
	public void setModelTasks(ModelTasks modelTasks) {
		this.modelTasks = modelTasks;
	}

	public IBackend getBackend() {
		return backend;
	}

	public void setBackend(IBackend backend) {
		this.backend = backend;
	}

	public IRequirement getRequirement() {
		return requirement;
	}

	public void setRequirement(IRequirement requirement) {
		this.requirement = requirement;
	}

	public void setReporters(List<IVerificationReporter> reporters) {
		this.reporters = reporters;
	}

	public List<IVerificationReporter> getReporters() {
		return reporters;
	}
	
	public void setToolAgents(List<IToolAgent> toolAgents) {
		this.toolAgents = toolAgents;
	}

	public List<IToolAgent> getToolAgents() {
		return toolAgents;
	}

	@Override
	public SettingsElement retrieveSettings() {
		SettingsElement ret = PlatformSettingsUtils.retrieveSettings(metadata);
		ret.setSimpleAttribute(CfaJobSettings.OUTPUT_DIRECTORY, this.getOutputDirectory().toString());
		ret.overrideWith(PlatformSettingsUtils.retrieveSettings(modelTasks));

		SettingsElement jobSettings = SettingsElement.createRootElement(CfaJobSettings.JOB,
				VerificationJobExtension.CMD_ID);
		ret.setAttribute(CfaJobSettings.JOB, jobSettings);

		// Requirement settings
		SettingsElement reqSettings = Preconditions.checkNotNull(
				this.getRequirement().retrieveSettings(), "The retrieved requirement settings shall not be null.");
		logIfValueNotSet(reqSettings,
				"The requirement plug-in did not set its ID. This may make the retrieved settings unsuitable for repeating the measurements. This problem can only be addressed by the developer of the selected requirement plug-in.",
				PlatformLogger.INSTANCE);
		jobSettings.setAttribute(VerificationJobOptions.REQUIREMENT, reqSettings);
		

		// Backend settings
		SettingsElement backendSettings = Preconditions.checkNotNull(this.getBackend().retrieveSettings(),
				"The retrieved backend settings shall not be null.");
		logIfValueNotSet(backendSettings,
				"The backend plug-in did not set its ID. This may make the retrieved settings unsuitable for repeating the measurements. This problem can only be addressed by the developer of the selected backend plug-in.",
				PlatformLogger.INSTANCE);
		jobSettings.setAttribute(VerificationJobOptions.BACKEND, backendSettings);

		// Reporter settings
		SettingsList reportersSettings = jobSettings.addListAttribute(VerificationJobOptions.REPORTERS, this.getReporters());
		for (Settings item : reportersSettings.elements()) {
			if (item instanceof SettingsElement) {
				logIfValueNotSet((SettingsElement)item,
						"One of the verification reporters not set its ID. This may make the retrieved settings unsuitable for repeating the measurements. This problem can only be addressed by the developer of the selected reporter plug-ins.",
						PlatformLogger.INSTANCE);
			}
		}
		
		// ToolAgent settings
		SettingsList toolAgentsSettings = jobSettings.addListAttribute(VerificationJobOptions.TOOL_AGENTS, this.getToolAgents());
		for (Settings item : toolAgentsSettings.elements()) {
			if (item instanceof SettingsElement) {
				logIfValueNotSet((SettingsElement)item,
						"One of the verification tool agents not set its ID. This may make the retrieved settings unsuitable for repeating the measurements. This problem can only be addressed by the developer of the selected tool agent plug-ins.",
						PlatformLogger.INSTANCE);
			}
		}

		Preconditions.checkNotNull(ret, "ret");
		return ret;
	}

	private static void logIfValueNotSet(SettingsElement element, String message, IPlcverifLogger log) {
		if (element.value() == null) {
			log.logWarning(message);
		}
	}

	@Override
	public VerificationResult execute(ICancelingProgressReporter progressReporter) {
		// Create output directory if does not exist
		if (!getOutputDirectory().toFile().exists()) {
			try {
				Files.createDirectories(getOutputDirectory());
			} catch (Exception e) {
				throw new PlcverifPlatformException(String.format("Unable to create the output directory '%s'. %s",
						getOutputDirectory(), e.getMessage()), e);
			}
		}

		// 1. Create verification result
		// -----------------------------
		VerificationResult result = new VerificationResult();

		try {
			result.setSettings(this.retrieveSettings());
			result.setSpecificSettings(this.getSettings());
			result.setJobMetadata(getMetadata());
			result.setOutputDirectory(getOutputDirectory());
			result.setProgramDirectory(getProgramDirectory());
			result.setProgressReporter(progressReporter); // to report about
															// current stages

			// 2. Parse code
			// -------------
			IParserLazyResult parserResult = stepParseCode(result);
			// does not throw any exception
			throwInterruptedExceptionIfCancelled(progressReporter);

			// 3. Generate CFA
			// ---------------
			CfaNetworkDeclaration cfa = stepGenerateCfa(result, parserResult);
			// This throws CodeParsingException if there is a problem
			throwInterruptedExceptionIfCancelled(progressReporter);

			if (settings.isStrict()) {
				CfaValidation.validate(cfa);
			}

			// 4. Create verification problem
			// -----------------------------
			VerificationProblem problem = new VerificationProblem();
			problem.setModel(cfa);
			result.setVerifProblem(problem);
			throwInterruptedExceptionIfCancelled(progressReporter);

			// 5. Embed requirement
			// --------------------
			result.switchToStage("Requirement representation");
			this.getRequirement().fillVerificationProblem(problem, result.getParserResult(), result,
					getBackend().getPreferredRequirementFormat());
			serializeExpressionIfDiagOutputEnabled("req-cfd.txt", result, problem.getRequirement());
			
			if (settings.isStrict()) {
				CfaValidation.validate(problem.getModel());
			}

			// 6. Decide transformation goal
			// -----------------------------
			CfaTransformationGoal goal = getBackend().getPreferredModelFormat();
			boolean inline = getModelTasks().isForceInline() || getBackend().isInliningRequired();

			result.setTransformationGoal(goal);
			result.setInlined(inline);
			result.setCfaDeclaration((CfaNetworkDeclaration) problem.getModel());

			if (!getBackend().areCfaAnnotationsRequired()) {
				CfaUtils.removeAllNonlocalAnnotations(problem.getModel());
			}

			// Serialize the CFD (for diagnostics)
			serializeCfaIfDiagOutputEnabled("verif.cfd-original", problem, result);
			throwInterruptedExceptionIfCancelled(progressReporter);
			
			// 7. Apply reductions (CFD)
			// -------------------------
			if (this.getModelTasks().getReductions().isEmpty() || getBackend().areCfaAnnotationsRequired()) {
				result.currentStage().logInfo("No CFD reductions have been performed: reductions are disabled.");
			} else {
				result.switchToStage("Reductions (CFD)", JobStageTags.REDUCTIONS);
				doReductionLoop(problem, result, progressReporter);
				
				// Serialize the CFD (for diagnostics)
				serializeCfaIfDiagOutputEnabled("verif.cfd-reduced", problem, result);
	
				if (settings.isStrict()) {
					CfaValidation.validate(problem.getModel());
				}
			}

			// 8. Transform and prepare model
			// ------------------------------
			result.switchToStage("Model preparation");
			switch (goal) {
			case DECLARATION:
				prepareModel(problem, inline, result);
				break;
			case INSTANCE_ARRAYS:
				instantiateModel(problem, inline, result);
				break;
			case INSTANCE_ENUMERATED:
				instantiateAndEnumerateModel(problem, inline, result);
				break;
			default:
				break;
			}

			if (settings.isStrict()) {
				CfaValidation.validate(problem.getModel());
			}

			if (problem.getModel() instanceof CfaNetworkInstance) {
				// Serialize the CFI (for diagnostics)
				serializeCfaIfDiagOutputEnabled("verif.cfi-original", problem, result);
			}

			// 9. Apply reductions again
			// -------------------------
			if (goal != CfaTransformationGoal.DECLARATION) {
				if (this.getModelTasks().getReductions().isEmpty() || getBackend().areCfaAnnotationsRequired()) {
					result.currentStage().logInfo("No CFI reductions have been performed: reductions are disabled.");
				} else {
					result.switchToStage("Reductions (CFI)", JobStageTags.REDUCTIONS);	
					if (settings.isDiagnosticOutputEnabled()) {
						result.currentStage().logInfo("PSS before CFI reductions: %s.", new CfaInstanceStatistics((CfaNetworkInstance) problem.getModel()).potentialStateSpaceDouble());
					}
					
					doReductionLoop(problem, result, progressReporter);
					
					if (settings.isDiagnosticOutputEnabled()) {
						result.currentStage().logInfo("PSS after CFI reductions: %s.", new CfaInstanceStatistics((CfaNetworkInstance) problem.getModel()).potentialStateSpaceDouble());
					}
				}
			}

			// Validate even in case of strict settings before verification
			CfaValidation.validate(problem.getModel());

			// Serialize the verification model (for diagnostics)
			serializeCfaIfDiagOutputEnabled("verif.cfa-final", problem, result);

			// 10. Execute verification backend (and parse result)
			// ---------------------------------------------------
			stepExecuteBackend(result, progressReporter);

			// 11. (Try to) diagnose the result using the requirement
			// ------------------------------------------------------
			result.switchToStage("Result diagnosis");
			this.getRequirement().diagnoseResult(problem, result);
		} catch (CfaGenerationException | PlcverifPlatformException ex) {
			handleException(result, ex);
			// We handle the exception here (with logging), then we try to
			// generate report albeit the error.
		} catch (InterruptedException e) {
			// User requested a cancellation of the job, nothing to do.
			// Let's continue with the reporting.
		}

		if (result.hasError() && result.getResult() == ResultEnum.Unknown) {
			result.setResult(ResultEnum.Error);
		}

		result.switchToStage("Reporting");
		// Diagnostics: check non-consumed settings elements
		try {
			Settings rootSettingsNode = SettingsUtils.getRootSettingsNode(settings.getRepresentedSettingsElement());
			Optional<Settings> reporterSettingsNode = rootSettingsNode.toSingle()
					.getAttribute(VerificationJobOptions.REPORTERS);

			for (SettingsElement nonconsumed : SettingsUtils.nonConsumedSettingsElements(rootSettingsNode)) {
				// It is too early to check the consumption of the reporter
				// nodes. However, we'd like to include the rest in the reports.
				if (!reporterSettingsNode.isPresent()
						|| !SettingsUtils.isDescendantOrSelf(nonconsumed, reporterSettingsNode.get())) {
					result.currentStage().logWarning(
							"The settings element '%s' (value: %s) has not been used. Maybe its name has a typo?",
							nonconsumed.fqn(), nonconsumed.valueWithoutConsuming());
				}
			}
		} catch (SettingsParserException ex) {
			// Best effort. This exception is highly unlikely.
			throw new PlcverifPlatformException(
					"Problem while checking non-consumed settings elements: " + ex.getMessage(), ex);
		}

		// 12. Report
		// ----------
		for (IVerificationReporter reporter : this.getReporters()) {
			reporter.generateReport(result);
		}
		result.currentStage().endStage();

		// Diagnostics: check non-consumed settings elements
		try {
			for (SettingsElement nonconsumed : SettingsUtils.nonConsumedSettingsElements(
					SettingsUtils.getRootSettingsNode(settings.getRepresentedSettingsElement()))) {
				PlatformLogger.logWarning(String.format(
						"The settings element '%s' (value: %s) has not been used. Maybe its name has a typo?",
						nonconsumed.fqn(), nonconsumed.valueWithoutConsuming()));
			}
		} catch (SettingsParserException ex) {
			// Best effort. This exception is highly unlikely.
			throw new PlcverifPlatformException(
					"Problem while checking non-consumed settings elements: " + ex.getMessage(), ex);
		}
		
		
		// 13. Misc. Tool agents
		result.switchToStage("Executing Agents");
		
		
		for (IToolAgent agent : this.getToolAgents()) {
			agent.execute(result);
		}

		return result;
	}

	private void doReductionLoop(VerificationProblem problem, VerificationResult result,
			ICancelingProgressReporter progressReporter) throws InterruptedException {
		int iteration = 0;
		boolean isModified = true;

		//if (getBackend().areCfaAnnotationsRequired()) {
		//	result.currentStage().logInfo(
		//		"Reductions were skipped as the backend requires the preservation of annotations");
		//	return;
		//}
		
		while (isModified && iteration < settings.getMaxReductionIterations()) {
			iteration++;
			isModified = false;

			for (IReduction reduction : this.getModelTasks().getReductions()) {
				Preconditions.checkNotNull(reduction, "Null element returned by getModelTasks().getReductions()");

				isModified = reduction.reduce(problem.getModel(), problem.getRequirement(), result, progressReporter)
						|| isModified;
				throwInterruptedExceptionIfCancelled(progressReporter);
			}
			
			if (this.getModelTasks().getReductions().size() < 2) {
				// There is no point in repeating the only reduction.
				return;
			}
		}

		if (isModified) {
			// If there was modification but the loop was left, the maximum
			// number of iterations has been reached.
			result.currentStage().logInfo(
					"The reductions were interrupted as the maximum number of iterations (%s) has been reached. This value can be modified with the '%s' setting.",
					settings.getMaxReductionIterations(), VerificationJobSettings.MAX_REDUCTION_ITERATIONS);
		}
	}

	private static void throwInterruptedExceptionIfCancelled(ICanceling reporter) throws InterruptedException {
		if (reporter.isCanceled()) {
			throw new InterruptedException();
		}
	}

	private IParserLazyResult stepParseCode(VerificationResult result) {
		result.switchToStage("Parsing source code");
		IParserLazyResult parserResult = parseCode(result);
		result.setParserResult(parserResult);
		if (getModelTasks().getParser() != null) {
			result.setInputFiles(new ArrayList<>(getModelTasks().getParser().getFiles().keySet()));
		}
		return parserResult;
	}

	protected IParserLazyResult parseCode(VerificationResult result) {
		return this.getModelTasks().getParser().parseCode(result);
	}

	private CfaNetworkDeclaration stepGenerateCfa(VerificationResult result, IParserLazyResult parserResult)
			throws CfaGenerationException {
		result.switchToStage("Control-flow declaration generation");
		return generateCfa(result, parserResult);
	}

	protected CfaNetworkDeclaration generateCfa(VerificationResult result, IParserLazyResult parserResult)
			throws CfaGenerationException {
		try {
			return parserResult.generateCfa(result);
		} catch (AstParsingException e) {
			throw new CfaGenerationException("Unable to generate the CFA due to errors in parsing the source file.", e);
		}
	}

	private void stepExecuteBackend(VerificationResult result, ICanceling canceler) {
		Preconditions.checkState(result.getVerifProblem().isPresent(),
				"Unknown verification problem at VerificationJob.");

		// Human-friendly name should be set by the plugin, this is just a
		// failback.
		result.setBackendName(backend.getClass().getSimpleName());

		executeBackend(result, canceler);
	}

	protected void executeBackend(VerificationResult result, ICanceling canceler) {
		Preconditions.checkState(result.getVerifProblem().isPresent());
		this.getBackend().execute(result.getVerifProblem().get(), result, canceler);
	}

	private static void handleException(VerificationResult result, Exception ex) {
		Preconditions.checkNotNull(result, "result");
		if (result.currentStage() != null) {
			result.currentStage().logError(ex.getMessage(), ex);
		} else {
			PlatformLogger.logError(ex.getMessage(), ex);
		}
	}

	private static void prepareModel(VerificationProblem problem, boolean inline, VerificationResult result) {
		// Inline if asked to
		if (inline) {
			throw new UnsupportedOperationException("Inlining for CFA declarations is not supported.");
		}

		// Nothing else to do for the moment.
	}

	private static void instantiateModel(VerificationProblem problem, boolean inline, VerificationResult result) {
		instantiateAndEnumerateModel(problem, inline, result, false);
	}

	private static void instantiateAndEnumerateModel(VerificationProblem problem, boolean inline, VerificationResult result) {
		instantiateAndEnumerateModel(problem, inline, result, true);
	}

	private static void instantiateAndEnumerateModel(VerificationProblem problem, boolean inline, VerificationResult result,
			boolean enumerateArrays) {
		// Instantiate CFA network
		CfaInstantiationTrace traceModel = CfaInstantiator
				.transformCfaNetwork((CfaNetworkDeclaration) problem.getModel(), enumerateArrays);
		Expression instanceRequirement = CfaInstantiator.transformExpression(traceModel, problem.getRequirement());

		problem.setModel(traceModel.getInstance());
		problem.setRequirement(instanceRequirement, problem.getRequirementRepresentation());
		problem.setEocLocation(
				traceModel.getInstanceOf(problem.getEocLocation(), traceModel.getInstance().getMainAutomaton()));
		problem.setBocLocation(
				traceModel.getInstanceOf(problem.getBocLocation(), traceModel.getInstance().getMainAutomaton()));

		result.setCfaInstance(traceModel.getInstance());
		result.setVariableTrace(traceModel.getVariableTrace());

		if (inline) {
			CallInliner.transform((CfaNetworkInstance) problem.getModel());
		}
	}

	private void serializeCfaIfDiagOutputEnabled(String fileId, VerificationProblem problem,
			VerificationResult result) {
		if (settings.isDiagnosticOutputEnabled()) {
			try {
				CfaToXmi.serializeToFile(problem.getModel(),
						getOutputDirectory().resolve(String.format("%s.%s", result.getJobMetadata().getId(), fileId)));

				Path outFile = getOutputDirectory()
						.resolve(String.format("%s.%s.dot", result.getJobMetadata().getId(), fileId));
				IoUtils.writeAllContent(outFile,
						CfaToDot.represent(problem.getModel()));
				result.getSavedGraphs().add(outFile);
				result.currentStage().logInfo("Diagnostic CFA visualization has been saved to '%s'.", outFile.toAbsolutePath().toString());
			} catch (IOException e) {
				throw new PlcverifPlatformException(
						"Problem occurred while generating the diagnostic CFA outputs: " + e.getMessage(), e);
			}
		}
	}
	
	private void serializeExpressionIfDiagOutputEnabled(String fileNameSuffix, VerificationResult result, Expression expr) {
		if (settings.isDiagnosticOutputEnabled()) {
			try {
				Path outFile = getOutputDirectory()
						.resolve(String.format("%s.%s", result.getJobMetadata().getId(), fileNameSuffix));
				CharSequence content = EmfModelPrinter.print(expr, CfaEObjectToText.INSTANCE);
				IoUtils.writeAllContent(outFile, content);
			} catch (IOException e) {
				throw new PlcverifPlatformException(
						"Problem occurred while generating the diagnostic outputs: " + e.getMessage(), e);
			}
		}
	}
			

	/**
	 * Returns the logger attached to the current stage, or if there is no stage
	 * open, then the platform logger.
	 * <p>
	 * Note: if the returned logger is the platform logger, its content will
	 * typically not be included in the reports.
	 * 
	 * @param result
	 *            The result object which may contain an active stage. May be
	 *            {@code null}, then the platform logger is returned.
	 * @return Most specific currently active logger.
	 */
	public static IPlcverifLogger getCurrentLog(JobResult result) {
		if (result != null && result.isStageOpen()) {
			return result.currentStage();
		} else {
			return PlatformLogger.INSTANCE;
		}
	}
}
