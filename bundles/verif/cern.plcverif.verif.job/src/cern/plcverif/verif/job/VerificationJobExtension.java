/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions, refactoring
 *****************************************************************************/
package cern.plcverif.verif.job;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import cern.plcverif.base.common.extension.ExtensionHelper;
import cern.plcverif.base.common.logging.PlatformLogger;
import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsSerializer;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.help.SettingsHelp;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory;
import cern.plcverif.base.interfaces.IJobExtension;
import cern.plcverif.base.interfaces.data.PlatformConfig;
import cern.plcverif.verif.interfaces.IBackend;
import cern.plcverif.verif.interfaces.IBackendExtension;
import cern.plcverif.verif.interfaces.IRequirement;
import cern.plcverif.verif.interfaces.IRequirementExtension;
import cern.plcverif.verif.interfaces.IToolAgent;
import cern.plcverif.verif.interfaces.IToolAgentExtension;
import cern.plcverif.verif.interfaces.IVerificationReporter;
import cern.plcverif.verif.interfaces.IVerificationReporterExtension;
import cern.plcverif.verif.interfaces.data.VerificationJobSettings;

public class VerificationJobExtension implements IJobExtension {
	public static final String CMD_ID = "verif";

	public Map<String, IRequirementExtension> getRequirements() {
		return ExtensionHelper.getExtensions(IRequirementExtension.class);
	}

	public Map<String, IBackendExtension> getBackends() {
		return ExtensionHelper.getExtensions(IBackendExtension.class);
	}

	public Map<String, IVerificationReporterExtension> getReporters() {
		return ExtensionHelper.getExtensions(IVerificationReporterExtension.class);
	}
	
	public Map<String, IToolAgentExtension> getToolAgents() {
		return ExtensionHelper.getExtensions(IToolAgentExtension.class);
	}

	public Set<String> listRequirements() {
		return getRequirements().keySet();
	}

	public IRequirementExtension getRequirementExtension(String requirementId) {
		IRequirementExtension ret = null;
		ret = getRequirements().getOrDefault(requirementId, null);
		return ret;
	}

	public IRequirement createRequirement(String requirementId) {
		IRequirement ret = null;
		IRequirementExtension requirement = getRequirements().getOrDefault(requirementId, null);
		if (requirement != null) {
			ret = requirement.createRequirement();
		}
		return ret;
	}

	public Set<String> listBackends() {
		return getBackends().keySet();
	}

	public IBackendExtension getBackendExtension(String backendId) {
		IBackendExtension ret = null;
		ret = getBackends().getOrDefault(backendId, null);
		return ret;
	}

	public IBackend createBackend(String backendId) {
		IBackend ret = null;
		IBackendExtension backend = getBackends().getOrDefault(backendId, null);
		if (backend != null) {
			ret = backend.createBackend();
		}
		return ret;
	}

	public Set<String> listReporters() {
		return getReporters().keySet();
	}

	public IVerificationReporterExtension getReporterExtension(String reporterId) {
		IVerificationReporterExtension ret = null;
		ret = getReporters().getOrDefault(reporterId, null);
		return ret;
	}

	public IVerificationReporter createReporter(String reporterId) {
		IVerificationReporter ret = null;
		IVerificationReporterExtension reporter = getReporters().getOrDefault(reporterId, null);
		if (reporter != null) {
			ret = reporter.createReporter();
		}
		return ret;
	}

	@Override
	public VerificationJob createJob() {
		return new VerificationJob();
	}

	@Override
	public VerificationJob createJob(SettingsElement settings, PlatformConfig platformConfig)
			throws SettingsParserException {
		VerificationJob ret = new VerificationJob();
		VerificationJobSettings verifJobSettings = SpecificSettingsSerializer.parse(settings,
				VerificationJobSettings.class);

		IRequirement requirement = parseRequirement(settings, platformConfig);
		IBackend backend = parseBackend(settings, platformConfig);
		List<IVerificationReporter> reporters = parseReporters(settings, platformConfig);
		List<IToolAgent> toolAgents = parseToolAgents(settings, platformConfig);

		ret.setRequirement(requirement);
		ret.setBackend(backend);
		ret.setReporters(reporters);
		ret.setToolAgents(toolAgents);
		ret.setSettings(verifJobSettings);
		return ret;
	}

	@Override
	public void fillSettingsHelp(SettingsHelp help) {
		help.addPluginDetails(CMD_ID, "Verification job", 
				"Provides the verification job.",
				VerificationJobExtension.class);
		help.addSettingItem(VerificationJobOptions.BACKEND, "Backend to be used.", String.class,
				PlcverifSettingsMandatory.MANDATORY, getBackends().keySet());
		help.addSettingItem(VerificationJobOptions.REQUIREMENT, "Requirement type to be used.", String.class,
				PlcverifSettingsMandatory.MANDATORY, getRequirements().keySet());
		help.addSettingItem(VerificationJobOptions.REPORTERS, "List of reporters to be used.", List.class,
				PlcverifSettingsMandatory.OPTIONAL, getReporters().keySet());
		help.addSettingItem(VerificationJobOptions.TOOL_AGENTS, "List of tool agents to be used.", List.class,
				PlcverifSettingsMandatory.OPTIONAL, getToolAgents().keySet());
		SpecificSettingsSerializer.fillSettingsHelp(help, VerificationJobSettings.class);

		help.startPrefix(VerificationJobOptions.BACKEND);
		for (IBackendExtension backend : getBackends().values()) {
			backend.fillSettingsHelp(help);
		}
		help.endPrefix();

		help.startPrefix(VerificationJobOptions.REQUIREMENT);
		for (IRequirementExtension req : getRequirements().values()) {
			req.fillSettingsHelp(help);
		}
		help.endPrefix();

		help.startPrefixList(VerificationJobOptions.REPORTERS);
		for (IVerificationReporterExtension reporter : getReporters().values()) {
			reporter.fillSettingsHelp(help);
		}
		help.endPrefix();
		
		help.startPrefixList(VerificationJobOptions.TOOL_AGENTS);
		for (IToolAgentExtension toolAgent : getToolAgents().values()) {
			toolAgent.fillSettingsHelp(help);
		}
		help.endPrefix();
	}

	private IRequirement parseRequirement(SettingsElement settings, PlatformConfig platformConfig)
			throws SettingsParserException {
		Optional<Settings> reqSettings = settings.getAttribute(VerificationJobOptions.REQUIREMENT);
		if (!reqSettings.isPresent() || reqSettings.get().toSingle().value() == null) {
			throw new SettingsParserException("Requirement not specified - nothing to check.");
		}

		String reqId = reqSettings.get().toSingle().value();
		IRequirementExtension reqManager = getRequirements().getOrDefault(reqId, null);
		if (reqManager == null) {
			throw new SettingsParserException("Unknown requirement class: %s. Available requirement classes: %s.",
					reqSettings.get().toSingle().value(), getRequirements().keySet().toString());
		}

		SettingsElement resultantSettings = SettingsSerializer.extendWithInstallationSettings(
				reqSettings.get().toSingle(), reqId, platformConfig.getProgramSettingsDirectory());

		return reqManager.createRequirement(resultantSettings);
	}

	private IBackend parseBackend(SettingsElement settings, PlatformConfig platformConfig)
			throws SettingsParserException {
		Optional<Settings> backendSettings = settings.getAttribute(VerificationJobOptions.BACKEND);
		if (!backendSettings.isPresent() || backendSettings.get().toSingle().value() == null) {
			throw new SettingsParserException(
					"Verification backend not specified - nothing to check with. "
							+ "Specify a verification backend using the `-%s` argument. "
							+ "Currently available verification backends: %s.",
					VerificationJobOptions.BACKEND, String.join(", ", listBackends()));
		}

		String backendId = backendSettings.get().toSingle().value();
		IBackendExtension backendManager = getBackends().getOrDefault(backendId, null);
		if (backendManager == null) {
			throw new SettingsParserException("Unknown verification backend: %s. Available verification backends: %s.",
					backendSettings.get().toSingle().value(), getBackends().keySet().toString());
		}

		SettingsElement resultantSettings = SettingsSerializer.extendWithInstallationSettings(
				backendSettings.get().toSingle(), backendId, platformConfig.getProgramSettingsDirectory());

		return backendManager.createBackend(resultantSettings);
	}

	private List<IVerificationReporter> parseReporters(SettingsElement settings, PlatformConfig platformConfig)
			throws SettingsParserException {
		List<IVerificationReporter> ret = new ArrayList<>();
		Optional<Settings> repSettings = settings.getAttribute(VerificationJobOptions.REPORTERS);
		if (!repSettings.isPresent()) {
			PlatformLogger.logWarning("No reporters specified. No output artifacts will be generated.");
			return ret;
		}

		for (Settings elem : repSettings.get().toList().elements()) {
			if (elem == null) {
				continue;
			}
			String reporterId = elem.toSingle().value();
			IVerificationReporterExtension repdef = getReporters().getOrDefault(reporterId, null);
			if (repdef == null) {
				PlatformLogger.logWarning("Skipped undefined reporter: " + elem.toSingle().value());
				continue;
			}

			SettingsElement resultantSettings = SettingsSerializer.extendWithInstallationSettings(elem.toSingle(),
					reporterId, platformConfig.getProgramSettingsDirectory());

			ret.add(repdef.createReporter(resultantSettings));
		}

		return ret;
	}
	
	private List<IToolAgent> parseToolAgents(SettingsElement settings, PlatformConfig platformConfig)
			throws SettingsParserException {
		List<IToolAgent> ret = new ArrayList<>();
		Optional<Settings> toolAgentSettings = settings.getAttribute(VerificationJobOptions.TOOL_AGENTS);
		if (!toolAgentSettings.isPresent()) {
			PlatformLogger.logWarning("No tool agents specified. No additional tools will be executed.");
			return ret;
		}
		
		for (Settings elem : toolAgentSettings.get().toList().elements()) {
			if (elem == null) {
				continue;
			}
			String toolAgentId = elem.toSingle().value();
			IToolAgentExtension toolAgentdef = getToolAgents().getOrDefault(toolAgentId, null);
			if (toolAgentdef == null) {
				PlatformLogger.logWarning("Skipped undefined tool agent: " + elem.toSingle().value());
				continue;
			}
			
			Path testPath = platformConfig.getProgramSettingsDirectory();

			SettingsElement resultantSettings = SettingsSerializer.extendWithInstallationSettings(elem.toSingle(),
					toolAgentId, platformConfig.getProgramSettingsDirectory());

			ret.add(toolAgentdef.createAgent(resultantSettings));
		}

		return ret;
	}
}
