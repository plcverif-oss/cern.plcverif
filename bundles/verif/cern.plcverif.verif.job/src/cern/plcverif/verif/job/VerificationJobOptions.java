/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions, refactoring
 *****************************************************************************/
package cern.plcverif.verif.job;

public final class VerificationJobOptions {
	public static final String BACKEND = "backend";
	public static final String REQUIREMENT = "req";
	public static final String REPORTERS = "reporters";
	public static final String TOOL_AGENTS = "toolAgents";
	
	private VerificationJobOptions() {
		// Utility class.
	}
}
