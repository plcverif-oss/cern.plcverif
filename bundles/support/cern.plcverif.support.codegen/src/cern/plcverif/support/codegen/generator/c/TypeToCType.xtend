/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.support.codegen.generator.c

import cern.plcverif.base.models.cfa.cfabase.ArrayType
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructureRef
import cern.plcverif.base.models.expr.BoolType
import cern.plcverif.base.models.expr.FloatType
import cern.plcverif.base.models.expr.IntType
import cern.plcverif.base.models.expr.StringType
import cern.plcverif.base.models.expr.Type
import java.util.Arrays

class TypeToCType {
	StructNames structNames;
	
	new() {
		this.structNames = null;
	}
	
	new(StructNames structNames) {
		this.structNames = structNames;
	}
	
	def dispatch String declare(String name, Type type) {
		return '''« typeName(type) » « name »'''; 
	}
	
	def dispatch String declare(String name, ArrayType type) {		
		if (!type.dimension.defined) {
			throw new UnsupportedOperationException("Arrays with undefined dimensions are not supported.");			
		}
		
		return '''«getCTypeName(type.elementType)» « name »[«type.dimension.upperIndex + 1»]'''; 
	}

	def dispatch String typeName(Type type) {
		return getCTypeName(type);
	}
	
	def dispatch String typeName(DataStructureRef type) {
		if (structNames === null) {
			throw new UnsupportedOperationException("This instance of TypeToCType does not have the structNames defined, thus cannot be used for CFDs.");
		}
		return structNames.toName(type.definition);
	}
		
	def String variableDeclaration(Type type, String varName, String initValue) {
		if (type instanceof ArrayType) {
			// Handling for multi-dimensional arrays
			val parentArrayTypes = newArrayList();
			var Type currentElementType = type;
			while (currentElementType instanceof ArrayType) {
				parentArrayTypes.add(currentElementType);
				currentElementType = currentElementType.elementType;
			}
			
			return '''«getCTypeName(currentElementType)» «varName»«FOR t : parentArrayTypes»[«t.dimension.upperIndex + 1»]«ENDFOR»;'''
		} else {
			return '''«getCTypeName(type)» «varName»«IF initValue !== null» = «initValue»«ENDIF»;''';
		}
	}
	
	def dispatch getCTypeName(IntType type) {
		var s = "";
		
		if (Arrays.asList(8, 16, 32, 64).contains(type.bits)) {
			if (!type.signed) {
				s += 'u'
			}
			
			s += "int"
			
			s += type.bits
			s += "_t";
		} else {
			if (!type.signed) {
				s += "unsigned "
			}
	
			s += "int"			
		}
		
		return s;
	}
	
	def dispatch String getCTypeName(Type type) {
		return "***" + type;
	}
	
	def dispatch String getCTypeName(DataStructureRef type) {
		return typeName(type);
	}
	
	def dispatch String getCTypeName(BoolType type) {
		return "bool";
	}
	
	def dispatch String getCTypeName(FloatType type) {
		return if (type.bits.value <= 32) "float" else "double" ;
	}
	
	def dispatch String getCTypeName(StringType type) {
		throw new UnsupportedOperationException("String types are not supported yet");
	}
	
	def dispatch String getCTypeName(ArrayType type) {
		return '''«getCTypeName(type.elementType)»[«type.dimension.upperIndex + 1»]'''; 
	}
}
