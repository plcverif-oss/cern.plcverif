/*******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.support.codegen.generator

import cern.plcverif.base.models.cfa.cfadeclaration.VariableAssignment
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.support.codegen.program.Module
import cern.plcverif.support.codegen.program.statement.AssignStatement
import cern.plcverif.support.codegen.program.statement.BlockStatement
import cern.plcverif.support.codegen.program.statement.BreakStatement
import cern.plcverif.support.codegen.program.statement.ContinueStatement
import cern.plcverif.support.codegen.program.statement.DoWhileStatement
import cern.plcverif.support.codegen.program.statement.ForStatement
import cern.plcverif.support.codegen.program.statement.IfStatement
import cern.plcverif.support.codegen.program.statement.StatementVisitor
import cern.plcverif.support.codegen.program.statement.WhileStatement
import cern.plcverif.support.codegen.program.statement.InlineStatement
import cern.plcverif.support.codegen.program.statement.AssumeBoundStatement
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef
import cern.plcverif.base.models.cfa.cfabase.ArrayType

abstract class CFamilyCodeGenerator extends Generator implements StatementVisitor<String> {
	
	new(Module module) {
		super(module)
	}

	override visit(BlockStatement blockStatement) {
		return '''{
	«FOR stmt : blockStatement.statements»
		«stmt.accept(this)»
	«ENDFOR»
}
	'''
	}
	
	override visit(AssignStatement assignStatement) {
		return this.generateAssignment(assignStatement.assignment)
	}
	
	override visit(AssumeBoundStatement assumeBoundStatement) {
		return '''verifier_assume(«this.transformExpression(assumeBoundStatement.bound)»);'''
	}
	
	override visit(BreakStatement breakStatement) {
		return "break;"
	}
	
	override visit(ContinueStatement continueStatement) {
		return "continue;" 
	}
	
	override visit(InlineStatement inlineStatement) {
		return inlineStatement.content;
	}
		
	override visit(DoWhileStatement doWhileStatement) {
		return '''do «doWhileStatement.loop.accept(this)»while («this.transformExpression(doWhileStatement.condition)»);'''
	}
	
	override visit(IfStatement ifStatement) {
		val ifPart = '''if («this.transformExpression(ifStatement.condition)») «ifStatement.then.accept(this)»'''
		var elzePart = ""
		
		if (ifStatement.elze.present) {
			elzePart = '''else «ifStatement.elze.get.accept(this)»'''
		}
		
		return '''«ifPart»«elzePart»'''
	}
	
	override visit(WhileStatement whileStatement) {
		return '''while («this.transformExpression(whileStatement.condition)») «whileStatement.loop.accept(this)»'''
	}
	
	override visit(ForStatement forStatement) {
		return '''for («this.generateAssignmentWithoutSemicolon(forStatement.init)»; «this.transformExpression(forStatement.condition)»; «this.generateAssignmentWithoutSemicolon(forStatement.update)») «forStatement.loop.accept(this)»''';
	}
	
	private def generateAssignmentWithoutSemicolon(VariableAssignment assign) {
		// Handle array case
		if (assign.leftValue instanceof FieldRef && (assign.leftValue as FieldRef).field.type instanceof ArrayType){
			return '''memmove(&(«this.transformExpression(assign.leftValue)»), &(«this.transformExpression(assign.rightValue)»), sizeof «this.transformExpression(assign.leftValue)»)'''
		}
		return '''«this.transformExpression(assign.leftValue)» = «this.transformExpression(assign.rightValue)»'''
	}
	
	protected def generateAssignment(VariableAssignment assign) {
		return this.generateAssignmentWithoutSemicolon(assign) + ";"
	}
	
	abstract protected def String transformExpression(Expression expr);
	
}