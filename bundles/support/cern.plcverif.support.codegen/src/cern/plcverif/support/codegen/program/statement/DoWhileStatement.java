/*******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.support.codegen.program.statement;

import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.string.ExprToString;

public final class DoWhileStatement extends LoopStatement {

	public DoWhileStatement(Expression condition) {
		super(condition);
	}

	@Override
	public <T> T accept(StatementVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public String toString() {
		return String.format("Do(){%s}While(%s)EndDo()", this.loop.toString(), ExprToString.INSTANCE.toString(this.condition));
	}


}
