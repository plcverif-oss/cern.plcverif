/*******************************************************************************
 * (C) Copyright CERN 2023-2025. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Xaver Fink - initial API and implementation
 *******************************************************************************/
package cern.plcverif.support.codegen.program.statement;

import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.string.ExprToString;

public final class AssumeBoundStatement implements Statement {

	private Expression bound;

	public AssumeBoundStatement(Expression bound) {
		super();
		this.bound = bound;
	}

	@Override
	public <T> T accept(StatementVisitor<T> visitor) {
		return visitor.visit(this);
	}

	public Expression getBound() {
		return this.bound;
	}
	
	public void setAssignment(Expression bound) {
		this.bound = bound;
	}

	@Override
	public String toString() {
		return String.format("verifier_assume(%s)",
			ExprToString.INSTANCE.toString(this.bound));
	}

}
