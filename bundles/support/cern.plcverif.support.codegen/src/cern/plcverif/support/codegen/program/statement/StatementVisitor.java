/*******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.support.codegen.program.statement;

public interface StatementVisitor<T> {

	T visit(AssignStatement assignStatement);
	
	T visit(AssumeBoundStatement assumeBoundStatement);

	T visit(BlockStatement blockStatement);

	T visit(BreakStatement breakStatement);

	T visit(CallStatement callStatement);

	T visit(ContinueStatement continueStatement);

	T visit(DoWhileStatement doWhileStatement);

	T visit(IfStatement ifStatement);

	T visit(WhileStatement whileStatement);

	T visit(ForStatement forStatement);

	T visit(GotoStatement gotoStatement);

	T visit(LabeledStatement labeledStatement);

	T visit(AssertStatement assertStatement);

	T visit(ReturnStatement returnStatement);
	
	T visit(InlineStatement inlineStatement);
}
