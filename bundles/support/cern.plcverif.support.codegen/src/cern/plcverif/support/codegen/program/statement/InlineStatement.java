/*******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.support.codegen.program.statement;

/**
 * Represents a statement which should be generated "as is" by the code generator.
 * Use it mainly for backend-specific transformation passes.
 */
public class InlineStatement implements Statement {

	private String content;	
	
	public InlineStatement(String content) {
		this.content = content;
	}

	public String getContent() {
		return content;
	}
	
	@Override
	public <T> T accept(StatementVisitor<T> visitor) {
		return visitor.visit(this);
	}
	
	@Override
	public String toString() {
		return String.format("Inline(%s)", this.content);
	}

}
