/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.support.codegen.exception;

public class CodeGeneratorException extends RuntimeException {
	private static final long serialVersionUID = 5286310302261787895L;

	public CodeGeneratorException(String message) {
		super(message);
	}

	public CodeGeneratorException(String message, Throwable cause) {
		super(message, cause);
	}

}
