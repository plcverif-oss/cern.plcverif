/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.support.codegen.generator.c;

import cern.plcverif.base.models.cfa.cfadeclaration.DataStructure;
import cern.plcverif.verif.utils.backend.BackendNameTrace;

/**
 * Mapping to assign unique names to the structs representing CFA declaration {@link DataStructure}s. 
 */
public final class StructNames extends BackendNameTrace<DataStructure>{
	public StructNames() {
		super((String name) -> (name.startsWith("__") ? name : "__" + name).replaceAll("[^a-zA-Z0-9_]", "_"));
	}
}