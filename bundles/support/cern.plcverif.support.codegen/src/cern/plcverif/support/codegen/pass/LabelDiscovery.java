/*******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.support.codegen.pass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.google.common.base.Preconditions;

import cern.plcverif.support.codegen.program.Procedure;
import cern.plcverif.support.codegen.program.statement.AssertStatement;
import cern.plcverif.support.codegen.program.statement.AssignStatement;
import cern.plcverif.support.codegen.program.statement.AssumeBoundStatement;
import cern.plcverif.support.codegen.program.statement.BlockStatement;
import cern.plcverif.support.codegen.program.statement.BreakStatement;
import cern.plcverif.support.codegen.program.statement.CallStatement;
import cern.plcverif.support.codegen.program.statement.ContinueStatement;
import cern.plcverif.support.codegen.program.statement.DoWhileStatement;
import cern.plcverif.support.codegen.program.statement.ForStatement;
import cern.plcverif.support.codegen.program.statement.GotoStatement;
import cern.plcverif.support.codegen.program.statement.IfStatement;
import cern.plcverif.support.codegen.program.statement.InlineStatement;
import cern.plcverif.support.codegen.program.statement.LabeledStatement;
import cern.plcverif.support.codegen.program.statement.ReturnStatement;
import cern.plcverif.support.codegen.program.statement.StatementVisitor;
import cern.plcverif.support.codegen.program.statement.WhileStatement;

/**
 * Utility class for discovering all labels used in a given procedure
 */
public final class LabelDiscovery {

	private LabelDiscovery() {
	}

	private static class VisitedLabel {
		private LabeledStatement statement = null;
		private List<GotoStatement> users = new ArrayList<>();

		private VisitedLabel() {
		}
	}

	/**
	 * Returns a list of all labels which are referenced within the procedure
	 */
	public static List<String> getVisitedLabels(Procedure function) {
		LabelDiscoveryVisitor visitor = new LabelDiscoveryVisitor();
		function.getRootStatement().accept(visitor);

		return visitor.visited.entrySet().stream().filter(e -> !e.getValue().users.isEmpty()).map(e -> e.getKey())
				.collect(Collectors.toList());
	}

	public static Optional<LabeledStatement> getLabelByName(String name, Procedure function) {
		LabelDiscoveryVisitor visitor = new LabelDiscoveryVisitor();
		function.getRootStatement().accept(visitor);

		VisitedLabel label = visitor.visited.get(name);
		if (label == null) {
			return Optional.empty();
		}

		return Optional.ofNullable(label.statement);
	}

	private static final class LabelDiscoveryVisitor implements StatementVisitor<Void> {
		private Map<String, VisitedLabel> visited = new HashMap<>();

		@Override
		public Void visit(GotoStatement gotoStatement) {
			String label = gotoStatement.getLabel();

			VisitedLabel entry = visited.computeIfAbsent(label, s -> new VisitedLabel());
			entry.users.add(gotoStatement);

			return null;
		}

		@Override
		public Void visit(AssignStatement assignStatement) {
			return null;
		}
		
		@Override
		public Void visit(AssumeBoundStatement assumeBoundStatement) {
			return null;
		}

		@Override
		public Void visit(BreakStatement breakStatement) {
			return null;
		}

		@Override
		public Void visit(ContinueStatement gotoStatement) {
			return null;
		}

		@Override
		public Void visit(CallStatement callStatement) {
			return null;
		}

		@Override
		public Void visit(InlineStatement inlineStatement) {
			return null;
		}

		@Override
		public Void visit(LabeledStatement labeledStatement) {
			String label = labeledStatement.getLabel();
			VisitedLabel entry = visited.computeIfAbsent(label, s -> new VisitedLabel());

			Preconditions.checkState(entry.statement == null, "A labelled statement should only be visited once!");
			entry.statement = labeledStatement;

			return null;
		}

		@Override
		public Void visit(AssertStatement assertStatement) {
			return null;
		}

		@Override
		public Void visit(ReturnStatement returnStatement) {
			return null;
		}

		@Override
		public Void visit(BlockStatement blockStatement) {
			blockStatement.getStatements().forEach(s -> s.accept(this));

			return null;
		}

		@Override
		public Void visit(DoWhileStatement doWhileStatement) {
			doWhileStatement.getLoop().accept(this);

			return null;
		}

		@Override
		public Void visit(IfStatement ifStatement) {
			ifStatement.getThen().accept(this);
			if (ifStatement.getElze().isPresent()) {
				ifStatement.getElze().get().accept(this);
			}

			return null;
		}

		@Override
		public Void visit(WhileStatement whileStatement) {
			whileStatement.getLoop().accept(this);

			return null;
		}

		@Override
		public Void visit(ForStatement forStatement) {
			forStatement.getLoop().accept(this);

			return null;
		}
	}

}
