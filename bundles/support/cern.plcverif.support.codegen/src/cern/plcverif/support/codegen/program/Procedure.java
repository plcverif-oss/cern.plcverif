/*******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.support.codegen.program;

import com.google.common.base.Preconditions;

import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration;
import cern.plcverif.support.codegen.program.statement.Statement;

/**
 * A class which represents an executable procedure.
 */
public class Procedure {

	protected final String name;

	protected Statement rootStatement;
	
	protected AutomatonDeclaration correspondingAutomaton;

	public Procedure(String name, Statement rootStatement, AutomatonDeclaration automaton) {
		Preconditions.checkNotNull(name);
		Preconditions.checkNotNull(rootStatement);
		Preconditions.checkNotNull(automaton);

		this.name = name;
		this.rootStatement = rootStatement;
		this.correspondingAutomaton = automaton;
	}

	public final String getName() {
		return this.name;
	}

	public final void setRootStatement(Statement rootStatement) {
		this.rootStatement = rootStatement;
	}

	public final Statement getRootStatement() {
		return this.rootStatement;
	}
	
	public AutomatonDeclaration getCorrespondingAutomaton() {
		return correspondingAutomaton;
	}
	
	@Override
	public String toString() {
		return String.format("Procedure('%s', INPUTS={}, OUTPUTS={}, INOUTS={}) %s", this.name, this.rootStatement.toString());
	}
}
