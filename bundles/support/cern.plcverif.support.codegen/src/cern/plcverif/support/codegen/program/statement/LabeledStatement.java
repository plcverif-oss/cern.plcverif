/*******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.support.codegen.program.statement;

import java.util.Optional;

public final class LabeledStatement implements Statement {

	private final String label;
	private Optional<Statement> innerStatement;

	public LabeledStatement(String label) {
		super();
		this.label = label;
		this.innerStatement = Optional.empty();
	}

	public LabeledStatement(String label, Statement innerStatement) {
		super();
		this.label = label;
		this.innerStatement = Optional.of(innerStatement);
	}

	@Override
	public <T> T accept(StatementVisitor<T> visitor) {
		return visitor.visit(this);
	}

	public Optional<Statement> getInnerStatement() {
		return this.innerStatement;
	}

	public void setInnerStatement(Statement stmt) {
		this.innerStatement = Optional.of(stmt);
	}

	public String getLabel() {
		return this.label;
	}

	@Override
	public String toString() {
		return String.format("Label('%s')", this.label);
	}

}
