/*******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.support.codegen.program.statement;

import cern.plcverif.base.models.cfa.cfadeclaration.VariableAssignment;
import cern.plcverif.base.models.expr.Expression;

public class ForStatement extends LoopStatement {

	private VariableAssignment init;
	private VariableAssignment update;

	public ForStatement(Expression condition) {
		super(condition);
	}

	public VariableAssignment getInit() {
		return this.init;
	}

	public void setInit(VariableAssignment init) {
		this.init = init;
	}

	public VariableAssignment getUpdate() {
		return this.update;
	}

	public void setUpdate(VariableAssignment update) {
		this.update = update;
	}

	@Override
	public <T> T accept(StatementVisitor<T> visitor) {
		return visitor.visit(this);
	}

}
