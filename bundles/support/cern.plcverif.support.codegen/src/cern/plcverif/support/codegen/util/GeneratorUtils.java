/*******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.support.codegen.util;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Queue;

import cern.plcverif.base.models.cfa.cfabase.DataDirection;
import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef;
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructure;
import cern.plcverif.base.models.cfa.cfadeclaration.DirectionFieldAnnotation;
import cern.plcverif.base.models.cfa.cfadeclaration.Field;
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef;
import cern.plcverif.base.models.cfa.utils.CfaUtils;

/**
 * Helper class for various code generation tasks
 */
public final class GeneratorUtils {

	private GeneratorUtils() {
	}

	public static String getLocationLabelName(Location loc) {
		return loc.getParentAutomaton().getName() + "_" + loc.getName();
	}
	
	public static String canonize(String name) {
		return name.replaceAll("[^A-Za-z0-9_]", "_");
	}

	public static boolean isRefToTemp(DataRef ref) {
		// Extract field
		DataRef current = ref;
		while (current != null) {
			if (current instanceof FieldRef) {
				FieldRef r = (FieldRef) current;
				if (isTempField(r.getField())) {
					return true;
				}
			}
			current = current.getPrefix();
		}

		return false;
	}

	public static boolean isTempField(Field f) {
		DirectionFieldAnnotation annotation = CfaUtils.getFirstAnnotationOfType(f, DirectionFieldAnnotation.class);
		if (annotation != null && annotation.getDirection() == DataDirection.TEMP) {
			return true;
		}

		return false;
	}

	public static List<DataStructure> orderStructs(Collection<DataStructure> structs) {
		List<DataStructure> ordered = new ArrayList<>();
		Queue<DataStructure> queue = new ArrayDeque<>(structs);

		while (!queue.isEmpty()) {
			DataStructure current = queue.poll();
			if (ordered.contains(current)) {
				continue;
			}

			boolean root = structs.stream().noneMatch(s -> s.getComplexTypes().contains(current));
			if (root) {
				// Start a DFS from this node
				visitNestedStructs(current, ordered);
			}
		}

		return ordered;
	}

	private static List<DataStructure> visitNestedStructs(DataStructure root, List<DataStructure> visited) {
		for (DataStructure struct : root.getComplexTypes()) {
			if (!visited.contains(struct)) {
				visitNestedStructs(struct, visited);
			}
		}

		visited.add(root);

		return visited;
	}
}
