/*******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.support.codegen.pass;

import cern.plcverif.support.codegen.program.Module;
import cern.plcverif.support.codegen.program.Procedure;

public interface ModulePass {

	public default void run(Module module)
	{
		for (Procedure function : module.getProcedures()) {
			this.run(function);
		}
	}

	public void run(Procedure function);

}
