/*******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.support.codegen.pass;

import cern.plcverif.support.codegen.program.statement.AssertStatement;
import cern.plcverif.support.codegen.program.statement.AssignStatement;
import cern.plcverif.support.codegen.program.statement.AssumeBoundStatement;
import cern.plcverif.support.codegen.program.statement.BlockStatement;
import cern.plcverif.support.codegen.program.statement.BreakStatement;
import cern.plcverif.support.codegen.program.statement.CallStatement;
import cern.plcverif.support.codegen.program.statement.ContinueStatement;
import cern.plcverif.support.codegen.program.statement.DoWhileStatement;
import cern.plcverif.support.codegen.program.statement.ForStatement;
import cern.plcverif.support.codegen.program.statement.GotoStatement;
import cern.plcverif.support.codegen.program.statement.IfStatement;
import cern.plcverif.support.codegen.program.statement.InlineStatement;
import cern.plcverif.support.codegen.program.statement.LabeledStatement;
import cern.plcverif.support.codegen.program.statement.ReturnStatement;
import cern.plcverif.support.codegen.program.statement.Statement;
import cern.plcverif.support.codegen.program.statement.StatementVisitor;
import cern.plcverif.support.codegen.program.statement.WhileStatement;

/**
 * Base class for statement simplification transformations.
 *
 * Without redefining any methods, this pass returns an identical copy of each
 * statement.
 */
public abstract class StatementTransformVisitor implements StatementVisitor<Statement> {

	@Override
	public Statement visit(BlockStatement blockStatement) {
		BlockStatement block = new BlockStatement();
		for (Statement stmt : blockStatement.getStatements()) {
			block.getStatements().add(stmt.accept(this));
		}

		return block;
	}

	@Override
	public Statement visit(DoWhileStatement doWhileStatement) {
		DoWhileStatement newDo = new DoWhileStatement(doWhileStatement.getCondition());
		newDo.setLoop(doWhileStatement.getLoop().accept(this));

		return newDo;
	}

	@Override
	public Statement visit(IfStatement ifStatement) {
		IfStatement newIf = new IfStatement(ifStatement.getCondition());
		newIf.setThen(ifStatement.getThen().accept(this));
		if (ifStatement.getElze().isPresent()) {
			newIf.setElze(ifStatement.getElze().get().accept(this));
		}

		return newIf;
	}

	@Override
	public Statement visit(LabeledStatement labeledStatement) {
		if (labeledStatement.getInnerStatement().isPresent()) {
			return new LabeledStatement(labeledStatement.getLabel(),
					labeledStatement.getInnerStatement().get().accept(this));
		}

		return labeledStatement;
	}

	@Override
	public Statement visit(WhileStatement whileStatement) {
		WhileStatement newWhile = new WhileStatement(whileStatement.getCondition());
		newWhile.setLoop(whileStatement.getLoop().accept(this));

		return newWhile;
	}

	@Override
	public Statement visit(ForStatement forStatement) {
		ForStatement newFor = new ForStatement(forStatement.getCondition());
		Statement loop = forStatement.getLoop().accept(this);

		newFor.setInit(forStatement.getInit());
		newFor.setUpdate(forStatement.getUpdate());
		newFor.setLoop(loop);

		return newFor;
	}

	@Override
	public Statement visit(AssignStatement assignStatement) {
		return assignStatement;
	}
	
	@Override
	public Statement visit(AssumeBoundStatement assumeBoundStatement) {
		return assumeBoundStatement;
	}

	@Override
	public Statement visit(BreakStatement breakStatement) {
		return breakStatement;
	}

	@Override
	public Statement visit(CallStatement callStatement) {
		return callStatement;
	}

	@Override
	public Statement visit(ContinueStatement continueStatement) {
		return continueStatement;
	}

	@Override
	public Statement visit(GotoStatement gotoStatement) {
		return gotoStatement;
	}

	@Override
	public Statement visit(AssertStatement assertStatement) {
		return assertStatement;
	}

	@Override
	public Statement visit(ReturnStatement returnStatement) {
		return returnStatement;
	}

	@Override
	public Statement visit(InlineStatement inlineStatement) {
		return inlineStatement;
	}

}
