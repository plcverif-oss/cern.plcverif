/*******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.support.codegen.generator;

import cern.plcverif.support.codegen.program.Module;

/**
 * Abstract base class for all code generators.
 */
public abstract class Generator {

	protected Module module;

	public Generator(Module module) {
		this.module = module;
	}

	public abstract String generate();
}
