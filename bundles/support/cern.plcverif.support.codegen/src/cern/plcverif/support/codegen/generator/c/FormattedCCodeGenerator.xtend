package cern.plcverif.support.codegen.generator.c

import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.Call
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructure
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructureRef
import cern.plcverif.base.models.cfa.cfadeclaration.Field
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef
import cern.plcverif.base.models.cfa.cfadeclaration.VariableAssignment
import cern.plcverif.base.models.cfa.transformation.CfdInitialAssignmentsToTransition
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.support.codegen.generator.CFamilyCodeGenerator
import cern.plcverif.support.codegen.generator.CFamilyExprTransformer
import cern.plcverif.support.codegen.program.Module
import cern.plcverif.support.codegen.program.Procedure
import cern.plcverif.support.codegen.program.statement.AssertStatement
import cern.plcverif.support.codegen.program.statement.AssignStatement
import cern.plcverif.support.codegen.program.statement.CallStatement
import cern.plcverif.support.codegen.program.statement.GotoStatement
import cern.plcverif.support.codegen.program.statement.LabeledStatement
import cern.plcverif.support.codegen.program.statement.ReturnStatement
import cern.plcverif.support.codegen.program.statement.StatementVisitor
import cern.plcverif.verif.utils.backend.BackendNameTrace
import com.google.common.base.Preconditions
import java.util.Comparator
import java.util.List
import java.util.Map
import java.util.Set
import java.util.TreeMap

import static extension cern.plcverif.support.codegen.util.GeneratorUtils.isTempField
import cern.plcverif.support.codegen.program.statement.AssumeBoundStatement

abstract class FormattedCCodeGenerator extends CFamilyCodeGenerator implements StatementVisitor<String> {

	protected static final class CMethodNameTrace extends BackendNameTrace<AutomatonDeclaration> {
		new() {
			super([it|BackendNameTrace::basicNameSanitizer(it)]);
		}

		override protected isReservedWord(String name) {
			return C_KEYWORDS.contains(name);
		}
	}

	public val static String CONTEXT_VARIABLE = "__context";

	protected CfaNetworkDeclaration cfdNetwork
	protected CFamilyExprTransformer exprTransformer
	protected StructNames structNames
	protected TypeToCType typeTransformer;
	protected CMethodNameTrace methodNameTrace = new CMethodNameTrace();
	
	public val static Set<String> C_KEYWORDS = #{
		// List of valid C11 keywords. Source: https://en.cppreference.com/w/c/keyword
		"auto", "break", "case", "char", "const", "continue", "default", "do",
		"double", "else", "enum", "extern", "float", "for", "goto", "if",
		"inline", "int", "long", "register", "restrict", "return", "short", "signed",
		"sizeof", "static", "struct", "switch", "typedef", "union", "unsigned", "void",
		"volatile", "while", "_Alignas", "_Alignof", "_Atomic", "_Bool", "_Complex", "_Generic", "_Imaginary", "_Noreturn",
		"_Static_assert", "_Thread_local", "_Pragma",
		// convenience macros defined in the standard library
		 "alignas", "alignof", "atomic", "bool", "complex", "imaginary", "noreturn",
		"static_assert", "thread_local",
		// present on most platforms as compiler extensions
		 "asm", "fortran",
		// commonly used types, functions, literals
		"int8_t", "int16_t", "int32_t", "int64_t",
		"uint8_t", "uint16_t", "uint32_t", "uint64_t",
		"true", "false", "main"
	};

	new(Module module) {
		super(module)
		Preconditions.checkNotNull(module);
		Preconditions.checkNotNull(module.cfaNetwork);
		this.cfdNetwork = module.getCfaNetwork();
		this.structNames = new StructNames();
		this.typeTransformer = new TypeToCType(this.structNames);
		this.exprTransformer = CFamilyExprTransformer.createForCfd(CONTEXT_VARIABLE, true, this.typeTransformer);
	}
			
	override String generate() {
		return '''
		#include <stdbool.h>
		#include <stdint.h>
		#include <assert.h>
		#include <math.h>
		 
		« this.generateStructDefinitions() »

		« this.generateGlobalDefinitions() »
		
		// Forward declarations of the generated functions
		«FOR automaton : cfdNetwork.automata»
			«declareAutomaton(automaton)»
		«ENDFOR»
		
		// Declare nondet assignment functions
		« this.generateNondetDefinitions() »
		
		// Translated functions
		«FOR proc : module.procedures SEPARATOR "\n" »
			« representProcedure(proc) »
		«ENDFOR»
		
		// Entry point
		« this.generateEntryProcedure() »
		''';
	}
	
	protected def String generateGlobalDefinitions() {
		return '''
			// Global variables
			«FOR field : cfdNetwork.rootDataStructure.fields »
				«representField(field)»
			«ENDFOR»
		''';
	}
	
	protected def String generateStructDefinitions() {
		return '''
			// Struct definitions
			«FOR nestedDs : topologicalOrder(cfdNetwork.rootDataStructure.eAllContents.filter(DataStructure).toList) SEPARATOR "\n"»
				«representDataStructure(nestedDs)»
			«ENDFOR»
		''';
	}
	
	protected def String generateNondetDefinitions() {
		return '''
			bool nondet_bool(void);
			«FOR prefix : #['u', '']»
				«FOR bits : #[8, 16, 32, 64] »
					«prefix»int«bits»_t nondet_«prefix»int«bits»_t(void);
				«ENDFOR»
			«ENDFOR»
			double nondet_float(void);
			double nondet_double(void);
		''';
	}
	
	/**
	 * Generates the main logic of this generator.
	 * The default generated main procedure already contains the initial variable assignments,
	 * and a "return 0;" statement in the end. The main logic takes place between these two.
	 * If you need more control, override {@link #generateEntryProcedure() generateEntryProcedure()}. 
	 */
	protected abstract def String generateMainLogic();
	 
	protected def String generateEntryProcedure() {
		return '''
			int main(void) {
				// Initial values
				«FOR initAmt : CfdInitialAssignmentsToTransition.getInstantiatedInitAmts(cfdNetwork).filter[it | !isRefToTemp(it.leftValue)]»
					«this.generateInitialAssignment(initAmt)»
				«ENDFOR»

				// Custom entry logic
				« this.generateMainLogic() »

				return 0;
			}
		''';
	}
	 
	/**
	 * E.g. {@code void automatonName(__ContextType context);}
	 */
	private def declareAutomaton(AutomatonDeclaration automaton) {
		return '''void «methodNameTrace.toName(automaton)»(«automaton.contextVarParameter»);''';
	}
	
	private def String generateInitialAssignment(VariableAssignment initAmt) {
		return '''
			«this.transformExpression(initAmt.leftValue)» = «this.transformExpression(initAmt.rightValue)»;
		'''
	}
	
	/**
	 * E.g. {@code ContextType *context}
	 */
	private def contextVarParameter(AutomatonDeclaration automaton) {
		if (automaton.hasContext) {
			return '''«structNames.toName(automaton.localDataStructure.definition)» *«CONTEXT_VARIABLE»'''
		} else {
			return ''''''
		}
	}
	
	/**
	 * E.g. {@code typedef struct { ... } DataStructureName;}
	 */
	private def representDataStructure(DataStructure ds) {
		return '''
			typedef struct {
				«FOR field : ds.fields»
					«IF !field.isTempField»«representField(field)»«ENDIF»
				«ENDFOR»
			} «structNames.toName(ds)»;
		''';
	}
	
	private def static isEmpty(DataStructure ds) {
		return ds.fields.empty;
	}
	
	def representField(Field field) {
		return '''« this.typeTransformer.declare(field.name, field.type) »;'''
	}
	
	def String representProcedure(Procedure proc) {		
		return '''
			void «methodNameTrace.toName(proc.correspondingAutomaton)»(«proc.correspondingAutomaton.contextVarParameter») {
				// Temporary variables
				«FOR f : proc.correspondingAutomaton.localDataStructure.definition.fields.filter[it | it.isTempField]»
					«representField(f)»
				«ENDFOR»

				«proc.rootStatement.accept(this)»
			}
		''';
	}
 
    override String visit(AssignStatement assignStatement) {
    	val assignment = assignStatement.assignment;    	
    	return this.generateAssignment(assignment)
    }
    
    override String visit(AssumeBoundStatement assumeBoundStatement) {
    	val bound = assumeBoundStatement.bound;    	
    	return '''verifier_assume(«transformExpression(bound)»);'''
    }

	override visit(AssertStatement assertStatement) {
		return '''assert(«transformExpression(assertStatement.condition)»);'''
	}

    override visit(GotoStatement gotoStatement) {
        '''goto «gotoStatement.label»;'''
    }

    override visit(LabeledStatement labeledStatement) {    	
    	if (labeledStatement.innerStatement.isPresent()) {
    		return '''«labeledStatement.label»: «labeledStatement.innerStatement.get().accept(this)»'''
    	} else {
    		return '''«labeledStatement.label»: ;'''
    	}
    }

	override protected String transformExpression(Expression expr) {
		Preconditions.checkNotNull(exprTransformer, "exprTransformer has not been initialized.");
		return this.exprTransformer.toString(expr).toString();
	}
	
	override visit(ReturnStatement returnStatement) {
		return "return;"
	}
	
	override visit(CallStatement callStatement) {
		val Call call = callStatement.call;
		val AutomatonDeclaration callee = call.calledAutomaton;
		
		if (!callee.hasContext) {
			return '''« methodNameTrace.toName(callee) »();'''
		} else {
			val calleeExprTransformer = CFamilyExprTransformer.createForCfd(exprTransformer.toString(call.calleeContext), false, typeTransformer);
			return '''
				// Assign inputs
				« FOR input : call.inputAssignments »
					« calleeExprTransformer.toString(input.leftValue) » = « exprTransformer.toString(input.rightValue) »;
				« ENDFOR »
				« methodNameTrace.toName(callee) »(&« exprTransformer.toString(call.calleeContext) »);
				// Assign outputs
				« FOR output : call.outputAssignments »
					« exprTransformer.toString(output.leftValue) » = « calleeExprTransformer.toString(output.rightValue) »;
				« ENDFOR »
			''';
		}
	}
	
	def CFamilyExprTransformer getExprTransformer() {
		return this.exprTransformer;
	}
	
	// Helpers
	protected def static hasContext(AutomatonDeclaration automaton) {
		return (automaton.localDataStructure.definition.isEmpty == false);
	}

	private def boolean isRefToTemp(DataRef ref) {
		// Extract field
		var DataRef current = ref;
		while (current !== null) {
			if (current instanceof FieldRef) {
				if (current.field.isTempField) {
					return true;
				}
			}
			current = current.prefix;
		}

		return false;
	}
	
	/**
	 * Returns a topological order of the given data structures,
	 * i.e. a sorted list of them in which there is no forward reference of data structures.
	 * (If a data structure {@code A} contains a field with the data structure type {@code B},
	 * {@code A} will be strictly <b>after</b> {@code B} in the returned list. 
	 */
	private def topologicalOrder(List<DataStructure> listToOrder) {
		// very basic algorithm to create the topological order
		// Create a map that will store the indegrees
		val Comparator<DataStructure> byName = [ DataStructure o1, DataStructure o2 |
			structNames.toName(o1).compareTo(structNames.toName(o2))
		];
		val Map<DataStructure, Integer> indegree = new TreeMap(byName);

		// Fill with values
		for (DataStructure ds : listToOrder) {
			indegree.put(ds, ds.fields.filter[it|it.type instanceof DataStructureRef].size);
		}
		Preconditions.checkState(indegree.size == listToOrder.size);

		val List<DataStructure> ret = newArrayList();

		while (!indegree.empty) {
			// Find one with zero indegree
			val item = indegree.entrySet.findFirst[it|it.value == 0];
			Preconditions.checkNotNull(item, "Loop detected in data structure references.");
			Preconditions.checkState(item.value == 0);
			val key = item.key;
			indegree.remove(key);
			ret.add(key);

			// Maintain the reference counts (slow and dirty)
			for (k : indegree.keySet) {
				indegree.put(k, indegree.get(k) - k.fields.filter [ it |
					it.type instanceof DataStructureRef && (it.type as DataStructureRef).definition == key
				].size);
			}
		}

		return ret;
	}
}
