/*******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.support.codegen.program;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.StringJoiner;

import com.google.common.base.Preconditions;

import cern.plcverif.base.models.cfa.cfabase.AutomatonBase;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.support.codegen.exception.CodeGeneratorException;

/**
 * The Module class represents a single abstract program.
 */
public final class Module {

	/**
	 * Builder utility for modules
	 */
	public static final class ModuleBuilder {
		private String name;
		private Procedure mainProcedure;
		private CfaNetworkDeclaration cfaNetwork;

		// We are using LinkedHashMap to keep insertion order
		private Map<AutomatonBase, Procedure> procedures = new LinkedHashMap<>();

		public ModuleBuilder(String name, CfaNetworkDeclaration cfaNetwork) {
			this.name = name;
			this.cfaNetwork = cfaNetwork;
		}

		/**
		 * Adds a procedure to the builder
		 *
		 * @param procedure A procedure instance
		 */
		public void addFunction(AutomatonBase origin, Procedure procedure) {
			Preconditions.checkNotNull(procedure);
			this.procedures.put(origin, procedure);
		}

		/**
		 * Sets the main procedure of this module. The procedure should already be part of this module.
		 *
		 * @param procedure A Procedure instance
		 */
		public void setMainProcedure(Procedure procedure) {
			if (!this.procedures.containsValue(procedure)) {
				throw new CodeGeneratorException("The main function must be present in the module.");
			}

			this.mainProcedure = procedure;
		}

		public void setName(String name) {
			this.name = name;
		}

		/**
		 * Build the resulting Module instance
		 *
		 * @return A Module instance
		 */
		public Module build() {
			if (this.mainProcedure == null) {
				throw new CodeGeneratorException("Cannot build a Module instance without a main function");
			}
			
			if (this.cfaNetwork == null) {
				throw new CodeGeneratorException("Cannot build a Module instance without giving the corresponding CFA.");
			}
			
			return new Module(
				this.name,
				this.procedures,
				this.mainProcedure,
				this.cfaNetwork
			);
		}
	}

	private final CfaNetworkDeclaration cfaNetwork;
	private final Map<AutomatonBase, Procedure> procedures;
	private final String name;
	private final Procedure mainProcedure;

	private Module(
			String name,
			Map<AutomatonBase, Procedure> procedures,
			Procedure mainProcedure,
			CfaNetworkDeclaration cfaNetwork
	) {
		this.name = name;
		this.procedures = procedures;
		this.mainProcedure = mainProcedure;
		this.cfaNetwork = cfaNetwork;
	}

	/**
	 * Get a builder object for Module building
	 *
	 * @param name	The name of the new module
	 * @param container	Variable container for the new module
	 *
	 * @return
	 */
	public static ModuleBuilder builder(String name, CfaNetworkDeclaration cfaNetwork) {
		return new ModuleBuilder(name, cfaNetwork);
	}

	public Collection<Procedure> getProcedures() {
		return Collections.unmodifiableCollection(this.procedures.values());
	}
	
	public Procedure getProcedureFor(AutomatonBase cfa) {
		Procedure proc = this.procedures.get(cfa);
		if (proc == null) {
			throw new NoSuchElementException(String.format("No procedure was found for the automaton '%s'.", cfa.getName()));
		}
		
		return proc;
	}

	public Procedure getMainProcedure() {
		return this.mainProcedure;
	}

	public Procedure getProcedureByName(String name) {
		Optional<Procedure> procedure = this.getProcedures().stream().filter(p -> p.name.equals(name)).findAny();
		if (!procedure.isPresent()) {
			throw new NoSuchElementException("No procedure exists with the name: " + name);
		}
		
		return procedure.get();
	}

	public String getName() {
		return this.name;
	}

	public CfaNetworkDeclaration getCfaNetwork() {
		return cfaNetwork;
	}
	
	@Override
	public String toString() {
		StringJoiner sj = new StringJoiner("\n");
		for (Procedure function : this.getProcedures()) {
			sj.add(function.toString());
		}

		return String.format("Module(%s){%n%s%n}", this.name, sj.toString());
	}

}
