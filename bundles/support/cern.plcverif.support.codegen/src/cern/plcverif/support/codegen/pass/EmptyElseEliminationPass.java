/*******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.support.codegen.pass;

import cern.plcverif.support.codegen.program.Procedure;
import cern.plcverif.support.codegen.program.statement.BlockStatement;
import cern.plcverif.support.codegen.program.statement.IfStatement;
import cern.plcverif.support.codegen.program.statement.Statement;

/**
 * Simplification pass to remove empty else blocks from if-else statements
 */
public class EmptyElseEliminationPass extends StatementTransformVisitor implements ModulePass {

	@Override
	public void run(Procedure function) {
		Statement root = function.getRootStatement().accept(this);
		function.setRootStatement(root);
	}

	@Override
	public Statement visit(IfStatement ifStatement) {
		if (!ifStatement.getElze().isPresent()) {
			return super.visit(ifStatement);
		}

		Statement elze = ifStatement.getElze().get();
		if (elze instanceof BlockStatement && ((BlockStatement) elze).getStatements().isEmpty()) {
			Statement then = ifStatement.getThen().accept(this);
			ifStatement.setThen(then);
			ifStatement.clearElze();

			return ifStatement;
		}

		return super.visit(ifStatement);
	}

}
