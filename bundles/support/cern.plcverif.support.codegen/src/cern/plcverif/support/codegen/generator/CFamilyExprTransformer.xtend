/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.support.codegen.generator

import cern.plcverif.base.models.cfa.cfabase.ElseExpression
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef
import cern.plcverif.base.models.cfa.cfadeclaration.Indexing
import cern.plcverif.base.models.cfa.cfainstance.ArrayElementRef
import cern.plcverif.base.models.cfa.cfainstance.VariableRef
import cern.plcverif.base.models.cfa.utils.CfaDeclarationUtils
import cern.plcverif.base.models.expr.BinaryArithmeticOperator
import cern.plcverif.base.models.expr.BinaryLogicExpression
import cern.plcverif.base.models.expr.BinaryLogicOperator
import cern.plcverif.base.models.expr.ComparisonOperator
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.LibraryFunction
import cern.plcverif.base.models.expr.Nondeterministic
import cern.plcverif.base.models.expr.TypeConversion
import cern.plcverif.base.models.expr.string.AbstractExprToString
import cern.plcverif.support.codegen.exception.CodeGeneratorException
import cern.plcverif.support.codegen.generator.c.TypeToCType
import com.google.common.base.Preconditions

import static extension cern.plcverif.support.codegen.util.GeneratorUtils.isTempField
import cern.plcverif.base.models.expr.StringLiteral
import java.util.regex.Matcher
import cern.plcverif.base.models.expr.ExpressionSafeFactory
import java.util.regex.Pattern

/**
 * CFA to C expression translation.
 */
class CFamilyExprTransformer extends AbstractExprToString {
	/**
	 * 
	 * May be null. In that case only CFI elements are supported.
	 */
	final CharSequence targetContextName;
	final boolean targetContextIsPointer;
	
	/**
	 * 
	 * Shall not be null.
	 */
	protected final TypeToCType typeToString;
	
	/**
	 * Creates a new CFA to C expression translation object.
	 * 
	 * The non-global, non-temporary variables are assumed to be in the context {@code targetContextName}. 
	 * If {@code targetContextIsPointer} is true, the non-global, non-temporary variables will be accessed using the {@code ->} operator,
	 * otherwise using the {@code .} operator. The CFA data structures are represented using their names given by {@code structNames}.
	 */
	new(CharSequence targetContextName, boolean targetContextIsPointer, TypeToCType typeToString) {
		// TODO: have some name tracing
		this.targetContextName = targetContextName;
		this.targetContextIsPointer = targetContextIsPointer;
		this.typeToString = Preconditions.checkNotNull(typeToString);
	}
	
	static def createForCfi(TypeToCType typeTransformer) {
		return new CFamilyExprTransformer(null, false, typeTransformer);
	}
	
	static def createForCfd(CharSequence targetContextName, boolean targetContextIsPointer, TypeToCType typeToString) {
		return new CFamilyExprTransformer(targetContextName, targetContextIsPointer, typeToString);
	}

	override dispatch CharSequence toString(Expression expr) {
		return "???"
	}

	def dispatch CharSequence toString(VariableRef expr) {
		return '''«expr.variable.name»''';
	}
	
	def dispatch CharSequence toString(ArrayElementRef expr) {
		return '''«expr.variable.name»«FOR idx : expr.indices»[«toString(idx)»]«ENDFOR»''';
	}

	def dispatch CharSequence toString(FieldRef expr) {
		if (targetContextName === null) {
			throw new UnsupportedOperationException("This specific instance can only be used for CFIs, as 'targetContextName' was not initialized.");
		}
		
		if (expr.prefix !== null) {
			return '''«toString(expr.prefix)».«expr.field.name»'''
		} else {
			if (!CfaDeclarationUtils.isFieldDefinedInRoot(expr.field) && !expr.field.isTempField) {
				// Not global, not temp: we need the context to refer to the variable.
				return '''«targetContextName»«IF targetContextIsPointer»->«ELSE».«ENDIF»«expr.field.name»''';
			} else {
				return '''«expr.field.name»''';
			}
		}
	}

	def dispatch CharSequence toString(Indexing expr) {
		Preconditions.checkNotNull(expr.prefix);
		return '''«toString(expr.prefix)»[«toString(expr.index)»]'''
	}

	override dispatch CharSequence toString(Nondeterministic expr) {
		return '''nondet_«this.typeToString.typeName(expr.type)»()'''
	}

	def dispatch CharSequence toString(ElseExpression expr) {
		throw new UnsupportedOperationException("ElseExpression expressions are not supported by this generator.")
	}

	override dispatch CharSequence toString(TypeConversion expr) {
		return '''((«this.typeToString.typeName(expr.type)») «this.toString(expr.operand)»)'''
	}

	override dispatch CharSequence toString(BinaryLogicExpression expr) {
		if (expr.operator == BinaryLogicOperator.IMPLIES) {
			return '''(!(«this.toString(expr.leftOperand)») || «this.toString(expr.rightOperand)»)'''
		}

		return '''(«this.toString(expr.leftOperand)» «this.opToString(expr.operator)» «this.toString(expr.rightOperand)»)'''
	}
	
	override dispatch CharSequence toString(LibraryFunction expr) {
		switch (expr.function) {
			case SIN,
			case COS,
			case TAN,
			case ASIN,
			case ACOS,
			case ATAN,
			case SQRT,
			case EXP: {
				Preconditions.checkState(expr.operands.size == 1, expr.function.getName() + " must have exactly 1 operand.");
				val cFuncName = expr.function.getName.toLowerCase;
				return '''«cFuncName»(«toString(expr.operands.get(0))»)'''
			}
			
			case LN: {
				Preconditions.checkState(expr.operands.size == 1, expr.function.getName() + " must have exactly 1 operand.");
				val cFuncName = "log";
				return '''«cFuncName»(«toString(expr.operands.get(0))»)'''
			}
			
			case LOG: {
				Preconditions.checkState(expr.operands.size == 1, expr.function.getName() + " must have exactly 1 operand.");
				val cFuncName = "log10";
				return '''«cFuncName»(«toString(expr.operands.get(0))»)'''
			}
		}

		throw new CodeGeneratorException(String.format("The C language translator plug-in does not know how to represent the library function '%s'.", expr.function.getName()));
	}
	
	override dispatch CharSequence toString(StringLiteral expr) {
		 throw new AssertionError("Strings are not supported.")
	}

	def String notOpToString() {
		return "!"
	}

	override String opToString(BinaryLogicOperator operator) {
		switch (operator) {
			case AND:
				return "&&"
			case OR:
				return "||"
			case XOR:
				return "!="
			case IMPLIES:
				throw new AssertionError("IMPLIES operator should have been generated earlier")
			default:
				throw new UnsupportedOperationException("Unknown arithmetic operator: " + operator)
		}
	}

	override String opToString(ComparisonOperator operator) {
		switch (operator) {
			case EQUALS: return "=="
			case NOT_EQUALS: return "!="
			case LESS_THAN: return "<"
			case GREATER_THAN: return ">"
			case LESS_EQ: return "<="
			case GREATER_EQ: return ">="
		}
		throw new UnsupportedOperationException("Unknown comparison/equality operator: " + operator);
	}

	override String opToString(BinaryArithmeticOperator operator) {
		switch (operator) {
			case PLUS:
				return "+"
			case MINUS:
				return "-"
			case MULTIPLICATION:
				return "*"
			case DIVISION:
				return "/"
			case MODULO:
				return "%"
			case INTEGER_DIVISION:
				return "/"
			case BITSHIFT_LEFT:
				return "<<"
			case BITSHIFT_RIGHT:
				return ">>"
			case POWER:
				throw new UnsupportedOperationException("Cannot generate power operator for C code.")
			case BITWISE_AND:
				return "&"
			case BITWISE_OR:
				return "|"
			case BITWISE_XOR:
				return "^"
			default:
				throw new UnsupportedOperationException("Unknown arithmetic operator: " + operator)
		}
	}
}