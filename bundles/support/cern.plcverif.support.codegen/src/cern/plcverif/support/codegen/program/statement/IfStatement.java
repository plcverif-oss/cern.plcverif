/*******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.support.codegen.program.statement;

import java.util.Optional;

import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.string.ExprToString;

public final class IfStatement extends ConditionedStatement {

	private Statement then;
	private Optional<Statement> elze = Optional.empty();

	public IfStatement(Expression condition) {
		super(condition);
	}

	@Override
	public <T> T accept(StatementVisitor<T> visitor) {
		return visitor.visit(this);
	}

	public Statement getThen() {
		return this.then;
	}

	public void setThen(Statement then) {
		this.then = then;
	}

	public Optional<Statement> getElze() {
		return this.elze;
	}

	public void setElze(Statement elze) {
		this.elze = Optional.of(elze);
	}

	@Override
	public String toString() {
		if (this.elze.isPresent()) {
			return String.format("If(%s)Then{%s}Else{%s}Endif()", ExprToString.INSTANCE.toString(this.condition), this.then.toString(), this.elze.get().toString());
		}

		return String.format("If(%s)Then{%s}Endif()", ExprToString.INSTANCE.toString(this.condition), this.then.toString());
	}

	public void clearElze() {
		this.elze = Optional.empty();
	}

}
