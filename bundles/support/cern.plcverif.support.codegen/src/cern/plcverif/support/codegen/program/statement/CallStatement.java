/*******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.support.codegen.program.statement;

import java.util.List;
import java.util.StringJoiner;

import cern.plcverif.base.models.cfa.cfadeclaration.Call;
import cern.plcverif.base.models.cfa.cfadeclaration.VariableAssignment;
import cern.plcverif.base.models.expr.string.ExprToString;

public final class CallStatement implements Statement {

	private Call call;

	public CallStatement(Call call) {
		this.call = call;
	}

	public Call getCall() {
		return this.call;
	}

	@Override
	public <T> T accept(StatementVisitor<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public String toString() {
		String inputs = formatAssignments(this.call.getInputAssignments());
		String outputs = formatAssignments(this.call.getOutputAssignments());

		return String.format("Call(%s, {%s}, {%s})", this.call.getCalledAutomaton().getName(), inputs, outputs);
	}

	private static String formatAssignments(List<VariableAssignment> assignments) {
		StringJoiner sj = new StringJoiner(", ");
		assignments.forEach(a -> {
			String assign = String.format("%s := %s",
				ExprToString.INSTANCE.toString(a.getLeftValue()),
				ExprToString.INSTANCE.toString(a.getRightValue())
			);
			sj.add(assign);
		});

		return sj.toString();
	}

}
