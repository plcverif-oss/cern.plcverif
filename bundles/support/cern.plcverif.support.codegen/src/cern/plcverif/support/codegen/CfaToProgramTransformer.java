/*******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.support.codegen;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.base.Preconditions;

import cern.plcverif.base.models.cfa.cfabase.AssertionAnnotation;
import cern.plcverif.base.models.cfa.cfabase.AutomatonBase;
import cern.plcverif.base.models.cfa.cfabase.CaseLocationAnnotation;
import cern.plcverif.base.models.cfa.cfabase.ContinueTransitionAnnotation;
import cern.plcverif.base.models.cfa.cfabase.ElseExpression;
import cern.plcverif.base.models.cfa.cfabase.ExitTransitionAnnotation;
import cern.plcverif.base.models.cfa.cfabase.GotoTransitionAnnotation;
import cern.plcverif.base.models.cfa.cfabase.IfLocationAnnotation;
import cern.plcverif.base.models.cfa.cfabase.LabelledLocationAnnotation;
import cern.plcverif.base.models.cfa.cfabase.LineNumberAnnotation;
import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfabase.LocationAnnotation;
import cern.plcverif.base.models.cfa.cfabase.LoopLocationAnnotation;
import cern.plcverif.base.models.cfa.cfabase.RepeatLoopLocationAnnotation;
import cern.plcverif.base.models.cfa.cfabase.SwitchLocationAnnotation;
import cern.plcverif.base.models.cfa.cfabase.Transition;
import cern.plcverif.base.models.cfa.cfabase.TransitionAnnotation;
import cern.plcverif.base.models.cfa.cfabase.WhileLoopLocationAnnotation;
import cern.plcverif.base.models.cfa.cfadeclaration.AssignmentTransition;
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.Call;
import cern.plcverif.base.models.cfa.cfadeclaration.CallTransition;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.ForLoopLocationAnnotation;
import cern.plcverif.base.models.cfa.utils.CfaUtils;
import cern.plcverif.base.models.expr.BinaryLogicExpression;
import cern.plcverif.base.models.expr.BinaryLogicOperator;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.ExpressionSafeFactory;
import cern.plcverif.support.codegen.program.Module;
import cern.plcverif.support.codegen.program.Module.ModuleBuilder;
import cern.plcverif.support.codegen.program.Procedure;
import cern.plcverif.support.codegen.program.statement.AssertStatement;
import cern.plcverif.support.codegen.program.statement.AssignStatement;
import cern.plcverif.support.codegen.program.statement.AssumeBoundStatement;
import cern.plcverif.support.codegen.program.statement.BlockStatement;
import cern.plcverif.support.codegen.program.statement.BreakStatement;
import cern.plcverif.support.codegen.program.statement.CallStatement;
import cern.plcverif.support.codegen.program.statement.ContinueStatement;
import cern.plcverif.support.codegen.program.statement.DoWhileStatement;
import cern.plcverif.support.codegen.program.statement.ForStatement;
import cern.plcverif.support.codegen.program.statement.GotoStatement;
import cern.plcverif.support.codegen.program.statement.IfStatement;
import cern.plcverif.support.codegen.program.statement.LabeledStatement;
import cern.plcverif.support.codegen.program.statement.ReturnStatement;
import cern.plcverif.support.codegen.program.statement.Statement;
import cern.plcverif.support.codegen.program.statement.WhileStatement;
import cern.plcverif.support.codegen.util.GeneratorUtils;


public final class CfaToProgramTransformer {

	private final CfaNetworkDeclaration network;

	private boolean ignoreAssertions = false;
	
	private CfaToProgramTransformer(CfaNetworkDeclaration network) {
		this.network = network;
	}
	
	public static CfaToProgramTransformer create(CfaNetworkDeclaration network) {
		return new CfaToProgramTransformer(network);
	}
	
	/**
	 * Builds a new language-independent module from an automata system
	 */
	public static Module transform(CfaNetworkDeclaration network) {
		CfaToProgramTransformer moduleTransform = new CfaToProgramTransformer(network);
		return moduleTransform.createModule();
	}

	public Module createModule() {
		ModuleBuilder builder = Module.builder(this.network.getName(), this.network);
		
		for (AutomatonDeclaration automaton : this.network.getAutomata()) {
			String name = automaton.getName();
			// Perform statement transformation
			FunctionTransformation transformation = new FunctionTransformation(automaton);
			BlockStatement root = new BlockStatement(transformation.transform());			
			
			Procedure procedure = new Procedure(name, root, automaton);			
			builder.addFunction(automaton, procedure);
			
			if (this.network.getMainAutomaton() == automaton) {
				builder.setMainProcedure(procedure);
			}
		}

		return builder.build();
	}

	public boolean isIgnoreAssertions() {
		return ignoreAssertions;
	}

	public void setIgnoreAssertions(boolean ignoreAssertions) {
		this.ignoreAssertions = ignoreAssertions;
	}


	/**
	 * Nested class for a single function transformation
	 */
	private final class FunctionTransformation {

		/**
		 * Stores the ending points of nested control structures
		 */
		private final Deque<Location> currentEnds = new ArrayDeque<>();

		/**
		 * Locations which were skipped during generation, but are actually reachable by goto jumps
		 */
		private final Queue<Location> skippedLocs = new ArrayDeque<>();
		private final AutomatonBase automaton;

		private FunctionTransformation(AutomatonBase automaton) {
			this.automaton = automaton;
		}

		private List<Statement> transform() {
			List<Statement> root = new ArrayList<>();
			this.currentEnds.push(this.automaton.getEndLocation());

			Set<Location> visited = new HashSet<>();

			this.transformStatements(this.automaton.getInitialLocation(), visited, root);

			while (!this.skippedLocs.isEmpty()) {
				Location loc = this.skippedLocs.poll();
				this.transformStatements(loc, visited, root);
			}

			return root;
		}

		private void transformStatements(Location loc, Set<Location> visited, List<Statement> parent) {
			if (visited.contains(loc)) {
				return;
			}

			visited.add(loc);

			// Create a label for this location
			// FIXME: If there are labels declared in the automaton with the same name as a location, the same label name will be generated twice.
			LabeledStatement label = new LabeledStatement(GeneratorUtils.getLocationLabelName(loc));
			parent.add(label);

			// See if there is an assertion annotation on this location
			if (!ignoreAssertions) {
				List<AssertionAnnotation> assertAnnotations = loc.getAnnotations().stream()
					.filter(a -> a instanceof AssertionAnnotation).map(a -> (AssertionAnnotation) a).collect(Collectors.toList());
				assertAnnotations.forEach(a -> parent.add(new AssertStatement(a.getInvariant())));
			}

			if (loc == this.automaton.getEndLocation()) {
				// If there is a label right at the end location, we would skip its generation.
				// Handle that special case here.
				LabelledLocationAnnotation labelAnnotation = CfaUtils.getFirstAnnotationOfType(loc, LabelledLocationAnnotation.class); 
				if (labelAnnotation != null) {
					parent.add(new LabeledStatement(labelAnnotation.getLabelName()));
				}
				
				parent.add(new ReturnStatement());
				// This is the last location - do not continue
				return;
			}

			// See if the annotations tell us anything
			boolean structureAnnotationFound = false;

			for (LocationAnnotation annotation : loc.getAnnotations()) {
				if (annotation instanceof IfLocationAnnotation) {
					this.handleIfStatement(loc, visited, parent, (IfLocationAnnotation) annotation);
					structureAnnotationFound = true;
				} else if (annotation instanceof WhileLoopLocationAnnotation) {
					this.handleWhileLoop(loc, visited, parent, (WhileLoopLocationAnnotation) annotation);
					structureAnnotationFound = true;
				} else if (annotation instanceof RepeatLoopLocationAnnotation) {
					this.handleRepeatLoop(loc, visited, parent, (RepeatLoopLocationAnnotation) annotation);
					structureAnnotationFound = true;
				} else if (annotation instanceof ForLoopLocationAnnotation) {
					this.handleForLoop(loc, visited, parent, (ForLoopLocationAnnotation) annotation);
					structureAnnotationFound = true;
				} else if (annotation instanceof SwitchLocationAnnotation) {
					this.handleSwitch(loc, visited, parent, (SwitchLocationAnnotation) annotation);
					structureAnnotationFound = true;
				} else if (annotation instanceof AssertionAnnotation) {
					// Do nothing, we have handled this before
				} else if (annotation instanceof CaseLocationAnnotation) {
					// Do nothing, these were handled in their parent switch
				} else if (annotation instanceof LabelledLocationAnnotation) {
					parent.add(new LabeledStatement(((LabelledLocationAnnotation) annotation).getLabelName()));
				} else if (annotation instanceof LineNumberAnnotation) {
					// Do nothing for the moment.
					// TODO: Add support line number annotations in statements.
				} else {
					System.err.println("Unhandled annotation: " + annotation.getClass().getName());
				}
			}

			if (!structureAnnotationFound) {
				// Fallback if there are no structure annotations
				if (loc.getOutgoing().size() == 1) {
					// Create and unconditional jump to this target
					Transition transition = loc.getOutgoing().get(0);
					Location target = transition.getTarget();

					this.handleTransition(parent, transition);

					// Try not to generate a goto statement, unless it is really necessary.
					if (!canAvoidGoto(transition, target)) {
						parent.add(new GotoStatement(GeneratorUtils.getLocationLabelName(target)));
					}

					// Goto only parents?
					if (CfaUtils.getAllAnnotationsOfType(transition, GotoTransitionAnnotation.class).size() == 1) {
						// If this is a GOTO transition, do not continue the generation to its target.
						// This may cause the generator to entirely skip the generation of the target location,
						// if the target is not reachable by other means.
						// In that case, they need to be scheduled for generation later.
						if (this.unreachableByControl(target)) {
							this.skippedLocs.add(target);
						}
					} else {
						this.transformStatements(target, visited, parent);
					}
				} else if (loc.getOutgoing().size() >= 2) {
					// If the location has multiple children and no structure annotations are available,
					// we will just use conditioned gotos.
					this.generateConditionalGotosFromOutgoingTransitions(loc, parent);
				}
			}
		}

		private boolean canAvoidGoto(Transition transition, Location target) {
			// No goto is needed if:
			// 	  1. the path is sequential (single parent and single child),
			//    2. the target node has multiple parents, but it is a merging point of a branching path,
			//    3. the target is a loop header
			//    4. the target transition has a structure annotation
			if (target.getIncoming().size() == 1 && target != this.automaton.getEndLocation()) {
				return true;
			}
			
			if (!this.currentEnds.isEmpty() && target == this.currentEnds.peek()) {
				return true;				
			}
			
			if (this.hasLoopAnnotation(target)) {
				return true;
			}
			
			if (this.hasJumpAnnotation(transition)) {
				return true;
			}
			
			return false;
		}

		private void generateConditionalGotosFromOutgoingTransitions(Location loc, List<Statement> parent) {
			for (Transition transition : loc.getOutgoing()) {
				Expression cond = transition.getCondition();
				GotoStatement jump = new GotoStatement(GeneratorUtils.getLocationLabelName(transition.getTarget()));

				Preconditions.checkState(cond != null, "Multiple outgoing transitions from a single location should be conditioned.");
				
				IfStatement ifJump = new IfStatement(cond);

				List<Statement> ifStmts = new ArrayList<>();
				this.handleTransition(ifStmts, transition);

				ifStmts.add(jump);
				ifJump.setThen(new BlockStatement(ifStmts));

				parent.add(ifJump);
			}

			// We do not wish to generate these locations now,
			// because their natural order might not be correct.
			List<Location> targets = loc.getOutgoing()
				.stream()
				.map(t -> t.getTarget())
				.collect(Collectors.toList());

			this.skippedLocs.addAll(targets);
		}

		private void handleTransition(List<Statement> parent, Transition transition) {
			if (transition instanceof AssignmentTransition) {
				((AssignmentTransition) transition).getAssignments()
					.stream()
					.map(AssignStatement::new)
					.forEach(parent::add);
				((AssignmentTransition) transition).getAssumeBounds()
				.stream()
				.map(AssumeBoundStatement::new)
				.forEach(parent::add);
			} else if (transition instanceof CallTransition) {
				CallTransition callTransition = (CallTransition) transition;
				for (Call call : callTransition.getCalls()) {
					parent.add(new CallStatement(call));
				}
			}

			for (TransitionAnnotation annotation : transition.getAnnotations()) {
				if (annotation instanceof ContinueTransitionAnnotation) {
					parent.add(new ContinueStatement());
				} else if (annotation instanceof ExitTransitionAnnotation) {
					parent.add(new BreakStatement());
				} else if (annotation instanceof GotoTransitionAnnotation) {
					parent.add(new GotoStatement(((GotoTransitionAnnotation) annotation).getTargetLabel()));
				} else {
					System.err.println("Unhandled transition annotation: " + annotation.getClass());
				}
			}
		}

		private void handleIfStatement(Location loc, Set<Location> visited, List<Statement> parent,
				IfLocationAnnotation ifAnnotation) {

			// Find the 'then' and 'else' children
			// TODO: We should not depend on transition ordering
			
			Transition then = ifAnnotation.getThen();
			Transition elze = loc.getOutgoing().get(loc.getOutgoing().size() - 1);
			Location merge = ifAnnotation.getEndsAt();

			// The merging point should not be visited here
			Set<Location> restricted = new HashSet<>(visited);
			restricted.add(merge);

			this.currentEnds.push(merge);

			// It is possible that a location may have transitions before the 'then' transition,
			// which are not part of the if statement. Find these transitions and handle them now.
			int thenIdx = loc.getOutgoing().indexOf(then);
			Preconditions.checkState(thenIdx != -1,
					"The 'then' transition of an IF statement must be present among its outgoing transitions!");
			
			for (int i = 0; i < thenIdx; ++i) {
				Transition transition = loc.getOutgoing().get(i);
				this.handleTransition(parent, transition);
				this.transformStatements(transition.getTarget(), restricted, parent);
			}
			
			IfStatement stmt = new IfStatement(then.getCondition());

			parent.add(stmt);

			List<Statement> thenStmts = new ArrayList<>();
			this.handleTransition(thenStmts, then);
			this.transformStatements(then.getTarget(), restricted, thenStmts);
		
			stmt.setThen(new BlockStatement(thenStmts));
			
			ExpressionSafeFactory exprFactory = ExpressionSafeFactory.INSTANCE;
			
			List<Expression> handledConditions = new ArrayList<>();
			handledConditions.add(then.getCondition());
			
			// If there are other paths (due to an 'else if'), handle them here
			for (int i = thenIdx + 1; i < loc.getOutgoing().size() - 1; i++) {
				Transition transition = loc.getOutgoing().get(i);
				Expression condition = transition.getCondition();

				if (condition instanceof BinaryLogicExpression && ((BinaryLogicExpression) condition).getOperator() == BinaryLogicOperator.AND) {
					BinaryLogicExpression andExpr = (BinaryLogicExpression) condition;
					Expression previousBranches = exprFactory.and(handledConditions.stream().map(it -> exprFactory.neg(it)));
					
					if (EcoreUtil.equals(andExpr.getLeftOperand(), previousBranches)) {
						condition = andExpr.getRightOperand();
						handledConditions.add(condition);
					}
				}
				
				IfStatement elzeIf = new IfStatement(condition);

				List<Statement> elzeIfThen = new ArrayList<>();
				
				this.handleTransition(elzeIfThen, transition);

				this.transformStatements(transition.getTarget(), restricted, elzeIfThen);
				elzeIf.setThen(new BlockStatement(elzeIfThen));

				stmt.setElze(elzeIf);
				stmt = elzeIf;
			}

			List<Statement> elzeStmts = new ArrayList<>();
			this.handleTransition(elzeStmts, elze);
			this.transformStatements(elze.getTarget(), restricted, elzeStmts);

			BlockStatement elzeBlock = new BlockStatement(elzeStmts);

			// Collect the previous guards. 
			List<Expression> guards = new ArrayList<>();
			for (int i = 0; i < loc.getOutgoing().size() - 1; i++) {
				guards.add(loc.getOutgoing().get(i).getCondition());
			}
			
			if (isElseBranch(elze.getCondition(), guards)) {
				stmt.setElze(elzeBlock);
			} else {
				IfStatement elzeIf = new IfStatement(elze.getCondition());
				elzeIf.setThen(elzeBlock);
				stmt.setElze(elzeIf);
			}
			

			this.currentEnds.pop();

			restricted.remove(merge);
			visited.addAll(restricted);

			// Visit the merging point and proceed as normal
			this.transformStatements(ifAnnotation.getEndsAt(), visited, parent);
		}
		
		private boolean isElseBranch(Expression condition, List<Expression> previousConditions) {
			if (condition instanceof ElseExpression) {
				return true;
			}
			
			// It is possible that the condition is in the format of
			// NOT(C1) AND NOT(C2) AND ...
			Expression prev = ExpressionSafeFactory.INSTANCE.and(previousConditions.stream().map(
				c -> ExpressionSafeFactory.INSTANCE.neg(c))
			);
			
			return EcoreUtil.equals(condition, prev);
		}

		private void handleSwitch(Location loc, Set<Location> visited, List<Statement> parent,
				SwitchLocationAnnotation switchAnnotation) {
			// The CASE statement in SCL more expressive than the SWITCH construct in most
			// programming languages. As such, we do not attempt to recreate it as a SWITCH,
			// just represent the case statement as a series of IF-ELSE statements.			
			int numCases = switchAnnotation.getCases().size();
			Preconditions.checkArgument(numCases >= 1);

			boolean hasElseCase = switchAnnotation.getElseCase() != null;
			Location merge = switchAnnotation.getEndsAt();
			
			Set<Location> restricted = new HashSet<>(visited);
			restricted.add(merge);

			this.currentEnds.push(merge);
			
			// Handle the first transition
			Preconditions.checkArgument(loc.getOutgoing().size() == 2, "A location with a SWITCH annotation may only have 2 outgoing transitions.");			
			Location currentCase = switchAnnotation.getCases().get(0).getParentLocation();
			
			Preconditions.checkArgument(currentCase.getIncoming().size() == 1);
			IfStatement stmt = new IfStatement(currentCase.getIncoming().get(0).getCondition());
			
			parent.add(stmt);
			List<Statement> thenStmts = new ArrayList<>();
			this.transformStatements(currentCase, restricted, thenStmts);

			stmt.setThen(new BlockStatement(thenStmts));
			
			for (int i = 1; i < (hasElseCase ? numCases - 1 : numCases); ++i) {
				currentCase = switchAnnotation.getCases().get(i).getParentLocation();
				Preconditions.checkArgument(currentCase.getIncoming().size() == 1);
				
				IfStatement elzeIf = new IfStatement(currentCase.getIncoming().get(0).getCondition());
				List<Statement> elzeIfThen = new ArrayList<>();

				this.transformStatements(currentCase, restricted, elzeIfThen);
				elzeIf.setThen(new BlockStatement(elzeIfThen));

				stmt.setElze(elzeIf);
				stmt = elzeIf;
			}
			
			// Handle the default case
			CaseLocationAnnotation elzeCase = switchAnnotation.getElseCase();
			if (elzeCase != null) {
				List<Statement> elzeStmts = new ArrayList<>();
				this.transformStatements(elzeCase.getParentLocation(), restricted, elzeStmts);
				
				stmt.setElze(new BlockStatement(elzeStmts));
			}

			this.currentEnds.pop();

			restricted.remove(merge);
			visited.addAll(restricted);

			this.transformStatements(merge, visited, parent);
		}
		
		private void handleRepeatLoop(Location loc, Set<Location> visited, List<Statement> parent,
				RepeatLoopLocationAnnotation annotation) {
			// Get the important edges
			Transition loopTransition = annotation.getLoopBodyEntry();
			Transition backEdge = annotation.getLoopNextCycle();
			Transition exitTransition = annotation.getLoopExit();

			DoWhileStatement stmt = new DoWhileStatement(backEdge.getCondition());

			this.currentEnds.push(exitTransition.getTarget());

			Set<Location> restricted = new HashSet<>(visited);
			restricted.add(annotation.getLoopExit().getSource());
			restricted.add(annotation.getLoopExit().getTarget());

			List<Statement> loopBlock = new ArrayList<>();
			this.handleTransition(loopBlock, loopTransition);
			this.transformStatements(loopTransition.getTarget(), restricted, loopBlock);

			this.currentEnds.pop();

			stmt.setLoop(new BlockStatement(loopBlock));
			parent.add(stmt);

			restricted.remove(exitTransition.getTarget());
			visited.addAll(restricted);

			// Proceed with the exit block
			this.transformStatements(exitTransition.getTarget(), visited, parent);
		}

		private void handleWhileLoop(Location loc, Set<Location> visited, List<Statement> parent,
				WhileLoopLocationAnnotation annotation) {
			// Find the loop edge
			Transition loopTransition = annotation.getLoopBodyEntry();
			Transition exitTransition = annotation.getLoopExit();

			WhileStatement stmt = new WhileStatement(loopTransition.getCondition());

			this.currentEnds.push(exitTransition.getTarget());

			Set<Location> restricted = new HashSet<>(visited);
			restricted.add(exitTransition.getTarget());

			List<Statement> loopBlock = new ArrayList<>();
			this.handleTransition(loopBlock, loopTransition);
			this.transformStatements(loopTransition.getTarget(), restricted, loopBlock);

			this.currentEnds.pop();
			stmt.setLoop(new BlockStatement(loopBlock));

			parent.add(stmt);

			restricted.remove(exitTransition.getTarget());
			visited.addAll(restricted);

			// Proceed with the exit block
			this.transformStatements(exitTransition.getTarget(), visited, parent);
		}

		private void handleForLoop(Location loc, Set<Location> visited, List<Statement> parent,
				ForLoopLocationAnnotation annotation) {
			Preconditions.checkState(annotation.getLoopVariableInit() instanceof AssignmentTransition,
					"The initialization transition of a for loop should be an AssignmentTransition.");
			Preconditions.checkState(annotation.getLoopVariableIncrement() instanceof AssignmentTransition,
					"The increment transition of a for loop should be an AssignmentTransition.");
		
			AssignmentTransition loopInit = (AssignmentTransition) annotation.getLoopVariableInit();
			Transition loopTransition = annotation.getLoopBodyEntry();
			Transition exitTransition = annotation.getLoopExit();
			AssignmentTransition increment = (AssignmentTransition) annotation.getLoopVariableIncrement();

			this.currentEnds.push(exitTransition.getTarget());

			Set<Location> restricted = new HashSet<>(visited);
			restricted.add(exitTransition.getTarget());
			restricted.add(increment.getSource());
			restricted.add(loopTransition.getSource());
			restricted.add(increment.getTarget());

			List<Statement> loopBlock = new ArrayList<>();
			this.transformStatements(loopTransition.getTarget(), restricted, loopBlock);

			// TODO: Continue statements can break if the increment transition label does not exist.
			// This workaround makes sure we generate a label for it.
			loopBlock.add(new LabeledStatement(GeneratorUtils.getLocationLabelName(increment.getSource())));
			
			this.currentEnds.pop();

			Preconditions.checkState(loopInit.getAssignments().size() == 1);
			Preconditions.checkState(increment.getAssignments().size() == 1);

			ForStatement forStatement = new ForStatement(loopTransition.getCondition());
			forStatement.setInit(
				loopInit.getAssignments().get(0)
			);
			forStatement.setLoop(new BlockStatement(loopBlock));
			forStatement.setUpdate(
				increment.getAssignments().get(0)
			);

			parent.add(forStatement);

			restricted.remove(exitTransition.getTarget());
			visited.add(loc);
			visited.addAll(restricted);

			// Proceed with the exit block
			this.transformStatements(exitTransition.getTarget(), visited, parent);
		}

		private boolean hasJumpAnnotation(Transition transition) {
			return transition.getAnnotations().stream().filter(a -> 
				a instanceof ContinueTransitionAnnotation
				|| a instanceof ExitTransitionAnnotation
				|| a instanceof GotoTransitionAnnotation
			).count() != 0;
		}

		private boolean hasLoopAnnotation(Location target) {
			return target.getAnnotations().stream().filter(a -> a instanceof LoopLocationAnnotation).count() == 1;
		}

		private boolean unreachableByControl(Location loc) {
			return loc.getIncoming().stream().allMatch(edge -> this.hasJumpAnnotation(edge));
		}
	}
}
