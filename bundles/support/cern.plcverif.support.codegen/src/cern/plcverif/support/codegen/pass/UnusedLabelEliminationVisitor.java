/*******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.support.codegen.pass;

import java.util.ArrayList;
import java.util.List;

import cern.plcverif.support.codegen.program.Procedure;
import cern.plcverif.support.codegen.program.statement.BlockStatement;
import cern.plcverif.support.codegen.program.statement.DoWhileStatement;
import cern.plcverif.support.codegen.program.statement.IfStatement;
import cern.plcverif.support.codegen.program.statement.LabeledStatement;
import cern.plcverif.support.codegen.program.statement.Statement;
import cern.plcverif.support.codegen.program.statement.WhileStatement;

/**
 * Transformation pass that removes unused labels from a module
 */
public class UnusedLabelEliminationVisitor extends StatementTransformVisitor implements ModulePass {

	private List<String> usedLabels = new ArrayList<>();

	@Override
	public void run(Procedure function) {
		this.usedLabels.clear();
		this.usedLabels.addAll(LabelDiscovery.getVisitedLabels(function));

		Statement root = function.getRootStatement().accept(this);
		function.setRootStatement(root);
	}

	@Override
	public Statement visit(BlockStatement blockStatement) {
		BlockStatement block = new BlockStatement();
		for (Statement stmt : blockStatement.getStatements()) {
			Statement newStmt = stmt.accept(this);
			if (newStmt != null) {
				block.getStatements().add(newStmt);
			}
		}

		return block;
	}

	@Override
	public Statement visit(DoWhileStatement doWhileStatement) {
		DoWhileStatement stmt = new DoWhileStatement(doWhileStatement.getCondition());

		Statement loop = doWhileStatement.getLoop().accept(this);
		stmt.setLoop(loop == null ? doWhileStatement.getLoop() : loop);

		return stmt;
	}

	@Override
	public Statement visit(IfStatement ifStatement) {
		IfStatement newIf = new IfStatement(ifStatement.getCondition());

		Statement then = ifStatement.getThen().accept(this);
		newIf.setThen(then == null ? ifStatement.getThen() : then);

		if (ifStatement.getElze().isPresent()) {
			Statement elze = ifStatement.getElze().get().accept(this);

			newIf.setElze(elze == null ? ifStatement.getElze().get() : elze);
		}

		return newIf;
	}

	@Override
	public Statement visit(WhileStatement whileStatement) {
		WhileStatement stmt = new WhileStatement(whileStatement.getCondition());

		Statement loop = whileStatement.getLoop().accept(this);
		stmt.setLoop(loop == null ? whileStatement.getLoop() : loop);

		return stmt;
	}


	@Override
	public Statement visit(LabeledStatement labeledStatement) {
		if (this.usedLabels.contains(labeledStatement.getLabel()) || labeledStatement.getInnerStatement().isPresent()) {
			// Proceed as normal
			return super.visit(labeledStatement);
		}

		// Intentional - we will use 'null' to indicate this statement should be removed
		return null;
	}


}
