/*******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.support.codegen.program.statement;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class BlockStatement implements Statement {

	private final List<Statement> statements;

	public BlockStatement() {
		super();
		this.statements = new ArrayList<>();
	}

	public BlockStatement(List<Statement> statements) {
		super();
		this.statements = statements;
	}

	@Override
	public <T> T accept(StatementVisitor<T> visitor) {
		return visitor.visit(this);
	}

	public List<Statement> getStatements() {
		return this.statements;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Block(\n");
		sb.append(String.join("\n",
				this.statements.stream().map(s -> "    " + s.toString()).collect(Collectors.toList())));
		sb.append(")");

		return sb.toString();
	}

}
