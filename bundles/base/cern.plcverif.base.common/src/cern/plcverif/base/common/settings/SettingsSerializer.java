/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.common.settings;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.base.common.utils.OsUtils;
import cern.plcverif.base.common.utils.OsUtils.OperatingSystem;

public final class SettingsSerializer {
	private static final Logger LOGGER = LogManager.getLogger(SettingsSerializer.class);
	private static final String SETTINGS_FILE_EXT_WITH_DOT = ".settings";

	private SettingsSerializer() {
		// Utility class.
	}
	
	public static SettingsElement parseSettingsFromString(String input) throws SettingsParserException {
		if (input.contains("\n")) {
			String[] split = input.split("\n");
			for (int i = 0; i < split.length; ++i) {
				split[i] = split[i].trim();
				// Trim will remove the eventual leftover \r
			}
			return parseSettingsFromFile(new ArrayList<String>(Arrays.asList(split)));
		} else {
			return parseSettings(input);
		}
	}

	public static SettingsElement parseSettingsFromCommandArgs(String[] input) throws SettingsParserException {
		// Restore quotes
		for (int i = 0; i < input.length; ++i) {
			String argument = input[i];
			if (argument.contains(" ")) {
				int firstEqualsIdx = argument.indexOf('=');
				if (firstEqualsIdx < 0) {
					// no equals sign
					input[i] = "\"" + argument + "\"";
				} else {
					// equals sign found, quote the value part if needed
					String[] segments = argument.split("=", 2);
					Preconditions.checkState(segments.length == 2);
					boolean isQuoted = segments[1].startsWith("\"") && segments[1].endsWith("\"");
					boolean isBraced = segments[1].startsWith("{") && segments[1].endsWith("}");
					if (!isQuoted && !isBraced) {
						input[i] = String.format("%s=\"%s\"", segments[0], sanitize(segments[1]));
					}
				}
				
				if (DebugTracing.SETTINGS_PARSING_DEBUG_TRACING) {
					System.out.printf("SettingsSerializer.parseSettingsFromCommandArgs:: argument '%s' was modified to '%s'.%n", argument, input[i]);
				}
			}
		}
		return parseSettings(String.join(" ", input));
	}

	public static SettingsElement parseSettingsFromCommandArgs(List<String> input) throws SettingsParserException {
		return parseSettingsFromCommandArgs(input.toArray(new String[0]));
	}
	
	/**
	 * Parses the given textual settings description, assuming that it is in settings file format (one key-value pair per line)
	 * @param input Textual representation to parse
	 * @return The parsed settings element
	 * @throws SettingsParserException if it is not possible to parse the given settings file
	 */
	public static SettingsElement parseSettingsFromFile(String input) throws SettingsParserException {
		List<String> inputLineByLine = new ArrayList<>();
		for (String line : input.split("\n")) {
			if (line.endsWith("\r")) {
				inputLineByLine.add(withoutLastChar(line));
			} else {
			inputLineByLine.add(line);
			}
		}
		return parseSettingsFromFile(inputLineByLine);
	}

	private static String withoutLastChar(String line) {
		if (line == null || line.isEmpty()) {
			return line;
		} else {
			return line.substring(0, line.length() - 1);
		}
	}

	/**
	 * Parses the given textual settings description, assuming that it is in settings file format (one key-value pair per line)
	 * @param input Textual representation to parse, line by line (each element of the list is a line)
	 * @return The parsed settings element
	 * @throws SettingsParserException if it is not possible to parse the given settings file
	 */
	public static SettingsElement parseSettingsFromFile(List<String> input) throws SettingsParserException {
		// Remove eventual comments from settings file
		for (Iterator<String> i = input.iterator(); i.hasNext();) {
			String current = i.next();
			if (current.trim().startsWith("//")) {
				i.remove();
			}
		}

		for (int i = 0; i < input.size(); ++i) {
			int commentStart = findComment(input.get(i));
			if (commentStart >= 0) {
				input.set(i, input.get(i).substring(0, commentStart));
			}
		}

		return parseSettings(String.join(" ", input));
	}

	private static int findComment(String line) {
		boolean inQuotes = false;
		boolean firstSlashFound = false;
		for (int i = 0; i < line.length(); ++i) {
			if (line.charAt(i) == '"') {
				inQuotes = !inQuotes;
				firstSlashFound = false;
			} else if (line.charAt(i) == '/' && !inQuotes) {
				if (firstSlashFound) {
					return i - 1;
				} else {
					firstSlashFound = true;
				}
			} else {
				firstSlashFound = false;
			}
		}
		return -1;
	}

	private static SettingsElement parseSettings(String input) throws SettingsParserException {
		if (DebugTracing.SETTINGS_PARSING_DEBUG_TRACING) {
			System.out.printf("SettingsSerializer.parseparseSettings:: started on input '%s'.%n", input);
		}
		List<String> tokens = tokenizeInput(input);
		if (DebugTracing.SETTINGS_PARSING_DEBUG_TRACING) {
			System.out.printf("SettingsSerializer.parseSettings:: tokens created: %s.%n", String.join("; ", tokens));
		}
		Map<String, String> arguments = matchArguments(tokens);
		if (DebugTracing.SETTINGS_PARSING_DEBUG_TRACING) {
			System.out.printf("SettingsSerializer.parseSettings:: matchArguments returned: %s.%n", 
					String.join("; ", arguments.entrySet().stream().map(it -> String.format("%s=%s", it.getKey(), it.getValue())).collect(Collectors.toList())));
		}

		Map<String, Settings> registry = new HashMap<>();
		SettingsElement root = SettingsElement.empty();
		registry.put("", root);
		for (Entry<String, String> kvp : arguments.entrySet()) {
			String key = kvp.getKey();
			String value = kvp.getValue();

			String[] fqn = key.split("\\.");

			// Create elements
			String subName = "";
			for (int i = 0; i < fqn.length; ++i) {
				// Decide the type of elements - this can be done by checking
				// what follows: if it is a number, then it is a list
				Settings parent = registry.getOrDefault(subName, null);
				if (fqn[i].matches("\\d+")) {
					// parent is/should be a list
					if (parent == null) {
						Preconditions.checkState(i > 0);
						parent = SettingsList.empty();
						putIfNoValueYet(registry, subName, parent);
					} else if (parent.kind() != SettingsKind.LIST) {
						if (parent == root) {
							throw new SettingsParserException("The root element cannot be a list.");
						} else {
							throw new SettingsParserException(
									subName.substring(1) + " cannot be a list and have attributes at the same time.");
						}
					}
				} else {
					// parent is/should be a single object
					if (parent == null) {
						Preconditions.checkState(i > 0);
						parent = SettingsElement.createRootElement(fqn[i - 1]);
						putIfNoValueYet(registry, subName, parent);
					} else if (parent.kind() != SettingsKind.ELEMENT) {
						throw new SettingsParserException(
								subName.substring(1) + " cannot be a list and have attributes at the same time.");
					}
				}
				subName += "." + fqn[i];
			}

			// Create last element
			if (value == null) {
				// Last element is a switch with no value
				putIfNoValueYet(registry, subName, SettingsElement.createRootElement(fqn[fqn.length - 1]));
			} else {
				List<String> values = new ArrayList<>();
				boolean list = false;
				if (value.startsWith("{") && value.endsWith("}")) {
					value = value.substring(1, value.length() - 1);
					list = true;
					if (DebugTracing.SETTINGS_PARSING_DEBUG_TRACING) {
						System.out.println("SettingsSerializer.parseSettings:: list found");
					}
				}
				Matcher m = Pattern.compile("(\"[^\\\"]*\"|[^\"\\{\\},]+)\\s*(\\,|$)\\s*").matcher(value);
				while (m.find()) {
					if (DebugTracing.SETTINGS_PARSING_DEBUG_TRACING) {
						System.out.printf("SettingsSerializer.parseSettings:: match for value element. Full match: %s. Captured group: %s.%n", m.group(0), m.group(1));
					}
					values.add(desanitize(removeLeadingTrailingQuotationMarks(m.group(1))));
				}
				Settings element = registry.getOrDefault(subName, null);
				if (values.size() != 1 || list || (element != null && element.kind() == SettingsKind.LIST)) {
					// Last element is a list with value list
					if (element == null) {
						element = SettingsList.empty();
						putIfNoValueYet(registry, subName, element);
					}
					SettingsList listParent = element.toList();
					for (int i = 0; i < values.size(); ++i) {
						String fullName = subName + '.' + i;
						Settings child = registry.getOrDefault(fullName, null);
						if (child == null) {
							child = SettingsElement.createRootElement(Integer.toString(i));
							putIfNoValueYet(registry, fullName, child);
						}
						if (child.kind() != SettingsKind.ELEMENT) {
							throw new SettingsParserException(
									fullName.substring(1) + " cannot be a list and have a value at the same time.");
						}
						SettingsElement singleElement = (SettingsElement) child;
						if (singleElement.valueWithoutConsuming() != null) {
							throw new SettingsParserException("Multiple values defined for " + fullName.substring(1)
									+ ": \"" + singleElement.valueWithoutConsuming() + "\" and \"" + values.get(i));
						}
						singleElement.setValue(removeLeadingTrailingQuotationMarks(values.get(i)));
						listParent.set(i, singleElement);
					}
				} else {
					// Last element is a single value
					if (element == null) {
						element = SettingsElement.createRootElement(fqn[fqn.length - 1]);
						putIfNoValueYet(registry, subName, element);
					}
					if (element.kind() != SettingsKind.ELEMENT) {
						throw new SettingsParserException(
								subName.substring(1) + " cannot be a list and have a value at the same time.");
					}
					SettingsElement singleElement = (SettingsElement) element;
					if (singleElement.valueWithoutConsuming() != null) {
						throw new SettingsParserException("Multiple values defined for " + subName.substring(1) + ": \""
								+ singleElement.valueWithoutConsuming() + "\" and \"" + value);
					}
					singleElement.setValue(removeLeadingTrailingQuotationMarks(values.get(0)));
				}
			}

			// Link elements
			subName = "";
			for (int i = 0; i < fqn.length; ++i) {
				Settings parent = registry.get(subName);
				if (fqn[i].matches("\\d+")) {
					// parent is a list
					SettingsList listParent = (SettingsList) parent;
					int index = Integer.parseUnsignedInt(fqn[i]);
					Settings element = registry.get(subName + "." + fqn[i]);
					listParent.set(index, element);
				} else {
					SettingsElement singleParent = (SettingsElement) parent;
					Settings element = registry.get(subName + "." + fqn[i]);
					singleParent.setAttribute(fqn[i], element);
				}
				subName += "." + fqn[i];
			}
		}

		return root;
	}

	private static <K, V> void putIfNoValueYet(Map<K, V> map, K key, V newValue) throws SettingsParserException {
		if (map.containsKey(key) && map.get(key) != null) {
			throw new SettingsParserException(String.format("The key '%s' has been already parsed with value '%s' before value '%s'.", 
					key, map.get(key).toString(), newValue));
		} else {
			map.put(key, newValue);
			if (DebugTracing.SETTINGS_PARSING_DEBUG_TRACING) {
				System.out.printf("SettingsSerializer.putIfNoValueYet:: put key: '%s', value: '%s'.%n", key, newValue);
			}
		}
	}
	
	// Based on https://stackoverflow.com/a/7804472
	private static List<String> tokenizeInput(String input) {
		List<String> list = new ArrayList<>();
		// Looking for groups which can either be:
		// - a comma-separated list of the following items
		// -- anything but '"', '{' and '}': [^\"\\{\\}\\s]+
		// -- anything between quotes: \"[^\"]*\"
		// - anything between braces: \\{[^\\}]*\\}
		// - followed by at least one whitespace character or '=' (may be
		// combined), or the end of the string
		// all special characters escaped

		Matcher m = Pattern
				.compile(
						"("+
								"(\"[^\"]*\"|[^\"\\{\\}\\s=]+)" +
								"(\\,(\"[^\"]*\"|[^\"\\{\\}\\s=]+))*|"+
								"\\{[^\\}]*\\}" + 
						")" + 
						"([\\s=]+|$)")
				.matcher(input);
		while (m.find()) {
			list.add(m.group(1));
		}
		return list;
	}

	private static Map<String, String> matchArguments(List<String> tokens) throws SettingsParserException {
		Map<String, String> arguments = new HashMap<>();

		String lastKey = null;
		for (int i = 0; i < tokens.size(); i++) { // to emphasize that order is
													// important here
			String token = tokens.get(i);
			if (token.startsWith("-")) {
				lastKey = token.substring(1); // remove '-'
				putIfNoValueYet(arguments, lastKey, null);
			} else {
				// this is the value of the previous argument
				if (lastKey == null) {
					throw new SettingsParserException(String.format(
							"The following value is not preceded by an argument name: %s (missing '-' or missing file?). "
									+ "Already parsed keys: %s.",
							token, arguments.keySet().stream().collect(Collectors.joining(", "))));
				}
				putIfNoValueYet(arguments, lastKey, token);
				lastKey = null; // argument was set
			}
		}

		return arguments;
	}

	public static String serialize(SettingsElement root) {
		return serialize(root, false);
	}

	public static String serialize(SettingsElement root, boolean forCmndLine) {
		StringBuilder sb = new StringBuilder();

		String prefix = "-";

		List<String> keys = new ArrayList<>(root.getAttributeNames());
		Collections.sort(keys);

		for (String key : keys) {
			serialize(sb, root.getAttribute(key).orElseThrow(() -> new IllegalStateException("Internal inconsistency in settings element.")), prefix + key, forCmndLine);
		}

		return sb.toString();
	}

	private static void serialize(StringBuilder sb, Settings element, String prefix, boolean forCmndLine) {
		if (element.kind() == SettingsKind.ELEMENT) {
			SettingsElement single = (SettingsElement) element;
			if (single.valueWithoutConsuming() != null) {
				sb.append(prefix);
				if (forCmndLine) {
					sb.append("=");
				} else {
					sb.append(" = ");
				}
				
				String sanitizedValue = sanitize(single.valueWithoutConsuming());
				if (sanitizedValue.indexOf(' ') >= 0 || sanitizedValue.isEmpty()) {
					sb.append('"');
					sb.append(sanitizedValue);
					sb.append('"');
					
				} else {
					sb.append(sanitizedValue);
				}
				
				if (forCmndLine) {
					sb.append(" ");
				} else {
					sb.append(System.lineSeparator());
				}
				
			}

			List<String> keys = new ArrayList<>(single.getAttributeNames());
			Collections.sort(keys);

			for (String key : keys) {
				serialize(sb, single.getAttribute(key).orElseThrow(() -> new IllegalStateException("Internal inconsistency in settings element.")), prefix + '.' + key, forCmndLine);
			}
		} else {
			SettingsList list = (SettingsList) element;
			for (int i = 0; i < list.elements().size(); ++i) {
				if (list.elements().get(i) != null) {
					serialize(sb, list.elements().get(i), prefix + '.' + i, forCmndLine);
				}
			}
		}
	}

	/**
	 * Replaces the {@code ${OS}} or {@code $OS} placeholder(s) of the given
	 * string {@code str} with the constant. The list of supported operating
	 * systems can be found in {@link cern.plcverif.base.common.utils.OsUtils.OperatingSystem}.
	 */
	public static String replaceOsPlaceholder(String str) {
		OperatingSystem currentOs = OsUtils.getCurrentOs();
		if (currentOs == OperatingSystem.Unknown) {
			throw new PlcverifPlatformException(String.format("Unsupported OS: '%s'.", OsUtils.getCurrentOsText()));
		} else {
			return str.replace("${OS}", currentOs.getKey()).replace("$OS", currentOs.getKey());
		}
	}

	public static Settings loadOsSpecificDefaultSettings(String resourceFolder, String rootKey, ClassLoader classLoader)
			throws SettingsParserException {
		String settingString = IoUtils.readResourceFile(
				SettingsSerializer.replaceOsPlaceholder(resourceFolder + "/default.${OS}" + SETTINGS_FILE_EXT_WITH_DOT),
				classLoader);
		Settings ret = SettingsSerializer.parseSettingsFromString(settingString);
		ret.toSingle().setValue(rootKey);
		return ret;
	}

	public static Settings loadDefaultSettings(String resourceFolder, String rootKey, ClassLoader classLoader)
			throws SettingsParserException {
		String settingString = IoUtils.readResourceFile(resourceFolder + "/default" + SETTINGS_FILE_EXT_WITH_DOT,
				classLoader);
		Settings ret = SettingsSerializer.parseSettingsFromString(settingString);
		ret.toSingle().setValue(rootKey);
		return ret;
	}

	/**
	 * Loads the installation-specific settings for the given plug-in, from the
	 * given directory.
	 * <p>
	 * If there is no installation-specific settings for the given plug-in, it
	 * returns {@link Optional#empty()}.
	 * 
	 * @param pluginKey
	 *            The ID of the plug-in of which settings to be loaded.
	 * @param settingsDirectory
	 *            The directory of settings files.
	 * @return The installation-specific settings for the given plug-in, if
	 *         there was any.
	 * @throws SettingsParserException
	 */
	public static Optional<SettingsElement> loadInstallationPluginSettings(String pluginKey, Path settingsDirectory)
			throws SettingsParserException {
		Preconditions.checkNotNull(pluginKey);
		Preconditions.checkNotNull(settingsDirectory);

		Path settingsPath = settingsDirectory.resolve(Paths.get(pluginKey + SETTINGS_FILE_EXT_WITH_DOT))
				.toAbsolutePath();
		try {
			String settingString = IoUtils.readFile(settingsPath);
			SettingsElement ret = SettingsSerializer.parseSettingsFromString(settingString).toSingle();
			LOGGER.debug(String.format("Installation-specific settings file loaded for plugin '%s' from %s.", pluginKey,
					settingsPath.toString()));

			Preconditions.checkNotNull(ret);
			return Optional.of(ret);
		} catch (IOException e) {
			LOGGER.debug(String.format(
					"Installation-specific settings file not found for plugin '%s' at %s. This is not a problem, the presence of this file is optional.",
					pluginKey, settingsPath.toString()));

			return Optional.empty();
		}
	}

	/**
	 * Loads the installation-specific settings for the given plug-in from the
	 * settings folder, then overwrites it with the given
	 * {@code specificSettings}. If no settings file is found for the given
	 * plug-in, the parameter {@code specificSettings} is returned without any
	 * modification.
	 * <p>
	 * It can be used to load installation-specific default settings, such as
	 * location of backends.
	 * 
	 * @param specificSettings
	 *            The settings for the current job.
	 * @param pluginKey
	 *            ID of the plug-in.
	 * @param settingsDirectory
	 *            Location of the installation-specific default settings.
	 * @return The combination of the installation-specific and current
	 *         settings, where the latter has priority.
	 * @throws SettingsParserException
	 *             if there is an installation-specific settings file, but it
	 *             cannot be parsed.
	 */
	public static SettingsElement extendWithInstallationSettings(SettingsElement specificSettings, String pluginKey,
			Path settingsDirectory) throws SettingsParserException {
		Preconditions.checkNotNull(specificSettings);
		Preconditions.checkNotNull(pluginKey);
		Preconditions.checkNotNull(settingsDirectory);

		Optional<SettingsElement> fileSettings = loadInstallationPluginSettings(pluginKey, settingsDirectory);
		if (fileSettings.isPresent()) {
			SettingsElement result = fileSettings.get();
			result.overrideWith(specificSettings);
			return result;
		} else {
			return specificSettings;
		}
	}
	
	/**
	 * If the given string starts and sends with quotation marks, they are both removed, 
	 * otherwise the returned string is the same as the input.
	 * @param str Value string to be modified.  Shall not be null.
	 * @return Value string without leading and trailing quotation marks.
	 */
	public static String removeLeadingTrailingQuotationMarks(String str) {
		Preconditions.checkNotNull(str);
		
		if (str.startsWith("\"") && str.endsWith("\"") && str.length() >= 2) {
			return str.substring(1, str.length() - 1);
		} else {
			return str;
		}
	}
	
	/**
	 * Sanitizes the string to be used as value.
	 * <ul>
	 * 	<li>Replaces the quotation marks ({@code "}) within values by {@code ''},
	 * 	<li>Replaces the line breaks ({@code \r}, {@code \n}) by {@code &#x0D;}, {@code &#x0A;}.
	 * </ul>
	 * @param unsanitizedStr Value string to be sanitized. Shall not be {@code null}.
	 * @return Sanitized string. Never {@code null}.
	 */
	public static String sanitize(String unsanitizedStr) {
		Preconditions.checkNotNull(unsanitizedStr);
		
		return unsanitizedStr
			.replace("\"", "''")
			.replace("\r", "&#x0D;")
			.replace("\n", "&#x0A;");
	}
	
	/**
	 * Desanitizes the string to be used as value.
	 * <ul>
	 * 	<li>Replaces the sanitized quotation marks ({@code &quot;} or {@code ''}) within values by real quotation marks ({@code "}),
	 *  <li>Replaces the sanitized line breaks ({@code &#x0D;}, {@code &#x0A;}) by {@code \r}, {@code \n}.
	 * </ul>
	 * @param sanitizedStr Value string to be desanitized.  Shall not be null.
	 * @return Desanitized string.
	 */
	public static String desanitize(String sanitizedStr) {
		Preconditions.checkNotNull(sanitizedStr);
		
		return sanitizedStr
			.replace("&quot;", "\"")
			.replace("''", "\"")
			.replace("&#x0D;", "\r")
			.replace("&#x0A;", "\n");
	}
}
