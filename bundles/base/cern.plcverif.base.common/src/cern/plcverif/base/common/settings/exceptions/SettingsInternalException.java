/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.settings.exceptions;

public class SettingsInternalException extends RuntimeException {
	private static final long serialVersionUID = 5307363644455302383L;

	public SettingsInternalException() {
		super("Internal error occurred during serializing the configuration and the operation could not finish.");
	}

	public SettingsInternalException(String message, Object... params) {
		super(String.format(message, params));
	}

	public SettingsInternalException(Throwable cause, String message, Object... params) {
		super(String.format(message, params), cause);
	}
}
