/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.progress;

/**
 * Interface which permits to emit information about the progress of the current
 * job, including the name of the current task and the overall progress.
 */
public interface IProgressReporter {
	/**
	 * Method to report the name of the current task.
	 * 
	 * @param currentTask
	 *            Name of the current task
	 */
	void reportTask(String currentTask);

	/**
	 * Method to report the overall progress of the job in percentage. The value
	 * is for information purposes only and is only an estimation. However, it
	 * is expected to monotonically increase.
	 * 
	 * @param percentage
	 *            Percentage of the current job that is ready
	 */
	void reportProgress(int percentage);

	/**
	 * Method to report the name of the current task and the overall progress.
	 * 
	 * The result of this method is expected to be identical to the sequential
	 * calls of {@link #reportTask(String)} and {@link #reportProgress(int)}.
	 * 
	 * @param currentTask
	 *            Name of the current task
	 * @param percentage
	 *            Percentage of the current job that is ready
	 */
	void report(String currentTask, int percentage);
}
