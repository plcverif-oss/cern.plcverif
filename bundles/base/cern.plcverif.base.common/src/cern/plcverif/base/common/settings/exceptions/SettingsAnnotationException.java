/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.settings.exceptions;

/**
 * Represents a major issue with the usage of PlcverifSettings annotations that cannot be fixed in run time. 
 */
public class SettingsAnnotationException extends RuntimeException {
	private static final long serialVersionUID = 8676270118694118194L;

	public SettingsAnnotationException() {
		super("The annotations used for specific settings parsing are invalid.");
	}

	public SettingsAnnotationException(String message, Object... params) {
		super(String.format(message, params));
	}

	public SettingsAnnotationException(Throwable cause, String message, Object... params) {
		super(String.format(message, params), cause);
	}
}
