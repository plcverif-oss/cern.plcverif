/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.settings.help;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Stack;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory;

/**
 * Class to represent the accepted settings and their meaning in the current
 * configuration.
 */
public final class SettingsHelp {
	/**
	 * Describes a single field.
	 */
	public static class Field {
		private final String plugin;
		private final String attributeFqn;
		private final String description;
		private final Collection<String> permittedValues;

		Field(String plugin, String attributeFqn, String description, Collection<String> permittedValues) {
			super();
			this.plugin = plugin == null ? "" : plugin;
			this.attributeFqn = attributeFqn == null ? "" : attributeFqn;
			this.description = description == null ? "" : description;
			this.permittedValues = permittedValues == null ? Collections.emptyList()
					: Collections.unmodifiableCollection(permittedValues);
		}

		/**
		 * Returns the plug-in which this field belongs to.
		 * 
		 * @return Name of the plug-in. May be empty string if it is a built-in
		 *         PLCverif field. Never {@code null}.
		 */
		public String getPlugin() {
			return plugin;
		}

		/**
		 * Returns the FQN of the attribute.
		 * 
		 * @return FQN of the attribute. Never {@code null}.
		 */
		public String getAttributeFqn() {
			return attributeFqn;
		}

		/**
		 * Returns the human-readable description of the attribute.
		 * 
		 * @return Description of the attribute. Never {@code null}.
		 */
		public String getDescription() {
			return description;
		}

		/**
		 * Returns the permitted values of this attribute if available.
		 * 
		 * @return Unmodifiable, non-{@code null} collection of permitted
		 *         values, if applicable.
		 */
		public Collection<String> getPermittedValues() {
			return permittedValues;
		}
	}

	/**
	 * Writer to be used to print the help.
	 */
	private SettingsHelpWriter writer;

	/**
	 * Stack of prefixes. Each prefix should end with '.'. Each entry is a full
	 * prefix, i.e. the storage is redundant. For example, the following is a
	 * valid stack content, from bottom to top: {@code [a., a.b., a.b.c.]}.
	 */
	private final Stack<String> prefixStack = new Stack<>();

	/**
	 * Stored field descriptions.
	 */
	private final List<Field> fields = new ArrayList<>();

	/**
	 * ID of the plug-in that is currently described.
	 */
	private String lastPlugin = null;

	/**
	 * Creates a new settings help representation object. The textual
	 * representation will be created by using the given writer. It can also be
	 * accessed via {@link #getFields()}.
	 * 
	 * @param writer
	 *            Writer to be used for the textual representation.
	 */
	public SettingsHelp(SettingsHelpWriter writer) {
		this.writer = writer;
		printHeader();
	}

	/**
	 * Creates a new settings help representation object. The textual
	 * representation can be accessed via {@link #getFields()}.
	 */
	public SettingsHelp() {
		this.writer = null;
	}

	private void printHeader() {
		if (this.writer != null) {
			writer.printHeader();
		}
	}

	/**
	 * Starts a section where each new setting will have the given prefix
	 * element. This section ends at the {@link #endPrefix()} call. The prefix
	 * elements are stored in a stack and the concatenation of all non-ended
	 * prefixes will be used to prefix the setting elements to be defined.
	 * 
	 * @param prefixElement
	 *            New prefix element. Should not start or end with '.'.
	 */
	public void startPrefix(String prefixElement) {
		prefixStack.push(fullPrefix(prefixElement));
	}

	/**
	 * Starts a section where each new setting will have the given prefix
	 * element. The prefix element designates a settings list node. This section
	 * ends at the {@link #endPrefix()} call. The prefix elements are stored in
	 * a stack and the concatenation of all non-ended prefixes will be used to
	 * prefix the setting elements to be defined.
	 * 
	 * @param prefixElement
	 *            New prefix element. Should not start or end with '.'.
	 */
	public void startPrefixList(String prefixElement) {
		prefixStack.push(fullPrefix(prefixElement + ".<ID>"));
	}

	/**
	 * Includes the definition of an extension in the settings help.
	 * 
	 * @param commandId
	 *            ID of the extension (command ID).
	 * @param pluginName
	 *            Human-readable name of the plug-in.
	 * @param pluginDescription
	 *            Description of the plug-in. Typically it is one paragraph of
	 *            text describing the goal of the plug-in.
	 *            <p>
	 *            Example: <i>Generates a HTML representation of the
	 *            verification results, including the metadata, configuration
	 *            and the eventual counterexample.</i>
	 * @param clazz
	 *            Class that represents the extension.
	 */
	public void addPluginDetails(String commandId, String pluginName, String pluginDescription, Class<?> clazz) {
		printPluginDetailsHeader(commandId, pluginName, pluginDescription);

		lastPlugin = commandId;
	}

	private void printPluginDetailsHeader(String extensionId, String extensionName, String extensionDescription) {
		if (this.writer != null) {
			Optional<String> settingsFqn;
			if (prefixStack.isEmpty()) {
				settingsFqn = Optional.empty();
			} else {
				settingsFqn = Optional.of(removeTrailingDot(prefixStack.peek()));
			}

			writer.printPluginDetailsHeader(settingsFqn, extensionId, extensionName, extensionDescription);
		}
	}

	private static String removeTrailingDot(String str) {
		if (str == null) {
			return str;
		}

		return str.replaceAll("\\.$", "");
	}

	/**
	 * Declares a new setting item in the settings help.
	 * <p>
	 * The set of permitted values and the default value will not be defined for
	 * this setting item.
	 * 
	 * @param settingName
	 *            Name of the setting item.
	 * @param description
	 *            Description of the setting item.
	 * @param type
	 *            Type of the setting item.
	 * @param mandatory
	 *            Whether the setting item is mandatory to be provided.
	 */
	public void addSettingItem(String settingName, String description, Class<?> type,
			PlcverifSettingsMandatory mandatory) {
		addSettingElement(settingName, description, type, mandatory, null, null);
	}

	/**
	 * Declares a new setting item in the settings help.
	 * <p>
	 * The default value will not be defined for this setting item.
	 * 
	 * @param settingName
	 *            Name of the setting item.
	 * @param description
	 *            Description of the setting item.
	 * @param type
	 *            Type of the setting item.
	 * @param mandatory
	 *            Whether the setting item is mandatory to be provided.
	 * @param permittedValues
	 *            Explicit definition of the values permitted for this setting
	 *            element.
	 */
	public void addSettingItem(String settingName, String description, Class<?> type,
			PlcverifSettingsMandatory mandatory, Collection<String> permittedValues) {
		addSettingElement(settingName, description, type, mandatory, permittedValues, null);
	}

	/**
	 * Declares a new setting item in the settings help.
	 * 
	 * @param settingName
	 *            Name of the setting item.
	 * @param description
	 *            Description of the setting item.
	 * @param type
	 *            Type of the setting item.
	 * @param mandatory
	 *            Whether the setting item is mandatory to be provided. The
	 *            setting element will be treated as optional even if this
	 *            setting is {@link PlcverifSettingsMandatory#MANDATORY}, if
	 *            there is a non-null default value provided for this setting
	 *            element.
	 * @param permittedValues
	 *            Explicit definition of the values permitted for this setting
	 *            item. May be null, then the set of permitted values will not
	 *            be defined for this setting item.
	 * @param defaultValue
	 *            Default value for this setting item. May be null, then the
	 *            default value will not be defined for this setting element.
	 */
	public void addSettingElement(String settingName, String description, Class<?> type,
			PlcverifSettingsMandatory mandatory, Collection<String> permittedValues, String defaultValue) {
		Preconditions.checkNotNull(settingName);
		Preconditions.checkNotNull(description);
		Preconditions.checkNotNull(type);
		Preconditions.checkNotNull(mandatory);

		String prefix = prefixStack.isEmpty() ? "" : prefixStack.peek();
		String typeStr = type.isEnum() ? "String" : type.getSimpleName();
		if ("boolean".equalsIgnoreCase(typeStr)) {
			typeStr = "Boolean";
		}
		String optionalStr = mandatory == PlcverifSettingsMandatory.OPTIONAL || defaultValue != null ? ", optional"
				: "";
		String permittedValuesString = permittedValues == null ? ""
				: String.format("%n        Permitted values: %s (total: %s)", String.join(", ", permittedValues),
						permittedValues.size());
		String defaultValueString = defaultValue == null ? ""
				: String.format("%n        Default value: '%s'", defaultValue);

		printSettingsItem(prefix, settingName, typeStr, description, mandatory, permittedValues, defaultValue);

		String fullDescription = String.format("%s%s%s (%s%s)", description, permittedValuesString, defaultValueString,
				typeStr, optionalStr);

		fields.add(new Field(lastPlugin, prefix + settingName, fullDescription, permittedValues));
	}

	private void printSettingsItem(String prefix, String paramName, String typeStr, String description,
			PlcverifSettingsMandatory mandatory, Collection<String> permittedValues, String defaultValue) {
		if (writer != null) {
			writer.printSettingsItem(Optional.of(prefix), paramName, typeStr, description, mandatory,
					Optional.ofNullable(permittedValues), Optional.ofNullable(defaultValue));
		}
	}

	/**
	 * Ends the scope of the last-defined prefix.
	 * 
	 * @see #startPrefix(String)
	 */
	public void endPrefix() {
		prefixStack.pop();
	}

	public List<Field> getFields() {
		return Collections.unmodifiableList(fields);
	}

	/**
	 * Returns the full prefix composed by the prefixes in the
	 * {@link #prefixStack} and the given {@code pluginName}, correctly
	 * separated by dots.
	 */
	private String fullPrefix(String pluginName) {
		String previousPrefix = prefixStack.isEmpty() ? "" : prefixStack.peek();
		String toAppend = pluginName == null ? "" : pluginName;

		Preconditions.checkNotNull(previousPrefix);
		Preconditions.checkNotNull(toAppend);

		if (previousPrefix.isEmpty() && toAppend.isEmpty()) {
			return "";
		} else {
			return previousPrefix + toAppend + ".";
		}
	}
}
