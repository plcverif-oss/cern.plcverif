/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.emf;

import java.util.Map;

import org.eclipse.emf.ecore.EObject;

/**
 * Interface for maps with dedicated support for {@link EObject} keys. This is
 * needed as the EMF nodes' equals method are based on reference comparison.
 * Implementors of this interface are expected to use value-based comparison for
 * the {@link EObject} keys.
 */
public interface IEObjectMap<K extends EObject, V> extends Map<K, V> {
}
