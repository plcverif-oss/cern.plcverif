/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.emf;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;

import com.google.common.base.Preconditions;

/**
 * Class to efficiently delete a collection of {@link EObject}s. It permits to first
 * register elements for deletion, then delete them together. This helps to avoid 
 * repeatedly explore the crossreferences between the objects, thus speed up the
 * deletion.
 * 
 * Can be subclassed to provide more specific deletion methods for certain EObject subtypes.
 */
public class CollectionDeleter {
	protected Set<EObject> objectsToDelete = new HashSet<>();
	
	public void registerForDeletion(EObject objectToDelete) {
		Preconditions.checkState(objectsToDelete != null, "CollectionDeleter cannot be reused.");
		
		objectsToDelete.add(objectToDelete);
	}
	
	public final void registerForDeletion(Collection<? extends EObject> objectsToDelete) {
		for (EObject e : objectsToDelete) {
			registerForDeletion(e);
		}
	}
	
	public void deleteAll() {
		Preconditions.checkState(objectsToDelete != null, "CollectionDeleter cannot be reused.");
		
		EmfHelper.deleteAll(objectsToDelete);
		objectsToDelete = null;
	}
	
	public boolean isMarkedForDeletion(EObject e) {
		Preconditions.checkState(objectsToDelete != null, "CollectionDeleter cannot be reused.");
		return objectsToDelete.contains(e);
	}
	
	public boolean isEmpty() {
		return objectsToDelete.isEmpty();
	}
	
	public final void cancel() {
		objectsToDelete = null;
	}
}
