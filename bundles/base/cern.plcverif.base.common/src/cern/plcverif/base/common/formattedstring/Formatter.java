/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.formattedstring;

import cern.plcverif.base.common.formattedstring.Formats.BoldFormat;
import cern.plcverif.base.common.formattedstring.Formats.ColorFormat;
import cern.plcverif.base.common.formattedstring.Formats.ItalicFormat;

/**
 * Formatter to be used with {@link BasicFormattedString}s. 
 */
public abstract class Formatter {
	/**
	 * Returns the prefix required to precede a text formatted in the given bold format.
	 * @param f Format to represent
	 * @return Prefix
	 */
	public abstract String formatPrefix(BoldFormat f);
	
	/**
	 * Returns the suffix required to follow a text formatted in the given bold format.
	 * @param f Format to represent
	 * @return Suffix
	 */
	public abstract String formatSuffix(BoldFormat f);
	
	/**
	 * Returns the prefix required to precede a text formatted in the given italic format.
	 * @param f Format to represent
	 * @return Prefix
	 */
	public abstract String formatPrefix(ItalicFormat f);
	
	/**
	 * Returns the suffix required to follow a text formatted in the given italic format.
	 * @param f Format to represent
	 * @return Suffix
	 */
	public abstract String formatSuffix(ItalicFormat f);
	
	/**
	 * Returns the prefix required to precede a text formatted in the given color format.
	 * @param f Format to represent
	 * @return Prefix
	 */
	public abstract String formatPrefix(ColorFormat f);
	
	/**
	 * Returns the suffix required to follow a text formatted in the given color format.
	 * @param f Format to represent
	 * @return Suffix
	 */
	public abstract String formatSuffix(ColorFormat f);
	
	/**
	 * Sanitizes the given text. The given string does not contain any formatting prefix or suffix.
	 * 
	 * This method can be used to escape the characters with special meaning, or to replace new line characters.
	 * @param string String to represent
	 * @return Escaped string
	 */
	public abstract String sanitizeText(String string);
	
	/**
	 * Returns the prefix required to precede the full formatted string.
	 * @return Prefix
	 */
	public abstract String stringPrefix();
	
	/**
	 * Returns the suffix required to follow the full formatted string.
	 * @return Suffix
	 */
	public abstract String stringSuffix();
}
