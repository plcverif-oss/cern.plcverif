/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
 package cern.plcverif.base.common.emf

import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.emf.ecore.EAttribute
import java.util.List
import java.util.Arrays
import com.google.common.base.Preconditions

/**
 * Implementation of hashCode for EObjects that is consistent with 
 * the {@link org.eclipse.emf.ecore.util.EcoreUtil#equals} method.
 */
final class EObjectHash {
	private new() {
		// Utility class.
	}

	def static dispatch int hashCode(Object e) {
		Preconditions.checkArgument(e instanceof EObject == false);
		return e.hashCode();
	}

	def static dispatch int hashCode(Void e) {
		return 0;
	}

	def static dispatch int hashCode(EObject e) {
		var result = 1;

		// Hash based on the attributes
		for (EAttribute attribute : e.eClass().getEAllAttributes()) {
			result = hash(result, hashCode(e.eGet(attribute)));
		}

		// Hash based on the containments
		for (EReference containment : e.eClass().getEAllContainments()) {
			val containedElement = e.eGet(containment);
			result = hash(result, hashCode(containedElement));
		}

		// References are omitted on purpose.
		return result;
	}

	def static dispatch int hashCode(List<?> e) {
		var result = 1;
		for (it : e) {
			result = hash(result, hashCode(it));
		}
		return result;
	}

	/**
	 * Implementation based on {@link Arrays#hashCode}.
	 */
	private def static int hash(int previousResult, int nextHash) {
		return 31 * previousResult + nextHash;
	}
}
