/*******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Vince Molnar - add checkNotNull message support
 *******************************************************************************/

package cern.plcverif.base.common.emf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.EcoreUtil.UsageCrossReferencer;

import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;

/**
 * Utility methods to manipulate EMF models.
 */
public final class EmfHelper {
	private EmfHelper() {
		// Utility class.
	}

	/**
	 * Throws an exception if the given {@link EObject e} is already contained
	 * by an object.
	 *
	 * @param e
	 *            The {@link EObject} to be checked.
	 * @throws UnsupportedOperationException
	 *             if the given {@link EObject} has already a container
	 * @throws IllegalArgumentException
	 *             if the given argument is null
	 */
	public static void checkNotContained(EObject e) {
		checkNotContained(e, "The given EObject must not be contained by any other expression node.");
	}

	/**
	 * Throws an exception if the given {@link EObject e} is already contained
	 * by an object.
	 *
	 * @param e
	 *            The {@link EObject} to be checked.
	 * @param message
	 *            The message used to construct the exception.
	 * @throws UnsupportedOperationException
	 *             if the given {@link EObject} has already a container
	 * @throws IllegalArgumentException
	 *             if the given argument is null
	 */
	public static void checkNotContained(EObject e, String message) {
		if (e == null) {
			throw new IllegalArgumentException("checkNotContained is not valid for null parameters.");
		}
		if (e.eContainer() != null) {
			throw new UnsupportedOperationException(message);
		}
	}

	/**
	 * Returns the closest {@link EObject#eContainer() container} of the given
	 * object having the given type {@code T}. If no such object is found, it
	 * returns {@code null}. If {@code e} is already of type {@code T}, it DOES
	 * NOT return {@code e}.
	 */
	@SuppressWarnings("unchecked")
	public static <T extends EObject> T getContainerOfType(EObject e, Class<T> clazz) {
		Preconditions.checkNotNull(e, "The given EObject is null.");

		EObject current = e.eContainer();
		while (current != null) {
			if (clazz.isInstance(current)) {
				return (T) current;
			}
			current = current.eContainer();
		}
		return null;
	}

	/**
	 * Returns true iff the given object {@code e} has any container that is an
	 * instance of any of the given {@code classes}.
	 * <p>
	 * Just the object {@code e} being of any of the given types will not make
	 * the result true.
	 */
	@SuppressWarnings("unchecked")
	public static boolean isContainedInAny(EObject e, Class<? extends EObject>... classes) {
		Preconditions.checkNotNull(e, "The given EObject is null.");

		EObject current = e.eContainer();
		while (current != null) {
			for (Class<? extends EObject> c : classes) {
				if (c.isInstance(current)) {
					return true;
				}
			}
			current = current.eContainer();
		}
		return false;
	}

	/**
	 * Returns the last ancestor of {@code e} before an ancestor with type
	 * {@code clazz}. Returns null, if there is no ancestor with type
	 * {@code clazz}.
	 */
	@SuppressWarnings("unchecked")
	public static <T extends EObject> T getLastAncestorBeforeContainerOfType(EObject e, Class<T> clazz) {
		EObject current = e;
		while (current != null && current.eContainer() != null) {
			if (clazz.isInstance(current.eContainer())) {
				return (T) current;
			}
			current = current.eContainer();
		}
		return null;
	}

	/**
	 * Returns the farthest (topmost, closest to root) ancestor of {@code e}
	 * that is of type {@code clazz}. For example, it can find the root of an
	 * expression subtree within an EMF model.
	 */
	@SuppressWarnings("unchecked")
	public static <T extends EObject> T getFarthestContinousAncestorOfType(T e, Class<T> clazz) {
		Preconditions.checkNotNull(e);
		EObject current = e;

		while (current != null && current.eContainer() != null && clazz.isInstance(current.eContainer())) {
			current = current.eContainer();
		}

		if (current == null) {
			// this should never happen
			Preconditions.checkState(false);
			return null;
		} else {
			return (T) current;
		}
	}

	/**
	 * Determines the number of occurrences of the given type {@code ofType} in
	 * the container hierarchy of {@code obj}.
	 */
	public static <T extends EObject> int countContainersOfTypeRecursively(EObject obj, Class<T> ofType) {
		return countContainersOfTypeRecursively(obj, ofType, (i -> true));
	}

	/**
	 * Determines the number of occurrences of the given type {@code ofType},
	 * satisfying the given {@code predicate}, in the container hierarchy of
	 * {@code obj}.
	 */
	@SuppressWarnings("unchecked")
	public static <T extends EObject> int countContainersOfTypeRecursively(EObject obj, Class<T> ofType,
			Predicate<T> predicate) {
		int count = 0;

		EObject current = obj.eContainer();
		while (current != null) {
			if (ofType.isInstance(current) && predicate.test((T) current)) {
				count++;
			}
			current = current.eContainer();
		}

		return count;
	}

	/**
	 * Determines the number of container nodes satisfying the given
	 * {@code predicate} in the container hierarchy of {@code obj}.
	 */
	public static int countContainersOfTypeRecursively(EObject obj, Predicate<EObject> predicate) {
		int count = 0;

		EObject current = obj.eContainer();
		while (current != null) {
			if (predicate.test(current)) {
				count++;
			}
			current = current.eContainer();
		}

		return count;
	}

	/**
	 * Returns an iterable for the given tree iterator. This permits to use the
	 * {@link TreeIterator} returned by {@link EObject#eAllContents()} in a
	 * {@code for} loop conveniently.
	 * 
	 * @param allContentsIterator
	 *            Tree iterator.
	 * @return Iterable.
	 */
	public static Iterable<EObject> toIterable(final TreeIterator<EObject> allContentsIterator) {
		Preconditions.checkNotNull(allContentsIterator);
		return new Iterable<EObject>() {
			public Iterator<EObject> iterator() {
				return allContentsIterator;
			}
		};
	}

	/**
	 * Returns a list of all objects with the given type in the containment
	 * subtree of the given root object's subtree.
	 * 
	 * The method will return a subset of all descendants, not only the
	 * immediate children. Depending on the {@code includeRoot} parameter, the
	 * subtree that is searched may or may not include the root object itself.
	 * If the given root object's type is not {@code clazz}, the value of the
	 * {@code includeRoot} parameter is indifferent.
	 * 
	 * If the result is only to be used in a for-each loop, the
	 * {@link #getAllContentsOfTypeIterable(EObject, Class, boolean)} method may
	 * be more suitable.
	 * 
	 * @see #getAllContentsOfTypeIterable(EObject, Class, boolean)
	 * @see #getAllContentsOfTypeStream(EObject, Class, boolean)
	 * 
	 * @param rootObject
	 *            The root object. The results will be returned from the
	 *            subtree.
	 * @param clazz
	 *            The type of the objects to be returned.
	 * @param includeRoot
	 *            If true and the root object has type {@code clazz}, it will be
	 *            included in the result. Otherwise, the root object will not be
	 *            in the result list.
	 * @return List of contents with the given type.
	 */
	public static <T extends EObject> List<T> getAllContentsOfType(EObject rootObject, Class<T> clazz,
			boolean includeRoot) {
		List<T> ret = new ArrayList<>();
		if (includeRoot && clazz.isInstance(rootObject)) {
			ret.add(clazz.cast(rootObject));
		}

		for (EObject e : toIterable(rootObject.eAllContents())) {
			if (clazz.isInstance(e)) {
				ret.add(clazz.cast(e));
			}
		}

		return ret;
	}

	/**
	 * Returns a list of all objects with the given type in the containment
	 * subtree of the given root object's subtree which satisfy the given
	 * predicate.
	 * 
	 * The method will return a subset of all descendants, not only the
	 * immediate children. Depending on the {@code includeRoot} parameter, the
	 * subtree that is searched may or may not include the root object itself.
	 * If the given root object's type is not {@code clazz}, the value of the
	 * {@code includeRoot} parameter is indifferent.
	 * 
	 * If the result is only to be used in a for-each loop, the
	 * {@link #getAllContentsOfTypeIterable(EObject, Class, boolean, Predicate)}
	 * method may be more suitable.
	 * 
	 * @see #getAllContentsOfTypeIterable(EObject, Class, boolean, Predicate)
	 * @see #getAllContentsOfTypeStream(EObject, Class, boolean, Predicate)
	 * 
	 * @param rootObject
	 *            The root object. The results will be returned from the
	 *            subtree.
	 * @param clazz
	 *            The type of the objects to be returned.
	 * @param includeRoot
	 *            If true and the root object has type {@code clazz}, it will be
	 *            included in the result. Otherwise, the root object will not be
	 *            in the result list.
	 * @param predicate
	 *            Predicate that must be satisfied for all returned content
	 *            elements.
	 * @return List of contents with the given type satisfying the given
	 *         predicate.
	 */
	public static <T extends EObject> List<T> getAllContentsOfType(EObject rootObject, Class<T> clazz,
			boolean includeRoot, Predicate<T> predicate) {
		List<T> ret = new ArrayList<>();
		if (includeRoot && clazz.isInstance(rootObject)) {
			T candidate = clazz.cast(rootObject);
			if (predicate.test(candidate)) {
				ret.add(candidate);
			}
		}

		for (EObject e : toIterable(rootObject.eAllContents())) {
			if (clazz.isInstance(e)) {
				T candidate = clazz.cast(e);
				if (predicate.test(candidate)) {
					ret.add(candidate);
				}
			}
		}

		return ret;
	}

	/**
	 * Returns an iterable of all objects with the given type in the containment
	 * subtree of the given root object's subtree. No new list will be created
	 * by this method, thus its memory footprint is smaller than of
	 * {@link #getAllContentsOfType(EObject, Class, boolean)}}.
	 * 
	 * The method will return a subset of all descendants, not only the
	 * immediate children. Depending on the {@code includeRoot} parameter, the
	 * subtree that is searched may or may not include the root object itself.
	 * If the given root object's type is not {@code clazz}, the value of the
	 * {@code includeRoot} parameter is indifferent.
	 * 
	 * @see #getAllContentsOfType(EObject, Class, boolean)
	 * @see #getAllContentsOfTypeStream(EObject, Class, boolean)
	 * 
	 * @param rootObject
	 *            The root object. The results will be returned from the
	 *            subtree.
	 * @param clazz
	 *            The type of the objects to be returned.
	 * @param includeRoot
	 *            If true and the root object has type {@code clazz}, it will be
	 *            included in the result. Otherwise, the root object will not be
	 *            in the result list.
	 * @return Iterable contents with the given type.
	 */
	public static <T extends EObject> Iterable<T> getAllContentsOfTypeIterable(EObject rootObject, Class<T> clazz,
			boolean includeRoot) {
		Iterable<T> childrenIterable = Iterables.filter(toIterable(rootObject.eAllContents()), clazz);

		if (includeRoot && clazz.isInstance(rootObject)) {
			// root node is needed too
			T root = clazz.cast(rootObject);
			return Iterables.concat(Collections.singletonList(root), childrenIterable);
		} else {
			return childrenIterable;
		}
	}

	/**
	 * Returns an iterable of all objects with the given type in the containment
	 * subtree of the given root object's subtree which satisfy the given
	 * predicate. No new list will be created by this method, thus its memory
	 * footprint is smaller than of
	 * {@link #getAllContentsOfType(EObject, Class, boolean, Predicate)}}.
	 * 
	 * The method will return a subset of all descendants, not only the
	 * immediate children. Depending on the {@code includeRoot} parameter, the
	 * subtree that is searched may or may not include the root object itself.
	 * If the given root object's type is not {@code clazz}, the value of the
	 * {@code includeRoot} parameter is indifferent.
	 * 
	 * @see #getAllContentsOfType(EObject, Class, boolean, Predicate)
	 * @see #getAllContentsOfTypeStream(EObject, Class, boolean, Predicate)
	 * 
	 * @param rootObject
	 *            The root object. The results will be returned from the
	 *            subtree.
	 * @param clazz
	 *            The type of the objects to be returned.
	 * @param includeRoot
	 *            If true and the root object has type {@code clazz}, it will be
	 *            included in the result. Otherwise, the root object will not be
	 *            in the result list.
	 * @param predicate
	 *            Predicate that must be satisfied for all returned content
	 *            elements.
	 * @return Iterable contents with the given type satisfying the given
	 *         predicate.
	 */
	public static <T extends EObject> Iterable<T> getAllContentsOfTypeIterable(EObject rootObject, Class<T> clazz,
			boolean includeRoot, Predicate<T> predicate) {
		Iterable<T> childrenIterable = Iterables.filter(Iterables.filter(toIterable(rootObject.eAllContents()), clazz),
				predicate::test);

		if (includeRoot && clazz.isInstance(rootObject) && predicate.test(clazz.cast(rootObject))) {
			// root node is needed too
			T root = clazz.cast(rootObject);
			return Iterables.concat(Collections.singletonList(root), childrenIterable);
		} else {
			return childrenIterable;
		}
	}

	/**
	 * Returns an ordered stream of all objects with the given type in the
	 * containment subtree of the given root object's subtree.
	 * 
	 * The method will return a subset of all descendants, not only the
	 * immediate children. Depending on the {@code includeRoot} parameter, the
	 * subtree that is searched may or may not include the root object itself.
	 * If the given root object's type is not {@code clazz}, the value of the
	 * {@code includeRoot} parameter is indifferent.
	 * 
	 * @see #getAllContentsOfType(EObject, Class, boolean)
	 * @see #getAllContentsOfTypeIterable(EObject, Class, boolean)
	 * 
	 * @param rootObject
	 *            The root object. The results will be returned from the
	 *            subtree.
	 * @param clazz
	 *            The type of the objects to be returned.
	 * @param includeRoot
	 *            If true and the root object has type {@code clazz}, it will be
	 *            included in the result. Otherwise, the root object will not be
	 *            in the result list.
	 * @return Stream of the contents with the given type.
	 */
	public static <T extends EObject> Stream<T> getAllContentsOfTypeStream(EObject rootObject, Class<T> clazz,
			boolean includeRoot) {
		return getAllContentsOfTypeStream(rootObject, clazz, includeRoot, it -> true);
	}

	/**
	 * Returns an ordered stream of all objects with the given type in the
	 * containment subtree of the given root object's subtree which satisfy the
	 * given predicate.
	 * 
	 * The method will return a subset of all descendants, not only the
	 * immediate children. Depending on the {@code includeRoot} parameter, the
	 * subtree that is searched may or may not include the root object itself.
	 * If the given root object's type is not {@code clazz}, the value of the
	 * {@code includeRoot} parameter is indifferent.
	 * 
	 * @see #getAllContentsOfType(EObject, Class, boolean, Predicate)
	 * @see #getAllContentsOfTypeIterable(EObject, Class, boolean, Predicate)
	 * 
	 * @param rootObject
	 *            The root object. The results will be returned from the
	 *            subtree.
	 * @param clazz
	 *            The type of the objects to be returned.
	 * @param includeRoot
	 *            If true and the root object has type {@code clazz}, it will be
	 *            included in the result. Otherwise, the root object will not be
	 *            in the result list.
	 * @param predicate
	 *            Predicate that must be satisfied for all returned content
	 *            elements.
	 * @return Stream of the contents with the given type satisfying the given
	 *         predicate.
	 */
	public static <T extends EObject> Stream<T> getAllContentsOfTypeStream(EObject rootObject, Class<T> clazz,
			boolean includeRoot, Predicate<T> predicate) {
		Iterator<T> iter = new Iterator<T>() {
			private T next = (includeRoot && clazz.isInstance(rootObject) && predicate.test(clazz.cast(rootObject)))
					? clazz.cast(rootObject)
					: null;
			private final TreeIterator<EObject> wrappedTreeIterator = rootObject.eAllContents();

			@SuppressWarnings("unchecked")
			@Override
			public boolean hasNext() {
				if (next != null) {
					return true;
				} else {
					while (wrappedTreeIterator.hasNext()) {
						EObject nextEobject = wrappedTreeIterator.next();
						if (clazz.isInstance(nextEobject) && predicate.test((T) nextEobject)) {
							next = clazz.cast(nextEobject);
							return true;
						}
					}
					return false;
				}
			}

			@Override
			public T next() {
				if (next != null || hasNext()) {
					T ret = next;
					next = null;
					return ret;
				} else {
					throw new NoSuchElementException();
				}
			}
		};
		return StreamSupport
				.stream(Spliterators.spliteratorUnknownSize(iter, Spliterator.ORDERED | Spliterator.NONNULL), false);
	}

	/**
	 * Deletes all {@link EObject}s in the {@code eObjects} collection. It is an
	 * improved version of {@link EcoreUtil#delete(EObject)} that does not
	 * compute the cross-references from scratch for each object to be deleted.
	 * 
	 * @param eObjects
	 *            Objects to be deleted.
	 */
	public static void deleteAll(Collection<? extends EObject> eObjects) {
		if (eObjects == null || eObjects.isEmpty()) {
			return;
		}

		ArrayList<? extends EObject> list = new ArrayList<>(eObjects);
		EObject rootEObject = EcoreUtil.getRootContainer(list.get(0));
		Resource resource = rootEObject.eResource();

		Map<EObject, Collection<EStructuralFeature.Setting>> usagesMap;
		if (resource == null) {
			usagesMap = UsageCrossReferencer.findAll(eObjects, rootEObject);
		} else {
			ResourceSet resourceSet = resource.getResourceSet();
			if (resourceSet == null) {
				usagesMap = UsageCrossReferencer.findAll(eObjects, resource);
			} else {
				usagesMap = UsageCrossReferencer.findAll(eObjects, resourceSet);
			}
		}

		for (EObject eObject : list) {
			Collection<EStructuralFeature.Setting> usages = usagesMap.get(eObject);
			if (usages != null) {
				for (EStructuralFeature.Setting setting : usages) {
					if (setting.getEStructuralFeature().isChangeable()) {
						EcoreUtil.remove(setting, eObject);
					}
				}
			} else {
				// nothing to do, it is not used anywhere else
			}
			EcoreUtil.remove(eObject);
		}
	}

	public static IEquatableEObjectFactory defaultEObjectWrapperFactory() {
		return new IEquatableEObjectFactory() {
			public EquatableEObject createEquatableWrapper(EObject objToWrap) {
				return new EquatableEObject(objToWrap);
			}
		};
	}
}
