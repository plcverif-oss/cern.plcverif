/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.common.settings;

import java.util.Map.Entry;

public final class SettingsToPlantUml {
	private SettingsToPlantUml() {
		// Utility class.
	}
	
	public static String generate(SettingsElement root) {
		StringBuilder sb = new StringBuilder();

		sb.append("@startuml\n\n");

		generateElement(sb, root);

		sb.append("note top of ");

		if (root.fqn().equals("")) {
			sb.append("__root");
		} else {
			sb.append(root.fqn().replaceAll("[^A-Za-z0-9]", "_"));
		}
		sb.append("\n");
		sb.append(SettingsSerializer.serialize(root).replace("\\", "\\\\"));
		sb.append("end note\n\n");

		sb.append("@enduml\n");

		return sb.toString();
	}

	public static void generateElement(StringBuilder sb, Settings element) {
		sb.append("object \"");
		if (element.fqn().equals("")) {
			sb.append("__root");
		} else {
			sb.append(element.fqn().replaceAll("[^A-Za-z0-9]", "_"));
		}
		sb.append(" : ");
		if (element.kind() == SettingsKind.ELEMENT) {
			SettingsElement single = (SettingsElement) element;
			sb.append("Object\" as ");
			if (element.fqn().equals("")) {
				sb.append("__root");
			} else {
				sb.append(element.fqn().replaceAll("[^A-Za-z0-9]", "_"));
			}
			sb.append(" {\n");

			sb.append("\tvalue = ");
			
			if (single.valueWithoutConsuming() == null) {
				sb.append("null");
			} else {
				sb.append(single.valueWithoutConsuming().contains(" ") ? "\"" + single.valueWithoutConsuming().replace("\\", "\\\\") + "\"" : single.valueWithoutConsuming().replace("\\", "\\\\"));	
			}
				
			sb.append("\n}\n\n");

			for (Entry<String, Settings> kvp : single.attributes().entrySet()) {
				generateElement(sb, kvp.getValue());

				if (single.fqn().equals("")) {
					sb.append("__root");
				} else {
					sb.append(single.fqn().replaceAll("[^A-Za-z0-9]", "_"));
				}
				sb.append(" *-- ");
				sb.append(kvp.getValue().fqn().replaceAll("[^a-zA-Z0-9]", "_"));
				sb.append(": \"");
				sb.append(kvp.getKey());
				sb.append("\"\n\n");
			}
		}
		else {
			SettingsList list = (SettingsList) element;
			sb.append("List\" as ");
			sb.append(element.fqn().replaceAll("[^a-zA-Z0-9]", "_"));
			sb.append("\n");

			for (int i = 0; i < list.elements().size(); ++i) {
				if (list.elements().get(i) != null) {
					generateElement(sb, list.elements().get(i));

					if (list.fqn().equals("")) {
						sb.append("__root");
					} else {
						sb.append(list.fqn().replaceAll("[^A-Za-z0-9]", "_"));
					}
					sb.append(" *-- ");
					sb.append(list.elements().get(i).fqn().replaceAll("[^a-zA-Z0-9]", "_"));
					sb.append(": [");
					sb.append(i);
					sb.append("]");
					sb.append('\n');
				}
			}
		}
	}
}
