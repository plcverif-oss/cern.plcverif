/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.formattedstring;

import com.google.common.base.Preconditions;

/**
 * Formats that can be used for the segments of {@linkplain BasicFormattedString
 * basic formatted strings}.
 */
public final class Formats {
	private Formats() {
		// Empty on purpose.
	}
	
	/**
	 * Bold font format for a string segment.
	 */
	public static final class BoldFormat implements BasicFormattedString.Format {
		public static final BoldFormat INSTANCE = new BoldFormat();

		private BoldFormat() {
		}

		@Override
		public String getFormatPrefix(Formatter f) {
			return f.formatPrefix(this);
		}

		@Override
		public String getFormatSuffix(Formatter f) {
			return f.formatSuffix(this);
		}
	}

	/**
	 * Italic font format for a string segment.
	 */
	public static final class ItalicFormat implements BasicFormattedString.Format {
		public static final ItalicFormat INSTANCE = new ItalicFormat();

		private ItalicFormat() {
		}

		@Override
		public String getFormatPrefix(Formatter f) {
			return f.formatPrefix(this);
		}

		@Override
		public String getFormatSuffix(Formatter f) {
			return f.formatSuffix(this);
		}
	}

	/**
	 * Colored font format for a string segment.
	 */
	public static final class ColorFormat implements BasicFormattedString.Format {
		private String hexColor;

		/**
		 * Returns true iff the given color string matches the format expected
		 * by {@link ColorFormat}.
		 * 
		 * @param colorString
		 *            Color string
		 * @return True iff the color string matches the expected format
		 */
		public static boolean isValidColor(String colorString) {
			return colorString != null && colorString.matches("#?[0-9A-Fa-f]{6}");
		}

		/**
		 * Creates a new colored font format.
		 * 
		 * @param hexColor
		 *            Hexadecimal representation of the color to be used. Shall
		 *            not be {@code null}.
		 */
		public ColorFormat(String hexColor) {
			this.hexColor = Preconditions.checkNotNull(hexColor);
			Preconditions.checkArgument(isValidColor(hexColor),
					"The given color is not valid. It is expected to be in hexadecimal HTML format, e.g., AB12EF or #AB12EF.");
		}

		public String getHexColor() {
			return hexColor;
		}

		@Override
		public String getFormatPrefix(Formatter f) {
			return f.formatPrefix(this);
		}

		@Override
		public String getFormatSuffix(Formatter f) {
			return f.formatSuffix(this);
		}
	}
}
