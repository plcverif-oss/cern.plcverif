/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Vince Molnar - refactoring
 *******************************************************************************/
package cern.plcverif.base.common.emf;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;

public final class EObjectMap<K extends EObject, V> implements Map<K, V>, IEObjectMap<K, V> {
	/**
	 * Interface for Map<K,V> factories to alter the map implementation used in
	 * the EObjectMap.
	 */
	@FunctionalInterface
	public interface IMapFactory<V> {
		Map<EquatableEObject, V> createMap();
	}

	private Map<EquatableEObject, V> wrappedMap;
	private IEquatableEObjectFactory eobjectWrapperFactory;

	/**
	 * Creates a new EObject map with default underlying implementation
	 * ({@link HashMap}).
	 */
	public EObjectMap() {
		this.wrappedMap = new HashMap<>();
		this.eobjectWrapperFactory = EmfHelper.defaultEObjectWrapperFactory();
	}

	/**
	 * Creates a new EObject map with the underlying implementation created
	 * using the given {@code mapFactory}.
	 */
	public EObjectMap(IMapFactory<V> mapFactory) {
		this.wrappedMap = mapFactory.createMap();
		this.eobjectWrapperFactory = EmfHelper.defaultEObjectWrapperFactory();
	}
	
	/**
	 * Creates a new EObject map with the underlying implementation created
	 * using the given {@code mapFactory} and {@code eobjectWrapperFactory}.
	 */
	public EObjectMap(IMapFactory<V> mapFactory, IEquatableEObjectFactory eobjectWrapperFactory) {
		this.wrappedMap = mapFactory.createMap();
		this.eobjectWrapperFactory = eobjectWrapperFactory;
	}

	@Override
	public void clear() {
		wrappedMap.clear();
	}

	@Override
	public boolean containsKey(Object key) {
		if (key instanceof EObject) {
			return wrappedMap.containsKey(toEquatableEObject((EObject) key));
		} else {
			return false;
		}
	}

	public boolean containsKey(K key) {
		return wrappedMap.containsKey(toEquatableEObject(key));
	}

	@Override
	public boolean containsValue(Object value) {
		return wrappedMap.containsValue(value);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * This implementation satisfies the contract of {@link Map#entrySet()}, but
	 * it is <b>not an efficient implementation</b>.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Set<java.util.Map.Entry<K, V>> entrySet() {
		return wrappedMap.entrySet().stream()
				.map(it -> new AbstractMap.SimpleEntry<K, V>((K) it.getKey().getWrappedEObject(), it.getValue()))
				.collect(Collectors.toSet());
	}
	
	public Iterable<K> keys() {
		return new Iterable<K>() {
			@Override
			public Iterator<K> iterator() {
				return new Iterator<K>() {
					private Iterator<EquatableEObject> wrappedIterator = wrappedMap.keySet().iterator();
					
					@Override
					public boolean hasNext() {
						return wrappedIterator.hasNext();
					}

					@SuppressWarnings("unchecked")
					@Override
					public K next() {
						return (K) wrappedIterator.next().getWrappedEObject();
					}
				};
			}
		};
	}
	
	public Set<java.util.Map.Entry<EquatableEObject, V>> wrappedEntrySet() {
		return wrappedMap.entrySet();
	}

	@Override
	public V get(Object key) {
		if (key instanceof EObject) {
			return wrappedMap.get(toEquatableEObject((EObject) key));
		}
		return null;
	}

	public V get(K key) {
		return wrappedMap.get(toEquatableEObject(key));
	}

	@Override
	public boolean isEmpty() {
		return wrappedMap.isEmpty();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<K> keySet() {
		return wrappedMap.keySet().stream().map(it -> (K) it.getWrappedEObject()).collect(Collectors.toSet());
	}

	@Override
	public V put(K key, V value) {
		return wrappedMap.put(toEquatableEObject(key), value);
	}
	
	public V putWrapped(EquatableEObject wrappedKey, V value) {
		return wrappedMap.put(wrappedKey, value);
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> map) {
		for (java.util.Map.Entry<? extends K, ? extends V> kvp : map.entrySet()) {
			put(kvp.getKey(), kvp.getValue());
		}
	}

	@Override
	public V remove(Object key) {
		if (key instanceof EObject) {
			return remove((EObject) key);
		}
		return null;
	}

	public V remove(EObject key) {
		return wrappedMap.remove(toEquatableEObject(key));
	}

	@Override
	public int size() {
		return wrappedMap.size();
	}

	@Override
	public Collection<V> values() {
		return wrappedMap.values();
	}

	private EquatableEObject toEquatableEObject(EObject e) {
		return eobjectWrapperFactory.createEquatableWrapper(e);
	}

	/**
	 * Removes all items from the map for which the given predicate is evaluated
	 * to be true.
	 * 
	 * @param predicate
	 *            Predicate to decide which entries need to be removed from the
	 *            map. The first parameter of the predicate is the key, the
	 *            second is the value of the map entry.
	 */
	@SuppressWarnings("unchecked")
	public void removeIf(BiPredicate<K, V> predicate) {
		for (EquatableEObject wrappedKey : new ArrayList<>(this.wrappedMap.keySet())) {
			if (predicate.test((K) wrappedKey.getWrappedEObject(), this.wrappedMap.get(wrappedKey))) {
				this.wrappedMap.remove(wrappedKey);
			}
		}
	}
}
