/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.progress;

/**
 * Progress reporter that does not do anything and that will never cancel the job.
 * 
 * Empty implementation of the {@link ICancelingProgressReporter} interface.
 */
public final class NullProgressReporter implements ICancelingProgressReporter {
	private static final NullProgressReporter INSTANCE = new NullProgressReporter();
	
	public static NullProgressReporter getInstance() {
		return INSTANCE;
	}
	
	private NullProgressReporter() {
		// Singleton.
	}
	
	@Override
	public void reportTask(final String currentTask) {
		// Nothing to do in the null progress reporter.
	}

	@Override
	public void reportProgress(final int percentage) {
		// Nothing to do in the null progress reporter.
	}

	@Override
	public final void report(final String currentTask, final int percentage) {
		// Nothing to do in the null progress reporter.
	}

	@Override
	public boolean isCanceled() {
		return false;
	}
}
