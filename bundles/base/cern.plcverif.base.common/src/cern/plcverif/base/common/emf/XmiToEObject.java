/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.emf;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

import com.google.common.base.Preconditions;

public final class XmiToEObject {
	private XmiToEObject() {
		// Utility class.
	}
	
	public static <T extends EObject> T deserializeFromFile(URI sourceUri, Class<T> expectedType) throws IOException {
		Preconditions.checkNotNull(sourceUri);

		// Create resource
		final XMIResourceImpl resource = new XMIResourceImpl(sourceUri);
		resource.getDefaultLoadOptions().put(XMLResource.OPTION_DEFER_IDREF_RESOLUTION, Boolean.TRUE);
		resource.setIntrinsicIDToEObjectMap(new HashMap<>());
		resource.load(null);

		return tryToExtractCfa(resource, expectedType);
	}

	public static <T extends EObject> T deserializeFromFile(File sourceFile, Class<T> expectedType) throws IOException {
		Preconditions.checkNotNull(sourceFile);

		URI sourceUri = URI.createURI(sourceFile.toURI().toString());
		return deserializeFromFile(sourceUri, expectedType);
	}

	public static <T extends EObject> T deserializeFromString(String serializedCfa, Class<T> expectedType) throws IOException {
		Preconditions.checkNotNull(serializedCfa);

		// Create resource
		final XMIResource resource = new XMIResourceImpl(URI.createURI("resource"));

		// Create StringReader
		try (URIConverter.ReadableInputStream inStream = new URIConverter.ReadableInputStream(serializedCfa, "UTF-8")) {
			resource.load(inStream, null);
			return tryToExtractCfa(resource, expectedType);
		}
	}

	private static <T extends EObject> T tryToExtractCfa(XMIResource resource, Class<T> expectedType) throws IOException {
		EObject content = resource.getContents().get(0);
		// check validity
		if (content == null) {
			throw new IOException("Unsuccessful loading. The loaded content is null.");
		} else if (!expectedType.isInstance(content)) {
			throw new IOException(
				String.format("Unsuccessful loading. The loaded root element has a type %s instead of %s.",
						content.getClass().getSimpleName(), expectedType.getSimpleName()));
		}

		return expectedType.cast(content);
	}
}
