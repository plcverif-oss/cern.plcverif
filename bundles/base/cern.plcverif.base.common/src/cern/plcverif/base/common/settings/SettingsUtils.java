/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.settings;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.regex.Pattern;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;

public final class SettingsUtils {
	public static final String FQN_SEPARATOR = ".";
	
	/**
	 * A string that is guaranteed to be treated as value {@code true} by {@link SpecificSettingsSerializer}.
	 */
	public static final String TRUE_STRING = "true";
		
	/**
	 * A string that that is guaranteed to be treated as value {@code false} by {@link SpecificSettingsSerializer}.
	 */
	public static final String FALSE_STRING = "false";
	
	private SettingsUtils() {
		// Utility class.
	}
	
	/**
	 * Collects all children settings elements recursively which have not been consumed yet.
	 * <p>
	 * The settings elements which have null value AND children at the same time are not included in the returned collection. 
	 * @param settings The root of settings tree in which the search is performed.
	 * @return The list of non-consumed children elements.
	 * @throws SettingsParserException 
	 * @see SettingsElement#isConsumed()
	 */
	public static List<SettingsElement> nonConsumedSettingsElements(Settings settings) throws SettingsParserException {
		List<SettingsElement> ret = new ArrayList<>();
		collectNonConsumedSettingsElements(settings, ret);
		return ret;
	}
	
	private static void collectNonConsumedSettingsElements(Settings settings, List<SettingsElement> accu) throws SettingsParserException {
		if (settings == null) {
			// Nothing to do.
			return;
		}
		
		if (settings.kind() == SettingsKind.ELEMENT) {
			collectNonConsumedSettingsElements(settings.toSingle(), accu);
		} else if (settings.kind() == SettingsKind.LIST) {
			collectNonConsumedSettingsElements(settings.toList(), accu);
		} else {
			throw new UnsupportedOperationException("Unknown SettingsKind: " + settings.kind());
		}
	}
	
	private static void collectNonConsumedSettingsElements(SettingsElement settings, List<SettingsElement> accu) throws SettingsParserException {
		Preconditions.checkNotNull(settings);
		
		if (!settings.isConsumed()) {
			if (settings.value() == null && !settings.getAttributeNames().isEmpty()) {
				// This looks like a container pseudonode, let's skip. 
			} else {
				accu.add(settings);
			}
		}
		
		for (String attribName : settings.getAttributeNames()) {
			collectNonConsumedSettingsElements(settings.getAttribute(attribName).orElseThrow(() -> new IllegalStateException("Internal inconsistency in the given settings element.")), accu);
		}
	}
	
	private static void collectNonConsumedSettingsElements(SettingsList settings, List<SettingsElement> accu) throws SettingsParserException {
		Preconditions.checkNotNull(settings);
		
		for (Settings child : settings.elements()) {
			collectNonConsumedSettingsElements(child, accu);
		}
	}
	
	/**
	 * Collects all children settings nodes recursively which satisfy the given
	 * predicate. The root of the given settings tree will also be included if
	 * it satisfies the predicate.
	 * 
	 * @param settings
	 *            The root of settings tree in which the search is performed.
	 * @param predicate
	 *            Predicate.
	 * @return The list of children elements satisfying the predicate.
	 * @throws SettingsParserException
	 */
	public static List<Settings> collectAllSettings(Settings settings, Predicate<Settings> predicate) throws SettingsParserException {
		List<Settings> ret = new ArrayList<>();
		collectAllSettings(settings, ret, predicate);
		return ret;
	}
	
	private static void collectAllSettings(Settings settings, List<Settings> accu, Predicate<Settings> predicate) throws SettingsParserException {
		if (settings == null) {
			// Nothing to do.
			return;
		}
		
		if (settings.kind() == SettingsKind.ELEMENT) {
			collectAllSettings(settings.toSingle(), accu, predicate);
		} else if (settings.kind() == SettingsKind.LIST) {
			collectAllSettings(settings.toList(), accu, predicate);
		} else {
			throw new UnsupportedOperationException("Unknown SettingsKind: " + settings.kind());
		}
	}
	
	private static void collectAllSettings(SettingsElement settings, List<Settings> accu, Predicate<Settings> predicate) throws SettingsParserException {
		Preconditions.checkNotNull(settings);
		
		if (predicate.test(settings)) {
			accu.add(settings);
		}
		
		for (String attribName : settings.getAttributeNames()) {
			collectAllSettings(settings.getAttribute(attribName).orElseThrow(() -> new IllegalStateException("Internal inconsistency in the given settings element.")), accu, predicate);
		}
	}
	
	private static void collectAllSettings(SettingsList settings, List<Settings> accu, Predicate<Settings> predicate) throws SettingsParserException {
		Preconditions.checkNotNull(settings);
		if (predicate.test(settings)) {
			accu.add(settings);
		}
		
		for (Settings child : settings.elements()) {
			collectAllSettings(child, accu, predicate);
		}
	}
	
	/**
	 * Returns the root parent element of the given settings node, i.e. the
	 * topmost settings node in the settings hierarchy that has
	 * {@link Settings#getParent()} {@code null}.
	 * 
	 * @param settings
	 *            The object to get the root settings node for.
	 * @return The root settings node.
	 */
	public static Settings getRootSettingsNode(Settings settings) {
		Settings ret = settings;
		while (ret.getParent() != null) {
			ret = ret.getParent();
		}
		return ret;
	}
	
	/**
	 * Returns true iff the given {@code settings} is a descendant of the given {@code node}.
	 * If {@code settings} is the same as {@code node}, the returned value is true.
	 * @param settings Checked settings node that may or may not be a descendant of {@code node}.
	 * @param node Settings node.
	 * @return True iff {@code settings} is a descendant of {@code node}.
	 */
	public static boolean isDescendantOrSelf(Settings settings, Settings node) {
		Settings current = settings;
		while (current != null) {
			if (current == node ) {
				return true;
			}
			current = current.getParent();
		}
		return false;
	}
	
	/**
	 * Returns true iff the given textual value shall be treated as {@code true} for a settings value.
	 * 
	 * It is guaranteed that this method returns true for {@code Boolean.toString(true)}.
	 * @param value Textual value. Shall not be {@code null}.
	 * @return True iff it represents the value {@code true}
	 */
	public static boolean isTrue(String value) {
		Preconditions.checkNotNull(value);
		String trimmedValue = value.trim();
		return TRUE_STRING.equalsIgnoreCase(trimmedValue) || "1".equals(trimmedValue);
	}
	
	/**
	 * Returns true iff the given textual value shall be treated as {@code false} for a settings value.
	 * 
	 * It is guaranteed that this method returns true for {@code Boolean.toString(false)}.
	 * @param value Textual value. Shall not be {@code null}.
	 * @return True iff it represents the value {@code false}
	 */
	public static boolean isFalse(String value) {
		Preconditions.checkNotNull(value);
		String trimmedValue = value.trim();
		return FALSE_STRING.equalsIgnoreCase(trimmedValue) || "0".equals(trimmedValue);
	}
	
	/**
	 * Concatenates the given attribute names (FQN segments) into a FQN.
	 */
	public static String toFqn(String attribute1, String... attributes) {
		if (attributes == null) {
			return attribute1;
		} else {
			return attribute1 + FQN_SEPARATOR + String.join(FQN_SEPARATOR, attributes);
		}
	}
	
	/**
	 * Returns the value of the given attribute, if such attribute exists in the {@code parentNode} and it is a {@link SettingsElement} (thus has a value).
	 * No exception is thrown if the attribute is not found.
	 * @param parentNode Settings node in which the attribute is looked up. Shall not be {@code null}.
	 * @param attributeName Attribute name. Shall not be {@code null}.
	 * @return The value of the attribute, or {@link Optional#empty()} if it is not found
	 */
	public static Optional<String> tryGetAttributeValue(SettingsElement parentNode, String attributeName) {
		Preconditions.checkNotNull(parentNode);
		Preconditions.checkNotNull(attributeName);
		
		Optional<Settings> childNode = parentNode.getAttribute(attributeName);
		if (childNode.isPresent() && childNode.get() instanceof SettingsElement) {
			return Optional.ofNullable(((SettingsElement)childNode.get()).value());
		}
		return Optional.empty();
	}
	
	/**
	 * Tries to return the settings node that is located at the given path (FQN) relative to the given root settings node.
	 * @param rootNode Root settings node. The given FQN is relative to this.
	 * @param fqn The FQN of the settings node to be returned.
	 * @param createIfNotExists If no settings node is found with the given FQN and this flag is true, new {@link SettingsElement}s will be created
	 * @return The requested node, if exists (or if possible and requested to be created)
	 */
	public static Optional<Settings> tryGetNode(Settings rootNode, String fqn, boolean createIfNotExists) {
		Preconditions.checkNotNull(rootNode);
		Preconditions.checkNotNull(fqn);

		// Split
		String fqnHead;
		String fqnTail;
		String[] segments = fqn.split(Pattern.quote(FQN_SEPARATOR), 2);
		fqnHead = segments[0];
		fqnTail = segments.length >= 2 ? segments[1] : null;
		Preconditions.checkState(fqnHead != null);

		// Try to fetch attribute
		Optional<Settings> attribute = Optional.empty();
		try {
			if (rootNode.kind() == SettingsKind.ELEMENT) {
				attribute = rootNode.toSingle().getAttribute(fqnHead);

				if (createIfNotExists && !attribute.isPresent()) {
					attribute = Optional.of(SettingsElement.empty());
					rootNode.toSingle().setAttribute(fqnHead, attribute.get());
				}
			} else if (rootNode.kind() == SettingsKind.LIST) {
				int index = Integer.parseInt(fqnHead);
				if (rootNode.toList().elements().size() <= index) {
					if (createIfNotExists) {
						while (rootNode.toList().elements().size() < index) {
							rootNode.toList().add(null);
						}
						attribute = Optional.of(SettingsElement.empty());
						rootNode.toList().add(attribute.get());
					} else {
						attribute = Optional.empty();
					}
				} else {
					if (createIfNotExists && rootNode.toList().elements().get(index) == null) {
						rootNode.toList().elements().set(index, SettingsElement.empty());
					}
					attribute = Optional.ofNullable(rootNode.toList().elements().get(index));
				}
			} else {
				throw new UnsupportedOperationException("Unsupported settings kind: " + rootNode.kind());
			}
		} catch (SettingsParserException e) {
			return Optional.empty();
		}

		if (fqnTail == null) {
			// We reached the needed attribute
			return attribute;
		} else if (!attribute.isPresent()) {
			// If we didn't find this attribute, there is no point in going forward.
			return Optional.empty();
		} else {
			// Go deeper
			return tryGetNode(attribute.get(), fqnTail, createIfNotExists);
		}
	}
}
