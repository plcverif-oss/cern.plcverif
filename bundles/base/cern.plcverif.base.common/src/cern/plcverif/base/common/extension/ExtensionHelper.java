/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - refactoring and 
 *******************************************************************************/
package cern.plcverif.base.common.extension;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.Platform;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.logging.PlatformLogger;

public final class ExtensionHelper {
	private ExtensionHelper() {
		// Utility class.
	}

	/**
	 * Returns the extensions implementing the given class or interface. Works
	 * only for classes annotated with @{@link ExtensionPoint}. This annotation
	 * will define the ID of the extension point.
	 * 
	 * @param extensionPoint
	 *            Expected class or interface to be implemented
	 * @return Map of extensions: key is the command ID ({@code cmd_id}), value
	 *         is an instance of the extension point.
	 */
	public static <T> Map<String, T> getExtensions(Class<T> extensionPoint) {
		ExtensionPoint extensionAnnotation = extensionPoint.getAnnotation(ExtensionPoint.class);
		Preconditions.checkNotNull(extensionAnnotation,
				"The class provided as an extension point is not annotated with the ExtensionPoint annotation.");

		String extensionId = extensionAnnotation.extensionPointId();
		Preconditions.checkNotNull(extensionId,
				"The class provided as an extension point has not specified the extension point ID in the ExtensionPoint annotation.");

		IConfigurationElement[] config = Platform.getExtensionRegistry().getConfigurationElementsFor(extensionId);
		try {
			Map<String, T> ret = new HashMap<>();

			for (IConfigurationElement e : config) {
				final String cmdId = e.getAttribute("cmd_id");
				final Object o = e.createExecutableExtension("class");
				if (extensionPoint.isInstance(o)) {
					ret.put(cmdId, extensionPoint.cast(o));
				} else {
					PlatformLogger.logError("Unknown extension type: " + o);
				}
			}

			return ret;
		} catch (Exception ex) {
			throw new PlcverifPlatformException("Problem occurred while loading the PLCverif extensions.", ex);
		}
	}

	/**
	 * Returns the names of the extensions implementing the given class or
	 * interface. Works only for classes annotated with @{@link ExtensionPoint}.
	 * This annotation will define the ID of the extension point.
	 * 
	 * @param extensionPoint
	 *            Expected class or interface to be implemented
	 * @return Map of extensions: key is the command ID ({@code cmd_id}), value
	 *         is the name of the extension.
	 */
	public static <T> Map<String, String> getExtensionNames(Class<T> extensionPoint) {
		ExtensionPoint extensionAnnotation = extensionPoint.getAnnotation(ExtensionPoint.class);
		Preconditions.checkNotNull(extensionAnnotation,
				"The class provided as an extension point is not annotated with the ExtensionPoint annotation.");

		String extensionId = extensionAnnotation.extensionPointId();
		Preconditions.checkNotNull(extensionId,
				"The class provided as an extension point has not specified the extension point ID in the ExtensionPoint annotation.");

		IConfigurationElement[] config = Platform.getExtensionRegistry().getConfigurationElementsFor(extensionId);
		try {
			Map<String, String> ret = new HashMap<>();

			for (IConfigurationElement e : config) {
				final String label = ((IExtension) e.getParent()).getLabel();
				final String cmdId = e.getAttribute("cmd_id");
				ret.put(cmdId, label);
			}

			return ret;
		} catch (Exception ex) {
			throw new PlcverifPlatformException("Problem occurred while loading the PLCverif extensions.", ex);
		}
	}
}
