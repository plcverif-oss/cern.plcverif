/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.formattedstring;

import com.google.common.html.HtmlEscapers;

import cern.plcverif.base.common.formattedstring.Formats.BoldFormat;
import cern.plcverif.base.common.formattedstring.Formats.ColorFormat;
import cern.plcverif.base.common.formattedstring.Formats.ItalicFormat;

/**
 * Formatter to represent a {@linkplain BasicFormattedString basic formatted string} in HTML.
 */
public class HtmlFormatter extends Formatter {
	@Override
	public String formatPrefix(BoldFormat f) {
		return "<b>";
	}

	@Override
	public String formatSuffix(BoldFormat f) {
		return "</b>";
	}

	@Override
	public String formatPrefix(ItalicFormat f) {
		return "<i>";
	}

	@Override
	public String formatSuffix(ItalicFormat f) {
		return "</i>";
	}

	@Override
	public String formatPrefix(ColorFormat f) {
		return String.format("<font color=\"%s\">", f.getHexColor().startsWith("#") ? f.getHexColor() : "#" + f.getHexColor());
	}

	@Override
	public String formatSuffix(ColorFormat f) {
		return "</font>";
	}

	@Override
	public String stringPrefix() {
		return "<p>";
	}

	@Override
	public String stringSuffix() {
		return "</p>";
	}
	
	@Override
	public String sanitizeText(String string) {
		return HtmlEscapers.htmlEscaper().escape(string).replace("\n", "<br>");
	}
}
