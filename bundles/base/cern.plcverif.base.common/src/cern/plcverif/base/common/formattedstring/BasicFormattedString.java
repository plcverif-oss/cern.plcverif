/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.formattedstring;

import java.util.ArrayList;
import java.util.List;

/**
 * Generic representation of a formatted string. It consists of segments, each
 * segment may have zero, one or more formats ({@link Format}) attached to them.
 * The formatted string can be translated to a textual markup format using
 * various implementations of {@link Formatter} and {@link #format(Formatter)}.
 */
public final class BasicFormattedString {
	/**
	 * A format description for a string segment.
	 */
	public interface Format {
		String getFormatPrefix(Formatter f);

		String getFormatSuffix(Formatter f);
	}

	private static class StringFormatPair {
		private String string;
		private Format[] formats;

		public StringFormatPair(String string, Format... formats) {
			this.string = string;
			this.formats = formats;
		}
		
		public StringFormatPair copy() {
			return new StringFormatPair(this.string, this.formats);
		}
	}

	private List<StringFormatPair> segments = new ArrayList<>();

	/**
	 * Creates a new empty formatted string.
	 */
	public BasicFormattedString() {
	}

	/**
	 * Creates a new formatted string with the given initial content. The object
	 * is mutable and the content can be modified using the
	 * {@link #append(String, Format...)} method.
	 * 
	 * @param string
	 *            Unformatted string to be used as initial content
	 * @param formats
	 *            Zero or more formats to be applied to the given string
	 */
	public BasicFormattedString(CharSequence string, Format... formats) {
		this.segments.add(new StringFormatPair(string.toString(), formats));
	}

	/**
	 * Appends a new string to this formatted string with the given formats.
	 * 
	 * @param string
	 *            Unformatted string to be appended
	 * @param formats
	 *            Zero or more formats to be applied to the given string
	 */
	public final void append(String string, Format... formats) {
		this.segments.add(new StringFormatPair(string, formats));
	}
	
	/**
	 * Appends a formatted string to this formatted string.
	 * 
	 * @param formattedString
	 *            Formatted string to be appended
	 */
	public final void append(BasicFormattedString formattedString) {
		for (StringFormatPair segment : formattedString.segments) {
			this.segments.add(segment.copy());
		}
	}
	
	/**
	 * Prepends a new string to this formatted string with the given formats.
	 * This method has worse performance than {@link #append(String, Format...)}.
	 * 
	 * @param string
	 *            Unformatted string to be prepended
	 * @param formats
	 *            Zero or more formats to be applied to the given string
	 */
	public final void prepend(String string, Format... formats) {
		this.segments.add(0, new StringFormatPair(string, formats));
	}
	

	/**
	 * Returns a text representation of the formatted string using the given
	 * formatter.
	 * 
	 * @param formatter
	 *            Formatter to be used to generate the text representation.
	 * @return Formatted (markup) text representation of this object
	 */
	public String format(Formatter formatter) {
		StringBuilder ret = new StringBuilder();

		ret.append(formatter.stringPrefix());

		for (StringFormatPair segment : segments) {
			// Format prefixes
			for (Format format : segment.formats) {
				ret.append(format.getFormatPrefix(formatter));
			}

			ret.append(formatter.sanitizeText(segment.string));

			// Format suffixes
			for (Format format : segment.formats) {
				ret.append(format.getFormatSuffix(formatter));
			}
		}

		ret.append(formatter.stringSuffix());

		return ret.toString();
	}
	
	/**
	 * Returns true if this represents an empty string.
	 * @return True if no string segment has been specified.
	 */
	public boolean isEmpty() {
		return this.segments.isEmpty();
	}
}
