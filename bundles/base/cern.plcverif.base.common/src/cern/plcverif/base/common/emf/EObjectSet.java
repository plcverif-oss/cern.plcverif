/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - refactoring and performance improvements
 *******************************************************************************/
package cern.plcverif.base.common.emf;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;

import com.google.common.base.Preconditions;

public final class EObjectSet<E extends EObject> implements Set<E> {
	/**
	 * Interface for Set<E> factories to alter the set implementation used in
	 * the EObjectSet.
	 */
	public interface ISetFactory {
		<E> Set<E> createSet();
	}

	private Set<EquatableEObject> wrappedSet;
	private IEquatableEObjectFactory eobjectWrapperFactory;

	/**
	 * Creates a new EObject set with default underlying implementation
	 * ({@link HashSet}).
	 */
	public EObjectSet() {
		this(EmfHelper.defaultEObjectWrapperFactory());
	}

	/**
	 * Creates a new EObject set with the underlying implementation created
	 * using the given {@code setFactory}.
	 */
	public EObjectSet(ISetFactory setFactory) {
		this(setFactory, EmfHelper.defaultEObjectWrapperFactory());
	}

	/**
	 * Creates a new EObject set with default underlying implementation
	 * ({@link HashSet}) and the given EObject wrapper factory.
	 */
	public EObjectSet(IEquatableEObjectFactory eobjectWrapperFactory) {
		this.wrappedSet = new HashSet<>();
		this.eobjectWrapperFactory = Preconditions.checkNotNull(eobjectWrapperFactory);
	}

	/**
	 * Creates a new EObject set with the underlying implementation created
	 * using the given {@code setFactory} and the given EObject wrapper factory.
	 */
	public EObjectSet(ISetFactory setFactory, IEquatableEObjectFactory eobjectWrapperFactory) {
		this.wrappedSet = setFactory.createSet();
		this.eobjectWrapperFactory = Preconditions.checkNotNull(eobjectWrapperFactory);
	}

	@Override
	public void clear() {
		wrappedSet.clear();
	}

	@Override
	public boolean contains(Object key) {
		if (key instanceof EObject) {
			return wrappedSet.contains(toEquatableEObject((EObject) key));
		} else {
			return false;
		}
	}

	public boolean contains(E key) {
		return wrappedSet.contains(toEquatableEObject(key));
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		for (Object item : c) {
			if (!this.contains(item)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean isEmpty() {
		return wrappedSet.isEmpty();
	}

	@Override
	public boolean add(E e) {
		return wrappedSet.add(toEquatableEObject(e));
	}

	@Override
	public boolean addAll(Collection<? extends E> c) {
		return wrappedSet.addAll(c.stream().map(elem -> toEquatableEObject(elem)).collect(Collectors.toList()));
	}

	@Override
	public boolean remove(Object o) {
		if (o instanceof EObject) {
			return wrappedSet.remove(toEquatableEObject((EObject) o));
		}
		return false;
	}

	public boolean remove(EObject key) {
		return wrappedSet.remove(toEquatableEObject(key));
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		boolean changed = false;
		for (Object obj : c) {
			changed = this.remove(obj) || changed;
		}
		return changed;
	}

	@Override
	public int size() {
		return wrappedSet.size();
	}

	private EquatableEObject toEquatableEObject(EObject e) {
		return eobjectWrapperFactory.createEquatableWrapper(e);
	}

	@Override
	public Iterator<E> iterator() {
		return new Iterator<E>() {
			Iterator<EquatableEObject> internalIterator = wrappedSet.iterator();

			@Override
			public boolean hasNext() {
				return internalIterator.hasNext();
			}

			@SuppressWarnings("unchecked")
			@Override
			public E next() {
				return (E) internalIterator.next().getWrappedEObject();
			}
		};
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		boolean hasChanged = false;
		if (c.isEmpty()) {
			hasChanged = !this.wrappedSet.isEmpty();
			this.clear();
			return hasChanged;
		}

		EObjectSet<EObject> toBeRetained = new EObjectSet<>();
		for (Object item : c) {
			if (item instanceof EObject) {
				toBeRetained.add((EObject) item);
			}
		}

		for (EquatableEObject item : new ArrayList<>(this.wrappedSet)) {
			if (!toBeRetained.contains(item.getWrappedEObject())) {
				this.wrappedSet.remove(item);
				hasChanged = true;
			}
		}
		return hasChanged;
	}

	@Override
	public Object[] toArray() {
		return this.wrappedSet.stream().map(elem -> elem.getWrappedEObject()).collect(Collectors.toList()).toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return this.wrappedSet.stream().map(elem -> elem.getWrappedEObject()).collect(Collectors.toList()).toArray(a);
	}

	public static <E extends EObject> Collector<E, ?, EObjectSet<E>> collector() {
		return Collector.of(() -> new EObjectSet<E>(), (result, it) -> result.add((E) it), (result1, result2) -> {
			result1.addAll(result2);
			return result1;
		}, Collector.Characteristics.UNORDERED);
	}
}
