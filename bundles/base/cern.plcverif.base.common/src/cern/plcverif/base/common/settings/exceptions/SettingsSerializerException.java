/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.settings.exceptions;

public class SettingsSerializerException extends Exception {
	private static final long serialVersionUID = -2479792736073359730L;

	public SettingsSerializerException() {
		super("There was a problem during serializing the configuration and the operation could not finish.");
	}

	public SettingsSerializerException(String message, Object... params) {
		super(String.format(message, params));
	}

	public SettingsSerializerException(Throwable cause, String message, Object... params) {
		super(String.format(message, params), cause);
	}
}
