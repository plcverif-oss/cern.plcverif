/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.common.utils

import java.text.DecimalFormatSymbols
import java.text.DecimalFormat
import com.google.common.base.Preconditions

/**
 * Utility class for user interface-related (both console and graphical) methods.
 */
final class UiUtils {
	static final java.util.Locale LOCALE = java.util.Locale.US;
	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

	private new() {
		// Utility class.
	}

	/**
	 * Returns given run time as a human-readable string. It is shown in a convenient unit, e.g., 
	 * instead of 5000000 ms, 1.39 h is shown.
	 * If the run time is converted to another unit, the duration in ms is shown after between parentheses (e.g. {@code 2.5 s (2504 ms)}).
	 * 
	 * @param runtimeMs Run time in milliseconds
	 */
	def static displayRuntime(long runtimeMs) {
		if (runtimeMs < 2_000) {
			return '''«runtimeMs» ms''';
		} else if (runtimeMs < 100_000) {
			return '''«String.format(LOCALE,"%.2f", runtimeMs / 1000.0)» s («thousandsSeparated(runtimeMs)» ms)''';
		} else if (runtimeMs < 400_000) {
			return '''«String.format(LOCALE,"%.2f", runtimeMs / (1000.0 * 60.0))» min («thousandsSeparated(runtimeMs)» ms)''';
		} else {
			return '''«String.format(LOCALE,"%.2f", runtimeMs / (1000.0 * 60.0 * 60.0))» h («thousandsSeparated(runtimeMs)» ms)''';
		}
	}
	
	private def static thousandsSeparated(long number) {
		// http://stackoverflow.com/questions/5323502/how-to-set-thousands-separator-in-java
		val DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
		symbols.setGroupingSeparator(' ');

		val DecimalFormat formatter = new DecimalFormat("###,###", symbols);
		return formatter.format(number);
	}
	
	/**
	 * Inserts the given separators to create groups from the right with the given lengths.
	 * For example, if the given string is {@code 1234}, group size is 3, separator 
	 * character is {@code _}, then the result will be {@code 1_234}.
	 * @param str String to be modified to contain separators. Shall not be {@code null}.
	 * @param groupSize Length of the groups between two separators. Shall be at least one.
	 * @param separator Separator character to be inserted.
	 * @return The string with separators inserted.
	 */
	def static String separatedString(String str, int groupSize, char separator) {
		Preconditions.checkNotNull(str, "String should not be null.");
		Preconditions.checkArgument(groupSize >= 1, "Group size shall be at least 1.")
		
		var nextGroupSize = if (str.length % groupSize == 0) Math.min(groupSize, str.length) else str.length % groupSize;
		val StringBuilder result = new StringBuilder(str.substring(0, nextGroupSize));
		var i = nextGroupSize;
		
		while (i < str.length) {
			result.append(separator);
			result.append(str.substring(i, Math.min(i + groupSize, str.length)));
			i += groupSize;
		}
		
		return result.toString();
	}
}
