/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.logging;

public enum PlcverifSeverity {
	Error(3), Warning(2), Info(1), Debug(0);
	
	/**
	 * Relative severity number. Higher number means more severe problem.
	 */
	private int severityNumber;
	
	PlcverifSeverity(int severityNumber) {
		this.severityNumber = severityNumber;
	}
	
	public boolean isAtLeast(PlcverifSeverity severity) {
		return this.severityNumber >= severity.severityNumber;
	}
}
