/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.google.common.base.Preconditions;

/**
 * Utility class that can replace the placeholders in a text (string or list of
 * strings) with concrete values (e.g. {@code "--depth ${depth}"}).
 * <p>
 * The placeholder for key '{@code K}' should be in the format '$&#123;K&#125;'.
 */
public abstract class PlaceholderReplacement {
	private static final String PLACEHOLDER_REGEX = "\\$\\{.+\\}";

	public static class NonReplacedPlaceholderException extends RuntimeException {
		private static final long serialVersionUID = 8261931345865813145L;

		public NonReplacedPlaceholderException(String message) {
			super(message);
		}
	}

	private static final class StringPlaceholderReplacement extends PlaceholderReplacement {
		private String str;

		private StringPlaceholderReplacement(String str) {
			this.str = str;
		}

		public StringPlaceholderReplacement replace(String key, String value) {
			Preconditions.checkArgument(value != null, "Replacement value should not be null.");

			String placeholder = placeholderFormat(key);
			str = str.replace(placeholder, value);
			return this;
		}

		@Override
		public String toText() {
			throwIfContainsPlaceholder(str);
			return str;
		}

		@Override
		public List<String> toList() {
			throwIfContainsPlaceholder(str);
			return Collections.singletonList(str);
		}
	}

	private static final class ListPlaceholderReplacement extends PlaceholderReplacement {
		private List<String> list;

		private ListPlaceholderReplacement(List<String> list) {
			this.list = new ArrayList<>(list);
		}

		@Override
		public ListPlaceholderReplacement replace(String key, String value) {
			Preconditions.checkArgument(value != null, "Replacement value should not be null.");

			String placeholder = placeholderFormat(key);

			for (int i = 0; i < list.size(); i++) {
				if (list.get(i) != null) {
					list.set(i, list.get(i).replace(placeholder, value));
				}
			}
			return this;
		}

		@Override
		public String toText() {
			list.forEach(str -> throwIfContainsPlaceholder(str));
			return String.join(" ", list);
		}

		@Override
		public List<String> toList() {
			list.forEach(str -> throwIfContainsPlaceholder(str));
			return Collections.unmodifiableList(list);
		}
	}
	
	/**
	 * Defines a text with placeholders that will be modified.
	 */
	public static PlaceholderReplacement text(String text) {
		return new StringPlaceholderReplacement(text);
	}

	/**
	 * Defines a list with placeholders that will be modified. The original list
	 * ({@code list}) object will not be altered.
	 */
	public static PlaceholderReplacement list(List<String> list) {
		return new ListPlaceholderReplacement(list);
	}

	/**
	 * Replaces the occurrences of placeholders having key {@code key} with
	 * {@code value} in the text to be modified.
	 * <p>
	 * 
	 * @param key
	 *            Placeholder key to be replaced.
	 * @param value
	 *            Value to be used in replacement. Shall not be null.
	 * @throws IllegalArgumentException
	 *             if the value is null
	 */
	public abstract PlaceholderReplacement replace(String key, String value);

	/**
	 * Replaces the occurrences of placeholders having key {@code key} with
	 * {@code value} in the text to be modified, if {@code value} is not
	 * {@code null}.
	 */
	public PlaceholderReplacement replaceIfNotNull(String key, String value) {
		if (value != null) {
			return replace(key, value);
		} else {
			return this;
		}
	}

	/**
	 * Replaces the occurrences of {@code map}'s keys with the corresponding
	 * values in the text to be modified.
	 */
	public PlaceholderReplacement replace(Map<String, String> map) {
		for (Map.Entry<String, String> entry : map.entrySet()) {
			replace(entry.getKey(), entry.getValue());
		}
		return this;
	}

	/**
	 * Returns the final text as string.
	 * 
	 * @throws NonReplacedPlaceholderException
	 *             if the text still contains placeholders.
	 */
	public abstract String toText();

	/**
	 * Returns the final text as list.
	 * 
	 * @throws NonReplacedPlaceholderException
	 *             if the text still contains placeholders.
	 */
	public abstract List<String> toList();

	@Override
	public String toString() {
		return toText();
	}

	// Internal helpers
	private static String placeholderFormat(String key) {
		return String.format("${%s}", key);
	}

	private static void throwIfContainsPlaceholder(String str) {
		if (str.matches(String.format(".*%s.*", PLACEHOLDER_REGEX))) {
			throw new NonReplacedPlaceholderException(str + " still contains placeholder(s).");
		}
	}
}
