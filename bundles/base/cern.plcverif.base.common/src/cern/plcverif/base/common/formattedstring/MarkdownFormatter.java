/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.formattedstring;

import cern.plcverif.base.common.formattedstring.Formats.BoldFormat;
import cern.plcverif.base.common.formattedstring.Formats.ColorFormat;
import cern.plcverif.base.common.formattedstring.Formats.ItalicFormat;

/**
 * Formatter to represent a {@linkplain BasicFormattedString basic formatted
 * string} in Markdown.
 */
public class MarkdownFormatter extends Formatter {
	@Override
	public String formatPrefix(BoldFormat f) {
		return "*";
	}

	@Override
	public String formatSuffix(BoldFormat f) {
		return "*";
	}

	@Override
	public String formatPrefix(ItalicFormat f) {
		return "_";
	}

	@Override
	public String formatSuffix(ItalicFormat f) {
		return "_";
	}

	@Override
	public String formatPrefix(ColorFormat f) {
		// Not supported
		return "";
	}

	@Override
	public String formatSuffix(ColorFormat f) {
		// Not supported
		return "";
	}

	@Override
	public String stringPrefix() {
		return "";
	}

	@Override
	public String stringSuffix() {
		return "";
	}
	
	@Override
	public String sanitizeText(String string) {
		return sanitize(string);
	}
	
	public static String sanitize(String string) {
		return string.replace("\n", "\n\n").replace("<", "&lt;");
	}
}
