/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.settings.specific;

import static com.google.common.base.Preconditions.checkNotNull;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsKind;
import cern.plcverif.base.common.settings.SettingsSerializer;
import cern.plcverif.base.common.settings.SettingsUtils;
import cern.plcverif.base.common.settings.exceptions.SettingsAnnotationException;
import cern.plcverif.base.common.settings.exceptions.SettingsInternalException;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.exceptions.SettingsSerializerException;
import cern.plcverif.base.common.settings.help.SettingsHelp;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsComposite;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsElement;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsList;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory;

/**
 * Utility class to parse and serialize specific settings from/to settings.
 */
public final class SpecificSettingsSerializer {
	private static final String ROOT_PREFIX = "__root";
	
	private SpecificSettingsSerializer() {
		// Utility class.
	}

	public static <T extends AbstractSpecificSettings> SettingsElement toGenericSettings(T specificSettings)
			throws SettingsSerializerException, SettingsParserException {
		return toGenericSettings(specificSettings, Optional.empty(), true);
	}

	public static <T extends AbstractSpecificSettings> SettingsElement toGenericSettings(T specificSettings, T defaultSettings)
			throws SettingsSerializerException, SettingsParserException {
		Preconditions.checkNotNull(defaultSettings, "defaultSettings");
		
		return toGenericSettings(specificSettings, Optional.of(defaultSettings), true);
	}

	public static <T extends AbstractSpecificSettings> SettingsElement toGenericSettings(T specificSettings,
			Optional<T> defaultSettings, boolean strict) throws SettingsSerializerException, SettingsParserException {
		Preconditions.checkNotNull(specificSettings, "specificSettings");
		Preconditions.checkNotNull(defaultSettings, "defaultSettings");
		
		String specificSettingsString = SpecificSettingsSerializer.serialize(specificSettings, ROOT_PREFIX,
				Optional.empty(), strict);
		if (specificSettingsString != null && specificSettingsString.isEmpty()) {
			return SettingsElement.empty();
		}
		Settings settings = SettingsSerializer.parseSettingsFromString(specificSettingsString);
		if (settings.kind() == SettingsKind.ELEMENT) {
			Optional<Settings> attrib = settings.toSingle().getAttribute(ROOT_PREFIX);
			if (!attrib.isPresent()) {
				throw new SettingsInternalException("The given prefix is not found. This is an internal error.");
			}
			if (attrib.get() instanceof SettingsElement) {
				return (SettingsElement) attrib.get();				
			} else {
				throw new SettingsInternalException("The generated settings has unexpected type, the settings node to be returned is not a SettingsElement. This is an internal error.");
			}
		} else {
			throw new SettingsInternalException("The generated settings has unexpected type. This is an internal error.");
		}
	}

	public static <T extends AbstractSpecificSettings> String serialize(T settings) throws SettingsSerializerException {
		return serialize(settings, ROOT_PREFIX, Optional.empty(), true);
	}

	public static <T extends AbstractSpecificSettings> String serialize(T settings, T defaultSettings)
			throws SettingsSerializerException {
		return serialize(settings, ROOT_PREFIX, Optional.of(defaultSettings), true);
	}

	public static <T extends AbstractSpecificSettings> String serialize(T settings, String prefix)
			throws SettingsSerializerException {
		return serialize(settings, prefix, Optional.empty(), true);
	}

	/**
	 * Returns the string settings representation of the given specific settings
	 * with the given prefix.
	 * 
	 * @param settings
	 *            The specific settings to be represented as string. Should not
	 *            be null.
	 * @param prefix
	 *            The prefix used for serialization. If empty string and there
	 *            is a root value to be serialized ({@code settings.getValue()}
	 *            is not null), it will be replaced with {@value #ROOT_PREFIX}. Should
	 *            not be null.
	 * @param defaultSettings
	 *            Description of the default settings. If given (i.e. not
	 *            {@link Optional#empty()}), only the values different from the
	 *            ones in {@code defaultSettings} will be serialized.
	 * @param strict
	 *            If true, exception is thrown if a mandatory element is not
	 *            provided. If false, even mandatory elements may be omitted
	 *            from the output if they are not set.
	 * @return The string settings representation of {@code settings}.
	 * @throws SettingsSerializerException
	 *             If error occurred during serialization.
	 */
	@SuppressWarnings("unchecked")
	public static <T extends AbstractSpecificSettings> String serialize(T settings, String prefix,
			Optional<T> defaultSettings, boolean strict) throws SettingsSerializerException {
		Preconditions.checkNotNull(settings, "settings");
		Preconditions.checkNotNull(prefix, "prefix");
		Preconditions.checkNotNull(defaultSettings, "defaultSettings");

		String newPrefix = prefix;
		if (settings.getValue() != null && "".equals(prefix)) {
			// We should avoid the "" prefix as this will cause troubles with
			// the root value (settings.getValue).
			newPrefix = ROOT_PREFIX;
		}

		try {
			StringBuilder builder = new StringBuilder();
			serialize(settings, (Class<T>) settings.getClass(), defaultSettings, newPrefix, strict, builder);
			return builder.toString();
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new SettingsSerializerException("Error occurred during settings serialization.", e);
		}
	}

	private static <T extends AbstractSpecificSettings> void serialize(T settings, Class<? extends T> settingsClass,
			Optional<T> defaultSettings, String prefix, boolean strict, StringBuilder builder)
			throws IllegalAccessException, SettingsSerializerException {
		String fullPrefix = ("".equals(prefix) || prefix.endsWith(".")) ? prefix : prefix + ".";

		if (settings.getValue() != null) {
			// remove trailing dots if any
			String prefixWithoutLastDots = withoutTrailingDots(prefix);
			builder.append(String.format("-%s = \"%s\"%n", prefixWithoutLastDots, settings.getValue()));
		}

		for (Field field : fieldsInClassAndItsSupers(settingsClass)) {
			field.setAccessible(true);
			if (field.isAnnotationPresent(PlcverifSettingsElement.class)) {
				PlcverifSettingsElement annotation = field.getAnnotation(PlcverifSettingsElement.class);
				serializeElement(settings, field, annotation, fullPrefix, defaultSettings, strict, builder);
			} else if (field.isAnnotationPresent(PlcverifSettingsList.class)) {
				PlcverifSettingsList annotation = field.getAnnotation(PlcverifSettingsList.class);
				serializeList(settings, field, annotation, fullPrefix, defaultSettings, strict, builder);
			} else if (field.isAnnotationPresent(PlcverifSettingsComposite.class)) {
				PlcverifSettingsComposite annotation = field.getAnnotation(PlcverifSettingsComposite.class);
				serializeComposite(settings, field, annotation, fullPrefix, defaultSettings, strict, builder);
			}
			field.setAccessible(false);
		}
	}

	private static String withoutTrailingDots(String str) {
		String ret = str;
		while (ret.endsWith(".")) {
			ret = ret.substring(0, ret.length() - 1);
		}
		return ret;
	}

	private static <T extends AbstractSpecificSettings> void serializeElement(T obj, Field field,
			PlcverifSettingsElement annotation, String prefix, Optional<T> defaultSettings, boolean strict,
			StringBuilder builder)
			throws IllegalAccessException, SettingsSerializerException {
		Preconditions.checkNotNull(obj, "obj");
		Preconditions.checkNotNull(field, "field");
		Preconditions.checkNotNull(annotation, "annotation");
		Preconditions.checkNotNull(builder, "builder");
		Preconditions.checkNotNull(prefix, "prefix");
		Preconditions.checkArgument("".equals(prefix) || prefix.endsWith("."), "Incorrect prefix format.");

		Object objValue = field.get(obj);
		if (objValue == null) {
			if (strict) {
				checkOptionalOrPresent(objValue, annotation.mandatory(), annotation.name());
			}
			return;
		}

		Preconditions.checkState(objValue != null);

		// Check if it is the default value, in which case no need for
		// serialization
		if (defaultSettings.isPresent()) {
			Object defValue = field.get(defaultSettings.get());
			if (defValue != null && defValue.equals(objValue)) {
				// 'objValue' is the default value, doesn't need to be
				// serialized
				return;
			}
		}

		String strValue = objValue.toString();

		builder.append(String.format("-%s%s = \"%s\"%n", prefix, annotation.name(), strValue));
	}

	private static <T extends AbstractSpecificSettings> void serializeList(T obj, Field field,
			PlcverifSettingsList annotation, String prefix, Optional<T> defaultSettings, boolean strict,
			StringBuilder builder)
			throws IllegalAccessException, SettingsSerializerException {
		Preconditions.checkNotNull(obj, "obj");
		Preconditions.checkNotNull(field, "field");
		Preconditions.checkNotNull(annotation, "annotation");
		Preconditions.checkNotNull(builder, "builder");
		Preconditions.checkNotNull(prefix, "prefix");
		Preconditions.checkArgument("".equals(prefix) || prefix.endsWith("."), "Incorrect prefix format.");

		Object objValue = field.get(obj);
		if (objValue == null) {
			if (strict) {
				checkOptionalOrPresent(objValue, annotation.mandatory(), annotation.name());
			}
			return;
		}
		Preconditions.checkState(objValue != null);

		if (!(objValue instanceof List<?>)) {
			throw new SettingsSerializerException(
					"Unexpected type for the field '%s' representing '%s': %s. Expected type: List<String>.",
					field.getName(), annotation.name(), objValue.getClass().getName());
		}
		List<?> list = (List<?>) objValue;

		// Check if it is the default value, in which case no need for
		// serialization
		if (defaultSettings.isPresent()) {
			Object defValue = field.get(defaultSettings.get());
			if (defValue != null && (defValue instanceof List<?>) && listEquals((List<?>) defValue, list)) {
				// 'objValue' is the default value, don't need to be serialized
				return;
			}
		}

		for (int i = 0; i < list.size(); i++) {
			if (list.get(i) == null) {
				continue;
			}
			String strValue = list.get(i).toString();
			builder.append(
					String.format("-%s%s.%s = \"%s\"%n", prefix, annotation.name(), Integer.toString(i), strValue));
		}

		if (list.isEmpty() && annotation.serializeIfEmpty()) {
			builder.append(String.format("-%s%s = {}%n", prefix, annotation.name()));
		}
	}

	@SuppressWarnings("unchecked")
	private static <T extends AbstractSpecificSettings> void serializeComposite(T obj, Field field,
			PlcverifSettingsComposite annotation, String prefix, Optional<T> defaultSettings, boolean strict,
			StringBuilder builder)
			throws IllegalAccessException, SettingsSerializerException {
		Preconditions.checkNotNull(obj, "obj");
		Preconditions.checkNotNull(field, "field");
		Preconditions.checkNotNull(annotation, "annotation");
		Preconditions.checkNotNull(builder, "builder");
		Preconditions.checkNotNull(prefix, "prefix");
		Preconditions.checkArgument("".equals(prefix) || prefix.endsWith("."), "Incorrect prefix format.");

		String newPrefix = String.format("%s%s.", prefix, annotation.name());
		Object objValue = field.get(obj);
		if (objValue == null) {
			if (strict) {
				checkOptionalOrPresent(objValue, annotation.mandatory(), annotation.name());
			}
			return;
		}
		Preconditions.checkState(objValue != null);

		if (!(objValue instanceof AbstractSpecificSettings)) {
			throw new SettingsSerializerException(
					"Unexpected type for the field '%s' representing '%s': %s. Expected type: AbstractSpecificSettings.",
					field.getName(), annotation.name(), objValue.getClass().getName());
		}

		Object defValue = null;
		if (defaultSettings.isPresent() && field.get(defaultSettings.get()) != null) {
			defValue = field.get(defaultSettings.get());
			if (!(defValue instanceof AbstractSpecificSettings)) {
				throw new SettingsSerializerException(
						"Unexpected type for the default value of field '%s' representing '%s': %s. Expected type: AbstractSpecificSettings.",
						field.getName(), annotation.name(), defValue.getClass().getName());
			}
		}

		serialize((AbstractSpecificSettings) objValue, (Class<? extends AbstractSpecificSettings>) objValue.getClass(),
				Optional.ofNullable((AbstractSpecificSettings) defValue), newPrefix, strict, builder);
	}

	/**
	 * Parses the given {@link SettingsElement} into the given specific settings
	 * class ({@code specSettingsClass}).
	 * 
	 * @param settings
	 *            Settings to parse.
	 * @param specSettingsClass
	 *            Specific settings class to be created.
	 * @return The specific settings class instance representing the given
	 *         settings. This value is never null.
	 * @throws SettingsParserException
	 *             if it was not possible to represent the given settings. This
	 *             may be due to incompatible data types, missing mandatory
	 *             parameters, etc.
	 */
	public static <T extends AbstractSpecificSettings> T parse(SettingsElement settings, Class<T> specSettingsClass)
			throws SettingsParserException {
		Preconditions.checkNotNull(settings, "settings");
		Preconditions.checkNotNull(specSettingsClass, "specSettingsClass");

		try {
			T result = parseInternal(settings, specSettingsClass);
			return Preconditions.checkNotNull(result);
		} catch (InstantiationException e) {
			throw new SettingsParserException(e, "Unable to instantiate the specific setting class.");
		} catch (IllegalAccessException e) {
			throw new SettingsParserException(e, "Illegal access during settings parsing.");
		} catch (NoSuchFieldException e) {
			throw new SettingsParserException(e, "Field not found during settings parsing.");
		} catch (SecurityException e) {
			throw new SettingsParserException(e, "Security exception during settings parsing.");
		}
	}

	private static <T extends AbstractSpecificSettings> T parseInternal(SettingsElement settings,
			Class<T> specSettingsClass) throws InstantiationException, IllegalAccessException, NoSuchFieldException,
			SettingsParserException {

		T ret;
		try {
			ret = specSettingsClass.getDeclaredConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException ex) {
			// Rewrap the exception to facilitate debugging
			throw new SettingsParserException(ex, "Unable to instantiate %s to parse the settings.",
					specSettingsClass.getSimpleName());
		} catch (NoSuchMethodException | InvocationTargetException e) {
			throw new SettingsParserException(e, "Unable to instantiate %s to parse the settings.",
					specSettingsClass.getSimpleName());
		} 

		// save the original
		Field repr = AbstractSpecificSettings.class.getDeclaredField("representedSettingsElement");
		repr.setAccessible(true);
		repr.set(ret, settings);

		// save the value
		ret.setValue(settings.value());

		for (Field field : fieldsInClassAndItsSupers(specSettingsClass)) {
			field.setAccessible(true);
			if (field.isAnnotationPresent(PlcverifSettingsElement.class)) {
				PlcverifSettingsElement annotation = field.getAnnotation(PlcverifSettingsElement.class);
				parseElementField(field, annotation, settings.getAttribute(annotation.name()), ret);
			} else if (field.isAnnotationPresent(PlcverifSettingsList.class)) {
				PlcverifSettingsList annotation = field.getAnnotation(PlcverifSettingsList.class);
				parseListField(field, annotation, settings.getAttribute(annotation.name()), ret);
			}

			if (field.isAnnotationPresent(PlcverifSettingsComposite.class)) {
				PlcverifSettingsComposite annotation = field.getAnnotation(PlcverifSettingsComposite.class);
				Optional<Settings> settingsNode = settings.getAttribute(annotation.name());
				if (!settingsNode.isPresent() && annotation.mandatory() == PlcverifSettingsMandatory.OPTIONAL) {
					continue;
				}
				checkIsPresent(settingsNode, settings, annotation.name());
				parseCompositeField(field, annotation, settingsNode, ret);

				Optional<Settings> attribute = settings.getAttribute(annotation.name());
				if (!AbstractSpecificSettings.class.isAssignableFrom(field.getType())) {
					throw new SettingsParserException(
							"The field '%s' is annotated as a complex element but has a type '%s' that is not a subclass of AbstractSpecificSettings.",
							field.getName(), field.getType().getName());
				} else {
					@SuppressWarnings("unchecked")
					Class<? extends AbstractSpecificSettings> compositeSpecificSettingsType = (Class<? extends AbstractSpecificSettings>) field
							.getType();
					if (!attribute.isPresent()) {
						throw new SettingsParserException("The setting '%s' is not found.", annotation.name());
					}
					if (attribute.get().kind() == SettingsKind.ELEMENT) {
						AbstractSpecificSettings subSettings = parse(attribute.get().toSingle(),
								compositeSpecificSettingsType);
						field.set(ret, subSettings);
					}
				}
			}
			field.setAccessible(false);
		}

		return ret;
	}

	/**
	 * Returns all declared fields in the given class and all its superclasses.
	 * The fields in the list keep the order of
	 * {@link Class#getDeclaredFields()}, from the most generic class towards
	 * {@code clazz}.
	 */
	private static List<Field> fieldsInClassAndItsSupers(Class<?> clazz) {
		List<Field> fields = new ArrayList<>();
		fieldsInClassAndItsSupers(clazz, fields);
		return fields;
	}

	private static void fieldsInClassAndItsSupers(Class<?> clazz, List<Field> accumulator) {
		if (clazz.getSuperclass() != null) {
			fieldsInClassAndItsSupers(clazz.getSuperclass(), accumulator);
		}
		for (Field f : clazz.getDeclaredFields()) {
			accumulator.add(f);
		}
	}

	private static void parseElementField(Field field, PlcverifSettingsElement annotation, Optional<Settings> attribute,
			AbstractSpecificSettings ret) throws SettingsParserException {
		if (!attribute.isPresent()) {
			// value not present
			if (annotation.mandatory() != PlcverifSettingsMandatory.OPTIONAL) {
				throw new SettingsParserException("The mandatory element '%s' is missing in the settings file.",
						annotation.name());
			} else {
				// nothing to do here, OPTIONAL value can be omitted
			}
		} else if (attribute.get().kind() == SettingsKind.ELEMENT) {
			try {
				setWithTypeConversion(ret, field, attribute.get().toSingle().value(), attribute.get().fqn());
			} catch (IllegalArgumentException e) {
				throw new SettingsParserException(e, "Unable to set the field '%s' representing '%s'.", field.getName(),
						annotation.name());
			}
		} else {
			throw new SettingsParserException(
					"The setting '%s' is defined as element, but the corresponding attribute is the settings to be parsed is not an element.",
					annotation.name());
		}
	}

	private static void parseListField(Field field, PlcverifSettingsList annotation, Optional<Settings> attribute,
			AbstractSpecificSettings ret) throws SettingsParserException {
		if (field.getType() != List.class) {
			throw new SettingsParserException(
					"The field '%s' representing attribute '%s' should be a list, but its type is '%s'.",
					field.getName(), annotation.name(), field.getType());
		}

		if (!attribute.isPresent()) {
			if (annotation.mandatory() == PlcverifSettingsMandatory.MANDATORY) {
				throw new SettingsParserException("The attribute '%s' has not been found in the settings file.",
						annotation.name());
			} else {
				// nothing to do, leave default value
			}
		} else if (attribute.get().kind() == SettingsKind.LIST) {
			List<String> list = new ArrayList<>();
			for (Settings settingsElement : attribute.get().toList().elements()) {
				if (settingsElement == null) {
					list.add(null);
				} else if (settingsElement.kind() == SettingsKind.ELEMENT) {
					list.add(settingsElement.toSingle().value());
				} else {
					throw new SettingsParserException("Parsing a list of lists is not supported yet.");
				}
			}
			setListField(ret, field, list);
		} else if (attribute.get().kind() == SettingsKind.ELEMENT) {
			// List with one item
			List<String> list = new ArrayList<>();
			list.add(attribute.get().toSingle().value());
			setListField(ret, field, list);
		} else {
			throw new SettingsParserException("Unhandled settings attribute kind: '%s'.", attribute.get().kind());
		}
	}

	private static void parseCompositeField(Field field, PlcverifSettingsComposite annotation,
			Optional<Settings> attribute, AbstractSpecificSettings ret) throws SettingsParserException {
		Preconditions.checkNotNull(field, "field");
		Preconditions.checkNotNull(annotation, "annotation");
		Preconditions.checkNotNull(attribute, "attribute");
		Preconditions.checkNotNull(ret, "ret");

		if (!AbstractSpecificSettings.class.isAssignableFrom(field.getType())) {
			throw new SettingsParserException(
					"The type of the composite field '%s' is not a subclass of AbstractSpecificSettings.",
					field.getName());
		} else if (!attribute.isPresent()) {
			if (annotation.mandatory() == PlcverifSettingsMandatory.MANDATORY) {
				throw new SettingsParserException("The attribute '%s' has not been found in the settings file.",
						annotation.name());
			} else {
				// nothing to do, leave default value
			}
		} else {
			@SuppressWarnings("unchecked")
			Class<? extends AbstractSpecificSettings> compositeSpecificSettingsType = (Class<? extends AbstractSpecificSettings>) field
					.getType();
			if (attribute.get().kind() == SettingsKind.ELEMENT) {
				AbstractSpecificSettings subSettings = parse(attribute.get().toSingle(), compositeSpecificSettingsType);
				try {
					field.set(ret, subSettings);
				} catch (IllegalArgumentException | IllegalAccessException e) {
					throw new SettingsParserException(e,
							"Unable to set the value of the field '%s' representing the attribute '%s'.",
							field.getName(), annotation.name());
				}
			}
		}
	}

	/**
	 * Fills the given settings help object to represent the given specific
	 * settings class.
	 * <p>
	 * This method tries to determine the default values defined in
	 * {@code settingsClass} and to include them in the help if possible.
	 * 
	 * @param help
	 *            The help object to be filled. Shall not be null.
	 * @param settingsClass
	 *            The specific settings class to be represented. Shall not be
	 *            null.
	 */
	public static <T extends AbstractSpecificSettings> void fillSettingsHelp(SettingsHelp help,
			Class<T> settingsClass) {
		// Try to determine the default values
		T defaults = null;
		try {
			defaults = settingsClass.getDeclaredConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException |  InvocationTargetException | NoSuchMethodException e) {
			// best effort, do nothing and keep on going with defaults = null
		}

		fillSettingsHelpInternal(help, settingsClass, defaults);
	}

	/**
	 * Fills the given settings help object to represent the given specific
	 * settings class. The default values are determined based on the current
	 * values of the fields in the given {@code defaults} object.
	 * 
	 * @param help
	 *            The help object to be filled. Shall not be null.
	 * @param settingsClass
	 *            The specific settings class to be represented. Shall not be
	 *            null.
	 * @param defaults
	 *            Settings object containing the default values. Shall not be
	 *            null.
	 */
	public static <T extends AbstractSpecificSettings> void fillSettingsHelp(SettingsHelp help, Class<T> settingsClass,
			T defaults) {
		fillSettingsHelpInternal(checkNotNull(help, "help"), checkNotNull(settingsClass, "settingsClass"),
				checkNotNull(defaults, "defaults"));
	}

	/**
	 * Fills the given settings help object to represent the given specific
	 * settings class. The default values are determined based on the current
	 * values of the fields in the given {@code defaults} object. If the
	 * {@code defaults} is {@code null}, the default settings will not be
	 * defined in the settings help.
	 * 
	 * @param help
	 *            The help object to be filled. Shall not be null.
	 * @param settingsClass
	 *            The specific settings class to be represented. Shall not be
	 *            null.
	 * @param defaults
	 *            Settings object containing the default values. May be null.
	 */
	private static <T extends AbstractSpecificSettings> void fillSettingsHelpInternal(SettingsHelp help,
			Class<T> settingsClass, T defaults) {
		Preconditions.checkNotNull(help);
		Preconditions.checkNotNull(settingsClass);
		// 'defaults' may be null

		for (Field field : fieldsInClassAndItsSupers(settingsClass)) {
			fillSettingsHelpSingleParameter(help, settingsClass, field, defaults, null);
		}
	}

	/**
	 * Fills the given settings help object to represent the given setting
	 * element (having the name {@code settingName}) of the given specific
	 * settings class. If defined, the {@code valueSet} parameter will be used
	 * to define the acceptable values of the setting element.
	 * 
	 * @param help
	 *            The help object to be filled. Shall not be null.
	 * @param settingsClass
	 *            The specific settings class that contains the field to be
	 *            represented. Shall not be null.
	 * @param settingName
	 *            The name of the setting element that is to be represented.
	 *            Shall not be null.
	 * @param valueSet
	 *            Set of acceptable values for the field to be defined. May be
	 *            null.
	 */
	public static <T extends AbstractSpecificSettings> void fillSettingsHelpSingleParameter(SettingsHelp help,
			Class<T> settingsClass, String settingName, List<String> valueSet) {
		Preconditions.checkNotNull(settingsClass);

		for (Field field : fieldsInClassAndItsSupers(settingsClass)) {
			if (hasPlcverifAnnotationWithGivenName(field, settingName)) {
				fillSettingsHelpSingleParameter(help, settingsClass, field, null, valueSet);
				return;
			}
		}
	}

	/**
	 * Fills the given settings help object to represent the given setting
	 * element (represented by the field {@code field} in {@code settingsClass})
	 * of the given specific settings class. If defined, the
	 * {@code forcedValueSet} parameter will be used to define the acceptable
	 * values of the setting element, otherwise it will be automatically
	 * determined for enumeration types.
	 * 
	 * @param help
	 *            The help object to be filled. Shall not be null.
	 * @param settingsClass
	 *            The specific settings class that contains the field to be
	 *            represented. Shall not be null.
	 * @param field
	 *            The field of the setting element that is to be represented.
	 *            Shall not be null.
	 * @param defaultSettings
	 *            Object to be used to acquire the default value for the field
	 *            to be represented. May be null.
	 * @param forcedValueSet
	 *            Set of acceptable values for the field to be defined. May be
	 *            null.
	 */
	@SuppressWarnings("unchecked")
	private static <T extends AbstractSpecificSettings> void fillSettingsHelpSingleParameter(SettingsHelp help,
			Class<T> settingsClass, Field field, T defaultSettings, List<String> forcedValueSet) {
		Preconditions.checkNotNull(help);
		Preconditions.checkNotNull(settingsClass);
		Preconditions.checkNotNull(field);

		// Try to determine the default values
		String defaultValueStr = null;
		if (defaultSettings != null) {
			try {
				// Fetch from the corresponding field of the given object
				// ('defaultSettings')
				Object defaultValueObj = field.get(defaultSettings);
				
				// Represent the default value
				if (defaultValueObj == null) {
					defaultValueStr = null;
				} else if (defaultValueObj instanceof List<?>) {
					// list value
					if (((List<?>) defaultValueObj).isEmpty()) {
						defaultValueStr = null;
					} else {
						// non-empty list
						defaultValueStr = defaultValueObj.toString();
						if (defaultValueStr.startsWith("[") && defaultValueStr.endsWith("]")) {
							defaultValueStr = String.format("{%s}", defaultValueStr.substring(1, defaultValueStr.length() - 1));
						}
					}
				} else {
					// atomic value
					defaultValueStr = defaultValueObj.toString();
				}
			} catch (IllegalArgumentException | IllegalAccessException e) {
				// unable to determine the default value, do nothing
			}
		}
		// defaultValueStr may be null, in that case the default value will not
		// be defined

		List<String> valueSet = forcedValueSet;
		if (field.isAnnotationPresent(PlcverifSettingsElement.class)) {
			PlcverifSettingsElement annotation = field.getAnnotation(PlcverifSettingsElement.class);
			// If 'forcedValueSet' does not define explicitly the allowed
			// values, try to determine it based on the type
			if (field.getType().isEnum() && forcedValueSet == null) {
				valueSet = Arrays.stream(field.getType().getEnumConstants()).map(it -> it.toString())
						.collect(Collectors.toList());
			} else if (field.getType().equals(Boolean.TYPE)) {
				valueSet = Arrays.asList(SettingsUtils.TRUE_STRING, SettingsUtils.FALSE_STRING);
			}
			help.addSettingElement(annotation.name(), annotation.description(), field.getType(), annotation.mandatory(),
					valueSet, defaultValueStr);
		} else if (field.isAnnotationPresent(PlcverifSettingsList.class)) {
			PlcverifSettingsList annotation = field.getAnnotation(PlcverifSettingsList.class);
			help.addSettingElement(annotation.name(), annotation.description(), field.getType(), annotation.mandatory(),
					valueSet, defaultValueStr);
		} else if (field.isAnnotationPresent(PlcverifSettingsComposite.class)) {
			PlcverifSettingsComposite annotation = field.getAnnotation(PlcverifSettingsComposite.class);
			help.addSettingElement(annotation.name(), annotation.description(), field.getType(), annotation.mandatory(),
					valueSet, defaultValueStr);
			help.startPrefix(annotation.name());
			if (defaultSettings != null) {
				try {
					fillSettingsHelp(help, (Class<AbstractSpecificSettings>) field.getType(), (AbstractSpecificSettings) field.get(defaultSettings));
				} catch (IllegalArgumentException | IllegalAccessException e) {
					// Unable to determine the default settings, move on without them.
					fillSettingsHelp(help, (Class<AbstractSpecificSettings>) field.getType());
				}
			} else {
				fillSettingsHelp(help, (Class<AbstractSpecificSettings>) field.getType());
			}
			help.endPrefix();
		}
	}

	/**
	 * Returns true iff the given field has a {@code PlcverifSettingsXxx}
	 * annotation with the given {@code name}.
	 */
	private static boolean hasPlcverifAnnotationWithGivenName(Field field, String name) {
		String nameInAnnotation = null;
		if (field.isAnnotationPresent(PlcverifSettingsElement.class)) {
			nameInAnnotation = field.getAnnotation(PlcverifSettingsElement.class).name();
		} else if (field.isAnnotationPresent(PlcverifSettingsList.class)) {
			nameInAnnotation = field.getAnnotation(PlcverifSettingsList.class).name();
		} else if (field.isAnnotationPresent(PlcverifSettingsComposite.class)) {
			nameInAnnotation = field.getAnnotation(PlcverifSettingsComposite.class).name();
		}

		return name.equals(nameInAnnotation);
	}

	// Misc. helper methods
	private static void checkOptionalOrPresent(Object value, PlcverifSettingsMandatory isMandatory, String elementName)
			throws SettingsSerializerException {
		if (value == null && isMandatory != PlcverifSettingsMandatory.OPTIONAL) {
			throw new SettingsSerializerException("Mandatory element '%s' shall not be null.", elementName);
		}
	}

	private static void checkIsPresent(Optional<Settings> settingsNode, SettingsElement settings, String nodeName)
			throws SettingsParserException {
		if (!settingsNode.isPresent()) {
			throw new SettingsParserException("Required attribute has not been found: " + settings.fqn() + nodeName);
		}
	}

	private static void setListField(Object obj, Field field, List<String> value) throws SettingsParserException {
		try {
			if (Modifier.isFinal(field.getModifiers())) {
				throw new SettingsParserException(
						String.format("The list field '%s' to be set is final.", field.getName()));
			}
			field.set(obj, value);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new SettingsParserException(e, "Unable to set the value of the list field '%s'.", field.getName());
		}
	}

	private static void setWithTypeConversion(Object obj, Field field, String value, String settingsElementName)
			throws SettingsParserException {
		try {
			if (Modifier.isFinal(field.getModifiers())) {
				throw new SettingsParserException(String.format("The field '%s' to be set is final.", field.getName()));
			}

			if (field.getType() == String.class) {
				field.set(obj, value);
			} else if (field.getType() == Integer.TYPE) {
				field.set(obj, Integer.parseInt(value));
			} else if (field.getType() == Long.TYPE) {
				field.set(obj, Long.parseLong(value));
			} else if (field.getType() == Boolean.TYPE) {
				if (value == null) {
					field.set(obj, true);
					// If the value is null, the parsed setting is a flag (e.g. -debug).
				} else if (SettingsUtils.isTrue(value)) {
					field.set(obj, true);
				} else if (SettingsUtils.isFalse(value)) {
					field.set(obj, false);
				} else {
					throw new SettingsParserException("Invalid Boolean value: '%s'.", value);
				}
			} else if (field.getType() == Path.class) {
				if (value == null) {
					throw new IllegalArgumentException(
							String.format("'null' is an invalid value for the setting '%s'.", settingsElementName));
				} else {
					field.set(obj, Paths.get(value));
				}
			} else if (field.getType().isEnum()) {
				boolean found = false;
				// try to find the corresponding enum constant
				List<String> enumLiterals = new ArrayList<>();
				for (Object enumConstant : field.getType().getEnumConstants()) {
					if (enumConstant.toString().equals(value)) {
						field.set(obj, enumConstant);
						found = true;
						break;
					}
					enumLiterals.add(enumConstant.toString());
				}
				if (!found) {
					String enumLiteralsStr = String.join(", ", enumLiterals);

					throw new SettingsParserException(
							"The value '%s' for the setting '%s' is not in the set of acceptable values (%s). Acceptable values: %s.",
							value, settingsElementName, field.getType().getSimpleName(), enumLiteralsStr);
				}
			} else {
				throw new SettingsAnnotationException(
						"The type '%s' is not supported for element fields annotated with @PlcverifSettingsElement. The conversion to this type is not defined in the method 'setWithTypeConversion()'.",
						field.getType());
			}
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new SettingsParserException(e,
					"Unable to set the field '%s' (representing the setting '%s') in %s to '%s'.", field.getName(),
					settingsElementName, obj.getClass().getSimpleName(), value);
		}
	}

	private static boolean listEquals(List<?> list1, List<?> list2) {
		if (list1 == null && list2 == null) {
			return true;
		} else if (list1 == null || list2 == null) {
			return false;
		}

		if (list1.size() != list2.size()) {
			return false;
		}

		for (int i = 0; i < list1.size(); i++) {
			Object item1 = list1.get(i);
			Object item2 = list2.get(i);

			if (item1 == null && item2 == null) {
				continue;
			} else if (item1 == null || item2 == null) {
				return false;
			} else {
				if (!item1.equals(item2)) {
					return false;
				}
			}
		}

		return true;
	}
}
