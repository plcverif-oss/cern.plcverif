/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.common.settings;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;

/**
 * Class for settings element, i.e., a settings node which contain:
 * <ul>
 * <li>A string value (zero or one),
 * <li>Attributes, i.e., named children settings nodes (zero or more).
 * </ul>
 */
public final class SettingsElement extends Settings {
	private String value;
	private Map<String, Settings> attributes = new HashMap<>();
	private boolean consumed = false;

	/**
	 * Creates an unnamed settings element. It will get its name after set as
	 * child of a parent settings.
	 */
	private SettingsElement() {
		super();
	}

	/**
	 * Creates a named settings element with the given value.
	 */
	private SettingsElement(String name, String value) {
		super(name);
		this.value = value;
	}

	/**
	 * Creates an unnamed settings element. It will get its name after set as
	 * child of a parent settings.
	 */
	public static SettingsElement empty() {
		return new SettingsElement();
	}

	/**
	 * Creates an unnamed settings element with the given value. It will get its
	 * name after set as child of a parent settings.
	 * 
	 * @param value
	 *            Value to be used in the newly created settings element
	 * @return Created settings element
	 */
	public static SettingsElement ofValue(String value) {
		SettingsElement ret = new SettingsElement();
		ret.setValue(value);
		return ret;
	}

	/**
	 * Creates a named root settings element.
	 * <p>
	 * Typically named settings elements are only needed to be created in
	 * exceptional cases, as root elements. Otherwise name will be set
	 * automatically once set as the attribute of a parent settings node. If
	 * added as child of a parent settings node, the name will be overwritten.
	 */
	public static SettingsElement createRootElement(String name) {
		return new SettingsElement(name, null);
	}

	/**
	 * Creates a named root settings element with the given value.
	 * <p>
	 * Typically named settings elements are only needed to be created in
	 * exceptional cases, as root elements. Otherwise name will be set
	 * automatically once set as the attribute of a parent settings node. If
	 * added as child of a parent settings node, the name will be overwritten.
	 */
	public static SettingsElement createRootElement(String name, String value) {
		return new SettingsElement(name, value);
	}

	/**
	 * Sets the value of the settings element to the given value.
	 * 
	 * @param value
	 *            Desired value for the given settings element
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Returns the value of this settings element and marks it as consumed.
	 */
	public String value() {
		this.consumed = true;
		return value;
	}

	/**
	 * Returns the value of this settings element.
	 * <p>
	 * To be only used when all values are fetched e.g. for diagnostic
	 * printouts.
	 */
	public String valueWithoutConsuming() {
		return value;
	}

	/**
	 * Sets the given {@code settings} as an attribute with key {@code name}.
	 * 
	 * @param name
	 *            The name of the attribute. Shall not be {@code null} or
	 *            integer.
	 * @param settings
	 *            The settings node to be used as attribute. The name of this
	 *            node will be changed to the given name.
	 */
	public void setAttribute(String name, Settings settings) {
		Preconditions.checkNotNull(name, "Null name is invalid for settings.");
		Preconditions.checkArgument(!name.matches("\\d+"),
				"The name cannot be an integer, as it would cause confusion with the SettingsLists.");
		if (settings == null) {
			throw new PlcverifPlatformException(
					String.format("The value 'null' is not valid for the settings '%s'.", name));
		}

		if (attributes.containsKey(name)) {
			Settings oldValue = attributes.get(name);
			if (oldValue != null) {
				oldValue.setParent(null);
			}
		}

		settings.setParent(this);
		settings.setName(name);
		attributes.put(name, settings);
	}

	/**
	 * Creates a new settings element with value {@code value}, then sets it as
	 * an attribute with key {@code name}.
	 * 
	 * @param name
	 *            Name of the attribute. Shall not be {@code null}.
	 * @param value
	 *            Value of the attribute. May be {@code null}, in this case no
	 *            {@link SettingsElement} will be created for this key.
	 */
	public void setSimpleAttribute(String name, String value) {
		Preconditions.checkNotNull(name, "The name 'null' is invalid for a settings element.");

		if (value == null) {
			setAttribute(name, null);
		} else {
			setAttribute(name, SettingsElement.ofValue(value));
		}
	}

	/**
	 * Returns an unmodifiable collection of attribute names known for this
	 * settings element.
	 * 
	 * @return Known attribute names
	 */
	public Collection<String> getAttributeNames() {
		return Collections.unmodifiableCollection(attributes.keySet());
	}

	/**
	 * Returns the attribute with the given name if available.
	 * 
	 * @param name
	 *            Attribute name
	 * @return The settings node known with the given attribute name, or
	 *         {@link Optional#empty()} if the given attribute name is not known
	 *         for this settings element.
	 */
	public Optional<Settings> getAttribute(String name) {
		Settings attribute = attributes.getOrDefault(name, null);

		if (attribute != null && attribute.getParent() != this) {
			throw new IllegalStateException(String.format(
					"The attribute to be returned has an inconsistent parent setting: %s, %s.", this.fqn(), name));
		}

		return Optional.ofNullable(attribute);
	}

	/**
	 * Helper method to add a given list of settings under the given attribute.
	 * 
	 * @param attribute
	 *            Name of the new attribute
	 * @param listToRepresent
	 *            List to be represented. For each list item, the
	 *            {@link ISettingsProvider#retrieveSettings()} will be called to
	 *            provide the settings node to be included in the newly created
	 *            list.
	 * @return Newly created settings list
	 */
	public SettingsList addListAttribute(String attribute, List<? extends ISettingsProvider> listToRepresent) {
		Preconditions.checkNotNull(attribute, "attribute");
		Preconditions.checkNotNull(listToRepresent, "listToRepresent");

		SettingsList list = SettingsList.empty();
		list.setParent(this);

		for (ISettingsProvider it : listToRepresent) {
			list.add(it.retrieveSettings());
		}

		this.setAttribute(attribute, list);

		return list;
	}

	/**
	 * Helper method to add a given list of strings under the given attribute.
	 * For each element in the given list a new {@link SettingsElement} will be
	 * created, with the given string as value.
	 * 
	 * @param attribute
	 *            Name of the new attribute
	 * @param listToRepresent
	 *            Strings that will be represented by settings elements.
	 */
	public void addStringListAttribute(String attribute, List<String> listToRepresent) {
		Preconditions.checkNotNull(attribute, "attribute");
		Preconditions.checkNotNull(listToRepresent, "listToRepresent");

		SettingsList list = SettingsList.empty();
		list.setParent(this);

		for (String it : listToRepresent) {
			SettingsElement element = SettingsElement.empty();
			element.setValue(it);
			list.add(element);
		}

		this.setAttribute(attribute, list);
	}

	Map<String, Settings> attributes() {
		return attributes;
	}

	/**
	 * Determines whether this settings element has been consumed.
	 * <p>
	 * Keeping track of what has been consumed is a diagnostic method to locate
	 * the ignored settings provided by the user.
	 * 
	 * @return True, iff the settings has been consumed.
	 */
	public boolean isConsumed() {
		return consumed;
	}

	@Override
	public SettingsKind kind() {
		return SettingsKind.ELEMENT;
	}

	@Override
	protected void overrideRecursiveWith(Settings other) {
		Preconditions.checkNotNull(other, "other");

		if (other instanceof SettingsElement) {
			SettingsElement otherElement = (SettingsElement) other;
			if (otherElement.valueWithoutConsuming() != null) {
				this.value = otherElement.valueWithoutConsuming();
			}

			for (Entry<String, Settings> otherAttribEntry : new HashSet<>(otherElement.attributes.entrySet())) {
				Settings currentSettings = this.attributes.get(otherAttribEntry.getKey());
				if (currentSettings == null) {
					otherAttribEntry.getValue().removeFromParent();
					this.setAttribute(otherAttribEntry.getKey(), otherAttribEntry.getValue());
				} else {
					currentSettings.overrideRecursiveWith(otherAttribEntry.getValue());
				}
			}
		} else {
			throw new IllegalArgumentException("Failed to override settings: incompatible settings encountered.");
		}
	}

	/**
	 * Remove the given setting node child.
	 * 
	 * @param setting
	 *            Settings node to be removed
	 * @throws IllegalArgumentException
	 *             if the given setting does not have a parent or it is not this
	 *             settings node.
	 */
	@Override
	void removeChild(Settings setting) {
		Preconditions.checkArgument(setting.getParent() == null || setting.getParent() == this,
				"Invalid parent for the setting to be removed.");

		List<String> keysToRemove = new ArrayList<>();
		for (Entry<String, Settings> attribute : attributes.entrySet()) {
			if (attribute.getValue() == setting) {
				keysToRemove.add(attribute.getKey());
			}
		}

		for (String key : keysToRemove) {
			attributes.remove(key);
		}
	}

	@Override
	protected void replaceChild(Settings childToBeReplaced, Settings newChild) {
		Optional<String> childAttrib = attributeOfChild(childToBeReplaced);
		if (childAttrib.isPresent()) {
			setAttribute(childAttrib.get(), newChild);
		} else {
			throw new IllegalArgumentException("The child to be replaced is not present.");
		}
	}

	private Optional<String> attributeOfChild(Settings child) {
		for (Entry<String, Settings> attribute : attributes.entrySet()) {
			if (attribute.getValue() == child) {
				return Optional.of(attribute.getKey());
			}
		}
		return Optional.empty();
	}

	@Override
	public String toString() {
		return fqn() + " = " + (value() == null ? "null" : value());
	}

	@Override
	public SettingsElement toSingle() throws SettingsParserException {
		return this;
	}

	@Override
	public SettingsList toList() throws SettingsParserException {
		if (!this.getAttributeNames().isEmpty()) {
			throw new SettingsParserException(String.format("The element %s must be a list.", fqn()));
		}
		SettingsList ret = SettingsList.empty();
		ret.set(0, SettingsElement.ofValue(this.valueWithoutConsuming()));

		// set it consumed, we have no better idea
		this.consumed = true;

		return ret;
	}

	@Override
	public SettingsElement copy() {
		SettingsElement ret = new SettingsElement();
		ret.setName(this.getName());
		ret.value = this.value;
		ret.consumed = this.consumed;
		for (Entry<String, Settings> attribute : attributes.entrySet()) {
			Settings copy = attribute.getValue().copy();
			copy.setParent(ret);
			ret.attributes.put(attribute.getKey(), copy);
		}
		return ret;
	}
}
