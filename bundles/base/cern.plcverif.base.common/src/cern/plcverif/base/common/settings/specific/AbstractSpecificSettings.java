/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.settings.specific;

import cern.plcverif.base.common.settings.SettingsElement;

/**
 * Abstract superclass for classes representing parsed specific setting elements.
 *
 */
public abstract class AbstractSpecificSettings {
	private SettingsElement representedSettingsElement;

	public SettingsElement getRepresentedSettingsElement() {
		return representedSettingsElement;
	}
	
	private String value = null;
	
	/**
	 * Returns the value for the SettingsElement that is represented by this object.  
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value for the SettingsElement that is represented by this object.  
	 */
	public void setValue(String value) {
		this.value = value;
	}
}