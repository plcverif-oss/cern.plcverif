/******************************************************************************
 * (C) Copyright CERN 2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.common.settings.help;

import java.io.PrintWriter;
import java.util.Collection;
import java.util.Optional;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory;

/**
 * Abstract superclass for settings help writers. The subclasses of this class
 * will define the exact format of the resulting output.
 */
public abstract class SettingsHelpWriter {
	private final PrintWriter writer;

	/**
	 * Creates a new settings help writer that will write the final, formatted
	 * representation using the given {@code writer}.
	 * 
	 * @param writer
	 *            Writer to be used to output the formatted representation.
	 *            Shall not be {@code null}.
	 */
	public SettingsHelpWriter(PrintWriter writer) {
		Preconditions.checkNotNull(writer);
		this.writer = writer;
	}

	/**
	 * Prints the header for the help. The default implementation will not print
	 * anything. Can be overridden in the subclass. This method is expected to
	 * be called before any other print method.
	 */
	public void printHeader() { // NOPMD
		// Empty on purpose (optional to override).
	}

	/**
	 * Prints the header for a given plug-in.
	 * 
	 * @param settingsFqn
	 *            The fully qualified name of the setting where this plug-in is
	 *            available as a valid value. E.g., {@code job.reporters}. It is
	 *            expected to not end with a {@code .}.
	 * @param pluginId
	 *            Command ID of the plug-in. Shall not be {@code null}.
	 * @param pluginName
	 *            Name of the plug-in. Shall not be {@code null}.
	 * @param pluginDescription
	 *            Description of the plug-in. Typically it is one paragraph of
	 *            text describing the goal of the plug-in. Shall not be
	 *            {@code null}.
	 *            <p>
	 *            Example: <i>Generates a HTML representation of the
	 *            verification results, including the metadata, configuration
	 *            and the eventual counterexample.</i>
	 */
	public abstract void printPluginDetailsHeader(Optional<String> settingsFqn, String pluginId, String pluginName,
			String pluginDescription);

	/**
	 * Prints an entry for the given settings item.
	 * 
	 * @param prefix
	 *            The prefix of the settings item, if any. Shall not be
	 *            {@code null}, can be {@link Optional#empty()}.
	 * @param settingName
	 *            Name of the current settings item (element or list). Shall not
	 *            be {@code null}.
	 * @param typeStr
	 *            String representation of the accepted type for this settings
	 *            item. Shall not be {@code null}.
	 * @param description
	 *            Description of the current settings item. Shall not be
	 *            {@code null}.
	 * @param mandatory
	 *            Whether the settings item is mandatory to be specified or not.
	 *            Shall not be {@code null}.
	 * @param permittedValues
	 *            List of permitted values for this settings item. Shall not be
	 *            {@code null}, can be {@link Optional#empty()}.
	 * @param defaultValue
	 *            Default value for the settings item. Shall not be
	 *            {@code null}, can be {@link Optional#empty()}.
	 */
	public abstract void printSettingsItem(Optional<String> prefix, String settingName, String typeStr,
			String description, PlcverifSettingsMandatory mandatory, Optional<Collection<String>> permittedValues,
			Optional<String> defaultValue);

	/**
	 * Writes a new line.
	 */
	protected final void println() {
		println("");
	}

	/**
	 * Writes the given message followed by a new line.
	 */
	protected final void println(String msg) {
		writer.println(msg);
	}

	/**
	 * Writes the given formatted message.
	 */
	protected final void printf(String format, Object... args) {
		writer.printf(format, args);
	}
}
