/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.common.settings;

import cern.plcverif.base.common.settings.exceptions.SettingsParserException;

/**
 * Abstract superclass for settings nodes.
 */
public abstract class Settings {
	private Settings parent = null;
	private String name;

	/**
	 * Creates a new settings node with {@code null} name.
	 */
	protected Settings() {
		this.name = null;
	}

	/**
	 * Creates a new settings node with the given name.
	 */
	protected Settings(String name) {
		this.name = name;
	}

	/**
	 * Returns the type of this settings node.
	 * 
	 * @return Type of this settings node
	 */
	public abstract SettingsKind kind();

	/**
	 * Returns the parent of this settings node. May be {@code null} if this is
	 * the root node of a settings tree.
	 * 
	 * @return Parent settings node
	 */
	public Settings getParent() {
		return parent;
	}

	/**
	 * Sets the parent of this settings node. Shall only be used by the methods
	 * in the descendants of {@link Settings} which add children.
	 * 
	 * If this settings node was contained by another settings node before, it
	 * will be removed from that previous settings node (previous parent).
	 * 
	 * @param parent
	 *            New parent node
	 */
	void setParent(Settings parent) {
		removeFromParent();

		this.parent = parent;
	}

	/**
	 * Overrides non-null values and children with the ones from {@code other}.
	 * This operation alters this {@link Settings} element.
	 * <p>
	 * It may reuse some subtrees from {@code other}, thus after this operation,
	 * {@code other} shall not be used.
	 * <p>
	 * The parent of {@code other} will be unset, and the parent of this
	 * settings will be set to the parent of {@code other}.
	 * 
	 * @param other
	 *            Settings with which this settings should be overwritten.
	 */
	public void overrideWith(Settings other) {
		this.overrideRecursiveWith(other);

		if (other.parent != null) {
			other.parent.replaceChild(other, this);
		}
	}

	protected abstract void overrideRecursiveWith(Settings other);

	/**
	 * Removes the given settings node children. Does not do anything if this
	 * settings node does not have a parent.
	 * 
	 * @param setting
	 *            Settings node to be removed from the children
	 */
	abstract void removeChild(Settings setting);

	/**
	 * Removes this settings node from its parent and sets the parent field to
	 * {@code null}. Does nothing if the parent flag is currently {@code null}.
	 */
	void removeFromParent() {
		if (parent != null) {
			this.parent.removeChild(this);
			this.parent = null;
		}
	}

	protected abstract void replaceChild(Settings childToBeReplaced, Settings newChild);

	/**
	 * Returns the name of this settings node.
	 * 
	 * @return Name of this settings node. May be {@code null}.
	 */
	public String getName() {
		return name;
	}

	protected void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the fully qualified name (FQN) of this settings node.
	 * 
	 * @return FQN. May be {@code null} if the name is {@code null}.
	 */
	public String fqn() {
		if (parent == null || parent.fqn().length() == 0) {
			return name == null ? "" : name;
		} else {
			return parent.fqn() + "." + name;
		}
	}

	/**
	 * Returns a {@link SettingsElement} representation of this node if
	 * possible.
	 * 
	 * @return {@link SettingsElement} representation.
	 * @throws SettingsParserException
	 *             if it is not possible to represent as a
	 *             {@link SettingsElement}.
	 */
	public abstract SettingsElement toSingle() throws SettingsParserException;

	/**
	 * Returns a {@link SettingsList} representation of this node if possible.
	 * 
	 * @return {@link SettingsList} representation.
	 * @throws SettingsParserException
	 *             if it is not possible to represent as a {@link SettingsList}.
	 */
	public abstract SettingsList toList() throws SettingsParserException;

	/**
	 * Makes an identical (deep) copy of this settings node and its subtree.
	 * 
	 * @return Copy of this node and its subtree.
	 */
	public abstract Settings copy();
}
