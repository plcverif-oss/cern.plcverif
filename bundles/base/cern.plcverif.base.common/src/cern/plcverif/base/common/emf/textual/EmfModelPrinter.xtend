/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Vince Molnar - improve formatting
 *****************************************************************************/
package cern.plcverif.base.common.emf.textual

import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.util.EObjectContainmentEList
import java.util.Stack

class EmfModelPrinter {
	StringBuilder output = new StringBuilder();	
	EObjectToText eobjToText;
	
	private new(EObjectToText eobjToText) {
		this.eobjToText = eobjToText;
	}
	
	private def append(CharSequence toAppend) {
		output.append(toAppend);
	}
	private def appendln(CharSequence toAppend) {
		output.append(toAppend + System.lineSeparator());
	}
	private def appendln() {
		output.append(System.lineSeparator());
	}
	
	def static CharSequence print(EObject e, EObjectToText eobjToText) {
		val printer = new EmfModelPrinter(eobjToText);
		printer.print(e,new Stack<Boolean>());
		printer.output.toString;
	}
	
	@SuppressWarnings("rawtypes")
	def void print(EObject e, Stack<Boolean> lineOrTab) {
		indent(lineOrTab)
		append("{")
		append(e.eClass.name)
		for (attribute : e.eClass.getEAllAttributes) {
			append(''' «attribute.name»=«e.eGet(attribute)»''')
		}
		
		for (reference : e.eClass.getEAllReferences.filter[!isContainment]) {
			if (e.eGet(reference) === null) {
				append(''' «reference.name»:<null>''')
			} else if (e.eGet(reference) instanceof EObject) {
				append(''' «reference.name»:«eobjToText.text(e.eGet(reference) as EObject)»''')
			} else if (e.eGet(reference) instanceof EList<?>) {
				append(''' «reference.name»:[«(e.eGet(reference) as EList<?>).map[eobjToText.text(it as EObject)].join(",")»]''')
			} else {
				System.err.println("Unknown: " + e.eGet(reference))
			}
		}
		appendln("}")
		
		for (reference : e.eClass.getEAllContainments) {
			val last = reference == e.eClass.EAllContainments.last
			indent(lineOrTab)
			if (last) {
				append(" \u2514\u2500\u2500" + reference.name + ": ")  // |___
			} else {
				append(" \u251c\u2500\u2500" + reference.name + ": ")  // +--
			}
			
			if (e.eGet(reference) === null) {
				appendln("<null>")
			} else
			if (e.eGet(reference) instanceof EObject) {
				appendln
				lineOrTab.push(!last)
				print(e.eGet(reference) as EObject, lineOrTab)
				lineOrTab.pop()	
			} else if (e.eGet(reference) instanceof EObjectContainmentEList<?>) {
				if ((e.eGet(reference) as EObjectContainmentEList<?>).isEmpty) {
					appendln("<empty>")
				} else {
					appendln
					for (items : (e.eGet(reference) as EObjectContainmentEList<EObject>)) {
						lineOrTab.push(!last)
						print(items, lineOrTab)
						lineOrTab.pop()	
					}
				}
			} else {
				System.err.println("ERROR: " + e.eGet(reference).class.name)
			}
			
		}
	}

	def indent(Stack<Boolean> lineOrTab) {
		for (line : lineOrTab) {
			if (line) {
				append(" \u2502  ")
			} else {
				append("    ")
			}
		}
	}
}
