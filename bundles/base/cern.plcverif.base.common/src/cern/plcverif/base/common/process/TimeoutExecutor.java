/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.process;

import java.util.concurrent.TimeUnit;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.logging.PlatformLogger;
import cern.plcverif.base.common.progress.ICanceling;

public final class TimeoutExecutor {
	public static final int CHECK_ISCANCELLED_INTERVAL_SEC = 2;

	private boolean isCanceled = false;
	private boolean isTimeout = false;

	private TimeoutExecutor() {
		// Will only be created by #execute().
	}

	public static TimeoutExecutor execute(final Process process, final int timeoutSec) {
		return execute(process, timeoutSec, ICanceling.NEVER_CANCELING_INSTANCE);
	}

	public static TimeoutExecutor execute(final Process process, final int timeoutSec, final ICanceling reporter) {
		return execute(process, timeoutSec, reporter, false);
	}
		
	public static TimeoutExecutor execute(final Process process, final int timeoutSec, final ICanceling reporter, final boolean killForcibly) {
		Preconditions.checkNotNull(process);
		Preconditions.checkNotNull(reporter);

		final TimeoutExecutor ret = new TimeoutExecutor();

		Thread supervisorThread = new Thread() {
			@Override
			public void run() {
				int elapsedSec = 0;
				try {
					while (true) {
						TimeUnit.SECONDS.sleep(CHECK_ISCANCELLED_INTERVAL_SEC);
						elapsedSec += CHECK_ISCANCELLED_INTERVAL_SEC;
						if (process.isAlive()) {
							if (timeoutSec > 0 && elapsedSec >= timeoutSec) {
								ret.isTimeout = true;
								PlatformLogger.logDebug("Timeout. Timeout executor will attempt to destroy the process.");
								TimeoutExecutor.destroy(process, killForcibly);
							} else if (reporter.isCanceled()) {
								ret.isCanceled = true;
								TimeoutExecutor.destroy(process, killForcibly);
							}
						} else {
							// stop thread
							return;
						}
					}
				} catch (InterruptedException ex) {
					PlatformLogger.logDebug("Thread interrupted. Timeout executor will attempt to destroy the process.");
					TimeoutExecutor.destroy(process, killForcibly);
				}
			}
		};

		supervisorThread.start();
		return ret;
	}
	
	private static void destroy(Process process, boolean killForcibly) {
		Preconditions.checkNotNull(process, "process");
		if (killForcibly) {
			process.destroyForcibly();
		} else {
			process.destroy();
		}
	}

	public boolean isCanceled() {
		return this.isCanceled;
	}

	public boolean isTimeout() {
		return this.isTimeout;
	}
}
