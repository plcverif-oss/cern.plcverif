/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.logging;

/**
 * Common interface for various loggers used in PLCverif. It is implemented at
 * least by the {@code PlatformLogger} and the {@code JobStage}.
 */
public interface IPlcverifLogger {
	/**
	 * Null logger instance that implements the {@link IPlcverifLogger}
	 * interface, but calling any of its methods will have no outcome.
	 */
	public static final NullPlcverifLogger NULL_LOGGER = new NullPlcverifLogger();

	void log(PlcverifSeverity severity, String messageFormat, Object... args);

	void log(PlcverifSeverity severity, String message, Throwable throwable);

	default void logDebug(String message, Object... args) {
		log(PlcverifSeverity.Debug, message, args);
	}

	default void logInfo(String message, Object... args) {
		log(PlcverifSeverity.Info, message, args);
	}

	default void logWarning(String message, Object... args) {
		log(PlcverifSeverity.Warning, message, args);
	}
	
	default void logWarning(String message, Throwable throwable) {
		log(PlcverifSeverity.Warning, message, throwable);
	}

	default void logError(String message, Object... args) {
		log(PlcverifSeverity.Error, message, args);
	}

	default void logError(String message, Throwable throwable) {
		log(PlcverifSeverity.Error, message, throwable);
	}

	/**
	 * {@link IPlcverifLogger} implementation that does nothing at logging.
	 * It cannot be instantiated, use the instance at {@link IPlcverifLogger#NULL_LOGGER}.
	 */
	public static final class NullPlcverifLogger implements IPlcverifLogger {
		private NullPlcverifLogger() {
		}

		@Override
		public void log(PlcverifSeverity severity, String messageFormat, Object... args) {
			// Nothing to do in the null logger.
		}

		@Override
		public void log(PlcverifSeverity severity, String message, Throwable throwable) {
			// Nothing to do in the null logger.
		}
	}
}
