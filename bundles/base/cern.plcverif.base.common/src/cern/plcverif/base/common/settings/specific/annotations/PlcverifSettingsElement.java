/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.settings.specific.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Field representing a SettingsElement with the name {@code name}.
 * The purpose of the field should be described in the {@code description}.
 * 
 * By default it is mandatory. It can be made optional by setting
 * {@code mandatory = PlcverifSettingsMandatory.OPTIONAL}.
 * 
 * Supported field type(s): {@code int}, {@code long}, {@link String},
 * {@link java.nio.file.Path}, enumeration types.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface PlcverifSettingsElement {
	String name();

	String description() default "N/A";

	PlcverifSettingsMandatory mandatory() default PlcverifSettingsMandatory.MANDATORY;
}