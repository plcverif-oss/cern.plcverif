/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.settings;

import java.util.Optional;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.exceptions.SettingsSerializerException;
import cern.plcverif.base.common.settings.specific.AbstractSpecificSettings;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;

/**
 * Helper class to facilitate determining the effective settings based on
 * several generic or specific settings objects. None of the passed settings
 * arguments will be modified, instead defensive copies will be made.
 */
public final class EffectiveSettings {
	private Settings effectiveSettings = null;

	private EffectiveSettings() {
	}

	private static EffectiveSettings base() {
		return new EffectiveSettings();
	}

	/**
	 * Creates an effective settings helper object with the given generic
	 * settings as base (if present).
	 * 
	 * @param settings
	 *            Base settings
	 * @return Effective settings helper
	 */
	public static EffectiveSettings base(Optional<? extends Settings> settings) {
		return new EffectiveSettings().overrideWith(settings);
	}

	/**
	 * Creates an effective settings helper object with the given specific
	 * settings as base.
	 * 
	 * @param settings
	 *            Base settings
	 * @return Effective settings helper
	 */
	public static EffectiveSettings base(AbstractSpecificSettings settings)
			throws SettingsSerializerException, SettingsParserException {
		return new EffectiveSettings().overrideWith(settings);
	}

	/**
	 * Overrides the previously stored settings with the given generic settings
	 * (if present).
	 * 
	 * @param settings
	 *            Settings to override the previously stored settings with
	 * @return This
	 */
	public EffectiveSettings overrideWith(Optional<? extends Settings> settings) {
		if (settings.isPresent()) {
			return overrideWith(settings.get());
		} else {
			// Nothing to do, no settings provided.
		}

		return this;
	}

	/**
	 * Overrides the previously stored settings with the given specific
	 * settings.
	 * 
	 * @param settings
	 *            Settings to override the previously stored settings with
	 * @return This
	 */
	public EffectiveSettings overrideWith(AbstractSpecificSettings settings)
			throws SettingsSerializerException, SettingsParserException {
		Preconditions.checkNotNull(settings, "settings");
		Settings asGenericSettings = SpecificSettingsSerializer.toGenericSettings(settings, Optional.empty(), false);
		Preconditions.checkState(asGenericSettings != null);
		return overrideWith(asGenericSettings);
	}

	/**
	 * Overrides the previously stored settings with the given generic settings.
	 * 
	 * @param settings
	 *            Settings to override the previously stored settings with. Must
	 *            not be {@code null}.
	 * @return This
	 */
	public EffectiveSettings overrideWith(Settings settings) {
		Preconditions.checkNotNull(settings, "settings");

		if (effectiveSettings == null) {
			effectiveSettings = settings.copy();
		} else {
			effectiveSettings.overrideWith(settings.copy());
		}

		return this;
	}

	/**
	 * Returns the effective settings created in this.
	 * 
	 * @return Effective settings, or {@link Optional#empty()} if no settings
	 *         was passed.
	 */
	public Optional<Settings> getEffectiveSettings() {
		return Optional.ofNullable(this.effectiveSettings);
	}

	/**
	 * Returns the effective settings created in this. If no settings were
	 * passed to this object, it throws an exception.
	 * 
	 * @return Effective settings. Never {@code null}.
	 * @throws SettingsParserException
	 *             if no settings was given to this object
	 */
	public Settings getEffectiveSettingsOrThrow() throws SettingsParserException {
		if (this.effectiveSettings == null) {
			throw new SettingsParserException("Unable to get the effective settings.");
		} else {
			return this.effectiveSettings;
		}
	}

	/**
	 * Returns the effective settings for a plug-in.
	 * 
	 * @param hardcodedDefaults
	 *            Hard-coded defaults (i.e., a specific settings object with its
	 *            default values) if available. It can be empty if there are no
	 *            specific settings for the given plug-in.
	 * @param resourceDefaults
	 *            Default settings from the resources (typically the content of
	 *            the default.settings file in the JAR) if available. It can be
	 *            empty if there are no resource settings for the given plug-in.
	 * @param installationDefaults
	 *            Installation-specific settings if available. It can be empty
	 *            if there are no installation-specific settings for the given
	 *            plug-in.
	 * @return The effective settings based on the given parts
	 * @throws SettingsParserException
	 * @throws SettingsSerializerException
	 */
	public static Settings pluginEffectiveSettings(Optional<? extends AbstractSpecificSettings> hardcodedDefaults,
			Optional<? extends Settings> resourceDefaults, Optional<? extends Settings> installationDefaults)
			throws SettingsParserException, SettingsSerializerException {
		EffectiveSettings effective = hardcodedDefaults.isPresent() ? base(hardcodedDefaults.get()) : base();
		return effective.overrideWith(resourceDefaults).overrideWith(installationDefaults)
				.getEffectiveSettingsOrThrow();
	}

	/**
	 * Returns the effective settings for a plug-in.
	 * 
	 * @param hardcodedDefaults
	 *            Hard-coded defaults (i.e., a specific settings object with its
	 *            default values). Shall not be {@code null}.
	 * @param resourceDefaults
	 *            Default settings from the resources (typically the content of
	 *            the default.settings file in the JAR). Shall not be
	 *            {@code null}.
	 * @param installationDefaults
	 *            Installation-specific settings if available. It can be empty
	 *            if there are no installation-specific settings for the given
	 *            plug-in.
	 * @return The effective settings based on the given parts
	 * @throws SettingsParserException
	 * @throws SettingsSerializerException
	 */
	public static Settings pluginEffectiveSettings(AbstractSpecificSettings hardcodedDefaults,
			Settings resourceDefaults, Optional<? extends Settings> installationDefaults)
			throws SettingsParserException, SettingsSerializerException {
		Preconditions.checkNotNull(hardcodedDefaults, "hardcodedDefaults");
		Preconditions.checkNotNull(resourceDefaults, "resourceDefaults");
		return EffectiveSettings.base(hardcodedDefaults).overrideWith(resourceDefaults)
				.overrideWith(installationDefaults).getEffectiveSettingsOrThrow();
	}
}
