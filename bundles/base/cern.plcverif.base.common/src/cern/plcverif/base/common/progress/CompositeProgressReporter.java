/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.progress;

import java.util.Arrays;
import java.util.List;

import com.google.common.base.Preconditions;

/**
 * Composite progress reporter that incorporates multiple progress reporters.
 * Progress reports will be sent to each of the components. If any of the
 * components request cancellation, {@link #isCanceled()} of this will be true.
 */
public final class CompositeProgressReporter implements ICancelingProgressReporter {
	/**
	 * List of component progress reporters.
	 * Never {@code null}.
	 */
	private List<ICancelingProgressReporter> components;

	private CompositeProgressReporter(List<ICancelingProgressReporter> components) {
		this.components = Preconditions.checkNotNull(components);
	}

	/**
	 * Creates a new composite reporter of the given components.
	 * 
	 * @param components
	 *            Components to include in the composite reporter. Shall not be
	 *            empty.
	 * @return Composite reporter uniting the given components
	 */
	public static CompositeProgressReporter of(ICancelingProgressReporter... components) {
		Preconditions.checkNotNull(components, "At least one component is expected for CompositeProgressReporter.");
		return new CompositeProgressReporter(Arrays.asList(components));
	}

	@Override
	public boolean isCanceled() {
		for (ICancelingProgressReporter component : components) {
			if (component.isCanceled()) {
				return true;
			}
		}

		return false;
	}

	@Override
	public void reportTask(String currentTask) {
		components.forEach(it -> it.reportTask(currentTask));
	}

	@Override
	public void reportProgress(int percentage) {
		components.forEach(it -> it.reportProgress(percentage));
	}

	@Override
	public void report(String currentTask, int percentage) {
		components.forEach(it -> it.report(currentTask, percentage));
	}
}
