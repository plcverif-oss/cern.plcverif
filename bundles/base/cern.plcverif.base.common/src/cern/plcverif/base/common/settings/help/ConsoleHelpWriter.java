/******************************************************************************
 * (C) Copyright CERN 2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.common.settings.help;

import java.io.PrintWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory;

/**
 * Settings help writer producing a compact plain-text representation, suitable
 * for displaying in the console.
 */
public class ConsoleHelpWriter extends SettingsHelpWriter {
	public ConsoleHelpWriter(PrintWriter writer) {
		super(writer);
	}

	@Override
	public void printPluginDetailsHeader(Optional<String> settingsFqn, String pluginId, String pluginName, String pluginDescription) {
		Preconditions.checkNotNull(settingsFqn, "settingsFqn");
		Preconditions.checkNotNull(pluginId, "pluginId");
		Preconditions.checkNotNull(pluginName, "pluginName");
		Preconditions.checkNotNull(pluginDescription, "pluginDescription");

		println();

		int titleLength = 0;
		if (!settingsFqn.isPresent()) {
			printf("%s : %s%n", pluginId, pluginName);
			titleLength = pluginId.length();
		} else {
			printf("%s = %s : %s%n", settingsFqn.get(), pluginId, pluginName);
			titleLength = settingsFqn.get().length() + 3 + pluginId.length();
		}

		// Print separator line
		println(String.join("", Collections.nCopies(Math.max(1, titleLength), "=")));
		
		// Print description
		println(pluginDescription);
		println();
	}

	@Override
	public void printSettingsItem(Optional<String> prefix, String settingName, String typeStr, String description,
			PlcverifSettingsMandatory mandatory, Optional<Collection<String>> permittedValues,
			Optional<String> defaultValue) {
		Preconditions.checkNotNull(prefix, "prefix");
		Preconditions.checkNotNull(settingName, "settingName");
		Preconditions.checkNotNull(typeStr, "typeStr");
		Preconditions.checkNotNull(description, "description");
		Preconditions.checkNotNull(mandatory, "mandatory");
		Preconditions.checkNotNull(permittedValues, "permittedValues");
		Preconditions.checkNotNull(defaultValue, "defaultValue");

		String optionalStr = mandatory == PlcverifSettingsMandatory.OPTIONAL || defaultValue.isPresent() ? ", optional"
				: "";
		String permittedValuesString = permittedValues.isPresent()
				? String.format("%n        Permitted values: %s (total: %s)", String.join(", ", permittedValues.get()),
						permittedValues.get().size())
				: "";
		String defaultValueString = defaultValue.isPresent()
				? String.format("%n        Default value: '%s'", defaultValue.get())
				: "";
		printf("  -%s%s (%s%s): %s%s%s%n", prefix.orElse(""), settingName, typeStr, optionalStr, description,
				permittedValuesString, defaultValueString);
	}
}