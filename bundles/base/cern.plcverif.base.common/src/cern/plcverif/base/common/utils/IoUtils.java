/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.io.ByteStreams;

import cern.plcverif.base.common.logging.PlatformLogger;

/**
 * Utility class for I/O-handling-related methods.
 */
public final class IoUtils {
	private IoUtils() {
		// Utility class.
	}

	public static class IoUtilsRuntimeException extends RuntimeException {
		private static final long serialVersionUID = -7890980488069109113L;

		public IoUtilsRuntimeException(String message, Throwable throwable) {
			super(message, throwable);
		}
	}

	/**
	 * Reads the resource at the given path and returns its contents as a
	 * string, <b>assuming UTF-8 encoding</b>. It wraps the
	 * {@link Class#getResourceAsStream(String)} method for convenience. The
	 * value of {@code path} is not altered, thus the same rules apply for it as
	 * in case of {@link Class#getResourceAsStream(String)}.
	 * 
	 * @param path
	 *            Path of the resource to read.
	 * @param classLoader
	 *            Class loader to be used to locate the resource.
	 * @return The content of the file, using {@link System#lineSeparator()} as
	 *         line separator.
	 */
	public static String readResourceFile(String path, ClassLoader classLoader) {
		InputStream resourceStream = classLoader.getResourceAsStream(path);
		Preconditions.checkNotNull(resourceStream,
				String.format("The resource stream shall not be null. Does the file '%s' exist?", path));
		try (BufferedReader buffer = new BufferedReader(
				new InputStreamReader(resourceStream, StandardCharsets.UTF_8))) {
			return buffer.lines().collect(Collectors.joining(System.lineSeparator()));
		} catch (IOException e) {
			throw new IoUtilsRuntimeException("IOException while reading the resource.", e);
		}
	}

	/**
	 * Reads the resource at the given path and returns its contents as a byte
	 * array. It wraps the {@link Class#getResourceAsStream(String)} method for
	 * convenience. The value of {@code path} is not altered, thus the same
	 * rules apply for it as in case of
	 * {@link Class#getResourceAsStream(String)}.
	 * 
	 * @param path
	 *            Path of the resource to read.
	 * @param classLoader
	 *            Class loader to be used to locate the resource.
	 * @return The content of the file as {@code byte[]}.
	 */
	public static byte[] readBinaryResourceFile(String path, ClassLoader classLoader) {
		InputStream resourceStream = classLoader.getResourceAsStream(path);
		Preconditions.checkNotNull(resourceStream,
				String.format("The resource stream shall not be null. Does the file '%s' exist?", path));
		try {
			return ByteStreams.toByteArray(resourceStream);
		} catch (IOException e) {
			throw new IoUtilsRuntimeException("IOException while reading the resource.", e);
		}
	}

	/**
	 * Reads the text file at the given path and returns its contents as a
	 * string.
	 * 
	 * @param path
	 *            Path of the file to read.
	 * @return The content of the file, using {@link System#lineSeparator()} as
	 *         line separator.
	 * @throws IOException
	 *             Throws IOException if reading was unsuccessful.
	 */
	public static String readFile(Path path) throws IOException {
		return Files.readAllLines(path).stream().collect(Collectors.joining(System.lineSeparator()));
	}

	/**
	 * Writes all content in {@code content} to {@code path} using UTF-8
	 * encoding.
	 * 
	 * @param path
	 *            Target path.
	 * @param content
	 *            Content to write.
	 * @throws IOException
	 *             Throws IOException if writing was unsuccessful.
	 */
	public static void writeAllContent(File path, CharSequence content) throws IOException {
		com.google.common.io.Files.asCharSink(path, Charsets.UTF_8).write(content);
	}

	/**
	 * Writes all content in {@code content} to {@code path} using UTF-8
	 * encoding.
	 * 
	 * @param path
	 *            Target path.
	 * @param content
	 *            Content to write.
	 * @throws IOException
	 *             Throws IOException if writing was unsuccessful.
	 */
	public static void writeAllContent(Path path, CharSequence content) throws IOException {
		Files.write(path, Arrays.asList(content), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
	}

	/**
	 * Resolves the given file path pattern. It returns a list of {@link Path}s
	 * that matches the given pattern. The {@code filePathPattern} may use
	 * {@code *}, {@code **} and {@code ?}.
	 * <ul>
	 * <li>{@code *} matches zero or more characters, except for the directory separator character on the current system, defined by {@link File#separator}.
	 * <li>{@code **} matches zero or more characters, including the directory separator character. This is only supported on Unix systems.
	 * <li>{@code ?} matches zero or one character, except for the directory separator character on the current system, defined by {@link File#separator}.
	 * </ul>
	 * <p>
	 * If the given parameter does not contain any wildcard ({@code *} or
	 * {@code ?}), the result is a list with a single item that is the
	 * {@link Path} representation of the given parameter.
	 * If no match has been found, an empty list will be returned.
	 * <p>
	 * See more details of the path pattern matching in {@link FileSystem#getPathMatcher(String)}.
	 * 
	 * @param filePathPattern
	 *            The file path pattern in a textual form. (Shall not be prefixed by {@code glob:} or anything else.) Shall not be {@code null}.
	 * @return The list of files matching the path pattern. Never {@code null}.
	 * @see FileSystem#getPathMatcher(String)
	 */
	public static List<Path> resolvePathsMatchingPattern(String filePathPattern) {
		Preconditions.checkNotNull(filePathPattern);
		
		if (!hasPathWildcard(filePathPattern)) {
			return Collections.singletonList(Paths.get(filePathPattern));
		}

		// Create base path (this is the root for search)
		Path basePathWithoutWildcard = getBasePathWithoutWildcard(filePathPattern);
		
		if (Files.notExists(basePathWithoutWildcard)) {
			// Important: this condition is not equivalent to `!basePathWithoutWildcard.toFile().exists()`!
			// The empty path is a possible value for `basePathWithoutWildcard` (e.g. for *.scl), in which 
			// case the behavior is different.
			
			// Impossible to have any match if the base path does not exist.
			return Collections.emptyList();
		}
		
		// Backslashes shall be double-escaped, see
		// https://docs.oracle.com/javase/7/docs/api/java/nio/file/FileSystem.html#getPathMatcher(java.lang.String)
		String globPattern = "glob:" + filePathPattern.replace("\\", "\\\\");
		
		return WildcardFileCollector.collect(basePathWithoutWildcard, FileSystems.getDefault().getPathMatcher(globPattern));
	}
	
	private static final class WildcardFileCollector extends SimpleFileVisitor<Path> {
		private PathMatcher pathMatcher;
		private List<Path> result = new ArrayList<>();
		
		private WildcardFileCollector(PathMatcher pathMatcher) {
			this.pathMatcher = Preconditions.checkNotNull(pathMatcher);
		}
		
		/**
		 * Collects all files matching the given matcher, nested within the given base path.
		 * <p>
		 * Any potential failure will be silent.
		 * 
		 * @param basePath Base path where the collection will be started. All files and directories within this path will be visited.
		 * @param pathMatcher Path matcher. Matching paths will be included in the result. Should not be {@code null}.
		 * @return List of paths matching the given pattern within the given path. 
		 */
		public static List<Path> collect(Path basePath, PathMatcher pathMatcher) {
			WildcardFileCollector visitor = new WildcardFileCollector(pathMatcher);
			try {
				Files.walkFileTree(basePath, visitor);
			} catch (IOException e) {
				// Nothing to do, besides a bit of logging
				PlatformLogger.logDebug(String.format("I/O exception occurred while collecting files in %s with pattern %s.", basePath, pathMatcher), e);
			}
			return visitor.getResult();
		}
		
		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
			if (pathMatcher.matches(file)) {
				result.add(file);
			} 			
			return FileVisitResult.CONTINUE;
		}
		
		public List<Path> getResult() {
			return result;
		}
	}
	
	/**
	 * Returns the longest prefix path of the given file path pattern that does not contain wildcards (? or *).
	 * The returned path may not exist on the file system.
	 * @param filePathPattern File path pattern
	 * @return Longest prefix path without wildcard
	 */
	private static Path getBasePathWithoutWildcard(String filePathPattern) {
		// Split file path into segments and find where is the first segment
		// with wildcard
		List<String> segments = Arrays.asList(filePathPattern.split(Pattern.quote(File.separator)));
		int firstIdxWithWildcard = -1;
		for (int i = 0; i < segments.size(); i++) {
			if (hasPathWildcard(segments.get(i))) {
				firstIdxWithWildcard = i;
				break;
			}
		}
		Preconditions.checkState(firstIdxWithWildcard >= 0);

		Path basePathWithoutWildcard = Paths
				.get(String.join(File.separator, segments.subList(0, firstIdxWithWildcard)));

		return basePathWithoutWildcard;
	}

	private static boolean hasPathWildcard(String path) {
		return path.contains("*") || path.contains("?");
	}

	/**
	 * Returns a new path that points to the given base path, appended with the
	 * given extension.
	 * <p>
	 * For example, if {@code dir1/dir2/file.txt} is provided as path parameter
	 * and {@code bak} is given as extension, the result {@link Path} will point
	 * to {@code dir1/dir2/file.txt.bak}.
	 * 
	 * @param basePath
	 *            Base path to be appended. Shall not be {@code null}.
	 * @param extension
	 *            Extension to be appended. If it does not start with a
	 *            {@code .}, it will be automatically added. Shall not be
	 *            {@code null}.
	 * @return The resulting path, with the given extension. Never {@code null}.
	 */
	public static Path appendExtension(Path basePath, String extension) {
		Preconditions.checkNotNull(basePath, "basePath");
		Preconditions.checkNotNull(extension, "extension");
		
		String sepDotIfNeeded = extension.startsWith(".") ? "" : ".";
		String newName = basePath.getFileName().toString() + sepDotIfNeeded + extension;
		
		return Preconditions.checkNotNull(basePath.resolveSibling(newName));
	}
}
