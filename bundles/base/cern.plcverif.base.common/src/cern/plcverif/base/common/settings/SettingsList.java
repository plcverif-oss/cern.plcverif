/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.common.settings;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.settings.exceptions.SettingsParserException;

/**
 * Class for settings list, i.e., a settings node which contain some indexed
 * settings nodes.
 */
public final class SettingsList extends Settings {
	private ArrayList<Settings> elements = new ArrayList<>();

	private SettingsList() {
		super();
	}

	/**
	 * Creates an unnamed settings list. It will get its name after set as child
	 * of a parent settings.
	 */
	public static SettingsList empty() {
		return new SettingsList();
	}

	/**
	 * Returns an <b>unmodifiable</b> list of its elements. The returned list
	 * may contain {@code null} elements.
	 * 
	 * @return Unmodifiable list of children elements.
	 */
	public List<Settings> elements() {
		// check correctness
		for (int i = 0; i < elements.size(); i++) {
			if (elements.get(i) != null && elements.get(i).getParent() == null) {
				throw new IllegalStateException(String.format(
						"Illegal attribute is about to be returned. Element '%s' of '%s' does not have a parent set.",
						i, this.fqn()));
			}
		}

		return Collections.unmodifiableList(elements);
	}

	/**
	 * Adds a new element to the list at the given index. The list is filled by
	 * {@code null} values if needed. The current list will be registered as
	 * parent of the given {@code element}, unless it is {@code null}.
	 * 
	 * @param element
	 *            The element to be added. May be null.
	 */
	public void set(int index, Settings element) {
		if (element != null) {
			element.removeFromParent();
		}

		if (elements.size() == index) {
			elements.add(element);
		} else {
			if (elements.size() < index) {
				elements.ensureCapacity(index + 1);
				while (elements.size() < index + 1) {
					elements.add(null);
				}
			}

			if (elements.get(index) != null) {
				// there was already an element present, unregister it
				elements.get(index).setParent(null);
			}
			elements.set(index, element);
		}

		if (element != null) {
			element.setName(Integer.toString(index));
			element.setParent(this);
		}
	}

	/**
	 * Adds a new element to the list. The current list will be registered as
	 * parent of the given {@code element}, unless the element to be added is
	 * {@code null}.
	 * 
	 * @param element
	 *            The element to be added. May be {@code null}.
	 */
	public void add(Settings element) {
		elements.add(element);
		if (element != null) {
			element.setName(Integer.toString(elements.size() - 1));
			element.setParent(this);
		}
	}

	/**
	 * Removes the given child object.
	 * 
	 * @setting Settings node to be removed. Should not be {@code null}.
	 * @throws IllegalArgumentException
	 *             if the given settings node is not a child of this settings
	 *             node.
	 */
	@Override
	void removeChild(Settings setting) {
		Preconditions.checkNotNull(setting);
		Preconditions.checkArgument(setting.getParent() == null || setting.getParent() == this,
				"Invalid parent for the setting to be removed.");

		for (int i = 0; i < elements.size(); i++) {
			if (elements.get(i) == setting) {
				elements.set(i, null);
			}
		}
	}

	@Override
	protected void replaceChild(Settings childToBeReplaced, Settings newChild) {
		for (int i = 0; i < elements.size(); i++) {
			if (elements.get(i) == childToBeReplaced) {
				set(i, newChild);
				return;
			}
		}

		throw new IllegalArgumentException("The child to be replaced is not present.");
	}

	@Override
	protected void overrideRecursiveWith(Settings other) {
		Preconditions.checkNotNull(other, "other");
		if (other instanceof SettingsList) {
			SettingsList otherList = (SettingsList) other;
			for (int i = 0; i < otherList.elements().size(); i++) {
				Settings currentSettings = i < this.elements().size() ? this.elements().get(i) : null;
				Settings newSettings = otherList.elements().get(i);
				if (currentSettings == null) {
					this.set(i, newSettings);
				} else {
					if (newSettings != null) {
						currentSettings.overrideRecursiveWith(newSettings);
					} else {
						// nothing to do, the overriding element is null
					}
				}
			}
		} else {
			throw new IllegalArgumentException("Failed to override settings: incompatible settings encountered.");
		}
	}

	/**
	 */
	@Override
	public SettingsKind kind() {
		return SettingsKind.LIST;
	}

	/**
	 * Returns a string representation for diagnostic purposes.
	 */
	@Override
	public String toString() {
		return fqn() + "[]";
	}

	@Override
	public SettingsElement toSingle() throws SettingsParserException {
		throw new SettingsParserException(String.format("The element %s cannot be a list.", fqn()));
	}

	@Override
	public SettingsList toList() throws SettingsParserException {
		return this;
	}

	/**
	 * Creates and returns a deep copy of this settings node and its subtree.
	 * 
	 * @return Deep copy of this settings node's subtree
	 */
	@Override
	public SettingsList copy() {
		SettingsList ret = new SettingsList();
		ret.setName(this.getName());
		for (Settings element : elements) {
			if (element == null) {
				ret.elements.add(null);
			} else {
				Settings copy = element.copy();
				ret.elements.add(copy);
				copy.setParent(ret);
			}
		}
		return ret;
	}

	/**
	 * Removes the given element from the children elements.
	 * 
	 * @param element
	 *            Element to be removed
	 */
	public void removeElement(Settings element) {
		removeChild(element);
	}
	
	/**
	 * Returns true iff this setting list is empty, i.e., it does not have any non-null child.
	 * @return True iff this is an empty settings list.
	 */
	public boolean isEmpty() {
		if (elements.isEmpty()) {
			return true;
		} else {
			return elements.stream().allMatch(it -> it == null);
		}
	}
}
