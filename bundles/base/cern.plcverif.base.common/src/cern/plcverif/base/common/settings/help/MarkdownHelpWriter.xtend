/******************************************************************************
 * (C) Copyright CERN 2018. All rights not expressly granted are reserved.
 * 
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.common.settings.help

/**
 * Settings help writer producing a Markdown representation, suitable
 * for displaying in the documentation.
 */
import java.io.PrintWriter
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory
import java.util.Collection
import java.util.Optional
import cern.plcverif.base.common.formattedstring.MarkdownFormatter
import java.util.Collections
import com.google.common.base.Preconditions

class MarkdownHelpWriter extends SettingsHelpWriter {
	/**
	 * Number of hashmarks preceeding the main title.
	 */
	final int mainTitleLevel;

	/**
	 * Creates a new settings help writer that will write the final, formatted
	 * representation using the given {@code writer}.
	 * 
	 * @param writer
	 *            Writer to be used to output the formatted representation.
	 *            Shall not be {@code null}.
	 */
	new(PrintWriter writer) {
		this(writer, 1);
	}

	/**
	 * Creates a new settings help writer that will write the final, formatted
	 * representation using the given {@code writer}.
	 * 
	 * @param writer
	 *            Writer to be used to output the formatted representation.
	 *            Shall not be {@code null}.
	 * @param mainTitleLevel
	 *            Title level of the main title. It should be at least 1.
	 */
	new(PrintWriter writer, int mainTitleLevel) {
		super(writer);
		Preconditions.checkArgument(mainTitleLevel >= 1, "The main title level should be at least 1.");
		this.mainTitleLevel = mainTitleLevel;
	}

	override printHeader() {
		println('''«titlePrefix(1)» PLCverif Command Line Arguments''')
	}

	override printPluginDetailsHeader(Optional<String> settingsFqn, String pluginId, String pluginName, String pluginDescription) {
		Preconditions.checkNotNull(settingsFqn, "settingsFqn");
		Preconditions.checkNotNull(pluginId, "pluginId");
		Preconditions.checkNotNull(pluginName, "pluginName");
		Preconditions.checkNotNull(pluginDescription, "pluginDescription");

		println('''
			«titlePrefix(2)» «pluginName.toMd» (`-«settingsFqn.orElse("N/A")» = «pluginId»`)
			«pluginDescription»
			
		''');
	}

	override printSettingsItem(Optional<String> prefix, String settingName, String typeStr, String description,
		PlcverifSettingsMandatory mandatory, Optional<Collection<String>> permittedValues,
		Optional<String> defaultValue) {
			Preconditions.checkNotNull(prefix, "prefix");
			Preconditions.checkNotNull(settingName, "settingName");
			Preconditions.checkNotNull(typeStr, "typeStr");
			Preconditions.checkNotNull(description, "description");
			Preconditions.checkNotNull(mandatory, "mandatory");
			Preconditions.checkNotNull(permittedValues, "permittedValues");
			Preconditions.checkNotNull(defaultValue, "defaultValue");

			println('''
				- **-«prefix.orElse("").toMd»«settingName.toMd»**: «description.toMd»
				    - Type: «typeStr» «IF mandatory == PlcverifSettingsMandatory.OPTIONAL»(optional)«ENDIF»
				    «IF permittedValues.isPresent»- Permitted values: «FOR v : permittedValues.get SEPARATOR ', '»`«v»`«ENDFOR»«ENDIF»
				    «IF defaultValue.isPresent»- Default value: «IF defaultValue.get.isEmpty»(empty)«ELSE»`«defaultValue.get»`«ENDIF»«ENDIF»
				
			''');

		}

		private def toMd(String text) {
			return MarkdownFormatter.sanitize(text);
		}

		/**
		 * Level: one-indexed.
		 */
		private def titlePrefix(int level) {
			Preconditions.checkArgument(level >= 1, "The title level should be at least 1.");
			return String.join("", Collections.nCopies(Math.max(1, level + mainTitleLevel - 1), "#"))
		}
	}
	