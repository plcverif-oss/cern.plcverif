/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.emf;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * Wrapper class for EObjects to use {@link EcoreUtil#equals} as equality
 * check (and the corresponding {@code hashCode} implementation).
 */
public class EquatableEObject {
	private static final int NO_HASH_COMPUTED = -1;
	
	private EObject wrappedEObject;
	private int hash = NO_HASH_COMPUTED;

	public EquatableEObject(EObject wrappedEObject) {
		this.wrappedEObject = wrappedEObject;
	}

	public EObject getWrappedEObject() {
		return wrappedEObject;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof EquatableEObject) {
			EObject other = ((EquatableEObject) obj).getWrappedEObject();
			return (wrappedEObject == other) || EcoreUtil.equals(wrappedEObject, other);
		}
		return false;
	}

	@Override
	public int hashCode() {
		if (this.hash == NO_HASH_COMPUTED) {
			this.hash = EObjectHash.hashCode(wrappedEObject);			
		}
		return this.hash;
	}
}