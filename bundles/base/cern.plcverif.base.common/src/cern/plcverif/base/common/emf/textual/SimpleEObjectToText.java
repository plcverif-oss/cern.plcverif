/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.emf.textual;

import java.util.Optional;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;


public final class SimpleEObjectToText implements EObjectToText {
	private SimpleEObjectToText() {
		// singleton
	}
	
	public static final SimpleEObjectToText INSTANCE = new SimpleEObjectToText();
	
	@Override
	public String text(EObject e) {
		if (e == null) {
			return "<null>";
		} else {
			Optional<String> name = tryGetName(e);
			if (name.isPresent()) {
				return name.get();
			} else {
				return e.toString();
			}
		}
	}
	
	private static Optional<String> tryGetName(EObject e) {
		EStructuralFeature feature = e.eClass().getEStructuralFeature("name");
		if (feature != null) {
			Object val = e.eGet(feature); // normally this should be a String if exists
			if (val != null) {
				return Optional.of(val.toString());
			}
		}
		
		return Optional.empty();
	}
}
