/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.logging;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class PlatformLogger {
	public static final IPlcverifLogger INSTANCE = new IPlcverifLogger() {
		@Override
		public void log(PlcverifSeverity severity, String message, Throwable throwable) {
			switch (severity) {
			case Error:
				PlatformLogger.logError(message, throwable);
				break;
			case Warning:
				PlatformLogger.logWarning(message, throwable);
				break;
			case Info:
				PlatformLogger.logInfo(message);
				break;
			case Debug:
				PlatformLogger.logDebug(message);
				break;
			default:
				throw new UnsupportedOperationException("Unknown PLCverif severity: " + severity);
			}
		}

		@Override
		public void log(PlcverifSeverity severity, String messageFormat, Object... args) {
			String message = String.format(messageFormat, args);
			log(severity, message, (Throwable) null);
		}
	};

	private static final Logger LOGGER = LogManager.getLogger(PlatformLogger.class);
	
	private PlatformLogger() {
		// Utility class.
	}
	
	public static void logError(String message) {
		logError(message, null);
	}

	public static void logError(String message, Throwable exception) {
		LOGGER.error(message, exception);
	}

	public static void logWarning(String message) {
		logWarning(message, null);
	}

	public static void logWarning(String message, Throwable exception) {
		LOGGER.warn(message, exception);
	}

	public static void logInfo(String message) {
		LOGGER.info(message);
	}

	public static void logDebug(String message) {
		LOGGER.debug(message);
	}
	
	public static void logDebug(String message, Throwable exception) {
		LOGGER.debug(message, exception);
	}
}
