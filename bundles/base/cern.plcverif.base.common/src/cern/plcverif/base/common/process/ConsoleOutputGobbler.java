/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.process;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public final class ConsoleOutputGobbler extends Thread {
	// source of ideas:
	// https://www.securecoding.cert.org/confluence/display/java/FIO07-J.+Do+not+let+external+processes+block+on+IO+buffers

	private final BufferedReader inputReader;
	private StringBuilder inputStringBuilder = null;
	private String commentLineRegex = null;
	private String promtChar = ">";
	private boolean verbose = true;

	public ConsoleOutputGobbler(final InputStream inputStream, boolean verbose) {
		this.inputReader = new BufferedReader(new InputStreamReader(inputStream));
		this.verbose = verbose;
	}
	
	public void setPromtChar(String promtChar) {
		this.promtChar = promtChar;
	}

	@Override
	public void run() {
		final long startTime = System.nanoTime();
		this.inputStringBuilder = new StringBuilder();
		try {
			String line = null;
			synchronized (inputStringBuilder) {
				while ((line = this.inputReader.readLine()) != null) {
					// readLine() returns null once the end of the stream has ended
					this.inputStringBuilder.append(line);
					this.inputStringBuilder.append(System.lineSeparator());
					if ((commentLineRegex == null || !line.matches(commentLineRegex)) && verbose) {
						System.out.println(
								String.format("(%6dms)%s %s", (System.nanoTime() - startTime) / 1_000_000, promtChar, line));
					}
				}
			}
		} catch (final IOException x) {
			if (x.getMessage() != null && x.getMessage().equals("Stream closed")) {
				// This is a known issue.
				// On Linux this may happen when the backend terminates.
				// There is no need to do anything.
				this.inputStringBuilder.append("--- Stream closed ---");
				this.inputStringBuilder.append(System.lineSeparator());
				return;
			}
			
			// best effort
			x.printStackTrace();
		}
	}

	/**
	 * Returns the collected string.
	 * <p>
	 * The call blocks until the end of the stream has been reached.
	 * 
	 * @return The text collected from the given stream.
	 */
	public String getReadString() {
		if (this.inputStringBuilder != null) {
			synchronized (inputStringBuilder) {
				return this.inputStringBuilder.toString();
			}
		}

		return "N/A";
	}

	public void setCommentLineRegex(String regex) {
		this.commentLineRegex = regex;
	}
}
