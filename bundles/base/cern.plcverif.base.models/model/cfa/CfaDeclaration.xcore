/******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/

@GenModel(generateModelWizard="false",
	modelPluginID="cern.plcverif.base.models",
	modelDirectory="cern.plcverif.base.models/src-gen",
	complianceLevel="8.0", 
	copyrightText="Copyright (C) 2013-2019 CERN. All rights reserved.\r\n \r\nThis file is part of the PLCverif project.",
	resource="XMI",
	updateClasspath="false") 
package cern.plcverif.base.models.cfa.cfadeclaration

import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.LeftValue
import cern.plcverif.base.models.expr.Type
import cern.plcverif.base.models.expr.ComparisonOperator
import cern.plcverif.base.models.cfa.cfabase.NamedElement
import cern.plcverif.base.models.cfa.cfabase.Annotation
import cern.plcverif.base.models.cfa.cfabase.ArrayDimension
import cern.plcverif.base.models.cfa.cfabase.ArrayType
import cern.plcverif.base.models.cfa.cfabase.CfaNetworkBase
import cern.plcverif.base.models.cfa.cfabase.AutomatonBase
import cern.plcverif.base.models.cfa.cfabase.LoopLocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.Transition
import cern.plcverif.base.models.cfa.cfabase.DataDirection
import cern.plcverif.base.models.expr.ExplicitlyTyped
import cern.plcverif.base.models.cfa.cfabase.Freezable
import cern.plcverif.base.models.cfa.cfabase.Annotable
import cern.plcverif.base.models.cfa.utils.CfaDeclarationUtils

// Types & fields
// --------------

// Data structures

@GenModel(documentation="Class to represent a complex type definition with fields and other embedded complex types.
	Note that a DataStructure is not a Type and shall be unique in the model. Use DataStructureRef if you need a Type.")
class DataStructure extends NamedElement, Annotable {
	@GenModel(documentation="The collection of fields that define the layout of this data structure. May be empty.")
	contains Field [*] fields opposite enclosingType
	
	@GenModel(documentation="The collection of data structures nested in this data structure.")
	contains DataStructure [*] complexTypes opposite enclosingType
	
	@GenModel(documentation="The data structure in which the current data structure is nested. If null, 
		the data structure is the global root data structure of a CFA network.")
	refers DataStructure [0..1] enclosingType opposite complexTypes
	
	@GenModel(documentation="Annotations of the data structure. Used for code generation and traceability.")
	contains DataStructureAnnotation [*] annotations opposite parentDataStructure
}

@GenModel(documentation="Class to represent a complex type defined by a DataStructure.")
class DataStructureRef extends Type {
	@GenModel(documentation="The DataStrcture that defines the type.")
	refers DataStructure [1] definition
	
	@GenModel(documentation="Two data structure references are equal if they have the same definition (which should be unique).")
	op boolean dataTypeEquals(Type other) {
		if (other instanceof DataStructureRef) {
			return definition == other.definition
		}
		return false
	}
}

// Fields

@GenModel(documentation="Represents a data field of a complex data structure. Note that fields are unique in the model,
	referencing them is possible via FieldRef instances, which may be copied. This element can be frozen (see Freezable).")
class Field extends NamedElement, ExplicitlyTyped, Freezable, Annotable {
	@GenModel(documentation="The data structure that defines this field.")
	container DataStructure [1] enclosingType opposite fields
	
	@GenModel(documentation="The set of initial assignments related to this field. In addition to setting the initial value 
		of fields of elementary type, it is also possible to set elements of arrays or override the initial values of the 
		subfields for this specific instance in case this is a field of complex type. Use 
		cern.plcverif.base.models.cfa.utils.ArrayInitialValueHelper to read and process the initial values of multidimensional
		arrays (arrays of arrays).")
	contains VariableAssignment [*] initialAssignments
	
	contains VariableComparison [*] bounds
	
	@GenModel(documentation="Annotations of the field. Used for code generation and traceability.")
	contains FieldAnnotation[*] annotations opposite parentField
	
	// Only for debugging.
	// It is also implemented in cern.plcverif.base.models.cfa.utils.CfaToString
	op String toString() {
		return displayName + " : " + ^type.toString()
	}
}

// References
// ----------

// NOTE maybe introduce ThisRef and GlobalRef
@GenModel(documentation="Represents a reference to a piece of data. In the CFA declaration metamodel, data elements are only
	declared and not instantiated, so a reference has to specify a path to navigate through the types and specify an instance
	of a field or an array element.")
abstract class DataRef extends LeftValue, Freezable {
	@GenModel(documentation="The DataRef identifying the enclosing complex data type (an array or a data structure). 
		If not specified, the current reference is assumed to refer to a field in the local or global data structure
		(see CfaNetworkDeclaration and AutomatonDeclaration).")
	// VALIDATION: if we have a scope (CfaNetworkDeclaration or AutomatonDeclaration) and the prefix is not set, then
	// the current DataRef should be a FieldRef (ArrayType as global or local data context is not supported) and the referred
	// field should be a member of one of the scope data structures.
	contains DataRef [0..1] prefix
}

@GenModel(documentation="Represents a reference to a field. Along with the prefix and the local or global data structure, it
	identifies a specific instance of the field.")
class FieldRef extends DataRef {
	
	@GenModel(documentation="The unique field to which this FieldRef refers. The enclosing type of the field should always
		be the type of the prefix DataRef (if any).")
	refers Field [1] field
	
	@GenModel(documentation="The type of a FieldRef is always the type of the referred field.")
	op Type getType() {
		return field.^type
	}
	
	// Only for debugging.
	// It is also implemented in cern.plcverif.base.models.cfa.utils.CfaToString
	op String toString() {
		return (if (prefix === null) "" else (prefix.toString() + "/")) + field.displayName
	}
}

// VALIDATION: prefix must not be null (we have to know what to index). If the index is a literal, it should be a valid index. 
@GenModel(documentation="Represents a reference to an element of an array. Along with the prefix and the local or global data 
	structure, it identifies a specific instance of this element.")
class Indexing extends DataRef {
	@GenModel(documentation="The index of the referenced element in the enclosing array. It is expected to have a signed 16-bit integer type.")
	contains Expression [1] index
	
	@GenModel(documentation="Returns the dimension of the enclosing array.")
	refers derived ArrayDimension [1] dimension get {
		return (prefix.^type as ArrayType).dimension
	}
	
	@GenModel(documentation="Returns the element type of the enclosing array.")
	op Type getType() {
		return (prefix.^type as ArrayType).elementType
	}
	
	// Only for debugging.
	// It is also implemented in cern.plcverif.base.models.cfa.utils.CfaToString
	op String toString() {
		return (if (prefix === null) "null" else prefix.toString()) + "[" + index.toString() + "]"
	}
}

// CFA network declaration
// -----------------------

@GenModel(documentation="Class to represent the declaration of a network of control flow automata (CFA).")
class CfaNetworkDeclaration extends CfaNetworkBase {
	@GenModel(documentation="Automata in the network.")
	contains AutomatonDeclaration [1..*] automata opposite ^container
	
	@GenModel(documentation="Automaton which starts the execution.")
	refers AutomatonDeclaration [1] mainAutomaton
	
	@GenModel(documentation="The root data structure containing every other data structure and field. Fields of this 
		data structure are considered global.")
	contains DataStructure [1] rootDataStructure
	
	@GenModel(documentation="The variable representing the local context of the main CFA.")
	contains DataRef [1] mainContext
	
	@GenModel(documentation="Returns true if the specified DataRef starts in the global root data structure.")
	op boolean isGlobal(DataRef reference) {
		return CfaDeclarationUtils.isGlobal(reference, this);
	}
}

// Automaton declaration
// ---------------------
	
@GenModel(documentation="Class to represent the declaration of a control flow automaton.")
class AutomatonDeclaration extends AutomatonBase {
	@GenModel(documentation="Automaton system to which this automaton belongs.")
	container CfaNetworkDeclaration [1] ^container opposite automata
	
	@GenModel(documentation="A reference to the local data structure that serves as the context for local references.")
	contains DataStructureRef [1] localDataStructure
	
	@GenModel(documentation="Returns true if the specified DataRef starts in the local data structure.")
	op boolean isLocal(DataRef reference) {
		var ref = reference
		while (ref.prefix !== null) {
			ref = ref.prefix
		}
		return ref instanceof FieldRef && (ref as FieldRef).field.enclosingType == localDataStructure.definition
	}
}

// Special transitions
// -------------------

@GenModel(documentation="Class to represent a (potentially guarded) CFA declaration transition with assignment.")
class AssignmentTransition extends Transition {
	@GenModel(documentation="Variable assignments to perform when the transition fires.")
	contains VariableAssignment[*] assignments
	contains Expression[*] assumeBounds
}

@GenModel(documentation="Class to represent a (potentially guarded) CFA declaration transition with an automaton call. 
	Semantically, this is equivalent to copying the called automata here, replacing this transition, and
	inlining the input and output assignments before and after the call.")
class CallTransition extends Transition {
	@GenModel(documentation="The calls performed upon transition. If it contains multiple calls, it is a parallel composition.
		In that case, all input assignments are performed before any of the executions and all output assignments are performed
		after all executions.")
	contains Call[1..*] calls opposite parentTransition
}

@GenModel(documentation="Class to represent a CFA automaton declaration call.")
class Call {
	@GenModel(documentation="Transition which contains the call.")
	container CallTransition [1] parentTransition opposite calls
	
	@GenModel(documentation="Automaton to be executed.")
	refers AutomatonDeclaration [1] calledAutomaton
	
	@GenModel(documentation="The context in which the function must be called, defining local variables.")
	contains DataRef [1] calleeContext
	
	@GenModel(documentation="Input assignments of the called automaton. Performed in arbitrary order, before executing 'calledAutomaton'.")
	contains VariableAssignment[*] inputAssignments
	
	@GenModel(documentation="Output assignments of the called automaton. Performed in arbitrary order, after executing 'calledAutomaton'.")
	contains VariableAssignment[*] outputAssignments
}

@GenModel(documentation="Class to represent a variable assignment in the CFA declaration.")
// Note: CfaDeclarationUtils.deleteVariableAssignment assumes that a 
// VariableAssignment (or any of its super types) is never referred, only contained.
class VariableAssignment extends Freezable {
	contains DataRef [1] leftValue
	contains Expression [1] rightValue
}

@GenModel(documentation="Class to represent a variable comparison in the CFA declaration.")
// Note: CfaDeclarationUtils.deleteVariableComparison assumes that a 
// VariableComparison (or any of its super types) is never referred, only contained.
class VariableComparison extends Freezable {
	ComparisonOperator [1] comparator
	contains DataRef [1] leftValue
	contains Expression [1] rightValue
}

// Annotations
// -----------

// Data structure

@GenModel(documentation="Abstract superclass for data structure annotations.")
abstract class DataStructureAnnotation extends Annotation {
	refers DataStructure parentDataStructure opposite annotations
}

@GenModel(documentation="Data structure annotation to trace the type from which this data structure was transformed.")
class SourceTypeDataStructureAnnotation extends DataStructureAnnotation {
	String typename
	DataStructureSourceType sourceType
}

@GenModel(documentation="Represents the different kinds of PLC structures that are transformed into data structures.")
enum DataStructureSourceType {
	TYPE, DATA_BLOCK, FUNCTION_BLOCK, ORGANIZATION_BLOCK, FUNCTION
}

// Field

@GenModel(documentation="Abstract superclass for field annotations.")
abstract class FieldAnnotation extends Annotation {
	container Field [1] parentField opposite annotations
}

@GenModel(documentation="Field annotation to indicate mode in which the annotated field should be interpreted or used.")
class DirectionFieldAnnotation extends FieldAnnotation {
	DataDirection [1] direction
}

@GenModel(documentation="Field annotation to trace the original data type of the field in the PLC code.")
class OriginalDataTypeFieldAnnotation extends FieldAnnotation {
	String [1] plcDataType
}

@GenModel(documentation="Field annotation to indicate that the annotated field is used to store return values of an automaton.")
class ReturnValueFieldAnnotation extends FieldAnnotation {
}

@GenModel(documentation="Field annotation to indicate that the annotated field was created for internal purposes and it does not correspond to any of the source program's fields/variables.")
class InternalGeneratedFieldAnnotation extends FieldAnnotation {
}

// Location

@GenModel(documentation="Location annotation to represent a FOR loop statement that starts at the given location.")
class ForLoopLocationAnnotation extends LoopLocationAnnotation {
	contains DataRef loopVariable
	refers Transition loopVariableInit
	refers Transition loopVariableIncrement
}
