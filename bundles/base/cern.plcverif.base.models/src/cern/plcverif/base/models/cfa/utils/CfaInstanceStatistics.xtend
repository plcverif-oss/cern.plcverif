package cern.plcverif.base.models.cfa.utils

import cern.plcverif.base.models.cfa.cfainstance.AutomatonInstance
import cern.plcverif.base.models.cfa.cfainstance.Call
import cern.plcverif.base.models.cfa.cfainstance.CallTransition
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance
import java.math.BigInteger
import java.util.Map
import org.eclipse.xtend.lib.annotations.FinalFieldsConstructor

/**
 * Class to provide various statistics about the represented CFA instance
 */
@FinalFieldsConstructor
class CfaInstanceStatistics {
	static final int NOT_COMPUTED_YET = -1;

	final CfaNetworkInstance cfi;
	long locations = NOT_COMPUTED_YET;
	Map<AutomatonInstance, Long> inlinedLocations = newHashMap();
	long transitions = NOT_COMPUTED_YET;
	int sumVariableBits = NOT_COMPUTED_YET;

	def BigInteger potentialStateSpace() {
		// RESULT = #locations * 2 ^ (totalVarBits) 
		return BigInteger.valueOf(getLocations()).multiply(BigInteger.valueOf(2).pow(totalVariableBits));
	}

	def double potentialStateSpaceDouble() {
		return potentialStateSpace.doubleValue;
	}

	/**
	 * Returns the total number of locations in the represented CFA instance.
	 */
	def long getLocations() {
		if (locations == NOT_COMPUTED_YET) {
			locations = 0;
			for (automaton : cfi.automata) {
				locations += automaton.locations.size;
			}
		}

		return locations;
	}

	/**
	 * Returns the total number of locations in the represented CFA instance if it was fully inlined.
	 */
	def long getInlinedLocations() {
		return getInlinedLocations(cfi.mainAutomaton);
	}

	private def long getInlinedLocations(AutomatonInstance automaton) {
		if (!inlinedLocations.containsKey(automaton)) {
			var long result = automaton.locations.size;
			
			// Calculate all called automaton transitively as if inlined			
			var long n = 0; 
			for (Call call : automaton.transitions.filter(CallTransition).map[it | it.calls].flatten) {
				if (n == 0) {
					n = getInlinedLocations(call.calledAutomaton);
				} else {
					n *= getInlinedLocations(call.calledAutomaton);
				}
				// It can be an infinite loop, but only if there is a circular 
				// call trace in the code, which cannot be handled in CFA anyway.
			}
			result += n;
			
			inlinedLocations.put(automaton, result);
		}

		return inlinedLocations.get(automaton);
	}

	/**
	 * Returns the total number of transitions in the represented CFA instance.
	 */
	def long getTransitions() {
		if (transitions == NOT_COMPUTED_YET) {
			transitions = 0;
			for (automaton : cfi.automata) {
				transitions += automaton.transitions.size;
			}
		}

		return transitions;
	}

	/**
	 * Returns the total number of bits occupied by all the variables in 
	 * the represented CFA instance.
	 */
	def int getTotalVariableBits() {
		if (sumVariableBits == NOT_COMPUTED_YET) {
			sumVariableBits = 0;
			for (variable : cfi.variables) {
				sumVariableBits += CfaUtils.sizeInBits(variable.type);
			}
		}

		return sumVariableBits;
	}
}
