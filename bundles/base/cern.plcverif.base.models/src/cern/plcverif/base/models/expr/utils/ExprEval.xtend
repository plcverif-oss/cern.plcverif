/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.expr.utils

import cern.plcverif.base.models.expr.AtomicExpression
import cern.plcverif.base.models.expr.BinaryArithmeticExpression
import cern.plcverif.base.models.expr.BinaryExpression
import cern.plcverif.base.models.expr.BinaryLogicExpression
import cern.plcverif.base.models.expr.BoolLiteral
import cern.plcverif.base.models.expr.BoolType
import cern.plcverif.base.models.expr.ComparisonExpression
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.ExpressionSafeFactory
import cern.plcverif.base.models.expr.FloatLiteral
import cern.plcverif.base.models.expr.FloatType
import cern.plcverif.base.models.expr.IntLiteral
import cern.plcverif.base.models.expr.IntType
import cern.plcverif.base.models.expr.Literal
import cern.plcverif.base.models.expr.StringLiteral
import cern.plcverif.base.models.expr.TypeConversion
import cern.plcverif.base.models.expr.UnaryArithmeticExpression
import cern.plcverif.base.models.expr.UnaryArithmeticOperator
import cern.plcverif.base.models.expr.UnaryExpression
import cern.plcverif.base.models.expr.UnaryLogicExpression
import com.google.common.base.Preconditions
import java.util.Map

import static cern.plcverif.base.models.expr.ExprOperationResult.*
import static org.eclipse.emf.ecore.util.EcoreUtil.copy
import static org.eclipse.emf.ecore.util.EcoreUtil.equals
import java.math.BigInteger

/**
 * Expression evaluator. It can be used to evaluate given expressions
 * using a set of given valuations.
 */
class ExprEval {
	/**
	 * Valuations for some atomic expressions. May be null if no valuation is given.
	 */
	Map<? extends AtomicExpression, Literal> valuations;

	/**
	 * Creates a new expression evaluator without any known valuation. Essentially,
	 * the newly created evaluator can only be used to evaluate constant expressions,
	 * which contain only literals as atomic expressions (e.g., {@code 1 + 2 * 3}).  
	 */
	new() {
		this.valuations = null;
	}

	/**
	 * Creates a new expression evaluator with the given valuations. 
	 * The atomic expressions in the evaluated expressions will be replaced by the
	 * corresponding literals from the given valuations.
	 * 
	 * @param valuations 
	 *      Valuations to be used for the evaluation. 
	 * 		The valuations will be checked using the {@link Map#get(Object)} method.
	 * 		As in case of EMF models the {@link Object#equals(Object)} method is a
	 * 		reference comparison, make sure to use the 
	 *      {@link cern.plcverif.base.common.emf.EObjectMap} class if
	 * 		value-based comparison is desired.  
	 */
	new(Map<? extends AtomicExpression, Literal> valuations) {
		this.valuations = Preconditions.checkNotNull(valuations);
		this.valuations.values.forEach [ it |
			Preconditions.checkNotNull(it, "The valuation map cannot contain null literal as value.")
		];
	}

	// compute()
	// ---------
	/**
	 * Computes every computable subexpression within the specified expression and returns a new expression with the
	 * computed subexpressions substituted with a literal. If the whole expression is computable, the result itself 
	 * is a {@link Literal}.
	 * 
	 * @param expr The expression to compute.
	 * @return A new expression with all computable subexpressions substituted with a literal.
	 */
	def dispatch Expression compute(Expression expr) {
		if (isComputableConstant(expr)) {
			return literalRepresentation(expr);
		} else {
			return expr;
		}
	}

	def dispatch Expression compute(BinaryExpression expr) {
		if (isComputableConstant(expr)) {
			return literalRepresentation(expr);
		} else {
			expr.setLeftOperand(compute(expr.leftOperand));
			expr.setRightOperand(compute(expr.rightOperand));
			return expr;
		}
	}

	def dispatch Expression compute(UnaryExpression expr) {
		if (isComputableConstant(expr)) {
			return literalRepresentation(expr);
		} else {
			expr.setOperand(compute(expr.operand));
			return expr;
		}
	}

	// isComputableConstant()
	// ------------
	/**
	 * Checks if the expression can be fully computed to reduce it into a single constant (literal) expression.
	 * 
	 * @param expr The expression to check.
	 * @return True, if all atomic expressions are literals, false otherwise.
	 */
	def dispatch boolean isComputableConstant(Expression expr) {
		throw new UnsupportedOperationException("Unknown expression type: " + expr.class.name);
	}
	
	def dispatch boolean isComputableConstant(Void expr) {
		return false;
	}

	/**
	 * Checks if the expression can be fully computed to reduce it into a single constant (literal) expression.
	 * 
	 * @param expr The expression to check.
	 * @return True, if all atomic expressions are literals, false otherwise.
	 */
	def dispatch boolean isComputableConstant(BinaryExpression expr) {
		return isComputableConstant(expr.leftOperand) && isComputableConstant(expr.rightOperand)
	}

	/**
	 * Checks if the expression can be fully computed to reduce it into a single constant (literal) expression.
	 * 
	 * @param expr The expression to check.
	 * @return True, if all atomic expressions are literals, false otherwise.
	 */
	def dispatch boolean isComputableConstant(UnaryExpression expr) {
		return isComputableConstant(expr.operand)
	}

	/**
	 * Checks if the expression can be fully computed to reduce it into a single constant (literal) expression.
	 * 
	 * @param expr The expression to check.
	 * @return True, if all atomic expressions are literals, false otherwise.
	 */
	def dispatch boolean isComputableConstant(AtomicExpression exp) {
		return hasValuation(exp);
	}

	/**
	 * Checks if the expression can be fully computed to reduce it into a single constant (literal) expression.
	 * 
	 * @param expr The expression to check.
	 * @return True, if all atomic expressions are literals, false otherwise.
	 */
	def dispatch boolean isComputableConstant(Literal exp) {
		return true;
	}

	def Literal literalRepresentation(Expression expr) {
		Preconditions.checkArgument(isComputableConstant(expr));

		if (expr instanceof AtomicExpression && hasValuation(expr as AtomicExpression)) {
			return getValuation(expr as AtomicExpression);
		}

		if (expr instanceof Literal) {
			return expr;
		}

		val t = expr.type;
		switch (t) {
			BoolType:
				return ExpressionSafeFactory.INSTANCE.createBoolLiteral(toBoolean(expr), copy(t))
			IntType:
				return ExpressionSafeFactory.INSTANCE.createIntLiteral(toLong(expr), copy(t))
			FloatType:
				return ExpressionSafeFactory.INSTANCE.createFloatLiteral(toDouble(expr), copy(t))
			default:
				throw new UnsupportedOperationException("Unsupported type for a compute(Expression): " + t)
		}
	}

	// Literal conversions
	// toLong()
	def dispatch long toLong(Expression expr) {
		throw new IllegalArgumentException(
			"The following type of expression cannot be converted into a long value: " + expr.class.name);
	}

	def dispatch long toLong(BinaryArithmeticExpression expr) {
		return operationResult(toLong(expr.leftOperand), toLong(expr.rightOperand), expr.operator);
	}

	def dispatch long toLong(UnaryArithmeticExpression expr) {
		if (expr.operator == UnaryArithmeticOperator.BITWISE_NOT) {
			if (ExprUtils.isIntLiteral(expr.operand)) {
				return bwNotResult(expr.operand as IntLiteral);
			} else {
				throw new UnsupportedOperationException(
					"Impossible to compute the bitwise not for a non-integer literal.");
			}
		}
		return operationResult(toLong(expr.operand), expr.operator);
	}

	def dispatch long toLong(TypeConversion expr) {
		val operandType = expr.operand.type;
		val targetType = expr.type;
		Preconditions.checkArgument(targetType instanceof IntType, "Non-integer type cannot be evaluated to long");
		
		switch (operandType) {
			IntType: return handleOverflow(toLong(expr.operand), targetType as IntType) 
			FloatType: return toDouble(expr.operand) as long
			default: throw new IllegalArgumentException("Impossible type conversion.")
		}
	}
	
	private def long handleOverflow(long value, IntType type) {
		val minValue = BigInteger.valueOf(TypeUtils.minValue(type));
		val maxValue = BigInteger.valueOf(TypeUtils.maxValue(type));
		val range = maxValue.subtract(minValue).add(BigInteger.ONE);
		
		// result = (value - MIN) % RANGE + MIN
		var result = BigInteger.valueOf(value).subtract(minValue).mod(range).add(minValue);
		return result.longValue;
	}

	def dispatch long toLong(AtomicExpression expr) {
		if (hasValuation(expr)) {
			val literal = getValuation(expr);
			if (literal instanceof IntLiteral) {
				return literal.value;
			}
		}

		throw new IllegalArgumentException(
			"The following type of expression cannot be converted into a long value: " + expr.class.name);
	}

	def dispatch long toLong(IntLiteral expr) {
		return expr.value
	}

	// toDouble()
	def dispatch double toDouble(Expression expr) {
		throw new IllegalArgumentException(
			"The following type of expression cannot be converted into a double value: " + expr.class.name);
	}

	def dispatch double toDouble(BinaryArithmeticExpression expr) {
		return operationResult(toDouble(expr.leftOperand), toDouble(expr.rightOperand), expr.operator);
	}

	def dispatch double toDouble(UnaryArithmeticExpression expr) {
		return operationResult(toDouble(expr.operand), expr.operator);
	}

	def dispatch double toDouble(TypeConversion expr) {
		switch (expr.operand.type) {
			IntType: return toLong(expr.operand)
			FloatType: return toDouble(expr.operand)
			default: throw new IllegalArgumentException("Impossible type conversion.")
		}
	}

	def dispatch double toDouble(AtomicExpression expr) {
		if (hasValuation(expr)) {
			val literal = getValuation(expr);
			if (literal instanceof FloatLiteral) {
				return literal.value;
			}
		}

		throw new IllegalArgumentException(
			"The following type of expression cannot be converted into a double value: " + expr.class.name);
	}

	def dispatch double toDouble(FloatLiteral expr) {
		return expr.value
	}

	// toBoolean()
	def dispatch boolean toBoolean(Expression expr) {
		throw new IllegalArgumentException(
			"The following type of expression cannot be converted into a Boolean value: " + expr.class.name);
	}

	def dispatch boolean toBoolean(BinaryLogicExpression expr) {
		return operationResult(toBoolean(expr.leftOperand), toBoolean(expr.rightOperand), expr.operator);
	}

	def dispatch boolean toBoolean(UnaryLogicExpression expr) {
		return operationResult(toBoolean(expr.operand), expr.operator);
	}

	def dispatch boolean toBoolean(ComparisonExpression expr) {
		val left = expr.leftOperand;
		val right = expr.rightOperand;

		if (!equals(left.type, right.type)) {
			throw new IllegalArgumentException("Illegal comparison expression: operands with different types.");
		}

		switch (left.type) {
			BoolType:
				return operationResult(toBoolean(left), toBoolean(right), expr.operator)
			IntType:
				return operationResult(toLong(left), toLong(right), expr.operator)
			FloatType:
				return operationResult(toDouble(left), toDouble(right), expr.operator)
			default:
				throw new UnsupportedOperationException("Unsupported operator: " + left.type)
		}
	}

	def dispatch boolean toBoolean(AtomicExpression expr) {
		if (hasValuation(expr)) {
			val literal = getValuation(expr);
			if (literal instanceof BoolLiteral) {
				return literal.value;
			}
		}

		throw new IllegalArgumentException(
			"The following type of expression cannot be converted into a Boolean value: " + expr.class.name);
	}

	def dispatch boolean toBoolean(BoolLiteral expr) {
		return expr.value;
	}

	def double toStringValue(Expression expr) {
		if (expr instanceof StringLiteral == false) {
			throw new IllegalArgumentException(
				"The following type of expression cannot be converted into a string value: " + expr.class.name);
		} else {
			return toStringValue(expr);
		}
	}

	def String toStringValue(StringLiteral expr) {
		return expr.value
	}

	private def boolean hasValuation(AtomicExpression e) {
		if (e instanceof Literal) {
			return true;
		}
		
		return this.valuations !== null && this.valuations.containsKey(e);
	}

	private def Literal getValuation(AtomicExpression e) {
		if (e instanceof Literal) {
			return e;
		}
		
		Preconditions.checkNotNull(this.valuations, "No valuation is known to this ExprEval object.");

		val Literal result = this.valuations.get(e);
		Preconditions.checkNotNull(result, "getValuation cannot find a valuation, null was returned by get().");
		return result;
	}

}
