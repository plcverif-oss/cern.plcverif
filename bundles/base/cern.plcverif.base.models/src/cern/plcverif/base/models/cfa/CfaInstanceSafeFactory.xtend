/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.models.cfa

import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.base.models.Guava21Workaround
import cern.plcverif.base.models.cfa.cfabase.ArrayDimension
import cern.plcverif.base.models.cfa.cfabase.DataDirection
import cern.plcverif.base.models.cfa.cfabase.Location
import cern.plcverif.base.models.cfa.cfabase.Transition
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariable
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariableRef
import cern.plcverif.base.models.cfa.cfainstance.ArrayElementInitialValue
import cern.plcverif.base.models.cfa.cfainstance.ArrayElementRef
import cern.plcverif.base.models.cfa.cfainstance.ArrayVariable
import cern.plcverif.base.models.cfa.cfainstance.AssignmentTransition
import cern.plcverif.base.models.cfa.cfainstance.AutomatonInstance
import cern.plcverif.base.models.cfa.cfainstance.Call
import cern.plcverif.base.models.cfa.cfainstance.CallTransition
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance
import cern.plcverif.base.models.cfa.cfainstance.CfainstanceFactory
import cern.plcverif.base.models.cfa.cfainstance.DirectionVariableAnnotation
import cern.plcverif.base.models.cfa.cfainstance.ForLoopLocationAnnotation
import cern.plcverif.base.models.cfa.cfainstance.FullyQualifiedName
import cern.plcverif.base.models.cfa.cfainstance.InternalGeneratedVariableAnnotation
import cern.plcverif.base.models.cfa.cfainstance.OriginalDataTypeVariableAnnotation
import cern.plcverif.base.models.cfa.cfainstance.ReturnValueVariableAnnotation
import cern.plcverif.base.models.cfa.cfainstance.Variable
import cern.plcverif.base.models.cfa.cfainstance.VariableAssignment
import cern.plcverif.base.models.cfa.cfainstance.VariableRef
import cern.plcverif.base.models.cfa.utils.CfaTypeUtils
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.InitialValue
import cern.plcverif.base.models.expr.IntLiteral
import cern.plcverif.base.models.expr.IntType
import cern.plcverif.base.models.expr.Type
import cern.plcverif.base.models.expr.utils.ExprChecks
import cern.plcverif.base.models.expr.utils.ExprUtils
import com.google.common.base.Preconditions
import java.util.List
import org.eclipse.emf.ecore.util.EcoreUtil

import static cern.plcverif.base.models.cfa.utils.CfaTypeUtils.haveSameType

class CfaInstanceSafeFactory extends CfaBaseSafeFactory {
	public static val INSTANCE = new CfaInstanceSafeFactory(DEFAULT_STRICT);
	
	protected new(boolean strict) {
		super(strict);
	}
	
	def static CfaInstanceSafeFactory create(boolean strict) {
		return new CfaInstanceSafeFactory(strict);
	}
	
	// CFA high-level objects
	// ----------------------
	
	def CfaNetworkInstance createCfaNetworkInstance(String displayName) {
		Preconditions.checkNotNull(displayName, "displayName");
		
		val ret = CfainstanceFactory.eINSTANCE.createCfaNetworkInstance();
		ret.name = LocalIdGenerator.INSTANCE.sanitizeName(displayName);
		ret.displayName = displayName;
		return ret;
	}
	
	def AutomatonInstance createAutomatonInstance(String displayName, CfaNetworkInstance cfaNetwork) {	
		Preconditions.checkNotNull(displayName, "displayName");
		Preconditions.checkNotNull(cfaNetwork, "cfaNetwork");
			
		val ret = CfainstanceFactory.eINSTANCE.createAutomatonInstance();
		ret.name = LocalIdGenerator.INSTANCE.generateId(cfaNetwork, displayName);
		ret.displayName = displayName;
		ret.container = cfaNetwork;
		return ret;
	}
	
	// Transitions
	// -----------
	
	def AssignmentTransition createAssignmentTransition(String displayName, AutomatonInstance parentAutomaton, Location source, Location target, Expression condition) {
		Preconditions.checkNotNull(displayName, "displayName");
		Preconditions.checkNotNull(parentAutomaton, "parentAutomaton");
		Preconditions.checkNotNull(source, "source");
		Preconditions.checkNotNull(target, "target");
		Preconditions.checkNotNull(condition, "condition");
		EmfHelper.checkNotContained(condition, "The provided expression is already assigned to another model element. Copy it with EcoreUtils.copy().");
		ExprChecks.checkBoolType(condition, "The provided expression is not of type Boolean. Received type: %s", condition.type.toString());
				
		val ret = CfainstanceFactory.eINSTANCE.createAssignmentTransition();
		ret.name = LocalIdGenerator.INSTANCE.generateId(parentAutomaton, displayName);
		ret.displayName = displayName;
		ret.parentAutomaton = parentAutomaton;
		ret.source = source;
		ret.target = target;
		ret.condition = condition;
		return ret;
	}
	
	def CallTransition createCallTransition(String displayName, AutomatonInstance parentAutomaton, Location source, Location target, Expression condition) {
		Preconditions.checkNotNull(displayName, "displayName");
		Preconditions.checkNotNull(parentAutomaton, "parentAutomaton");
		Preconditions.checkNotNull(source, "source");
		Preconditions.checkNotNull(target, "target");
		Preconditions.checkNotNull(condition, "condition");
		EmfHelper.checkNotContained(condition, "The provided expression is already assigned to another model element. Copy it with EcoreUtils.copy().");
		ExprChecks.checkBoolType(condition, "The provided expression is not of type Boolean. Received type: %s", condition.type.toString());
				
		val ret = CfainstanceFactory.eINSTANCE.createCallTransition();
		ret.name = LocalIdGenerator.INSTANCE.generateId(parentAutomaton, displayName);
		ret.displayName = displayName;
		ret.parentAutomaton = parentAutomaton;
		ret.source = source;
		ret.target = target;
		ret.condition = condition;
		return ret;
	}
	
	def Call createCall(CallTransition parentTransition, AutomatonInstance calledAutomaton) {	
		Preconditions.checkNotNull(parentTransition, "parentTransition");
		Preconditions.checkNotNull(calledAutomaton, "calledAutomaton");
			
		val ret = CfainstanceFactory.eINSTANCE.createCall();
		ret.parentTransition = parentTransition;
		ret.calledAutomaton = calledAutomaton;
		return ret;
	}
	
	def VariableAssignment createInputAssignment(Call call, VariableRef leftValue, Expression rightValue) {
		Preconditions.checkNotNull(call, "call");
		
		val ret = createAssignment(leftValue, rightValue);
		call.inputAssignments.add(ret);
		return ret;
	}
	
	def VariableAssignment createOutputAssignment(Call call, VariableRef leftValue, Expression rightValue) {
		Preconditions.checkNotNull(call, "call");
		
		val ret = createAssignment(leftValue, rightValue);
		call.outputAssignments.add(ret);
		return ret;
	}
	
	def VariableAssignment createVariableAssignment(AssignmentTransition transition, VariableRef leftValue, Expression rightValue) {
		Preconditions.checkNotNull(transition, "transition");
		
		val ret = createAssignment(leftValue, rightValue);
		transition.assignments.add(ret);
		return ret;
	}
	
	def VariableAssignment createAssignment(AbstractVariableRef leftValue, Expression rightValue) {
		Preconditions.checkNotNull(leftValue, "leftValue");
		Preconditions.checkNotNull(rightValue, "rightValue");
		EmfHelper.checkNotContained(leftValue, "The provided left value is already assigned to another model element. Copy it with EcoreUtils.copy().");
		EmfHelper.checkNotContained(rightValue, "The provided right value is already assigned to another model element. Copy it with EcoreUtils.copy().");
		ExprChecks.checkSameTypes(leftValue, rightValue, "The left and right values do not have the same type. If the two types are compatible, use explicit casting. Left value type: %s. Right value type: %s.", leftValue.type, rightValue.type);
		//Preconditions.checkArgument(leftValue.type.dataTypeEquals(rightValue.type), "The left and right values do not have the same type. If the two types are compatible, use explicit casting. Left value type: " + leftValue.type + ". Right value type: " + rightValue.type)
				
		val ret = CfainstanceFactory.eINSTANCE.createVariableAssignment();
		ret.leftValue = leftValue;
		ret.rightValue = rightValue;
		return ret;
	}
	
	def Expression createAssumeBound(Expression bound) {
		Preconditions.checkNotNull(bound, "bound");
		EmfHelper.checkNotContained(bound, "The provided bound value is already assigned to another model element. Copy it with EcoreUtils.copy().");
				
		return bound;
	}
	
	// Data structures
	// ---------------
	
	def FullyQualifiedName createFullyQualifiedName(List<String> identifiers) {
		Preconditions.checkNotNull(identifiers, "identifiers");
		for (identifier : identifiers) {
			Preconditions.checkNotNull(identifier, "element of identifier");
		}
		
		val ret = CfainstanceFactory.eINSTANCE.createFullyQualifiedName();
		ret.identifiers.addAll(identifiers);
		return ret;
	}
	
	def Variable createVariable(FullyQualifiedName fqn, CfaNetworkInstance cfaNetwork, Type type) {
		Preconditions.checkNotNull(fqn, "fqn");
		Preconditions.checkNotNull(cfaNetwork, "cfaNetwork");
		Preconditions.checkNotNull(type, "type");
		EmfHelper.checkNotContained(fqn, "The provided fully qualified name is already assigned to another model element. Do not use the same fully qualified name for multiple variables.");
		EmfHelper.checkNotContained(type, "The provided type is already assigned to another model element. Copy it with EcoreUtils.copy().");
		
		val ret = CfainstanceFactory.eINSTANCE.createVariable();
		ret.displayName = fqn.toString();
		ret.name = LocalIdGenerator.INSTANCE.generateId(cfaNetwork, ret.displayName);
		ret.fqn = fqn;
		ret.container = cfaNetwork;
		ret.type = type;
		return ret;
	}
	
	def Variable createVariable(List<String> identifiers, CfaNetworkInstance cfaNetwork, Type type) {
		return createVariable(createFullyQualifiedName(identifiers), cfaNetwork, type);
	}
	
	def ArrayVariable createArrayVariable(FullyQualifiedName fqn, CfaNetworkInstance cfaNetwork, Type type, List<ArrayDimension> dimensionsToCopy) {
		Preconditions.checkNotNull(fqn, "fqn");
		Preconditions.checkNotNull(cfaNetwork, "cfaNetwork");
		Preconditions.checkNotNull(type, "type");
		EmfHelper.checkNotContained(fqn, "The provided fully qualified name is already assigned to another model element. Do not use the same fully qualified name for multiple variables.");
		EmfHelper.checkNotContained(type, "The provided type is already assigned to another model element. Copy it with EcoreUtils.copy().");
		
		val ret = CfainstanceFactory.eINSTANCE.createArrayVariable();
		ret.displayName = fqn.toString();
		ret.name = LocalIdGenerator.INSTANCE.generateId(cfaNetwork, ret.displayName);
		ret.fqn = fqn;
		ret.container = cfaNetwork;
		ret.type = type;
		for (dimension : dimensionsToCopy) {
			Preconditions.checkNotNull(dimension, "element of dimensions");
			ret.dimensions.add(EcoreUtil.copy(dimension));
		}
		return ret;
	}
	
	def ArrayVariable createArrayVariable(List<String> identifiers, CfaNetworkInstance cfaNetwork, Type type, List<ArrayDimension> dimensionsToCopy) {
		return createArrayVariable(createFullyQualifiedName(identifiers), cfaNetwork, type, dimensionsToCopy);
	}
	
	def void setInitialValue(Variable variableToInitialize, InitialValue initialValue) {
		Preconditions.checkNotNull(variableToInitialize, "variableToInitialize");
		Preconditions.checkNotNull(initialValue, "initialValue");
		EmfHelper.checkNotContained(initialValue, "The provided value is already assigned to another model element. Copy it with EcoreUtils.copy().");
		CfaTypeUtils.checkArgumentsHaveSameType(variableToInitialize, initialValue, "Unable to set initial value.");
		
		variableToInitialize.initialValue = initialValue;
	}
	
	/**
	 * Creates a new initial value for the given array element.
	 * @param arrayToInitialize Array variable that contains the element to be initialized.
	 * @param indices Index of the variable element to be initialized.
	 * @param initialValue Initial value.
	 * @return The created initial value. It is already registered for the array to be initialized.
	 */
	def ArrayElementInitialValue createArrayElementInitialValue(ArrayVariable arrayToInitialize, List<Expression> indices, InitialValue initialValue) {
		Preconditions.checkNotNull(arrayToInitialize, "arrayToInitialize");
		Preconditions.checkNotNull(indices, "indices");
		Preconditions.checkNotNull(initialValue, "initialValue");
		Preconditions.checkArgument(indices.length == arrayToInitialize.dimensions.size,
			"%d indices were specified, but the array variable has %d dimensions.", 
				indices.length, arrayToInitialize.dimensions.size);
		for (var i = 0; i < indices.size; i++) {
			val index = indices.get(i);
			Preconditions.checkNotNull(index, "element of indices");
			EmfHelper.checkNotContained(index, "One of the provided indices is already assigned to another model element. Copy it with EcoreUtils.copy().");
			Guava21Workaround.checkArgument(index.type instanceof IntType, "Arrays can be indexed with integer expressions only. Received type: %s.", index.type);
			val indexType = index.type as IntType;
			Guava21Workaround.checkArgument(indexType.isSigned && indexType.bits == 16, "Arrays can be indexed with signed 16-bit integer expressions only. Received type: %s.", indexType);
			Preconditions.checkArgument((index instanceof IntLiteral == false) || arrayToInitialize.dimensions.get(i).isValidIndex(ExprUtils.toLong(index)),
			"The provided index is out of bounds of the array to which this indexing belongs.");
		}
		EmfHelper.checkNotContained(initialValue, 
			"The provided value is already assigned to another model element. Copy it with EcoreUtils.copy().");
		Guava21Workaround.checkArgument(haveSameType(arrayToInitialize, initialValue), 
			"The value %s is not compatible with %s.", initialValue, arrayToInitialize);
		
		val ret = CfainstanceFactory.eINSTANCE.createArrayElementInitialValue();
		ret.indices.addAll(indices);
		ret.initialValue = initialValue;
		arrayToInitialize.initialValues.add(ret);
		return ret;
	}
	
	// References
	// ----------
	
	def VariableRef createVariableRef(Variable variable) {
		Preconditions.checkNotNull(variable, "variable");
		
		val VariableRef ret = CfainstanceFactory.eINSTANCE.createVariableRef();
		ret.variable = variable;
		return ret;
	}
	
	def ArrayElementRef createArrayElementRef(ArrayVariable variable, List<Expression> indices) {
		Preconditions.checkNotNull(variable, "variable");
		Preconditions.checkNotNull(indices, "indices");
		Guava21Workaround.checkArgument(indices.size == variable.dimensions.size, 
			"%d indices were specified, but the array variable has %d dimensions.", indices.size, variable.dimensions.size);
		for (var i = 0; i < indices.size; i++) {
			val index = indices.get(i);
			Preconditions.checkNotNull(index, "element of indices");
			EmfHelper.checkNotContained(index, "One of the provided indices is already assigned to another model element. Copy it with EcoreUtils.copy().");
			Guava21Workaround.checkArgument(index.type instanceof IntType, "Arrays can be indexed with integer expressions only. Received type: %s.", index.type);
			Preconditions.checkArgument((index instanceof IntLiteral == false) || variable.dimensions.get(i).isValidIndex(ExprUtils.toLong(index)),
			"The provided index is out of bounds of the array to which this indexing belongs.");
		}
		
		val ret = CfainstanceFactory.eINSTANCE.createArrayElementRef();
		ret.variable = variable;
		ret.indices.addAll(indices);
		return ret;
	}
	
	// Annotations
	// -----------
		
	def ForLoopLocationAnnotation createForLoopLocationAnnotation(Location parentLocation, Transition loopBodyEntry, Transition loopNextCycle, Transition loopExit, AbstractVariableRef loopVariable, Transition loopVariableInit, Transition loopVariableIncrement) {
		Preconditions.checkNotNull(loopVariable, "loopVariable");
		Preconditions.checkNotNull(loopVariableInit, "loopVariableInit");
		Preconditions.checkNotNull(loopVariableIncrement, "loopVariableIncrement");
		EmfHelper.checkNotContained(loopVariable, "The provided reference is already assigned to another model element. Copy it with EcoreUtils.copy().")
		
		val ret = CfainstanceFactory.eINSTANCE.createForLoopLocationAnnotation();
		setupLoopLocationAnnotation(parentLocation, ret, loopBodyEntry, loopNextCycle, loopExit);
		ret.loopVariable = loopVariable;
		ret.loopVariableInit = loopVariableInit;
		ret.loopVariableIncrement = loopVariableIncrement;
		return ret;
	}
	
	def DirectionVariableAnnotation createDirectionVariableAnnotation(AbstractVariable parentVariable, DataDirection direction) {
		Preconditions.checkNotNull(parentVariable, "parentVariable");
		Preconditions.checkNotNull(direction, "direction");
		
		val ret = CfainstanceFactory.eINSTANCE.createDirectionVariableAnnotation();
		ret.parentVariable = parentVariable;
		ret.direction = direction;
		return ret;
	}
	
	def OriginalDataTypeVariableAnnotation createOriginalDataTypeVariableAnnotation(AbstractVariable parentVariable, String plcDataType) {
		Preconditions.checkNotNull(parentVariable, "parentVariable");
		Preconditions.checkNotNull(plcDataType, "plcDataType");
		
		val ret = CfainstanceFactory.eINSTANCE.createOriginalDataTypeVariableAnnotation();
		ret.parentVariable = parentVariable;
		ret.plcDataType = plcDataType;
		return ret;
	}
	
	def ReturnValueVariableAnnotation createReturnValueVariableAnnotation(AbstractVariable parentVariable) {
		Preconditions.checkNotNull(parentVariable, "parentVariable");
		
		val ret = CfainstanceFactory.eINSTANCE.createReturnValueVariableAnnotation();
		ret.parentVariable = parentVariable;
		return ret;
	}
	
	def InternalGeneratedVariableAnnotation createInternalGeneratedVariableAnnotation(AbstractVariable parentVariable) {
		Preconditions.checkNotNull(parentVariable, "parentVariable");
		
		val ret = CfainstanceFactory.eINSTANCE.createInternalGeneratedVariableAnnotation();
		ret.parentVariable = parentVariable;
		return ret;
	}
}
