/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.cfa.trace.instance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cern.plcverif.base.models.cfa.CfaInstanceSafeFactory;
import cern.plcverif.base.models.expr.Expression;

public final class TracedArrayElementVariable extends TracedSingleVariable {
	private int[] indices;
	private List<Expression> indexExpressions;
	private TracedArrayVariable elementOf;

	public TracedArrayElementVariable(TracedArrayVariable elementOf, int[] indices) {
		super(elementOf.getFlattenedFrom());
		this.elementOf = elementOf;
		this.indices = Arrays.copyOf(indices, indices.length);
		indexExpressions = new ArrayList<>(indices.length);
		for (int i = 0; i < indices.length; ++i) {
			indexExpressions.add(CfaInstanceSafeFactory.INSTANCE.createIndexLiteral(indices[i]));
		}
	}

	public int[] getIndices() {
		return indices;
	}

	public List<Expression> getIndexExpressions() {
		return indexExpressions;
	}

	public TracedArrayVariable getElementOf() {
		return elementOf;
	}

	@Override
	public List<String> getFqn() {
		return getFlattenedFrom().getFqn(indexExpressions);
	}
}
