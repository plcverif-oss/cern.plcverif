/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.models.cfa.validation;

import org.eclipse.emf.ecore.EObject;

import cern.plcverif.base.common.logging.IPlcverifLogger;

public interface IEmfValidation<T extends EObject> {
	void check(EObject e);

	IPlcverifLogger getLogger();
	
	default void error(String message) {
		getLogger().logError(message);
	}

	default void error(String format, Object... args) {
		error(String.format(format, args));
	}

	default void error(boolean expectedCondition, String message) {
		if (!expectedCondition) {
			error(message);
		}
	}
}
