/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.cfa.trace.declared;

import java.util.List;

import cern.plcverif.base.models.cfa.cfabase.ArrayDimension;
import cern.plcverif.base.models.cfa.trace.instance.TracedVariable;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.Type;

public abstract class TracedData {
	private TracedType tracedType;
	private TracedVariable flattenedInto;

	public TracedType getTracedType() {
		return tracedType;
	}

	void setTracedType(TracedType tracedType) {
		this.tracedType = tracedType;
	}

	public abstract TracedType getParentTracedType();

	public TracedVariable getFlattenedInto() {
		return flattenedInto;
	}

	public void setFlattenedInto(TracedVariable flattenedInto) {
		this.flattenedInto = flattenedInto;
	}

	public abstract List<String> getFqn();
	public abstract List<String> getFqn(List<Expression> indices);
	public abstract List<ArrayDimension> getDimensions();
	public abstract boolean isArray();
	public abstract Type getDataType();
}
