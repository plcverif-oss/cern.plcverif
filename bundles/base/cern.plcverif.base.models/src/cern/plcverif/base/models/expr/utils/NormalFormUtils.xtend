/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.expr.utils

import cern.plcverif.base.models.expr.AtomicExpression
import cern.plcverif.base.models.expr.BinaryExpression
import cern.plcverif.base.models.expr.BinaryLogicExpression
import cern.plcverif.base.models.expr.ComparisonExpression
import cern.plcverif.base.models.expr.ComparisonOperator
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.ExpressionSafeFactory
import cern.plcverif.base.models.expr.NaryExpression
import cern.plcverif.base.models.expr.UnaryExpression
import cern.plcverif.base.models.expr.UnaryLogicExpression
import cern.plcverif.base.models.expr.UnaryLogicOperator
import java.util.List

import static cern.plcverif.base.models.expr.BinaryLogicOperator.*
import static cern.plcverif.base.models.expr.UnaryLogicOperator.NEG
import static cern.plcverif.base.models.expr.utils.ExprUtils.*

/**
 * Utility methods to transform expressions into normal forms.
 */
class NormalFormUtils {
	static final ExpressionSafeFactory FACTORY = ExpressionSafeFactory.INSTANCE_NON_STRICT;

	/**
	 * Creates a copy of the given expression and transforms it into conjunctive normal form, if possible.
	 * 
	 * The returned expression is expected to be in the following form:
	 * <pre>
	 * (x1 OR x2 OR ... OR xn) AND (y1 OR ... ym) AND ...
	 * </pre>
	 * where {@code x1}, ..., {@code ym} are atomic expressions or their negation.
	 * 
	 * The method currently does not handle XOR and IFF operators.
	 */
	def static Expression toConjunctiveNf(Expression expr) {
		var result = ExprUtils.exprCopy(expr);

		// Eliminate --> and XOR
		eliminateDerivedBoolOps(result);
		
		// Push negation down
		result = pushNegationDown(result);

		distributeOrOverAnd(result);

		result = simplify(result);
		// currently simplify is executed again as the transformation of a=FALSE may introduce double negation 
		result = simplify(result);
		return result;
	}

	/**
	 * Creates a copy of the given expression, transforms it into conjunctive normal form,
	 * if possible, then returns its clauses. I.e., this method splits the result of 
	 * {@link #toConjunctiveNf(Expression)} at the top-level AND operators.
	 */
	def static List<Expression> getCnfClauses(Expression expr) {
		val cnfForm = toConjunctiveNf(expr);
		val List<Expression> ret = newArrayList();
		collectTopLevelClauses(cnfForm, ret);
		return ret;
	}

	private static def void collectTopLevelClauses(Expression expr, List<Expression> clausesAccu) {
		if (expr instanceof BinaryLogicExpression) {
			if (expr.operator == AND) {
				collectTopLevelClauses(expr.leftOperand, clausesAccu);
				collectTopLevelClauses(expr.rightOperand, clausesAccu);
				return;
			}
		}

		clausesAccu.add(expr);
	}

	/**
	 * Eliminates the derived logic operations from the given expression and its whole subtree. 
	 * The given expression subtree will me modified.
	 * <p>
	 * The following logic operations will be eliminated: implication, <=>, xor.
	 */
	def dispatch static void eliminateDerivedBoolOps(Expression expr) {
		println('''eliminateImplication on unexpected expression type: «expr»''');
	}

	def dispatch static void eliminateDerivedBoolOps(AtomicExpression expr) {
		// Nothing to do here
	}

	def dispatch static void eliminateDerivedBoolOps(UnaryExpression expr) {
		eliminateDerivedBoolOps(expr.operand);
	}

	def dispatch static void eliminateDerivedBoolOps(BinaryExpression expr) {
		eliminateDerivedBoolOps(expr.leftOperand);
		eliminateDerivedBoolOps(expr.rightOperand);
	}

	def dispatch static void eliminateDerivedBoolOps(BinaryLogicExpression expr) {
		if (expr.operator == IMPLIES) {
			// L --> R => (not L) OR R
			val left = expr.leftOperand;
			expr.leftOperand = null;
			expr.leftOperand = FACTORY.createUnaryLogicExpression(left, UnaryLogicOperator.NEG);
			expr.operator = OR;
		} else if (expr.operator == XOR) {
			// L xor R ==> (L and not R) or (not L and R)
			val left = expr.leftOperand;
			val right = expr.rightOperand;
			
			expr.operator = OR;
			expr.leftOperand = FACTORY.and(left, FACTORY.neg(right));
			expr.rightOperand = FACTORY.and(FACTORY.neg(left), right);
		}

		eliminateDerivedBoolOps(expr.leftOperand);
		eliminateDerivedBoolOps(expr.rightOperand);
	}

	def dispatch static void eliminateDerivedBoolOps(NaryExpression expr) {
		for (operand : expr.operands) {
			eliminateDerivedBoolOps(operand);
		}
	}

	/**
	 * Pushes the negations down to the deepest levels in the given expression and 
	 * its subtree using the De Morgan rule:
	 * <pre>
	 * NOT (a AND b)  ==>  ((NOT a) OR (NOT b))
	 * </pre>
	 * 
	 * The given expression subtree will me modified. As the modification may cause
	 * a change in the root node of the expression subtree, the method returns the 
	 * root node of the given expression subtree after the transformation.
	 */
	def dispatch static Expression pushNegationDown(Expression expr) {
		println('''pushNegationDown on unexpected expression type: «expr»''');
		return expr;
	}

	def dispatch static Expression pushNegationDown(AtomicExpression expr) {
		// Nothing to do here
		return expr;
	}

	def dispatch static Expression pushNegationDown(UnaryExpression expr) {
		if (!isBooleanLeaf(expr)) {
			expr.operand = pushNegationDown(expr.operand);
		}
		return expr;
	}

	def dispatch static Expression pushNegationDown(UnaryLogicExpression expr) {
		if (expr.operator == NEG) {
			if (expr.operand instanceof BinaryLogicExpression) {
				val child = expr.operand as BinaryLogicExpression;
				if (child.operator == AND || child.operator == OR) {
					// De Morgan: NOT (a AND b)  ==>  ((NOT a) OR (NOT b))
					val a = child.leftOperand;
					val b = child.rightOperand;

					child.leftOperand = null;
					child.rightOperand = null;

					child.leftOperand = pushNegationDown(FACTORY.createUnaryLogicExpression(a, NEG));
					child.rightOperand = pushNegationDown(FACTORY.createUnaryLogicExpression(b, NEG));

					child.operator = if(child.operator == OR) AND else OR;

					expr.operand = null;
					return child;
				}
			}
		}

		expr.operand = pushNegationDown(expr.operand);
		return expr;
	}

	def dispatch static Expression pushNegationDown(BinaryExpression expr) {
		if (!isBooleanLeaf(expr)) {
			expr.leftOperand = pushNegationDown(expr.leftOperand);
			expr.rightOperand = pushNegationDown(expr.rightOperand);
		}
		return expr;
	}

	def dispatch static Expression pushNegationDown(NaryExpression expr) {
		// We don't do anything for the moment.
		return expr;
	}

	/**
	 * Applies exhaustively the "distributivity of OR over AND" to the given expression
	 * and its subtree, in order to push OR operators closer to the leaves.
	 *  
	 * The given expression subtree will me modified.
	 */
	def dispatch static void distributeOrOverAnd(Expression expr) {
		println('''distributeOrOverAnd on unexpected expression type: «expr»''');
	}

	def dispatch static void distributeOrOverAnd(AtomicExpression expr) {
		// Nothing to do here.
	}

	def dispatch static void distributeOrOverAnd(UnaryExpression expr) {
		distributeOrOverAnd(expr.operand);
	}

	def dispatch static void distributeOrOverAnd(BinaryExpression expr) {
		distributeOrOverAnd(expr.leftOperand);
		distributeOrOverAnd(expr.rightOperand);
	}

	def dispatch static void distributeOrOverAnd(BinaryLogicExpression expr) {
		distributeOrOverAnd(expr.leftOperand);
		distributeOrOverAnd(expr.rightOperand);
		
		if (expr.operator == OR && isAndOperation(expr.rightOperand)) {
			// A OR (B AND C) --> (A OR B) AND (A OR C)
			val a = expr.leftOperand;
			expr.leftOperand = null;

			val b = (expr.rightOperand as BinaryLogicExpression).leftOperand;
			(expr.rightOperand as BinaryLogicExpression).leftOperand = null;
			val c = (expr.rightOperand as BinaryLogicExpression).rightOperand;
			(expr.rightOperand as BinaryLogicExpression).rightOperand = null;

			distributeOrOverAnd(a);
			distributeOrOverAnd(b);
			distributeOrOverAnd(c);

			expr.operator = AND;
			expr.leftOperand = FACTORY.createBinaryLogicExpression(exprCopy(a), b, OR);
			expr.rightOperand = FACTORY.createBinaryLogicExpression(a, c, OR);
			
			distributeOrOverAnd(expr.leftOperand);
			distributeOrOverAnd(expr.rightOperand);
		} else if (expr.operator == OR && isAndOperation(expr.leftOperand)) {
			// (B AND C) OR A --> (A OR B) AND (A OR C)
			val a = expr.rightOperand;
			expr.rightOperand = null;

			val b = (expr.leftOperand as BinaryLogicExpression).leftOperand;
			(expr.leftOperand as BinaryLogicExpression).leftOperand = null;
			val c = (expr.leftOperand as BinaryLogicExpression).rightOperand;
			(expr.leftOperand as BinaryLogicExpression).rightOperand = null;

			distributeOrOverAnd(a);
			distributeOrOverAnd(b);
			distributeOrOverAnd(c);

			expr.operator = AND;
			expr.leftOperand = FACTORY.createBinaryLogicExpression(exprCopy(a), b, OR);
			expr.rightOperand = FACTORY.createBinaryLogicExpression(a, c, OR);
			
			distributeOrOverAnd(expr.leftOperand);
			distributeOrOverAnd(expr.rightOperand);
		}
	}

	def dispatch static void distributeOrOverAnd(NaryExpression expr) {
		for (operand : expr.operands) {
			distributeOrOverAnd(operand);
		}
	}

	/**
	 * Simplifies the given expression and its subtree using the following steps:
	 * <ul>
	 * 		<li>Remove double negations ({@code NOT NOT a ==> a}),
	 * 		<li>Remove unnecessary "equals true" checks ({@code a = true ==> a}),
	 * 		<li>Replaces "equals false" checks with negations ({@code a = false ==> NOT a}). 
	 * </ul>
	 * 
	 * The given expression subtree will me modified. As the modification may cause
	 * a change in the root node of the expression subtree, the method returns the 
	 * root node of the given expression subtree after the transformation.
	 * 
	 * Currently the method may introduce double negations when "equals false" is
	 * replaces with negation which will not be eliminated. 
	 */
	def dispatch static Expression simplify(Expression expr) {
		println('''simplify on unexpected expression type: «expr»''');
		return expr;
	}

	def dispatch static Expression simplify(AtomicExpression expr) {
		return expr;
	}

	def dispatch static Expression simplify(UnaryExpression expr) {
		expr.operand = simplify(expr.operand);
		return expr;
	}

	def dispatch static Expression simplify(UnaryLogicExpression expr) {
		if (expr.operator == NEG && expr.operand instanceof UnaryLogicExpression &&
			(expr.operand as UnaryLogicExpression).operator == NEG) {
			// NOT NOT a --> a
			val result = (expr.operand as UnaryLogicExpression).operand;
			(expr.operand as UnaryLogicExpression).operand = null;
			return simplify(result);
		} else {
			expr.operand = simplify(expr.operand);
			return expr;
		}
	}

	def dispatch static Expression simplify(BinaryExpression expr) {
		expr.leftOperand = simplify(expr.leftOperand);
		expr.rightOperand = simplify(expr.rightOperand);
		return expr;
	}

	def dispatch static Expression simplify(ComparisonExpression expr) {
		if (expr.operator == ComparisonOperator.EQUALS) {
			if (isTrueLiteral(expr.leftOperand)) {
				val ret = expr.rightOperand;
				expr.rightOperand = null;
				return ret;
			} else if (isTrueLiteral(expr.rightOperand)) {
				val ret = expr.leftOperand;
				expr.leftOperand = null;
				return ret;
			} else if (isFalseLiteral(expr.leftOperand)) {
				val ret = expr.rightOperand;
				expr.rightOperand = null;
				return FACTORY.createUnaryLogicExpression(ret, NEG);
			} else if (isFalseLiteral(expr.rightOperand)) {
				val ret = expr.leftOperand;
				expr.leftOperand = null;
				return FACTORY.createUnaryLogicExpression(ret, NEG);
			}
		}

		expr.leftOperand = simplify(expr.leftOperand);
		expr.rightOperand = simplify(expr.rightOperand);
		return expr;
	}

	def dispatch static Expression simplify(NaryExpression expr) {
		// We don't do anything for the moment.
		return expr;
	}

	/**
	 * Returns true if the given expression is a Boolean leaf, i.e., an expression
	 * node or subtree that needs to be handled as atomic in a Boolean expression.
	 * It may be an atomic expression (e.g., named reference or constant literal),
	 * a negated atomic expression, but also a comparison of atomic expressions
	 * (e.g., {@code a > 5}).
	 */
	private def static isBooleanLeaf(Expression expr) {
		if (expr instanceof AtomicExpression) {
			return true;
		} else if (expr instanceof ComparisonExpression) {
			// e.g. "a > 5"
			return expr.leftOperand instanceof AtomicExpression && expr.rightOperand instanceof AtomicExpression;
		} else if (expr instanceof UnaryLogicExpression && (expr as UnaryLogicExpression).operator == NEG) {
			return (expr as UnaryLogicExpression).operand instanceof AtomicExpression;
		}

		return false;
	}
}
