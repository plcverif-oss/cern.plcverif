/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Vince Molnar - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.models.cfa.validation;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;

import com.google.common.collect.Lists;

import cern.plcverif.base.common.logging.IPlcverifLogger;
import cern.plcverif.base.common.logging.PlcverifSeverity;
import cern.plcverif.base.models.cfa.cfabase.CfaNetworkBase;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;

/**
 * CFA validation utility class. Checks the given CFA network against the
 * constraints included in the respective metamodels. In addition, the validation
 * rules defined in {@link CfaInstanceValidation} and
 * {@link CfaDeclarationValidation} will be checked too (depending on the type
 * of the given CFA).
 */
public final class CfaValidation {
	private CfaValidation() {
		// Utility class.
	}
	
	/**
	 * Logger implementation that raises {@link CfaValidationException}
	 * exception if there is a validation problem with at least
	 * {@link PlcverifSeverity#Error} severity.
	 */
	public static class CfaValidationExceptionLogger implements IPlcverifLogger {
		@Override
		public void log(PlcverifSeverity severity, String messageFormat, Object... args) {
			if (severity.isAtLeast(PlcverifSeverity.Error)) {
				throw new CfaValidationException(String.format(messageFormat, args), null);
			}
		}

		@Override
		public void log(PlcverifSeverity severity, String message, Throwable throwable) {
			if (severity.isAtLeast(PlcverifSeverity.Error)) {
				throw new CfaValidationException(message, throwable);
			}
		}

		public static class CfaValidationException extends RuntimeException {
			private static final long serialVersionUID = 1895170129516015944L;

			public CfaValidationException(String message, Throwable cause) {
				super(message, cause);
			}
		}
	}

	/**
	 * Checks whether the given CFA network declaration satisfies all
	 * constraints that are given in the EMF metamodel or in the
	 * {@link CfaDeclarationValidation} specific validator. Throws an exception
	 * if the model violates any requirement. The type of the thrown exception
	 * is defined in {@link CfaValidationExceptionLogger}.
	 * 
	 * @param cfaNetwork
	 *            CFA network to be checked
	 * @throws RuntimeException
	 *             if a constraint has not been satisfied by the given model.
	 */
	public static void validate(CfaNetworkBase cfaNetwork) {
		validate(cfaNetwork, new CfaValidationExceptionLogger());
	}
	
	/**
	 * Validates the given CFA network. If a problem is found, it will be logged
	 * using the given logger. If an exception is needed, use an instance of the
	 * {@link CfaValidationExceptionLogger} that raises an exception upon
	 * problems.
	 * 
	 * @param cfaNetwork
	 *            CFA network to be checked
	 * @param logger
	 *            Logger to report the eventual problems
	 */
	public static void validate(CfaNetworkBase cfaNetwork, IPlcverifLogger logger) {
		if (cfaNetwork instanceof CfaNetworkDeclaration) {
			validate((CfaNetworkDeclaration) cfaNetwork, logger);
		} else if (cfaNetwork instanceof CfaNetworkInstance) {
			validate((CfaNetworkInstance) cfaNetwork, logger);
		}
	}


	/**
	 * Checks whether the given CFA network declaration satisfies all
	 * constraints that are given in the EMF metamodel or in the
	 * {@link CfaDeclarationValidation} specific validator. Logs the found
	 * problems using the given logger.
	 *
	 * @param cfaNetwork
	 *            CFA declaration network to be checked
	 * @param logger
	 *            Logger to be used for reporting found problems
	 */
	public static void validate(CfaNetworkDeclaration cfaNetwork, IPlcverifLogger logger) {
		boolean emfValidationResult = validateObject(cfaNetwork, logger);
		logIfFalse(emfValidationResult, "Constraints of the EMF model were violated.", logger);

		boolean containmentValidationresult = validateContainment(cfaNetwork, logger);
		logIfFalse(containmentValidationresult,
				"One or more referred elements of the EMF model are not contained in any other element.", logger);

		IEmfValidation<CfaNetworkDeclaration> specificValidator = new CfaDeclarationValidation(cfaNetwork, logger);
		specificValidation(cfaNetwork, specificValidator, logger);
	}

	/**
	 * Checks whether the given CFA network instance model satisfies all
	 * constraints that are given in the EMF metamodel or in the
	 * {@link CfaDeclarationValidation} specific validator. Logs the found
	 * problems using the given logger.
	 * 
	 * @param cfaNetwork
	 *            CFA network to be checked
	 * @param logger
	 *            Logger to be used for reporting found problems
	 */
	public static void validate(CfaNetworkInstance cfaNetwork, IPlcverifLogger logger) {
		boolean emfValidationResult = validateObject(cfaNetwork, logger);
		logIfFalse(emfValidationResult, "Constraints of the EMF model were violated.", logger);

		boolean containmentValidationresult = validateContainment(cfaNetwork, logger);
		logIfFalse(containmentValidationresult,
				"One or more referred elements of the EMF model are not contained in any other element.", logger);

		IEmfValidation<CfaNetworkInstance> specificValidator = new CfaInstanceValidation(cfaNetwork, logger);
		specificValidation(cfaNetwork, specificValidator, logger);
	}

	/**
	 * Calls the EMF validator on the given object. Logs the found problems
	 * using the given logger.
	 * 
	 * @return True, if all EMF constraints were satisfied.
	 */
	private static boolean validateObject(EObject eObject, IPlcverifLogger logger) {
		Diagnostic diagnostic = Diagnostician.INSTANCE.validate(eObject);
		if (diagnostic.getSeverity() == Diagnostic.ERROR || diagnostic.getSeverity() == Diagnostic.WARNING) {
			StringBuilder msgBuilder = new StringBuilder(diagnostic.getMessage());
			for (Diagnostic childDiagnostic : diagnostic.getChildren()) {
				switch (childDiagnostic.getSeverity()) {
				case Diagnostic.ERROR:
				case Diagnostic.WARNING:
				case Diagnostic.INFO:
					msgBuilder.append(System.lineSeparator());
					msgBuilder.append("\t");
					msgBuilder.append(childDiagnostic.getMessage());
					break;
				default: /* Nothing to do else. */ break;
				}
			}
			logger.logWarning(msgBuilder.toString());
			return false;
		}
		return true;
	}

	private static boolean validateContainment(EObject rootElement, IPlcverifLogger logger) {
		boolean valid = true;
		Set<EObject> containedElements = new HashSet<>();
		containedElements.addAll(Lists.newArrayList(IteratorExtensions.toIterable(rootElement.eAllContents())));
		for (EObject content : containedElements) {
			for (EObject referred : content.eCrossReferences()) {
				if (referred != null && !containedElements.contains(referred)) {
					logger.logWarning(
							"Element \"%s\" (of type %s) referred by \"%s\" (of type %s) is not part of the containment tree.",
							referred, referred.eClass().getName(), content, content.eClass().getName());
					valid = false;
				}
			}
		}
		return valid;
	}

	private static <T extends EObject> void specificValidation(T rootElement, IEmfValidation<T> specificValidator,
			IPlcverifLogger logger) {
		specificValidator.check(rootElement);
		for (EObject it : IteratorExtensions.toIterable(rootElement.eAllContents())) {
			specificValidator.check(it);
		}
	}

	private static void logIfFalse(boolean condition, String message, IPlcverifLogger logger) {
		if (!condition) {
			logger.logError(message);
		}
	}

}
