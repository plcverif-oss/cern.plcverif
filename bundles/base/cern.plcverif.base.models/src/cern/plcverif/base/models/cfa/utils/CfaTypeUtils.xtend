/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.models.cfa.utils

import cern.plcverif.base.models.expr.Typed
import cern.plcverif.base.models.expr.utils.AbstractTypeUtils
import com.google.common.base.Preconditions
import cern.plcverif.base.models.cfa.textual.CfaToString

final class CfaTypeUtils extends AbstractTypeUtils {
	private new() {
		// Singleton.
	}
	
	public static final CfaTypeUtils INSTANCE = new CfaTypeUtils();	
	
	def static boolean haveSameType(Typed left, Typed right) {
		Preconditions.checkNotNull(left, "left");
		Preconditions.checkNotNull(right, "right");
		return INSTANCE.dataTypeEquals(left.type, right.type); 
	}
	
	/**
	 * Wrapper for Preconditions.checkArgument. 
	 * Throws an exception if {@code expr1} and {@code expr2} does not have the same type, or if any of them is null.
	 */
	def static void checkArgumentsHaveSameType(Typed expr1, Typed expr2) {
		checkArgumentsHaveSameType(expr1, expr2, "");
	}
	
	/**
	 * Wrapper for Preconditions.checkArgument. 
	 * Throws an exception if {@code expr1} and {@code expr2} does not have the same type, or if any of them is null.
	 */
	def static void checkArgumentsHaveSameType(Typed expr1, Typed expr2, String prefixMessage, Object... args) {
		Preconditions.checkNotNull(expr1);
		Preconditions.checkNotNull(expr2);
		
		val type1 = expr1.type;
		val type2 = expr2.type;
		
		if (!INSTANCE.dataTypeEquals(type1, type2)) {
			val leftText = CfaToString.toDiagString(expr1);
			val rightText = CfaToString.toDiagString(expr2);
			val leftType = CfaToString.toDiagString(type1);
			val rightType = CfaToString.toDiagString(type2);
			Preconditions.checkArgument(false, '''«if (prefixMessage == "") "" else String.format(prefixMessage, args) + " "»The types of the two operands are different. Use type conversion if necessary. Expression 1: «leftText» (type: «leftType»); expression 2: «rightText» (type: «rightType»).''');
		}
	}
}
