/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Vince Molnar - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.models.cfa.validation

import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.Field
import cern.plcverif.base.models.cfa.textual.CfaToString
import cern.plcverif.base.models.expr.ElementaryType
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.validation.ExprValidation
import org.eclipse.emf.ecore.EObject
import cern.plcverif.base.common.logging.IPlcverifLogger
import cern.plcverif.base.models.cfa.cfadeclaration.Indexing
import cern.plcverif.base.models.expr.IntType
import cern.plcverif.base.models.expr.BeginningOfCycle
import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.base.models.expr.EndOfCycle
import cern.plcverif.base.models.cfa.cfabase.Annotation

final class CfaDeclarationValidation implements IEmfValidation<CfaNetworkDeclaration>{
	CfaNetworkDeclaration rootElement;
	IPlcverifLogger logger;
	ExprValidation exprValidator;
	
	new(CfaNetworkDeclaration rootElement, IPlcverifLogger logger) {
		this.rootElement = rootElement;
		this.logger = logger;
		this.exprValidator = new ExprValidation(logger);
	}
	
	override getLogger() {
		return this.logger;
	}
		
	def dispatch check(EObject e) {}	
	
	def dispatch check(Field e) {
		if (e.type instanceof ElementaryType) {
			error(e.initialAssignments.size > 0, "No initial assignment set for elementary field " + CfaToString.toDiagString(e));
		}
	}	
	
	def dispatch check(Expression e) {
		exprValidator.check(e);
	}	
	
	def dispatch check(Indexing e) {
		val indexType = e.index.type;
		if (indexType instanceof IntType) {
			if (indexType.bits != 16 || !indexType.signed) {
				error("The array index must have a signed 16-bit integer type.");
			}
		} else {
			error("The array index must have an integer type.");
		}
	}	
	
	def dispatch check(BeginningOfCycle e) {
		if (EmfHelper.isContainedInAny(e, CfaNetworkDeclaration) && !EmfHelper.isContainedInAny(e, Annotation)) {
			error("'BeginningOfCycle' is not permitted in CFA models (except for annotations).");
		}	
	}
	
	
	def dispatch check(EndOfCycle e) {
		if (EmfHelper.isContainedInAny(e, CfaNetworkDeclaration) && !EmfHelper.isContainedInAny(e, Annotation)) {
			error("'EndOfCycle' is not permitted in CFA models (except for annotations).");
		}	
	}
	
	//TODO: add validation rules to check initial values thoroughly (e.g. for arrays; also if the leftValue is the field or its child)
}
