/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.models.cfa.transformation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.base.Preconditions;

import cern.plcverif.base.models.cfa.CfaInstanceSafeFactory;
import cern.plcverif.base.models.cfa.LocalIdGenerator;
import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfabase.Transition;
import cern.plcverif.base.models.cfa.cfainstance.AssignmentTransition;
import cern.plcverif.base.models.cfa.cfainstance.AutomatonInstance;
import cern.plcverif.base.models.cfa.cfainstance.Call;
import cern.plcverif.base.models.cfa.cfainstance.CallTransition;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.cfa.utils.CfaCollectionDeleter;
import cern.plcverif.base.models.cfa.utils.CfaInstanceUtils;
import cern.plcverif.base.models.expr.Expression;

/**
 * This class provides a way to transform the CFA instance network into an
 * equivalent without any call transitions. It will inline all calls and remove
 * all automata beside the main automaton.
 */
public final class CallInliner {
	private CallInliner() {
		// Utility class.
	}

	public static void transform(CfaNetworkInstance network) {
		CfaCollectionDeleter transitionDeleter = new CfaCollectionDeleter();

		inline((AutomatonInstance) network.getMainAutomaton(), new HashSet<>(), transitionDeleter);

		// delete old call transitions
		transitionDeleter.deleteAll();

		// Delete non-main automata
		for (AutomatonInstance automaton : new ArrayList<>(network.getAutomata())) {
			if (automaton != network.getMainAutomaton()) {
				CfaInstanceUtils.deleteAutomatonForced(automaton);
			}
		}
	}

	private static void inline(AutomatonInstance automaton, Set<AutomatonInstance> callStack,
			CfaCollectionDeleter transitionDeleter) {
		if (callStack.contains(automaton)) {
			throw new UnsupportedOperationException(
					"Inlining of (indirectly) recursive calls is currently not supported.");
		}

		callStack.add(automaton);

		// Replace each CallTransition with the called automaton
		for (Transition t : new ArrayList<>(automaton.getTransitions())) {
			if (t instanceof CallTransition) {
				inline((CallTransition) t, callStack, transitionDeleter);
			}
		}
		callStack.remove(automaton);
	}

	private static void inline(CallTransition callTransition, Set<AutomatonInstance> callStack,
			CfaCollectionDeleter transitionDeleter) {
		if (callTransition.getCalls().isEmpty()) {
			return;
		}

		if (callTransition.getCalls().size() > 1) {
			throw new UnsupportedOperationException("Inlining parallel composition is not supported.");
		}

		Preconditions.checkState(callTransition.getParentAutomaton() instanceof AutomatonInstance);

		AutomatonInstance callerAutomaton = (AutomatonInstance) callTransition.getParentAutomaton();
		Call call = callTransition.getCalls().get(0);
		AutomatonInstance calledAutomaton = call.getCalledAutomaton();

		if (calledAutomaton == callTransition.getParentAutomaton()) {
			throw new UnsupportedOperationException(
					"Inlining of (indirectly) recursive calls is currently not supported.");
		}
		inline(calledAutomaton, callStack, transitionDeleter);

		Location start = callTransition.getSource();
		Location end = callTransition.getTarget();

		// Called unit's body (loc1 -- o o o -> loc2)
		// We assume that the called automaton does not contain any variables,
		// so copying the whole automaton is not that big overhead
		AutomatonInstance copy = EcoreUtil.copy(calledAutomaton);
		Location loc1 = copy.getInitialLocation();
		Location loc2 = copy.getEndLocation();

		Expression condition = callTransition.getCondition();
		callTransition.setCondition(null);

		// Ensuring name uniqueness for transitions and locations in the
		// callerAutomaton
		// TODO_LOWPRI naming is ugly

		String prefix = callerAutomaton.getDisplayName() + "_";

		for (Location l : copy.getLocations()) {
			l.setDisplayName(prefix + l.getName());
			l.setName(LocalIdGenerator.INSTANCE.generateId(callerAutomaton, l.getDisplayName()));
		}
		for (Transition t : copy.getTransitions()) {
			t.setDisplayName(prefix + t.getName());
			t.setName(LocalIdGenerator.INSTANCE.generateId(callerAutomaton, t.getDisplayName()));
		}

		// Quickfix: callee's initial location is not special anymore, doesn't
		// need to be frozen
		copy.getInitialLocation().setFrozen(false);

		callerAutomaton.getLocations().addAll(copy.getLocations());
		callerAutomaton.getTransitions().addAll(copy.getTransitions());
		CfaInstanceUtils.deleteAutomatonForced(copy);

		// Input variable passing transition (start --> loc1)
		AssignmentTransition t1 = CfaInstanceSafeFactory.INSTANCE.createAssignmentTransition(prefix + "inputs",
				callerAutomaton, start, loc1, condition);
		t1.getAssignments().addAll(call.getInputAssignments());

		// Output variable passing transition (loc2 --> end)
		AssignmentTransition t2 = CfaInstanceSafeFactory.INSTANCE.createAssignmentTransition(prefix + "outputs",
				callerAutomaton, loc2, end, CfaInstanceSafeFactory.INSTANCE.createBoolLiteral(true));
		t2.getAssignments().addAll(call.getOutputAssignments());

		// Remove old call transition
		transitionDeleter.registerForDeletion(callTransition);
		EcoreUtil.remove(callTransition); // to avoid future copying
		callTransition.setSource(null);
		callTransition.setTarget(null);
	}
}