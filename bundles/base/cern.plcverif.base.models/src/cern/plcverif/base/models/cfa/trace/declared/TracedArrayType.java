/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.cfa.trace.declared;

import com.google.common.base.Preconditions;

public final class TracedArrayType extends TracedType {
	private TracedElementData element;

	public TracedElementData getElement() {
		return element;
	}

	public void setElement(TracedElementData element) {
		Preconditions.checkNotNull(element, "Multiple element definitions assigned as child of an array type.");
		this.element = element;
	}

	@Override
	public void registerChild(TracedData data) {
		Preconditions.checkArgument(data instanceof TracedElementData, "Field assigned as child of an array type.");
		setElement((TracedElementData) data);
	}

	private TracedArrayType(TracedData typeOf) {
		super(typeOf);
	}
	
	public static TracedArrayType createAndRegisterFor(TracedData typeOf) {
		return new TracedArrayType(typeOf);
	}
}
