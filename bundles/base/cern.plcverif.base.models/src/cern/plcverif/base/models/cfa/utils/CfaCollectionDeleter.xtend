/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.cfa.utils

import cern.plcverif.base.common.emf.CollectionDeleter
import cern.plcverif.base.models.expr.Expression
import org.eclipse.emf.ecore.EObject
import java.util.Set

final class CfaCollectionDeleter extends CollectionDeleter {
	Set<Expression> exprsToDelete = newHashSet();
	
	override registerForDeletion(EObject objectToDelete) {
		switch (objectToDelete) {
			// Expressions are handled separately, as the CfaUtils.delete(Expression) method is efficient, no need for cross-reference resolution.
			Expression: exprsToDelete.add(objectToDelete)
			default: super.registerForDeletion(objectToDelete)
		}
	}
	
	override deleteAll() {
		exprsToDelete.forEach[it | CfaUtils.delete(it)];
		super.deleteAll();
	}
	
	override isMarkedForDeletion(EObject e) {
		return super.isMarkedForDeletion(e) || exprsToDelete.contains(e);
	}
	
	override isEmpty() {
		return super.isEmpty() && exprsToDelete.isEmpty;
	}	
}
