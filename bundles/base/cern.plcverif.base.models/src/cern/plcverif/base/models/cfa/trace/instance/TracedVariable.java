/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.cfa.trace.instance;

import java.util.List;

import cern.plcverif.base.models.cfa.cfainstance.AbstractVariable;
import cern.plcverif.base.models.cfa.trace.declared.TracedData;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.Type;

public abstract class TracedVariable {
	private TracedData flattenedFrom;
	private Expression equalsTo;
	private boolean deleted = false;

	public TracedVariable(TracedData flattenedFrom) {
		this.flattenedFrom = flattenedFrom;
	}

	public TracedData getFlattenedFrom() {
		return flattenedFrom;
	}

	public Expression getEqualsTo() {
		return equalsTo;
	}

	public void setEqualsTo(Expression equalsTo) {
		this.equalsTo = equalsTo;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public Type getDataType() {
		return flattenedFrom.getDataType();
	}

	public abstract List<String> getFqn();

	public abstract AbstractVariable getRepresentedVariable();
}