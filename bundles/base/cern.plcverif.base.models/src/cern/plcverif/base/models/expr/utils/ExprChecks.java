/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.models.expr.utils;

import com.google.common.base.Preconditions;

import cern.plcverif.base.models.expr.BoolType;
import cern.plcverif.base.models.expr.Typed;

public final class ExprChecks {
	private ExprChecks() {
		// Utility class.
	}
	
	public static void checkBoolType(Typed typed) {
		checkBoolType(typed, 
				"The provided expression is not of type Boolean. Received type: " + typed.getType().toString());
	}
	
	public static void checkBoolType(Typed typed, String message) {
		Preconditions.checkArgument(typed.getType() instanceof BoolType,
				message);
	}
	
	public static void checkBoolType(Typed typed, String messageFormat, Object... args) {
		if (!(typed.getType() instanceof BoolType)) {
			throw new IllegalArgumentException(String.format(messageFormat, args));
		}
	}
	
	public static void checkSameTypes(Typed typed1, Typed typed2, String messageFormat, Object... args) {
		Preconditions.checkNotNull(typed1);
		Preconditions.checkNotNull(typed2);
		if (!typed1.getType().dataTypeEquals(typed2.getType())) {
			throw new AssertionError(String.format(messageFormat, args));
		}
	}
}
