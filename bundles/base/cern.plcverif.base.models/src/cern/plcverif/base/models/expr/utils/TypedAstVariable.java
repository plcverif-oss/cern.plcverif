/******************************************************************************
 * (C) Copyright CERN 2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.expr.utils;

import cern.plcverif.base.models.expr.Type;

/**
 * Class to describe a variable with its type information.
 */
public final class TypedAstVariable {
	private String variableName;
	private String plcType;
	private Type cfaType;
	private boolean topLevel;
	private boolean leaf;
	
	/**
	 * Creates a new typed AST variable descriptor.
	 * @param variableName Name of the described variable
	 * @param plcType Type of the described variable in the PLC code (AST), for diagnostic purposes only
	 * @param cfaType Type of the described variable in CFA
	 * @param topLevel True if this is a top level variable
	 * @param leaf True if this variable does not have any children variable
	 */
	public TypedAstVariable(String variableName, String plcType, Type cfaType, boolean topLevel, boolean leaf) {
		this.variableName = variableName;
		this.plcType = plcType;
		this.cfaType = cfaType;
		this.topLevel = topLevel;
		this.leaf = leaf;
	}
	
	/**
	 * Returns the name of the described variable.
	 * @return Name of the described variable
	 */
	public String getVariableName() {
		return variableName;
	}
	
	/**
	 * Returns the type of the described variable in the PLC code, for diagnostic purposes only
	 * @return Type of the described variable in the PLC code
	 */
	public String getPlcType() {
		return plcType;
	}
	
	/**
	 * Returns the type of the described variable in CFA.
	 * @return Type of the described variable in CFA
	 */
	public Type getCfaType() {
		return cfaType;
	}
	
	/**
	 * Returns true if this is a top level variable
	 * @return True if this is a top level variable
	 */
	public boolean isTopLevel() {
		return topLevel;
	}
	
	/**
	 * Returns true if this is a leaf variable, i.e., it does not have any children
	 * @return True if this is a leaf variable
	 */
	public boolean isLeaf() {
		return leaf;
	}
}
