/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.models.expr.utils

import cern.plcverif.base.models.expr.AtomicExpression
import cern.plcverif.base.models.expr.BinaryArithmeticExpression
import cern.plcverif.base.models.expr.BinaryArithmeticOperator
import cern.plcverif.base.models.expr.BinaryExpression
import cern.plcverif.base.models.expr.BinaryLogicExpression
import cern.plcverif.base.models.expr.BinaryLogicOperator
import cern.plcverif.base.models.expr.BoolLiteral
import cern.plcverif.base.models.expr.BoolType
import cern.plcverif.base.models.expr.ComparisonExpression
import cern.plcverif.base.models.expr.ComparisonOperator
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.ExpressionSafeFactory
import cern.plcverif.base.models.expr.FloatType
import cern.plcverif.base.models.expr.IntLiteral
import cern.plcverif.base.models.expr.IntType
import cern.plcverif.base.models.expr.Literal
import cern.plcverif.base.models.expr.Type
import cern.plcverif.base.models.expr.UnaryExpression
import cern.plcverif.base.models.expr.UnaryLogicExpression
import cern.plcverif.base.models.expr.UnaryLogicOperator
import com.google.common.base.Preconditions
import org.eclipse.emf.ecore.util.EcoreUtil

final class ExprUtils {
	static final ExpressionSafeFactory nonstrictFactory = ExpressionSafeFactory.INSTANCE_NON_STRICT;
	static final ExprEval evalInstance = new ExprEval();

	private new() {
		// Singleton.
	}

	/**
	 * Computes every computable subexpression within the specified expression and returns a new expression with the
	 * computed subexpressions substituted with a literal. If the whole expression is computable, the result itself 
	 * is a {@link Literal}.
	 * 
	 * Convenience method to call the corresponding method on {@link ExprEval} without providing 
	 * any context (any atomic expression valuation). 
	 * 
	 * @param expr The expression to compute.
	 * @return A new expression with all computable subexpressions substituted with a literal.
	 */
	def static Expression compute(Expression expr) {
		return evalInstance.compute(expr);
	}

	/**
	 * Checks if the expression can be fully computed to reduce it into a single constant (literal) expression.
	 * 
	 * Convenience method to call the corresponding method on {@link ExprEval} without providing 
	 * any context (any atomic expression valuation).
	 * 
	 * @param expr The expression to check.
	 * @return True, if all atomic expressions are literals, false otherwise.
	 */
	def static boolean isComputableConstant(Expression expr) {
		return evalInstance.isComputableConstant(expr);
	}

	/**
	 * Returns the literal representation of the given expression. Only valid for computable 
	 * constants, i.e., if for the given expression the {@link #isComputableConstant(Expression)} 
	 * returned true.
	 * 
	 * Convenience method to call the corresponding method on {@link ExprEval} without providing 
	 * any context (any atomic expression valuation).
	 */
	def static Literal literalRepresentation(Expression expr) {
		return evalInstance.literalRepresentation(expr);
	}
	
	def static long toLong(Expression expr) {
		return evalInstance.toLong(expr);
	}
	
	def static double toDouble(Expression expr) {
		return evalInstance.toDouble(expr);
	}
	
	def static boolean toBoolean(Expression expr) {
		return evalInstance.toBoolean(expr);
	}

	/**
	 * Creates a literal of a given type from a string.
	 * <p>
	 * <b>The literal returned is not guaranteed to have the given {@code type} type!</b>
	 * If the {@code value} does not fit into the given type, it will be automatically widened.
	 * 
	 * @param value String value
	 * @param type Type of the literal. This object will not be altered,
	 * 		it is copied for the new literal. The type may be widened if needed
	 * 		(in case of {@link IntType}, but the object type will not change. 
	 * @return A new literal parsed from the string
	 * 
	 * @throws IllegalArgumentException if the given {@code value} string cannot 
	 * be parsed as the given type. For example, this exception is thrown if the
	 * {@code value = true} is supplied with type {@link IntType}.
	 */
	static def createLiteralFromString(String value, Type type) {
		val factory = ExpressionSafeFactory.INSTANCE

		try {
			switch (type) {
				IntType: {
					val longValue = Long.parseLong(value);
					if (factory.fitsWithin(longValue, type)) {
						return factory.createIntLiteral(longValue, EcoreUtil.copy(type));
					} else {
						return factory.createIntLiteralWithMinimalType(longValue);
					}
				}
				FloatType: {
					val doubleValue = Double.parseDouble(value);
					return factory.createFloatLiteral(doubleValue, EcoreUtil.copy(type));
				}
				BoolType: {
					var boolean boolValue;
					if (value.toLowerCase.equalsIgnoreCase("false") || value.toLowerCase.equalsIgnoreCase("true")) {
						boolValue = Boolean.parseBoolean(value);
					} else if (value.equals("0")) {
						boolValue = false;
					} else if (value.equals("1")) {
						boolValue = true;
					} else {
						throw new IllegalArgumentException('''Unable to parse the value '«value»' as Boolean value. Only 'true', 'false', '0' and '1' are valid Boolean values.''');
					}

					return factory.createBoolLiteral(boolValue, EcoreUtil.copy(type));
				}
				default:
					throw new UnsupportedOperationException(
						'''Parsing in ExprUtils.createLiteralFromString for type «type.toString» is not implemented yet.''')
			}
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException('''Unable to parse the value «value» as «type.class.simpleName».''', e);
		}
	}

	def static isBoolLiteral(Expression expr) {
		return (expr instanceof BoolLiteral);
	}

	def static isTrueLiteral(Expression expr) {
		if (expr instanceof BoolLiteral) {
			return expr.value == true;
		}
		return false;
	}

	def static isFalseLiteral(Expression expr) {
		if (expr instanceof BoolLiteral) {
			return expr.value == false;
		}
		return false;
	}

	def static isIntLiteral(Expression expr) {
		return (expr instanceof IntLiteral);
	}

	def static isEquality(Expression expr) {
		if (expr instanceof ComparisonExpression) {
			return expr.operator == ComparisonOperator.EQUALS;
		}
		return false;
	}
	
	def static isAtomic(Expression expr) {
		return expr instanceof AtomicExpression;
	}

	def static isAndOperation(Expression expr) {
		if (expr instanceof BinaryLogicExpression) {
			return expr.operator == BinaryLogicOperator.AND;
		}
		return false;
	}

	def static isOrOperation(Expression expr) {
		if (expr instanceof BinaryLogicExpression) {
			return expr.operator == BinaryLogicOperator.OR;
		}
		return false;
	}

	def static isUnaryNegation(Expression expr) {
		if (expr instanceof UnaryLogicExpression) {
			return expr.operator == UnaryLogicOperator.NEG;
		}
		return false;
	}

	/**
	 * Alternative of {@link EcoreUtil#copy}, optimized to expressions defined 
	 * in {@link Expression} (with fallback to {@link EcoreUtil#copy} if needed).
	 */
	static def <T extends Expression> T exprCopy(T exprToCopy) {
		return exprCopyInternal(exprToCopy) as T;
	}

	private static def dispatch Expression exprCopyInternal(Expression e) {
		return EcoreUtil.copy(e);
	}

	private static def dispatch BinaryLogicExpression exprCopyInternal(BinaryLogicExpression e) {
		val ret = nonstrictFactory.createBinaryLogicExpression(exprCopyInternal(e.leftOperand),
			exprCopyInternal(e.rightOperand), e.operator);
		return ret;
	}

	private static def dispatch BinaryArithmeticExpression exprCopyInternal(BinaryArithmeticExpression e) {
		val ret = nonstrictFactory.createBinaryArithmeticExpression(exprCopyInternal(e.leftOperand),
			exprCopyInternal(e.rightOperand), e.operator);
		return ret;
	}

	private static def dispatch ComparisonExpression exprCopyInternal(ComparisonExpression e) {
		val ret = nonstrictFactory.createComparisonExpression(exprCopyInternal(e.leftOperand),
			exprCopyInternal(e.rightOperand), e.operator);
		return ret;
	}

	private static def dispatch UnaryLogicExpression exprCopyInternal(UnaryLogicExpression e) {
		val ret = nonstrictFactory.createUnaryLogicExpression(exprCopyInternal(e.operand), e.operator);
		return ret;
	}

	private static def dispatch BoolLiteral exprCopyInternal(BoolLiteral e) {
		val ret = nonstrictFactory.createBoolLiteral(e.value);
		return ret;
	}

	private static def dispatch IntLiteral exprCopyInternal(IntLiteral e) {
		Preconditions.checkState(e.type instanceof IntType, "Only IntLiterals with IntType type can be copied by exprCopyInternal. Other types (such as UnknownType) are not supported.");
		val type = e.type as IntType;
		val ret = nonstrictFactory.createIntLiteral(e.value, type.signed, type.bits);
		return ret;
	}

	/**
	 * Returns the number of nodes in the expression subtree rooted at {@code e}.
	 */
	static def dispatch int exprTreeSize(Expression e) {
		if (e.eContents === null || e.eContents.isEmpty) {
			return 1;
		} else {
			return 1 + e.eContents.filter(Expression).map([it|exprTreeSize(it)]).reduce[a, b|a + b];
		}
	}

	static def dispatch int exprTreeSize(Literal e) {
		return 1;
	}

	static def dispatch int exprTreeSize(BinaryExpression e) {
		return 1 + exprTreeSize(e.leftOperand) + exprTreeSize(e.rightOperand);
	}

	static def dispatch int exprTreeSize(UnaryExpression e) {
		return 1 + exprTreeSize(e.operand);
	}

	static def dispatch int exprTreeSize(Void e) {
		return 0;
	}

	/**
	 * Returns true iff the given operator is a bitwise binary operator
	 * (bitwise Boolean operation, bitshift or bitrotate).
	 */
	static def boolean isBitwiseOp(BinaryArithmeticOperator operator) {
		switch (operator) {
			case BITSHIFT_LEFT,
			case BITSHIFT_RIGHT,
			case BITROTATE,
			case BITWISE_OR,
			case BITWISE_AND,
			case BITWISE_XOR:
				return true
			default:
				return false
		}
	}
	
	/**
	 * Returns the size of the given data type in bits. 
	 */
	static def dispatch int sizeInBits(Type type) {
		throw new UnsupportedOperationException("Unknown data type: " + type);
	}
	
	static def dispatch int sizeInBits(BoolType type) {
		return 1;
	}
	
	static def dispatch int sizeInBits(IntType type) {
		return type.bits;
	}
	
	static def dispatch int sizeInBits(FloatType type) {
		return type.bits.value;
	}
}
