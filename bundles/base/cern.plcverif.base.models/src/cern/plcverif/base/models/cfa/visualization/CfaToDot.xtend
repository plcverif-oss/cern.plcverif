/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.cfa.visualization

import cern.plcverif.base.models.cfa.cfabase.Location
import cern.plcverif.base.models.cfa.cfabase.Transition
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariable
import cern.plcverif.base.models.cfa.cfainstance.AssignmentTransition
import cern.plcverif.base.models.cfa.cfainstance.AutomatonInstance
import cern.plcverif.base.models.cfa.cfainstance.Call
import cern.plcverif.base.models.cfa.cfainstance.CallTransition
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance
import cern.plcverif.base.models.cfa.cfainstance.VariableAssignment
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.cfa.textual.CfaToString
import cern.plcverif.base.models.cfa.cfainstance.Variable
import cern.plcverif.base.models.cfa.cfainstance.ArrayVariable
import cern.plcverif.base.models.cfa.utils.ArrayInitialValueHelper
import cern.plcverif.base.models.cfa.cfabase.CfaNetworkBase
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration
import cern.plcverif.base.models.cfa.cfabase.AutomatonBase
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructure
import cern.plcverif.base.models.cfa.cfabase.NamedElement
import static extension cern.plcverif.base.models.cfa.utils.CfaDeclarationUtils.refersTo
import static extension cern.plcverif.base.models.expr.utils.ExprUtils.isTrueLiteral

final class CfaToDot {
	CfaNetworkBase networkToVisualize;
	NameToDotId nameToDotId = new NameToDotId();
	ICfaToDotFormat formatter;
	
	new(CfaNetworkInstance networkToVisualize) {
		this(networkToVisualize, new BaseCfaToDotFormat());
	}
	
	new(CfaNetworkDeclaration networkToVisualize) {
		this(networkToVisualize, new BaseCfaToDotFormat());
	}
	
	new(CfaNetworkInstance networkToVisualize, ICfaToDotFormat formatter) {
		this.networkToVisualize = networkToVisualize;
		this.formatter = formatter;
	}
	
	new(CfaNetworkDeclaration networkToVisualize, ICfaToDotFormat formatter) {
		this.networkToVisualize = networkToVisualize;
		this.formatter = formatter;
	}

	static def represent(CfaNetworkBase networkToVisualize) {
		switch (networkToVisualize) {
			CfaNetworkInstance: return representCfi(networkToVisualize)
			CfaNetworkDeclaration: return representCfd(networkToVisualize)
			default: throw new UnsupportedOperationException("Unknown CFA type: " + networkToVisualize)
		}
	}

	static def representCfi(CfaNetworkInstance networkToVisualize) {
		return new CfaToDot(networkToVisualize).represent();
	}
	
	static def representCfd(CfaNetworkDeclaration networkToVisualize) {
		return new CfaToDot(networkToVisualize).represent();
	}
	
	static def representCfi(CfaNetworkInstance networkToVisualize, ICfaToDotFormat formatter) {
		return new CfaToDot(networkToVisualize, formatter).represent();
	}
	
	static def representCfd(CfaNetworkDeclaration networkToVisualize, ICfaToDotFormat formatter) {
		return new CfaToDot(networkToVisualize, formatter).represent();
	}

	def CharSequence represent() {
		return representNetwork(networkToVisualize);
	}

	private def dispatch CharSequence representNetwork(CfaNetworkBase cfa) {}

	private def dispatch CharSequence representNetwork(CfaNetworkInstance cfa) '''
		digraph G {
			«IF formatter.showVariables»
				«representVariables(cfa)»
			«ENDIF»
			«FOR automaton : cfa.automata»
				«representAutomaton(automaton)»
			«ENDFOR»
		}
	'''
	
	private def dispatch CharSequence representNetwork(CfaNetworkDeclaration cfa) '''
		digraph G {
			«IF formatter.showDataStructure»
				«representRootDs(cfa)»
			«ENDIF»
			«FOR automaton : cfa.automata»
				«representAutomaton(automaton)»
			«ENDFOR»
		}
	'''

	private def CharSequence representAutomaton(AutomatonInstance automaton) '''
		subgraph «automaton.dotId("cluster")» {
			node [style="filled"];
			«IF networkToVisualize.mainAutomaton === automaton»style=bold;«ENDIF»
			color="black";
			fontsize=10;
			ranksep=0.4;
			
			label="«label(automaton.displayName)»";
			
			«IF formatter.showAnnotations»
				«representAnnotations(automaton)»
			«ENDIF»
			
			«representLocsAndTrans(automaton)»
		}
	'''
	
	private def CharSequence representAutomaton(AutomatonDeclaration automaton) '''
		subgraph «automaton.dotId("cluster")» {
			node [style="filled"];
			«IF networkToVisualize.mainAutomaton === automaton»style=bold;«ENDIF»
			color="black";
			fontsize=10;
			ranksep=0.4;
			
			label="«label('''«automaton.displayName» : «automaton.localDataStructure.definition.name»''')»";
			
			«IF formatter.showAnnotations»
				«representAnnotations(automaton)»
			«ENDIF»
			
			«representLocsAndTrans(automaton)»
		}
	'''
	
	private def CharSequence representLocsAndTrans(AutomatonBase automaton) '''
		«FOR location : automaton.locations»
			«representLocation(location, automaton.initialLocation === location, automaton.endLocation === location)»
		«ENDFOR»
		«FOR transition : automaton.transitions»
			«representTransition(transition)»
		«ENDFOR»
	'''

	private def CharSequence representRootDs(CfaNetworkDeclaration cfd) '''
		«representDs(cfd.rootDataStructure)»
	'''
	
	private def CharSequence representDs(DataStructure ds) '''
		subgraph «ds.dotId("cluster_")» {
			node [shape="folder", style="filled"];
			//label="«ds.name»";
			«ds.dotId» [label="«label(ds.name)»"];
			«FOR field : ds.fields»
				«ds.dotId» -> «field.dotId»;
				«field.dotId» [label = "«label('''(F) «field.name» : «CfaToString.INSTANCE.toString(field.type)»''')»", fillcolor="white" «IF(networkToVisualize as CfaNetworkDeclaration).mainContext.refersTo(field)», peripheries=2«ENDIF»];
			«ENDFOR»
			«FOR complexType : ds.complexTypes»
				«ds.dotId» -> «complexType.dotId»;
				«representDs(complexType)»
			«ENDFOR»
			
			«representDsAnnotations(ds)»
			«representInitAmts(ds)»
		}
	'''

	private def CharSequence representVariables(CfaNetworkInstance container) '''
		«container.dotId("vars_declaration_pseudonode_")» [
			label="«label('''VARIABLES\l«FOR variable : container.variables»«representVariable(variable)»\l«ENDFOR»''')»",
			fillcolor="lightgray", shape="rectangle"];
	'''
		
	private def CharSequence representVariable(AbstractVariable variable) {
		return '''«formatter.variableToText(variable)» = «representInitialValue(variable)»'''
	}
	
	private def dispatch representInitialValue(AbstractVariable variable) {
		throw new UnsupportedOperationException("Unknown variable type: " + variable);
	}
	
	private def dispatch representInitialValue(Variable variable) {
		return '''«representExpression(variable.initialValue)»''';
	}
	
	private def dispatch representInitialValue(ArrayVariable variable) {
		val helper = new ArrayInitialValueHelper(variable);
		return '''«FOR iv : 0..variable.initialValues.size - 1 BEFORE '{' SEPARATOR ', ' AFTER '}'»«representExpression(helper.getInitialValue(iv))»«ENDFOR»''';
	}

	private def CharSequence representAnnotations(AutomatonBase automaton) '''
		«automaton.dotId("annotations_pseudonode_")» [
			label="«label('''ANNOTATIONS\l«FOR a : automaton.annotations»- «AnnotationToDisplayString.toDisplayString(a)»\l«ENDFOR»«FOR loc : automaton.locations»«FOR a : loc.annotations»«loc.name»: «AnnotationToDisplayString.toDisplayString(a)»\l«ENDFOR»«ENDFOR»«FOR trans : automaton.transitions»«FOR a : trans.annotations»«trans.name»: «AnnotationToDisplayString.toDisplayString(a)»\l«ENDFOR»«ENDFOR»''')»",
			fontsize=9, margin="0.04,0.04", fillcolor="white", shape="rectangle", style="dashed"];
	'''
	
	private def CharSequence representDsAnnotations(DataStructure ds) '''
		«ds.dotId("annotations_pseudonode_")» [
			label="«label('''ANNOTATIONS\l«FOR field : ds.fields»«FOR a : field.annotations»«a.parentField.name» : «AnnotationToDisplayString.toDisplayString(a)»\l«ENDFOR»«ENDFOR»«FOR complexType : ds.complexTypes»«FOR a : complexType.annotations»«a.parentDataStructure.name» : «AnnotationToDisplayString.toDisplayString(a)»\l«ENDFOR»«ENDFOR»''')»",
			fontsize=9, margin="0.04,0.04", fillcolor="white", shape="rectangle", style="dashed"];
	'''
	
	private def CharSequence representInitAmts(DataStructure ds) '''
		«ds.dotId("initamt_pseudonode_")» [
			label="«label('''INITIAL\l«FOR field : ds.fields»«FOR inAm : field.initialAssignments»«representAssignment(inAm)»\l«ENDFOR»«ENDFOR»''')»",
			fontsize=9, margin="0.04,0.04", fillcolor="lightyellow", shape="rectangle", style="dashed"];
	'''

	private def CharSequence representLocation(Location location, boolean isInitial, boolean isFinal) '''
		«location.dotId» [label="«label(location.displayName)»", color="«formatter.locationBorderColor(location)»", fillcolor="«formatter.locationFillColor(location)»"«IF isInitial», peripheries=2«ENDIF»«IF isFinal», style=bold«ENDIF», shape="«formatter.locationShape(location)»"];
	'''

	private def dispatch CharSequence representTransition(Transition transition) {
		return "// Unknown transition: " + transition
	}
	
	private def dispatch CharSequence representTransition(AssignmentTransition transition) '''
		«transition.source.dotId» -> «transition.target.dotId» [color="«formatter.transitionBorderColor(transition)»", label="«label('''«guardLabel(transition)»«FOR amt : transition.assignments»«representAssignment(amt)»\l«ENDFOR»«FOR bnd : transition.assumeBounds»«representAssumeBound(bnd)»\l«ENDFOR»''')»"];
	'''

	private def dispatch CharSequence representTransition(CallTransition transition) '''
		«transition.source.dotId» -> «transition.target.dotId» [color="«formatter.transitionBorderColor(transition)»", label="«label('''«guardLabel(transition)»CALL: «FOR call : transition.calls»«representCall(call)»\l«ENDFOR»''')»"];
	'''
	
	private def dispatch CharSequence representTransition(cern.plcverif.base.models.cfa.cfadeclaration.AssignmentTransition transition) '''
		«transition.source.dotId» -> «transition.target.dotId» [color="«formatter.transitionBorderColor(transition)»", label="«label('''«guardLabel(transition)»«FOR amt : transition.assignments»«representAssignment(amt)»\l«ENDFOR»«FOR bnd : transition.assumeBounds»«representAssumeBound(bnd)»\l«ENDFOR»''')»"];
	'''
	
	private def dispatch CharSequence representTransition(cern.plcverif.base.models.cfa.cfadeclaration.CallTransition transition) '''
		«transition.source.dotId» -> «transition.target.dotId» [color="«formatter.transitionBorderColor(transition)»", label="«label('''«guardLabel(transition)»CALL: «FOR call : transition.calls»«representCall(call)»\l«ENDFOR»''')»"];
	'''
	
	private def representAssignment(VariableAssignment assignment) {
		return formatter.assignmentToText(assignment);
	}
	
	private def representAssignment(cern.plcverif.base.models.cfa.cfadeclaration.VariableAssignment assignment) {
		return formatter.assignmentToText(assignment);
	}
	
	private def representAssumeBound(Expression bound) {
		return formatter.assumeBoundToText(bound);
	}
	
	private def representCall(Call call) {
		return '''«call.calledAutomaton.displayName»(\l   IN: «FOR amt : call.inputAssignments SEPARATOR ', '»«representExpression(amt.leftValue)» := «representExpression(amt.rightValue)»\l«ENDFOR»   OUT: «FOR amt : call.outputAssignments SEPARATOR ', '»«representExpression(amt.leftValue)» := «representExpression(amt.rightValue)»\l«ENDFOR»)'''
	}
	
	private def representCall(cern.plcverif.base.models.cfa.cfadeclaration.Call call) {
		return '''«call.calledAutomaton.displayName»:«call.calleeContext»(\l   IN: «FOR amt : call.inputAssignments SEPARATOR ', '»«representExpression(amt.leftValue)» := «representExpression(amt.rightValue)»\l«ENDFOR»   OUT: «FOR amt : call.outputAssignments SEPARATOR ', '»«representExpression(amt.leftValue)» := «representExpression(amt.rightValue)»\l«ENDFOR»)'''
	}
	
	private def CharSequence guardLabel(Transition transition) {
		if (transition.condition === null) {
			return '''[null]\l''';
		} else if (transition.condition.isTrueLiteral) {
			// Do not print constant true guards ([true])
			return '''''';
		} else {
			return '''[«representExpression(transition.condition)»]\l''';
		}
	}
	
	private def representExpression(Expression expr) {
		return formatter.expressionToText(expr);
	}
 
	private def dotId(Location location) {
		return nameToDotId.toId(String.format("%s_%s", location.name, location.parentAutomaton.name));
	}
	
	private def dotId(AutomatonBase container, String prefix) {
		return nameToDotId.toId(prefix + container.name);
	}
	
	private def dotId(CfaNetworkBase container, String prefix) {
		return nameToDotId.toId(prefix + container.name);
	}
	
	private def dotId(NamedElement e) {
		return nameToDotId.toId(e.name + e.hashCode);
	}
	
	private def dotId(NamedElement e, String prefix) {
		return nameToDotId.toId(prefix + e.name + e.hashCode);
	}
	
	private def label(String labelText) {
		return formatter.labelPostProcessing(labelText);	
	}

}
