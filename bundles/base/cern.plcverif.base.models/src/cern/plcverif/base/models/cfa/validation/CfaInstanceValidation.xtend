/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 * 
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Vince Molnar - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.models.cfa.validation

import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.base.common.logging.IPlcverifLogger
import cern.plcverif.base.models.cfa.cfabase.Annotation
import cern.plcverif.base.models.cfa.cfabase.ArrayDimension
import cern.plcverif.base.models.cfa.cfabase.ElseExpression
import cern.plcverif.base.models.cfa.cfabase.Transition
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariableRef
import cern.plcverif.base.models.cfa.cfainstance.ArrayElementRef
import cern.plcverif.base.models.cfa.cfainstance.AssignmentTransition
import cern.plcverif.base.models.cfa.cfainstance.AutomatonInstance
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance
import cern.plcverif.base.models.cfa.textual.CfaToString
import cern.plcverif.base.models.cfa.utils.CfaUtils
import cern.plcverif.base.models.expr.BeginningOfCycle
import cern.plcverif.base.models.expr.EndOfCycle
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.IntLiteral
import cern.plcverif.base.models.expr.IntType
import cern.plcverif.base.models.expr.validation.ExprValidation
import com.google.common.base.Preconditions
import org.eclipse.emf.ecore.EObject

final class CfaInstanceValidation implements IEmfValidation<CfaNetworkInstance> {
	CfaNetworkInstance rootElement;
	IPlcverifLogger logger;
	ExprValidation exprValidator;

	new(CfaNetworkInstance rootElement, IPlcverifLogger logger) {
		this.rootElement = rootElement;
		this.logger = logger;
		this.exprValidator = new ExprValidation(logger);
	}

	override getLogger() {
		return this.logger;
	}

	def dispatch check(EObject e) {}

	def dispatch check(DataRef e) {
		error("DataRef found in CFA instance: " + CfaToString.toDiagString(e));
	}

	def dispatch check(AutomatonInstance e) {
		Preconditions.checkNotNull(e.initialLocation, "initialLocation");
		Preconditions.checkNotNull(e.endLocation, "endLocation");

		if (e.initialLocation.parentAutomaton != e) {
			error("The initial location of '%' is contained by another automaton.", e.name);
		}

		if (e.endLocation.parentAutomaton != e) {
			error("The end location of '%' is contained by another automaton.", e.name);
		}
	}

	def dispatch check(AssignmentTransition e) {
		// No assignment of an assignment transition may have a reference
		// to a variable in the rightValue that is being modified by the same transition,
		// i.e. v1 := X; and v2 := Y or v1; should not be attached to the same transition
		for (amt1 : e.assignments) {
			val varRefs1 = EmfHelper.getAllContentsOfType(amt1.rightValue, AbstractVariableRef, true);

			for (amt2 : e.assignments) {
				if (amt1 !== amt2) {
					for (varRef1 : varRefs1) {
						if (CfaUtils.conflictingReferences(varRef1, amt2.leftValue)) {
							error(String.format(
								"The following two assignments are in conflict: '%s' and '%s'.",
								CfaToString.INSTANCE.toString(amt1),
								CfaToString.INSTANCE.toString(amt2)
							));
						}
					}
				}
			}
		}
	}

	def dispatch check(Expression e) {
		exprValidator.check(e);
	}

	def dispatch check(ElseExpression e) {
		error(e.eContainer instanceof Expression == false,
			"An ElseExpression cannot be nested within another expression, it shall be the root node of the expression subtree.");

		error(e.eContainer instanceof Transition,
			"ElseExpression shall only be contained by Transition expression nodes.");
	}
	
	def dispatch check(ArrayDimension e) {
		if (!e.isDefined && (e.lowerIndex != -1 || e.upperIndex != -1)) {
			error("A non-defined array dimension should have its lower and upper indices set to -1 in the CFA instance.");
		}
	}

	def dispatch check(ArrayElementRef e) {
		val arrayVarDims = e.variable.dimensions.size;
		if (arrayVarDims !== e.indices.size) {
			error('''The references to the elements of array '«CfaToString.INSTANCE.toString(e.variable)»' shall have «arrayVarDims» indices. (Reference with «e.indices.size» has been found: «CfaToString.INSTANCE.toString(e)».)''');
			return;
		}

		for (i : 0 .. (e.indices.size - 1)) {
			val index = e.indices.get(i);

			// Check type correctness of indices
			val indexType = index.type;
			if (indexType instanceof IntType) {
				if (indexType.bits != 16 || !indexType.signed) {
					error("The array index must have a signed 16-bit integer type.");
					return;
				}
			} else {
				error("The array index must have an integer type.");
				return;
			}

			// Check if they are within range if literals
			if (index instanceof IntLiteral) {
				val currentDimension = e.variable.dimensions.get(i)
				val valid = currentDimension.isValidIndex(index.value);
				if (!valid) {
					error('''The index in position «i» (zero-based) is invalid in «CfaToString.INSTANCE.toString(e)». It should be within «currentDimension.lowerIndex» and «currentDimension.upperIndex».''');
				}
			}
		}
	}
	
	def dispatch check(BeginningOfCycle e) {
		if (EmfHelper.isContainedInAny(e, CfaNetworkInstance) && !EmfHelper.isContainedInAny(e, Annotation)) {
			error("'BeginningOfCycle' is not permitted in CFA models (except for annotations).");
		}	
	}
	
	
	def dispatch check(EndOfCycle e) {
		if (EmfHelper.isContainedInAny(e, CfaNetworkInstance) && !EmfHelper.isContainedInAny(e, Annotation)) {
			error("'EndOfCycle' is not permitted in CFA models (except for annotations).");
		}	
	}
}
