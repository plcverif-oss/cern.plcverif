/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.expr.utils

import cern.plcverif.base.models.expr.BinaryCtlExpression
import cern.plcverif.base.models.expr.UnaryCtlExpression
import cern.plcverif.base.models.expr.UnaryLtlExpression
import cern.plcverif.base.models.expr.BinaryLtlExpression
import cern.plcverif.base.models.expr.Expression
import org.eclipse.emf.ecore.EObject

final class TlExprUtils {
	private new() {
		// Utility class.
	}
	
	static def containsOnlyLtl(Expression expr) {
		var ltlFound = expr.isLtl;
		for (e : expr.eAllContents.toIterable) {
			if (e.isLtl) {
				ltlFound = true;
			}
			if (e.isCtl) {
				return false;
			}
		}
		return ltlFound;
	} 
	
	static def containsOnlyCtl(Expression expr) {
		var ctlFound = expr.isCtl;
		for (e : expr.eAllContents.toIterable) {
			if (e.isCtl) {
				ctlFound = true;
			}
			if (e.isLtl) {
				return false;
			}
		}
		return ctlFound;
	}
	
	static def containsNoTl(Expression expr) {		
		for (e : expr.eAllContents.toIterable) {
			if (e.isCtl || e.isLtl) {
				return false;
			}
		}
		return true;
	}
	
	static def isptLtl(Expression expr) {
		if (containsNoTl(expr) && !expr.isLtl){
			return false;
		}	
		if (expr.isftLtl){
			return false
		}
		for (e : expr.eAllContents.toIterable) {
			if (e.isCtl || e.isftLtl) {
				return false;
			}
		}
		return true;
	}
	
	static def isftLtl(EObject expr) {
		return (expr instanceof BinaryLtlExpression && (expr as BinaryLtlExpression).operator.value == 1) || (expr instanceof UnaryLtlExpression && (expr as UnaryLtlExpression).operator.value == 1);
	}
	static def isLtl(EObject expr) {
		return expr instanceof BinaryLtlExpression || expr instanceof UnaryLtlExpression;
	}
	static def isCtl(EObject expr) {
		return expr instanceof BinaryCtlExpression || expr instanceof UnaryCtlExpression;
	}
}