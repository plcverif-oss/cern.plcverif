/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Vince Molnar - adjustment to updated models
 *******************************************************************************/
package cern.plcverif.base.models.cfa.serialization;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Path;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.ResourceEntityHandlerImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

import com.google.common.base.Preconditions;

import cern.plcverif.base.models.cfa.cfabase.CfaNetworkBase;
import cern.plcverif.base.models.cfa.cfadeclaration.impl.CfadeclarationPackageImpl;

public final class CfaToXmi {
	private CfaToXmi() {
		// Utility class.
	}
	
	public static void serializeToFile(CfaNetworkBase cfa, URI targetUri) throws IOException {
		Preconditions.checkNotNull(cfa);
		Preconditions.checkNotNull(targetUri);

		// Initialization, just in case
		CfadeclarationPackageImpl.init();

		// Create resource
		final XMIResource resource = new XMIResourceImpl(targetUri);

		// Assign IDs
		resource.getContents().add(cfa);
		new XmiIdAssignmentToCfa().assignIds(cfa, resource);

		// Save
		resource.save(getSaveOptions(resource));
	}

	public static void serializeToFile(CfaNetworkBase cfa, File targetFile) throws IOException {
		Preconditions.checkNotNull(targetFile);

		URI targetUri = URI.createURI(targetFile.toURI().toString());
		serializeToFile(cfa, targetUri);
	}
	
	public static void serializeToFile(CfaNetworkBase cfa, Path targetFile) throws IOException {
		Preconditions.checkNotNull(targetFile);

		URI targetUri = URI.createURI(targetFile.toUri().toString());
		serializeToFile(cfa, targetUri);
	}

	public static String serializeToString(CfaNetworkBase cfa) throws IOException {
		Preconditions.checkNotNull(cfa);

		// Initialization, just in case
		CfadeclarationPackageImpl.init();

		// Create resource
		final XMIResource resource = new XMIResourceImpl();

		// Create StringWriter
		StringWriter writer = new StringWriter();
		try (URIConverter.WriteableOutputStream outStream = new URIConverter.WriteableOutputStream(writer, "UTF-8")) {
			// Assign IDs
			resource.getContents().add(cfa);
			new XmiIdAssignmentToCfa().assignIds(cfa, resource);

			// Save
			resource.save(outStream, getSaveOptions(resource));

			return writer.toString();
		}
	}

	private static Map<Object, Object> getSaveOptions(XMIResource resource) {
		// Saving options
		final Map<Object, Object> saveOptions = resource.getDefaultSaveOptions();
		saveOptions.put(XMLResource.OPTION_ENCODING, "UTF-8");
		saveOptions.put(XMLResource.OPTION_FORMATTED, Boolean.TRUE);
		saveOptions.put(XMLResource.OPTION_SCHEMA_LOCATION, Boolean.TRUE);
		saveOptions.put(XMLResource.OPTION_KEEP_DEFAULT_CONTENT, Boolean.TRUE);
		saveOptions.put(XMLResource.OPTION_URI_HANDLER, new ResourceEntityHandlerImpl("x"));
		return saveOptions;
	}
}
