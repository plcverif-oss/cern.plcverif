/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - refactoring and fixes
 *****************************************************************************/
package cern.plcverif.base.models.cfa;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import com.google.common.base.Preconditions;

public final class LocalIdGenerator {
	private static class ScopeNamePair {
		public final Object scope;
		public final String name;

		public ScopeNamePair(Object scope, String name) {
			Preconditions.checkNotNull(scope, "scope");
			Preconditions.checkNotNull(name, "name");
			this.scope = scope;
			this.name = name;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof ScopeNamePair) {
				return scope.equals(((ScopeNamePair) obj).scope) && name.equals(((ScopeNamePair) obj).name);				
			} else {
				return false;
			}
		}

		@Override
		public int hashCode() {
			return Objects.hash(scope, name);
		}
	}

	public static final LocalIdGenerator INSTANCE = new LocalIdGenerator();
	public static final Object DEFAULT_SCOPE = new Object();

	private Set<ScopeNamePair> usedNames = new HashSet<>();

	public String generateId(String name) {
		return generateId(DEFAULT_SCOPE, name);
	}

	public String generateId(Object scope, String name) {
		Preconditions.checkNotNull(scope, "scope");
		Preconditions.checkNotNull(name, "name");
		int serial = 1;
		String sanitized = sanitizeName(name);
		String ret = sanitized;
		// PERF room for improvement
		while (usedNames.contains(new ScopeNamePair(scope, ret))) {
			ret = sanitized + serial;
			serial++;
		}
		usedNames.add(new ScopeNamePair(scope, ret));
		return ret;
	}

	public String sanitizeName(String name) {
		return name.replaceAll("[^a-zA-Z0-9_]", "_");
	}

	public void reset() {
		usedNames.clear();
	}
}
