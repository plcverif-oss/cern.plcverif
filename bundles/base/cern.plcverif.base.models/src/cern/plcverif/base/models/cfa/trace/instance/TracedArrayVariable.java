/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.cfa.trace.instance;

import java.util.Arrays;
import java.util.List;

import com.google.common.base.Preconditions;

import cern.plcverif.base.models.cfa.cfabase.ArrayDimension;
import cern.plcverif.base.models.cfa.cfainstance.ArrayVariable;
import cern.plcverif.base.models.cfa.trace.declared.TracedData;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.IntLiteral;

public final class TracedArrayVariable extends TracedVariable {
	private ArrayVariable representedVariable;
	private TracedArrayElementVariable[] elements;

	public TracedArrayVariable(TracedData flattenedFrom) {
		super(flattenedFrom);
	}

	@Override
	public ArrayVariable getRepresentedVariable() {
		return representedVariable;
	}

	public void setRepresentedVariable(ArrayVariable arrayVariable) {
		assert this.representedVariable == null : "Represented variable should not be set more than once.";
		this.representedVariable = arrayVariable;
	}

	public boolean isEnumerated() {
		return elements != null;
	}

	public List<TracedSingleVariable> enumerateElements() {
		assert elements == null : "Calling this method multiple times is a sign of lacking understanding of what it is for. Use this method only once, when you wish to move to element-wise representation.";
		List<ArrayDimension> arrayDimensions = this.getFlattenedFrom().getDimensions();
		Preconditions.checkState(!arrayDimensions.isEmpty(), "Traced array variable does not trace an array.");
		
		int elementCount = 1;
		for (ArrayDimension arrayDimension : arrayDimensions) {
			if (!arrayDimension.isDefined()) {
				throw new UnsupportedOperationException("It is not possible to perform the requested operation: enumerate arrays. The size of one of the arrays is not specified.");
			}
			elementCount *= arrayDimension.getSize();
		}

		elements = new TracedArrayElementVariable[elementCount];
		enumerateElements(arrayDimensions, new int[arrayDimensions.size()], 0, 0);

		return Arrays.asList(elements);
	}

	private void enumerateElements(List<ArrayDimension> arrayDimensions, int[] indices, int currentDim, int offset) {
		if (currentDim == arrayDimensions.size()) {
			// Filled indices, time to create element
			elements[offset] = new TracedArrayElementVariable(this, indices);
		}
		else {
			// Compute offset and recurse
			ArrayDimension arrayDimension = arrayDimensions.get(currentDim);
			int stepSize = 1;
			for (int i = currentDim + 1; i < arrayDimensions.size(); ++i) {
				stepSize *= arrayDimensions.get(i).getSize();
			}
			for (int i = 0; i < arrayDimension.getSize(); ++i) {
				indices[currentDim] = i + arrayDimension.getLowerIndex();
				enumerateElements(arrayDimensions, indices, currentDim + 1, offset + i * stepSize);
			}
		}
	}

	public TracedArrayElementVariable getElement(List<Expression> indices) {
		assert isEnumerated() : "Tried to access a variable representing an array element without enumerating the array.";
		List<ArrayDimension> arrayDimensions = this.getFlattenedFrom().getDimensions();
		Preconditions.checkState(!arrayDimensions.isEmpty(), "Traced array variable does not trace an array.");
		Preconditions.checkState(arrayDimensions.size() == indices.size(), "The number of provided indices does not match the number of arrays to index.");
		
		int stepsize = 1;
		int index = 0;
		for (int i = arrayDimensions.size() - 1; i >= 0; --i) {
			Expression currentIndex = indices.get(i);
			if (!(currentIndex instanceof IntLiteral)) {
				throw new IllegalArgumentException("It is not possible to perform the requested operation: enumerate arrays. One of the arrays is indexed with a variable or an expression, and therefore can be evaluated only dynamically.");
			}
			index += stepsize * (((IntLiteral) currentIndex).getValue() - arrayDimensions.get(i).getLowerIndex());
			stepsize *= arrayDimensions.get(i).getSize();
		}
		Preconditions.checkElementIndex(index, elements.length);
		return elements[index];
	}

	@Override
	public List<String> getFqn() {
		return getFlattenedFrom().getFqn();
	}
}