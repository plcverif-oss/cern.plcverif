/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 * 
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.models.cfa.transformation.assertion

import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory
import cern.plcverif.base.models.cfa.cfabase.AssertionAnnotation
import cern.plcverif.base.models.cfa.cfabase.Location
import cern.plcverif.base.models.cfa.cfabase.Transition
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.CallTransition
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef
import cern.plcverif.base.models.cfa.cfadeclaration.Field
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef
import cern.plcverif.base.models.cfa.cfadeclaration.Indexing
import cern.plcverif.base.models.cfa.transformation.assertion.AssertInliningResult.InlinedAssertion
import cern.plcverif.base.models.cfa.utils.CfaUtils
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.ExpressionSafeFactory
import cern.plcverif.base.models.expr.utils.ExprUtils
import com.google.common.base.Preconditions
import java.util.List
import java.util.Optional

import static cern.plcverif.base.models.cfa.transformation.assertion.AssertInliningResult.NO_ASSERTION_HAS_BEEN_VIOLATED
import static cern.plcverif.base.models.cfa.utils.CfaUtils.asFrozen
import static org.eclipse.emf.ecore.util.EcoreUtil.copy

/**
 * This class will modify the structure of the CFA declaration network 
 * to express the assertion annotations with transitions having the 
 * appropriate conditions. It will create a new field whose value will 
 * permit checking whether the assertion has been violated. The type of 
 * this field is determined by the chosen {@link AssertInliningStrategy}.
 */
final class AssertInliner {
	static final CfaDeclarationSafeFactory FACTORY = CfaDeclarationSafeFactory.INSTANCE;

	private new() {
		// Utility class.
	}

	def static AssertInliningResult transform(CfaNetworkDeclaration network, AssertInliningStrategy strategy) {
		Preconditions.checkNotNull(network, "network");

		val errorField = strategy.createErrorField(network.rootDataStructure);
		val result = new AssertInliningResult(strategy, asFrozen(CfaDeclarationSafeFactory.INSTANCE.createFieldRef(errorField)));

		var assertionId = NO_ASSERTION_HAS_BEEN_VIOLATED + 1;

		val f = ExpressionSafeFactory.INSTANCE_NON_STRICT;
		for (automaton : network.automata) {
			// Go to end location when assertion fails
			for (location : automaton.getLocations()) {
				val assertions = CfaUtils.getAllAnnotationsOfType(location, AssertionAnnotation);
				if (!assertions.isNullOrEmpty) {
					val List<Transition> existingTransitions = newArrayList(location.outgoing);
					location.outgoing.clear();
					
					var Transition firstFailTransition = null;
					val List<Expression> previousAssertionInvariants = newArrayList();
					val List<Expression> previousConditions = newArrayList();
					var Expression previousAssertionFailure = null;
					for (assertion : assertions) {
						location.frozen = true;
						freezeReferredFields(assertion.invariant);
						var Expression assertionFailure;
						if (previousConditions.isEmpty) {
							assertionFailure = f.neg(assertion.invariant);
						} else {
							// if the location has multiple assertion annotations, they shall be made mutually exclusive
							// This assertion fails if all previous ones were passing and this fails.
							
							// We want the assertion failure guard to be in the format of
							// NOT(I1) AND NOT(I2) AND NOT ...
							
							val otherAssertionsPassCondition = f.and(previousConditions.map[it | f.neg(it)]);							
							assertionFailure = f.and(otherAssertionsPassCondition, f.neg(assertion.invariant)); 
						}
						
						val failEdge = inlineAssertion(errorField, automaton, location, assertionFailure, assertionId, strategy);
						if (firstFailTransition === null) {
							firstFailTransition = failEdge;
						}
						
						result.addInlinedAssertion(
							new InlinedAssertion(assertionId, ExprUtils.exprCopy(assertion.invariant),
								Optional.ofNullable(assertion.name)));
						assertionId++;
						previousAssertionInvariants.add(assertion.invariant)

						previousConditions.add(f.neg(assertion.invariant));

						previousAssertionFailure = assertionFailure;
					}

					// Insert the existing transitions again, with the modified guard
					for (Transition transition : existingTransitions) {
						val Expression otherConditions = f.and(previousConditions.map[it | f.neg(it)]);
						val Expression condition = 
							if (ExprUtils.isTrueLiteral(transition.condition)) otherConditions 
							else f.and(otherConditions, transition.condition);

						previousConditions.add(transition.condition);
						transition.condition = condition;
						location.outgoing.add(transition);
					}

					// Insert an IF annotation for the assertions
					FACTORY.createIfLocationAnnotation(location, firstFailTransition.target, firstFailTransition);
				}
			}

			// Go to end location after a call if an assertion has failed
			for (transition : newArrayList(automaton.transitions)) {
				if (transition instanceof CallTransition) {
					handleSubroutineError(errorField, automaton, transition, strategy);
				}
			}
		}
		errorField.frozen = true;
		return result;
	}

	/**
	 * Sets the fields referred in the given assertion expression frozen.
	 * <p>
	 * Does not set the prefix of the referred fields frozen. Does not set anything frozen for DataRefs which are of type {@link Indexing}.
	 * <p>
	 * This is to help the user in diagnosing the problem, not to ensure the correctness!
	 */
	private def static freezeReferredFields(Expression assertionInvariant) {
		for (dataRef : EmfHelper.getAllContentsOfType(assertionInvariant, DataRef, true)) {
			if (dataRef instanceof FieldRef) {
				if (!(dataRef.eContainer instanceof DataRef)) {
					dataRef.field.frozen = true;
				// This is to help the user in diagnosing the problem, not to ensure the correctness!
				} else {
					// don't freeze the whole container (i.e. the prefixes), only the leaf of the data reference!
				}
			} else {
				// Do not freeze a complete array just because a part of it is needed for the requirement.
			}
		}
	}

	private def static Transition inlineAssertion(Field errorField, AutomatonDeclaration declaration, Location location,
		Expression assertionFailure, long assertionId, AssertInliningStrategy strategy) {
		val failTransition = FACTORY.createAssignmentTransition("fail", declaration, location, declaration.endLocation,
			copy(assertionFailure));			
		
		strategy.createViolatedAssignment(failTransition, FACTORY.createFieldRef(errorField), assertionId);
		
		return failTransition;
	}

	private def static handleSubroutineError(Field errorField, AutomatonDeclaration declaration,
		CallTransition callTransition, AssertInliningStrategy strategy) {
		val factory = CfaDeclarationSafeFactory.INSTANCE;
		val location = callTransition.target;
		for (transition : location.outgoing) {
			val originalExpr = transition.condition;
			transition.condition = null;
			transition.condition = factory.and(
				strategy.createNoAssertionViolatedExpr(FACTORY.createFieldRef(errorField)),
				originalExpr
			);
		}

		factory.createAssignmentTransition(
			"fail",
			declaration,
			location,
			declaration.endLocation,
			strategy.createSomeAssertionViolatedExpr(FACTORY.createFieldRef(errorField))
		);
	}
}
