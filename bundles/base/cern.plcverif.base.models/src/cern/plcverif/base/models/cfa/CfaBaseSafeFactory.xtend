/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.models.cfa

import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.base.models.cfa.cfabase.ArrayDimension
import cern.plcverif.base.models.cfa.cfabase.ArrayType
import cern.plcverif.base.models.cfa.cfabase.AssertionAnnotation
import cern.plcverif.base.models.cfa.cfabase.AutomatonBase
import cern.plcverif.base.models.cfa.cfabase.BlockAutomatonAnnotation
import cern.plcverif.base.models.cfa.cfabase.CaseLocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.CfabaseFactory
import cern.plcverif.base.models.cfa.cfabase.ContinueTransitionAnnotation
import cern.plcverif.base.models.cfa.cfabase.ElseExpression
import cern.plcverif.base.models.cfa.cfabase.ExitTransitionAnnotation
import cern.plcverif.base.models.cfa.cfabase.GotoTransitionAnnotation
import cern.plcverif.base.models.cfa.cfabase.IfLocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.LabelledLocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.LineNumberAnnotation
import cern.plcverif.base.models.cfa.cfabase.Location
import cern.plcverif.base.models.cfa.cfabase.LoopLocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.RepeatLoopLocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.SwitchLocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.Transition
import cern.plcverif.base.models.cfa.cfabase.WhileLoopLocationAnnotation
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.Type
import com.google.common.base.Preconditions
import cern.plcverif.base.models.expr.ExpressionSafeFactory
import cern.plcverif.base.models.cfa.cfabase.BlockReturnTransitionAnnotation
import cern.plcverif.base.models.expr.Literal
import cern.plcverif.base.models.expr.IntType

class CfaBaseSafeFactory extends ExpressionSafeFactory {
	public static val INSTANCE = new CfaBaseSafeFactory(DEFAULT_STRICT);
	
	protected new(boolean strict) {
		super(strict);
	}
		
	// Location
	// --------
	
	def Location createLocation(String displayName, AutomatonBase parentAutomaton) {
		Preconditions.checkNotNull(displayName, "displayName");
		Preconditions.checkNotNull(parentAutomaton, "parentAutomaton");
		
		var Location ret = CfabaseFactory.eINSTANCE.createLocation();
		ret.name = LocalIdGenerator.INSTANCE.generateId(parentAutomaton, displayName);
		ret.displayName = displayName;
		ret.parentAutomaton = parentAutomaton;
		return ret;
	}
	
	def Location createInitialLocation(String displayName, AutomatonBase parentAutomaton) {
		Preconditions.checkNotNull(displayName, "displayName");
		Preconditions.checkNotNull(parentAutomaton, "parentAutomaton");
		Preconditions.checkState(parentAutomaton.initialLocation === null, "The specified automaton already has an initial location.");

		var Location ret = createLocation(displayName, parentAutomaton);
		ret.frozen = true;
		parentAutomaton.initialLocation = ret;
		return ret;
	}
	
	def Location createEndLocation(String displayName, AutomatonBase parentAutomaton) {
		Preconditions.checkNotNull(displayName, "displayName");
		Preconditions.checkNotNull(parentAutomaton, "parentAutomaton");
		Preconditions.checkState(parentAutomaton.endLocation === null, "The specified automaton already has an end location.");
		
		var Location ret = createLocation(displayName, parentAutomaton);
		ret.frozen = true;
		parentAutomaton.endLocation = ret;
		return ret;
	}
	
	// Array types
	// -----------
	
	def ArrayDimension createArrayDimension(int inclusiveLowerIndex, int inclusiveUpperIndex) {
		val ret = CfabaseFactory.eINSTANCE.createArrayDimension();
		ret.defined = true;
		ret.lowerIndex = inclusiveLowerIndex;
		ret.upperIndex = inclusiveUpperIndex;
		return ret;
	}
	
	def ArrayDimension createUndefinedArrayDimension() {
		val ret = CfabaseFactory.eINSTANCE.createArrayDimension();
		ret.defined = false;
		return ret;
	}
	
	def ArrayType createArrayType(Type elementType, ArrayDimension dimension) {
		Preconditions.checkNotNull(elementType, "elementType");
		Preconditions.checkNotNull(dimension, "dimension");
		EmfHelper.checkNotContained(elementType, "The provided element type is already assigned to another model element. Copy it with EcoreUtil.copy().");
		EmfHelper.checkNotContained(dimension, "The provided array dimension is already assigned to another model element. Copy it with EcoreUtil.copy().");
		
		val ret = CfabaseFactory.eINSTANCE.createArrayType();
		ret.elementType = elementType;
		ret.dimension = dimension;
		return ret;
	}
	
	def ArrayType createArrayType(Type elementType, int inclusiveLowerIndex, int inclusiveUpperIndex) {
		return createArrayType(elementType, createArrayDimension(inclusiveLowerIndex, inclusiveUpperIndex));
	}
	
	def ArrayType createUndefinedArrayType(Type elementType) {
		return createArrayType(elementType, createUndefinedArrayDimension());
	}
	
	/**
	 * Creates an index literal with the given value and with the expected type. (Currently: 16-bit signed integer.)
	 */
	def Literal createIndexLiteral(long value) {
		return createIntLiteral(value, true, 16);
	}
	
	/**
	 * Returns true iff the given type is valid to index an array.
	 */
	protected def boolean isValidIndexType(Type type) {
		if (type instanceof IntType) {
			return (type.signed && type.bits == 16);
		}
		
		return false;
	}
	
	// Special expressions
	// -------------------
	
	def ElseExpression createElseExpression() {
		val ret = CfabaseFactory.eINSTANCE.createElseExpression();
		ret.type = createBoolType();
		return ret;
	}
	
	// Annotations
	// -----------
	
	def BlockAutomatonAnnotation createBlockAutomatonAnnotation(AutomatonBase parentAutomaton, String blockType, String name, boolean stateful) {
		Preconditions.checkNotNull(parentAutomaton, "parentAutomaton");
		Preconditions.checkNotNull(blockType, "blockType");
		Preconditions.checkNotNull(name, "name");
		
		val ret = CfabaseFactory.eINSTANCE.createBlockAutomatonAnnotation();
		ret.parentAutomaton = parentAutomaton;
		ret.blockType = blockType;
		ret.name = name;
		ret.stateful = stateful;
		return ret;
	}
	
	def IfLocationAnnotation createIfLocationAnnotation(Location parentLocation, Location endsAt, Transition then) {
		Preconditions.checkNotNull(parentLocation, "parentLocation");
		Preconditions.checkNotNull(endsAt, "endsAt");
		Preconditions.checkNotNull(then, "then");
		
		val ret = CfabaseFactory.eINSTANCE.createIfLocationAnnotation();
		ret.parentLocation = parentLocation;
		ret.endsAt = endsAt;
		ret.then = then;
		return ret;
	}
	
	def SwitchLocationAnnotation createSwitchLocationAnnotation(Location parentLocation, Location endsAt, Expression selectionExpr) {
		Preconditions.checkNotNull(parentLocation, "parentLocation");
		Preconditions.checkNotNull(endsAt, "endsAt");
		Preconditions.checkNotNull(selectionExpr, "selectionExpr");
		
		val ret = CfabaseFactory.eINSTANCE.createSwitchLocationAnnotation();
		ret.parentLocation = parentLocation;
		ret.endsAt = endsAt;
		ret.selectionExpr = selectionExpr;
		return ret;
	}
	
	def CaseLocationAnnotation createCaseLocationAnnotation(Location parentLocation, SwitchLocationAnnotation switchAnnotation) {
		Preconditions.checkNotNull(parentLocation, "parentLocation");
		Preconditions.checkNotNull(switchAnnotation, "switchAnnotation");
		
		val ret = CfabaseFactory.eINSTANCE.createCaseLocationAnnotation();
		ret.parentLocation = parentLocation;
		ret.switchAnnotation = switchAnnotation;
		return ret;
	}
	
	def CaseLocationAnnotation createElseCaseLocationAnnotation(Location parentLocation, SwitchLocationAnnotation switchAnnotation) {
		Preconditions.checkNotNull(parentLocation, "parentLocation");
		Preconditions.checkNotNull(switchAnnotation, "switchAnnotation");
		
		val ret = CfabaseFactory.eINSTANCE.createCaseLocationAnnotation();
		ret.parentLocation = parentLocation;
		ret.switchAnnotation = switchAnnotation;
		switchAnnotation.elseCase = ret;
		return ret;
	}
	
	protected def void setupLoopLocationAnnotation(Location parentLocation, LoopLocationAnnotation annotation, Transition loopBodyEntry, Transition loopNextCycle, Transition loopExit) {
		Preconditions.checkNotNull(parentLocation, "parentLocation");
		Preconditions.checkNotNull(annotation, "annotation");
		Preconditions.checkNotNull(loopBodyEntry, "loopBodyEntry");
		Preconditions.checkNotNull(loopNextCycle, "loopNextCycle");
		Preconditions.checkNotNull(loopExit, "loopExit");
		
		annotation.parentLocation = parentLocation;
		annotation.loopBodyEntry = loopBodyEntry;
		annotation.loopNextCycle = loopNextCycle;
		annotation.loopExit = loopExit;
	}
	
	def WhileLoopLocationAnnotation createWhileLoopLocationAnnotation(Location parentLocation, Transition loopBodyEntry, Transition loopNextCycle, Transition loopExit) {
		val ret = CfabaseFactory.eINSTANCE.createWhileLoopLocationAnnotation();
		setupLoopLocationAnnotation(parentLocation, ret, loopBodyEntry, loopNextCycle, loopExit);
		return ret;
	}
	
	def RepeatLoopLocationAnnotation createRepeatLoopLocationAnnotation(Location parentLocation, Transition loopBodyEntry, Transition loopNextCycle, Transition loopExit) {
		val ret = CfabaseFactory.eINSTANCE.createRepeatLoopLocationAnnotation();
		setupLoopLocationAnnotation(parentLocation, ret, loopBodyEntry, loopNextCycle, loopExit);
		return ret;
	}
	
	def LabelledLocationAnnotation createLabelledLocationAnnotation(Location parentLocation, String labelName) {
		Preconditions.checkNotNull(parentLocation, "parentLocation");
		Preconditions.checkNotNull(labelName, "labelName");
		
		val ret = CfabaseFactory.eINSTANCE.createLabelledLocationAnnotation();
		ret.parentLocation = parentLocation;
		ret.labelName = labelName;
		return ret;
	}
	
	def LineNumberAnnotation createLineNumberAnnotation(Location parentLocation, String fileName, int lineNumber) {
		Preconditions.checkNotNull(parentLocation, "parentLocation");
		Preconditions.checkNotNull(fileName, "fileName");
		
		val ret = CfabaseFactory.eINSTANCE.createLineNumberAnnotation();
		ret.parentLocation = parentLocation;
		ret.fileName = fileName;
		ret.lineNumber = lineNumber;
		return ret;
	}
	
	def LineNumberAnnotation createLineNumberAnnotation(Location parentLocation, int lineNumber) {
		Preconditions.checkNotNull(parentLocation, "parentLocation");
		val ret = CfabaseFactory.eINSTANCE.createLineNumberAnnotation();
		ret.parentLocation = parentLocation;
		ret.lineNumber = lineNumber;
		return ret;
	}
	
	def AssertionAnnotation createAssertionAnnotation(Location parentLocation, Expression invariant) {
		return this.createAssertionAnnotation(parentLocation, invariant, null);
	}
	
	def AssertionAnnotation createAssertionAnnotation(Location parentLocation, Expression invariant, String name) {
		Preconditions.checkNotNull(parentLocation, "parentLocation");
		Preconditions.checkNotNull(invariant, "expr");
		EmfHelper.checkNotContained(invariant, "The provided expression is already assigned to another model element. Copy it with EcoreUtil.copy().");
		// 'name' may be null
		
		val ret = CfabaseFactory.eINSTANCE.createAssertionAnnotation();
		ret.parentLocation = parentLocation;
		ret.invariant = invariant;
		ret.name = name;
		return ret;
	}
	
	def GotoTransitionAnnotation createGotoTransitionAnnotation(Transition parentTransition, String targetLabel) {
		Preconditions.checkNotNull(parentTransition, "parentTransition");
		Preconditions.checkNotNull(targetLabel, "targetLabel");
		
		val ret = CfabaseFactory.eINSTANCE.createGotoTransitionAnnotation();
		ret.parentTransition = parentTransition;
		ret.targetLabel = targetLabel;
		return ret;
	}
	
	def ContinueTransitionAnnotation createContinueTransitionAnnotation(Transition parentTransition) {
		Preconditions.checkNotNull(parentTransition, "parentTransition");
		
		val ret = CfabaseFactory.eINSTANCE.createContinueTransitionAnnotation();
		ret.parentTransition = parentTransition;
		return ret;
	}
	
	def ExitTransitionAnnotation createExitTransitionAnnotation(Transition parentTransition) {
		Preconditions.checkNotNull(parentTransition, "parentTransition");
		
		val ret = CfabaseFactory.eINSTANCE.createExitTransitionAnnotation();
		ret.parentTransition = parentTransition;
		return ret;
	}
	
	def BlockReturnTransitionAnnotation createBlockReturnTransitionAnnotation(Transition parentTransition) {
		Preconditions.checkNotNull(parentTransition, "parentTransition");
		
		val ret = CfabaseFactory.eINSTANCE.createBlockReturnTransitionAnnotation();
		ret.parentTransition = parentTransition;
		return ret;
	}
}
