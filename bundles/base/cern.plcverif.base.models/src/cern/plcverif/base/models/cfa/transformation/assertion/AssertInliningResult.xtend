/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 * 
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.cfa.transformation.assertion

import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef
import cern.plcverif.base.models.expr.Expression
import com.google.common.base.Preconditions
import java.util.Optional
import java.util.SortedMap
import java.util.TreeMap
import org.eclipse.xtend.lib.annotations.Data
import org.eclipse.emf.ecore.util.EcoreUtil

/**
 * Class that represents the result of assertion inlining. It contains 
 * reference to the 'error variable' field that can be used in the 
 * temporal logic expression. If the value of this field is not zero, 
 * one of the assertions has been violated. The non-zero value of this
 * field will identify which inlined assertion has been violated. The 
 * {@link #getAssertion(long)} function can be used to identify the violated
 * assertion based on the value of the error field.
 */
class AssertInliningResult {
	public static final long NO_ASSERTION_HAS_BEEN_VIOLATED = 0;

	/**
	 * Inlined assertion descriptor.
	 */
	@Data
	static class InlinedAssertion {
		 final long id;
		 final Expression assertionInvariant;
		 final Optional<String> tag;
	}

	/**
	 * The variable whose value changes from 0 if one of the inlined assertions has been violated.
	 */
	 final FieldRef errorField;

	 final AssertInliningStrategy inliningStrategy;

	 SortedMap<Long, InlinedAssertion> inlinedAssertions = new TreeMap();

	/**
	 * Creates a new assertion inlining result.
	 * @param inliningStrategy
	 * 			The inlining strategy used to produce this result. 
	 * @param errorField
	 * 			Reference to the field that will determine the 
	 * 			violated assertion (or will have the value 0 is none of them has 
	 * 			been violated).
	 */
	new(AssertInliningStrategy inliningStrategy, FieldRef errorField) {
		this.inliningStrategy = Preconditions.checkNotNull(inliningStrategy);
		this.errorField = Preconditions.checkNotNull(errorField);
	}

	/**
	 * Returns a reference to the error field. Never {@code null}.
	 * The returned {@link FieldRef} will not have any container set (a new copy is returned). 
	 */
	def FieldRef getErrorField() {
		return EcoreUtil.copy(errorField);
	}

	package def addInlinedAssertion(InlinedAssertion item) {
		Preconditions.checkNotNull(item);
		this.inlinedAssertions.put(item.getId(), item);
	}

	/** 
	 * Returns the inlined assertion descriptor corresponding to the given ID. 
	 */
	def Optional<InlinedAssertion> getAssertion(long id) {
		return Optional.ofNullable(inlinedAssertions.getOrDefault(id, null));
	}

	/**
	 * Returns the number of known inlined assertions.
	 */
	def int getInlinedAssertionCount() {
		return inlinedAssertions.size();
	}

	/**
	 * Returns the inlining strategy that was used to produce this inlining result.
	 */
	def AssertInliningStrategy getInliningStrategy() {
		return inliningStrategy;
	}
}
