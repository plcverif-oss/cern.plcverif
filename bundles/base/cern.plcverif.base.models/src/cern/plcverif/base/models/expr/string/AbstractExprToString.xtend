/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Vince Molnar - update to new metamodels
 *   Xaver Fink - additions
 *****************************************************************************/
package cern.plcverif.base.models.expr.string

import cern.plcverif.base.models.expr.BeginningOfCycle
import cern.plcverif.base.models.expr.BinaryArithmeticExpression
import cern.plcverif.base.models.expr.BinaryArithmeticOperator
import cern.plcverif.base.models.expr.BinaryCtlExpression
import cern.plcverif.base.models.expr.BinaryCtlOperator
import cern.plcverif.base.models.expr.BinaryLogicExpression
import cern.plcverif.base.models.expr.BinaryLogicOperator
import cern.plcverif.base.models.expr.BinaryLtlExpression
import cern.plcverif.base.models.expr.BinaryLtlOperator
import cern.plcverif.base.models.expr.BoolLiteral
import cern.plcverif.base.models.expr.BoolType
import cern.plcverif.base.models.expr.ComparisonExpression
import cern.plcverif.base.models.expr.ComparisonOperator
import cern.plcverif.base.models.expr.EndOfCycle
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.FloatLiteral
import cern.plcverif.base.models.expr.FloatType
import cern.plcverif.base.models.expr.IntLiteral
import cern.plcverif.base.models.expr.IntType
import cern.plcverif.base.models.expr.LibraryFunction
import cern.plcverif.base.models.expr.LibraryFunctions
import cern.plcverif.base.models.expr.Literal
import cern.plcverif.base.models.expr.Nondeterministic
import cern.plcverif.base.models.expr.StringLiteral
import cern.plcverif.base.models.expr.StringType
import cern.plcverif.base.models.expr.TemporalBoolType
import cern.plcverif.base.models.expr.Type
import cern.plcverif.base.models.expr.TypeConversion
import cern.plcverif.base.models.expr.UnaryArithmeticExpression
import cern.plcverif.base.models.expr.UnaryArithmeticOperator
import cern.plcverif.base.models.expr.UnaryCtlExpression
import cern.plcverif.base.models.expr.UnaryCtlOperator
import cern.plcverif.base.models.expr.UnaryLogicExpression
import cern.plcverif.base.models.expr.UnaryLogicOperator
import cern.plcverif.base.models.expr.UnaryLtlExpression
import cern.plcverif.base.models.expr.UnaryLtlOperator
import cern.plcverif.base.models.expr.UninterpretedFunction
import cern.plcverif.base.models.expr.UninterpretedSymbol
import cern.plcverif.base.models.expr.UnknownType
import java.util.List
import org.eclipse.emf.ecore.EObject

// For Jira PV-66 and PV-51
import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory
import org.eclipse.emf.ecore.util.EcoreUtil;
import cern.plcverif.base.models.expr.TernaryArithmeticExpression

//

abstract class AbstractExprToString {
	protected new() {}
	
	// Root case
	/**
	 * Returns the textual representation of the given expression.
	 * If the given expression node is not atomic, the returned string shall be surrounded with parentheses.
	 */
	def dispatch CharSequence toString(EObject obj) {
		return obj.toString();
	}
	
	def dispatch CharSequence toString(Object obj) {
		throw new UnsupportedOperationException("Only EObject and (E)List instances are supported.");
	}
	
	def dispatch CharSequence toString(List<? extends EObject> list) {
		if (list.isEmpty) {
			return "[<empty>]";
		} else {
			return '''[«FOR it : list SEPARATOR ', '»«toString(it)»«ENDFOR»]'''
		}
	}
	
	// Expressions
	// -----------
	
	// Generic cases
	
	def dispatch CharSequence toString(Expression expr) {
		return "?"
	}
	
	def dispatch CharSequence toString(Void expr) {
		return "<null>"
	}
	
	// N-ary operations
	
	def dispatch CharSequence toString(UninterpretedFunction expr) {
		return '''«expr.symbol»(«FOR op : expr.operands»«toString(op)»«ENDFOR»)''';	
	}
	
	def dispatch CharSequence toString(LibraryFunction expr) {
		return '''«opToString(expr.function)»(«FOR op : expr.operands»«toString(op)»«ENDFOR»)''';	
	}
	
	// Binary operations
	
	def dispatch CharSequence toString(BinaryArithmeticExpression expr) {
		return '''(«toString(expr.leftOperand)» «opToString(expr.operator)» «toString(expr.rightOperand)»)''';	
	}
	
	def dispatch CharSequence toString(TernaryArithmeticExpression expr) {
		return '''(«toString(expr.left)» ? «toString(expr.middle)» : «toString(expr.right)» )''';	
	}
	
	def dispatch CharSequence toString(ComparisonExpression expr) {
		return '''(«toString(expr.leftOperand)» «opToString(expr.operator)» «toString(expr.rightOperand)»)''';	
	}
	
	def dispatch CharSequence toString(BinaryLogicExpression expr) {
		return '''(«toString(expr.leftOperand)» «opToString(expr.operator)» «toString(expr.rightOperand)»)''';	
	}
	
	def dispatch CharSequence toString(BinaryCtlExpression expr) {
		return '''(«opToPrefixString(expr.operator)»[«toString(expr.leftOperand)» «opToInfixString(expr.operator)» «toString(expr.rightOperand)»])''';	
	}
	
	def dispatch CharSequence toString(BinaryLtlExpression expr) {
		return '''(«toString(expr.leftOperand)» «opToString(expr.operator)» «toString(expr.rightOperand)»)''';	
	}
	
	// Unary expressions
	def dispatch CharSequence toString(UnaryLogicExpression expr) {
		return '''(«opToString(expr.operator)» «toString(expr.operand)»)''';	
	}
	
	def dispatch CharSequence toString(UnaryArithmeticExpression expr) {
		// For the "not" operator (Jira PV-66 and PV-51) we need to cast the result of the "not" operation
		val cast = toString(CfaDeclarationSafeFactory.INSTANCE.createTypeConversion(EcoreUtil.copy(expr.operand), EcoreUtil.copy(expr.operand.type)))
			.toString().replace(toString(expr.operand),"")
		return '''(«IF expr.operator.literal == "BITWISE_NOT"»«cast.substring(1,cast.length()-2)»«ENDIF»«opToString(expr.operator)» «toString(expr.operand)»)''';
	}
	
	def dispatch CharSequence toString(UnaryCtlExpression expr) {
		return '''(«opToString(expr.operator)» «toString(expr.operand)»)''';	
	}
	
	def dispatch CharSequence toString(UnaryLtlExpression expr) {
		return '''(«opToString(expr.operator)» «toString(expr.operand)»)''';	
	}
	
	def dispatch CharSequence toString(TypeConversion expr) {
		return '''((«toString(expr.type)») «toString(expr.operand)»)''';	
	}
		
	def dispatch CharSequence toString(UninterpretedSymbol expr) {
		return '''?«expr.symbol»?''';
	}
	
	def dispatch CharSequence toString(Nondeterministic expr) {
		return '''NONDETERMINISTIC of «toString(expr.type)»''';
	}
	
	// Literals
	def dispatch CharSequence toString(Literal expr) {
		return "? (constant)";	
	}
	
	def dispatch CharSequence toString(IntLiteral expr) {
		return '''«expr.value»''';	
	}
	
	def dispatch CharSequence toString(FloatLiteral expr) {
		return '''«expr.value»''';	
	}
	
	def dispatch CharSequence toString(BoolLiteral expr) {
		return '''«expr.value»''';	
	}
	
	def dispatch CharSequence toString(StringLiteral expr) {
		return "unknown type";
	}
	
	// Misc
	def dispatch CharSequence toString(BeginningOfCycle expr) {
		return "<BoC>";
	}
	
	def dispatch CharSequence toString(EndOfCycle expr) {
		return "<EoC>";
	}
	
	// Operators
	
	def opToString(LibraryFunctions function) {
		switch (function) {
			case SIN: return "sin"
			case COS: return "cos"
			case TAN: return "tan"
			case SQRT: return "sqrt"
			case EXP: return "exp"
			case LN: return "ln"
			case LOG: return "log"
			case ASIN: return "asin"
			case ACOS: return "acos"
			case ATAN: return "atan"
		}
		throw new UnsupportedOperationException("Unknown library function: " + function);
	}
	
	def opToString(BinaryArithmeticOperator operator) {
		switch (operator) {
			case PLUS: return "+"
			case MINUS: return "-"
			case MULTIPLICATION: return "*"
			case DIVISION: return "/"
			case MODULO: return "MOD"
			case INTEGER_DIVISION: return "DIV"
			case POWER: return "^"
			case BITSHIFT_RIGHT: return "b>>"
			case BITSHIFT_LEFT: return "b<<"
			case BITROTATE: return "b>>>"
			case BITWISE_OR: return "|\u1D47" // \u1D47 is a superscript 'b'
			case BITWISE_AND: return "&\u1D47"
			case BITWISE_XOR: return "^\u1D47;"
		}
		throw new UnsupportedOperationException("Unknown arithmetic operator: " + operator);
	}
	
	def opToString(ComparisonOperator operator) {
		switch (operator) {
			case EQUALS: return "="
			case NOT_EQUALS: return "!="
			case LESS_THAN: return "<"
			case GREATER_THAN: return ">"
			case LESS_EQ: return "<="
			case GREATER_EQ: return ">="
		}
		throw new UnsupportedOperationException("Unknown comparison operator: " + operator);
	}
		
	def opToString(BinaryLogicOperator operator) {
		switch (operator) {
			case AND: return "&&"
			case OR: return "||"
			case XOR: return "^"
			case IMPLIES: return "-->"	
		}
		throw new UnsupportedOperationException("Unknown logic operator: " + operator);
	}
	
	def CharSequence opToPrefixString(BinaryCtlOperator operator) {
		switch (operator) {
			case AU: return "A"
			case EU: return "E"
		}
		throw new UnsupportedOperationException("Unknown CTL operator: " + operator);
	}
	
	def CharSequence opToInfixString(BinaryCtlOperator operator) {
		switch (operator) {
			case AU: return "U"
			case EU: return "U"
		}
		throw new UnsupportedOperationException("Unknown CTL operator: " + operator);
	}
		
	def opToString(BinaryLtlOperator operator) {
		switch (operator) {
			case U: return "U"
			case R: return "R"
			case S: return "S"
		}
		throw new UnsupportedOperationException("Unknown LTL operator: " + operator);
	}
	
	def opToString(UnaryLogicOperator operator) {
		switch (operator) {
			case NEG: return "!"
		}
		throw new UnsupportedOperationException("Unknown logic operator: " + operator);
	}
	
	def opToString(UnaryArithmeticOperator operator) {
		switch (operator) {
			case MINUS: return "-"
			case BITWISE_NOT: return "~"
		}
		throw new UnsupportedOperationException("Unknown arithmetic operator: " + operator);
	}
		
	def opToString(UnaryCtlOperator operator) {
		switch (operator) {
			case AX: return "AX"
			case AF: return "AF"
			case AG: return "AG"
			case EX: return "EX"
			case EF: return "EF"
			case EG: return "EG"
		}
		throw new UnsupportedOperationException("Unknown CTL operator: " + operator);
	}
		
	def opToString(UnaryLtlOperator operator) {
		switch (operator) {
			case X: return "X"
			case F: return "F"
			case G: return "G"
			case Y: return "Y"
			case O: return "O"
			case H: return "H"
		}
		throw new UnsupportedOperationException("Unknown LTL operator: " + operator);
	}
	
	// Types
	// -----
	
	// Generic cases
	def dispatch CharSequence toString(Type type) {
		return "?"
	}
	
	// Elementary types
	def dispatch CharSequence toString(IntType type) {
		val signedStr = if (type.signed) "signed" else "unsigned"; 
		return String.format("%s int%d", signedStr, type.bits);
	}
	
	def dispatch CharSequence toString(FloatType type) {
		return String.format("float%s", type.bits.literal);
	}
	
	def dispatch CharSequence toString(BoolType type) {
		return "bool";
	}
	
	def dispatch CharSequence toString(TemporalBoolType type) {
		return "temporal bool";
	}
	
	def dispatch CharSequence toString(StringType type) {
		return String.format("string%d", type.maxLength);
	}
	
	// Misc
	
	def dispatch CharSequence toString(UnknownType type) {
		return "unknown type";
	}
	
}
