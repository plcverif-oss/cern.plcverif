/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.models.cfa.textual

import cern.plcverif.base.models.expr.string.AbstractExprToString
import cern.plcverif.base.models.cfa.cfabase.ElseExpression
import cern.plcverif.base.models.cfa.cfabase.ArrayDimension
import cern.plcverif.base.models.cfa.cfabase.ArrayType
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructure
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructureRef
import org.eclipse.emf.ecore.EObject
import cern.plcverif.base.models.cfa.cfabase.NamedElement
import cern.plcverif.base.models.cfa.cfadeclaration.Field
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef
import cern.plcverif.base.models.cfa.cfadeclaration.Indexing
import com.google.common.base.Preconditions
import cern.plcverif.base.models.cfa.cfainstance.Variable
import cern.plcverif.base.models.cfa.cfainstance.FullyQualifiedName
import cern.plcverif.base.models.cfa.cfainstance.ArrayVariable
import cern.plcverif.base.models.cfa.cfainstance.VariableRef
import cern.plcverif.base.models.cfa.cfainstance.ArrayElementRef
import cern.plcverif.base.models.cfa.cfainstance.VariableAssignment

class CfaToString extends AbstractExprToString {
	public static final CfaToString INSTANCE = new CfaToString(true);	
	
	boolean useDisplayNames; 	
	
	/**
	 * Creates a new CFA string representation instance.
	 * @param useDisplayNames If true, the display names of the fields or 
	 *     variables will be used. Otherwise it will use the (sanitized) 
	 *     CFA names for the fields and variables.
	 */
	new(boolean useDisplayNames) {
		this.useDisplayNames = useDisplayNames;
	}
	
	
	static def String toDiagString(EObject obj) {
		return INSTANCE.toString(obj).toString();
	}
	
	def dispatch toString(ElseExpression expr) {
		return "ELSE";
	}
	
	def dispatch toString(NamedElement elem) {
		return elem.nameToUse;
	}
	
	// Arrays
	
	def dispatch CharSequence toString(ArrayDimension dim) {
		return if (dim.defined) dim.lowerIndex + ".." + dim.upperIndex else "?..?";
	}
	
	def dispatch CharSequence toString(ArrayType type) {
		return '''«this.toString(type.elementType)»[«toString(type.dimension)»]''';
	}
	
	// Data structures
	
	def dispatch CharSequence toString(DataStructure ds) {
		return (if (ds.enclosingType === null) "" else toString(ds.enclosingType) + segmentSeparator()) + ds.nameToUse;
	}
	
	def dispatch CharSequence toString(DataStructureRef type) {
		return toString(type.definition);
	}
	
	// Fields and references
	
	def dispatch CharSequence toString(Field field) {
		return field.nameToUse + " : " + this.toString(field.type);
	}
	
	def dispatch CharSequence toString(FieldRef ref) {
		return (if (ref.prefix === null) "" else (this.toString(ref.prefix) + segmentSeparator())) + ref.field.nameToUse;
	}
	
	def dispatch CharSequence toString(Indexing ref) {
		return '''«if (ref.prefix === null) "null" else this.toString(ref.prefix)»[«this.toString(ref.index)»]''';
	}
	
	// Variables and references
	
	def dispatch CharSequence toString(FullyQualifiedName fqn) {
		return String.join(segmentSeparator(), fqn.identifiers);
	}
	
	def dispatch CharSequence toString(Variable v) {
		Preconditions.checkNotNull(v.fqn, "v.fqn");
		return this.toString(v.fqn);
		//return '''«this.toString(v.fqn)» : «this.toString(v.type)»''';
	}
	
	def dispatch CharSequence toString(ArrayVariable v) {
		Preconditions.checkNotNull(v.fqn, "v.fqn");
		var String ret = v.fqn.toString() + "[";
		for (dimension : v.dimensions) {
			if (!ret.endsWith("[")) {
				ret += ",";
			}
			ret += toString(dimension);
		}
		ret = ret + "]";
		return '''«ret» : «this.toString(v.type)»''';
	}
	
	def dispatch CharSequence toString(VariableRef ref) {
		Preconditions.checkNotNull(ref.variable, "ref.variable");
		Preconditions.checkNotNull(ref.variable.fqn, "ref.variable.fqn");
		return this.toString(ref.variable.fqn);
	}
	
	def dispatch CharSequence toString(ArrayElementRef ref) {
		Preconditions.checkNotNull(ref.variable, "ref.variable");
		Preconditions.checkNotNull(ref.variable.fqn, "ref.variable.fqn");
		var String ret = this.toString(ref.variable.fqn) + "[";
		for (index : ref.indices) {
			if (!ret.endsWith("[")) {
				ret += ",";
			}
			ret += this.toString(index);
		}
		return ret + "]";
	}
	
	// Variable assignments
	def dispatch CharSequence toString(VariableAssignment amt) {
		Preconditions.checkNotNull(amt, "amt");
		Preconditions.checkNotNull(amt.leftValue, "amt.leftValue");
		Preconditions.checkNotNull(amt.rightValue, "amt.rightValue");
		return '''«toString(amt.leftValue)» := «toString(amt.rightValue)»''';
	}
	
	def dispatch CharSequence toString(cern.plcverif.base.models.cfa.cfadeclaration.VariableAssignment amt) {
		Preconditions.checkNotNull(amt, "amt");
		Preconditions.checkNotNull(amt.leftValue, "amt.leftValue");
		Preconditions.checkNotNull(amt.rightValue, "amt.rightValue");
		return '''«toString(amt.leftValue)» := «toString(amt.rightValue)»''';
	}
	
	protected def String segmentSeparator() {
		return "/";
	}
	
	private def String nameToUse(NamedElement e) {
		if (useDisplayNames && e.displayName !== null && !e.displayName.isEmpty) {
			return e.displayName;
		} else {
			return e.name;
		}
	}
}
