/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.cfa.transformation;

import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;

/**
 * Enum specifying the possible goals of CFA instantiation and transformation.
 *
 * <ul>
 * <li>{@link #DECLARATION}: The model will not be instantiated (will be an
 * instance of {@link CfaNetworkDeclaration}). Certain transformation (like
 * constant propagation) may be applied.</li>
 * <li>{@link #INSTANCE_ARRAYS}: The model will be instantiated (will be an
 * instance of {@link CfaNetworkInstance}). Transformations to facilitate the
 * instantiation (such as constant propagation and loop unrolling).</li>
 * <li>{@link #INSTANCE_ENUMERATED}: The model will be instantiated (will be an
 * instance of {@link CfaNetworkInstance}). Aggressive transformations will be
 * applied to eliminate indexing with dynamic expressions and arrays of
 * undefined size.</li>
 * </ul>
 */
public enum CfaTransformationGoal {
	DECLARATION, INSTANCE_ARRAYS, INSTANCE_ENUMERATED,
}
