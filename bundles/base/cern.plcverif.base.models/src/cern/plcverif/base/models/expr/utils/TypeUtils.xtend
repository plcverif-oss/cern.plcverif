/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.models.expr.utils

import cern.plcverif.base.models.expr.Type
import cern.plcverif.base.models.expr.BoolType
import cern.plcverif.base.models.expr.Typed
import cern.plcverif.base.models.expr.UnknownType
import cern.plcverif.base.models.expr.TemporalBoolType
import cern.plcverif.base.models.expr.IntType
import cern.plcverif.base.models.expr.FloatType
import cern.plcverif.base.models.expr.StringType
import cern.plcverif.base.models.expr.ElementaryType
import java.math.BigInteger

final class TypeUtils extends AbstractTypeUtils {
	private new() {
		// Singleton.
	}
	
	public static final TypeUtils INSTANCE = new TypeUtils();	
	
	static def boolean isBoolType(Type dataType) {
		return dataType instanceof BoolType;
	}
		
	static def boolean hasBoolType(Typed data) {
		return data !== null && data.type.isBoolType;
	}
	
	static def boolean isTemporalBoolType(Type dataType) {
		return dataType instanceof TemporalBoolType;
	}
	
	static def boolean isTemporalOrRegularBoolType(Type dataType) {
		return isBoolType(dataType) || isTemporalBoolType(dataType);
	}
	
	static def boolean hasTemporalOrRegularBoolType(Typed data) {
		return data !== null && data.type.isTemporalOrRegularBoolType;
	}
	
	static def boolean isUnknownType(Type dataType) {
		return dataType instanceof UnknownType;
	}
	
	static def boolean hasUnknownType(Typed data) {
		return data !== null && data.type.isUnknownType;
	}
	
	static def boolean isIntType(Type dataType) {
		return dataType instanceof IntType;
	}
		
	static def boolean hasIntType(Typed data) {
		return data !== null && data.type.isIntType;
	}
	
	static def boolean isFloatType(Type dataType) {
		return dataType instanceof FloatType;
	}
	
	static def boolean hasFloatType(Typed data) {
		return data !== null && data.type.isFloatType;
	}
	
	static def dispatch getBitLength(ElementaryType dataType) {
		throw new UnsupportedOperationException("Unknown elementary type: " + dataType);
	}
	
	static def dispatch getBitLength(BoolType dataType) {
		return 1;
	}
	
	static def dispatch getBitLength(IntType dataType) {
		return dataType.bits;
	}
	
	static def dispatch getBitLength(FloatType dataType) {
		return dataType.bits.value;
	}
	
	static def dispatch getBitLength(StringType dataType) {
		return dataType.maxLength * 8; // TODO document and decide if appropriate (to have 8-bit strings for CFA)
	}
	
	/**
	 * Returns the maximum representable value by the given integer type.
	 */
	static def long maxValue(IntType type) {
		if (type.signed) {
			// 2^(bitLength-1) - 1
			return BigInteger.valueOf(2).pow(type.bitLength - 1).subtract(BigInteger.ONE).longValue;
		} else {
			// 2^(bitLength) - 1
			return BigInteger.valueOf(2).pow(type.bitLength).subtract(BigInteger.ONE).longValue;
		}
	}
	
	/**
	 * Returns the minimum representable value by the given integer type.
	 */
	static def long minValue(IntType type) {
		if (type.signed) {
			// - 2^(bitLength-1)
			return BigInteger.valueOf(2).pow(type.bitLength - 1).multiply(BigInteger.valueOf(-1)).longValue;
		} else {
			return 0;
		}
	}
}
