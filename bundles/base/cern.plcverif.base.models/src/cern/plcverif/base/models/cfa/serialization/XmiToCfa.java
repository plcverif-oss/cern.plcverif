/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Vince Molnar - adjustment to updated models
 *******************************************************************************/
package cern.plcverif.base.models.cfa.serialization;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

import com.google.common.base.Preconditions;

import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.impl.CfadeclarationPackageImpl;

public final class XmiToCfa {
	private XmiToCfa() {
		// Utility class.
	}
	
	public static CfaNetworkDeclaration deserializeFromFile(URI sourceUri) throws IOException {
		Preconditions.checkNotNull(sourceUri);

		// Initialization, just in case
		CfadeclarationPackageImpl.init();

		// Create resource
		final XMIResourceImpl resource = new XMIResourceImpl(sourceUri);
		resource.getDefaultLoadOptions().put(XMLResource.OPTION_DEFER_IDREF_RESOLUTION, Boolean.TRUE);
		resource.setIntrinsicIDToEObjectMap(new HashMap<>());
		resource.load(null);

		return tryToExtractCfa(resource);
	}

	public static CfaNetworkDeclaration deserializeFromFile(File sourceFile) throws IOException {
		Preconditions.checkNotNull(sourceFile);

		URI sourceUri = URI.createURI(sourceFile.toURI().toString());
		return deserializeFromFile(sourceUri);
	}

	public static CfaNetworkDeclaration deserializeFromString(String serializedCfa) throws IOException {
		Preconditions.checkNotNull(serializedCfa);

		// Initialization, just in case
		CfadeclarationPackageImpl.init();

		// Create resource
		final XMIResource resource = new XMIResourceImpl(URI.createURI("resource.cfa"));

		// Create StringReader
		try (URIConverter.ReadableInputStream inStream = new URIConverter.ReadableInputStream(serializedCfa, "UTF-8")) {
			resource.load(inStream, null);
			return tryToExtractCfa(resource);
		}
	}

	private static CfaNetworkDeclaration tryToExtractCfa(XMIResource resource) throws IOException {
		EObject content = resource.getContents().get(0);
		if (content == null || !(content instanceof CfaNetworkDeclaration)) {
			// check validity
			throw new IOException("Unsuccessful loading.");
		}

		return (CfaNetworkDeclaration) content;
	}
}
