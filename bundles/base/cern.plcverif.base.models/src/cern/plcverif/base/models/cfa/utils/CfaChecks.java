/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.models.cfa.utils;

import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfabase.Transition;

public final class CfaChecks {
	private CfaChecks() {
		// Utility class.
	}
	
	public static void checkInSameAutomaton(Location loc1, Location loc2) {
		checkInSameAutomaton(loc1, loc2, String.format("Locations '%s' and '%s' are not in the same automaton.",
					loc1.getName(), loc2.getName()));
	}
	
	public static void checkInSameAutomaton(Location loc1, Location loc2, String message) {
		if (loc1.getParentAutomaton() != loc2.getParentAutomaton()) {
			throw new AssertionError(message);
		}
	}

	public static void checkInSameAutomaton(Transition t1, Transition t2) {
		checkInSameAutomaton(t1, t2, String.format("Transitions '%s' and '%s' are not in the same automaton.",
					t1.getName(), t2.getName()));
	}
	
	public static void checkInSameAutomaton(Transition t1, Transition t2, String message) {
		if (t1.getParentAutomaton() != t2.getParentAutomaton()) {
			throw new AssertionError(message);
		}
	}

	public static void checkEndsInSameAutomaton(Transition transition) {
		if (transition.getParentAutomaton() != transition.getSource().getParentAutomaton()) {
			throw new AssertionError(
					String.format("Transition '%s' and its source ('%s') are not in the same automaton.",
							transition.getName(), transition.getSource().getName()));
		}

		if (transition.getParentAutomaton() != transition.getTarget().getParentAutomaton()) {
			throw new AssertionError(
					String.format("Transition '%s' and its target ('%s') are not in the same automaton.",
							transition.getName(), transition.getTarget().getName()));
		}
	}
}
