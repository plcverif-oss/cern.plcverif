/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Xaver Fink - additions
 *****************************************************************************/
package cern.plcverif.base.models.cfa.textual

import cern.plcverif.base.common.formattedstring.BasicFormattedString
import cern.plcverif.base.common.formattedstring.Formats.BoldFormat
import cern.plcverif.base.common.formattedstring.Formats.ColorFormat
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef
import cern.plcverif.base.models.expr.BinaryArithmeticExpression
import cern.plcverif.base.models.expr.BinaryArithmeticOperator
import cern.plcverif.base.models.expr.BinaryLogicExpression
import cern.plcverif.base.models.expr.BinaryLogicOperator
import cern.plcverif.base.models.expr.BoolLiteral
import cern.plcverif.base.models.expr.ComparisonExpression
import cern.plcverif.base.models.expr.ComparisonOperator
import cern.plcverif.base.models.expr.Literal
import com.google.common.base.Preconditions
import java.util.Map
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.common.formattedstring.Formats.ItalicFormat
import cern.plcverif.base.models.expr.BinaryExpression
import static cern.plcverif.base.models.expr.BinaryArithmeticOperator.*
import cern.plcverif.base.models.expr.UnaryArithmeticExpression
import cern.plcverif.base.models.expr.UnaryLogicExpression
import cern.plcverif.base.models.expr.UnaryLogicOperator
import cern.plcverif.base.models.expr.utils.ExprUtils
import cern.plcverif.base.models.expr.utils.ExprEval
import cern.plcverif.base.models.expr.IntType
import cern.plcverif.base.models.cfa.textual.CfaToString
import cern.plcverif.base.models.expr.TypeConversion
import cern.plcverif.base.models.expr.TernaryArithmeticExpression
import cern.plcverif.base.models.expr.TernaryArithmeticOperator

/**
 * Class that produces a formatted string representing a given expression.
 * It colors the data references according to their Boolean values, using
 * the given valuations.
 */
class CfaExprToFormattedString {
	/**
	 * Fallback string producer.
	 */
	CfaToString baseString = CfaToString.INSTANCE;

	/**
	 * Valuations to be used for coloring. Never {@code null}.
	 */
	Map<DataRef, Literal> valuations;

	val GREEN = "#42A642";
	val RED = "#BF0303";

	/**
	 * Color to be used for satisfied data references (data references with
	 * true Boolean value according to the valuations).
	 */
	String satisfiedColor = GREEN;

	/**
	 * Color to be used for violated data references (data references with
	 * false Boolean value according to the valuations).
	 */
	String violatedColor = RED;
	
	ExprEval exprEvaluator; 

	new() {
		this(newHashMap());
	}

	new(Map<DataRef, Literal> valuations) {
		this.valuations = Preconditions.checkNotNull(valuations);
		this.exprEvaluator = new ExprEval(valuations);
	}

	/**
	 * Sets the color to be used for satisfied data references (data references with
	 * true Boolean value according to the valuations). It is expected to be a hexadecimal HTML color.
	 * Shall not be {@code null}.
	 */
	def setSatisfiedColor(String satisfiedColor) {
		Preconditions.checkArgument(ColorFormat.isValidColor(satisfiedColor),
			"The given color is not in the expected format.");
		this.satisfiedColor = satisfiedColor;
	}

	/**
	 * Sets the color to be used for violated data references (data references with
	 * false Boolean value according to the valuations). It is expected to be a hexadecimal HTML color.
	 * Shall not be {@code null}.
	 */
	def setViolatedColor(String violatedColor) {
		Preconditions.checkArgument(ColorFormat.isValidColor(violatedColor),
			"The given color is not in the expected format.");
		this.violatedColor = violatedColor;
	}

	/**
	 * Returns the formatted textual representation of the given expression.
	 */
	def dispatch BasicFormattedString toString(Expression obj) {
		return new BasicFormattedString("No specific method in CfaExprToFormattedString for Expression " +
			obj.toString(), ItalicFormat.INSTANCE);
	}

	def dispatch BasicFormattedString toString(UnaryArithmeticExpression obj) {
		val ret = new BasicFormattedString(baseString.opToString(obj.operator));
		ret.append(toString(obj.operand));
		return ret;
	}

	def dispatch BasicFormattedString toString(UnaryLogicExpression obj) {
		val ret = new BasicFormattedString();
		if (obj.operator == UnaryLogicOperator.NEG) {
			ret.append("NOT ");
		} else {
			ret.append(baseString.opToString(obj.operator));
		}
		ret.append(toString(obj.operand));
		return ret;
	}
	
	def dispatch BasicFormattedString toString(TypeConversion obj) {
		val ret = new BasicFormattedString();
		ret.append('''(«baseString.toString(obj.type)») ''');
		ret.append(toString(obj.operand));
		return ret;
	}

	def dispatch BasicFormattedString toString(BinaryExpression obj) {
		return new BasicFormattedString("No specific method in CfaExprToFormattedString for BinaryExpression " +
			obj.toString(), ItalicFormat.INSTANCE);
	}

	def dispatch BasicFormattedString toString(BinaryArithmeticExpression obj) {
		return binaryExprToString(obj, opToString(obj.operator));
	}
	
	def dispatch BasicFormattedString toString(TernaryArithmeticExpression obj) {
		return ternaryExprToString(obj);
	}
	
	def BasicFormattedString ternaryExprToString(TernaryArithmeticExpression obj){
		val ret = new BasicFormattedString();
		
		ret.append(toString(obj.left))
		ret.append(" ? ")
		ret.append(toString(obj.middle))
		ret.append(" : ")
		ret.append(toString(obj.right))
		
		return ret
	}

	def dispatch BasicFormattedString toString(BinaryLogicExpression obj) {
		return binaryExprToString(obj, opToString(obj.operator));
	}

	def dispatch BasicFormattedString toString(ComparisonExpression obj) {
		if (ExprUtils.isAtomic(obj.leftOperand) && ExprUtils.isAtomic(obj.rightOperand) && 
			(obj.leftOperand.type instanceof IntType && obj.rightOperand.type instanceof IntType) &&
			exprEvaluator.isComputableConstant(obj)
		) {
			// Comparison between integers
			
			// evaluate it -- it is possible as per isComputableConstant, and a comparison must be evaluated to a Boolean
			val result = exprEvaluator.toBoolean(obj);
			return new BasicFormattedString(baseString.toString(obj), new ColorFormat(if (result) satisfiedColor else violatedColor), BoldFormat.INSTANCE);
		}
			
		// fallback
		return binaryExprToString(obj, opToString(obj.operator));
	}

	def dispatch BasicFormattedString toString(DataRef ref) {
		val valuation = valuations.get(ref);
		if (valuation !== null && valuation instanceof BoolLiteral) {
			val boolValue = (valuation as BoolLiteral).value;
			if (boolValue == true) {
				return new BasicFormattedString(baseString.toString(ref), new ColorFormat(satisfiedColor), BoldFormat.INSTANCE);
			} else {
				return new BasicFormattedString(baseString.toString(ref), new ColorFormat(violatedColor), BoldFormat.INSTANCE);
			}
		} else {
			// we don't know the value
			// println('''No valuation for «CfaToString.toDiagString(ref)», known values: «FOR it : valuations.keySet»«CfaToString.toDiagString(it)»«ENDFOR». Type of valuations: «valuations.class.name»''')
			return new BasicFormattedString(baseString.toString(ref), BoldFormat.INSTANCE);
		}
	}

	def dispatch BasicFormattedString toString(Literal e) {
		return new BasicFormattedString(baseString.toString(e), BoldFormat.INSTANCE);
	}

	// ----
	private def BasicFormattedString binaryExprToString(BinaryExpression e, String operatorAsString) {
		val ret = new BasicFormattedString();

		// Try to avoid parentheses if they are not necessary to improve clarity
		val parensNeeded = e.eContainer !== null &&
			!(e.eContainer instanceof BinaryExpression && associativeOperators(e, e.eContainer as BinaryExpression));

		if (parensNeeded) {
			ret.append("(");
		}
		ret.append(toString(e.leftOperand));
		ret.append(operatorAsString);
		ret.append(toString(e.rightOperand));
		if (parensNeeded) {
			ret.append(")");
		}

		return ret;
	}

	/**
	 * If returns true, there is no need to put parentheses between these two expressions as they are associative.
	 */
	private def associativeOperators(BinaryExpression e1, BinaryExpression e2) {
		if (e1 instanceof BinaryLogicExpression && e2 instanceof BinaryLogicExpression) {
			return (e1 as BinaryLogicExpression).operator == (e2 as BinaryLogicExpression).operator &&
				(e1 as BinaryLogicExpression).operator != BinaryLogicOperator.IMPLIES;
		} else if (e1 instanceof BinaryArithmeticExpression && e2 instanceof BinaryArithmeticExpression) {
			if ((e1 as BinaryArithmeticExpression).operator == (e2 as BinaryArithmeticExpression).operator) {
				val op = (e1 as BinaryArithmeticExpression).operator;
				return op == PLUS || op == MINUS || op == MULTIPLICATION;
			}
		}
		return false;
	}

	private def String opToString(BinaryArithmeticOperator e) {
		return ''' «baseString.opToString(e)» ''';
	}

	private def String opToString(BinaryLogicOperator e) {
		switch (e) {
			case AND: return " AND "
			case OR: return " OR "
			case XOR: return " XOR "
			case IMPLIES: return " ==> "
		}
	}

	private def String opToString(ComparisonOperator e) {
		return baseString.opToString(e);
	}
}
