/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Vince Molnar - update to new metamodels
 *****************************************************************************/

package cern.plcverif.base.models.expr.string

import cern.plcverif.base.models.cfa.cfainstance.ArrayElementRef
import cern.plcverif.base.models.cfa.cfainstance.VariableRef
import org.eclipse.emf.ecore.EObject

final class ExprToString extends AbstractExprToString {
	private new() {
		// Singleton.
	}
	
	public static final ExprToString INSTANCE = new ExprToString();	
	
	static def dispatch String toDiagString(EObject obj) {
		return INSTANCE.toString(obj).toString();
	}
	
	static def dispatch String toDiagString(VariableRef varRef) {
		return varRef.toString();
	}
	
	static def dispatch String toDiagString(ArrayElementRef varRef) {
		return String.format("%s[%s]", 
			toDiagString(varRef.variable),
			varRef.indices.map([it | toDiagString(it)]).join(", ")			
		);
	}
}
