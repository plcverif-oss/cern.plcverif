/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.models.cfa.utils;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EquatableEObject;
import cern.plcverif.base.common.emf.IEquatableEObjectFactory;
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef;
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef;

/**
 * Wrapper class explicitly for {@link DataRef} objects. It relaxes the default,
 * {@link EcoreUtil#equals(Object)}-based equality check of the
 * {@link EquatableEObject} and uses
 * {@link CfaUtils#dataRefEquals(DataRef, DataRef)} instead.
 */
public class EquatableDataRef extends EquatableEObject {
	public static final IEquatableEObjectFactory FACTORY = (EObject e) -> new EquatableDataRef(e);

	public EquatableDataRef(EObject wrappedEObject) {
		super(wrappedEObject);
		Preconditions.checkNotNull(wrappedEObject, "wrappedEObject");
		if (!(wrappedEObject instanceof DataRef)) {
			throw new IllegalArgumentException(
					String.format("EquatableDataRef is supposed to be used for DataRef objects, instead it wraps: %s.",
							wrappedEObject.getClass().getName()));
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof EquatableDataRef) {
			EObject otherUnwrapped = ((EquatableDataRef) obj).getWrappedEObject();

			if (getWrappedEObject() instanceof DataRef && otherUnwrapped instanceof DataRef) {
				return dataRefEquals((DataRef) getWrappedEObject(), (DataRef) otherUnwrapped);
			}
		}

		return false;
	}

	@Override
	public int hashCode() {
		if (getWrappedEObject() instanceof DataRef) {
			DataRef thisRef = (DataRef) getWrappedEObject();
			return hashCode(thisRef);
		}

		return super.hashCode();
	}

	private static int hashCode(DataRef e) {
		if (e instanceof FieldRef) {
			return ((FieldRef) e).getField().getName().hashCode();
		} else if (e.getPrefix() != null) {
			return hashCode(e.getPrefix());
		} else {
			return 0;
		}
	}

	private static boolean dataRefEquals(EObject e1, EObject e2) {
		if (e1 instanceof DataRef && e2 instanceof DataRef) {
			return CfaUtils.dataRefEquals((DataRef) e1, (DataRef) e2);
		} else {
			return false;
		}
	}
}