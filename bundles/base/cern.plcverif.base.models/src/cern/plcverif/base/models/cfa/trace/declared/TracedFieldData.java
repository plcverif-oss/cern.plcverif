/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.cfa.trace.declared;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Preconditions;

import cern.plcverif.base.models.cfa.cfabase.ArrayDimension;
import cern.plcverif.base.models.cfa.cfadeclaration.Field;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.Type;

public final class TracedFieldData extends TracedData {
	private TracedComplexType parentTracedType;
	private Field field;

	public TracedFieldData(TracedComplexType parentType, Field field) {
		Preconditions.checkNotNull(parentType);
		Preconditions.checkNotNull(field);
		this.parentTracedType = parentType;
		this.field = field;
		parentType.addField(this);
	}

	@Override
	public TracedComplexType getParentTracedType() {
		return parentTracedType;
	}

	public Field getField() {
		return field;
	}

	@Override
	public List<String> getFqn() {
		List<String> ret;
		if (parentTracedType.getTypeOf() == null) {
			ret = new ArrayList<>();
		} else {
			ret = parentTracedType.getTypeOf().getFqn();
		}
		ret.add(field.getDisplayName());
		return ret;
	}

	@Override
	public List<String> getFqn(List<Expression> indices) {
		List<String> ret;
		if (parentTracedType.getTypeOf() == null) {
			ret = new ArrayList<>();
		} else {
			ret = parentTracedType.getTypeOf().getFqn(indices);
		}
		ret.add(field.getDisplayName());
		return ret;
	}

	@Override
	public List<ArrayDimension> getDimensions() {
		List<ArrayDimension> ret;
		if (parentTracedType.getTypeOf() == null) {
			ret = new ArrayList<>();
		} else {
			ret = parentTracedType.getTypeOf().getDimensions();
		}
		return ret;
	}

	@Override
	public boolean isArray() {
		if (parentTracedType.getTypeOf() == null) {
			return false;
		} else {
			return parentTracedType.getTypeOf().isArray();
		}
	}

	@Override
	public Type getDataType() {
		return field.getType();
	}
}
