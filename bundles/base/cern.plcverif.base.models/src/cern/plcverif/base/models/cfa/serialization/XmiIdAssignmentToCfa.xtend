/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.cfa.serialization

import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.xmi.XMIResource
import cern.plcverif.base.models.cfa.cfabase.NamedElement
import cern.plcverif.base.models.cfa.LocalIdGenerator

final class XmiIdAssignmentToCfa {
	int counter = 0	
	LocalIdGenerator idGenerator = new LocalIdGenerator();
	
	def dispatch void assignIds(EObject root, XMIResource resource) {
		resource.setID(root, "id" + counter++);
		for (child : root.eContents) {
			assignIds(child, resource);
		}
	}
	
	def dispatch void assignIds(NamedElement elem, XMIResource resource) {
		var id = elem.name;
		var current = elem.eContainer;
		while (current instanceof NamedElement) {
			id = current.name + "_" + id;
			current = current.eContainer;
		}
		
		resource.setID(elem, idGenerator.generateId(id));
		for (child : elem.eContents) {
			assignIds(child, resource);
		}
	}
}
