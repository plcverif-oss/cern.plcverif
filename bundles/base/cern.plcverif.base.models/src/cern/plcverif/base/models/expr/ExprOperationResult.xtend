/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Vince Molnar - update to new metamodels
 *****************************************************************************/
package cern.plcverif.base.models.expr

import cern.plcverif.base.common.utils.XtendHelper
import com.google.common.base.Preconditions
import java.math.BigInteger

final class ExprOperationResult {
	private new() {
		// Utility class.
	}
	
	// BinaryLogicalOperator
	def static boolean operationResult(boolean leftValue, boolean rightValue, BinaryLogicOperator operator) {
		switch (operator) {
			case AND: return leftValue && rightValue
			case OR: return leftValue || rightValue
			case XOR: return !(leftValue == rightValue)
			case IMPLIES: return !leftValue || rightValue
			default: throw new UnsupportedOperationException("Unsupported operator: " + operator)
		}
	}

	// BinaryArithmeticOperator
	def static long operationResult(long leftValue, long rightValue, BinaryArithmeticOperator operator) {
		switch (operator) {
			case PLUS: return leftValue + rightValue
			case MINUS: return leftValue - rightValue
			case MULTIPLICATION: return leftValue * rightValue
			case DIVISION: return leftValue / rightValue
			case MODULO: return leftValue % rightValue
			case INTEGER_DIVISION: return leftValue / rightValue
			case POWER: return Math.pow(leftValue, rightValue) as long
			case BITSHIFT_LEFT: return leftValue << (rightValue as int)
			case BITSHIFT_RIGHT: return leftValue >> (rightValue as int)
//			case BITROTATE: 
			case BITWISE_OR: return XtendHelper.bitwiseOr(leftValue, rightValue)
			case BITWISE_AND: return XtendHelper.bitwiseAnd(leftValue, rightValue)
			case BITWISE_XOR: return XtendHelper.bitwiseXor(leftValue, rightValue)
			default: throw new UnsupportedOperationException("Unsupported operator: " + operator)
		}
	}

	def static double operationResult(double leftValue, double rightValue, BinaryArithmeticOperator operator) {
		switch (operator) {
			case PLUS: return leftValue + rightValue
			case MINUS: return leftValue - rightValue
			case MULTIPLICATION: return leftValue * rightValue
			case DIVISION: return leftValue / rightValue
			case MODULO: return leftValue % rightValue
			case INTEGER_DIVISION: return leftValue / rightValue
			case POWER: return Math.pow(leftValue, rightValue)
			default: throw new UnsupportedOperationException("Unsupported operator: " + operator)
		}
	}

	// UnaryArithmeticOperator
	def static long operationResult(long operand, UnaryArithmeticOperator operator) {
		switch (operator) {
			case MINUS: return -1 * operand
			case BITWISE_NOT: throw new UnsupportedOperationException("BITWISE_NOT shall be handled elsewhere.")
			default: throw new UnsupportedOperationException("Unsupported operator: " + operator)
		}
	}

	def static double operationResult(double operand, UnaryArithmeticOperator operator) {
		switch (operator) {
			case MINUS: return -1 * operand
			default: throw new UnsupportedOperationException("Unsupported operator: " + operator)
		}
	}
	
	// UnaryLogicalOperator
	def static boolean operationResult(boolean operand, UnaryLogicOperator operator) {
		switch (operator) {
			case NEG: return !operand
			default: throw new UnsupportedOperationException("Unsupported operator: " + operator)
		}
	}
	
	// ComparisonOperator
	def static boolean operationResult(long leftValue, long rightValue, ComparisonOperator operator) {
		switch (operator) {
			case EQUALS: return leftValue == rightValue
			case NOT_EQUALS: return leftValue != rightValue
			case LESS_THAN: return leftValue < rightValue
			case GREATER_THAN: return leftValue > rightValue
			case LESS_EQ: return leftValue <= rightValue
			case GREATER_EQ: return leftValue >= rightValue
			default: throw new UnsupportedOperationException("Unsupported operator: " + operator)
		}
	}
	
	def static boolean operationResult(double leftValue, double rightValue, ComparisonOperator operator) {
		switch (operator) {
			case EQUALS: return leftValue == rightValue
			case NOT_EQUALS: return leftValue != rightValue
			case LESS_THAN: return leftValue < rightValue
			case GREATER_THAN: return leftValue > rightValue
			case LESS_EQ: return leftValue <= rightValue
			case GREATER_EQ: return leftValue >= rightValue
			default: throw new UnsupportedOperationException("Unsupported operator: " + operator)
		}
	}
	
	def static boolean operationResult(boolean leftValue, boolean rightValue, ComparisonOperator operator) {
		switch (operator) {
			case EQUALS: return leftValue == rightValue
			case NOT_EQUALS: return leftValue != rightValue
			default: throw new UnsupportedOperationException("Unsupported operator: " + operator)
		}
	}
	
	def static long bwNotResult(IntLiteral operand) {
		Preconditions.checkNotNull(operand);
		Preconditions.checkArgument(operand.type instanceof IntType, "IntLiteral must have IntType as type.");
		
		val bitsize = (operand.type as IntType).bits;
		Preconditions.checkState(bitsize <= 64, "IntLiterals with >64 bits are not supported.");
		
		// mask = 2^(bitsize)-1
		// Example: 2^8 - 1 = 255 = 0b1111_1111
		val mask = BigInteger.valueOf(2).pow(bitsize).subtract(BigInteger.ONE);
		Preconditions.checkState(bitsize == 64 || mask.longValue > 0, "No overflow/underflow is supposed to happen.");
		// There is an underflow with bitsize=64, but it is desired to have only 1's in the mask.
		return XtendHelper.bitwiseXor(operand.value, mask.longValue);
	}
}
