/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.models;

/**
 * Before upgrading to the newest Xtext we are tied to Guava 15. This class is
 * used to mimic some of the functionality that is present in Guava 21+.
 */
public final class Guava21Workaround {
	private Guava21Workaround() {
		// Not intended to be instantiated.
	}
	
	public static void checkArgument(boolean condition, String messageFormat, Object... args) {
		if (!condition) {
			throw new IllegalArgumentException(String.format(messageFormat, args));
		}
	}
}
