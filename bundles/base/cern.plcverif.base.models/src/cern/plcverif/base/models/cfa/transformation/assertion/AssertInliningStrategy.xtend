/******************************************************************************
 * (C) Copyright CERN 2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.cfa.transformation.assertion

import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory
import cern.plcverif.base.models.cfa.cfadeclaration.AssignmentTransition
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructure
import cern.plcverif.base.models.cfa.cfadeclaration.Field
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef
import cern.plcverif.base.models.expr.BoolType
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.IntType
import cern.plcverif.base.models.expr.Literal
import cern.plcverif.base.models.expr.Type

import static cern.plcverif.base.models.cfa.transformation.assertion.AssertInliningResult.NO_ASSERTION_HAS_BEEN_VIOLATED
import static org.eclipse.emf.ecore.util.EcoreUtil.copy
import com.google.common.base.Preconditions

/**
 * Class to define an assertion inlining strategy. Assertion inlining 
 * strategies define how should the error fields represent the violation 
 * or non-violation of CFA assertions. 
 */
abstract class AssertInliningStrategy {
	static final CfaDeclarationSafeFactory FACTORY = CfaDeclarationSafeFactory.INSTANCE;
	static final String ERROR_FIELD_NAME = "__assertion_error";
	
	/**
	 * Assertion inlining strategy that represents the error field as integer.
	 * The value of the integer will determine the ID of the violated assertion.
	 */
	public static final AssertInliningStrategy INT_STRATEGY = new IntAssertInliningStrategy();
	
	/**
	 * Assertion inlining strategy that represents the error field as Boolean.
	 * The ID of the violated assertion cannot be determined, only the fact that one of the
	 * assertions has been violated.
	 */
	public static final AssertInliningStrategy BOOL_STRATEGY = new BoolAssertInliningStrategy();

	private new() {
		// Sealed.
	}

	/**
	 * Returns the name of the error field.
	 */
	protected abstract def String getErrorFieldName();

	/**
	 * Returns the type of the error field.
	 */
	protected abstract def Type getErrorFieldType();
	
	/**
	 * Creates an assignment representing that the assertion with the given ID has been violated.
	 * The assignment will be attached to the given transition.
	 * @param transition Transition whose firing represents that the assertion with the given ID has been violated
	 * @param errorFieldRef Reference to the error field. The reference will not be modified or directly used, it will be copied.
	 * @param violatedAssertionId ID of the assertion that has been violated.
	 */
	abstract def void createViolatedAssignment(AssignmentTransition transition, FieldRef errorFieldRef,
		long violatedAssertionId);

	/**
	 * Returns the literal associated to the "none of the assertions violated" value.
	 */
	abstract def Literal createNoAssertionViolatedValue();

	/**
	 * Creates a new error field in the given data structure. The field's initial
	 * assignment will be set. The field will be annotated with internal generated
	 * field annotation.
	 * @param parentDataStructure Parent data structure. Should not be {@code null}.
	 * @return The created error field. Never {@code null}.
	 */
	def Field createErrorField(DataStructure parentDataStructure) {
		Preconditions.checkNotNull(parentDataStructure, "parentDataStructure");
		
		val errorField = FACTORY.createField(getErrorFieldName(), parentDataStructure, getErrorFieldType());
		FACTORY.createInitialAssignment(errorField, createNoAssertionViolatedValue());
		FACTORY.createInternalGeneratedFieldAnnotation(errorField);

		return errorField;
	}

	/**
	 * Creates a new Boolean expression. The expression is true iff the error 
	 * field's current value represents that none of the assertions has been 
	 * violated.
	 * @param errorFieldRef Reference to the error field. The reference will not be modified or directly used, it will be copied.
	 * @return Expression
	 */
	def Expression createNoAssertionViolatedExpr(FieldRef errorFieldRef) {
		return FACTORY.eq(copy(errorFieldRef), createNoAssertionViolatedValue());
	}

	/**
	 * Creates a new Boolean expression. The expression is true iff the error 
	 * field's current value represents that some of the assertions have been 
	 * violated.
	 * @param errorFieldRef Reference to the error field. The reference will not be modified or directly used, it will be copied.
	 * @return Expression
	 */
	def Expression createSomeAssertionViolatedExpr(FieldRef errorFieldRef) {
		return FACTORY.neq(copy(errorFieldRef), createNoAssertionViolatedValue());
	}


	// STRATEGIES
	/**
	 * Assertion inlining strategy that represents the error field as integer.
	 * The value of the integer will determine the ID of the violated assertion.
	 */
	private static final class IntAssertInliningStrategy extends AssertInliningStrategy {
		override getErrorFieldName() {
			return ERROR_FIELD_NAME;
		}

		override IntType getErrorFieldType() {
			return FACTORY.createIntType(false, 16);
		}

		override createViolatedAssignment(AssignmentTransition transition, FieldRef errorFieldRef, long violatedAssertionId) {
			Preconditions.checkArgument(violatedAssertionId != NO_ASSERTION_HAS_BEEN_VIOLATED, "The given assertion ID is reserved for representing that no assertion has been violated.");
			
			val assertionValue = FACTORY.createIntLiteral(violatedAssertionId, getErrorFieldType());
			FACTORY.createAssignment(transition, copy(errorFieldRef), assertionValue);
		}

		override createNoAssertionViolatedValue() {
			return FACTORY.createIntLiteral(NO_ASSERTION_HAS_BEEN_VIOLATED, getErrorFieldType());
		}
	}

	/**
	 * Assertion inlining strategy that represents the error field as Boolean.
	 * The ID of the violated assertion cannot be determined, only the fact that one of the
	 * assertions has been violated.
	 */
	private static final class BoolAssertInliningStrategy extends AssertInliningStrategy {
		override getErrorFieldName() {
			return ERROR_FIELD_NAME;
		}

		override BoolType getErrorFieldType() {
			return FACTORY.createBoolType();
		}

		override createViolatedAssignment(AssignmentTransition transition, FieldRef errorFieldRef, long violatedAssertionId) {
			// error=TRUE means violation
			FACTORY.createAssignment(transition, copy(errorFieldRef), FACTORY.trueLiteral);
		}

		override createNoAssertionViolatedValue() {
			return FACTORY.falseLiteral;
		}
	}
}
