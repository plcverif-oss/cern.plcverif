/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Vince Molnar - update to new metamodels
 *******************************************************************************/
package cern.plcverif.base.models.cfa.visualization

import cern.plcverif.base.models.cfa.cfabase.Annotation
import cern.plcverif.base.models.cfa.cfabase.AssertionAnnotation
import cern.plcverif.base.models.cfa.cfabase.CaseLocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.ContinueTransitionAnnotation
import cern.plcverif.base.models.cfa.cfabase.ExitTransitionAnnotation
import cern.plcverif.base.models.cfa.cfabase.GotoTransitionAnnotation
import cern.plcverif.base.models.cfa.cfabase.IfLocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.LabelledLocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.LocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.RepeatLoopLocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.SwitchLocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.TransitionAnnotation
import cern.plcverif.base.models.cfa.cfabase.WhileLoopLocationAnnotation
import cern.plcverif.base.models.cfa.textual.CfaToString
import cern.plcverif.base.models.expr.string.AbstractExprToString
import cern.plcverif.base.models.cfa.cfadeclaration.DirectionFieldAnnotation
import cern.plcverif.base.models.cfa.cfadeclaration.OriginalDataTypeFieldAnnotation
import cern.plcverif.base.models.cfa.cfabase.LineNumberAnnotation
import cern.plcverif.base.models.cfa.cfadeclaration.InternalGeneratedFieldAnnotation
import cern.plcverif.base.models.cfa.cfainstance.InternalGeneratedVariableAnnotation
import cern.plcverif.base.models.cfa.cfabase.BlockAutomatonAnnotation
import cern.plcverif.base.models.cfa.cfabase.BlockReturnTransitionAnnotation
import cern.plcverif.base.models.cfa.cfadeclaration.ForLoopLocationAnnotation

final class AnnotationToDisplayString {
	static final AbstractExprToString EXPR_TO_STRING = CfaToString.INSTANCE;
	
	private new() {
		// Utility class.
	}
	
	def static dispatch CharSequence toDisplayString(Annotation ann) {
		return ann.toString().replaceFirst("cern\\..*\\.impl\\.", "");
	}
	
	def static dispatch CharSequence toDisplayString(LocationAnnotation ann) {
		return ann.toString().replaceFirst("cern\\..*\\.impl\\.", "");
	}
	
	def static dispatch CharSequence toDisplayString(IfLocationAnnotation ann) {
		return '''IF {then=«ann.then.name», endsAt=«ann.endsAt.name»}''';
	}
	
	def static dispatch CharSequence toDisplayString(WhileLoopLocationAnnotation ann) {
		return '''WHILE {loopBodyEntry=«ann.loopBodyEntry.name», loopExit=«ann.loopExit.name», loopNextCycle=«ann.loopNextCycle.name»}''';
	}
	
	def static dispatch CharSequence toDisplayString(RepeatLoopLocationAnnotation ann) {
		return '''REPEAT {loopBodyEntry=«ann.loopBodyEntry.name», loopExit=«ann.loopExit.name», loopNextCycle=«ann.loopNextCycle.name»}''';
	}
	
	def static dispatch CharSequence toDisplayString(ForLoopLocationAnnotation ann) {
		return '''FOR {loopBodyEntry=«ann.loopBodyEntry.name», loopExit=«ann.loopExit.name», loopNextCycle=«ann.loopNextCycle.name», loopIncrement=« ann.loopVariableIncrement.name »  loopVariableInit=«ann.loopVariableInit.name», loopVariable=«ann.loopVariable»}''';
	}
	
	def static dispatch CharSequence toDisplayString(SwitchLocationAnnotation ann) {
		return '''SWITCH {endsAt=«ann.endsAt.name», cases=«ann.cases.map[it | it.parentLocation.name].join(",")», elseCase=«ann.elseCase?.parentLocation?.name»}''';
	}
	
	def static dispatch CharSequence toDisplayString(CaseLocationAnnotation ann) {
		return '''CASE {switchAnnotation=«ann.switchAnnotation.parentLocation.name»}''';
	}
	
	def static dispatch CharSequence toDisplayString(LabelledLocationAnnotation ann) {
		return '''LABEL {name=«ann.labelName»}''';
	}

	def static dispatch CharSequence toDisplayString(TransitionAnnotation ann) {
		return ann.toString();
	}
	
	def static dispatch CharSequence toDisplayString(GotoTransitionAnnotation ann) {
		return '''GOTO {targetLabel=«ann.targetLabel»}''';
	}
	
	def static dispatch CharSequence toDisplayString(ExitTransitionAnnotation ann) {
		return '''EXIT {}''';
	}
	
	def static dispatch CharSequence toDisplayString(ContinueTransitionAnnotation ann) {
		return '''CONTINUE {}''';
	}
	
	def static dispatch CharSequence toDisplayString(BlockReturnTransitionAnnotation ann) {
		return '''RETURN {}''';
	}
	
	def static dispatch CharSequence toDisplayString(AssertionAnnotation ann) {
		return '''ASSERT {invariant=«AnnotationToDisplayString.EXPR_TO_STRING.toString(ann.invariant)»}''';
	}
	
	def static dispatch CharSequence toDisplayString(DirectionFieldAnnotation ann) {
		return '''direction=«ann.direction.toString»''';
	}
	
	def static dispatch CharSequence toDisplayString(OriginalDataTypeFieldAnnotation ann) {
		return '''PLC type=«ann.plcDataType»''';
	}
	
	def static dispatch CharSequence toDisplayString(LineNumberAnnotation ann) {
		return '''Line «ann.lineNumber» in «ann.fileName»''';
	}
	
	def static dispatch CharSequence toDisplayString(BlockAutomatonAnnotation ann) {
		return '''Block type: «ann.blockType» {name=«ann.name», stateful=«ann.stateful»}'''
	}
	
	def static dispatch CharSequence toDisplayString(InternalGeneratedFieldAnnotation ann) {
		return "generated"
	}
	
	def static dispatch CharSequence toDisplayString(InternalGeneratedVariableAnnotation ann) {
		return "generated"
	}
}
