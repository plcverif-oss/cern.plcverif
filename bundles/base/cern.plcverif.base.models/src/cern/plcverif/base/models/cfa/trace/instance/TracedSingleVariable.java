/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.cfa.trace.instance;

import java.util.List;

import cern.plcverif.base.models.cfa.cfainstance.Variable;
import cern.plcverif.base.models.cfa.trace.declared.TracedData;

public class TracedSingleVariable extends TracedVariable {
	private Variable representedVariable;

	public TracedSingleVariable(TracedData flattenedFrom) {
		super(flattenedFrom);
	}

	@Override
	public Variable getRepresentedVariable() {
		return representedVariable;
	}

	public void setRepresentedVariable(Variable representedVariable) {
		assert this.representedVariable == null : "Represented variable should not be set more than once.";
		this.representedVariable = representedVariable;
	}

	@Override
	public List<String> getFqn() {
		return getFlattenedFrom().getFqn();
	}
}