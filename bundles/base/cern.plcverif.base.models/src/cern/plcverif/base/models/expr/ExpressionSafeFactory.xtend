/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *   Xaver Fink - additions
 *****************************************************************************/
package cern.plcverif.base.models.expr

import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.base.models.cfa.utils.CfaTypeUtils
import cern.plcverif.base.models.expr.string.ExprToString
import com.google.common.base.Preconditions
import java.util.Arrays
import java.util.List
import java.util.stream.Collectors
import java.util.stream.Stream
import org.eclipse.emf.ecore.util.EcoreUtil

import static cern.plcverif.base.models.expr.utils.ExprUtils.*
import cern.plcverif.base.models.Guava21Workaround

class ExpressionSafeFactory {
	public static val boolean DEFAULT_STRICT = true;
	public static val INSTANCE = new ExpressionSafeFactory(DEFAULT_STRICT);
	public static val INSTANCE_NON_STRICT = new ExpressionSafeFactory(false);
	
	/**
	 * Defines the strictness of the checks in the factory methods.
	 * If it is {@code true}, all checks are enabled. If it is {@code false}, the expensive checks (e.g. type checking) are disabled.
	 */
	boolean strictChecks;
	
	protected def boolean isStrict() {
		return this.strictChecks;
	}
	
	protected new(boolean strict) {
		this.strictChecks = strict;
	}
	
	def UnknownType createUnknownType() {
		return ExprFactory.eINSTANCE.createUnknownType();
	}
	
	def UninterpretedSymbol createUninterpretedSymbol(String symbol) {
		Preconditions.checkNotNull(symbol, "symbol");
		
		val ret = ExprFactory.eINSTANCE.createUninterpretedSymbol();
		ret.symbol = symbol;
		ret.type = createUnknownType();
		return ret;
	}
	
	def Nondeterministic createNondeterministic(Type type) {
		Preconditions.checkNotNull("type");
		EmfHelper.checkNotContained(type, "The provided type is already assigned to another model element. Copy it with EcoreUtil.copy().");
		
		val ret = ExprFactory.eINSTANCE.createNondeterministic();
		ret.type = type;
		return ret;
	}
	
	def TypeConversion createTypeConversion(Expression operand, Type type) {
		Preconditions.checkNotNull(operand, "operand");
		Preconditions.checkNotNull(type, "type");
		EmfHelper.checkNotContained(operand, "The provided operand is already assigned to another model element. Copy it with EcoreUtil.copy().");
		EmfHelper.checkNotContained(type, "The provided type is already assigned to another model element. Copy it with EcoreUtil.copy().");
		
		val ret = ExprFactory.eINSTANCE.createTypeConversion();
		ret.operand = operand;
		ret.type = type;
		return ret;
	}
	
	/**
	 * Returns the given {@code expr} converted to {@code targetType}. If the
	 * {@code expr} already has a type {@code targetType}, the original
	 * {@code expr} is returned. This method <b>does not check</b> whether the
	 * conversion of {@code expr} to the type {@code targetType} is valid or
	 * not.
	 * 
	 * @param expr
	 *            The expression to be converted (if needed).
	 * @param targetType
	 *            The target type. If needed for the newly created type conversion,
	 *            this type will be copied using {@link EcoreUtil#copy(EObject)}.
	 * @return The given expression, converted to the target type.
	 */
	def Expression createTypeConversionIfNeeded(Expression expr, Type targetType) {
		if (expr.getType().dataTypeEquals(targetType)) {
			return expr;
		} else {
			return createTypeConversion(expr, EcoreUtil.copy(targetType));
		}
	}

	// Arithmetic expressions
	
	def UnaryArithmeticExpression createUnaryArithmeticExpression(Expression operand, UnaryArithmeticOperator operator) {
		Preconditions.checkNotNull(operand, "operand");
		Preconditions.checkNotNull(operator, "operator");
		EmfHelper.checkNotContained(operand, "The provided operand is already assigned to another model element. Copy it with EcoreUtil.copy().");
		
		val ret = ExprFactory.eINSTANCE.createUnaryArithmeticExpression();
		ret.operand = operand;
		ret.operator = operator;
		return ret;
	}
	
	def TernaryArithmeticExpression createTernaryArithmeticExpression(Expression lhs, Expression mhs, Expression rhs, TernaryArithmeticOperator operator) {
		Preconditions.checkNotNull(lhs, "lhs");
		Preconditions.checkNotNull(mhs, "lhs");
		Preconditions.checkNotNull(rhs, "rhs");
		Preconditions.checkNotNull(operator, "operator");
		EmfHelper.checkNotContained(lhs, "The provided left operand is already assigned to another model element. Copy it with EcoreUtil.copy().");
		EmfHelper.checkNotContained(mhs, "The provided left operand is already assigned to another model element. Copy it with EcoreUtil.copy().");
		EmfHelper.checkNotContained(rhs, "The provided right operand is already assigned to another model element. Copy it with EcoreUtil.copy().");
		if (strictChecks) {
			CfaTypeUtils.checkArgumentsHaveSameType(mhs, rhs, "Unable to create a(n) %s expression.", operator.literal);
		}
		
		val ret = ExprFactory.eINSTANCE.createTernaryArithmeticExpression();
		ret.left = lhs;
		ret.middle = mhs;
		ret.right = rhs;
		ret.operator = operator;
		return ret;
	}
	
	def ternary(Expression cond, Expression ifTrue, Expression ifFalse) {
		return createTernaryArithmeticExpression(exprCopy(cond), exprCopy(ifTrue), exprCopy(ifFalse), TernaryArithmeticOperator.TERNARY);
	}
	
	def BinaryArithmeticExpression createBinaryArithmeticExpression(Expression lhs, Expression rhs, BinaryArithmeticOperator operator) {
		Preconditions.checkNotNull(lhs, "lhs");
		Preconditions.checkNotNull(rhs, "rhs");
		Preconditions.checkNotNull(operator, "operator");
		EmfHelper.checkNotContained(lhs, "The provided left operand is already assigned to another model element. Copy it with EcoreUtil.copy().");
		EmfHelper.checkNotContained(rhs, "The provided right operand is already assigned to another model element. Copy it with EcoreUtil.copy().");
		if (strictChecks) {
			CfaTypeUtils.checkArgumentsHaveSameType(lhs, rhs, "Unable to create a(n) %s expression.", operator.literal);
		}
		
		val ret = ExprFactory.eINSTANCE.createBinaryArithmeticExpression();
		ret.leftOperand = lhs;
		ret.rightOperand = rhs;
		ret.operator = operator;
		return ret;
	}
	
	def minus(Expression op) {
		return createUnaryArithmeticExpression(exprCopy(op), UnaryArithmeticOperator.MINUS);
	}
	
	def bwNot(Expression op) {
		return createUnaryArithmeticExpression(exprCopy(op), UnaryArithmeticOperator.BITWISE_NOT);
	}
	
	def plus(Expression lhs, Expression rhs) {
		return createBinaryArithmeticExpression(exprCopy(lhs), exprCopy(rhs), BinaryArithmeticOperator.PLUS);
	}
	
	def minus(Expression lhs, Expression rhs) {
		return createBinaryArithmeticExpression(exprCopy(lhs), exprCopy(rhs), BinaryArithmeticOperator.MINUS);
	}
	
	def multiply(Expression lhs, Expression rhs) {
		return createBinaryArithmeticExpression(exprCopy(lhs), exprCopy(rhs), BinaryArithmeticOperator.MULTIPLICATION);
	}
	
	def divide(Expression lhs, Expression rhs) {
		return createBinaryArithmeticExpression(exprCopy(lhs), exprCopy(rhs), BinaryArithmeticOperator.DIVISION);
	}
	
	def mod(Expression lhs, Expression rhs) {
		return createBinaryArithmeticExpression(exprCopy(lhs), exprCopy(rhs), BinaryArithmeticOperator.MODULO);
	}
	
	def iDivide(Expression lhs, Expression rhs) {
		return createBinaryArithmeticExpression(exprCopy(lhs), exprCopy(rhs), BinaryArithmeticOperator.INTEGER_DIVISION);
	}
	
	def pow(Expression lhs, Expression rhs) {
		return createBinaryArithmeticExpression(exprCopy(lhs), exprCopy(rhs), BinaryArithmeticOperator.POWER);
	}
	
	def bwShiftLeft(Expression lhs, Expression rhs) {
		return createBinaryArithmeticExpression(exprCopy(lhs), exprCopy(rhs), BinaryArithmeticOperator.BITSHIFT_LEFT);
	}
	
	def bwShiftRight(Expression lhs, Expression rhs) {
		return createBinaryArithmeticExpression(exprCopy(lhs), exprCopy(rhs), BinaryArithmeticOperator.BITSHIFT_RIGHT);
	}
	
	def bwRot(Expression lhs, Expression rhs) {
		return createBinaryArithmeticExpression(exprCopy(lhs), exprCopy(rhs), BinaryArithmeticOperator.BITROTATE);
	}
	
	def bwOr(Expression lhs, Expression rhs) {
		return createBinaryArithmeticExpression(exprCopy(lhs), exprCopy(rhs), BinaryArithmeticOperator.BITWISE_OR);
	}
	
	def bwAnd(Expression lhs, Expression rhs) {
		return createBinaryArithmeticExpression(exprCopy(lhs), exprCopy(rhs), BinaryArithmeticOperator.BITWISE_AND);
	}
	
	def bwXor(Expression lhs, Expression rhs) {
		return createBinaryArithmeticExpression(exprCopy(lhs), exprCopy(rhs), BinaryArithmeticOperator.BITWISE_XOR);
	}
	
	
	
	// Comparison expressions
		
	def ComparisonExpression createComparisonExpression(Expression lhs, Expression rhs, ComparisonOperator operator) {
		Preconditions.checkNotNull(lhs, "lhs");
		Preconditions.checkNotNull(rhs, "rhs");
		Preconditions.checkNotNull(operator, "operator");
		EmfHelper.checkNotContained(lhs, "The provided left operand is already assigned to another model element. Copy it with EcoreUtil.copy().");
		EmfHelper.checkNotContained(rhs, "The provided right operand is already assigned to another model element. Copy it with EcoreUtil.copy().");
		if (strictChecks) {
			CfaTypeUtils.checkArgumentsHaveSameType(lhs, rhs, "Unable to create a(n) %s expression.", operator.literal);
		}
		
		val ret = ExprFactory.eINSTANCE.createComparisonExpression();
		ret.leftOperand = lhs;
		ret.rightOperand = rhs;
		ret.operator = operator;
		ret.type = createBoolType();
		return ret;
	}
	
	def eq(Expression lhs, Expression rhs) {
		return createComparisonExpression(exprCopy(lhs), exprCopy(rhs), ComparisonOperator.EQUALS);
	}
	
	def neq(Expression lhs, Expression rhs) {
		return createComparisonExpression(exprCopy(lhs), exprCopy(rhs), ComparisonOperator.NOT_EQUALS);
	}
	
	def lt(Expression lhs, Expression rhs) {
		return createComparisonExpression(exprCopy(lhs), exprCopy(rhs), ComparisonOperator.LESS_THAN);
	}
	
	def gt(Expression lhs, Expression rhs) {
		return createComparisonExpression(exprCopy(lhs), exprCopy(rhs), ComparisonOperator.GREATER_THAN);
	}
	
	def leq(Expression lhs, Expression rhs) {
		return createComparisonExpression(exprCopy(lhs), exprCopy(rhs), ComparisonOperator.LESS_EQ);
	}
	
	def geq(Expression lhs, Expression rhs) {
		return createComparisonExpression(exprCopy(lhs), exprCopy(rhs), ComparisonOperator.GREATER_EQ);
	}
	
	// Logic expressions
		
	def UnaryLogicExpression createUnaryLogicExpression(Expression operand, UnaryLogicOperator operator) {
		Preconditions.checkNotNull(operand, "operand");
		Preconditions.checkNotNull(operator, "operator");
		EmfHelper.checkNotContained(operand, "The provided operand is already assigned to another model element. Copy it with EcoreUtil.copy().");
		
		val ret = ExprFactory.eINSTANCE.createUnaryLogicExpression();
		ret.operand = operand;
		ret.operator = operator;
		return ret;
	}
	
	def BinaryLogicExpression createBinaryLogicExpression(Expression lhs, Expression rhs, BinaryLogicOperator operator) {
		Preconditions.checkNotNull(lhs, "lhs");
		Preconditions.checkNotNull(rhs, "rhs");
		Preconditions.checkNotNull(operator, "operator");
		EmfHelper.checkNotContained(lhs, "The provided left operand is already assigned to another model element. Copy it with EcoreUtil.copy().");
		EmfHelper.checkNotContained(rhs, "The provided right operand is already assigned to another model element. Copy it with EcoreUtil.copy().");
		if (strictChecks) {
			CfaTypeUtils.checkArgumentsHaveSameType(lhs, rhs, "Unable to create a(n) %s expression.", operator.literal);
		}
		
		val ret = ExprFactory.eINSTANCE.createBinaryLogicExpression();
		ret.leftOperand = lhs;
		ret.rightOperand = rhs;
		ret.operator = operator;
		return ret;
	}
	
	def neg(Expression op) {
		return createUnaryLogicExpression(exprCopy(op), UnaryLogicOperator.NEG);
	}
	
	def and(Expression lhs, Expression rhs) {
		return createBinaryLogicExpression(exprCopy(lhs), exprCopy(rhs), BinaryLogicOperator.AND);
	}
	
	def or(Expression lhs, Expression rhs) {
		return createBinaryLogicExpression(exprCopy(lhs), exprCopy(rhs), BinaryLogicOperator.OR);
	}
	
	def xor(Expression lhs, Expression rhs) {
		return createBinaryLogicExpression(exprCopy(lhs), exprCopy(rhs), BinaryLogicOperator.XOR);
	}
	
	def impl(Expression lhs, Expression rhs) {
		return createBinaryLogicExpression(exprCopy(lhs), exprCopy(rhs), BinaryLogicOperator.IMPLIES);
	}
	
	private def Expression naryLogicOp(BinaryLogicOperator operator, Expression... arguments) {
		Preconditions.checkNotNull(arguments)
		Preconditions.checkNotNull(operator)
		Preconditions.checkArgument(arguments.length >= 1, "At least one argument is required for a logic operation.")

		if (arguments.length == 1) {
			return exprCopy(arguments.get(0));
		} else if (arguments.length == 2) {
			return createBinaryLogicExpression(
				exprCopy(arguments.get(0)), 
				exprCopy(arguments.get(1)),
				operator
			);
		} else {
			// arguments.length >= 3
			return createBinaryLogicExpression(exprCopy(arguments.get(0)),
					naryLogicOp(operator, Arrays.copyOfRange(arguments, 1, arguments.length)),
					operator);
		}
	}
	
	def Expression and(Expression... arguments) {
		return naryLogicOp(BinaryLogicOperator.AND, arguments);
	}
	
	def Expression and(Stream<Expression> arguments) {
		return naryLogicOp(BinaryLogicOperator.AND, arguments.collect(Collectors.toList));
	}
	
	def Expression or(Expression... arguments) {
		return naryLogicOp(BinaryLogicOperator.OR, arguments);
	}
	
	def Expression or(Stream<Expression> arguments) {
		return naryLogicOp(BinaryLogicOperator.OR, arguments.collect(Collectors.toList));
	}
	
	// CTL expressions
	def UnaryCtlExpression createUnaryCtlExpression(Expression operand, UnaryCtlOperator operator) {
		Preconditions.checkNotNull(operand, "operand");
		Preconditions.checkNotNull(operator, "operator");
		EmfHelper.checkNotContained(operand, "The provided operand is already assigned to another model element. Copy it with EcoreUtil.copy().");
		
		val ret = ExprFactory.eINSTANCE.createUnaryCtlExpression();
		ret.operand = operand;
		ret.operator = operator;
		ret.type = createTemporalBoolType();
		return ret;
	}
	
	def BinaryCtlExpression createBinaryCtlExpression(Expression lhs, Expression rhs, BinaryCtlOperator operator) {
		Preconditions.checkNotNull(lhs, "lhs");
		Preconditions.checkNotNull(rhs, "rhs");
		Preconditions.checkNotNull(operator, "operator");
		EmfHelper.checkNotContained(lhs, "The provided left operand is already assigned to another model element. Copy it with EcoreUtil.copy().");
		EmfHelper.checkNotContained(rhs, "The provided right operand is already assigned to another model element. Copy it with EcoreUtil.copy().");
		if (strictChecks) {
			CfaTypeUtils.checkArgumentsHaveSameType(lhs, rhs, "Unable to create a(n) %s expression.", operator.literal);
		}
		
		val ret = ExprFactory.eINSTANCE.createBinaryCtlExpression();
		ret.leftOperand = lhs;
		ret.rightOperand = rhs;
		ret.operator = operator;
		ret.type = createTemporalBoolType();
		return ret;
	}
	
	def AX(Expression op) {
		return createUnaryCtlExpression(exprCopy(op), UnaryCtlOperator.AX);
	}
	
	def EX(Expression op) {
		return createUnaryCtlExpression(exprCopy(op), UnaryCtlOperator.EX);
	}
	
	def AF(Expression op) {
		return createUnaryCtlExpression(exprCopy(op), UnaryCtlOperator.AF);
	}
	
	def EF(Expression op) {
		return createUnaryCtlExpression(exprCopy(op), UnaryCtlOperator.EF);
	}
	
	def AG(Expression op) {
		return createUnaryCtlExpression(exprCopy(op), UnaryCtlOperator.AG);
	}
	
	def EG(Expression op) {
		return createUnaryCtlExpression(exprCopy(op), UnaryCtlOperator.EG);
	}
	
	def AU(Expression lhs, Expression rhs) {
		return createBinaryCtlExpression(exprCopy(lhs), exprCopy(rhs), BinaryCtlOperator.AU);
	}
	
	def EU(Expression lhs, Expression rhs) {
		return createBinaryCtlExpression(exprCopy(lhs), exprCopy(rhs), BinaryCtlOperator.EU);
	}	
	
	// LTL expressions
	
	def UnaryLtlExpression createUnaryLtlExpression(Expression operand, UnaryLtlOperator operator) {
		Preconditions.checkNotNull(operand, "operand");
		Preconditions.checkNotNull(operator, "operator");
		EmfHelper.checkNotContained(operand, "The provided operand is already assigned to another model element. Copy it with EcoreUtil.copy().");
		
		val ret = ExprFactory.eINSTANCE.createUnaryLtlExpression();
		ret.operand = operand;
		ret.operator = operator;
		ret.type = createTemporalBoolType();
		return ret;
	}
	
	def BinaryLtlExpression createBinaryLtlExpression(Expression lhs, Expression rhs, BinaryLtlOperator operator) {
		Preconditions.checkNotNull(lhs, "lhs");
		Preconditions.checkNotNull(rhs, "rhs");
		Preconditions.checkNotNull(operator, "operator");
		EmfHelper.checkNotContained(lhs, "The provided left operand is already assigned to another model element. Copy it with EcoreUtil.copy().");
		EmfHelper.checkNotContained(rhs, "The provided right operand is already assigned to another model element. Copy it with EcoreUtil.copy().");
		if (strictChecks) {
			CfaTypeUtils.checkArgumentsHaveSameType(lhs, rhs, "Unable to create a(n) %s expression.", operator.literal);
		}
		
		val ret = ExprFactory.eINSTANCE.createBinaryLtlExpression();
		ret.leftOperand = lhs;
		ret.rightOperand = rhs;
		ret.operator = operator;
		ret.type = createTemporalBoolType();
		return ret;
	}
	
	def X(Expression op) {
		return createUnaryLtlExpression(exprCopy(op), UnaryLtlOperator.X);
	}
	
	def F(Expression op) {
		return createUnaryLtlExpression(exprCopy(op), UnaryLtlOperator.F);
	}
	
	def G(Expression op) {
		return createUnaryLtlExpression(exprCopy(op), UnaryLtlOperator.G);
	}
	
	def U(Expression lhs, Expression rhs) {
		return createBinaryLtlExpression(exprCopy(lhs), exprCopy(rhs), BinaryLtlOperator.U);
	}
	
	def R(Expression lhs, Expression rhs) {
		return createBinaryLtlExpression(exprCopy(lhs), exprCopy(rhs), BinaryLtlOperator.R);
	}
	
	// N-ary expressions
	
	def UninterpretedFunction createUinterpretedFunction(List<Expression> operands, String symbol, Type type) {
		Preconditions.checkNotNull(operands, "operands");
		Preconditions.checkNotNull(symbol, "symbol");
		Preconditions.checkNotNull(type, "type");
		EmfHelper.checkNotContained(type, "The provided type is already assigned to another model element. Copy it with EcoreUtil.copy().")
		
		for (op : operands) {
			Preconditions.checkNotNull(op, "element of operands");
			EmfHelper.checkNotContained(op, "One of the provided operands is already assigned to another model element. Copy it with EcoreUtil.copy().");
		}
		
		val ret = ExprFactory.eINSTANCE.createUninterpretedFunction();
		ret.operands.addAll(operands);
		ret.symbol = symbol;
		ret.type = type;
		return ret;
	}
	
	def LibraryFunction createLibraryFunction(List<Expression> operands, LibraryFunctions function, Type type) {
		Preconditions.checkNotNull(operands, "operands");
		Preconditions.checkNotNull(function, "function");
		Preconditions.checkNotNull(type, "type");
		EmfHelper.checkNotContained(type, "The provided type is already assigned to another model element. Copy it with EcoreUtil.copy().")
		
		for (op : operands) {
			Preconditions.checkNotNull(op, "element of operands");
			EmfHelper.checkNotContained(op, "One of the provided operands is already assigned to another model element. Copy it with EcoreUtil.copy().");
		}
		
		val ret = ExprFactory.eINSTANCE.createLibraryFunction();
		ret.operands.addAll(operands);
		ret.function = function;
		ret.type = type;
		return ret;
	}
	
	// Beginning and end of cycle predicates 
	def BeginningOfCycle createBeginningOfCycle() {
		val ret = ExprFactory.eINSTANCE.createBeginningOfCycle();
		ret.type = createBoolType();
		return ret;
	}
	
	def EndOfCycle createEndOfCycle() {
		val ret = ExprFactory.eINSTANCE.createEndOfCycle();
		ret.type = createBoolType();
		return ret;
	}
	
	// Elementary types
	// -----
	
	def IntType createIntType(boolean signed, int bits) {
		Preconditions.checkArgument(bits >= 1, "Bit width should be a positive integer number.");
		
		val ret = ExprFactory.eINSTANCE.createIntType();
		ret.signed = signed;
		ret.bits = bits;
		return ret;
	}
	
	def FloatType createFloatType(FloatWidth bits) {
		Preconditions.checkNotNull(bits, "bits");
		
		val ret = ExprFactory.eINSTANCE.createFloatType();
		ret.bits = bits;
		return ret;
	}
	
	def BoolType createBoolType() {
		val ret = ExprFactory.eINSTANCE.createBoolType();
		return ret;
	}
	
	def TemporalBoolType createTemporalBoolType() {
		val ret = ExprFactory.eINSTANCE.createTemporalBoolType();
		return ret;
	}
	
	def StringType createStringType(int maxLength) {
		Preconditions.checkArgument(maxLength >= 0, "Negative max length for string type.");
		
		val ret = ExprFactory.eINSTANCE.createStringType();
		ret.maxLength = maxLength;
		return ret;
	}
	
	// Literals
	// --------
	
	// Based on this answer: https://stackoverflow.com/a/680040
	private def IntType computeSmallestIntType(long value) {
		val ret = ExprFactory.eINSTANCE.createIntType();
		ret.signed = value < 0;
		if (ret.signed) {
			ret.bits = Long.SIZE-Long.numberOfLeadingZeros(-value - 1) + 1;
		}
		else {
			ret.bits = Long.SIZE-Long.numberOfLeadingZeros(value);
		}
		ret.bits = Math.max(ret.bits, 1);
		return ret;
	}
	
	def boolean fitsWithin(long value, IntType type) {
		if (value < 0 && !type.signed) {
			// negative number could not fit into unsigned type
			return false;
		}
		
		val smallestType = computeSmallestIntType(value);
		return smallestType.unsignedBits <= type.unsignedBits;
	}
	
	def IntLiteral createIntLiteral(long value, IntType type) {
		Preconditions.checkNotNull(type, "type");
		EmfHelper.checkNotContained(type, "The provided type is already assigned to another model element. Copy it with EcoreUtil.copy().");
		
		val smallestType = computeSmallestIntType(value)
		Preconditions.checkArgument(!smallestType.signed || type.signed,
			"The provided literal value cannot have the provided type: the type is unsigned, but the value is negative.");
		Guava21Workaround.checkArgument(fitsWithin(value, type),
			"The provided literal value (%s) cannot have the provided type (%s): the bit width is too small.",
			value, ExprToString.toDiagString(type));
		
		val ret = ExprFactory.eINSTANCE.createIntLiteral();
		ret.value = value;
		ret.type = type;
		return ret;
	}
	
	def IntLiteral createIntLiteralWithUnknownType(long value) {		
		val ret = ExprFactory.eINSTANCE.createIntLiteral();
		ret.value = value;
		ret.type = ExprFactory.eINSTANCE.createUnknownType();
		return ret;
	}
	
	def IntLiteral createIntLiteral(long value, boolean signed, int bits) {
		return createIntLiteral(value, createIntType(signed, bits));
	}
	
	def IntLiteral createIntLiteralWithMinimalType(long value) {
		val smallestType = computeSmallestIntType(value);
		val ret = ExprFactory.eINSTANCE.createIntLiteral();
		ret.value = value;
		ret.type = smallestType;
		return ret;
	}

	def FloatLiteral createFloatLiteral(double value, FloatType type) {
		Preconditions.checkNotNull(type, "type");
		EmfHelper.checkNotContained(type, "The provided type is already assigned to another model element. Copy it with EcoreUtil.copy().");
		Preconditions.checkNotNull(type.bits, "type.bits");
		
		val ret = ExprFactory.eINSTANCE.createFloatLiteral();
		ret.value = value;
		ret.type = type;
		return ret;
	}
	
	def FloatLiteral createFloatLiteral(double value, FloatWidth width) {
		return createFloatLiteral(value, createFloatType(width));
	}
	
	def BoolLiteral createBoolLiteral(boolean value, BoolType type) {
		Preconditions.checkNotNull(type, "type");
		EmfHelper.checkNotContained(type, "The provided type is already assigned to another model element. Copy it with EcoreUtil.copy().");
		
		val ret = ExprFactory.eINSTANCE.createBoolLiteral();
		ret.value = value;
		ret.type = type;
		return ret;
	}
	
	def BoolLiteral createBoolLiteral(boolean value) {
		return createBoolLiteral(value, createBoolType());
	}
	
	def BoolLiteral trueLiteral() {
		return createBoolLiteral(true, createBoolType());
	}
	
	def BoolLiteral falseLiteral() {
		return createBoolLiteral(false, createBoolType());
	}
	
	def StringLiteral createStringLiteral(String value, StringType type) {
		Preconditions.checkNotNull(value, "value");
		Preconditions.checkNotNull(type, "type");
		EmfHelper.checkNotContained(type, "The provided type is already assigned to another model element. Copy it with EcoreUtil.copy().");
		Preconditions.checkArgument(value.length <= type.maxLength, 
			"The provided literal value cannot have the provided type: the maximum allowed length is not enough to store the value.");
		
		val ret = ExprFactory.eINSTANCE.createStringLiteral();
		ret.value = value;
		ret.type = type;
		return ret;
	}
	
	def StringLiteral createStringLiteral(String value, int maxLength) {
		return createStringLiteral(value, createStringType(maxLength));
	}
	
	def Literal createDefaultLiteral(Type baseType) {
		Preconditions.checkNotNull(baseType, "type");
		EmfHelper.checkNotContained(baseType, "The provided type is already assigned to another model element. Copy it with EcoreUtil.copy().");
		Preconditions.checkNotNull(baseType, "type");
		
		if (baseType instanceof BoolType) {
			return createBoolLiteral(false, baseType);
		}

		if (baseType instanceof IntType) {
			return createIntLiteral(0, baseType);
		}

		if (baseType instanceof FloatType) {
			return createFloatLiteral(0.0, baseType);
		}
		
		throw new UnsupportedOperationException("Unsupported baseType: " + baseType);
	}
	
	
	
	/**
	 * Returns the given {@code expr} converted to {@code targetType}. If the
	 * {@code expr} already has a type {@code targetType}, the original
	 * {@code expr} is returned. This method <b>does not check</b> whether the
	 * conversion of {@code expr} to the type {@code targetType} is valid or
	 * not.
	 *
	 * @param expr
	 *            The expression to be converted (if needed).
	 * @param targetType
	 *            The target type.
	 * @return The given expression, converted to the target type.
	 */
	def Expression withTypeConversionIfNeeded(Expression expr, Type targetType) {
		if (expr.getType().dataTypeEquals(targetType)) {
			return expr;
		} else {
			return createTypeConversion(expr, EcoreUtil.copy(targetType));
		}
	}
}
