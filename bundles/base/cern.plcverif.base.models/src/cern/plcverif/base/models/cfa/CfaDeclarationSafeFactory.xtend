/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *   Xaver Fink - further additions
 *****************************************************************************/
package cern.plcverif.base.models.cfa

import static org.eclipse.emf.ecore.util.EcoreUtil.copy;
import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.base.models.Guava21Workaround
import cern.plcverif.base.models.cfa.cfabase.ArrayType
import cern.plcverif.base.models.cfa.cfabase.DataDirection
import cern.plcverif.base.models.cfa.cfabase.Location
import cern.plcverif.base.models.cfa.cfabase.Transition
import cern.plcverif.base.models.cfa.cfadeclaration.AssignmentTransition
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.Call
import cern.plcverif.base.models.cfa.cfadeclaration.CallTransition
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.CfadeclarationFactory
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructure
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructureRef
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructureSourceType
import cern.plcverif.base.models.cfa.cfadeclaration.DirectionFieldAnnotation
import cern.plcverif.base.models.cfa.cfadeclaration.Field
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef
import cern.plcverif.base.models.cfa.cfadeclaration.ForLoopLocationAnnotation
import cern.plcverif.base.models.cfa.cfadeclaration.Indexing
import cern.plcverif.base.models.cfa.cfadeclaration.InternalGeneratedFieldAnnotation
import cern.plcverif.base.models.cfa.cfadeclaration.OriginalDataTypeFieldAnnotation
import cern.plcverif.base.models.cfa.cfadeclaration.ReturnValueFieldAnnotation
import cern.plcverif.base.models.cfa.cfadeclaration.SourceTypeDataStructureAnnotation
import cern.plcverif.base.models.cfa.cfadeclaration.VariableAssignment
import cern.plcverif.base.models.cfa.utils.CfaDeclarationUtils
import cern.plcverif.base.models.cfa.utils.CfaTypeUtils
import cern.plcverif.base.models.expr.BoolType
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.InitialValue
import cern.plcverif.base.models.expr.IntLiteral
import cern.plcverif.base.models.expr.IntType
import cern.plcverif.base.models.expr.Type
import cern.plcverif.base.models.expr.utils.ExprUtils
import com.google.common.base.Preconditions
import java.util.ArrayList
import java.util.List
import org.eclipse.emf.ecore.util.EcoreUtil

import static cern.plcverif.base.models.cfa.utils.CfaTypeUtils.haveSameType
import cern.plcverif.base.models.expr.ExpressionSafeFactory
import cern.plcverif.base.models.expr.ComparisonOperator
import cern.plcverif.base.models.cfa.cfadeclaration.VariableComparison

class CfaDeclarationSafeFactory extends CfaBaseSafeFactory {
	public static val INSTANCE = new CfaDeclarationSafeFactory(DEFAULT_STRICT);
	
	protected new(boolean strict) {
		super(strict);
	}
	
	// CFA high-level objects
	// ----------------------
	
	def CfaNetworkDeclaration createCfaNetworkDeclaration(String displayName) {
		Preconditions.checkNotNull(displayName, "displayName");
		
		val ret = CfadeclarationFactory.eINSTANCE.createCfaNetworkDeclaration();
		ret.name = LocalIdGenerator.INSTANCE.sanitizeName(displayName);
		ret.displayName = displayName;
		createRootDataStructure("_global", ret);
		return ret;
	}
	
	def AutomatonDeclaration createAutomatonDeclaration(String displayName, CfaNetworkDeclaration cfaNetwork, DataStructureRef localDataStructure) {
		Preconditions.checkNotNull(displayName, "displayName");
		Preconditions.checkNotNull(cfaNetwork, "cfaNetwork");
		Preconditions.checkNotNull(localDataStructure, "localDataStructure");
		EmfHelper.checkNotContained(localDataStructure, "The provided data structure reference is already assigned to another model element. Copy it with EcoreUtils.copy().");
	
		val ret = CfadeclarationFactory.eINSTANCE.createAutomatonDeclaration();
		ret.name = LocalIdGenerator.INSTANCE.generateId(cfaNetwork, displayName);
		ret.displayName = displayName;
		ret.container = cfaNetwork;
		ret.localDataStructure = localDataStructure;
		return ret;
	}
	
	def AutomatonDeclaration createAutomatonDeclaration(String displayName, CfaNetworkDeclaration cfaNetwork, DataStructure localDataStructure) {
		Preconditions.checkNotNull(localDataStructure, "localDataStructure");
		
		return createAutomatonDeclaration(displayName, cfaNetwork, createDataStructureRef(localDataStructure));
	}
	
	def AutomatonDeclaration createMainAutomatonDeclaration(String displayName, CfaNetworkDeclaration cfaNetwork, DataRef mainContext) {
		Preconditions.checkNotNull(displayName, "displayName");
		Preconditions.checkNotNull(cfaNetwork, "cfaNetwork");
		Preconditions.checkNotNull(mainContext, "mainContext");
		EmfHelper.checkNotContained(mainContext, "The provided main context reference is already assigned to another model element. Copy it with EcoreUtils.copy().");
		
		val ret = createAutomatonDeclaration(displayName, cfaNetwork, EcoreUtil.copy(mainContext.type as DataStructureRef));
		cfaNetwork.mainAutomaton = ret;
		cfaNetwork.mainContext = mainContext;
		return ret;
	}
	
	// Transitions
	// -----------
	
	def AssignmentTransition createAssignmentTransition(String displayName, AutomatonDeclaration parentAutomaton, Location source, Location target, Expression condition) {
		Preconditions.checkNotNull(displayName, "displayName");
		Preconditions.checkNotNull(parentAutomaton, "parentAutomaton");
		Preconditions.checkNotNull(source, "source");
		Preconditions.checkNotNull(target, "target");
		Preconditions.checkNotNull(condition, "condition");
		EmfHelper.checkNotContained(condition, "The provided expression is already assigned to another model element. Copy it with EcoreUtils.copy().");
		checkBooleanType(condition);
		
		val ret = CfadeclarationFactory.eINSTANCE.createAssignmentTransition();
		ret.name = LocalIdGenerator.INSTANCE.generateId(parentAutomaton, displayName);
		ret.displayName = displayName;
		ret.parentAutomaton = parentAutomaton;
		ret.source = source;
		ret.target = target;
		ret.condition = condition;
		return ret;
	}
	
	def CallTransition createCallTransition(String displayName, AutomatonDeclaration parentAutomaton, Location source, Location target, Expression condition) {
		Preconditions.checkNotNull(displayName, "displayName");
		Preconditions.checkNotNull(parentAutomaton, "parentAutomaton");
		Preconditions.checkNotNull(source, "source");
		Preconditions.checkNotNull(target, "target");
		Preconditions.checkNotNull(condition, "condition");
		EmfHelper.checkNotContained(condition, "The provided expression is already assigned to another model element. Copy it with EcoreUtils.copy().");
		checkBooleanType(condition);
		
		val ret = CfadeclarationFactory.eINSTANCE.createCallTransition();
		ret.name = LocalIdGenerator.INSTANCE.generateId(parentAutomaton, displayName);
		ret.displayName = displayName;
		ret.parentAutomaton = parentAutomaton;
		ret.source = source;
		ret.target = target;
		ret.condition = condition;
		return ret;
	}
	
	def Call createCall(CallTransition parentTransition, AutomatonDeclaration calledAutomaton, DataRef calleeContext) {
		Preconditions.checkNotNull(parentTransition, "parentTransition");
		Preconditions.checkNotNull(calledAutomaton, "calledAutomaton");
		Preconditions.checkNotNull(calleeContext, "calleeContext");
		EmfHelper.checkNotContained(calleeContext, "The provided callee context reference is already assigned to another model element. Copy it with EcoreUtils.copy().");
		Preconditions.checkArgument(calleeContext.type.dataTypeEquals((calledAutomaton.localDataStructure)), "The provided callee context reference has different type than the local data structure of the called automaton.");
		
		val ret = CfadeclarationFactory.eINSTANCE.createCall();
		ret.parentTransition = parentTransition;
		ret.calledAutomaton = calledAutomaton;
		ret.calleeContext = calleeContext;
		return ret;
	}
	
	def List<VariableAssignment> createInputAssignment(Call call, DataRef leftValue, Expression rightValue) {
		Preconditions.checkNotNull(call, "call");
		
		val ret = newAssignment(leftValue, rightValue, new ArrayList<VariableAssignment>());
		call.inputAssignments.addAll(ret);
		return ret;
	}
	
	def List<VariableAssignment> createOutputAssignment(Call call, DataRef leftValue, Expression rightValue) {
		Preconditions.checkNotNull(call, "call");
		
		val ret = newAssignment(leftValue, rightValue, new ArrayList<VariableAssignment>());
		call.outputAssignments.addAll(ret);
		return ret;
	}
	
	def List<VariableAssignment> createAssignment(AssignmentTransition transition, DataRef leftValue, Expression rightValue) {
		Preconditions.checkNotNull(transition, "transition");
				
		val ret = newAssignment(leftValue, rightValue, new ArrayList<VariableAssignment>());
		transition.assignments.addAll(ret);
		return ret;
	}
	
	def void createAssumeBound(AssignmentTransition transition, DataRef leftValue, VariableComparison bound) {
		Preconditions.checkNotNull(transition, "transition");
		Preconditions.checkNotNull(leftValue, "leftValue");
		Preconditions.checkNotNull(bound, "bound");
		Preconditions.checkNotNull(bound.rightValue, "bound");
		Preconditions.checkNotNull(bound.comparator, "bound");
		val boundCopy = copy(bound.rightValue)
		EmfHelper.checkNotContained(leftValue, "The provided left value is already assigned to another model element. Copy it with EcoreUtils.copy().");
		EmfHelper.checkNotContained(boundCopy, "The provided right value is already assigned to another model element. Copy it with EcoreUtils.copy().");
		val boundExpr = ExpressionSafeFactory.INSTANCE.createComparisonExpression(leftValue, boundCopy, bound.comparator)
		
		transition.assumeBounds.add(boundExpr);
	}
	
	def void createAssumption(AssignmentTransition transition, Expression assumption) {
		Preconditions.checkNotNull(transition, "transition");
		Preconditions.checkNotNull(assumption, "assumption");
		
		transition.assumeBounds.add(ExprUtils.exprCopy(assumption));
	}
	
	
	def void createAssumeBound(AssignmentTransition transition, DataRef leftValue, Expression rightValue, ComparisonOperator comparator) {
		Preconditions.checkNotNull(transition, "transition");
		Preconditions.checkNotNull(leftValue, "leftValue");
		Preconditions.checkNotNull(rightValue, "bound");
		Preconditions.checkNotNull(comparator, "bound");
		val boundCopy = copy(rightValue)
		EmfHelper.checkNotContained(leftValue, "The provided left value is already assigned to another model element. Copy it with EcoreUtils.copy().");
		EmfHelper.checkNotContained(boundCopy, "The provided right value is already assigned to another model element. Copy it with EcoreUtils.copy().");
		val boundExpr = ExpressionSafeFactory.INSTANCE.createComparisonExpression(leftValue, boundCopy, comparator)
		
		transition.assumeBounds.add(boundExpr);
	}
	
	private def List<VariableAssignment> newAssignment(DataRef leftValue, Expression rightValue, List<VariableAssignment> assignments) {
		Preconditions.checkNotNull(leftValue, "leftValue");
		Preconditions.checkNotNull(rightValue, "rightValue");
		Preconditions.checkNotNull(assignments, "assignments");
		EmfHelper.checkNotContained(leftValue, "The provided left value is already assigned to another model element. Copy it with EcoreUtils.copy().");
		EmfHelper.checkNotContained(rightValue, "The provided right value is already assigned to another model element. Copy it with EcoreUtils.copy().");
		
		val lhsType = leftValue.type;
		val rhsType = rightValue.type;
		
		if (lhsType.dataTypeEquals(rhsType)) {
			// Fully compatible types, nothing to expand
		
			val assigment = CfadeclarationFactory.eINSTANCE.createVariableAssignment();
			assigment.leftValue = leftValue;
			assigment.rightValue = rightValue;
			assignments.add(assigment);
		}
		else if (lhsType instanceof DataStructureRef && rhsType instanceof DataStructureRef) {
			// Both data structures, have to check if they are compatible
			
			if (rightValue instanceof DataRef == false) {
				// Can only happen if we have and expression with a complex data type, which is currently impossible
				throw new IllegalArgumentException("Illegal assignment: the right-hand-side operand of a compound assignment must be a single reference to an instance of a data structure.");
			}
			for (var int i = 0; i < (lhsType as DataStructureRef).definition.fields.size; i++) {
				val newLhsField = (lhsType as DataStructureRef).definition.fields.get(i);
				val newRhsField = (rhsType as DataStructureRef).definition.fields.get(i);
				
				if (newLhsField.displayName.equals(newRhsField.displayName) == false) {
					throw new IllegalArgumentException("Illegal assignment: the provided operands have different and incompatible types (field names or order do not match).");
				}
				
				val newLhsRef = appendFieldRef(EcoreUtil.copy(leftValue), newLhsField);
				val newRhsRef = appendFieldRef(EcoreUtil.copy(rightValue as DataRef), newRhsField);
				newAssignment(newLhsRef, newRhsRef, assignments);
			}
		}
		else if (lhsType instanceof ArrayType && rhsType instanceof ArrayType && (lhsType as ArrayType).elementType instanceof DataStructureRef && (rhsType as ArrayType).elementType instanceof DataStructureRef) {
			// Both arrays with complex elements, possibly needs to be expanded
			
			val lhsDimension = (lhsType as ArrayType).dimension;
			val rhsDimension = (rhsType as ArrayType).dimension;
			if (lhsDimension.size != rhsDimension.size) {
				throw new IllegalArgumentException("Illegal assignment: the lengths of the two arrays do not match (may be embedded in a compound assignment).");
			}
			if (lhsDimension.defined == false) {
				throw new IllegalArgumentException("Illegal assignment: operands of a compound assignments must not contain arrays of undefined size.");
			}
			for (var int i = 0; i < lhsDimension.size; i++) {
				val lhsIndex = i + lhsDimension.lowerIndex;
				val rhsIndex = i + rhsDimension.lowerIndex;
				val newLhsRef = appendIndexing(EcoreUtil.copy(leftValue), lhsIndex);
				val newRhsRef = appendIndexing(EcoreUtil.copy(rightValue as DataRef), rhsIndex);
				newAssignment(newLhsRef, newRhsRef, assignments);
			}
		}
		else {
			// Totally different types (like array + data structure or different elementary types) 
			throw new IllegalArgumentException("Illegal assignment: the provided operands have different and incompatible types: left type=" + lhsType + "; right type=" + rhsType);
		}
		return assignments;
	}
	
	// Data structures
	// ---------------
	
	def DataStructure createRootDataStructure(String displayName, CfaNetworkDeclaration cfaNetwork) {
		Preconditions.checkNotNull(displayName, "displayName");
		Preconditions.checkNotNull(cfaNetwork, "cfaNetwork");
		
		val ret = CfadeclarationFactory.eINSTANCE.createDataStructure();
		ret.name = LocalIdGenerator.INSTANCE.generateId(cfaNetwork, displayName);
		ret.displayName = displayName;
		cfaNetwork.rootDataStructure = ret;
		return ret;
	}
	
	def DataStructure createDataStructure(String displayName, DataStructure enclosingType) {
		Preconditions.checkNotNull(displayName, "displayName");
		Preconditions.checkNotNull(enclosingType, "The enclosingType argument cannot be null. Use createRootDataStructure() for the global structure.");
		
		val ret = CfadeclarationFactory.eINSTANCE.createDataStructure();
		ret.name = LocalIdGenerator.INSTANCE.generateId(enclosingType, displayName);
		ret.displayName = displayName;
		ret.enclosingType = enclosingType;
		return ret;
	}
	
	def DataStructureRef createDataStructureRef(DataStructure definition) {
		Preconditions.checkNotNull(definition);
		
		val ret = CfadeclarationFactory.eINSTANCE.createDataStructureRef();
		ret.definition = definition;
		return ret;
	}
	
	def Field createField(String displayName, DataStructure enclosingType, Type type) {
		Preconditions.checkNotNull(displayName, "displayName");
		Preconditions.checkNotNull(enclosingType, "enclosingType");
		Preconditions.checkNotNull(type, "type");
		EmfHelper.checkNotContained(type, "The provided type is already assigned to another model element. Copy it with EcoreUtils.copy().");
		
		val ret = CfadeclarationFactory.eINSTANCE.createField();
		ret.name = LocalIdGenerator.INSTANCE.generateId(enclosingType, displayName);
		ret.displayName = displayName;
		ret.enclosingType = enclosingType;
		ret.type = type;
		return ret;
	}
	
	def void createFieldBound(Field fieldToBound, InitialValue bound, ComparisonOperator comparator) {
		Preconditions.checkNotNull(fieldToBound, "fieldToInitialize");
		Preconditions.checkNotNull(bound, "initialValue");
		EmfHelper.checkNotContained(bound, "The provided value is already assigned to another model element. Copy it with EcoreUtils.copy().");
		CfaTypeUtils.checkArgumentsHaveSameType(fieldToBound, bound, "Bound and variable should have the same type.");
		
		val fieldRef = createFieldRef(fieldToBound);
		val lhsType = fieldRef.type;
		val rhsType = bound.type;
		if (lhsType.dataTypeEquals(rhsType)) {	
			val comparison = CfadeclarationFactory.eINSTANCE.createVariableComparison();
			comparison.comparator = comparator
			comparison.leftValue = fieldRef
			comparison.rightValue = bound
			fieldToBound.bounds.add(comparison)
		} else {
			throw new IllegalArgumentException("Bounds are currently only supported for basic integer variable types.");
		}
	}
	
	def void createInitialAssignment(Field fieldToInitialize, InitialValue initialValue) {
		Preconditions.checkNotNull(fieldToInitialize, "fieldToInitialize");
		Preconditions.checkNotNull(initialValue, "initialValue");
		EmfHelper.checkNotContained(initialValue, "The provided value is already assigned to another model element. Copy it with EcoreUtils.copy().");
		CfaTypeUtils.checkArgumentsHaveSameType(fieldToInitialize, initialValue, "Unable to create an initial assignment.");
		
		val fieldRef = createFieldRef(fieldToInitialize);
		newAssignment(fieldRef, initialValue, fieldToInitialize.initialAssignments);
	}
	
	// TODO: mention in javadoc that relativePathToDataElement must start with a reference to the field
	def void createComplexInitialAssignment(Field complexFieldToInitialize, DataRef relativePathToDataElement, InitialValue initialValue) {
		Preconditions.checkNotNull(complexFieldToInitialize, "complexFieldToInitialize");
		Preconditions.checkNotNull(relativePathToDataElement, "relativePathToDataElement");
		Preconditions.checkNotNull(initialValue, "initialValue");
		EmfHelper.checkNotContained(relativePathToDataElement, 
			"The provided reference is already assigned to another model element. Copy it with EcoreUtils.copy().");
		EmfHelper.checkNotContained(initialValue, 
			"The provided value is already assigned to another model element. Copy it with EcoreUtils.copy().");
		Guava21Workaround.checkArgument(haveSameType(relativePathToDataElement, initialValue), 
			"The value %s is not compatible with %s.", initialValue, relativePathToDataElement);
		CfaDeclarationUtils.checkRefStartsWith(relativePathToDataElement, complexFieldToInitialize);
		
		newAssignment(relativePathToDataElement, initialValue, complexFieldToInitialize.initialAssignments);
	}
	
	// References
	// ----------
		
	def FieldRef createFieldRef(Field field) {
		Preconditions.checkNotNull(field, "Unable to create field reference to null.");
		
		val FieldRef ret = CfadeclarationFactory.eINSTANCE.createFieldRef();
		ret.field = field;
		return ret;
	}
	
	def DataRef prependDataRef(DataRef prefix, DataRef ref) {
		Preconditions.checkNotNull(prefix, "The prefix argument cannot be null.")
		Preconditions.checkNotNull(ref, "ref");
		EmfHelper.checkNotContained(prefix, "The provided prefix is already assigned to another model element. Copy it with EcoreUtils.copy().");
		EmfHelper.checkNotContained(ref, "The provided reference is already assigned to another model element. Copy it with EcoreUtils.copy().");
		
		val ret = ref;
		var current = ref;
		while (current.prefix !== null) {
			current = current.prefix;
		} 
		Preconditions.checkState(current.prefix === null);
		current.prefix = prefix;
		return ret;
	}
	
	def FieldRef appendFieldRef(DataRef prefix, Field field) {
		Preconditions.checkNotNull(prefix, "The prefix argument cannot be null. Use createFieldRef() for local or global references.")
		Preconditions.checkNotNull(field, "field");
		EmfHelper.checkNotContained(prefix, "The provided prefix is already assigned to another model element. Copy it with EcoreUtils.copy().");
		Guava21Workaround.checkArgument(prefix.type instanceof DataStructureRef,
			"The provided prefix references a data element that is not a data structure: prefix.type=%s", prefix.type);
		Guava21Workaround.checkArgument((prefix.type as DataStructureRef).definition === field.enclosingType,
			"The provided prefix references a data element that does not own the provided field: type=%s; field=%s", prefix.type, field);
					
		val FieldRef ret = CfadeclarationFactory.eINSTANCE.createFieldRef();
		ret.prefix = prefix;
		ret.field = field;
		ret.frozen = prefix.frozen;
		return ret;
	}
	
	def Indexing appendIndexing(DataRef prefix, Expression index) {
		Preconditions.checkNotNull(prefix, "The prefix argument cannot be null. Use createRootFieldRef() for local or global references.");
		Preconditions.checkNotNull(index, "index");
		EmfHelper.checkNotContained(prefix, "The provided prefix is already assigned to another model element. Copy it with EcoreUtils.copy().");
		Preconditions.checkArgument(prefix.type instanceof ArrayType, "The provided prefix references a data element that is not an array.");
		Guava21Workaround.checkArgument(index.type instanceof IntType, "Arrays can be indexed with integer expressions only. Received type: %s", index.type);
		Guava21Workaround.checkArgument(isValidIndexType(index.type), "Arrays can be indexed with signed 16-bit integer expressions only. Received type: %s", index.type);
		Preconditions.checkArgument((index instanceof IntLiteral == false) || (prefix.type as ArrayType).dimension.isValidIndex(ExprUtils.toLong(index)),
			"The provided index is out of bounds of the array to which this indexing belongs.");
		EmfHelper.checkNotContained(index, "The provided index is already assigned to another model element. Copy it with EcoreUtils.copy().");
		
		val ret = CfadeclarationFactory.eINSTANCE.createIndexing();
		ret.prefix = prefix;
		ret.index = index;
		ret.frozen = prefix.frozen;
		return ret;
	}
	
	def Indexing appendIndexing(DataRef prefix, long index) {
		return appendIndexing(
			prefix,
			createIndexLiteral(index)
		);
	}
	
	// Annotations
	// -----------
		
	def ForLoopLocationAnnotation createForLoopLocationAnnotation(Location parentLocation, Transition loopBodyEntry, Transition loopNextCycle, Transition loopExit, DataRef loopVariable, Transition loopVariableInit, Transition loopVariableIncrement) {
		Preconditions.checkNotNull(loopVariable, "loopVariable");
		Preconditions.checkNotNull(loopVariableInit, "loopVariableInit");
		Preconditions.checkNotNull(loopVariableIncrement, "loopVariableIncrement");
		EmfHelper.checkNotContained(loopVariable, "The provided reference is already assigned to another model element. Copy it with EcoreUtils.copy().")
		
		val ret = CfadeclarationFactory.eINSTANCE.createForLoopLocationAnnotation();
		setupLoopLocationAnnotation(parentLocation, ret, loopBodyEntry, loopNextCycle, loopExit);
		ret.loopVariable = loopVariable;
		ret.loopVariableInit = loopVariableInit;
		ret.loopVariableIncrement = loopVariableIncrement;
		return ret;
	}
	
	def DirectionFieldAnnotation createDirectionFieldAnnotation(Field parentField, DataDirection direction) {
		Preconditions.checkNotNull(parentField, "parentField");
		Preconditions.checkNotNull(direction, "direction");
		
		val ret = CfadeclarationFactory.eINSTANCE.createDirectionFieldAnnotation();
		ret.parentField = parentField;
		ret.direction = direction;
		return ret;
	}
	
	def OriginalDataTypeFieldAnnotation createOriginalDataTypeFieldAnnotation(Field parentField, String plcDataType) {
		Preconditions.checkNotNull(parentField, "parentField");
		Preconditions.checkNotNull(plcDataType, "plcDataType");
		
		val ret = CfadeclarationFactory.eINSTANCE.createOriginalDataTypeFieldAnnotation();
		ret.parentField = parentField;
		ret.plcDataType = plcDataType;
		return ret;
	}
	
	def ReturnValueFieldAnnotation createReturnValueFieldAnnotation(Field parentField) {
		Preconditions.checkNotNull(parentField, "parentField");
		
		val ret = CfadeclarationFactory.eINSTANCE.createReturnValueFieldAnnotation();
		ret.parentField = parentField;
		return ret;
	}
	
	def InternalGeneratedFieldAnnotation createInternalGeneratedFieldAnnotation(Field parentField) {
		Preconditions.checkNotNull(parentField, "parentField");
		
		val ret = CfadeclarationFactory.eINSTANCE.createInternalGeneratedFieldAnnotation();
		ret.parentField = parentField;
		return ret;
	}
	
	def SourceTypeDataStructureAnnotation createSourceTypeDataStructureAnnotation(DataStructure parentDataStructure, String typename, DataStructureSourceType sourceType) {
		Preconditions.checkNotNull(parentDataStructure, "parentDataStructure");
		Preconditions.checkNotNull(typename, "typename");
		Preconditions.checkNotNull(sourceType, "sourceType");
		
		val ret = CfadeclarationFactory.eINSTANCE.createSourceTypeDataStructureAnnotation();
		ret.parentDataStructure = parentDataStructure;
		ret.typename = typename;
		ret.sourceType = sourceType;
		return ret;
	}
	
	private static def checkBooleanType(Expression expression) {
		if (!(expression.type instanceof BoolType)) {
			throw new IllegalArgumentException("The provided expression is not of type Boolean. Received type: " + expression.type);
		}
	}
}
