/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.cfa.visualization

import cern.plcverif.base.models.cfa.cfabase.Location
import cern.plcverif.base.models.cfa.cfabase.Transition
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.string.AbstractExprToString
import cern.plcverif.base.models.cfa.textual.CfaToString
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariable

final class BaseCfaToDotFormat implements ICfaToDotFormat {
	val MAX_LINE_LENGTH = 50;
	static val AbstractExprToString EXPR_TO_STRING = new CfaToString(false); 
	boolean showFrozen = true;

	new() {
		this(true);
	}

	new(boolean showFrozen) {
		this.showFrozen = showFrozen;
	}

	override labelPostProcessing(String label) {
		val lines = label.split("\\\\l") // double escaping needed: split takes a regular expression
			.map[it | splitIntoSizedChunks(it, MAX_LINE_LENGTH)].flatten.toList;
		if (lines.size <= 1) {
			return (lines.join).replaceAll("\\\"", "\\\\\"");
		} else {
			return lines.join("", "\\l", "\\l", [it | it]).replaceAll("\\\"", "\\\\\"");
		}
	}
	
	def Iterable<String> splitIntoSizedChunks(String string, int maxChunkLength) {
		val ret = newArrayList();
		for (var i = 0; i < string.length; i += maxChunkLength) {
			ret.add(string.substring(i, Math.min(i + maxChunkLength, string.length)));
		}
		return ret;
	}
	
	override locationBorderColor(Location location) {
		return "black";
	}

	override locationFillColor(Location location) {
		return if (showFrozen && location.frozen) "cadetblue1" else "white";
	}
	
	override locationShape(Location location) {
		return "ellipse";
	}

	override locationLabel(Location location) {
		throw new UnsupportedOperationException("TODO: auto-generated method stub");
	}

	override transitionBorderColor(Transition transition) {
		if (showFrozen && transition.frozen) {
			return "blue";
		} else {
			return "black";
		}
	}

	override transitionLabel(Transition transition) {
		throw new UnsupportedOperationException("TODO: auto-generated method stub");
	}
	
	override expressionToText(Expression expr) {
		return EXPR_TO_STRING.toString(expr);
	}
	
	override variableToText(AbstractVariable v) {
		return EXPR_TO_STRING.toString(v);
	}
	
	override showAnnotations() {
		return true;
	}
	
	override showDataStructure() {
		return true;
	}
	
	override showVariables() {
		return true;
	}
	
	override assignmentToText(cern.plcverif.base.models.cfa.cfadeclaration.VariableAssignment assignment) {
		return '''«expressionToText(assignment.leftValue)» := «expressionToText(assignment.rightValue)»«IF showFrozen && assignment.frozen» (*)«ENDIF»''';
	}
	
	override assignmentToText(cern.plcverif.base.models.cfa.cfainstance.VariableAssignment assignment) {
		return '''«expressionToText(assignment.leftValue)» := «expressionToText(assignment.rightValue)»«IF showFrozen && assignment.frozen» (*)«ENDIF»''';
	}
	
	override assumeBoundToText(Expression bnd) {
		return '''verifier_assume(«expressionToText(bnd)»)''';
	}
	
}
