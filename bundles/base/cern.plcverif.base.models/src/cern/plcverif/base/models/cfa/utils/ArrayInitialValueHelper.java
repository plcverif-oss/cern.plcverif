/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.cfa.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import com.google.common.base.Preconditions;

import cern.plcverif.base.models.cfa.cfabase.ArrayDimension;
import cern.plcverif.base.models.cfa.cfabase.ArrayType;
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef;
import cern.plcverif.base.models.cfa.cfadeclaration.Field;
import cern.plcverif.base.models.cfa.cfadeclaration.Indexing;
import cern.plcverif.base.models.cfa.cfadeclaration.VariableAssignment;
import cern.plcverif.base.models.cfa.cfainstance.ArrayElementInitialValue;
import cern.plcverif.base.models.cfa.cfainstance.ArrayVariable;
import cern.plcverif.base.models.expr.ElementaryType;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.IntLiteral;
import cern.plcverif.base.models.expr.Type;
import cern.plcverif.base.models.expr.utils.ExprUtils;

public final class ArrayInitialValueHelper {
	private List<ArrayDimension> dimensions = new ArrayList<>();
	private Expression[] initialValues;

	public ArrayInitialValueHelper(Field field) {
		Preconditions.checkNotNull(field, "field");
		Preconditions.checkArgument(field.getType() instanceof ArrayType);
		Type currentType = field.getType();
		int totalSize = 1;
		while (currentType instanceof ArrayType) {
			ArrayDimension dimension = ((ArrayType) currentType).getDimension();
			Preconditions.checkArgument(dimension.isDefined(), "This class does not support arrays of undefined size.");
			dimensions.add(dimension);
			totalSize *= dimension.getSize();
			currentType = ((ArrayType) currentType).getElementType();
		}

		Preconditions.checkArgument(currentType instanceof ElementaryType,
				"This class works for (multidimensional) arrays of elementary types only.");

		initialValues = new Expression[totalSize];

		for (VariableAssignment initialAssignment : field.getInitialAssignments()) {
			Preconditions.checkState(initialAssignment.getLeftValue() instanceof Indexing,
					"This class works for (multidimensional) arrays of elementary types only.");
			Stack<IntLiteral> indices = new Stack<>();

			DataRef currentRef = initialAssignment.getLeftValue();
			while (currentRef instanceof Indexing) {
				Expression index = ((Indexing) currentRef).getIndex();
				Preconditions.checkArgument(index instanceof IntLiteral,
						"This class does not support indices other than literal integers.");
				indices.push((IntLiteral) index);
			}
			Preconditions.checkState(currentRef == null,
					"The initial assignment reference (" + initialAssignment.getLeftValue()
							+ "does not address the array, it has a prefix of field references.");
			Preconditions.checkState(indices.size() == dimensions.size(),
					"There is an incorrect number of indices in the initial assignment reference.");

			int flatIndex = 0;
			int stepSize = 1;
			for (int i = 0; i < dimensions.size(); ++i) {
				int currentIndex = (int) (ExprUtils.toLong(indices.pop()) - dimensions.get(i).getLowerIndex());

				flatIndex += stepSize * currentIndex;

				stepSize *= dimensions.get(i).getSize();
			}
			initialValues[flatIndex] = initialAssignment.getRightValue();
		}
	}

	public ArrayInitialValueHelper(ArrayVariable array) {
		Preconditions.checkNotNull(array, "array");
		int totalSize = 1;
		for (ArrayDimension dimension : array.getDimensions()) {
			Preconditions.checkArgument(dimension.isDefined(), "This class does not support arrays of undefined size.");
			dimensions.add(dimension);
			totalSize *= dimension.getSize();
		}

		initialValues = new Expression[totalSize];

		for (ArrayElementInitialValue initialValue : array.getInitialValues()) {
			Preconditions.checkState(initialValue.getIndices().size() == dimensions.size(),
					"There is an incorrect number of indices in the initial assignment reference.");

			int flatIndex = 0;
			int stepSize = 1;
			for (int i = 0; i < dimensions.size(); ++i) {
				Preconditions.checkState(
						dimensions.get(i).isValidIndex(ExprUtils.toLong(initialValue.getIndices().get(i))),
						"Invalid index detected - index is out of bounds of the array.");
				int currentIndex = (int) (ExprUtils.toLong(initialValue.getIndices().get(i))
						- dimensions.get(i).getLowerIndex());


				flatIndex += stepSize * currentIndex;

				stepSize *= dimensions.get(i).getSize();
			}
			initialValues[flatIndex] = initialValue.getInitialValue();
		}
	}

	public Expression getInitialValue(List<Integer> indices) {
		Preconditions.checkNotNull(indices, "indices");
		Preconditions.checkState(indices.size() == dimensions.size(),
			"There is an incorrect number of indices in the provided list.");

		int flatIndex = 0;
		int stepSize = 1;
		for (int i = 0; i < dimensions.size(); ++i) {
			Preconditions.checkState(dimensions.get(i).isValidIndex(indices.get(i)),
					"Index was out of bounds of the array: take into account that dimensions may have lower bounds different from zero.");

			int currentIndex = indices.get(i) - dimensions.get(i).getLowerIndex();

			flatIndex += stepSize * currentIndex;

			stepSize *= dimensions.get(i).getSize();
		}

		return initialValues[flatIndex];
	}

	public Expression getInitialValue(int flatIndex) {
		Preconditions.checkElementIndex(flatIndex, initialValues.length,
				"The index is out of bound of the initial values array.");

		return initialValues[flatIndex];
	}
}
