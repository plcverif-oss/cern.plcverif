/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.cfa.trace.declared;

import java.util.Stack;

import cern.plcverif.base.models.expr.Expression;

public final class Reference {
	private TracedData data;
	private Stack<Expression> indices = new Stack<>();

	public TracedData getData() {
		return data;
	}

	public void setData(TracedData data) {
		this.data = data;
	}

	public Reference(TracedData data) {
		this.data = data;
	}

	public Stack<Expression> getIndices() {
		return indices;
	}
}
