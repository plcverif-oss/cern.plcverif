/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.models.expr.utils

import cern.plcverif.base.models.expr.Type
import cern.plcverif.base.models.expr.UnknownType
import com.google.common.base.Preconditions
import org.eclipse.emf.ecore.util.EcoreUtil

abstract class AbstractTypeUtils {
	protected new() {}
	
	def boolean dataTypeEquals(Void left, Type right) {
		throw new UnsupportedOperationException("Cannot compare type with null.");
	}
	
	def boolean dataTypeEquals(Type left, Type right) {
		if (TypeUtils.isTemporalOrRegularBoolType(left) && TypeUtils.isTemporalOrRegularBoolType(right)) {
			return true;
		} else {
			EcoreUtil.equals(left, right);
		}
	}
	
	def boolean dataTypeEquals(UnknownType left, Type right) {
		Preconditions.checkNotNull(right, "right");
		return false;
	}
}
