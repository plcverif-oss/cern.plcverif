/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.models.cfa.trace

import cern.plcverif.base.common.logging.IPlcverifLogger
import cern.plcverif.base.common.logging.PlatformLogger
import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory
import cern.plcverif.base.models.cfa.CfaInstanceSafeFactory
import cern.plcverif.base.models.cfa.cfabase.ArrayType
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructure
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructureRef
import cern.plcverif.base.models.cfa.cfadeclaration.DirectionFieldAnnotation
import cern.plcverif.base.models.cfa.cfadeclaration.Field
import cern.plcverif.base.models.cfa.cfadeclaration.FieldAnnotation
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef
import cern.plcverif.base.models.cfa.cfadeclaration.Indexing
import cern.plcverif.base.models.cfa.cfadeclaration.InternalGeneratedFieldAnnotation
import cern.plcverif.base.models.cfa.cfadeclaration.OriginalDataTypeFieldAnnotation
import cern.plcverif.base.models.cfa.cfadeclaration.ReturnValueFieldAnnotation
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariable
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariableRef
import cern.plcverif.base.models.cfa.cfainstance.ArrayElementRef
import cern.plcverif.base.models.cfa.cfainstance.ArrayVariable
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance
import cern.plcverif.base.models.cfa.cfainstance.Variable
import cern.plcverif.base.models.cfa.cfainstance.VariableRef
import cern.plcverif.base.models.cfa.trace.declared.Reference
import cern.plcverif.base.models.cfa.trace.declared.TracedArrayType
import cern.plcverif.base.models.cfa.trace.declared.TracedComplexType
import cern.plcverif.base.models.cfa.trace.declared.TracedData
import cern.plcverif.base.models.cfa.trace.declared.TracedElementData
import cern.plcverif.base.models.cfa.trace.declared.TracedFieldData
import cern.plcverif.base.models.cfa.trace.instance.TracedArrayElementVariable
import cern.plcverif.base.models.cfa.trace.instance.TracedArrayVariable
import cern.plcverif.base.models.cfa.trace.instance.TracedSingleVariable
import cern.plcverif.base.models.cfa.trace.instance.TracedVariable
import cern.plcverif.base.models.expr.ElementaryType
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.Type
import java.util.ArrayList
import java.util.Collection
import java.util.HashMap
import java.util.List
import java.util.Map
import java.util.Stack
import org.eclipse.emf.ecore.util.EcoreUtil

final class VariableTraceModel implements IVariableTraceModel {
	List<TracedSingleVariable> variables = new ArrayList<TracedSingleVariable>();
	List<TracedArrayVariable> arrays = new ArrayList<TracedArrayVariable>();
	Map<AbstractVariable, TracedVariable> variableTraces = new HashMap<AbstractVariable, TracedVariable>();
	boolean enumerated = false;
	IPlcverifLogger logger = PlatformLogger.INSTANCE; 
	// TODO the logger may be changed to the VerificationJob's logger

	TracedComplexType rootType = TracedComplexType.createAndRegisterFor(null);

	def List<TracedSingleVariable> getVariables() {
		return variables;
	}

	def List<TracedArrayVariable> getArrays() {
		return arrays;
	}

	def boolean isEnumerated() {
		return enumerated
	}

	def void build(DataStructure rootDataStructure) {
		for (field : rootDataStructure.fields) {
			processField(field, rootType);
		}
	}

	private def void processField(Field field, TracedComplexType parentType) {
		val newFieldData = new TracedFieldData(parentType, field);
		processType(field.type, newFieldData);
	}

	private def dispatch void processType(Type type, TracedData data) {
		throw new UnsupportedOperationException("Unknown data type: " + type.class.name);
	}

	private def dispatch void processType(ElementaryType type, TracedData data) {
		// Flatten and store variable
		TracedComplexType.createAndRegisterFor(data);
		if (data.isArray()) {
			val newArray = new TracedArrayVariable(data);
			data.flattenedInto = newArray;
			arrays.add(newArray);
		} else {
			val newVariable = new TracedSingleVariable(data);
			data.flattenedInto = newVariable;
			variables.add(newVariable);
		}
	}

	private def dispatch void processType(ArrayType type, TracedData data) {
		val newType = TracedArrayType.createAndRegisterFor(data);
		val newElement = new TracedElementData(newType, type);
		processType(type.elementType, newElement);
	}

	private def dispatch void processType(DataStructureRef type, TracedData data) {
		val newType = TracedComplexType.createAndRegisterFor(data);
		for (field : type.definition.fields) {
			processField(field, newType);
		}
	}

	private def dispatch Reference resolveVariableRef(DataRef ref) {
		throw new UnsupportedOperationException("Unknown data reference instance: " + ref.class.name);
	}

	private def dispatch Reference resolveVariableRef(FieldRef ref) {
		if (ref.getPrefix === null) {
			val fieldData = rootType.getField(ref.field)
			if (fieldData === null) {
				throw new IllegalArgumentException("The provided reference is not a global reference.");
			}
			return new Reference(fieldData);
		} else {
			val base = resolveVariableRef(ref.getPrefix);
			if (base.data.tracedType instanceof TracedComplexType == false) {
				throw new IllegalArgumentException(
					"The provided reference is invalid: it refers to a field of an array type.")
			}
			val currentCfaType = base.data.dataType
			if (currentCfaType instanceof DataStructureRef == false) {
				throw new IllegalArgumentException(
					'''The provided reference is invalid: trying to refer to the field of a variable that is not a data structure (type: «currentCfaType.class.name»).''');
			}
			val parentType = base.data.getTracedType as TracedComplexType;
			base.data = parentType.getField(ref.field);
			if (base.data === null) {
				throw new IllegalArgumentException('''The provided reference is invalid: it refers to a field of a complex type that does not own that field. Field:«ref.field.displayName»; type: «(currentCfaType as DataStructureRef).definition.displayName».''');
			}
			return base;
		}
	}

	private def dispatch Reference resolveVariableRef(Indexing ref) {
		if (ref.getPrefix === null) {
			throw new IllegalArgumentException("The root of a variable reference cannot be an array indexing.");
		}

		val base = resolveVariableRef(ref.getPrefix);
		if (base.data.tracedType instanceof TracedArrayType == false) {
			throw new IllegalArgumentException(
				"The provided reference is invalid: it refers to an array element of a complex type.")
		}
		val parentType = base.data.getTracedType as TracedArrayType;
		base.data = parentType.getElement;
		base.indices.push(ref.index);
		return base;
	}

	override AbstractVariableRef transformHierarchicalReference(DataRef dataRef) {
		if (dataRef.type instanceof ElementaryType == false)
			throw new UnsupportedOperationException(
				"Transforming a single reference is valid only for elementary data types. Arrays and data structures should appear only in compound assignments and should be transformed with specific methods.");
		val Reference internalRef = resolveVariableRef(dataRef);
		val TracedVariable tracedVariable = internalRef.data.flattenedInto;
		val ret = createVariableRef(tracedVariable, internalRef.indices);
		ret.frozen = dataRef.frozen;
		return ret;
	}

	private def dispatch AbstractVariableRef createVariableRef(TracedVariable tracedVariable,
		List<Expression> indices) {
		throw new UnsupportedOperationException("Unknown traced variable type: " + tracedVariable.class.name);
	}

	private def dispatch AbstractVariableRef createVariableRef(TracedSingleVariable tracedVariable,
		List<Expression> indices) {
		val ret = CfaInstanceSafeFactory.INSTANCE.createVariableRef(tracedVariable.representedVariable)
		return ret;
	}

	private def dispatch AbstractVariableRef createVariableRef(TracedArrayVariable tracedVariable,
		List<Expression> indices) {
		if (tracedVariable.enumerated) {
			return CfaInstanceSafeFactory.INSTANCE.createVariableRef(
				tracedVariable.getElement(indices).representedVariable
			)
		} else {
			return CfaInstanceSafeFactory.INSTANCE.createArrayElementRef(tracedVariable.representedVariable,
				new ArrayList<Expression>(EcoreUtil.copyAll(indices)))
		}
	}

	def dispatch DataRef recoverHierarchicalReference(VariableRef variableRef) {
		val variableTrace = variableTraces.get(variableRef.variable);
		val dataTrace = variableTrace.flattenedFrom;
		val indices = new Stack<Expression>();
		if (variableTrace instanceof TracedArrayElementVariable) {
			indices.addAll(variableTrace.indexExpressions);
		}
		val ret = createDataRef(dataTrace, indices);
		ret.frozen = variableRef.frozen;
		return ret;
	}

	def dispatch DataRef recoverHierarchicalReference(ArrayElementRef arrayElementRef) {
		val variableTrace = variableTraces.get(arrayElementRef.variable);
		val dataTrace = variableTrace.flattenedFrom;
		val indices = new Stack<Expression>();
		for (index : arrayElementRef.indices) {
			indices.add(EcoreUtil.copy(index));
		}
		val ret = createDataRef(dataTrace, indices);
		ret.frozen = arrayElementRef.frozen;
		return ret;
	}

//	private def DataRef createDataRef(Reference ref) {
//		return createDataRef(ref.data, ref.indices.clone() as Stack<Expression>);
//	}
	private def dispatch FieldRef createDataRef(TracedFieldData fieldData, Stack<Expression> indices) {
		if (fieldData.getParentTracedType.getTypeOf !== null) {
			// Still not the root
			return CfaDeclarationSafeFactory.INSTANCE.appendFieldRef(
				createDataRef(fieldData.getParentTracedType.getTypeOf, indices), fieldData.field)
		} else {
			// This is the root
			return CfaDeclarationSafeFactory.INSTANCE.createFieldRef(fieldData.field)
		}
	}

	private def dispatch Indexing createDataRef(TracedElementData elementData, Stack<Expression> indices) {
		val lastIndex = indices.pop();
		return CfaDeclarationSafeFactory.INSTANCE.appendIndexing(
			createDataRef(elementData.getParentTracedType.getTypeOf, indices), EcoreUtil.copy(lastIndex));
	}

	def void enumerateArrays() {
		if (isEnumerated)
			return;
		for (arrayVariable : arrays) {
			variables.addAll(arrayVariable.enumerateElements())
		}
		enumerated = true;
	}

	def Collection<AbstractVariable> createSimpleVariables(CfaNetworkInstance container) {
		for (variable : variables) {
			val Variable newVar = CfaInstanceSafeFactory.INSTANCE.createVariable(variable.fqn, container,
				EcoreUtil.copy(variable.dataType))
			copyAnnotations(variable.flattenedFrom, newVar);
			copyFrozen(variable.flattenedFrom, newVar);
			variable.representedVariable = newVar
			variableTraces.put(newVar, variable)
		}
		if (!isEnumerated) {
			for (array : arrays) {
				val ArrayVariable newArray = CfaInstanceSafeFactory.INSTANCE.createArrayVariable(array.fqn, container,
					EcoreUtil.copy(array.dataType), array.flattenedFrom.dimensions)
				array.representedVariable = newArray
				variableTraces.put(newArray, array)
			}
		}
		return variableTraces.keySet()
	}

	private def copyAnnotations(TracedData data, Variable newVariable) {
		for (field : data.fields) {
			for (annotation : field.annotations) {
				transformFieldAnnotation(annotation, newVariable);
			}
		}
	}

	private def copyFrozen(TracedData data, Variable variable) {
		var frozen = false;
		for (field : data.fields) {
			frozen = frozen || field.frozen;
		}
		variable.setFrozen(frozen);
	}

	private def dispatch List<Field> getFields(TracedData data) {
		throw new UnsupportedOperationException("Unknown traced data: " + data);
	}

	private def dispatch List<Field> getFields(TracedFieldData data) {
		var List<Field> ret;
		if (data.parentTracedType.typeOf !== null) {
			ret = getFields(data.parentTracedType.typeOf);
		} else {
			ret = new ArrayList<Field>();
		}
		ret.add(data.field);
		return ret;
	}

	private def dispatch List<Field> getFields(TracedElementData data) {
		var List<Field> ret;
		if (data.parentTracedType.typeOf !== null) {
			ret = getFields(data.parentTracedType.typeOf);
		} else {
			ret = new ArrayList<Field>();
		}
		return ret;
	}

	private def dispatch void transformFieldAnnotation(FieldAnnotation annotation, Variable newVariable) {
		logger.logWarning("Unknown field annotation has been found: %s.", annotation.class.name);
	}

	private def dispatch void transformFieldAnnotation(DirectionFieldAnnotation annotation, Variable newVariable) {
		CfaInstanceSafeFactory.INSTANCE.createDirectionVariableAnnotation(newVariable, annotation.direction);
	}

	private def dispatch void transformFieldAnnotation(OriginalDataTypeFieldAnnotation annotation,
		Variable newVariable) {
		// We do not transform original data type annotations, as it can be retrieved by recovering the DataRef 
		// through recoverHierarchicalReference().
	}

	private def dispatch void transformFieldAnnotation(ReturnValueFieldAnnotation annotation, Variable newVariable) {
		CfaInstanceSafeFactory.INSTANCE.createReturnValueVariableAnnotation(newVariable);
	}
	
	private def dispatch void transformFieldAnnotation(InternalGeneratedFieldAnnotation annotation, Variable newVariable) {
		CfaInstanceSafeFactory.INSTANCE.createInternalGeneratedVariableAnnotation(newVariable);
	}
}
