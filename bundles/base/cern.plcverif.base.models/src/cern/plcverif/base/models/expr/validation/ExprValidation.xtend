/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.expr.validation

import cern.plcverif.base.models.cfa.textual.CfaToString
import cern.plcverif.base.models.cfa.validation.IEmfValidation
import cern.plcverif.base.models.expr.BinaryExpression
import cern.plcverif.base.models.expr.BinaryLogicExpression
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.UnaryLogicExpression
import org.eclipse.emf.ecore.EObject

import static extension cern.plcverif.base.models.expr.utils.TypeUtils.*
import cern.plcverif.base.models.cfa.cfadeclaration.VariableAssignment
import cern.plcverif.base.models.expr.BeginningOfCycle
import cern.plcverif.base.models.expr.BoolType
import cern.plcverif.base.models.expr.EndOfCycle
import cern.plcverif.base.common.logging.IPlcverifLogger
import cern.plcverif.base.models.expr.IntType

final class ExprValidation implements IEmfValidation<Expression> {
	IPlcverifLogger logger;
	
	new(IPlcverifLogger logger) {
		this.logger = logger;
	}
	
	override getLogger() {
		return logger;
	}
	
	def dispatch check(EObject e) {}

	def dispatch check(BinaryExpression expr) {
		// Type of two arguments should be the same
		val isSameType = expr.leftOperand.type.dataTypeEquals(expr.rightOperand.type);
		error(isSameType, '''Missing type conversion, «CfaToString.toDiagString(expr)» has operands with different types.''');
	}
	
	def dispatch check(BinaryLogicExpression expr) {
		error(expr.leftOperand.hasBoolType, '''The left operand of «CfaToString.toDiagString(expr)» is not of Boolean type.''');
		error(expr.rightOperand.hasBoolType, '''The right operand of «CfaToString.toDiagString(expr)» is not of Boolean type.''');
		_check(expr as BinaryExpression);
	}
	
	def dispatch check(UnaryLogicExpression expr) {
		error(expr.operand.hasBoolType, '''The operand of «CfaToString.toDiagString(expr)» is not of Boolean type.''');
	}
	
	def dispatch check(VariableAssignment amt) {
		val isSameType = amt.leftValue.type.dataTypeEquals(amt.rightValue.type);
		error(isSameType, '''Missing type conversion, «CfaToString.toDiagString(amt)» assignment has different types on the left and right side.''');
	}
	
	def dispatch check(cern.plcverif.base.models.cfa.cfainstance.VariableAssignment amt) {
		val isSameType = amt.leftValue.type.dataTypeEquals(amt.rightValue.type);
		error(isSameType, '''Missing type conversion, «CfaToString.toDiagString(amt)» assignment has different types on the left and right side.''');
	}
	
	def dispatch check(BeginningOfCycle e) {
		error(e.type instanceof BoolType, "'BeginningOfCycle' must have a 'BoolType' type.");
	}
	
	def dispatch check(EndOfCycle e) {
		error(e.type instanceof BoolType, "'EndOfCycle' must have a 'BoolType' type.");
	}
	
	def dispatch check(IntType type) {
		error(type.bits >= 1, '''The bits of IntType should be at least 1.''');
	}
}