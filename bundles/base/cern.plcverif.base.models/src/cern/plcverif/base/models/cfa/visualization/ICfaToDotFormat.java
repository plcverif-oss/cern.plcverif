/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.models.cfa.visualization;

import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfabase.Transition;
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariable;
import cern.plcverif.base.models.expr.Expression;

public interface ICfaToDotFormat {
	String locationBorderColor(Location location);
	
	String locationFillColor(Location location);
	
	String locationLabel(Location location);
	
	String locationShape(Location location);
	
	String transitionBorderColor(Transition transition);
	
	String transitionLabel(Transition transition);
	
	String labelPostProcessing(String label);
	
	CharSequence expressionToText(Expression expr);
	CharSequence variableToText(AbstractVariable v);
	CharSequence assignmentToText(cern.plcverif.base.models.cfa.cfadeclaration.VariableAssignment amt);
	CharSequence assignmentToText(cern.plcverif.base.models.cfa.cfainstance.VariableAssignment amt);
	CharSequence assumeBoundToText(Expression bnd);
	
	boolean showDataStructure();
	boolean showVariables();
	boolean showAnnotations();
}
