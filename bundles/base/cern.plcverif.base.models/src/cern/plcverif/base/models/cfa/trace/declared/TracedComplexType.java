/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.cfa.trace.declared;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.google.common.base.Preconditions;

import cern.plcverif.base.models.cfa.cfadeclaration.Field;

public final class TracedComplexType extends TracedType {
	private Map<Field, TracedFieldData> fields = new HashMap<>();
	private Map<String, TracedFieldData> nameToFieldMap = new HashMap<>();

	public Collection<TracedFieldData> getFields() {
		return fields.values();
	}

	public TracedFieldData getField(Field field) {
		return fields.get(field);
	}

	public TracedFieldData getField(String fieldName) {
		return nameToFieldMap.get(fieldName);
	}

	public void addField(TracedFieldData fieldData) {
		fields.put(fieldData.getField(), fieldData);
		nameToFieldMap.put(fieldData.getField().getDisplayName(), fieldData);
	}

	@Override
	public void registerChild(TracedData data) {
		Preconditions.checkArgument(data instanceof TracedFieldData, "Array element assigned as child of a complex type.");
		addField((TracedFieldData) data);
	}

	private TracedComplexType(TracedData typeOf) {
		super(typeOf);
	}
	
	public static TracedComplexType createAndRegisterFor(TracedData typeOf) {
		// The super ctor will already take care of the registration
		return new TracedComplexType(typeOf);
	}
}
