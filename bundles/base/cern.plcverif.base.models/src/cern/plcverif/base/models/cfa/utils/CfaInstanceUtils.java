/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.models.cfa.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EObjectSet;
import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfabase.Transition;
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariable;
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariableRef;
import cern.plcverif.base.models.cfa.cfainstance.AssignmentTransition;
import cern.plcverif.base.models.cfa.cfainstance.AutomatonInstance;
import cern.plcverif.base.models.cfa.cfainstance.Call;
import cern.plcverif.base.models.cfa.cfainstance.CallTransition;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.cfa.cfainstance.Variable;
import cern.plcverif.base.models.cfa.cfainstance.VariableAssignment;
import cern.plcverif.base.models.expr.utils.ExprUtils;

public final class CfaInstanceUtils {
	private CfaInstanceUtils() {
		// Utility class.
	}
	
	public static boolean refersTo(AbstractVariableRef ref, Variable variable) {
		return ref.getVariable() == variable;
	}

	public static EObjectSet<AbstractVariableRef> getAssignedVarRefs(AssignmentTransition transition) {
		return transition.getAssignments().stream().map(it -> it.getLeftValue())
				.collect(EObjectSet.collector()); 
	}
	
	public static EObjectSet<AbstractVariableRef> getAssignedByBoth(AssignmentTransition trans1, AssignmentTransition trans2) {
		if (trans1.getAssignments().isEmpty() || trans2.getAssignments().isEmpty()) {
			return new EObjectSet<>();
		}
		
		EObjectSet<AbstractVariableRef> amts1 = getAssignedVarRefs(trans1);
		EObjectSet<AbstractVariableRef> amts2 = getAssignedVarRefs(trans2);
		
		amts1.retainAll(amts2);
		return amts1;
	}
	
	/**
	 * Returns all {@link AbstractVariableRef}s which are assigned either by
	 * transition {@code trans1} or by transition {@code trans2}, but not by
	 * both.
	 */
	public static EObjectSet<AbstractVariableRef> getAssignedByExactlyOneOfThem(AssignmentTransition trans1, AssignmentTransition trans2) {
		EObjectSet<AbstractVariableRef> amts1 = getAssignedVarRefs(trans1);
		EObjectSet<AbstractVariableRef> amts2 = getAssignedVarRefs(trans2);
		
		// amts1 := amts1 \ amts2
		// amts2 := amts2 \ (amts1 \ amts2)
		// amts1 := (amts1 \ amts2) U (amts2 \ (amts1 \ amts2))
		amts1.removeAll(amts2);
		amts2.removeAll(amts1);
		amts1.addAll(amts2);
		return amts1;
	}
	
	/**
	 * Returns all {@link AbstractVariableRef}s which are assigned either by
	 * transition {@code trans1} or by transition {@code trans2} (or both).
	 */
	public static EObjectSet<AbstractVariableRef> getAssignedByAny(AssignmentTransition trans1, AssignmentTransition trans2) {
		EObjectSet<AbstractVariableRef> amts1 = getAssignedVarRefs(trans1);
		EObjectSet<AbstractVariableRef> amts2 = getAssignedVarRefs(trans2);
		
		amts1.addAll(amts2);
		return amts1;
	}
	
	public static Optional<VariableAssignment> getAssignmentOf(AbstractVariableRef leftRef, List<VariableAssignment> assignments) {
		VariableAssignment ret = null;
		for (VariableAssignment va : assignments) {
			if (EcoreUtil.equals(va.getLeftValue(), leftRef)) {
				if (ret != null) {
					throw new AssertionError("Multiple assignments to " + leftRef.getVariable().getDisplayName());
				}
				ret = va;
			}
		}
		
		return Optional.fromNullable(ret);
	}
	
	public static boolean isLocalVar(AbstractVariable var, AutomatonInstance automaton) {
		boolean isUsedLocally = false;
		for (AbstractVariableRef ref : EmfHelper.getAllContentsOfType(automaton.getContainer(), AbstractVariableRef.class, false, it -> it.getVariable() == var)) {
			// Checking all references to 'var'
			
			AutomatonInstance refContext = EmfHelper.getContainerOfType(ref, AutomatonInstance.class);
			if (refContext == automaton) {
				// local use of 'var'
				isUsedLocally = true;
				continue;
			}
			
			// If it is not a local use, check if it is a call to 'automaton'
			Call callContext = EmfHelper.getContainerOfType(ref, Call.class);
			if (callContext != null && callContext.getCalledAutomaton() == automaton) {
				// input assignments should not contain a local variable on the RHS
				for (VariableAssignment inAmt : callContext.getInputAssignments()) {
					if (EmfHelper.getAllContentsOfType(inAmt.getRightValue(), AbstractVariableRef.class, true, it -> it.getVariable() == var).isEmpty() == false) {
						return false;
					}
				}
				
				// output assignments should not contain a local variable on the LHS
				for (VariableAssignment outAmt : callContext.getOutputAssignments()) {
					if (outAmt.getLeftValue().getVariable() == var) {
						return false;
					}
				}
				
				
				// parameter passing to/from the 'automaton'
				isUsedLocally = true;
				continue;
			}
			
			// There is a non-local use of a given variable
			return false; // NOPMD
		}
		
		// No violation found (but probably it is never used, check 'isUsedLocally')
		return isUsedLocally;
	}
	
	/**
	 * Deletes the given transition.
	 * This is an optimised version of the {@link CfaUtils#delete(org.eclipse.emf.ecore.EObject)} which assumes that there are not annotations in the CFA.
	 *
	 * @param t The transition to be deleted. It must be an instance CFA transition.
	 */
	public static void deleteTransitionNoAnnotation(Transition t) {
		Preconditions.checkNotNull(t, "t");
		Preconditions.checkArgument(t instanceof AssignmentTransition || t instanceof CallTransition, "CfaInstanceUtils.deleteTransitionNoAnnotation can only be used for instance CFA transitions.");
		Preconditions.checkArgument(t.getAnnotations().isEmpty(), "deleteTransitionNoAnnotation can only be used on CFA without annotations.");
		CfaUtils.checkNotFrozen(t);
		
		// remove from source and target locations
		if (t.getSource() != null) {
			t.getSource().getOutgoing().remove(t);
		}
		if (t.getTarget() != null) {
			t.getTarget().getIncoming().remove(t);
		}
		
		// remove from container
		EcoreUtil.remove(t);
	}
	
	/**
	 * Deletes the given location.
	 * This is an optimised version of the {@link CfaUtils#delete(org.eclipse.emf.ecore.EObject)} which assumes that there are not annotations in the CFA.
	 *
	 * @param loc The location to be deleted. It must be an instance CFA transition. It shall not be the initial or end location of its parent automaton.
	 */
	public static void deleteLocationNoAnnotation(Location loc) {
		Preconditions.checkNotNull(loc, "loc");
		Preconditions.checkArgument(loc.getAnnotations().isEmpty(), "deleteLocationNoAnnotation can only be used on CFA without annotations.");
		CfaUtils.checkNotFrozen(loc);
		Preconditions.checkNotNull(loc.getParentAutomaton(), "loc.parentAutomaton");
		Preconditions.checkArgument(loc.getParentAutomaton().getInitialLocation() != loc);
		Preconditions.checkArgument(loc.getParentAutomaton().getEndLocation() != loc);
		
		// check if it has connected transitions
		Preconditions.checkArgument(loc.getIncoming().isEmpty(), "The given location has incoming transitions, thus cannot be deleted.");
		Preconditions.checkArgument(loc.getOutgoing().isEmpty(), "The given location has outgoing transitions, thus cannot be deleted.");
				
		// remove from container
		EcoreUtil.remove(loc);
	}
	
	/**
	 * Optimized deletion for {@link VariableAssignment}.
	 * <p>
	 * It relies on the fact that {@link VariableAssignment} objects are never
	 * referenced, only contained.
	 * 
	 * @param amt
	 *            Assignment to be deleted.
	 */
	public static void deleteVariableAssignment(VariableAssignment amt) {
		EcoreUtil.remove(amt);
	}
	

	/**
	 * Returns {@code true} iff the given transition has (1) a constant true literal as guard, and (2) has no assignments attached to it, and (3) has no calls attached to it.
	 * 
	 * Note that this method may not detect all, semantically equivalent transitions, e.g., if the guard is {@code [a OR NOT a]}.
	 */
	public static boolean isEmptyTransition(Transition t) {
		Preconditions.checkArgument(!(t instanceof cern.plcverif.base.models.cfa.cfadeclaration.AssignmentTransition || t instanceof cern.plcverif.base.models.cfa.cfadeclaration.CallTransition),
				"CfaInstanceUtils.isEmptyTransition cannot be used for declaration transitions.");
		
		boolean constantTrueGuard = ExprUtils.isTrueLiteral(t.getCondition());
		
		if (t instanceof AssignmentTransition) {
			AssignmentTransition amtTrans = (AssignmentTransition) t;
			return constantTrueGuard && amtTrans.getAssignments().isEmpty();
		} else if (t instanceof CallTransition) {
			CallTransition callTrans = (CallTransition) t;
			return constantTrueGuard && callTrans.getCalls().isEmpty();
		}
		return false;
	}
	
	/**
	 * Deletes the automaton and all its containments, even if they are frozen.
	 * <p>
	 * Delete will not be explicitly called on each content, only transitions and locations are removed (but their children are not).
	 * <p>
	 * It is assumed that there are no calls to {@code e}. The calls to {@code e} are not looked up and not removed.
	 * @param e The automaton instance to be deleted.
	 */
	public static void deleteAutomatonForced(AutomatonInstance e) {
		// All content can be deleted, even if it is frozen
		
		// Delete the annotations first
		e.getAnnotations().clear();
		e.getTransitions().forEach(it -> {it.getAnnotations().clear(); it.setFrozen(false);});
		e.getLocations().forEach(it -> {it.getAnnotations().clear(); it.setFrozen(false);});
		
		e.setInitialLocation(null);
		e.setEndLocation(null);
		
		(new ArrayList<Location>(e.getLocations())).forEach(it -> {it.getIncoming().clear(); it.getOutgoing().clear(); deleteLocationNoAnnotation(it);});
		(new ArrayList<Transition>(e.getTransitions())).forEach(it -> deleteTransitionNoAnnotation(it));
		
		// NOTE calls to 'e' will not be deleted!
		
		EcoreUtil.remove(e);
		Preconditions.checkState(e.eContainer() == null);
	}
	
	
	/**
	 * Returns all variable assignments contained in the given CFA network instance, satisfying the given predicate.
	 * <p>
	 * Can be considered as an optimized version of {@code EmfHelper.getAllContentsOfType(cfaNetwork, VariableAssignment.class, false, predicate)}.
	 */
	public static List<VariableAssignment> getAllVariableAssignments(CfaNetworkInstance cfaNetwork, Predicate<VariableAssignment> predicate) {
		List<VariableAssignment> ret = new ArrayList<>();
		for (AutomatonInstance automaton : cfaNetwork.getAutomata()) {
			for (Transition transition : automaton.getTransitions()) {
				getAllVariableAssignments(transition).stream().filter(predicate).forEach(ret::add);
			}
		}	
		return ret;
	}
	
	/**
	 * Returns all variable assignments contained in the given instance transition.
	 * <p>
	 * Can be considered as an optimized version of {@code EmfHelper.getAllContentsOfType(transition, VariableAssignment.class, false)}.
	 */
	public static Collection<VariableAssignment> getAllVariableAssignments(Transition transition) {
		if (transition instanceof AssignmentTransition) {
			return ((AssignmentTransition) transition).getAssignments();
		} else if (transition instanceof CallTransition) {
			List<VariableAssignment> ret = new ArrayList<>();
			for (Call call : ((CallTransition) transition).getCalls()) {
				ret.addAll(call.getInputAssignments());
				ret.addAll(call.getOutputAssignments());
			}
			return ret;
		} else {
			throw new UnsupportedOperationException("Unknown instance transition type: " + transition);
		}
	}
}
