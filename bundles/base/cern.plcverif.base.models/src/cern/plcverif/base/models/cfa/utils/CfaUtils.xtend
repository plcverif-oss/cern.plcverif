/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.models.cfa.utils

import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.base.models.cfa.cfabase.Annotation
import cern.plcverif.base.models.cfa.cfabase.ArrayType
import cern.plcverif.base.models.cfa.cfabase.AutomatonBase
import cern.plcverif.base.models.cfa.cfabase.CaseLocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.CfaNetworkBase
import cern.plcverif.base.models.cfa.cfabase.ContinueTransitionAnnotation
import cern.plcverif.base.models.cfa.cfabase.ElseExpression
import cern.plcverif.base.models.cfa.cfabase.ExitTransitionAnnotation
import cern.plcverif.base.models.cfa.cfabase.Freezable
import cern.plcverif.base.models.cfa.cfabase.GotoTransitionAnnotation
import cern.plcverif.base.models.cfa.cfabase.IfLocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.Location
import cern.plcverif.base.models.cfa.cfabase.LocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.LoopLocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.SwitchLocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.Transition
import cern.plcverif.base.models.cfa.cfabase.TransitionAnnotation
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructure
import cern.plcverif.base.models.cfa.cfadeclaration.Field
import cern.plcverif.base.models.cfa.cfadeclaration.VariableAssignment
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariable
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariableRef
import cern.plcverif.base.models.cfa.cfainstance.AssignmentTransition
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.Type
import cern.plcverif.base.models.expr.UnaryLogicExpression
import cern.plcverif.base.models.expr.utils.ExprUtils
import com.google.common.base.Preconditions
import java.util.Collection
import java.util.List
import java.util.stream.Stream
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.util.EcoreUtil

import static cern.plcverif.base.models.cfa.textual.CfaToString.*
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef
import cern.plcverif.base.models.cfa.cfadeclaration.Indexing
import cern.plcverif.base.models.expr.IntLiteral
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructureAnnotation
import cern.plcverif.base.models.cfa.cfabase.AutomatonAnnotation
import cern.plcverif.base.models.cfa.cfadeclaration.FieldAnnotation
import cern.plcverif.base.models.cfa.cfainstance.VariableAnnotation
import cern.plcverif.base.models.cfa.cfabase.Annotable
import cern.plcverif.base.models.cfa.cfainstance.OriginalDataTypeVariableAnnotation
import cern.plcverif.base.models.cfa.cfadeclaration.OriginalDataTypeFieldAnnotation
import cern.plcverif.base.models.cfa.cfadeclaration.DirectionFieldAnnotation
import cern.plcverif.base.models.cfa.cfainstance.DirectionVariableAnnotation
import cern.plcverif.base.models.cfa.cfainstance.ReturnValueVariableAnnotation
import cern.plcverif.base.models.cfa.cfadeclaration.ReturnValueFieldAnnotation
import cern.plcverif.base.models.cfa.cfadeclaration.SourceTypeDataStructureAnnotation
import cern.plcverif.base.models.cfa.cfadeclaration.InternalGeneratedFieldAnnotation
import cern.plcverif.base.models.cfa.cfainstance.Variable
import cern.plcverif.base.models.cfa.cfainstance.InternalGeneratedVariableAnnotation
import cern.plcverif.base.models.cfa.cfabase.AssertionAnnotation
import cern.plcverif.base.models.cfa.cfabase.ArrayDimension
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructureRef
import cern.plcverif.base.models.cfa.cfainstance.ArrayElementRef
import cern.plcverif.base.models.cfa.textual.CfaToString

final class CfaUtils {
	private new() {
		// Utility class.
	}
	
	def static dispatch Collection<? extends AutomatonBase> getAutomata(CfaNetworkBase network) {
		throw new UnsupportedOperationException("Unknown CFA network: " + network);
	}

	def static dispatch Collection<? extends AutomatonBase> getAutomata(CfaNetworkDeclaration network) {
		return network.automata;
	}

	def static dispatch Collection<? extends AutomatonBase> getAutomata(CfaNetworkInstance network) {
		return network.automata;
	}
	
	private static def <T extends Annotation> void checkAnnotationOfCorrectType(Annotable annotable, Class<T> annotationType) {
		switch (annotable) {
			Location: Preconditions.checkArgument(LocationAnnotation.isAssignableFrom(annotationType), '''A Location cannot have annotation of «annotationType.simpleName» as it is not a LocationAnnotation.''')
			Transition: Preconditions.checkArgument(TransitionAnnotation.isAssignableFrom(annotationType), '''A Transition cannot have annotation of «annotationType.simpleName» as it is not a TransitionAnnotation.''')
			AutomatonBase: Preconditions.checkArgument(AutomatonAnnotation.isAssignableFrom(annotationType), '''An automaton cannot have annotation of «annotationType.simpleName» as it is not an AutomatonAnnotation.''')
			Field: Preconditions.checkArgument(FieldAnnotation.isAssignableFrom(annotationType), '''A Field cannot have annotation of «annotationType.simpleName» as it is not a FieldAnnotation.''')
			AbstractVariable: Preconditions.checkArgument(VariableAnnotation.isAssignableFrom(annotationType), '''A variable cannot have annotation of «annotationType.simpleName» as it is not a VariableAnnotation.''')
			DataStructure: Preconditions.checkArgument(DataStructureAnnotation.isAssignableFrom(annotationType), '''A DataStructure cannot have annotation of «annotationType.simpleName» as it is not a DataStructureAnnotation.''')
		}
	}

	/**
	 * Returns the first annotation of {@code annotable} having the given type.
     * 
	 * @param annotable The annotable object whose annotations are checked.
	 * @param annotationType Type of annotation that is searched.
	 * @return The first annotation having the given type, or {@code null} if {@code annotable} has no such annotation.
	 * @throws IllegalArgumentException if the given annotable cannot possibly have an annotation with the given type (e.g., a {@link Transition} cannot have a {@link LocationAnnotation}).  
	 */
	def static <T extends Annotation> T getFirstAnnotationOfType(Annotable annotable, Class<T> annotationType) {
		checkAnnotationOfCorrectType(annotable, annotationType);
		annotationType.cast(annotable.annotations.findFirst[a|annotationType.isInstance(a)]);
	}

	/**
	 * Returns a new list with all annotations of the given annotable having the given type.
	 * 
	 * @param annotable The annotable object whose annotations are checked.
	 * @param annotationType Type of annotation that is searched.
	 * @return List of annotations with the given type. The returned value is never {@code null}.
	 * @throws IllegalArgumentException if the given annotable cannot possibly have an annotation with the given type (e.g., a {@link Transition} cannot have a {@link LocationAnnotation}).  
	 */
	def static <T extends Annotation> List<T> getAllAnnotationsOfType(Annotable annotable, Class<T> annotationType) {
		checkAnnotationOfCorrectType(annotable, annotationType);
		return annotable.annotations.filter(annotationType).map[a|annotationType.cast(a)].toList();
	}

	/**
	 * Returns a stream of all annotations of the given annotable having the given type.
	 * 
	 * @param annotable The annotable object whose annotations are checked.
	 * @param annotationType Type of annotation that is searched.
	 * @return Stream of annotations with the given type.
	 * @throws IllegalArgumentException if the given annotable cannot possibly have an annotation with the given type (e.g., a {@link Transition} cannot have a {@link LocationAnnotation}).  
	 */
	def static <T extends Annotation> Stream<T> getAnnotationStreamOfType(Annotable annotable, Class<T> annotationType) {
		checkAnnotationOfCorrectType(annotable, annotationType);
		return annotable.annotations.stream.filter([it|annotationType.isInstance(it)]).map([it|annotationType.cast(it)]);
	}

	def static Type getBaseType(DataRef arrayRef) {
		Preconditions.checkNotNull(arrayRef, "arrayRef");
		Preconditions.checkArgument(arrayRef.type instanceof ArrayType,
			"Expected a reference to an array, instead got " + toDiagString(arrayRef.type));

		var Type currentType = arrayRef.type;
		while (currentType instanceof ArrayType) {
			currentType = currentType.elementType;
		}

		return currentType;
	}

	/**
	 * Modifies the given element to be frozen.
	 * @return The element given as argument.
	 */
	def static <T extends Freezable> T asFrozen(T elementToFreeze) {
		elementToFreeze.frozen = true;
		return elementToFreeze;
	}
	
	/**
	 * Returns true iff the given location is frozen, initial location of its parent automaton, or end location of its parent automaton.
	 */
	def static boolean isFrozenInitOrEnd(Location loc) {
		return loc.isFrozen() || loc == loc.getParentAutomaton().getInitialLocation() || loc == loc.getParentAutomaton().getEndLocation();
	}

	/**
	 * Deletes the given {@link EObject} using the most optimal method available.
	 * If no specific method is defined for the given class, {@link EcoreUtil#delete} is used.
	 * 
	 * @throws IllegalStateException if the given object is {@link Freezable} and frozen.
	 */
	def static dispatch delete(EObject e) {
		EcoreUtil.delete(e);
	}

	def static dispatch delete(Freezable e) {
		checkNotFrozen(e);
		EcoreUtil.delete(e);
	}

	def static dispatch delete(Expression e) {
		// Based on the model we assume that an expression _cannot be referred_, only contained.
		// Therefore it is unnecessary to look up the cross-references and we can use the cheaper `remove` method.
		EcoreUtil.remove(e);
		Preconditions.checkState(e.eContainer() === null);
	}

	def static dispatch delete(VariableAssignment e) {
		// Based on the model we assume that a variable assignment _cannot be referred_, only contained.
		// Therefore it is unnecessary to look up the cross-references and we can use the cheaper `remove` method.
		checkNotFrozen(e);
		EcoreUtil.remove(e);
		Preconditions.checkState(e.eContainer() === null);
	}

	def static dispatch delete(cern.plcverif.base.models.cfa.cfainstance.VariableAssignment e) {
		// Based on the model we assume that a variable assignment _cannot be referred_, only contained.
		// Therefore it is unnecessary to look up the cross-references and we can use the cheaper `remove` method.
		checkNotFrozen(e);
		EcoreUtil.remove(e);
		Preconditions.checkState(e.eContainer() === null);
	}
	
	def static dispatch delete(AssertionAnnotation e) {
		// Based on the model we assume that an assertion annotation _cannot be referred_, only contained.
		// Therefore it is unnecessary to look up the cross-references and we can use the cheaper `remove` method.
		EcoreUtil.remove(e);
		Preconditions.checkState(e.eContainer() === null);
	}
	
	static def void checkNotFrozen(Freezable e) {
		Preconditions.checkState(!e.isFrozen,
			"It is forbidden to delete frozen objects. Object: " + CfaToString.toDiagString(e));
	}

//	def static dispatch delete(Transition e) {
//		Preconditions.checkState(!e.isFrozen, "It is forbidden to delete frozen objects.");
//		removeReferencingAnnotations(e);
//		EcoreUtil.delete(e);
//	}
//	def static <T extends EObject> deleteEachElement(Collection<T> eCollection) {
//		for (EObject e : new ArrayList<T>(eCollection)) {
//			delete(e);
//			Preconditions.checkState(e.eContainer === null);
//		}
//	}

	/**
	 * Deletes each {@link EObject} in the given collection using {@link EmfHelper#deleteAll}.
	 * 
	 * @throws IllegalStateException if the given object is {@link Freezable} and frozen.
	 */
	def static deleteEachElement(List<? extends EObject> objectsToDelete) {
		for (e : objectsToDelete.filter(Freezable)) {
			checkNotFrozen(e);
		}

		EmfHelper.deleteAll(objectsToDelete);

		for (e : objectsToDelete) {
			Preconditions.checkState(e.eContainer === null);
		}
	}

	def static boolean isProgramStructureAnnotation(Annotation annotation) {
		switch (annotation) {
			LocationAnnotation: return isProgramStructureLocationAnnotation(annotation)
			TransitionAnnotation: return isProgramStructureTransitionAnnotation(annotation)
		}

		return false;
	}

	def static isProgramStructureLocationAnnotation(LocationAnnotation annotation) {
		switch (annotation) {
			IfLocationAnnotation,
			SwitchLocationAnnotation,
			CaseLocationAnnotation,
			LoopLocationAnnotation: return true
		}

		return false;
	}

	def static isProgramStructureTransitionAnnotation(TransitionAnnotation annotation) {
		switch (annotation) {
			GotoTransitionAnnotation,
			ContinueTransitionAnnotation,
			ExitTransitionAnnotation: return true
		}

		return false;
	}

	def static void removeAllProgramStructureAnnotations(CfaNetworkBase cfa) {
		for (automaton : cfa.automata) {
			for (t : automaton.transitions) {
				t.annotations.removeIf([it|it.isProgramStructureTransitionAnnotation]);
			}
			for (l : automaton.locations) {
				l.annotations.removeIf([it|it.isProgramStructureLocationAnnotation]);
			}
		}
	}

	def static void removeAllAnnotationsReferringToVariables(CfaNetworkBase cfa) {
		for (ann : EmfHelper.getAllContentsOfType(cfa, Annotation, false)) {
			if (EmfHelper.getAllContentsOfType(ann, AbstractVariableRef, false).isEmpty == false) {
				CfaUtils.delete(ann);
			}
			if (EmfHelper.getAllContentsOfType(ann, DataRef, false).isEmpty == false) {
				CfaUtils.delete(ann);
			}
		}
	}

	/**
	 * Removes all annotations from the given CFA which refer to any 
	 * other model element. The safe "local" annotations are kept which
	 * refer only primitive data types and their container. The local
	 * annotations need to be explicitly listed in this method.
	 */
	def static void removeAllNonlocalAnnotations(CfaNetworkBase cfa) {
		for (ann : EmfHelper.getAllContentsOfType(cfa, Annotation, false)) {
			switch (ann) {
				OriginalDataTypeFieldAnnotation, OriginalDataTypeVariableAnnotation: {}
				DirectionFieldAnnotation, DirectionVariableAnnotation: {}
				ReturnValueFieldAnnotation, ReturnValueVariableAnnotation: {}
				SourceTypeDataStructureAnnotation: {}
				InternalGeneratedFieldAnnotation, InternalGeneratedVariableAnnotation: {}
				
				default: EcoreUtil.remove(ann)
			}
		}
	}

	/**
	 * Removes all annotations from the given CFA.
	 */
	def static void removeAllAnnotations(CfaNetworkBase cfa) {
		for (ann : EmfHelper.getAllContentsOfType(cfa, Annotation, false)) {
			// we can assume that an annotation may only be referred from 
			// another annotation (besides the containment)
			// TODO document these deletion-related assumptions centrally
			
			EcoreUtil.remove(ann);
		}
	}

	/**
	 * Returns true iff expr2 is 'NOT(expr1)' or 'ELSE', or expr1 is 'NOT(expr2)' or 'ELSE'.
	 */
	static def boolean isNegatedOf(Expression expr1, Expression expr2) {
		return isNegatedOfOneWay(expr1, expr2) || isNegatedOfOneWay(expr2, expr1);
	}

	private static def boolean isNegatedOfOneWay(Expression expr1, Expression expr2) {
		if (expr2 instanceof ElseExpression) {
			return true;
		}

		if (ExprUtils.isUnaryNegation(expr2)) {
			val Expression expr2Operand = (expr2 as UnaryLogicExpression).getOperand();
			return EcoreUtil.equals(expr1, expr2Operand);
		}
		return false;
	}

	/**
	 * Returns true, iff the given transition is an assignment transition (CFD or CFI)
	 * and it has at least one assignment attached to it.
	 */
	def static boolean hasAssignments(Transition transition) {
		if (transition instanceof AssignmentTransition) {
			return transition.getAssignments().isEmpty() == false;
		} else if (transition instanceof cern.plcverif.base.models.cfa.cfadeclaration.AssignmentTransition) {
			return transition.getAssignments().
				isEmpty() == false;
		}
		return false;
	}

	/**
	 * Returns all transitions contained in the given CFA network.
	 * <p>
	 * Can be considered as an optimized version of {@code EmfHelper.getAllContentsOfType(cfaNetwork, Transition.class, false)}.
	 */
	def static Collection<Transition> getAllTransitions(CfaNetworkBase cfaNetwork) {
		val List<Transition> ret = newArrayList();
		for (automaton : cfaNetwork.automata) {
			ret.addAll(automaton.transitions);
		}
		return ret;
	}
	
	def static dispatch boolean dataRefEquals(DataRef dataref1, DataRef dataref2) {
		return false;	
	}
	
	def static dispatch boolean dataRefEquals(Void dataref1, DataRef dataref2) {
		return false;	
	}
	
	def static dispatch boolean dataRefEquals(DataRef dataref1, Void dataref2) {
		return false;	
	}
	
	def static dispatch boolean dataRefEquals(Void dataref1, Void dataref2) {
		return true;	
	}
	
	def static dispatch boolean dataRefEquals(FieldRef dataref1, FieldRef dataref2) {
		return dataref1.field === dataref2.field && dataRefEquals(dataref1.prefix, dataref2.prefix);	
	}
	
	def static dispatch boolean dataRefEquals(Indexing dataref1, Indexing dataref2) {
		if (EcoreUtil.equals(dataref1.dimension, dataref2.dimension)) {
			if (dataRefEquals(dataref1.prefix, dataref2.prefix)) {
				if (EcoreUtil.equals(dataref1.index, dataref2.index)) {
					return true;
				}
				
				// We don't care about differences in the type of indexing expression.		
				if (ExprUtils.isComputableConstant(dataref1.index) && ExprUtils.isComputableConstant(dataref2.index)) {
					val index1 = ExprUtils.literalRepresentation(dataref1.index);
					val index2 = ExprUtils.literalRepresentation(dataref2.index);
					if (index1 instanceof IntLiteral && index2 instanceof IntLiteral) {
						return (index1 as IntLiteral).value == (index2 as IntLiteral).value;
					}
				}
			}
		}
		
		return false;	
	}
	
	/**
	 * Returns true iff the given field contains an {@link InternalGeneratedFieldAnnotation}.
	 */
	def static boolean hasInternalGeneratedAnnotation(Field field) {
		return !field.annotations.filter(InternalGeneratedFieldAnnotation).empty;
	}
	
	/**
	 * Returns true iff the given variable contains an {@link InternalGeneratedVariableAnnotation}.
	 */
	def static boolean hasInternalGeneratedAnnotation(Variable variable) {
		return !variable.annotations.filter(InternalGeneratedVariableAnnotation).empty;
	}
	
	/**
	 * Returns true iff the field referred by the given reference contains
	 * an {@link InternalGeneratedFieldAnnotation}.
	 */
	def static boolean refersToInternalGenerated(DataRef ref) {
		Preconditions.checkNotNull(ref, "ref");
		return hasInternalGeneratedAnnotation(CfaDeclarationUtils.getReferredField(ref));
	}
	
	/**
	 * Returns true iff the given index is valid for the given array dimension.
	 * Any index is taken as valid for an "undefined" dimension.
	 */
	def static boolean isValidIndex(ArrayDimension dimension, long index) {
		Preconditions.checkNotNull(dimension, "dimension");
		return dimension.isValidIndex(index);
	}
	
	
	/**
	 * Returns the size of the given data type in bits. 
	 */
	static def int sizeInBits(Type type) {
		if (type instanceof ArrayType) {
			val elements = Math.max(0,
					type.getDimension().getUpperIndex() - type.getDimension().getLowerIndex() + 1);
			return elements * sizeInBits(type);
		} else if (type instanceof DataStructureRef) {
			return dataStructureSizeInBits(type);
		} else {
			return ExprUtils.sizeInBits(type);
		}
	}

	private static def int dataStructureSizeInBits(DataStructureRef type) {
		var int ret = 0;
		for (Field field : type.getDefinition().getFields()) {
			ret += sizeInBits(field.getType());
		}
		return ret;
	}
	
		/**
	 * Returns true if the two references are in conflict.
	 * They are NOT in conflict if:
	 *  - They are references to different [base] variables,
	 *  - They are both {@link ArrayElementRef}s to the same variable with <b>different constant</b> indexing.
	 */
	def static boolean conflictingReferences(AbstractVariableRef ref1, AbstractVariableRef ref2) {
		if (ref1.getVariable() != ref2.getVariable()) {
			// Different variables --> not in conflict.
			return false;
		}

		// If the two references point to the same variable, it is still possible that they
		// are not in conflict, e.g. a[1] and a[2]. (However a and a[1] ARE in conflict!)
		if (ref1 instanceof ArrayElementRef && ref2 instanceof ArrayElementRef) {
			val ArrayElementRef aref1 = ref1 as ArrayElementRef;
			val ArrayElementRef aref2 = ref2 as ArrayElementRef;

			Preconditions.checkState(aref1.getIndices().size() == aref2.getIndices().size(),
				"Assignment should have the same number of indices.");
			for (var int i = 0; i < aref1.getIndices().size(); i++) {
				val Expression idx1 = aref1.getIndices().get(i);
				val Expression idx2 = aref2.getIndices().get(i);

				if (idx1 instanceof IntLiteral && idx2 instanceof IntLiteral &&
					(idx1 as IntLiteral).getValue() != (idx2 as IntLiteral).getValue()) {
					// fine, carry on
				} else {
					// We have just identified a (potential) conflict
					return true;
				}
			}
			// No conflict was identified
			return false;
		}

		// Two refs to the same variable, or to non-distinct parts of the same array
		return true;
	}

	/**
	 * Returns true if the two references are in conflict.
	 * They are NOT in conflict if:
	 *  - They are references to different fields,
	 *  - They are both {@link ArrayElementRef}s to the same variable with <b>different constant</b> indexing.
	 */
	static def dispatch boolean conflictingCfdReferences(DataRef ref1, DataRef ref2) {
		throw new UnsupportedOperationException();
	}

	static def dispatch boolean conflictingCfdReferences(Void ref1, DataRef ref2) {
		return false;
	}

	static def dispatch boolean conflictingCfdReferences(DataRef ref1, Void ref2) {
		return false;
	}
	
	static def dispatch boolean conflictingCfdReferences(Void ref1, Void ref2) {
		return true;
	}

	static def dispatch boolean conflictingCfdReferences(FieldRef ref1, Indexing ref2) {
		return conflictingCfdReferences(ref1, ref2.prefix);
	}

	static def dispatch boolean conflictingCfdReferences(Indexing ref1, FieldRef ref2) {
		return conflictingCfdReferences(ref2, ref1);
	}

	static def dispatch boolean conflictingCfdReferences(FieldRef ref1, FieldRef ref2) {
		return (ref1.field === ref2.field && conflictingCfdReferences(ref1.prefix, ref2.prefix)) ||
			conflictingCfdReferences(ref1.prefix, ref2) ||
			conflictingCfdReferences(ref1, ref2.prefix);
	}

	static def dispatch boolean conflictingCfdReferences(Indexing ref1, Indexing ref2) {
		if (EcoreUtil.equals(ref1.prefix, ref2.prefix)) {
			if (ExprUtils.isComputableConstant(ref1.index) && ExprUtils.isComputableConstant(ref2.index)) {
				// compute the indices
				val long idx1 = ExprUtils.toLong(ref1.index);
				val long idx2 = ExprUtils.toLong(ref2.index);

				// conflicting iff they refer to the same index
				return idx1 === idx2;
			} else {
				// we cannot be sure that the two indices are different
				return true;
			}
		} else {
			val indexingLen1 = getIndexingLen(ref1);
			val indexingLen2 = getIndexingLen(ref2);			
			
			// Cut the last suffix of the longer reference
			
			if (indexingLen1 > indexingLen2) {
				return conflictingCfdReferences(ref1.prefix, ref2);
			} else if (indexingLen1 < indexingLen2) {
				return conflictingCfdReferences(ref1, ref2.prefix);
			} else {
				return conflictingCfdReferences(ref1.prefix, ref2.prefix);
			}
		}
	}
	
	
	private static def int getIndexingLen(Indexing ref) {
		if (ref.prefix instanceof Indexing) {
			return getIndexingLen(ref.prefix as Indexing) + 1;
		} else {
			return 1;
		}
	}
}
