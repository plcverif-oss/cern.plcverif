/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Vince Molnar - adjustment to updated models
 *******************************************************************************/
package cern.plcverif.base.models.cfa.builder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory;
import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfabase.Transition;
import cern.plcverif.base.models.cfa.cfadeclaration.AssignmentTransition;
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.Call;
import cern.plcverif.base.models.cfa.cfadeclaration.CallTransition;
import cern.plcverif.base.models.expr.Expression;

public final class TransitionBuilder implements IEObjectBuilder {
	Optional<String> name = Optional.empty();
	Optional<Location> source = Optional.empty();
	Optional<Location> target = Optional.empty();
	Optional<Expression> condition = Optional.empty();
	List<VariableAssignmentSkeleton> assignments = new ArrayList<>();
	List<Call> calls = new ArrayList<>();
	
	private static final CfaDeclarationSafeFactory FACTORY = CfaDeclarationSafeFactory.INSTANCE;

	private TransitionBuilder() {
		// intentionally left blank
	}
	
	/**
	 * Creates new CFA declaration transition builder.
	 * @return Transition builder.
	 */
	public static TransitionBuilder builder() {
		return new TransitionBuilder();
	}

	/**
	 * Creates new CFA declaration transition builder that will build
	 * transitions between the two given locations.
	 * 
	 * @param source
	 *            Source location.
	 * @param target
	 *            Target location.
	 * @return Transition builder.
	 */
	public static TransitionBuilder builderBetween(Location source, Location target) {
		return (new TransitionBuilder()).source(source).target(target);
	}
	
	public TransitionBuilder name(String name) {
		Preconditions.checkNotNull(name);
		this.name = Optional.of(name);
		return this;
	}

	public TransitionBuilder source(Location source) {
		Preconditions.checkNotNull(source);
		this.source = Optional.of(source);
		return this;
	}

	public TransitionBuilder target(Location target) {
		Preconditions.checkNotNull(target);
		this.target = Optional.of(target);
		return this;
	}

	public TransitionBuilder condition(Expression condition) {
		Preconditions.checkNotNull(condition);
		EmfHelper.checkNotContained(condition);

		this.condition = Optional.of(condition);
		return this;
	}

	public TransitionBuilder conditionIfNotNull(Expression condition) {
		if (condition != null) {
			condition(condition);
		}
		return this;
	}

	public TransitionBuilder addAssignment(VariableAssignmentSkeleton assignment) {
		Preconditions.checkNotNull(assignment);

		this.assignments.add(assignment);
		return this;
	}

	public TransitionBuilder addAssignments(Collection<VariableAssignmentSkeleton> assignment) {
		Preconditions.checkNotNull(assignment);

		this.assignments.addAll(assignment);
		return this;
	}

	public TransitionBuilder addCall(Call call) {
		Preconditions.checkNotNull(call);
		EmfHelper.checkNotContained(call);

		this.calls.add(call);
		return this;
	}

	@Override
	public Transition build() {
		checkMandatoryParameter(source, "source"); // ensures source.isPresent
		checkMandatoryParameter(target, "target"); // ensures target.isPresent
		Preconditions.checkState(isAssignmentTransition() || isCallTransition());
		Preconditions.checkState(source.get().getParentAutomaton() == target.get().getParentAutomaton(), "Transitions can only be created between locations in the same automaton.");
		Preconditions.checkState(source.get().getParentAutomaton() instanceof AutomatonDeclaration, "This builder is only suitable to create declaration transitions.");
		AutomatonDeclaration parentAutomaton = (AutomatonDeclaration) source.get().getParentAutomaton();

		Transition ret;
		if (isAssignmentTransition()) {
			AssignmentTransition transition = FACTORY.createAssignmentTransition(
					name.orElse("N/A"),
					parentAutomaton, source.get(), target.get(),
					condition.orElse(FACTORY.trueLiteral()));
			this.assignments.forEach(it -> it.createVariableAssignments(transition));
			ret = transition;
		} else if (isCallTransition()) {
			CallTransition transition = FACTORY.createCallTransition(name.orElse("N/A"),
					parentAutomaton, source.get(), target.get(),
					condition.orElse(FACTORY.trueLiteral()));
			transition.getCalls().addAll(calls);
			ret = transition;
		} else {
			throw new CfaBuilderException("Invalid transition.");
		}

		return ret;
	}

	private static void checkMandatoryParameter(Optional<?> parameter, String parameterName) {
		if (!parameter.isPresent()) {
			throw new CfaBuilderException(String.format("Missing mandatory parameter: '%s'", parameterName));
		}
	}

	private boolean isAssignmentTransition() {
		return this.calls.isEmpty();
	}

	private boolean isCallTransition() {
		return this.assignments.isEmpty() && !this.calls.isEmpty();
	}
}
