package cern.plcverif.base.models.cfa.transformation

import cern.plcverif.base.common.emf.EObjectMap
import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructure
import cern.plcverif.base.models.cfa.cfadeclaration.VariableAssignment
import cern.plcverif.base.models.cfa.utils.CfaDeclarationUtils
import com.google.common.base.Preconditions
import java.util.List
import org.eclipse.emf.ecore.util.EcoreUtil

/**
 * Utility class to represent the initial assignments for each field instance in a CFD.
 */
final class CfdInitialAssignmentsToTransition {
	private new() {
		// Utility class.
	}

	static final extension CfaDeclarationSafeFactory FACTORY = CfaDeclarationSafeFactory.INSTANCE;

	/**
	 * Returns the assignments which are necessary to set the initial values for every field instance.
	 * If these assignments are performed at the beginning of the execution, the non-instantiated fields'
	 * initial assignments can be neglected.
	 * The returned variable assignments are all global.  
	 * @param cfd The network to be analysed
	 * @return The list of variable assignments that will set every field instance to their desired initial values.
	 */
	def static List<VariableAssignment> getInstantiatedInitAmts(CfaNetworkDeclaration cfd) {
		val resultAmts = newArrayList();

		/**
		 * Map of priorities.
		 * Key: assigned left value
		 * Value: priority (lower number means higher priority)
		 */
		val amtPriorities = new EObjectMap<DataRef, Integer>();

		val allFieldInstances = CfaDeclarationUtils.getAllFieldInstances(cfd);
		for (fieldRef : allFieldInstances) {

			for (initAmt : fieldRef.field.initialAssignments) {
				if (fieldRef.prefix !== null) {
					val newAmt = EcoreUtil.copy(initAmt);
					newAmt.leftValue = prependDataRef(EcoreUtil.copy(fieldRef.prefix),
						EcoreUtil.copy(newAmt.leftValue));
					addOrOverwriteWithPriority(newAmt, getPriority(initAmt), resultAmts, amtPriorities);
				} else {
					addOrOverwriteWithPriority(EcoreUtil.copy(initAmt), getPriority(initAmt), resultAmts,
						amtPriorities);
				}
			}
		}

		// Runtime check: 
		resultAmts.forEach [ it |
			Preconditions.checkState(CfaDeclarationUtils.isGlobal(it.leftValue, cfd),
				"Internal error: getInstantiatedInitAmts would return non-global assignment.")
		];

		return resultAmts;
	}

	/**
	 * Defines a priority for the variable assignment.
	 */
	private def static int getPriority(VariableAssignment e) {
		// priority is the number of parent data structures
		return EmfHelper.countContainersOfTypeRecursively(e, DataStructure);
	}

	/**
	 * Adds or overwrites the variable assignment in the list, depending on the priorities.
	 * This is needed as a given field instance may have several conflicting default values defined.
	 */
	private def static addOrOverwriteWithPriority(VariableAssignment amtToAdd, int priority,
		List<VariableAssignment> list, EObjectMap<DataRef, Integer> priorities) {
		if (priorities.containsKey(amtToAdd.leftValue)) {
			val previousPriority = priorities.get(amtToAdd.leftValue);
			if (previousPriority > priority) {
				// override
				priorities.put(amtToAdd.leftValue, priority);
				// PERF this list removeIf is not very efficient
				list.removeIf([it|EcoreUtil.equals(it.leftValue, amtToAdd.leftValue)]);
				list.add(amtToAdd);
			}
//			else {
//				println('''Assignment «CfaToString.toDiagString(amtToAdd)» hidden by an already present one with higher priority: «priorities.keySet.findFirst[it | EcoreUtil.equals(it, amtToAdd)]».''');
//			}
		} else {
			// no duplication, no need to remove anything
			priorities.put(amtToAdd.leftValue, priority);
			list.add(amtToAdd);
		}
	}

	/**
	 * Transforms the given network such that the main automaton's will have a
	 * new location and a new transition, preceding the previous initial location. 
	 * This transition will make sure that every field instance has the initial 
	 * values set up correctly. After these assignments, the non-instantiated 
	 * fields' initial values do not need to be represented. 
	 */
	def static void transform(CfaNetworkDeclaration cfd) {
		val resultAmts = getInstantiatedInitAmts(cfd);

		val mainAutomaton = cfd.mainAutomaton;
		val newInitLoc = CfaDeclarationSafeFactory.INSTANCE.createLocation(mainAutomaton.initialLocation.name + "0",
			mainAutomaton);
		val newTransition = CfaDeclarationSafeFactory.INSTANCE.createAssignmentTransition("t", mainAutomaton,
			newInitLoc, mainAutomaton.initialLocation, CfaDeclarationSafeFactory.INSTANCE.trueLiteral);

		for (initAmt : resultAmts) {
			newTransition.assignments.addAll(initAmt);
		}

		mainAutomaton.initialLocation = newInitLoc;
	}
}
