/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.models.cfa.utils;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EquatableEObject;
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariableRef;
import cern.plcverif.base.models.cfa.cfainstance.VariableRef;

/**
 * Subclass of {@link EquatableEObject} that can improve the performance by not
 * using {@link EcoreUtil#equals(Object)} for comparing {@link VariableRef}
 * objects.
 */
public final class EquatableCfiVariableRef extends EquatableEObject {
	public EquatableCfiVariableRef(AbstractVariableRef wrappedEObject) {
		super(wrappedEObject);
	}

	private AbstractVariableRef getWrappedVarRef() {
		EObject result = getWrappedEObject();
		Preconditions.checkState(result instanceof AbstractVariableRef,
				"EquatableCfiVariable should always wrap an AbstractVariableRef");
		return (AbstractVariableRef) result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof EquatableCfiVariableRef) {
			AbstractVariableRef thisWrapped = this.getWrappedVarRef();
			AbstractVariableRef otherWrapped = ((EquatableCfiVariableRef) obj).getWrappedVarRef();

			// Avoid calling EcoreUtil.equals, as it may be an expensive
			// operation
			if (thisWrapped instanceof VariableRef && otherWrapped instanceof VariableRef) {
				// Variable references (but not array refs!) can be compared by
				// their referred variable's reference
				return ((VariableRef) thisWrapped).getVariable() == ((VariableRef) otherWrapped).getVariable();
			} else {
				return EcoreUtil.equals(thisWrapped, otherWrapped);
			}
		}
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		if (getWrappedVarRef() instanceof VariableRef) {
			return getWrappedVarRef().getVariable().hashCode();
		} else {
			return super.hashCode();
		}
	}
}
