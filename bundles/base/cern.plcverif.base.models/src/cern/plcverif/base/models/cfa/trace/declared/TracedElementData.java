/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.cfa.trace.declared;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Preconditions;

import cern.plcverif.base.models.cfa.cfabase.ArrayDimension;
import cern.plcverif.base.models.cfa.cfabase.ArrayType;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.IntLiteral;
import cern.plcverif.base.models.expr.Type;

public class TracedElementData extends TracedData {
	private TracedArrayType parentTracedType;
	private ArrayType arrayType;

	public TracedElementData(TracedArrayType parentType, ArrayType arrayType) {
		Preconditions.checkNotNull(parentType);
		Preconditions.checkNotNull(arrayType);
		this.parentTracedType = parentType;
		this.arrayType = arrayType;
		parentType.setElement(this);
	}

	@Override
	public TracedArrayType getParentTracedType() {
		return parentTracedType;
	}

	public ArrayType getArrayType() {
		return arrayType;
	}

	@Override
	public List<String> getFqn() {
		List<String> ret;
		if (parentTracedType.getTypeOf() == null) {
			ret = new ArrayList<>();
		} else {
			ret = parentTracedType.getTypeOf().getFqn();
		}
		ret.add("*");
		return ret;
	}

	@Override
	public List<String> getFqn(List<Expression> indices) {
		List<String> ret;
		if (parentTracedType.getTypeOf() == null) {
			ret = new ArrayList<>();
		} else {
			ret = parentTracedType.getTypeOf().getFqn(indices.subList(0, indices.size() - 1));
		}
		Expression index = indices.get(indices.size() - 1);
		if (!(index instanceof IntLiteral)) {
			throw new IllegalArgumentException("Non-constant index used when computing fully qualified name of data element.");
		}
		ret.add(Long.toString(((IntLiteral)index).getValue()));
		return ret;
	}

	@Override
	public List<ArrayDimension> getDimensions() {
		List<ArrayDimension> ret;
		if (parentTracedType.getTypeOf() == null) {
			ret = new ArrayList<>();
		} else {
			ret = parentTracedType.getTypeOf().getDimensions();
		}
		ret.add(arrayType.getDimension());
		return ret;
	}

	@Override
	public boolean isArray() {
		return true;
	}

	@Override
	public Type getDataType() {
		return arrayType.getElementType();
	}
}
