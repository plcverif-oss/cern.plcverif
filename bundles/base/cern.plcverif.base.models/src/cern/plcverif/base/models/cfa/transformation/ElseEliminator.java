/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and fixes
 *****************************************************************************/
package cern.plcverif.base.models.cfa.transformation;

import static cern.plcverif.base.models.cfa.utils.CfaUtils.getAutomata;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Preconditions;

import cern.plcverif.base.models.cfa.cfabase.AutomatonBase;
import cern.plcverif.base.models.cfa.cfabase.CfaNetworkBase;
import cern.plcverif.base.models.cfa.cfabase.ElseExpression;
import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfabase.Transition;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.cfa.textual.CfaToString;
import cern.plcverif.base.models.cfa.utils.CfaUtils;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.ExpressionSafeFactory;

/**
 * This class transforms the CFA instance network in such way that else
 * expressions will not be present in the model. It will replace all else
 * expressions with a concrete expression based on the guards of the other
 * transitions leaving the same place.
 */
public final class ElseEliminator implements ICfaInstanceTransformation {
	public static boolean transform(CfaNetworkBase network) {
		boolean changed = false;
		for (AutomatonBase automaton : getAutomata(network)) {
			for (Location location : automaton.getLocations()) {
				changed = eliminateElseOnOutgoingTransitions(location) || changed;
			}
		}
		return changed;
	}

	/**
	 * Replaces the else expression used as guard on the outgoing transitions of
	 * the given location. If none of the location's outgoing transition uses
	 * {@link ElseExpression else expression} as guard, no modification will be
	 * done (and the method returns {@code false}).
	 * 
	 * @param location
	 *            The location whose output transitions will be checked and
	 *            potentially modified
	 * @throws IllegalStateException
	 *             if multiple outgoing transitions contain else expressions
	 * @return True iff there was a modification performed
	 */
	public static boolean eliminateElseOnOutgoingTransitions(Location location) {
		boolean changed = false;
		Transition elseTransition = null;
		List<Expression> conditions = new ArrayList<>();

		for (Transition transition : location.getOutgoing()) {
			if (transition.getCondition() instanceof ElseExpression) {
				Preconditions.checkState(elseTransition == null,
						"Multiple else guards found on outgoing transitions of location "
								+ CfaToString.toDiagString(location));
				elseTransition = transition;
			} else {
				conditions.add(transition.getCondition());
			}
		}

		if (elseTransition != null) {
			CfaUtils.delete(elseTransition.getCondition());
			changed = true;

			if (conditions.isEmpty()) {
				elseTransition.setCondition(ExpressionSafeFactory.INSTANCE.createBoolLiteral(true));
			} else {
				elseTransition.setCondition(ExpressionSafeFactory.INSTANCE
						.and(conditions.stream().map(c -> ExpressionSafeFactory.INSTANCE.neg(c))));
			}
		}

		return changed;
	}

	@Override
	public boolean transformCfi(CfaNetworkInstance cfaToTransform) {
		return ElseEliminator.transform(cfaToTransform);
	}
}
