/******************************************************************************
 * (C) Copyright CERN 2017-2021. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *   Mihaly Dobos-Kovacs - array representation support
 *   Xaver Fink - additions
 *****************************************************************************/
package cern.plcverif.base.models.cfa.transformation

import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory
import cern.plcverif.base.models.cfa.CfaInstanceSafeFactory
import cern.plcverif.base.models.cfa.cfabase.ArrayType
import cern.plcverif.base.models.cfa.cfabase.AssertionAnnotation
import cern.plcverif.base.models.cfa.cfabase.AutomatonAnnotation
import cern.plcverif.base.models.cfa.cfabase.BlockAutomatonAnnotation
import cern.plcverif.base.models.cfa.cfabase.BlockReturnTransitionAnnotation
import cern.plcverif.base.models.cfa.cfabase.CaseLocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.ContinueTransitionAnnotation
import cern.plcverif.base.models.cfa.cfabase.ElseExpression
import cern.plcverif.base.models.cfa.cfabase.ExitTransitionAnnotation
import cern.plcverif.base.models.cfa.cfabase.GotoTransitionAnnotation
import cern.plcverif.base.models.cfa.cfabase.IfLocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.LabelledLocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.LineNumberAnnotation
import cern.plcverif.base.models.cfa.cfabase.Location
import cern.plcverif.base.models.cfa.cfabase.LocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.RepeatLoopLocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.SwitchLocationAnnotation
import cern.plcverif.base.models.cfa.cfabase.Transition
import cern.plcverif.base.models.cfa.cfabase.TransitionAnnotation
import cern.plcverif.base.models.cfa.cfabase.WhileLoopLocationAnnotation
import cern.plcverif.base.models.cfa.cfadeclaration.AssignmentTransition
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.Call
import cern.plcverif.base.models.cfa.cfadeclaration.CallTransition
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructureRef
import cern.plcverif.base.models.cfa.cfadeclaration.ForLoopLocationAnnotation
import cern.plcverif.base.models.cfa.cfadeclaration.Indexing
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariableRef
import cern.plcverif.base.models.cfa.cfainstance.ArrayElementRef
import cern.plcverif.base.models.cfa.cfainstance.ArrayVariable
import cern.plcverif.base.models.cfa.cfainstance.AutomatonInstance
import cern.plcverif.base.models.cfa.cfainstance.Variable
import cern.plcverif.base.models.cfa.cfainstance.VariableAssignment
import cern.plcverif.base.models.cfa.cfainstance.VariableRef
import cern.plcverif.base.models.cfa.trace.CfaInstantiationTrace
import cern.plcverif.base.models.cfa.trace.VariableTraceModel
import cern.plcverif.base.models.cfa.utils.CfaDeclarationUtils
import cern.plcverif.base.models.expr.AtomicExpression
import cern.plcverif.base.models.expr.BeginningOfCycle
import cern.plcverif.base.models.expr.BinaryArithmeticExpression
import cern.plcverif.base.models.expr.BinaryCtlExpression
import cern.plcverif.base.models.expr.BinaryLogicExpression
import cern.plcverif.base.models.expr.BinaryLtlExpression
import cern.plcverif.base.models.expr.ComparisonExpression
import cern.plcverif.base.models.expr.ElementaryType
import cern.plcverif.base.models.expr.EndOfCycle
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.InitialValue
import cern.plcverif.base.models.expr.LeftValue
import cern.plcverif.base.models.expr.LibraryFunction
import cern.plcverif.base.models.expr.Type
import cern.plcverif.base.models.expr.TypeConversion
import cern.plcverif.base.models.expr.UnaryArithmeticExpression
import cern.plcverif.base.models.expr.UnaryCtlExpression
import cern.plcverif.base.models.expr.UnaryLogicExpression
import cern.plcverif.base.models.expr.UnaryLtlExpression
import com.google.common.base.Preconditions
import java.util.HashMap
import java.util.List
import java.util.Map

import static org.eclipse.emf.ecore.util.EcoreUtil.*
import cern.plcverif.base.models.cfa.cfadeclaration.impl.AssignmentTransitionImpl
import cern.plcverif.base.models.expr.TernaryArithmeticExpression

final class CfaInstantiator {	
	private static class AutomatonContext {
		public final DataRef dataContext;
		public final AutomatonDeclaration localAutomaton;
		
		new(DataRef dataContext, AutomatonDeclaration localAutomaton) {
			this.dataContext = dataContext
			this.localAutomaton = localAutomaton
		}
	}
	
	private static class RefInitValuePair {
		DataRef dataRef;
		InitialValue initValue;
		
		private new(DataRef dataRef, InitialValue initValue) {
			this.dataRef = dataRef;
			this.initValue = initValue;
		}
		
		static def RefInitValuePair of(DataRef dataRef, InitialValue initValue) {
			return new RefInitValuePair(dataRef, initValue);
		}
		
		def getDataRef() {
			return dataRef;
		}
		
		def getInitValue() {
			return initValue;
		}
	}
	
	static CfaInstanceSafeFactory factory = CfaInstanceSafeFactory.create(false);
	
	private new() {
		// Utility class.
	}
	
	static def CfaInstantiationTrace transformCfaNetwork(CfaNetworkDeclaration declaration, boolean enumerateArrays) {
		val traceModel = new CfaInstantiationTrace()
		traceModel.declaration = declaration
		traceModel.variableTrace = new VariableTraceModel()
		traceModel.instance = factory.createCfaNetworkInstance(declaration.displayName)
			
		// Transform variables
		traceModel.variableTrace.build(declaration.rootDataStructure)
		
		if (enumerateArrays) {
			traceModel.variableTrace.enumerateArrays()
		}
		traceModel.variableTrace.createSimpleVariables(traceModel.instance)
		
		traceModel.transformAllInitialValues();
		
		// Transform automata
		traceModel.instance.mainAutomaton = traceModel.transformAutomaton(new AutomatonContext(traceModel.declaration.mainContext, traceModel.declaration.mainAutomaton))
				
		return traceModel;
	}
	
	private static def void transformAllInitialValues(CfaInstantiationTrace traceModel) {
		//val List<RefInitValuePair> initialValues = new EObjectList<RefInitValuePair>();
		val List<RefInitValuePair> initialValues = newArrayList();
		
		for (field : traceModel.declaration.rootDataStructure.fields) {
			traceModel.transformInitialValuesOfType(field.type, CfaDeclarationSafeFactory.INSTANCE.createFieldRef(field), initialValues);
			
			for (initialAssignment : field.initialAssignments) {
				val fullRef = copy(initialAssignment.leftValue);
				Preconditions.checkState(initialAssignment.rightValue instanceof InitialValue, 
					"Found initial value that is not a nondeterministic value or a literal.");
				val initialValue = copy(initialAssignment.rightValue as InitialValue);
				
				initialValues.add(RefInitValuePair.of(fullRef, initialValue));
			}
		}
		
		for (pair : initialValues) {
			val varRef = traceModel.variableTrace.transformHierarchicalReference(pair.getDataRef);
			val initialValue = pair.getInitValue;
			
			setInitialValue(varRef, initialValue);
		}
	}
	
	private static def dispatch void setInitialValue(AbstractVariableRef varRef, InitialValue initialValue) {
		throw new UnsupportedOperationException("Unknown variable reference: " + varRef);
	}
	
	private static def dispatch void setInitialValue(VariableRef varRef, InitialValue initialValue) {
		val Variable variable = varRef.variable;
		factory.setInitialValue(variable, initialValue);
	}
	
	private static def dispatch void setInitialValue(ArrayElementRef varRef, InitialValue initialValue) {
		val ArrayVariable variable = varRef.variable;
		val indices = copyAll(varRef.indices).toList();
		factory.createArrayElementInitialValue(variable, indices, initialValue);
	}
	
	private static def dispatch void transformInitialValuesOfType(CfaInstantiationTrace traceModel, Type type, DataRef context, List<RefInitValuePair> initialValues) {
		throw new UnsupportedOperationException("Unknown type: " + type);
	}
	
	private static def dispatch void transformInitialValuesOfType(CfaInstantiationTrace traceModel, DataStructureRef type, DataRef context, List<RefInitValuePair> initialValues) {
		for (field : type.definition.fields) {
			traceModel.transformInitialValuesOfType(field.type, CfaDeclarationSafeFactory.INSTANCE.appendFieldRef(copy(context), field), initialValues);
			
			for (initialAssignment : field.initialAssignments) {
				val fullRef = CfaDeclarationUtils.concatenateDataRefs(copy(context), copy(initialAssignment.leftValue));
				Preconditions.checkState(initialAssignment.rightValue instanceof InitialValue, 
					"Found initial value that is not a nondeterministic value or a literal.");
				val initialValue = copy(initialAssignment.rightValue as InitialValue);
				
				initialValues.add(RefInitValuePair.of(fullRef, initialValue));
			}
		}
	}
	
	private static def dispatch void transformInitialValuesOfType(CfaInstantiationTrace traceModel, ArrayType type, DataRef context, List<RefInitValuePair> initialValues) {
		// Undefined arrays should not be initialized
		Preconditions.checkArgument(type.dimension.defined || type.elementType instanceof ElementaryType);
		
		val lowerIndex = type.dimension.lowerIndex;
		val size = type.dimension.size;	
		for (var i = 0; i < size; i++) {
			val index = i + lowerIndex;
			traceModel.transformInitialValuesOfType(type.elementType, CfaDeclarationSafeFactory.INSTANCE.appendIndexing(copy(context), index), initialValues);
			// Initial values for array elements are handled in in the array field.
		}
	}
	
	private static def dispatch void transformInitialValuesOfType(CfaInstantiationTrace traceModel, ElementaryType type, DataRef context, List<RefInitValuePair> initialValues) {
		// Do nothing, no more fields ahead.
	}
	
	private static def AutomatonInstance transformAutomaton(CfaInstantiationTrace traceModel, AutomatonContext context) {
		var newAutomaton = traceModel.getAutomatonInstance(context.localAutomaton, context.dataContext)
		if (newAutomaton !== null) {
			return newAutomaton
		}
		
		var String newName = context.dataContext.toString();
		if (newName.endsWith(context.localAutomaton.displayName) == false) {
			newName = newName + "." + context.localAutomaton.displayName;
		}
		
		newAutomaton = factory.createAutomatonInstance(newName, traceModel.instance)
		
		// Process locations
		
		val locationMap = new HashMap<Location, Location>()
		for (location : context.localAutomaton.locations) {
			var Location newLocation;
			if (location == context.localAutomaton.initialLocation) {
				newLocation = factory.createInitialLocation(location.displayName, newAutomaton)
			}
			else if (location == context.localAutomaton.endLocation) {
				newLocation = factory.createEndLocation(location.displayName, newAutomaton)
			}
			else {
				newLocation = factory.createLocation(location.displayName, newAutomaton)
			}
			newLocation.frozen = location.frozen
			locationMap.put(location, newLocation)
			traceModel.registerLocationTrace(location, newAutomaton, newLocation)
		}
		
		// Process transitions
		
		val transitionMap = new HashMap<Transition, Transition>()
		for (transition : context.localAutomaton.transitions) {
			val newTransition = traceModel.transformTransition(transition, context, newAutomaton, locationMap);
			newTransition.frozen = transition.frozen
			transitionMap.put(transition, newTransition);
		}
		
		// Process annotations
		
		for (annotation : context.localAutomaton.annotations) {
			traceModel.transformAutomatonAnnotation(context, annotation, newAutomaton);
		}
		
		for (location : context.localAutomaton.locations) {
			for (annotation : location.annotations) {
				traceModel.transformLocationAnnotation(context, annotation, locationMap.get(location), locationMap, transitionMap);
			}
		}
		
		for (transition : context.localAutomaton.transitions) {
			for (annotation : transition.annotations) {
				traceModel.transformTransitionAnnotation(context, annotation, transitionMap.get(transition), locationMap, transitionMap);
			}
		}
		
		traceModel.registerAutomatonInstance(context.localAutomaton, context.dataContext, newAutomaton);
		
		return newAutomaton;
	}
	
	private static dispatch def Transition transformTransition(CfaInstantiationTrace traceModel, Transition transition, AutomatonContext context, AutomatonInstance parentAutomaton, Map<Location, Location> locationMap) {
		throw new UnsupportedOperationException("Unknown transition type: " + transition.class.name)
	}
	
	private static dispatch def Transition transformTransition(CfaInstantiationTrace traceModel, AssignmentTransition transition, AutomatonContext context, AutomatonInstance parentAutomaton, Map<Location, Location> locationMap) {
		val newTransition = factory.createAssignmentTransition(
			transition.displayName,
			parentAutomaton, 
			locationMap.get(transition.source), 
			locationMap.get(transition.target), 
			traceModel.transformExpression(context, transition.condition)
		)
		
		// Process assignments
		for (assignment : transition.assignments) {
			traceModel.transformAssignment(assignment.leftValue, context, assignment.rightValue, context, assignment.frozen, newTransition.assignments)
		}
		
		for (assumeBound : transition.assumeBounds) {
			traceModel.transformAssumeBound(assumeBound, context, newTransition.assumeBounds)
		}
		
		return newTransition
	}
	
	private static dispatch def Transition transformTransition(CfaInstantiationTrace traceModel, CallTransition transition, AutomatonContext context, AutomatonInstance parentAutomaton, Map<Location, Location> locationMap) {
		val newTransition = factory.createCallTransition(
			transition.displayName,
			parentAutomaton, 
			locationMap.get(transition.source), 
			locationMap.get(transition.target), 
			traceModel.transformExpression(context, transition.condition)
		)
		
		// Process calls
		for (call : transition.calls) {
			traceModel.transformCall(context, call, newTransition)
		}
		
		return newTransition
	}
	
	private def static void transformCall(CfaInstantiationTrace traceModel, AutomatonContext callerContext, Call call, cern.plcverif.base.models.cfa.cfainstance.CallTransition transition) {
		var calleeDataContext = globalizeIfLocal(callerContext, call.calleeContext)
		Preconditions.checkState(traceModel.declaration.isGlobal(calleeDataContext))
		val calleeContext = new AutomatonContext(calleeDataContext, call.calledAutomaton)
		val newCalledAutomaton = traceModel.transformAutomaton(calleeContext)
		val newCall = factory.createCall(transition, newCalledAutomaton)
		
		// Process inputs
		for (inputAssignment : call.inputAssignments) {
			traceModel.transformAssignment(inputAssignment.leftValue, calleeContext, inputAssignment.rightValue, callerContext, inputAssignment.frozen, newCall.inputAssignments)
		}		
		
		// Process outputs
		for (outputAssignment : call.outputAssignments) {
			traceModel.transformAssignment(outputAssignment.leftValue, callerContext, outputAssignment.rightValue, calleeContext, outputAssignment.frozen, newCall.outputAssignments)
		}
	}
	
	private def static void transformAssignment(CfaInstantiationTrace traceModel, DataRef leftValue, AutomatonContext leftContext, Expression rightValue, AutomatonContext rightContext, boolean frozen, List<VariableAssignment> assignments) {
		if (rightValue instanceof DataRef) {
			traceModel.transformDirectAssignment(globalizeIfLocal(leftContext, leftValue), globalizeIfLocal(rightContext, rightValue), frozen, assignments)
		}
		else {	
			val newAssignment = factory.createAssignment(
					traceModel.variableTrace.transformHierarchicalReference(
						globalizeIfLocal(leftContext, leftValue)
					), 
					traceModel.transformExpression(rightContext, rightValue)
				)
				newAssignment.frozen = frozen
			assignments.add(newAssignment)
		}		
	}
	
	private def static void transformAssumeBound(CfaInstantiationTrace traceModel, Expression boundExpr, AutomatonContext boundContext, List<Expression> bounds) {
		val newBound = factory.createAssumeBound(
					traceModel.transformExpression(boundContext, boundExpr)
				)
			bounds.add(newBound)	
	}
	
	private def static void transformDirectAssignment(CfaInstantiationTrace traceModel, DataRef leftValue, DataRef rightValue, boolean frozen, List<VariableAssignment> assignments) {
		val lhsType = leftValue.type
		val rhsType = rightValue.type
		Preconditions.checkArgument(lhsType.dataTypeEquals(rhsType), "Invalid assignment found during instantiation: different types.")
		Preconditions.checkArgument(traceModel.declaration.isGlobal(leftValue))
		Preconditions.checkArgument(traceModel.declaration.isGlobal(rightValue))
		
		if (lhsType instanceof ElementaryType /* || 
			(
				!traceModel.variableTrace.enumerated &&
				lhsType instanceof ArrayType &&
				(lhsType as ArrayType).elementType instanceof ElementaryType
			)*/
		) {
			val newAssignment = factory.createAssignment(
					traceModel.variableTrace.transformHierarchicalReference(leftValue),
					traceModel.variableTrace.transformHierarchicalReference(rightValue)
				)
			assignments.add(newAssignment)
		} 
		else if (lhsType instanceof ArrayType) {
			Preconditions.checkArgument(lhsType.dimension.defined, "Cannot instantiate elements of an array of undefined size")
			for (var int i = 0; i < lhsType.dimension.size; i++) {
				val lhsIndex = i + lhsType.dimension.lowerIndex;
				val rhsIndex = lhsIndex;
				val newLhsRef = CfaDeclarationSafeFactory.INSTANCE.appendIndexing(copy(leftValue), lhsIndex);
				val newRhsRef = CfaDeclarationSafeFactory.INSTANCE.appendIndexing(copy(rightValue), rhsIndex);
				traceModel.transformDirectAssignment(
					newLhsRef,
					newRhsRef,
					frozen,
					assignments
				)
			}
		}
		else if (lhsType instanceof DataStructureRef) {
			for (field : lhsType.definition.fields) {
				val newLhsRef = CfaDeclarationSafeFactory.INSTANCE.appendFieldRef(copy(leftValue), field);
				val newRhsRef = CfaDeclarationSafeFactory.INSTANCE.appendFieldRef(copy(rightValue), field);
				traceModel.transformDirectAssignment(
					newLhsRef,
					newRhsRef,
					frozen,
					assignments
				)
			}
		}
		else {
			throw new UnsupportedOperationException("Unknown type: " + lhsType.class.name) 
		}
	}
	
	def static Expression transformExpression(CfaInstantiationTrace trace, Expression expr) {
		return transformExpression(trace, null, expr);
	}
	
	private def static dispatch Expression transformExpression(CfaInstantiationTrace trace, AutomatonContext context, Expression expression) {
		throw new UnsupportedOperationException("Unknown expression type: " + expression)
	}
	
	private def static dispatch Expression transformExpression(CfaInstantiationTrace trace, AutomatonContext context, ElseExpression expression) {
		return copy(expression)
	}
	
	private def static dispatch Expression transformExpression(CfaInstantiationTrace trace, AutomatonContext context, BeginningOfCycle expression) {
		return copy(expression)
	}
	
	private def static dispatch Expression transformExpression(CfaInstantiationTrace trace, AutomatonContext context, EndOfCycle expression) {
		return copy(expression)
	}
	
	private def static dispatch Expression transformExpression(CfaInstantiationTrace trace, AutomatonContext context, UnaryArithmeticExpression expression) {
		return factory.createUnaryArithmeticExpression(
			transformExpression(trace, context, expression.operand),
			expression.operator
		)
	}
	
	private def static dispatch Expression transformExpression(CfaInstantiationTrace trace, AutomatonContext context, BinaryArithmeticExpression expression) {
		return factory.createBinaryArithmeticExpression(
			transformExpression(trace, context, expression.leftOperand),
			transformExpression(trace, context, expression.rightOperand),
			expression.operator
		)
	}
	
	private def static dispatch Expression transformExpression(CfaInstantiationTrace trace, AutomatonContext context, TernaryArithmeticExpression expression) {
		return factory.createTernaryArithmeticExpression(
			transformExpression(trace, context, expression.left),
			transformExpression(trace, context, expression.middle),
			transformExpression(trace, context, expression.right),
			expression.operator
		)
	}
	
	private def static dispatch Expression transformExpression(CfaInstantiationTrace trace, AutomatonContext context, ComparisonExpression expression) {
		return factory.createComparisonExpression(
			transformExpression(trace, context, expression.leftOperand),
			transformExpression(trace, context, expression.rightOperand),
			expression.operator
		)
	}
	
	private def static dispatch Expression transformExpression(CfaInstantiationTrace trace, AutomatonContext context, UnaryLogicExpression expression) {
		return factory.createUnaryLogicExpression(
			transformExpression(trace, context, expression.operand),
			expression.operator
		)
	}
	
	private def static dispatch Expression transformExpression(CfaInstantiationTrace trace, AutomatonContext context, BinaryLogicExpression expression) {
		return factory.createBinaryLogicExpression(
			transformExpression(trace, context, expression.leftOperand),
			transformExpression(trace, context, expression.rightOperand),
			expression.operator
		)
	}
	
	private def static dispatch Expression transformExpression(CfaInstantiationTrace trace, AutomatonContext context, TypeConversion expression) {
		return factory.createTypeConversion(
			transformExpression(trace, context, expression.operand),
			copy(expression.type)
		)
	}
	
	private def static dispatch Expression transformExpression(CfaInstantiationTrace trace, AutomatonContext context, UnaryCtlExpression expression) {
		Preconditions.checkArgument(context === null, "Temporal expressions are not allowed in CFAs!")
		return factory.createUnaryCtlExpression(
			transformExpression(trace, context, expression.operand),
			expression.operator
		)
	}
	
	private def static dispatch Expression transformExpression(CfaInstantiationTrace trace, AutomatonContext context, BinaryCtlExpression expression) {
		Preconditions.checkArgument(context === null, "Temporal expressions are not allowed in CFAs!")
		return factory.createBinaryCtlExpression(
			transformExpression(trace, context, expression.leftOperand),
			transformExpression(trace, context, expression.rightOperand),
			expression.operator
		)
	}
	
	private def static dispatch Expression transformExpression(CfaInstantiationTrace trace, AutomatonContext context, UnaryLtlExpression expression) {
		Preconditions.checkArgument(context === null, "Temporal expressions are not allowed in CFAs!")
		return factory.createUnaryLtlExpression(
			transformExpression(trace, context, expression.operand),
			expression.operator
		)
	}
	
	private def static dispatch Expression transformExpression(CfaInstantiationTrace trace, AutomatonContext context, BinaryLtlExpression expression) {
		Preconditions.checkArgument(context === null, "Temporal expressions are not allowed in CFAs!")
		return factory.createBinaryLtlExpression(
			transformExpression(trace, context, expression.leftOperand),
			transformExpression(trace, context, expression.rightOperand),
			expression.operator
		)
	}
	
	private def static dispatch Expression transformExpression(CfaInstantiationTrace trace, AutomatonContext context, LibraryFunction expression) {
		val operandsCfi = expression.operands.map[it | transformExpression(trace, context, it)].toList;
		val ret = factory.createLibraryFunction(operandsCfi, expression.function, copy(expression.type));
		return ret;
	}
	
	private def static dispatch Expression transformExpression(CfaInstantiationTrace trace, AutomatonContext context, LeftValue expression) {
		throw new UnsupportedOperationException("Unknown left value type: " + expression.class.name)
	}
	
	private def static dispatch Expression transformExpression(CfaInstantiationTrace trace, AutomatonContext context, AtomicExpression expression) {
		return copy(expression)
	}
	
	private def static dispatch Expression transformExpression(CfaInstantiationTrace trace, AutomatonContext context, DataRef expression) {
		var ref = expression;
		if (context !== null) {
			ref = globalizeIfLocal(context, ref);
		}
		if (trace.declaration.isGlobal(ref) == false) {
			throw new IllegalArgumentException("Unknown reference encountered while transforming expression: " + ref.toString())
		}
		transformDataRef(trace, context, ref)
		trace.variableTrace.transformHierarchicalReference(ref)
	}
	
	private def static dispatch void transformDataRef(CfaInstantiationTrace trace, AutomatonContext context, DataRef dataRef) {}
	private def static dispatch void transformDataRef(CfaInstantiationTrace trace, AutomatonContext context, Indexing indexing) {
		indexing.index = transformExpression(trace, context, indexing.index)
		transformDataRef(trace, context, indexing.prefix)
	}
	
	private static def DataRef globalizeIfLocal(AutomatonContext context, DataRef ref) {
		if (context.localAutomaton.isLocal(ref)) {
			return CfaDeclarationUtils.concatenateDataRefs(copy(context.dataContext), copy(ref))
		}
		return copy(ref)
	}
	
	private def static dispatch void transformAutomatonAnnotation(CfaInstantiationTrace traceModel, AutomatonContext context, AutomatonAnnotation annotation, AutomatonInstance newInstance) {
		throw new UnsupportedOperationException("Unknown automaton annotation: " + annotation);
	}
	
	private def static dispatch void transformAutomatonAnnotation(CfaInstantiationTrace traceModel, AutomatonContext context, BlockAutomatonAnnotation annotation, AutomatonInstance newInstance) {
		factory.createBlockAutomatonAnnotation(newInstance, annotation.blockType, annotation.name, annotation.stateful);
	}
	
	private def static dispatch void transformLocationAnnotation(CfaInstantiationTrace traceModel, AutomatonContext context, LocationAnnotation annotation, Location newLocation, Map<Location, Location> locationMap, HashMap<Transition, Transition> transitionMap) {
		throw new UnsupportedOperationException("Unknown location annotation: " + annotation);
	}
	
	private def static dispatch void transformLocationAnnotation(CfaInstantiationTrace traceModel, AutomatonContext context, IfLocationAnnotation annotation, Location newLocation, Map<Location, Location> locationMap, HashMap<Transition, Transition> transitionMap) {
		factory.createIfLocationAnnotation(newLocation, locationMap.get(annotation.endsAt), transitionMap.get(annotation.then));
	}
	
	private def static dispatch void transformLocationAnnotation(CfaInstantiationTrace traceModel, AutomatonContext context, SwitchLocationAnnotation annotation, Location newLocation, Map<Location, Location> locationMap, HashMap<Transition, Transition> transitionMap) {
		val newAnnotation = factory.createSwitchLocationAnnotation(newLocation, locationMap.get(annotation.endsAt), transformExpression(traceModel, context, annotation.selectionExpr));
		for (caseAnnotation : newAnnotation.cases) {
			factory.createCaseLocationAnnotation(locationMap.get(caseAnnotation.parentLocation), newAnnotation);
		}
	}
	
	private def static dispatch void transformLocationAnnotation(CfaInstantiationTrace traceModel, AutomatonContext context, CaseLocationAnnotation annotation, Location newLocation, Map<Location, Location> locationMap, HashMap<Transition, Transition> transitionMap) {
		// Case annotations are processed with the switch annotation
	}
	
	private def static dispatch void transformLocationAnnotation(CfaInstantiationTrace traceModel, AutomatonContext context, WhileLoopLocationAnnotation annotation, Location newLocation, Map<Location, Location> locationMap, HashMap<Transition, Transition> transitionMap) {
		factory.createWhileLoopLocationAnnotation(
			newLocation,
			transitionMap.get(annotation.loopBodyEntry),
			transitionMap.get(annotation.loopNextCycle),
			transitionMap.get(annotation.loopExit)
		)
	}
	
	private def static dispatch void transformLocationAnnotation(CfaInstantiationTrace traceModel, AutomatonContext context, RepeatLoopLocationAnnotation annotation, Location newLocation, Map<Location, Location> locationMap, HashMap<Transition, Transition> transitionMap) {
		factory.createRepeatLoopLocationAnnotation(
			newLocation,
			transitionMap.get(annotation.loopBodyEntry),
			transitionMap.get(annotation.loopNextCycle),
			transitionMap.get(annotation.loopExit)
		)
	}
	
	private def static dispatch void transformLocationAnnotation(CfaInstantiationTrace traceModel, AutomatonContext context, LabelledLocationAnnotation annotation, Location newLocation, Map<Location, Location> locationMap, HashMap<Transition, Transition> transitionMap) {
		factory.createLabelledLocationAnnotation(newLocation, annotation.labelName);
	}
	
	private def static dispatch void transformLocationAnnotation(CfaInstantiationTrace traceModel, AutomatonContext context, LineNumberAnnotation annotation, Location newLocation, Map<Location, Location> locationMap, HashMap<Transition, Transition> transitionMap) {
		if (annotation.fileName === null) {
			factory.createLineNumberAnnotation(newLocation, annotation.lineNumber);
		} else {
			factory.createLineNumberAnnotation(newLocation, annotation.fileName, annotation.lineNumber);
		}
	}
	
	private def static dispatch void transformLocationAnnotation(CfaInstantiationTrace traceModel, AutomatonContext context, AssertionAnnotation annotation, Location newLocation, Map<Location, Location> locationMap, HashMap<Transition, Transition> transitionMap) {
		factory.createAssertionAnnotation(newLocation, transformExpression(traceModel, context, annotation.invariant));
	}
	
	private def static dispatch void transformLocationAnnotation(CfaInstantiationTrace traceModel, AutomatonContext context, ForLoopLocationAnnotation annotation, Location newLocation, Map<Location, Location> locationMap, HashMap<Transition, Transition> transitionMap) {
		factory.createForLoopLocationAnnotation(
			newLocation,
			transitionMap.get(annotation.loopBodyEntry),
			transitionMap.get(annotation.loopNextCycle),
			transitionMap.get(annotation.loopExit),
			traceModel.variableTrace.transformHierarchicalReference(
				globalizeIfLocal(context, annotation.loopVariable)
			),
			transitionMap.get(annotation.loopVariableInit),
			transitionMap.get(annotation.loopVariableIncrement)
		);
	}
	
	private def static dispatch void transformTransitionAnnotation(CfaInstantiationTrace traceModel, AutomatonContext context, TransitionAnnotation annotation, Transition newTransition, Map<Location, Location> locationMap, HashMap<Transition, Transition> transitionMap) {
		throw new UnsupportedOperationException("Unknown transition annotation: " + annotation);
	}
	
	private def static dispatch void transformTransitionAnnotation(CfaInstantiationTrace traceModel, AutomatonContext context, GotoTransitionAnnotation annotation, Transition newTransition, Map<Location, Location> locationMap, HashMap<Transition, Transition> transitionMap) {
		factory.createGotoTransitionAnnotation(newTransition, annotation.targetLabel);
	}
	
	private def static dispatch void transformTransitionAnnotation(CfaInstantiationTrace traceModel, AutomatonContext context, ContinueTransitionAnnotation annotation, Transition newTransition, Map<Location, Location> locationMap, HashMap<Transition, Transition> transitionMap) {
		factory.createContinueTransitionAnnotation(newTransition);
	}
	
	private def static dispatch void transformTransitionAnnotation(CfaInstantiationTrace traceModel, AutomatonContext context, ExitTransitionAnnotation annotation, Transition newTransition, Map<Location, Location> locationMap, HashMap<Transition, Transition> transitionMap) {
		factory.createExitTransitionAnnotation(newTransition);
	}
	
	private def static dispatch void transformTransitionAnnotation(CfaInstantiationTrace traceModel, AutomatonContext context, BlockReturnTransitionAnnotation annotation, Transition newTransition, Map<Location, Location> locationMap, HashMap<Transition, Transition> transitionMap) {
		factory.createBlockReturnTransitionAnnotation(newTransition);
	}
}
