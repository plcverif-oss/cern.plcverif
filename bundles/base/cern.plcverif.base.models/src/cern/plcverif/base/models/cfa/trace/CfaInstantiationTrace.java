/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - support for EoC location
 *****************************************************************************/
package cern.plcverif.base.models.cfa.trace;

import java.util.HashMap;
import java.util.Map;

import cern.plcverif.base.common.emf.EObjectMap;
import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef;
import cern.plcverif.base.models.cfa.cfainstance.AutomatonInstance;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;

public final class CfaInstantiationTrace {
	private CfaNetworkDeclaration declaration = null;
	private CfaNetworkInstance instance = null;
	private VariableTraceModel variableTrace = null;
	private Map<AutomatonDeclaration, Map<DataRef, AutomatonInstance>> automatonInstances = new HashMap<>();
	private Map<Location, Map<AutomatonInstance, Location>> locationTrace = new HashMap<>();

	public CfaNetworkDeclaration getDeclaration() {
		return declaration;
	}

	public void setDeclaration(CfaNetworkDeclaration declaration) {
		this.declaration = declaration;
	}

	public CfaNetworkInstance getInstance() {
		return instance;
	}

	public void setInstance(CfaNetworkInstance instance) {
		this.instance = instance;
	}

	public VariableTraceModel getVariableTrace() {
		return variableTrace;
	}

	public void setVariableTrace(VariableTraceModel variableTrace) {
		this.variableTrace = variableTrace;
	}

	public void registerAutomatonInstance(AutomatonDeclaration declaration, DataRef context, AutomatonInstance instance) {
		Map<DataRef, AutomatonInstance> instances = automatonInstances.getOrDefault(declaration, null);
		if (instances == null) {
			instances = new EObjectMap<>();
			automatonInstances.put(declaration, instances);
		}
		instances.put(context, instance);
	}

	public AutomatonInstance getAutomatonInstance(AutomatonDeclaration declaration, DataRef context) {
		Map<DataRef, AutomatonInstance> map = automatonInstances.get(declaration);
		if (map == null) {
			return null;
		}
		return map.get(context);
	}

	public void registerLocationTrace(Location declaration, AutomatonInstance automatonInstance, Location instance) {
		Map<AutomatonInstance, Location> instances = locationTrace.getOrDefault(declaration, null);
		if (instances == null) {
			instances = new HashMap<>();
			locationTrace.put(declaration, instances);
		}
		instances.put(automatonInstance, instance);
	}

	public Location getInstanceOf(Location declaration, AutomatonInstance automatonInstance) {
		Map<AutomatonInstance, Location> map = locationTrace.get(declaration);
		if (map == null) {
			return null;
		}
		return map.get(automatonInstance);
	}
}
