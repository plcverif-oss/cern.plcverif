package cern.plcverif.base.models.cfa.exception;

public class InvalidUndefinedArrayOperation extends Exception {
	private static final long serialVersionUID = 1546491637962866819L;
	
	public InvalidUndefinedArrayOperation(String message) {
		super(message);
	}

}
