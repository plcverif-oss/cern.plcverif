/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Vince Molnar - adjustment to updated models
 *******************************************************************************/
package cern.plcverif.base.models.cfa.builder;

import java.security.PublicKey;

import org.eclipse.emf.ecore.util.EcoreUtil;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory;
import cern.plcverif.base.models.cfa.cfadeclaration.AssignmentTransition;
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef;
import cern.plcverif.base.models.expr.Expression;

public final class VariableAssignmentSkeleton {
	private DataRef leftValue;
	private Expression rightValue;

	private VariableAssignmentSkeleton(DataRef leftValue, Expression rightValue) {
		this.leftValue = leftValue;
		this.rightValue = rightValue;

		EmfHelper.checkNotContained(leftValue);
		EmfHelper.checkNotContained(rightValue);
		
	}

	public Expression getRightValue() {
		return rightValue;
	}

	public static VariableAssignmentSkeleton create(DataRef leftValue, Expression rightValue) {
		return new VariableAssignmentSkeleton(leftValue, rightValue);
	}

	public void createVariableAssignments(AssignmentTransition targetTransition) {
		if (rightValue.getType().dataTypeEquals(leftValue.getType()) == false) {
			rightValue = CfaDeclarationSafeFactory.INSTANCE.createTypeConversion(rightValue,
					EcoreUtil.copy(leftValue.getType()));
		}

		CfaDeclarationSafeFactory.INSTANCE.createAssignment(targetTransition, leftValue, rightValue);
	}
}