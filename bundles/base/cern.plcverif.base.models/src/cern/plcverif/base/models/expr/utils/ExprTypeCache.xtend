/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.expr.utils

import cern.plcverif.base.models.expr.BinaryExpression
import cern.plcverif.base.models.expr.BinaryLogicExpression
import cern.plcverif.base.models.expr.Literal
import cern.plcverif.base.models.expr.TemporalBoolType
import cern.plcverif.base.models.expr.Type
import cern.plcverif.base.models.expr.Typed
import cern.plcverif.base.models.expr.UnaryExpression
import java.util.HashMap

import static extension cern.plcverif.base.models.expr.utils.TypeUtils.*
import cern.plcverif.base.models.expr.ExplicitlyTyped
import cern.plcverif.base.models.expr.TypeConversion

final class ExprTypeCache {
	HashMap<Typed, Type> typeCache = newHashMap();

	def Type getType(Typed e) {
		val cachedValue = typeCache.getOrDefault(e, null);
		if (cachedValue !== null) {
			// data can be served from cache
			return cachedValue;
		} else {
			val fetchedType = if (e instanceof ExplicitlyTyped) e.type else fetchType(e);
			if (canBeCached(e, fetchedType)) {
				typeCache.put(e, fetchedType);
			}
			return fetchedType;
		}
	}

	private def canBeCached(Typed e, Type fetchedType) {
		if (TypeUtils.isUnknownType(fetchedType)) {
			return false;
		}

		switch (e) {
			Literal, ExplicitlyTyped: {
				// no need to cache literals and explicitly typed expressions
				return false;
			}
			BinaryExpression: {
				//return typeCache.containsKey(e.leftOperand) && typeCache.containsKey(e.rightOperand);
				return e.leftOperand !== null && e.rightOperand !== null;
			}
			UnaryExpression: {
				//return typeCache.containsKey(e.operand);
				return e.operand !== null;
			}
			default:
				return false
		}
	}

	private def dispatch fetchType(Typed typed) {
		return typed.type;
	}
	
	private def dispatch fetchType(BinaryLogicExpression e) {
		val leftType = this.getType(e.leftOperand)
		if (leftType.isUnknownType) {
			return leftType;
		}

		val rightType = this.getType(e.rightOperand)
		if (rightType.isUnknownType) {
			return rightType;
		}

		if (leftType.isBoolType && rightType.isBoolType) {
			return leftType;
		} else if ((leftType instanceof TemporalBoolType && rightType instanceof TemporalBoolType)) {
			return if (rightType.isBoolType) leftType else rightType; 
		} else 
		throw new IllegalStateException("Invalid types for operands of binary boolean expression.")
	}
	
	private def dispatch fetchType(UnaryExpression e) {
		if (e instanceof ExplicitlyTyped) {
			return e.type;
		} else {
			return this.getType(e.operand);
		}
	}
	
	private def dispatch fetchType(TypeConversion e) {
		return e.type;
	}
	
	private def dispatch fetchType(BinaryExpression e) {
		val leftType = this.getType(e.leftOperand);
		
		// Handle unknown types
		if (leftType.isUnknownType) {
			return leftType;
		}
		
		val rightType = this.getType(e.rightOperand);
		if (rightType.isUnknownType) {
			return rightType;
		}

		if (leftType.dataTypeEquals(rightType) == false) {
			throw new IllegalStateException("Binary operation has operands of different types.")
		}
		if (leftType.isBoolType && !(rightType.isBoolType)) {
			// The rightType is the temporal type
			// TODO_LOWPRI improve?
			return rightType;
		}
		
		return leftType;
	}
}
