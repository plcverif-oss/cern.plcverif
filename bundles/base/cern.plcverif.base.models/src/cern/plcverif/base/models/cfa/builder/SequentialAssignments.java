/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Vince Molnar - update according to new metamodels
 *******************************************************************************/
package cern.plcverif.base.models.cfa.builder;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

import com.google.common.base.Preconditions;

import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory;
import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfadeclaration.AssignmentTransition;
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef;
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef;
import cern.plcverif.base.models.expr.Expression;

public final class SequentialAssignments {
	private Location currentLocation = null;
	private Supplier<Location> createUniqueLocation = null;
	private Supplier<String> uniqueTransitionName;
	private boolean invalid = false;
	private CfaDeclarationSafeFactory factory = CfaDeclarationSafeFactory.INSTANCE;

	public static class IndependentAssignmentCollector {
		private List<VariableAssignmentSkeleton> independentAssignments = new ArrayList<>();

		public IndependentAssignmentCollector assignment(FieldRef leftValue, Expression rightValue) {
			VariableAssignmentSkeleton varAmt = VariableAssignmentSkeleton.create(leftValue, rightValue);
			this.independentAssignments.add(varAmt);

			return this;
		}
	}

	private SequentialAssignments(Supplier<Location> createUniqueLocation, Supplier<String> uniqueTransitionName) {
		this.createUniqueLocation = createUniqueLocation;
		this.uniqueTransitionName = uniqueTransitionName;
	}

	public static SequentialAssignments builder() {
		return new SequentialAssignments(null, () -> null);
	}

	public static SequentialAssignments builder(Supplier<Location> createUniqueLocation,
			Supplier<String> uniqueTransitionName) {
		return new SequentialAssignments(createUniqueLocation, uniqueTransitionName);
	}
	
	public SequentialAssignments startLocation(Location startLocation) {
		Preconditions.checkState(this.currentLocation == null);
		Preconditions.checkNotNull(startLocation);
		this.currentLocation = startLocation;
		return this;
	}

	public SequentialAssignments assignment(DataRef leftValue, Expression rightValue) {
		genericChecks();

		return this.assignmentIfPresent(VariableAssignmentSkeleton.create(leftValue, rightValue));
	}

	public SequentialAssignments assignmentIfPresent(VariableAssignmentSkeleton assignment) {
		genericChecks();

		if (assignment != null) {
			Location nextLocation = createUniqueLocation.get();
			Preconditions.checkState(currentLocation.getParentAutomaton() instanceof AutomatonDeclaration);
			AutomatonDeclaration automaton = (AutomatonDeclaration) currentLocation.getParentAutomaton();
			AssignmentTransition assignmentTransition = factory.createAssignmentTransition(
					uniqueTransitionName.get(), automaton, currentLocation, nextLocation,
					factory.trueLiteral());
			assignment.createVariableAssignments(assignmentTransition);
			this.currentLocation = nextLocation;
		}
		return this;
	}

	public SequentialAssignments independentAssignments(VariableAssignmentSkeleton... assignments) {
		genericChecks();

		Location nextLocation = createUniqueLocation.get();
		Preconditions.checkState(currentLocation.getParentAutomaton() instanceof AutomatonDeclaration);
		AssignmentTransition assignmentTransition = factory.createAssignmentTransition(
				uniqueTransitionName.get(),
				(AutomatonDeclaration) currentLocation.getParentAutomaton(), currentLocation, nextLocation,
				factory.trueLiteral());

		for (VariableAssignmentSkeleton va : assignments) {
			va.createVariableAssignments(assignmentTransition);
		}
		this.currentLocation = nextLocation;
		return this;
	}

	public SequentialAssignments independentAssignments(Consumer<IndependentAssignmentCollector> assignments) {
		genericChecks();

		IndependentAssignmentCollector collector = new IndependentAssignmentCollector();
		assignments.accept(collector);
		this.independentAssignments(collector.independentAssignments
				.toArray(new VariableAssignmentSkeleton[collector.independentAssignments.size()]));
		return this;
	}

	public Location endLocation() {
		genericChecks();

		invalid = true;
		return this.currentLocation;
	}

	private void genericChecks() {
		Preconditions.checkState(currentLocation != null, "Must be initialized with startLoc().");
		Preconditions.checkState(!invalid, "Invalid builder object.");
	}
}
