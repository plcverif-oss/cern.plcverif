/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.models.cfa.utils;

import static cern.plcverif.base.models.cfa.textual.CfaToString.toDiagString;
import static cern.plcverif.base.models.cfa.utils.CfaUtils.getBaseType;
import static cern.plcverif.base.models.cfa.utils.CfaUtils.getFirstAnnotationOfType;
import static org.eclipse.emf.ecore.util.EcoreUtil.copy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.models.Guava21Workaround;
import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory;
import cern.plcverif.base.models.cfa.cfabase.ArrayType;
import cern.plcverif.base.models.cfa.cfabase.DataDirection;
import cern.plcverif.base.models.cfa.cfabase.Transition;
import cern.plcverif.base.models.cfa.cfadeclaration.AssignmentTransition;
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.CallTransition;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef;
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructure;
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructureRef;
import cern.plcverif.base.models.cfa.cfadeclaration.DirectionFieldAnnotation;
import cern.plcverif.base.models.cfa.cfadeclaration.Field;
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef;
import cern.plcverif.base.models.cfa.cfadeclaration.Indexing;
import cern.plcverif.base.models.cfa.cfadeclaration.VariableAssignment;
import cern.plcverif.base.models.cfa.textual.CfaToString;
import cern.plcverif.base.models.expr.ElementaryType;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.IntLiteral;
import cern.plcverif.base.models.expr.Literal;
import cern.plcverif.base.models.expr.Type;
import cern.plcverif.base.models.expr.utils.ExprUtils;

public final class CfaDeclarationUtils {
	private CfaDeclarationUtils() {
		// Utility class.
	}

	public static DataRef concatenateDataRefs(DataRef prefix, DataRef suffix) {
		Preconditions.checkNotNull(prefix, "prefix");
		Preconditions.checkNotNull(suffix, "suffix");
		EmfHelper.checkNotContained(prefix,
				"The provided prefix is already assigned to another model element. Copy it with EcoreUtils.copy().");
		EmfHelper.checkNotContained(suffix,
				"The provided suffix is already assigned to another model element. Copy it with EcoreUtils.copy().");

		DataRef localRoot = getRoot(suffix);

		// Validate that the suffix is compatible with the prefix
		Preconditions.checkState(localRoot instanceof FieldRef,
				"The suffix starts with an indexing. It should be impossible to obtain an indexing without a base array.");
		Guava21Workaround.checkArgument(prefix.getType() instanceof DataStructureRef,
				"The provided prefix references a data element that is not a data structure: prefix.type=%s",
				prefix.getType());

		Guava21Workaround.checkArgument(
				((DataStructureRef) prefix.getType()).getDefinition() == ((FieldRef) localRoot).getField()
						.getEnclosingType(),
				"The provided prefix references a data element that does not own the provided suffix: type=%s; suffix=%s",
				prefix.getType(), suffix);

		localRoot.setPrefix(prefix);
		return suffix;
	}

	public static DataRef getRoot(DataRef ref) {
		Preconditions.checkNotNull(ref, "ref");

		DataRef localRoot = ref;
		while (localRoot.getPrefix() != null) {
			localRoot = localRoot.getPrefix();
		}
		return localRoot;
	}

	public static Field getFieldOrFail(DataRef ref) {
		return getFieldOrFail(ref,
				String.format("The reference %s was expected to be a field reference, but it is not.",
						CfaToString.INSTANCE.toString(ref)));
	}

	public static Field getFieldOrFail(DataRef ref, String errorMessage) {
		if (ref instanceof FieldRef) {
			return ((FieldRef) ref).getField();
		}
		throw new IllegalArgumentException(errorMessage);
	}

	/**
	 * Returns true iff the given data reference refers to the given field.
	 * I.e., if the given data reference is a field reference ({@link FieldRef})
	 * and it refers to the given field.
	 * 
	 * @param ref
	 *            Reference examined
	 * @param field
	 *            Target field
	 * @return True iff the examined reference refers to the target field
	 */
	public static boolean refersTo(DataRef ref, Field field) {
		return (ref instanceof FieldRef && ((FieldRef) ref).getField() == field);
	}

	public static void checkRefStartsWith(DataRef ref, Field expectedField) {
		Field field = getFieldOrFail(getRoot(ref));
		Preconditions.checkArgument(field == expectedField,
				String.format("The reference %s was expected to start with a reference to %s, but it refers to %s.",
						CfaToString.INSTANCE.toString(ref), CfaToString.INSTANCE.toString(expectedField),
						CfaToString.INSTANCE.toString(field)));
	}

	public static void checkStaticallyIndexed(DataRef ref) {
		DataRef localRoot = ref;
		while (localRoot.getPrefix() != null) {
			if (localRoot instanceof Indexing) {
				Guava21Workaround.checkArgument(((Indexing) localRoot).getIndex() instanceof Literal, String.format(
						"The reference %s is indexed with a dynamic expression. If it is computable, compute it, otherwise the operation is not supported.",
						CfaToString.INSTANCE.toString(ref)));
			}
			localRoot = localRoot.getPrefix();
		}
	}

	// TODO: this is possibly implemented in the step7 module
	public static void forEachMultiArrayElement(DataRef arrayRef, Consumer<Indexing> consumer) {
		Preconditions.checkNotNull(arrayRef, "arrayRef");
		Preconditions.checkNotNull(consumer, "consumer");
		Preconditions.checkArgument(arrayRef.getType() instanceof ArrayType,
				"Array reference was expected, but instead found " + arrayRef.getType());

		ArrayType arrayType = (ArrayType) arrayRef.getType();
		Preconditions.checkArgument(arrayType.getDimension().isDefined(),
				"Cannot enumerate elements of an array of undefined size.");

		boolean multiArray = arrayType.getElementType() instanceof ArrayType;
		for (int i = arrayType.getDimension().getLowerIndex(); i <= arrayType.getDimension().getLowerIndex(); i++) {
			Indexing elementRef = CfaDeclarationSafeFactory.INSTANCE.appendIndexing(copy(arrayRef), i);
			if (multiArray) {
				forEachMultiArrayElement(elementRef, consumer);
			} else {
				consumer.accept(elementRef);
			}
		}
	}

	public static Collection<DataRef> getAllMultiArrayElements(DataRef arrayRef) {
		final List<DataRef> ret = new ArrayList<>();
		forEachMultiArrayElement(arrayRef, ref -> ret.add(ref));
		return ret;
	}

	public static void assignToAllMultiArrayElements(AssignmentTransition transition, DataRef arrayRef,
			Expression value) {
		Preconditions.checkNotNull(transition);
		Preconditions.checkNotNull(arrayRef, "arrayRef");
		Preconditions.checkNotNull(value, "value");
		Type baseType = getBaseType(arrayRef);
		Guava21Workaround.checkArgument(CfaTypeUtils.INSTANCE.dataTypeEquals(baseType, value.getType()),
				"The provided value (%s) is not compatible with the element type (%s) of the (multidimensional) array.",
				toDiagString(value), toDiagString(baseType));

		forEachMultiArrayElement(arrayRef,
				ref -> CfaDeclarationSafeFactory.INSTANCE.createAssignment(transition, ref, copy(value)));
	}

	// TODO: this is possibly implemented in VariableTraceModel and
	// CfaDeclarationSafeFactory
	// TODO: it would also make sense to return only the relative references
	// (for example to create assignments with different prefixes), but it would
	// break some existing contracts.
	public static void forEachElementaryDataElement(DataRef ref, Consumer<DataRef> consumer) {
		Preconditions.checkNotNull(ref, "ref");
		Preconditions.checkNotNull(consumer, "consumer");

		if (ref.getType() instanceof ElementaryType) {
			consumer.accept(copy(ref));
		} else if (ref.getType() instanceof ArrayType) {
			ArrayType arrayType = (ArrayType) ref.getType();
			Preconditions.checkArgument(arrayType.getDimension().isDefined(),
					"Cannot enumerate elements of an array of undefined size.");

			for (int i = arrayType.getDimension().getLowerIndex(); i <= arrayType.getDimension().getUpperIndex(); i++) {
				Indexing elementRef = CfaDeclarationSafeFactory.INSTANCE.appendIndexing(copy(ref), i);
				forEachElementaryDataElement(elementRef, consumer);
			}
		} else if (ref.getType() instanceof DataStructureRef) {
			DataStructure dataStructure = ((DataStructureRef) ref.getType()).getDefinition();
			for (Field field : dataStructure.getFields()) {
				FieldRef fieldRef = CfaDeclarationSafeFactory.INSTANCE.appendFieldRef(copy(ref), field);
				forEachElementaryDataElement(fieldRef, consumer);
			}
		}
	}

	public static List<DataRef> getAllElementaryDataElements(DataRef arrayRef) {
		final List<DataRef> ret = new ArrayList<>();
		forEachElementaryDataElement(arrayRef, ref -> ret.add(ref));
		return ret;
	}

	public static Collection<DataRef> getAllInputFields(CfaNetworkDeclaration cfaNetwork) {
		Preconditions.checkNotNull(cfaNetwork, "cfaNetwork");
		List<DataRef> ret = new ArrayList<>();
		for (Field field : cfaNetwork.getRootDataStructure().getFields()) {
			DirectionFieldAnnotation annotation = getFirstAnnotationOfType(field, DirectionFieldAnnotation.class);
			if (annotation != null && (annotation.getDirection() == DataDirection.INPUT
					|| annotation.getDirection() == DataDirection.INOUT)) {
				ret.add(CfaDeclarationSafeFactory.INSTANCE.createFieldRef(field));
			}
		}

		for (Field field : cfaNetwork.getMainAutomaton().getLocalDataStructure().getDefinition().getFields()) {
			DirectionFieldAnnotation annotation = getFirstAnnotationOfType(field, DirectionFieldAnnotation.class);
			if (annotation != null && (annotation.getDirection() == DataDirection.INPUT
					|| annotation.getDirection() == DataDirection.INOUT)) {
				ret.add(CfaDeclarationSafeFactory.INSTANCE.appendFieldRef(copy(cfaNetwork.getMainContext()), field));
			}
		}

		return ret;
	}

	/**
	 * Returns the deepest referred field. If the given reference is a field
	 * reference, the returned field is that one. If the given reference is an
	 * indexing, the returned field is the field that is indexed.
	 * <p>
	 * It supports multi-dimension indexing, i.e., indexing and indexing.
	 */
	public static Field getReferredField(DataRef dataRef) {
		if (dataRef instanceof FieldRef) {
			return ((FieldRef) dataRef).getField();
		} else if (dataRef instanceof Indexing) {
			return getReferredField(dataRef.getPrefix());
		}

		throw new UnsupportedOperationException("referredField " + dataRef);
	}

	public static boolean isLocallyDefinedField(Field field, AutomatonDeclaration automaton) {
		return field.getEnclosingType() == automaton.getLocalDataStructure().getDefinition();
	}

	/**
	 * Returns true iff the given field is directly defined in the root data
	 * structure of the CFD network.
	 * 
	 * @param field
	 *            Field to be checked.
	 * @return True, iff field is defined in root data structure.
	 */
	public static boolean isFieldDefinedInRoot(Field field) {
		EObject root = EcoreUtil.getRootContainer(field);
		Preconditions.checkState(root instanceof CfaNetworkDeclaration,
				"The root EObject of the given field is not a CfaNetworkDeclaration. Unexpected state.");

		CfaNetworkDeclaration cfaRoot = (CfaNetworkDeclaration) root;
		return (field.getEnclosingType() == cfaRoot.getRootDataStructure());
	}

	/**
	 * Returns {@code true} iff the given transition has (1) a constant true
	 * literal as guard, and (2) has no assignments attached to it, and (3) has
	 * no calls attached to it.
	 * 
	 * Note that this method may not detect all, semantically equivalent
	 * transitions, e.g., if the guard is {@code [a OR NOT a]}.
	 */
	public static boolean isEmptyTransition(Transition t) {
		Preconditions.checkArgument(
				!(t instanceof cern.plcverif.base.models.cfa.cfainstance.AssignmentTransition
						|| t instanceof cern.plcverif.base.models.cfa.cfainstance.CallTransition),
				"CfaDeclarationUtils.isEmptyTransition cannot be used for instance transitions.");

		boolean constantTrueGuard = ExprUtils.isTrueLiteral(t.getCondition());

		if (t instanceof AssignmentTransition) {
			AssignmentTransition amtTrans = (AssignmentTransition) t;
			return constantTrueGuard && amtTrans.getAssignments().isEmpty();
		} else if (t instanceof CallTransition) {
			CallTransition callTrans = (CallTransition) t;
			return constantTrueGuard && callTrans.getCalls().isEmpty();
		}
		return false;
	}

	/**
	 * Returns true iff the given data structure has no fields.
	 */
	public static boolean isEmpty(DataStructure ds) {
		return ds.getFields().isEmpty();
	}

	/**
	 * Tries to parse the given textual data reference into a {@code DataRef}.
	 * <p>
	 * It is assumed that the different segments of the given textual reference
	 * are separated by {@code .} characters.
	 * 
	 * @param refToParse
	 *            The textual reference to be parsed.
	 * @param contextCfd
	 *            The context declaration CFA in which the references are
	 *            resolved.
	 * @param fieldNameProvider
	 *            Returns the name used for the given field.
	 * @return If the reference was resolved, the result is the resolved
	 *         {@link DataRef} object. If the resolution is unsuccessful,
	 *         exception is thrown. Null value is never returned.
	 * @throws IllegalArgumentException
	 *             If the given reference is illegal.
	 */
	public static DataRef parseHierarchicalDataRef(String refToParse, CfaNetworkDeclaration contextCfd,
			Function<Field, String> fieldNameProvider) {
		DataStructure contextStructure = contextCfd.getRootDataStructure();
		CfaDeclarationSafeFactory factory = CfaDeclarationSafeFactory.INSTANCE;

		// Try to handle it as a non-hierarchical name first.
		Optional<Field> exactFieldMatch = contextStructure.getFields().stream()
				.filter(it -> fieldNameProvider.apply(it).equalsIgnoreCase(refToParse)).findFirst();
		if (exactFieldMatch.isPresent()) {
			return factory.createFieldRef(exactFieldMatch.get());
		}

		// If there is no such field, split it into name segments and try to
		// find it that way.
		// Function names can have ".", so we create every possible combination and assign it to a list
		List<ArrayList<String>> listOfSegments = new ArrayList<ArrayList<String>>();//new
		
		long count = refToParse.chars().filter(ch -> ch == '.').count();
		
		if (count==0) {
			ArrayList<String> segments = new ArrayList<>(); 
			Iterator<String> iterator = Arrays.asList(refToParse.split("\\[|\\]")).iterator();
			// we remove the [...] of the end of the reference if it exists  
			String function = iterator.next();
			segments.add(function);
			listOfSegments.add(segments);
		}
		
		for (int i=0; i<count; i++) {
			ArrayList<String> segments = new ArrayList<>(); 
			Iterator<String> iterator = Arrays.asList(refToParse.split("\\.|\\[|\\]")).iterator();
			
			String function = iterator.next();
			for (int j=0; j<i; j++) {
				function += "."+iterator.next();
			}
			segments.add(function);
			
			while (iterator.hasNext()) {
					segments.add(iterator.next());
			}
			
			listOfSegments.add(segments);			
		}
				
		DataRef result = null;
		DataRef oldResult = null;
				
		Iterator<ArrayList<String>> iteratorLists = listOfSegments.iterator();
		while (iteratorLists.hasNext()) {
			ArrayList<String> segments = iteratorLists.next();
			Iterator<String> iterator = segments.iterator();
			
			while (iterator.hasNext()) {
				String nextSegment = iterator.next();

				Optional<Field> field = contextStructure.getFields().stream()
						.filter(it -> fieldNameProvider.apply(it).equalsIgnoreCase(nextSegment)).findFirst();
				if (!field.isPresent()) {
					if (contextStructure == contextCfd.getRootDataStructure()) {
						// if we tried all the possibilities and we did not find it
						if (!iteratorLists.hasNext()) {
							throw new IllegalArgumentException(String.format(
									"Unable to parse the named element '%s'. Field '%s' not found in the global context, it is not a global name. Did you mean 'instance.%s'?",
									refToParse, nextSegment, refToParse));
						} else { // we go to the next possibility
							segments = iteratorLists.next();
							iterator = segments.iterator();
							continue;
						}
					} else {
						throw new IllegalArgumentException(
								String.format("Unable to parse the named element '%s'. Field '%s' not found in context '%s'.",
										refToParse, nextSegment, contextStructure.getDisplayName()));
					}
				}
				Preconditions.checkState(field.isPresent());

				// build the result data reference
				if (result == null) {
					result = factory.createFieldRef(field.get());
				} else {
					oldResult =  result;
					result = factory.appendFieldRef(result, field.get());
				}

				Type fieldType = field.get().getType();
				if (fieldType instanceof DataStructureRef) {
					// still a complex type; we keep on parsing
					contextStructure = ((DataStructureRef) fieldType).getDefinition();
					if (!iterator.hasNext()) {
						throw new IllegalArgumentException(
								String.format("Error occurred while parsing '%s'. The referred field '%s' is not an atom.",
										refToParse, nextSegment));
					}
				} else if (fieldType instanceof ArrayType) {
					// fetch the next segment here and append as indexing
					if (!iterator.hasNext()) {
						throw new IllegalArgumentException(String.format(
								"Error occurred while parsing '%s'. The referred field '%s' is an array, but no index was given.",
								refToParse, nextSegment));
					}
					String index = iterator.next();
					try {
						result = factory.appendIndexing(result, Long.parseLong(index));
					} catch (NumberFormatException e) {
						throw new IllegalArgumentException(String.format(
								"Error occurred while parsing '%s'. The index '%s' for array '%s' is invalid: integer is expected.",
								refToParse, index, nextSegment));
					}
				} else if (iterator.hasNext()) {
					// If we thought its an elementary type but there is something left to parse, check whether it exists as an array due to var_views
					Optional<Field> arrayField = contextStructure.getFields().stream()
							.filter(it -> fieldNameProvider.apply(it).equalsIgnoreCase(nextSegment+"_AS_ARRAY")).findFirst();
					if (arrayField.isPresent()) {
						if (result == null) {
							result = factory.createFieldRef(arrayField.get());
						} else {
							
							result = factory.appendFieldRef(EcoreUtil.copy(oldResult), arrayField.get());
						}
						// TODO: substring(2) works for accesses of type %x{int}, maybe this needs to be more generic
						String index = iterator.next().substring(2);
						try {
							result = factory.appendIndexing(result, Long.parseLong(index));
						} catch (NumberFormatException e) {
							throw new IllegalArgumentException(String.format(
									"Error occurred while parsing '%s'. The index '%s' for array '%s' is invalid: integer is expected.",
									refToParse, index, nextSegment));
						}
					} else {
						// If the current field is not a data structure nor an array, we
						// cannot continue and we should have reached the end of the
						// atom to be parsed.
						throw new IllegalArgumentException(String.format(
								"Error occurred while parsing '%s'. Field '%s' with elementary type has found, but there are remaining segments (next segment: '%s').",
								refToParse, field.get().getName(), iterator.next()));
					}
				}
			}
			// it was found, so we can escape
			if (result != null) { break; }
		}

		Preconditions.checkState(result != null);
		return result;
	}

	/**
	 * Tries to parse the given textual data reference (which may contain '*'
	 * and '?') into a collection of {@code DataRef}s.
	 * <p>
	 * It is assumed that the different segments of the given textual reference
	 * are separated by {@code .} characters or {@code [} and {@code ]} pairs in
	 * case of indexing.
	 * <p>
	 * The wildcard {@code *} cannot substitute {@code .}, {@code [}, or
	 * {@code ]}.
	 * <p>
	 * As it checks all elementary data references in the model, the execution
	 * may be slow.
	 * 
	 * @param refToParse
	 *            The textual reference to be parsed. May contain {@code *} and
	 *            {@code ?} wildcards.
	 * @param contextCfd
	 *            The context declaration CFA in which the references are
	 *            resolved.
	 * @param fieldNameProvider
	 *            Returns the name used for the given field.
	 * @return All <b>elementary</b> data references whose name match the given
	 *         pattern. Never {@code null}.
	 */
	public static List<DataRef> parseHierarchicalElementaryDataRefsWithWildcard(String refToParse,
			CfaNetworkDeclaration contextCfd, Function<Field, String> fieldNameProvider) {
		List<DataRef> candidates = new ArrayList<>();
		for (Field rootFields : contextCfd.getRootDataStructure().getFields()) {
			candidates.addAll(getAllElementaryDataElements(FACTORY.createFieldRef(rootFields)));
		}

		String regexPattern = String.format("\\Q%s\\E", refToParse).replace("*", "\\E[^\\.\\[\\]]*\\Q").replace("?",
				"\\E[^\\.\\[\\]]\\Q");

		List<DataRef> ret = new ArrayList<>();
		for (DataRef candidate : candidates) {
			String dataRefHierName = dataRefNameFromSegments(candidate, fieldNameProvider);
			if (dataRefHierName.matches(regexPattern)) {
				ret.add(candidate);
			} 
		}
		return ret;
	}

	private static String dataRefNameFromSegments(DataRef dataRef, Function<Field, String> fieldNameProvider) {
		String thisSegmentName;
		if (dataRef instanceof FieldRef) {
			thisSegmentName = fieldNameProvider.apply(((FieldRef) dataRef).getField());
		} else if (dataRef instanceof Indexing) {
			Expression index = ((Indexing) dataRef).getIndex();
			if (index instanceof IntLiteral) {
				thisSegmentName = String.format("[%s]", ((IntLiteral) index).getValue());
			} else {
				throw new UnsupportedOperationException("Unexpected indexing: not an int literal.");
			}
		} else {
			throw new UnsupportedOperationException("Unknown element type: " + dataRef.getClass().getSimpleName());
		}

		if (dataRef.getPrefix() == null) {
			return thisSegmentName;
		} else {
			String previousSegments = dataRefNameFromSegments(dataRef.getPrefix(), fieldNameProvider);
			return previousSegments + (thisSegmentName.startsWith("[") ? "" : ".") + thisSegmentName;
		}
	}

	private static final CfaDeclarationSafeFactory FACTORY = CfaDeclarationSafeFactory.INSTANCE;

	public static Collection<FieldRef> getAllFieldInstances(CfaNetworkDeclaration cfa) {
		Collection<FieldRef> ret = new ArrayList<>();
		for (Field field : cfa.getRootDataStructure().getFields()) {
			getAllFieldInstances(null, field, ret);
		}
		return ret;
	}

	private static void getAllFieldInstances(FieldRef prefix, Field fieldToAppend, Collection<FieldRef> accu) {
		// TODO what to do with array elements?
		if (prefix == null) {
			accu.add(FACTORY.createFieldRef(fieldToAppend));
		} else {
			accu.add(FACTORY.appendFieldRef(EcoreUtil.copy(prefix), fieldToAppend));
		}

		if (fieldToAppend.getType() instanceof DataStructureRef) {
			// if complex type, continue
			FieldRef newPrefix;
			if (prefix == null) {
				newPrefix = FACTORY.createFieldRef(fieldToAppend);
			} else {
				newPrefix = FACTORY.appendFieldRef(EcoreUtil.copy(prefix), fieldToAppend);
			}
			for (Field subfield : ((DataStructureRef) fieldToAppend.getType()).getDefinition().getFields()) {
				getAllFieldInstances(newPrefix, subfield, accu);
			}
		}
	}

	/**
	 * Optimized deletion for {@link VariableAssignment}.
	 * <p>
	 * It relies on the fact that {@link VariableAssignment} objects are never
	 * referenced, only contained.
	 * 
	 * @param amt
	 *            Assignment to be deleted.
	 */
	public static void deleteVariableAssignment(VariableAssignment amt) {
		EcoreUtil.remove(amt);
	}

	/**
	 * Returns true iff the given reference is a global reference, i.e., the
	 * first segment of the qualified reference refers to a field declared in
	 * the root data structure.
	 * 
	 * This method is only for references already contained in a network. If the
	 * reference to be checked is not contained in a network, use
	 * {@link #isGlobal(DataRef, CfaNetworkDeclaration)}.
	 * 
	 * @param ref
	 *            The reference to be checked
	 * @return True if the given reference is global (fully qualified)
	 */
	public static boolean isGlobal(DataRef ref) {
		Preconditions.checkNotNull(ref, "ref");

		CfaNetworkDeclaration network = EmfHelper.getContainerOfType(ref, CfaNetworkDeclaration.class);
		Preconditions.checkNotNull(network, "The given reference is not contained in a CFA declaration network.");

		return isGlobal(ref, network);
	}

	/**
	 * Returns true iff the given reference is a global reference in the given
	 * network, i.e., the first segment of the qualified reference refers to a
	 * field declared in the root data structure of the given network.
	 * 
	 * This method is mainly for references which are not contained yet in a
	 * network. If the reference to be checked is contained in a network, it may
	 * be easier to use {@link #isGlobal(DataRef)}.
	 * 
	 * @param ref
	 *            The reference to be checked
	 * @param network
	 *            The network in which the given data reference is evaluated
	 * @return True if the given reference is global (fully qualified) in the
	 *         given network
	 */
	public static boolean isGlobal(DataRef ref, CfaNetworkDeclaration network) {
		Preconditions.checkNotNull(ref, "ref");
		Preconditions.checkNotNull(network, "network");
		DataStructure rootDataStructure = network.getRootDataStructure();

		DataRef rootOfRef = getRoot(ref);
		return rootOfRef instanceof FieldRef
				&& ((FieldRef) rootOfRef).getField().getEnclosingType() == rootDataStructure;
	}
}
