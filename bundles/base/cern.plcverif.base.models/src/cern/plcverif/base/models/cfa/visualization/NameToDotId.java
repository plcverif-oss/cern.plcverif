/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Vince Molnar - update to new metamodels
 *******************************************************************************/
package cern.plcverif.base.models.cfa.visualization;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Preconditions;

final class NameToDotId {
	private Map<String, String> nameToDotIdMap = new HashMap<>();
	private Set<String> knownDotIds = new HashSet<>();

	public String toId(String name) {
		if (!nameToDotIdMap.containsKey(name)) {
			String sanitizedName = sanitizeName(name);
			String uniqueName = makeUnique(sanitizedName);
			nameToDotIdMap.put(name, uniqueName);
			knownDotIds.add(uniqueName);
		}

		String ret = nameToDotIdMap.get(name);
		Preconditions.checkState(ret != null);
		return ret;
	}

	private static String sanitizeName(final String name) {
		String sanitizedName = name;

		// GraphViz ID should contain only the following characters: a-z, A-Z, 0-9, _
		// (Actually, it is slightly more permissive, see http://www.graphviz.org/content/dot-language)
		sanitizedName = sanitizedName.replaceAll("[^a-zA-Z0-9_]", "_");

		if (sanitizedName.matches("^[0-9].*$")) {
			// GraphViz ID should not start with numeric (if not a number itself)
			sanitizedName = "_" + sanitizedName;
		}

		return sanitizedName;
	}

	private String makeUnique(String name) {
		if (!knownDotIds.contains(name)) {
			return name;
		}

		// there is a clash with an already existing name, let's make this
		// unique
		int appendNumber = 1;
		while (knownDotIds.contains(name + appendNumber)) {
			appendNumber++;
		}

		return name + appendNumber;
	}

}
