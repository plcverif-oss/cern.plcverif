/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.interfaces;

import cern.plcverif.base.common.progress.ICanceling;
import cern.plcverif.base.common.settings.ISettingsProvider;
import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.models.cfa.cfabase.CfaNetworkBase;
import cern.plcverif.base.models.cfa.cfabase.Freezable;
import cern.plcverif.base.models.expr.Expression;

/**
 * A reduction is a CFA transformation with the goal of reducing the size or
 * complexity of the CFA model in some sense, preserving certain properties of
 * it.
 */
public interface IReduction extends ISettingsProvider {
	/**
	 * Should return a <i>new</i> {@link Settings} instance with enough content
	 * to perfectly reproduce the configuration of the current instance even
	 * when the default settings are changed.
	 */
	public Settings retrieveSettings();

	/**
	 * Perform the reduction transformation on {@code cfa}. The reduction
	 * <i>must not modify or remove</i> elements that implement the
	 * {@link Freezable} interface and their {@code frozen} attribute is true.
	 * <p>
	 * This method is expected to preserve the behavior of the CFA network such
	 * that the result is input-output conformant with the original CFA at the
	 * end of each cycle (of the main loop).
	 * <p>
	 * The results should be logged through the provided {@code result} object.
	 * <p>
	 * If a cancellation is requested through the given canceler object
	 * {@link ICanceling#isCanceled()}, the reduction process should be
	 * cancelled at the earliest convenience.
	 * 
	 * @param canceler Canceler object. May be {@code null}. 
	 * @return True iff the reduction made any modification on the CFA
	 */
	public boolean reduce(CfaNetworkBase cfa, JobResult result, ICanceling canceler);

	/**
	 * Perform the reduction transformation on {@code cfa}. The reduction
	 * <i>must not modify or remove</i> elements that implement the
	 * {@link Freezable} interface and their {@code frozen} attribute is true.
	 * <p>
	 * This method is expected to preserve the behavior of the CFA network in
	 * terms of the provided {@code requirement}, that is, if the requirement
	 * should hold on the result CFA exactly when it holds on the original CFA.
	 * <p>
	 * The results should be logged through the provided {@code result} object.
	 * Reductions are not expected to set any attributes of the result.
	 * <p>
	 * If a cancellation is requested through the given canceler object
	 * {@link ICanceling#isCanceled()}, the reduction process should be
	 * cancelled at the earliest convenience.
	 * 
	 * @param canceler Canceler object. May be {@code null}. 
	 * @return True iff the reduction made any modification on the CFA
	 */
	public boolean reduce(CfaNetworkBase cfa, Expression requirement, JobResult result, ICanceling canceler);
}
