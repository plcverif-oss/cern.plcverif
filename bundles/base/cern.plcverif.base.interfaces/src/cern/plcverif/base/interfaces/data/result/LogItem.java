/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.interfaces.data.result;

import java.util.Date;

import cern.plcverif.base.common.logging.PlcverifSeverity;

public final class LogItem {
	private JobStage stage;
	private PlcverifSeverity severity;
	private String message;
	private Throwable exception;
	private Date time; // FIXME change it to LocalDateTime or ZonedDateTime

	private LogItem(JobStage stage, PlcverifSeverity severity, String message) {
		this(stage, severity, message, null);
	}

	private LogItem(JobStage stage, PlcverifSeverity severity, String message, Throwable exception) {
		super();
		this.stage = stage;
		this.severity = severity;
		this.message = message;
		this.exception = exception;
		this.time = new Date();
	}
	
	public static LogItem create(JobStage stage, PlcverifSeverity severity, String message) {
		return new LogItem(stage, severity, message);
	}
	
	public static LogItem create(JobStage stage, PlcverifSeverity severity, String message, Throwable exception) {
		return new LogItem(stage, severity, message, exception);
	}

	public JobStage getStage() {
		return this.stage;
	}

	public PlcverifSeverity getSeverity() {
		return this.severity;
	}

	public String getMessage() {
		return this.message;
	}

	public Throwable getException() {
		return this.exception;
	}

	public Date getTime() {
		return this.time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public boolean severityAtLeast(PlcverifSeverity minimumSeverity) {
		return this.severity.isAtLeast(minimumSeverity);
	}
}
