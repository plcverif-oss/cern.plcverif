/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.interfaces;

import cern.plcverif.base.common.extension.ExtensionPoint;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.help.SettingsHelp;
import cern.plcverif.base.interfaces.data.PlatformConfig;

/**
 * Implementors of this interface are extension points serving as a factory for
 * {@link IBasicJob} instances. A job is a specification for a specific workflow
 * in PLCverif. The most important built-in job is the VerificationJob (found in
 * the {@code cern.plcverif.verif.job} project). A job is in charge of invoking
 * the tasks provided by the Platform to achieve a specific goal. By default,
 * they are given a specification of model-related tasks universal for all jobs
 * (which specifies how the input model should be processed) - other settings
 * should be job-specific.
 * <p>
 * Implementors must realize an Extension for the extension point
 * cern.plcverif.base.extensions.job.
 */
@ExtensionPoint(extensionPointId = "cern.plcverif.base.extensions.job")
public interface IJobExtension {
	/**
	 * Should create an {@link IBasicJob} instance with the default configuration.
	 * The result is expected to be used by client code to programmatically
	 * configure the job.
	 */
	public IBasicJob createJob();

	/**
	 * Should create an {@link IBasicJob} pre-configured according to the contents
	 * of the provided settings description.
	 * <p>
	 * The result is expected to be used as returned, so every setting has to be
	 * properly set. If this is not possible based on the provided settings
	 * description, the method should throw a {@link SettingsParserException}.
	 */
	public IBasicJob createJob(SettingsElement settings, PlatformConfig platformConfig) throws SettingsParserException;

	/**
	 * Fills the given {@link SettingsHelp} object with the settings accepted by
	 * this extension (recursively) and their description.
	 */
	public void fillSettingsHelp(SettingsHelp help);
}
