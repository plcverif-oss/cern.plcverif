/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.interfaces;

import java.nio.file.Path;

import cern.plcverif.base.common.progress.ICancelingProgressReporter;
import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.interfaces.data.BasicJobResult;
import cern.plcverif.base.interfaces.data.JobResult;

/**
 * A basic job is a specification for a specific workflow in PLCverif with very
 * few expectations about the workflow. Typical jobs will implement
 * {@link ICfaJob}, that inherits from {@link IBasicJob}. {@link IBasicJob} is
 * to be used mainly for tasks that are not PLC source code oriented, but for
 * some reason it is beneficial to incorporate in the PLCverif framework.
 */
public interface IBasicJob {
	/**
	 * Should return a <i>new</i> {@link Settings} instance with enough content
	 * to perfectly reproduce the configuration of the current instance even
	 * when the default settings are changed.
	 */
	public Settings retrieveSettings();

	/**
	 * Should store the specified path as the output directory of generated
	 * artifacts. The specified value should be set on the {@link JobResult}
	 * object as well once it is created by the job.
	 */
	public void setOutputDirectory(Path outputDirectory);

	/**
	 * Should return the path of the output directory set through
	 * {@link #setOutputDirectory(Path)}.
	 * <p>
	 * <b>The specified directory may not exists.</b>
	 */
	public Path getOutputDirectory();

	/**
	 * Sets the program directory of PLCverif. The relative paths to external
	 * tools will be resolved using this directory.
	 */
	public void setProgramDirectory(Path programDirectory);

	/**
	 * Returns the program directory of PLCverif. The relative paths to external
	 * tools will be resolved using this directory. Should return the path of
	 * the program directory set through {@link #setProgramDirectory(Path)}.
	 */
	public Path getProgramDirectory();

	/**
	 * Should execute the workflow specified by the job, based on the provided
	 * configuration. The method can assume that every interesting configuration
	 * parameter has already been set.
	 * <p>
	 * The return value is a {@link BasicJobResult}, which should be extended to
	 * store job-specific results and gradually filled during the execution of
	 * the workflow.
	 * 
	 * @param reporter
	 *            Reporter to give information about the progress of the job
	 *            execution. It can also request the cancellation of the job.
	 */
	public BasicJobResult execute(ICancelingProgressReporter reporter);
}
