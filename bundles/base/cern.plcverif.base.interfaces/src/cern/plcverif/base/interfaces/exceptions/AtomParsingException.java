/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.interfaces.exceptions;

public class AtomParsingException extends Exception {
	private static final long serialVersionUID = -6964906702897632245L;

	public AtomParsingException(String message) {
		super(message);
	}
	
	public AtomParsingException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
