/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.interfaces.data;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import com.google.common.base.Preconditions;

public final class PluginKeyValueStore {
	public static final class StoreKey {
		private String plugin;
		private String key;
		
		/**
		 * Creates a new key to be used in the {@link PluginKeyValueStore}.
		 * @param plugin ID of plug-in creating the key. Shall not be {@code null}.
		 * @param key Key. Shall not be {@code null}.
		 */
		public StoreKey(String plugin, String key) {
			this.plugin = Preconditions.checkNotNull(plugin);
			this.key = Preconditions.checkNotNull(key);
		}
		
		public String getPlugin() {
			return plugin;
		}
		
		public String getKey() {
			return key;
		}
		
		@Override
		public boolean equals(Object obj) {
			if (obj instanceof StoreKey) {
				StoreKey other = (StoreKey)obj;
				return (other.key.equals(this.key) && other.plugin.equals(this.plugin));
			}
			return super.equals(obj);
		}
		@Override
		public int hashCode() {
			return Objects.hash(this.plugin, this.key);
		}
		
		@Override
		public String toString() {
			return String.format("%s$%s", this.plugin, this.key);
		}
	}
	
	private Map<StoreKey, String> map = new HashMap<>();
	
	public void addValue(String plugin, String key, String value) {
		Preconditions.checkNotNull(plugin, "plugin");
		Preconditions.checkNotNull(key, "key");
		Preconditions.checkNotNull(value, "value");
		
		map.put(toKey(plugin, key), value);
	}
	
	public Optional<String> getValue(String plugin, String key) {
		StoreKey mapKey = toKey(plugin, key);
		if (map.containsKey(mapKey)) {
			return Optional.of(map.get(mapKey));
		} else {
			return Optional.empty();
		}
	}
	
	public Set<Entry<StoreKey, String>> getEntrySet() {
		return Collections.unmodifiableSet(map.entrySet());
	}
	
	private static StoreKey toKey(String plugin, String key) {
		return new StoreKey(plugin, key);
	}
}

