/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.interfaces;

import cern.plcverif.base.common.progress.ICancelingProgressReporter;
import cern.plcverif.base.interfaces.data.JobMetadata;
import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.interfaces.data.ModelTasks;

/**
 * A job is a specification for a specific workflow in PLCverif. The most
 * important built-in job is the VerificationJob (found in the
 * {@code cern.plcverif.verif.job} project). A job is in charge of invoking the
 * tasks provided by the Platform to achieve a specific goal. By default, they
 * are given a specification of model-related tasks universal for all jobs
 * (which specifies how the input model should be processed) - other settings
 * should be job-specific.
 */
public interface ICfaJob extends IBasicJob {
	/**
	 * Should store the metadata object for later retrieval.
	 */
	public void setMetadata(JobMetadata metadata);

	/**
	 * Should return the metadata object set through
	 * {@link #setMetadata(JobMetadata)}.
	 */
	public JobMetadata getMetadata();

	/**
	 * Should store the {@link ModelTasks} description to use during the job
	 * workflow. See {@link ModelTasks} for details about stored configuration
	 * and instructions about how to interpret them.
	 */
	public void setModelTasks(ModelTasks modelTasks);

	/**
	 * Should execute the workflow specified by the job, based on the provided
	 * configuration. The method can assume that every interesting configuration
	 * parameter has already been set.
	 * <p>
	 * The return value is a {@link JobResult}, which should be extended to
	 * store job-specific results and gradually filled during the execution of
	 * the workflow.
	 * 
	 * @param reporter
	 *            Reporter to give information about the progress of the job
	 *            execution. It can also request the cancellation of the job.
	 */
	public JobResult execute(ICancelingProgressReporter reporter);
}
