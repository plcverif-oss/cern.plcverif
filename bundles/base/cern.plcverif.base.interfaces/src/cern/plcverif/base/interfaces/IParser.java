/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.interfaces;

import java.util.Map;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;

/**
 * A parser should be able to turn a certain type of input (specified by the
 * parser itself, e.g. PLC code) into an AST (see {@link IAstReference}) and
 * then a {@link CfaNetworkDeclaration}.
 */
public interface IParser {
	/**
	 * Should return the content of all files used while parsing the input
	 * mapped to the corresponding filenames.
	 */
	public Map<String, String> getFiles();

	/**
	 * Used to set the files (and their contents) by the platform before calling
	 * {@link #parseCode(JobResult)}. Input files should never be included in
	 * the settings of the parser, as it is an input and not a configuration. An
	 * exception is auxiliary files (e.g. library functions).
	 */
	public void setFiles(Map<String, String> files);

	/**
	 * Should return a <i>new</i> {@link Settings} instance with enough content
	 * to perfectly reproduce the configuration of the current instance even
	 * when the default settings are changed.
	 */
	public Settings retrieveSettings();

	// Note: modify the javadoc about the result attributes if anything
	// parser-specific is added at some point.
	/**
	 * Should create an {@link IParserLazyResult} that is expected to contain at
	 * least the AST representation of the input (to generate parsing errors at
	 * this point and distinguish them from CFA transformation errors).
	 * <p>
	 * The details and results of the operation should be logged through the
	 * {@code result} object. Parsers are not expected to set any attributes of
	 * the result.
	 * <p>
	 * As the returned object is a lazy result, no exception is expected right
	 * now, even if the set source files are invalid.
	 */
	public IParserLazyResult parseCode(JobResult result);
}
