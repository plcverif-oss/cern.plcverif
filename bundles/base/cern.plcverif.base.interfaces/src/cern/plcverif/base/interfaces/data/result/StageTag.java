/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.interfaces.data.result;

/**
 * Tag that can be used to categorize the stages.
 *
 */
public abstract class StageTag {
	private String name;
	
	public StageTag(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
