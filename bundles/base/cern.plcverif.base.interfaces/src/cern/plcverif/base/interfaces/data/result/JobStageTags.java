/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.interfaces.data.result;

public final class JobStageTags {
	private static class JobStageTag extends StageTag {
		// This is private as noone needs to see the exact type (i.e., type more specific than StageTag).
		public JobStageTag(String name) {
			super(name);
		}
	}

	/**
	 * Tag to indicate the stage(s) which represents the execution of the
	 * reductions. To be used to compute the time spent on reducing the CFAs
	 * (both CFD and CFI).
	 */
	public static final StageTag REDUCTIONS = new JobStageTag("Model reductions");
	
	private JobStageTags() {
		// Not intended to be instantiated.
	}
}
