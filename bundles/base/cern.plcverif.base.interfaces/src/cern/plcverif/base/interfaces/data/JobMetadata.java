/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Vince Molnar - some documentation
 *******************************************************************************/
package cern.plcverif.base.interfaces.data;

/**
 * This class represents the metadata for jobs. The stored data has no
 * implication on how the job should be executed.
 */
public final class JobMetadata {
	private String id = "unknown-id";
	private String name = "unknown-name";
	private String description = "";

	/**
	 * An ID that may be used in file names.
	 */
	public String getId() {
		return id;
	}

	/**
	 * An ID that may be used in file names. It can only contain file-safe
	 * characters.
	 */
	public String getIdWithoutSpecialChars() {
		return id.replaceAll("[^a-zA-Z0-9_\\-\\.]", "_");
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
	 * A name that can be displayed in logs, report files or on a UI.
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * A longer textual description of the purpose of the job. Not used for any
	 * specific purpose other than documentation.
	 */
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
