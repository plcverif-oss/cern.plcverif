/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.interfaces.data;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Preconditions;

import cern.plcverif.base.interfaces.IParser;
import cern.plcverif.base.interfaces.IReduction;

/**
 * This class represents the configuration for model transformation to be
 * performed on the input model. Jobs should always use this information when
 * processing the input. The precise expectations are documented at the specific
 * getter methods.
 */
public final class ModelTasks {
	private IParser parser = null;
	private boolean forceInline = false;
	private List<IReduction> reductions = new ArrayList<>();

	/**
	 * The parser to use in order to obtain the AST or CFA models. The parser
	 * should be completely configured to work on its own by the time it should
	 * be used.
	 */
	public IParser getParser() {
		return parser;
	}

	public void setParser(IParser parser) {
		Preconditions.checkNotNull(parser, "parser");
		this.parser = parser;
	}

	/**
	 * When true, the job should perform call inlining even if it would not be
	 * necessary.
	 */
	public boolean isForceInline() {
		return forceInline;
	}

	public void setForceInline(boolean inline) {
		this.forceInline = inline;
	}

	public void setReductions(List<IReduction> reductions) {
		Preconditions.checkNotNull(reductions, "reductions");
		this.reductions = reductions;
	}

	/**
	 * The job is expected to perform these reductions whenever it obtains a new
	 * representation (e.g. by instantiating or inlining.
	 */
	public List<IReduction> getReductions() {
		return reductions;
	}
}
