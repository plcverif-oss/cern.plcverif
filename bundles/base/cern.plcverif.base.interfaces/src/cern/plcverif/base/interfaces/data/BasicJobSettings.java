/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.interfaces.data;

import static cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory.MANDATORY;
import static cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory.OPTIONAL;

import java.nio.file.Path;

import cern.plcverif.base.common.settings.specific.AbstractSpecificSettings;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsElement;

public class BasicJobSettings extends AbstractSpecificSettings {
	public static final String OUTPUT_DIRECTORY = "output";
	public static final String JOB = "job";
	public static final String PROGRAM_DIRECTORY = "program_directory";

	@PlcverifSettingsElement(name = OUTPUT_DIRECTORY, description = "Output artifact directory. If omitted, a new directory is created automatically.", mandatory = OPTIONAL)
	private Path outputDirectory = null;

	@PlcverifSettingsElement(name = JOB, description = "Job to be executed.", mandatory = MANDATORY)
	private String job;

	public Path getOutputDirectory() {
		return outputDirectory;
	}

	public String getJob() {
		return job;
	}

	public void setOutputDirectory(Path outputDirectory) {
		this.outputDirectory = outputDirectory;
	}
}
