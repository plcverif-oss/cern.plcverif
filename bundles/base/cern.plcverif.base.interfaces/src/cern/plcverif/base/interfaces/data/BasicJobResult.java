/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.interfaces.data;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

import cern.plcverif.base.common.logging.PlcverifSeverity;
import cern.plcverif.base.common.progress.IProgressReporter;
import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.interfaces.data.result.JobStage;
import cern.plcverif.base.interfaces.data.result.OutputFileToSave;
import cern.plcverif.base.interfaces.data.result.StageStatus;
import cern.plcverif.base.interfaces.data.result.StageTag;

public abstract class BasicJobResult {
	// Job details
	protected Settings settings;
	protected Path outputDirectory;
	protected Path programDirectory = Paths.get(".");

	// Logging
	private List<JobStage> stages = new ArrayList<>();
	private Map<String, JobStage> jobStages = new HashMap<>();
	private String currentStage = null;
	private IProgressReporter progressReporter = null;

	private final PluginKeyValueStore keyValueStore = new PluginKeyValueStore();

	// Artifacts to persist
	private List<OutputFileToSave> filesToSave = new ArrayList<>();

	// Job details
	/**
	 * Returns the specific settings used for the current job.
	 * 
	 * @return Parsed job settings. Shall not be {@code null}, but can be
	 *         {@link Optional#empty()}.
	 */
	protected abstract Optional<JobSettings> getSpecificSettings();

	/**
	 * Returns the {@link Settings} description that can be used to replicate
	 * the job.
	 */
	public Settings getSettings() {
		return settings;
	}

	/**
	 * Stores the {@link Settings} description that can be used to replicate the
	 * job.
	 * 
	 * @param settings
	 *            Settings used for the job.
	 */
	public void setSettings(Settings settings) {
		this.settings = settings;
	}

	/**
	 * Return the path of the output directory. Primarily used along with
	 * {@link #getOutputFilesToSave()} to prefix relative paths.
	 * <p>
	 * <b>The specified directory may not exists.</b> <b>May be null if not
	 * set.</b>
	 */
	public Path getOutputDirectory() {
		return outputDirectory;
	}

	/**
	 * Stores the path of the output directory.
	 * <p>
	 * The specified directory does not necessarily have to exist.</b> May be
	 * null.
	 */
	public void setOutputDirectory(Path outputDirectory) {
		this.outputDirectory = outputDirectory;
	}

	/**
	 * Return the path of the output directory. Primarily used along with
	 * {@link #getOutputFilesToSave()} to prefix relative paths.
	 * <p>
	 * <b>The specified directory may not exists.</b> <b>If it was not set up
	 * explicitly before, it creates a new temp directory and stores as output
	 * directory.</b>
	 * 
	 * @return Output directory. Never {@code null}.
	 */
	public Path getOutputDirectoryOrCreateTemp() throws IOException {
		if (outputDirectory == null) {
			outputDirectory = Files.createTempDirectory("PLCverif");
		}
		return outputDirectory;
	}

	/**
	 * Returns the program directory of PLCverif. The relative paths to external
	 * tools will be resolved using this directory.
	 * <p>
	 * If not set, it falls back to the current working directory
	 * ({@code Paths.get(".")}).
	 * 
	 * @return Program directory. Never {@code null}.
	 */
	public Path getProgramDirectory() {
		return programDirectory;
	}

	/**
	 * Sets the program directory of PLCverif. The relative paths to external
	 * tools will be resolved using this directory.
	 * 
	 * @param programDirectory
	 *            Program directory. Shall not be {@code null}.
	 * @throws NullPointerException
	 *             if the given path is null.
	 */
	public void setProgramDirectory(Path programDirectory) {
		this.programDirectory = Preconditions.checkNotNull(programDirectory);
	}

	/**
	 * Makes the given {@code path} absolute compared to the program directory.
	 * <ul>
	 * <li>If {@code path} is already absolute, it is returned without any
	 * change.
	 * <li>If the program directory is not set or if it is not absolute,
	 * {@code path} is returned, made absolute using the default directory.
	 * <li>Otherwise the {@code path} is resolved using the program directory.
	 * </ul>
	 * 
	 * @param path
	 *            The path to be made absolute.
	 * @return Absolute path
	 */
	public Path makeAbsoluteToProgramDirectory(Path path) {
		if (path.isAbsolute() || getProgramDirectory() == null) {
			return path.toAbsolutePath();
		}
		return getProgramDirectory().resolve(path).toAbsolutePath();
	}

	// Logging
	/**
	 * Returns the names of the job stages in the order they were opened.
	 */
	public List<String> getStageNames() {
		return stages.stream().map(log -> log.getStageName()).collect(Collectors.toList());
	}

	/**
	 * Returns the stages in the order they were opened.
	 */
	public List<JobStage> getAllStages() {
		return ImmutableList.copyOf(stages);
	}

	/**
	 * Returns the stages having the given stage tag, in the order they were
	 * opened. If no such stage found, it returns an empty collection.
	 * 
	 * @return Stages with the given tag. Never null.
	 */
	public List<JobStage> getStages(StageTag tag) {
		Preconditions.checkNotNull(tag);
		List<JobStage> ret = stages.stream().filter(it -> it.hasTag(tag)).collect(Collectors.toList());
		Preconditions.checkState(ret != null);
		return ret;
	}

	/**
	 * Returns the stage named {@code stage}.
	 */
	public JobStage getStage(String stage) {
		return jobStages.get(stage);
	}

	// WISHLIST It may be nice to have a hierarchical log/stage with a stack.
	/**
	 * This method switches the current stage to the one named {@code name}. If
	 * there exists a stage already with {@code name}, the same stage will be
	 * reopened. If not, a new stage with the given {@code name} is created.
	 * 
	 * If there is another stage already open (i.e., {@code currentStage} is not
	 * {@code null}), the previous stage will be ended using its
	 * {@link JobStage#endStage()} method. Also, if its stage status is
	 * {@link StageStatus#Unknown}, it will be set to
	 * {@link StageStatus#Successful}.
	 */
	public JobStage switchToStage(String stageName, StageTag... tags) {
		JobStage newStage;
		if (getStage(stageName) == null) {
			newStage = new JobStage(stageName, tags);
			stages.add(newStage);
			jobStages.put(newStage.getStageName(), newStage);
		} else {
			newStage = getStage(stageName);
			if (tags != null) {
				// Check if it has the given tags
				for (StageTag tag : Arrays.asList(tags)) {
					if (!newStage.hasTag(tag)) {
						newStage.logWarning("The already existing stage item does not have the tag '%'.",
								tag.getName());
					}
				}
			}
		}

		// End previous stage if there is any
		if (currentStage != null) {
			JobStage previousStage = getStage(currentStage);
			previousStage.endStage();
		}

		// Print current stage name to console if needed
		Optional<JobSettings> specificSettings = getSpecificSettings();
		if (specificSettings.isPresent() && specificSettings.get().isShowProgress()) {
			if (stageName != null && !"null".equals(stageName) && !stageName.isEmpty()) {
				System.out.printf("== %s ==%n", stageName);
			}
		}
		
		// Start stage (for time measurement)
		newStage.startStage();

		// Progress report (if needed)
		if (progressReporter != null) {
			progressReporter.reportTask(stageName);
		}

		this.currentStage = stageName;
		return newStage;
	}

	/**
	 * Returns the currently open stage (and opens a dummy one if none was
	 * open).
	 */
	public JobStage currentStage() {
		if (currentStage == null && !jobStages.containsKey("null")) {
			JobStage nullStage = switchToStage("null");
			nullStage.endStage();
		}
		return jobStages.get(currentStage);
	}

	/**
	 * Returns true iff there is an open stage.
	 * 
	 * @return True if there is an open stage.
	 */
	public boolean isStageOpen() {
		return currentStage != null;
	}

	public void setProgressReporter(IProgressReporter progressReporter) {
		this.progressReporter = progressReporter;
	}

	/**
	 * Returns true iff there is at least one log item in any of the stages with
	 * {@link PlcverifSeverity#Error} severity.
	 * 
	 * @return True iff there was an error.
	 */
	public boolean hasError() {
		return getAllStages().stream().filter(it -> !(it.getLogItems(PlcverifSeverity.Error).isEmpty())).count() > 0;
	}

	/**
	 * Returns the key-value store that can be used by the plug-ins.
	 * 
	 * @return Key-value store. Never {@code null}.
	 */
	public PluginKeyValueStore getValueStore() {
		return keyValueStore;
	}

	// Artifacts to persist
	/**
	 * Returns the collection of artifacts to save.
	 */
	public List<OutputFileToSave> getOutputFilesToSave() {
		return Collections.unmodifiableList(filesToSave);
	}

	/**
	 * Registers a new file to be created.
	 * 
	 * @param filename
	 *            Name of the file (<b>relative</b> path). Shall not be
	 *            {@code null}.
	 * @param origin
	 *            ID of the creator of the file. Shall not be {@code null}.
	 * @param content
	 *            Content of the file. Shall not be {@code null}.
	 * @throws IllegalArgumentException
	 *             if there is already a file registered with the same name
	 *             (case-insensitive).
	 */
	public void addFileToSave(String filename, String origin, String content) {
		Preconditions.checkNotNull(filename, "filename");
		Preconditions.checkNotNull(origin, "origin");
		Preconditions.checkNotNull(content, "content");

		// Check for duplicates
		for (OutputFileToSave file : filesToSave) {
			if (file.getFilename().equalsIgnoreCase(filename)) {
				throw new IllegalArgumentException(String.format(
						"A file with the name '%s' has already been registered by '%s'.", filename, file.getOrigin()));
			}
		}

		filesToSave.add(new OutputFileToSave(filename, origin, content));
	}
}
