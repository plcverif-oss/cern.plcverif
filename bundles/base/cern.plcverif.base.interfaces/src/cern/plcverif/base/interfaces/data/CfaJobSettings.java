/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.interfaces.data;

import static cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory.MANDATORY;
import static cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory.OPTIONAL;

import java.util.Collections;
import java.util.List;

import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsElement;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsList;

public class CfaJobSettings extends BasicJobSettings {
	public static final String ID = "id";
	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";
	public static final String SOURCE_FILES = "sourcefiles";
	public static final String ENCODING = "encoding";
	public static final String LANGUAGE_FRONTEND = "lf";
	public static final String INLINE = "inline";
	public static final String REDUCTIONS = "reductions";

	@PlcverifSettingsElement(name = ID, description = "ID of the job definition.", mandatory = MANDATORY)
	private String id;

	@PlcverifSettingsElement(name = NAME, description = "Name of the job definition.", mandatory = OPTIONAL)
	private String name = "unnamed";

	@PlcverifSettingsElement(name = DESCRIPTION, description = "Description of the job definition.", mandatory = OPTIONAL)
	private String description = "";

	@PlcverifSettingsList(name = SOURCE_FILES, 
			description = "List of files containing the input artifacts. The paths can be relative to the current working directory, and may use wildcards.", 
			mandatory = MANDATORY)
	private List<String> sourceFiles;

	@PlcverifSettingsList(name = ENCODING, description = "Encoding to be used to load the input artifacts. If omitted, the local default will be used.", mandatory = OPTIONAL)
	private List<String> encoding;

	@PlcverifSettingsElement(name = LANGUAGE_FRONTEND, description = "The language frontend to be used.", mandatory = OPTIONAL)
	private String languageFrontend = null;

	@PlcverifSettingsElement(name = INLINE, description = "Forcing CFA inlining. If true, calls will be substituted with the callees, if possible. " +
			"This may be requested by the verification backend too.", mandatory = OPTIONAL)
	private boolean inline = false;

	@PlcverifSettingsList(name = REDUCTIONS, description = "List of reductions to apply. If omitted, all reductions will be used.", mandatory = OPTIONAL)
	private List<String> reductions = Collections.emptyList();

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public List<String> getSourceFiles() {
		return sourceFiles;
	}

	public List<String> getEncoding() {
		return encoding;
	}

	/**
	 * Can be null if not specified in the input.
	 */
	public String getLanguageFrontend() {
		return languageFrontend;
	}

	public boolean isInline() {
		return inline;
	}

	public List<String> getReductions() {
		return reductions;
	}
}
