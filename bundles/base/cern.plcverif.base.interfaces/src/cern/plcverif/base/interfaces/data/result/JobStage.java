/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.interfaces.data.result;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.logging.IPlcverifLogger;
import cern.plcverif.base.common.logging.PlcverifSeverity;

/**
 * This class represents one of the steps of a job, such as input parsing or
 * backend execution. It contains essential information about the stage (name,
 * status, duration, etc.) and logged messages.
 * 
 */
public final class JobStage implements IPlcverifLogger {
	// Metadata
	private String stageName;
	private StageStatus status;
	private String summary;
	private Set<StageTag> tags = new HashSet<>();

	// Stage duration
	private long lengthNs = 0;
	private long startTime;
	private Date time; // FIXME change it to LocalDateTime or ZonedDateTime

	// Log
	private final List<LogItem> logItems = new ArrayList<>();
	private boolean logWarningsToConsole = true;

	public JobStage(String stage, StageTag... tags) {
		this(stage, StageStatus.Unknown, "", tags);
	}

	public JobStage(String stage, StageStatus status, StageTag... tags) {
		this(stage, status, "", tags);
	}

	public JobStage(String stageName, StageStatus status, String summary, StageTag... tags) {
		super();
		Preconditions.checkNotNull(stageName);
		Preconditions.checkNotNull(summary);

		this.stageName = stageName;
		this.status = status;
		this.summary = summary;
		this.time = new Date();

		if (tags != null) {
			this.tags.addAll(Arrays.asList(tags));
		}
	}

	public boolean isLogWarningsToConsole() {
		return logWarningsToConsole;
	}

	public void setLogWarningsToConsole(boolean logWarningsToConsole) {
		this.logWarningsToConsole = logWarningsToConsole;
	}

	public String getStageName() {
		return this.stageName;
	}

	public void setStageName(String stage) {
		this.stageName = stage;
	}

	public StageStatus getStatus() {
		return this.status;
	}

	public void setStatus(StageStatus status) {
		this.status = status;
	}

	public String getSummary() {
		return this.summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public boolean hasTag(StageTag tag) {
		return this.tags.contains(tag);
	}

	public Date getTime() {
		return this.time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public long getLengthNs() {
		return this.lengthNs;
	}

	public long getLengthMs() {
		return this.lengthNs / 1_000_000L;
	}
	
	public int getLengthS() {
		return (int) Math.ceil((double)this.lengthNs / (double) 1_000_000_000);
	}

	public void setLengthNs(long lengthNs) {
		this.lengthNs = lengthNs;
	}

	public void log(final LogItem logItem) {
		Preconditions.checkNotNull(logItem, "logItem");
		this.logItems.add(logItem);

		if (this.logWarningsToConsole && logItem.severityAtLeast(PlcverifSeverity.Warning)) {
			System.out.println(String.format("%s: %s [%s]", logItem.getSeverity().toString(), logItem.getMessage(),
					logItem.getStage().getStageName()));
		}
	}

	public void log(PlcverifSeverity severity, String messageFormat, Object... args) {
		log(LogItem.create(this, severity, String.format(messageFormat, args)));
	}

	public void log(PlcverifSeverity severity, String message, Throwable throwable) {
		log(LogItem.create(this, severity, message, throwable));
	}

	/**
	 * Logs the given log message <i>only if</i> there is no log item with the
	 * same message and severity already logged for the current stage.
	 * 
	 * @param severity
	 *            Severity of the log item.
	 * @param message
	 *            Message of the log item.
	 * @param throwable
	 *            Exception attached to the log item. Optional, may be null.
	 */
	private void logAtMostOnce(PlcverifSeverity severity, String message, Throwable throwable) {
		if (this.logItems.stream().filter(it -> it.getSeverity() == severity && it.getMessage().equals(message))
				.count() == 0) {
			log(LogItem.create(this, severity, message, throwable));
		}
	}

	/**
	 * Logs the given log message <i>only if</i> there is no log item with the
	 * same message and Warning severity already logged for the current stage.
	 * 
	 * @param message
	 *            Message (format) of the log item.
	 * @param args
	 *            Arguments referenced by the format specifiers in the format
	 *            string
	 */
	public void logWarningAtMostOnce(String message, Object... args) {
		logAtMostOnce(PlcverifSeverity.Warning, String.format(message, args), null);
	}

	/**
	 * Logs the given log message <i>only if</i> there is no log item with the
	 * same message and Error severity already logged for the current stage.
	 * 
	 * @param message
	 *            Message (format) of the log item.
	 * @param args
	 *            Arguments referenced by the format specifiers in the format
	 *            string
	 */
	public void logErrorAtMostOnce(String message, Object... args) {
		logAtMostOnce(PlcverifSeverity.Error, String.format(message, args), null);
	}

	public List<LogItem> getLogItems() {
		return Collections.unmodifiableList(this.logItems);
	}

	public List<LogItem> getLogItems(final PlcverifSeverity minimumSeverity) {
		final List<LogItem> ret = new ArrayList<>();
		for (final LogItem item : this.logItems) {
			if (item.severityAtLeast(minimumSeverity)) {
				ret.add(item);
			}
		}
		return ret;
	}

	/**
	 * Starts the stage.
	 * <p>
	 * Stores the current time to calculate the duration of the stage. An ended
	 * stage can be restarted many times with {@link #startStage()}. In this
	 * case, the start time (see {@link #getTime()}) will not be modified, but
	 * the total duration will be correctly calculated (it will be the sum of
	 * the durations between the respective {@link #startStage()} and
	 * {@link #endStage()} calls).
	 */
	public void startStage() {
		this.startTime = System.nanoTime();
	}

	/**
	 * Ends this stage.
	 * <p>
	 * The duration of the stage is calculated and stored. Furthermore, if the
	 * stage's status was unknown, it will be changed to
	 * {@link StageStatus#Successful}, i.e., it is assumed that a stage was
	 * successful if not marked explicitly as unsuccessful.
	 */
	public void endStage() {
		if (this.getStatus() == StageStatus.Unknown) {
			// Implicitly assuming that the previous stage was successful if
			// not indicated otherwise
			endStage(StageStatus.Successful);
		} else {
			endStage(this.status);
		}
	}

	/**
	 * Ends this stage with the given status.
	 * 
	 * @param status
	 *            Status of this stage
	 */
	public void endStage(StageStatus status) {
		this.status = status;
		this.lengthNs += System.nanoTime() - this.startTime;
	}

	/**
	 * Returns the total length (in ms) of the given stages, i.e., the sum of
	 * the values returned by {@link #getLengthMs()} of each given stage.
	 * 
	 * @param stages
	 *            Stages to compute the total length
	 * @return Total length (in ms)
	 */
	public static final long sumLengthMs(Collection<JobStage> stages) {
		return stages.stream().mapToLong(it -> it.getLengthMs()).sum();
	}
	public static final int sumLengthS(Collection<JobStage> stages) {
		return stages.stream().mapToInt(it -> it.getLengthS()).sum();
	}
}
