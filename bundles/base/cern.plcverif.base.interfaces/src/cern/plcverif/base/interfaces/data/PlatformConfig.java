/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.interfaces.data;

import java.nio.file.Path;
import java.nio.file.Paths;

import com.google.common.base.Preconditions;

public final class PlatformConfig {
	private static final String SETTINGS_DIRECTORY = "settings";
	
	private Path programDirectory;
	
	public PlatformConfig(Path programDirectory) {
		Preconditions.checkNotNull(programDirectory, "programDirectory is null.");
		
		this.programDirectory = programDirectory;
	}
	
	public PlatformConfig() {
		this.programDirectory = Paths.get(".");
	}
	
	public Path getProgramDirectory() {
		return programDirectory;
	}
	
	public Path getProgramSettingsDirectory() {
		return programDirectory.resolve(SETTINGS_DIRECTORY);
	}
}
