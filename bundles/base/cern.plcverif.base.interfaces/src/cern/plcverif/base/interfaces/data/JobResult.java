/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Vince Molnar - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.interfaces.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.interfaces.IParserLazyResult;
import cern.plcverif.base.interfaces.data.result.JobStage;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.cfa.trace.IVariableTraceModel;
import cern.plcverif.base.models.cfa.transformation.CfaTransformationGoal;

/**
 * This class aggregates the results of the operations related to the executable
 * and the processing of the input model(s). Included data (for details, see the
 * corresponding getter):
 * <p>
 * <ul>
 * <li>A {@link Settings} description that can be used to replicate the job (see
 * {@link #getSettings()})</li>
 * <li>A {@link JobMetadata} describing the job (see
 * {@link #getJobMetadata()})</li>
 * <li>A list of files used as input (see {@link #getInputFiles()})</li>
 * <li>An {@link IParserLazyResult} that can be used to parse and serialize
 * texts related to the parsed model (see {@link #getParserResult()})</li>
 * <li>A {@link CfaTransformationGoal} that is specified by the job and logged
 * here (see {@link #getTransformationGoal()})</li>
 * <li>A boolean indicating if call inlining has been performed (see
 * {@link #isInlined()})</li>
 * <li>The {@link CfaNetworkDeclaration} model that has been parsed by the
 * language frontend and possibly extended/reduced by the job (see
 * {@link #getCfaDeclaration()})</li>
 * <li>The {@link CfaNetworkInstance} model that may have been instantiated from
 * the CFA declaration model depending on the transformation goal (see
 * {@link #getCfaInstance()})</li>
 * <li>An {@link IVariableTraceModel} instance capable of translating between
 * data/variable references of the declaration and the instance representation,
 * if any (see {@link #getVariableTrace()})</li>
 * <li>A list of (filename, content) pairs that are intented to be persisted,
 * e.g. when running as a console application (see
 * {@link #getOutputFilesToSave()})</li>
 * </ul>
 * <p>
 * The class also provides logging capability (see {@link JobStage}).
 */
public abstract class JobResult extends BasicJobResult {
	// Job details
	private JobMetadata jobMetadata = new JobMetadata();

	// Parsing
	private List<String> inputFiles = new ArrayList<>();
	private IParserLazyResult parserResult;

	// Model & tracing
	private CfaTransformationGoal transformationGoal = null;
	private boolean inlined;
	private CfaNetworkDeclaration cfaDeclaration;
	private Optional<CfaNetworkInstance> cfaInstance = Optional.empty();
	private Optional<IVariableTraceModel> variableTrace = Optional.empty();

	// Reductions
	// NOTE Currently nothing, but maybe we need some data here as well (don't forget to extend the
	// javadoc of the class).

	// Job details
	/**
	 * Returns the {@link JobMetadata} describing the job.
	 * 
	 * @return Metadata of job. Never {@code null}.
	 */
	public JobMetadata getJobMetadata() {
		return jobMetadata;
	}

	/**
	 * Stores the {@link JobMetadata} describing the job.
	 * 
	 * @param jobMetadata
	 *            Metadata of job. Shall not be {@code null}.
	 * @throws NullPointerException
	 *             if given metadata object is {@code null}.
	 * @see JobMetadata
	 */
	public void setJobMetadata(JobMetadata jobMetadata) {
		Preconditions.checkNotNull(jobMetadata);
		this.jobMetadata = jobMetadata;
	}

	// Parsing
	// -------

	/**
	 * Returns the list of files used as input.
	 * 
	 * @return List of input files. Never {@code null}.
	 */
	public List<String> getInputFiles() {
		return inputFiles;
	}

	/**
	 * Stores the list of input files used as input
	 * 
	 * @param inputFiles
	 *            List of input files. Shall not be {@code null}.
	 * @throws NullPointerException
	 *             if the given list is {@code null}.
	 */
	public void setInputFiles(List<String> inputFiles) {
		this.inputFiles = Preconditions.checkNotNull(inputFiles);
	}

	public void setParserResult(IParserLazyResult parserResult) {
		this.parserResult = parserResult;
	}

	/**
	 * Returns the {@link IParserLazyResult} that can be used to parse and
	 * serialize texts related to the parsed model.
	 */
	public IParserLazyResult getParserResult() {
		return parserResult;
	}

	// Model & tracing
	/**
	 * Sets the transformation goal that was specified for the job.
	 * 
	 * @param transformationGoal
	 *            CFA transformation goal. Shall not be {@code null}.
	 */
	public void setTransformationGoal(CfaTransformationGoal transformationGoal) {
		this.transformationGoal = Preconditions.checkNotNull(transformationGoal);
	}

	/**
	 * Returns the {@link CfaTransformationGoal} that has been specified by the
	 * job and logged here.
	 * 
	 * @return CFA transformation goal. Can be {@code null} if it was not set.
	 */
	public CfaTransformationGoal getTransformationGoal() {
		return transformationGoal;
	}

	public void setInlined(boolean inlined) {
		this.inlined = inlined;
	}

	/**
	 * True if call inlining has been performed.
	 */
	public boolean isInlined() {
		return inlined;
	}

	/**
	 * Stores the declaration CFA that corresponds to the parsed sources. This
	 * CFA may have been reduced and possibly extended by the job.
	 * 
	 * @param cfaDeclaration
	 *            CFA declaration to store. Shall not be {@code null}.
	 */
	public void setCfaDeclaration(CfaNetworkDeclaration cfaDeclaration) {
		this.cfaDeclaration = Preconditions.checkNotNull(cfaDeclaration);
	}

	/**
	 * Returns the {@link CfaNetworkDeclaration} model that has been parsed by
	 * the language frontend and possibly extended/reduced by the job.
	 * 
	 * @return CFA declaration. May be {@code null} if it was not set.
	 */
	public CfaNetworkDeclaration getCfaDeclaration() {
		return cfaDeclaration;
	}

	/**
	 * Stores the instance CFA that corresponds to the parsed sources. This CFA
	 * may have been reduced and possibly extended by the job.
	 * 
	 * @param cfaInstance
	 *            CFA instance to store. Shall not be {@code null}.
	 */
	public void setCfaInstance(CfaNetworkInstance cfaInstance) {
		this.cfaInstance = Optional.of(Preconditions.checkNotNull(cfaInstance,
				"The cfaInstance to be stored in JobResult shall not be null."));
	}

	/**
	 * Returns the {@link CfaNetworkInstance} model that may have been
	 * instantiated from the CFA declaration model depending on the
	 * transformation goal.
	 * 
	 * @return The CFA instance. It is {@link Optional#empty()} if the model was
	 *         never instantiated. Never {@code null}.
	 */
	public Optional<CfaNetworkInstance> getCfaInstance() {
		return Preconditions.checkNotNull(cfaInstance);
	}

	/**
	 * Stores the variable trace between declaration and instance CFA.
	 * @param variableTrace Variable trace to be stored. Shall not be {@code null}.
	 */
	public void setVariableTrace(IVariableTraceModel variableTrace) {
		this.variableTrace = Optional.of(Preconditions.checkNotNull(variableTrace));
	}

	/**
	 * Returns the {@link IVariableTraceModel} instance capable of translating
	 * between data/variable references of the declaration and the instance
	 * representation.
	 * @return Variable trace model. It is {@link Optional#empty()} if the model was
	 *         never instantiated. Never {@code null}.
	 */
	public Optional<IVariableTraceModel> getVariableTrace() {
		return Preconditions.checkNotNull(variableTrace);
	}
}