/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.interfaces.data;

import java.nio.file.Path;

import cern.plcverif.base.interfaces.IBasicJob;

public abstract class AbstractBasicJob implements IBasicJob {
	private Path outputDirectory;
	private Path programDirectory;
	
	/**
	 * Returns the output directory to be used.
	 * @return Output directory. May be {@code null}.
	 */
	@Override
	public Path getOutputDirectory() {
		return outputDirectory;
	}

	@Override
	public void setOutputDirectory(Path outputDirectory) {
		this.outputDirectory = outputDirectory;
	}

	/**
	 * Returns the PLCverif program directory.
	 * @return PLCverif program directory. May be {@code null}.
	 */
	@Override
	public Path getProgramDirectory() {
		return programDirectory;
	}

	@Override
	public void setProgramDirectory(Path programDirectory) {
		this.programDirectory = programDirectory;
	}
}
