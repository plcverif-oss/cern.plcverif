/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.interfaces;

import java.util.Set;

import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.interfaces.exceptions.AstParsingException;
import cern.plcverif.base.interfaces.exceptions.AtomParsingException;
import cern.plcverif.base.interfaces.exceptions.CfaGenerationException;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef;
import cern.plcverif.base.models.cfa.cfadeclaration.FieldAnnotation;
import cern.plcverif.base.models.cfa.cfadeclaration.OriginalDataTypeFieldAnnotation;
import cern.plcverif.base.models.expr.AtomicExpression;
import cern.plcverif.base.models.expr.Literal;

/**
 * Implementors of this interface should provide additional services related to
 * a parsed input. These services include the (on-demand) generation of a CFA
 * network declaration representation, and additional parsing and serializing
 * tasks related to atomic expressions. The latter will be used to parse
 * potential requirements and to visualize the results of jobs.
 */
public interface IParserLazyResult {
	/**
	 * Should return the paths of all files used while parsing the input mapped
	 * to the corresponding filenames.
	 */
	public Set<String> getFileNames();

	/**
	 * Should return the parsed AST wrapped in an {@link IAstReference}. This
	 * method will be used by static analysis components specifically created
	 * for the AST of the implementing language frontend.
	 * 
	 * @throws AstParsingException
	 *             if it was unsuccessful to load the AST
	 */
	public IAstReference<?> getAst() throws AstParsingException;

	/**
	 * Should generate a CFA network declaration representation of the parsed
	 * AST. The recommended way of implementing this method is to perform the
	 * transformation when this function is called (and maybe cache it), because
	 * some jobs may not be interested in the CFA representation.
	 * <p>
	 * Clients should not assume that this is a getter method. Ideally, this
	 * method would be called only once for each instance, the returned model
	 * should be saved for future use.
	 * 
	 * @throws AstParsingException
	 *             if it was unsuccessful to load the AST
	 * @throws CfaGenerationException
	 *             if it was unsuccessful to build the CFA based on the AST
	 */
	public CfaNetworkDeclaration generateCfa(JobResult result) throws AstParsingException, CfaGenerationException;

	/**
	 * Should parse a string representation of an atomic expression (which is
	 * expected to be a {@link Literal} value or {@link DataRef} only).
	 * <p>
	 * The type of literals should be inferred whenever possible.
	 * <p>
	 * In case of references, the returned reference should refer to already
	 * existing fields of the CFA model and <b>is expected to be a global
	 * reference</b> starting in the global root data structure of the parsed
	 * {@link CfaNetworkDeclaration}.
	 * 
	 * @throws AtomParsingException
	 *             if it is not possible to parse the given atom.
	 */
	public AtomicExpression parseAtom(String atom) throws AtomParsingException;

	/**
	 * Should convert an atomic expression into a textual representation (the
	 * provided expression is expected to be a {@link Literal} value or a
	 * {@link DataRef} only).
	 * <p>
	 * Callers should guarantee that provided references are global and they
	 * refer to fields in the original {@link CfaNetworkDeclaration} model.
	 * Implementors are free to rely on {@link FieldAnnotation} instances
	 * annotated on the original Fields to interpret the references in their
	 * original context.
	 * 
	 * @param atom
	 *            Atomic expression (literal or data reference) to be textually
	 *            represented.
	 * @return The given atom in a textual representation
	 */
	public String serializeAtom(AtomicExpression atom);

	/**
	 * Should convert an atomic expression into a textual representation (the
	 * provided expression is expected to be a {@link Literal} value or a
	 * {@link DataRef} only). If an original data type field annotation is
	 * provided as hint, it will be potentially taken into account when
	 * representing the literal.
	 * <p>
	 * Callers should guarantee that provided references are global and they
	 * refer to fields in the original {@link CfaNetworkDeclaration} model.
	 * Implementors are free to rely on {@link FieldAnnotation} instances
	 * annotated on the original Fields to interpret the references in their
	 * original context.
	 * 
	 * @param atom
	 *            Atomic expression (literal or data reference) to be textually
	 *            represented.
	 * @param hint
	 *            Annotation representing the original data type of the given
	 *            literal. If not provided (is {@code null}), it will fall back
	 *            to {@link #serializeAtom(AtomicExpression)}.
	 * @return The given atom in a textual representation, taking the hint into
	 *         account if possible
	 */
	public String serializeAtom(AtomicExpression atom, OriginalDataTypeFieldAnnotation hint);
}
