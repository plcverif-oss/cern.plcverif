/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.interfaces;

/**
 * AST references hide the root of Abstract Syntax Tree models, defined and
 * produced by language frontends (specifically, {@link IParser} implementors).
 * This interface provides a way to provide a typed access to the AST models for
 * plugins interested in a specific type of AST (for example, for static
 * analysis).
 */
public interface IAstReference<T> {
	/**
	 * Should return the root of the AST model.
	 */
	public T getAst();
}
