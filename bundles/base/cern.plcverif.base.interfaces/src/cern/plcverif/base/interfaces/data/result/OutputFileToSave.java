/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.interfaces.data.result;

import com.google.common.base.Preconditions;

/**
 * An output file that is to be persisted or presented otherwise to the user.
 */
public final class OutputFileToSave {
	private String filename;
	private String origin;
	private String content;

	/**
	 * Creates a new output file representation
	 * @param filename Relative file name of the output text file. Shall not be {@code null}.
	 * @param origin String identifier of the file's creator. Shall not be {@code null}.
	 * @param content Textual content of the output file. Shall not be {@code null}.
	 */
	public OutputFileToSave(String filename, String origin, String content) {
		this.filename = Preconditions.checkNotNull(filename);
		this.origin = Preconditions.checkNotNull(origin);
		this.content = Preconditions.checkNotNull(content);
	}

	/**
	 * Returns the relative file name of the output text file.
	 * 
	 * @return Relative file name. Never {@code null}.
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * Returns the string identifier of the file's creator.
	 * 
	 * @return The ID of the file's creator. Never {@code null}.
	 */
	public String getOrigin() {
		return origin;
	}

	/**
	 * The textual content of the output file.
	 * 
	 * @return Text content. Never {@code null}.
	 */
	public String getContent() {
		return content;
	}
}