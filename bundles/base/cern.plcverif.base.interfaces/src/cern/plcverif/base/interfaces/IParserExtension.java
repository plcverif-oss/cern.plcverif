/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.interfaces;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import cern.plcverif.base.common.extension.ExtensionPoint;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.help.SettingsHelp;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;

/**
 * Implementors of this interface are extension points serving as a factory for
 * {@link IParser} instances. A parser should be able to turn a certain type of
 * input (specified by the parser itself, e.g. PLC code) into an AST (see
 * {@link IAstReference}) and then a {@link CfaNetworkDeclaration}.
 * <p>
 * Implementors must realize an Extension for the extension point
 * cern.plcverif.base.extensions.parser.
 */
@ExtensionPoint(extensionPointId = "cern.plcverif.base.extensions.parser")
public interface IParserExtension {
	/**
	 * This method is part of an intelligent parser selection approach. In case
	 * this parser extension is able to parse the specified {@code files} (given
	 * as filename and content pairs in the map), it should return a parser
	 * already configured in a way that parsing is possible. Otherwise it should
	 * return {@link Optional#empty}.
	 */
	public Optional<IParser> getDefaultParser(Map<String, String> files);

	/**
	 * Should create an {@link IParser} instance with the default configuration.
	 * The result is expected to be used by client code to programmatically
	 * configure a job.
	 */
	public IParser createParser();

	/**
	 * Should create an {@link IParser} preconfigured according to the contents
	 * of the provided settings description.
	 * <p>
	 * The result is expected to be used as returned, so every setting has to be
	 * properly set. If this is not possible based on the provided settings
	 * description, the method should throw a {@link SettingsParserException}.
	 */
	public IParser createParser(SettingsElement settings) throws SettingsParserException;

	/**
	 * Fills the given {@link SettingsHelp} object with the settings accepted by
	 * this extension (recursively) and their description.
	 */
	public void fillSettingsHelp(SettingsHelp help);

	/**
	 * Returns a list of (case insensitive) extensions that are potentially
	 * supported by this language frontend. If it is not applicable, return an
	 * empty list. The language frontend plug-in cannot assume that only files
	 * with this extension will be given for parsing, this is only used to
	 * filter files on the GUI.
	 * 
	 * @return List of supported extensions (case insensitive). Empty list if
	 *         not applicable. Shall not be {@code null}.
	 */
	public List<String> getSupportedExtensions();
}
