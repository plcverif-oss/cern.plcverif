/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.platform;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.extension.ExtensionHelper;
import cern.plcverif.base.common.logging.PlatformLogger;
import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsSerializer;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.help.ConsoleHelpWriter;
import cern.plcverif.base.common.settings.help.SettingsHelp;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.base.interfaces.IBasicJob;
import cern.plcverif.base.interfaces.ICfaJob;
import cern.plcverif.base.interfaces.IJobExtension;
import cern.plcverif.base.interfaces.IParser;
import cern.plcverif.base.interfaces.IParserExtension;
import cern.plcverif.base.interfaces.IReduction;
import cern.plcverif.base.interfaces.IReductionExtension;
import cern.plcverif.base.interfaces.data.BasicJobSettings;
import cern.plcverif.base.interfaces.data.CfaJobSettings;
import cern.plcverif.base.interfaces.data.JobMetadata;
import cern.plcverif.base.interfaces.data.ModelTasks;
import cern.plcverif.base.interfaces.data.PlatformConfig;

/**
 * The entry point of the API of PLCverif. This class can be used to query
 * available plugins directly providing/extending the functionality of PLCverif.
 * <p>
 * The main functionality of this class is to create or parse an {@link ICfaJob}
 * implementing a specific functionality. When parsing from a {@link Settings}
 * object, the platform will handle common settings and configure the
 * {@link ICfaJob} accordingly.
 */
public final class Platform {
	private static final String OUTPUT_DIR_DATE_FORMAT_PATTERN = "yyyy-MM-dd_HH-mm-ss";

	private PlatformConfig platformConfig;

	private Platform(PlatformConfig platformConfig) {
		this.platformConfig = platformConfig;
	}

	/**
	 * Creates a new platform with the given configuration.
	 * 
	 * @param config
	 *            Platform configuration
	 * @return The platform instance
	 */
	public static Platform createPlatform(PlatformConfig config) {
		return new Platform(config);
	}

	private static Map<String, IParserExtension> getParsers() {
		return ExtensionHelper.getExtensions(IParserExtension.class);
	}

	private static Map<String, IReductionExtension> getReductions() {
		return ExtensionHelper.getExtensions(IReductionExtension.class);
	}

	private static Map<String, IJobExtension> getJobs() {
		return ExtensionHelper.getExtensions(IJobExtension.class);
	}

	/**
	 * Returns the configuration of the current platform.
	 */
	public PlatformConfig getPlatformConfig() {
		return platformConfig;
	}

	/**
	 * Prints help message about the available settings (including the settings
	 * accepted by the loaded extensions, and their extensions, transitively) to
	 * the standard output.
	 */
	public void printSettingsHelp() {
		try (PrintWriter printWriter = new PrintWriter(System.out)) {
			SettingsHelp help = new SettingsHelp(new ConsoleHelpWriter(printWriter));
			fillSettingsHelp(help);
		}
	}

	/**
	 * Fills the given settings help object ({@code help}) with the settings
	 * accepted by the platform, or by the loaded job extensions and reduction
	 * extensions.
	 * 
	 * @param help
	 *            Settings help object to be filled.
	 */
	public void fillSettingsHelp(SettingsHelp help) {
		fillSettingsHelpForJobs(help, getJobs().values());
	}

	/**
	 * Fills the given settings help object ({@code help}) with the settings
	 * accepted by the platform, or by the given job extensions and the loaded
	 * reduction extensions.
	 * 
	 * @param help
	 *            Settings help object to be filled.
	 * @param jobs
	 *            The jobs to be documented. The settings of the loaded jobs
	 *            which are not included in the given collection will NOT be
	 *            included in the generated help.
	 */
	public void fillSettingsHelpForJobs(SettingsHelp help, Collection<IJobExtension> jobs) {
		Preconditions.checkNotNull(help, "help");
		Preconditions.checkNotNull(jobs, "jobs");

		fillSettingsHelp(help, CfaJobSettings.ID, null);
		fillSettingsHelp(help, CfaJobSettings.NAME, null);
		fillSettingsHelp(help, CfaJobSettings.DESCRIPTION, null);
		fillSettingsHelp(help, CfaJobSettings.JOB, new ArrayList<>(getJobs().keySet()));
		fillSettingsHelp(help, CfaJobSettings.LANGUAGE_FRONTEND, new ArrayList<>(getParsers().keySet()));
		fillSettingsHelp(help, CfaJobSettings.REDUCTIONS, new ArrayList<>(getReductions().keySet()));
		fillSettingsHelp(help, CfaJobSettings.INLINE, null);
		fillSettingsHelp(help, CfaJobSettings.SOURCE_FILES, null);
		fillSettingsHelp(help, CfaJobSettings.ENCODING, null);
		fillSettingsHelp(help, CfaJobSettings.OUTPUT_DIRECTORY, null);

		help.startPrefix(CfaJobSettings.LANGUAGE_FRONTEND);
		for (IParserExtension parser : getParsers().values()) {
			parser.fillSettingsHelp(help);
		}
		help.endPrefix();

		help.startPrefix(CfaJobSettings.JOB);
		for (IJobExtension job : jobs) {
			job.fillSettingsHelp(help);
		}
		help.endPrefix();

		help.startPrefixList(CfaJobSettings.REDUCTIONS);
		for (IReductionExtension reduction : getReductions().values()) {
			reduction.fillSettingsHelp(help);
		}
		help.endPrefix();
	}

	/**
	 * Fills the given settings help object to represent the given setting
	 * element (having the name {@code settingName}) of the
	 * {@code PlatformSpecificSettings} specific settings class. If defined, the
	 * {@code valueSet} parameter will be used to define the acceptable values
	 * of the setting element, otherwise automated inference of permitted values
	 * will be attempted.
	 * <p>
	 * Convenience wrapper method for
	 * {@link SpecificSettingsSerializer#fillSettingsHelpSingleParameter(SettingsHelp, Class, String, List)}.
	 * 
	 * @param help
	 *            The help object to be filled. Shall not be null.
	 * @param settingsClass
	 *            The specific settings class that contains the field to be
	 *            represented. Shall not be null.
	 * @param settingName
	 *            The name of the setting element that is to be represented.
	 *            Shall not be null.
	 * @param valueSet
	 *            Set of acceptable values for the field to be defined. May be
	 *            null.
	 */
	private static void fillSettingsHelp(SettingsHelp help, String settingsElementName, List<String> valueSet) {
		SpecificSettingsSerializer.fillSettingsHelpSingleParameter(help, CfaJobSettings.class, settingsElementName,
				valueSet);
	}

	// API

	public Set<String> listParsers() {
		return getParsers().keySet();
	}

	public IParserExtension getParserExtension(String parserId) {
		IParserExtension ret = null;
		ret = getParsers().getOrDefault(parserId, null);
		return ret;
	}

	public IParser createParser(String parserId) {
		IParser ret = null;
		IParserExtension frontend = getParsers().getOrDefault(parserId, null);
		if (frontend != null) {
			ret = frontend.createParser();
		}
		return ret;
	}

	/**
	 * Systematically queries the available language frontends if they can
	 * provides a configured parser for the provided files. This method is
	 * automatically invoked if no language frontend is specified in the
	 * {@link Settings} object describing the job.
	 * <p>
	 * <b>Returns {@link Optional#empty()} if none of the frontends could
	 * provide a parser.</b>
	 */
	public Optional<IParser> getDefaultParser(Map<String, String> files) {
		for (IParserExtension frontend : getParsers().values()) {
			Optional<IParser> ret = frontend.getDefaultParser(files);
			if (ret.isPresent()) {
				return ret;
			}
		}
		return Optional.empty();
	}

	public Set<String> listReductions() {
		return getReductions().keySet();
	}

	public IReductionExtension getReductionExtensions(String reductionId) {
		IReductionExtension ret = null;
		ret = getReductions().getOrDefault(reductionId, null);
		return ret;
	}

	public IReduction createReduction(String reductionId) {
		IReduction ret = null;
		IReductionExtension reduction = getReductions().getOrDefault(reductionId, null);
		if (reduction != null) {
			ret = reduction.createReduction();
		}
		return ret;
	}

	public Set<String> listJobExtensions() {
		return getJobs().keySet();
	}

	public IJobExtension getJobExtension(String jobId) {
		IJobExtension ret = null;
		ret = getJobs().getOrDefault(jobId, null);
		return ret;
	}

	public IBasicJob createJob(String jobId) {
		IBasicJob ret = null;
		IJobExtension job = getJobs().getOrDefault(jobId, null);
		if (job != null) {
			ret = job.createJob();
		}
		return ret;
	}

	public IBasicJob parseJob(SettingsElement settings) throws SettingsParserException {
		BasicJobSettings basicJobSettings = SpecificSettingsSerializer.parse(settings, BasicJobSettings.class);

		IBasicJob ret = parseDefinedJob(settings);

		Path outputDirectory = parseOutputDirectory(basicJobSettings);
		ret.setOutputDirectory(outputDirectory);
		ret.setProgramDirectory(platformConfig.getProgramDirectory());

		if (ret instanceof ICfaJob) {
			// Not only a basic job, it is a PLC-CFA job.
			// TODO review solution
			CfaJobSettings cfaJobSpecificSettings = SpecificSettingsSerializer.parse(settings, CfaJobSettings.class);
			parsePlcCfaJob(settings, cfaJobSpecificSettings, (ICfaJob) ret);
		}
		return ret;
	}

	private void parsePlcCfaJob(SettingsElement settings, CfaJobSettings specificSettings, ICfaJob plcCfaJob)
			throws SettingsParserException {
		JobMetadata metadata = parseJobMetadata(specificSettings);
		Map<String, String> sourceFiles = loadSourceFiles(settings);
		IParser parser = parseFrontend(sourceFiles, specificSettings);
		boolean inline = specificSettings.isInline();
		List<IReduction> reductions = parseReductions(settings);

		ModelTasks modelTasks = new ModelTasks();
		modelTasks.setParser(parser);
		modelTasks.setForceInline(inline);
		modelTasks.setReductions(reductions);

		plcCfaJob.setMetadata(metadata);
		plcCfaJob.setModelTasks(modelTasks);
	}

	private static JobMetadata parseJobMetadata(CfaJobSettings settings) {
		JobMetadata ret = new JobMetadata();

		ret.setId(settings.getId());
		ret.setName(settings.getName());
		ret.setDescription(settings.getDescription());

		return ret;
	}

	private static Map<String, String> loadSourceFiles(SettingsElement settings) throws SettingsParserException {
		Map<String, String> ret = new HashMap<>();
		Optional<Settings> fileSettings = settings.getAttribute(CfaJobSettings.SOURCE_FILES);
		if (!fileSettings.isPresent()) {
			throw new SettingsParserException("Input files not specified.");
		}

		for (Settings elem : fileSettings.get().toList().elements()) {
			if (elem == null) {
				continue;
			}
			
			String filename = elem.toSingle().value();
			if (filename == null) {
				throw new SettingsParserException(
						"No source file has been specified. " + "Specify source files using the `-%s` argument.",
						CfaJobSettings.SOURCE_FILES);
			}
			String encoding = null;
			Optional<Settings> encSettings = elem.toSingle().getAttribute(CfaJobSettings.ENCODING);
			if (encSettings.isPresent()) {
				encoding = encSettings.get().toSingle().value();
				if (encoding == null) {
					PlatformLogger.logError(
							"Encoding switch without assigned value encountered. Using system default encoding.");
				}
			}

			// Resolve file (it may contain wildcard)
			List<Path> filesToLoad = IoUtils.resolvePathsMatchingPattern(filename);
			if (filesToLoad.isEmpty()) {
				PlatformLogger.logWarning(String.format("The pattern '%s' did not match any file.%n", filename));
			}

			// Load each file based on the wildcard
			for (Path sourcePath : filesToLoad) {
				if (!sourcePath.toFile().exists()) {
					throw new SettingsParserException(
							"The following file does not exist: " + sourcePath.toAbsolutePath());
				}
				Charset charset = encoding == null ? Charset.defaultCharset() : Charset.forName(encoding);
				try {
					String content = new String(Files.readAllBytes(sourcePath), charset);
					ret.put(sourcePath.toString(), content);
				} catch (IOException e) {
					throw new SettingsParserException(e, "Could not open file: " + sourcePath.toAbsolutePath());
				}
			}
		}

		if (ret.isEmpty()) {
			throw new PlcverifPlatformException("No source file has been found.");
		}
		return ret;
	}

	private IParser parseFrontend(Map<String, String> files, CfaJobSettings settings) throws SettingsParserException {
		String frontendId = settings.getLanguageFrontend();
		if (frontendId == null) {
			PlatformLogger.logWarning("No language frontend specified. Trying to infer default parser.");
			for (Entry<String, IParserExtension> kvp : getParsers().entrySet()) {
				Optional<IParser> parser = kvp.getValue().getDefaultParser(files);
				if (parser.isPresent()) {
					return parser.get();
				}
			}
			throw new SettingsParserException(
					"None of the available language frontends could parse the provided files. Specify the frontend to use and its configuration or install a new frontend.");
		}

		IParserExtension frontend = getParsers().getOrDefault(frontendId, null);

		if (frontend == null) {
			throw new SettingsParserException(
					"Language frontend not found: %s. Specify a language frontend using the `-%s` argument. "
							+ "Currently available frontends: %s.",
					frontendId, CfaJobSettings.LANGUAGE_FRONTEND, String.join(", ", getParsers().keySet()));
		}

		SettingsElement currentSettings = settings.getRepresentedSettingsElement()
				.getAttribute(CfaJobSettings.LANGUAGE_FRONTEND).orElseThrow(() -> new IllegalStateException("Internal inconsistency.")).toSingle();
		// Load installation-specific settings from '<frontend_id>.settings'
		SettingsElement resultantSettings = SettingsSerializer.extendWithInstallationSettings(currentSettings,
				frontendId, platformConfig.getProgramSettingsDirectory());

		IParser ret;
		ret = frontend.createParser(resultantSettings);
		ret.setFiles(files);
		return ret;
	}

	private List<IReduction> parseReductions(SettingsElement settings) throws SettingsParserException {
		List<IReduction> ret = new ArrayList<>();
		Optional<Settings> reductionSettings = settings.getAttribute(CfaJobSettings.REDUCTIONS);
		if (!reductionSettings.isPresent()) {
			// Default set of reductions
			PlatformLogger.logInfo("No reductions specified. Using all defined reductions: "
					+ String.join(", ", getReductions().keySet()));
			for (String reductionId : getReductions().keySet()) {
				ret.add(instantiateAndConfigureReduction(reductionId, Optional.empty()));
			}
			return ret;
		} else {
			// Use only the specified reductions
			for (Settings elem : reductionSettings.get().toList().elements()) {
				if (elem == null) {
					continue;
				}
				String reductionId = elem.toSingle().value();
				ret.add(instantiateAndConfigureReduction(reductionId, Optional.of(elem.toSingle())));
			}
		}

		return ret;
	}

	/**
	 * Instantiates a new reduction ({@link IReduction}) object, registered with
	 * the given reduction ID. The settings used for the creation will be taken
	 * from the given settings ({@code reductionSettings}, if present) and from
	 * the installation-specific settings (with the given reduction ID).
	 * 
	 * @param reductionId
	 *            ID of the reduction to be instantiated. Shall not be
	 *            {@code null}.
	 * @param reductionSettings
	 *            Settings for the reduction. Shall not be {@code null}, but can
	 *            be empty.
	 * @return The {@link IReduction} object. Never {@code null}.
	 * @throws SettingsParserException
	 */
	private IReduction instantiateAndConfigureReduction(String reductionId, Optional<SettingsElement> reductionSettings)
			throws SettingsParserException {
		Preconditions.checkNotNull(reductionId, "reductionId");
		Preconditions.checkNotNull(reductionSettings, "reductionSettings");

		IReductionExtension redExt = getReductions().getOrDefault(reductionId, null);
		if (redExt == null) {
			throw new PlcverifPlatformException(String.format("Unknown reduction: '%s'.", reductionId));
		}

		// Load installation-specific settings from '<reduction_id>.settings'
		SettingsElement resultantSettings;
		if (reductionSettings.isPresent()) {
			resultantSettings = SettingsSerializer.extendWithInstallationSettings(reductionSettings.get(), reductionId,
					platformConfig.getProgramSettingsDirectory());
		} else {
			resultantSettings = SettingsSerializer
					.loadInstallationPluginSettings(reductionId, platformConfig.getProgramSettingsDirectory())
					.orElse(SettingsElement.empty());
		}

		IReduction reduction = redExt.createReduction(resultantSettings);
		Preconditions.checkNotNull(reduction, "reduction");
		return reduction;

	}

	private static Path parseOutputDirectory(BasicJobSettings settings) {
		Path outputDirectory = settings.getOutputDirectory();

		if (outputDirectory != null) {
			return outputDirectory;
		} else {
			final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(OUTPUT_DIR_DATE_FORMAT_PATTERN);
			final String directoryName = formatter.format(LocalDateTime.now()).replaceAll("[^a-zA-Z0-9\\-]", "_");
			final String workingDir = System.getProperty("user.dir");
			Path ret = Paths.get(workingDir, "output", directoryName);
			PlatformLogger.logWarning(
					"No output directory was found in the job definition. Using generated folder in working directory: "
							+ ret);
			return ret;
		}
	}

	private IBasicJob parseDefinedJob(SettingsElement settings) throws SettingsParserException {
		Optional<Settings> jobSettings = settings.getAttribute(CfaJobSettings.JOB);
		if (!jobSettings.isPresent() || jobSettings.get().toSingle().value() == null) {
			throw new SettingsParserException("Service not specified - nothing to execute.");
		}

		String jobId = jobSettings.get().toSingle().value();
		IJobExtension job = getJobs().getOrDefault(jobId, null);
		if (job == null) {
			throw new SettingsParserException("Job not found: %s.", jobId);
		}

		// Load installation-specific settings from '<job_id>.settings'
		SettingsElement resultantSettings = SettingsSerializer.extendWithInstallationSettings(
				jobSettings.get().toSingle(), jobId, platformConfig.getProgramSettingsDirectory());
		return job.createJob(resultantSettings, this.getPlatformConfig());
	}
}
