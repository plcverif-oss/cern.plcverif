/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.platform.utils;

import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsList;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.interfaces.IParser;
import cern.plcverif.base.interfaces.data.JobMetadata;
import cern.plcverif.base.interfaces.data.ModelTasks;
import cern.plcverif.base.interfaces.data.CfaJobSettings;

public final class PlatformSettingsUtils {
	private PlatformSettingsUtils() {
		// Utility class.
	}

	public static SettingsElement retrieveSettings(ModelTasks modelTasks) {
		try {
			SettingsElement ret = SettingsElement.empty();
			fillSourceFiles(ret, modelTasks.getParser());
			if (modelTasks.getParser() != null) {
				SettingsElement langFrontendSettings = modelTasks.getParser().retrieveSettings().toSingle();
				ret.setAttribute(CfaJobSettings.LANGUAGE_FRONTEND, langFrontendSettings);
			}
			ret.addListAttribute(CfaJobSettings.REDUCTIONS, modelTasks.getReductions());
			return ret;
		} catch (SettingsParserException e) {
			throw new PlcverifPlatformException("Unable to retrieve the settings from the language frontend.", e);
		}
	}

	public static SettingsElement retrieveSettings(JobMetadata metadata) {
		SettingsElement ret = SettingsElement.empty();

		ret.setSimpleAttribute(CfaJobSettings.ID, metadata.getId());
		ret.setSimpleAttribute(CfaJobSettings.NAME, metadata.getName());
		ret.setSimpleAttribute(CfaJobSettings.DESCRIPTION, metadata.getDescription());
		return ret;
	}

	public static void fillSourceFiles(SettingsElement rootSettings, IParser parser) {
		SettingsList list = SettingsList.empty();
		rootSettings.setAttribute(CfaJobSettings.SOURCE_FILES, list);
		if (parser != null && parser.getFiles() != null) {
			for (String sourceFile : parser.getFiles().keySet()) {
				SettingsElement element = SettingsElement.empty();
				element.setValue(sourceFile);
				list.add(element);
			}
		}
	}
}
