/*******************************************************************************
 * (C) Copyright CERN 2017-2021. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Akos Hajdu - further implementation
 *   Tamas Toth - minor stylistic modifications
 *   Mihaly Dobos-Kovacs - integer representation, array representation support
 *******************************************************************************/
package cern.plcverif.library.backend.theta;

import java.nio.file.Path;

import cern.plcverif.base.common.settings.specific.AbstractSpecificSettings;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsElement;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory;

/**
 * Settings for the Theta backend. The settings of Theta can be categorized into
 * two main groups: parameters of the verification engine and parameters of the
 * algorithm.
 * 
 */
public final class ThetaSettings extends AbstractSpecificSettings {

	// ---------------------------------------------
	// Parameters of the verification engine
	// ---------------------------------------------

	public static final String TIMEOUT = "timeout";
	@PlcverifSettingsElement(name = TIMEOUT, description = "Timeout of the verification backend, in seconds.", mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private int timeoutInSec = 5;

	public static final String BINARY_PATH = "binary_path";
	@PlcverifSettingsElement(name = BINARY_PATH, description = "Full path to the Theta binary (.jar).")
	private Path binaryPath;

	public static final String LIBRARY_PATH = "lib_path";
	@PlcverifSettingsElement(name = LIBRARY_PATH, description = "Path of the libraries (Z3 DLLs: libz3.dll, libz3java.dll or Z3 SOs: libz3.so, libz3java.so) required by Theta.")
	private Path libraryPath;

	// ---------------------------------------------
	// Parameters of the algorithm
	// ---------------------------------------------
	public static final String DOMAIN = "domain";
	@PlcverifSettingsElement(name = DOMAIN, description = "Abstract domain. See Theta manual for more details and accepted values.")
	private String domain;

	public static final String REFINEMENT = "refinement";
	@PlcverifSettingsElement(name = REFINEMENT, description = "Refinement strategy. See Theta manual for more details and accepted values.")
	private String refinement;

	public static final String INTEGER_REPRESENTATION = "integer-representation";
	@PlcverifSettingsElement(name = INTEGER_REPRESENTATION, description = "Integer representation. Defines how PLC integers are mapped to Theta types.")
	private String integerRepresentation;

	public static final String ARRAY_REPRESENTATION = "array-representation";
	@PlcverifSettingsElement(name = ARRAY_REPRESENTATION, description = "Array representation. Defines how PLC arrays are mapped to Theta types.")
	private String arrayRepresentation;

	public static final String SEARCH = "search";
	@PlcverifSettingsElement(name = SEARCH, description = "Search strategy. See Theta manual for more details and accepted values.")
	private String search;

	public static final String PREC_GRANULARITY = "precgranularity";
	@PlcverifSettingsElement(name = PREC_GRANULARITY, description = "Granularity of the precision of abstraction. See Theta manual for more details and accepted values.")
	private String precGranularity;

	public static final String PRED_SPLIT = "predsplit";
	@PlcverifSettingsElement(name = PRED_SPLIT, description = "Splitting method for new predicates obtained from interpolation. See Theta manual for more details and accepted values.")
	private String predSplit;

	public static final String CFA_ENCODING = "encoding";
	@PlcverifSettingsElement(name = CFA_ENCODING, description = "Encoding of the CFA. See Theta manual for more details and accepted values.")
	private String encoding;

	public static final String MAX_ENUM = "maxenum";
	@PlcverifSettingsElement(name = MAX_ENUM, description = "Maximal successors to enumerate using the SMT solver. See Theta manual for more details and accepted values.")
	private String maxEnum;

	public static final String INIT_PRECISION = "initprec";
	@PlcverifSettingsElement(name = INIT_PRECISION, description = "Initial precision of the abstraction. See Theta manual for more details and accepted values.")
	private String initPrec;

	public String getAlgorithmConfigString() {
		return String.join("-", domain, refinement, integerRepresentation, search, precGranularity, predSplit, encoding, maxEnum, initPrec);
	}

	public int getTimeoutInSec() {
		return timeoutInSec;
	}

	public Path getBinaryPath() {
		return binaryPath;
	}

	public Path getLibraryPath() {
		return libraryPath;
	}

	public String getDomain() {
		return domain;
	}

	public String getRefinement() {
		return refinement;
	}

	public String getIntegerRepresentation() {
		return integerRepresentation;
	}
	
	void setIntegerRepresentation(final String value) {
		integerRepresentation = value;
	}

	public String getArrayRepresentation() {
		return arrayRepresentation;
	}
	
	void setArrayRepresentation(final String value) {
		arrayRepresentation = value;
	}

	public String getSearch() {
		return search;
	}

	public String getPrecGranularity() {
		return precGranularity;
	}

	public String getPredSplit() {
		return predSplit;
	}

	public String getEncoding() {
		return encoding;
	}

	public String getMaxEnum() {
		return maxEnum;
	}

	public String getInitPrec() {
		return initPrec;
	}
}