/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.theta.parser.sexpr;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class SList implements SExpr {
	private List<SExpr> list = new ArrayList<>();

	public void add(SExpr expr) {
		list.add(expr);
	}

	public List<SExpr> getContent() {
		return Collections.unmodifiableList(list);
	}

	public SExpr get(int index) {
		return list.get(index);
	}

	public int size() {
		return list.size();
	}

	@Override
	public boolean isAtom() {
		return false;
	}

	@Override
	public boolean isList() {
		return true;
	}

	@Override
	public SAtom asAtom() {
		throw new UnsupportedOperationException();
	}

	@Override
	public SList asList() {
		return this;
	}

	@Override
	public String toString() {
		return list.stream().map(it -> it.toString()).collect(Collectors.joining("   ", "(", ")"));
	}
}
