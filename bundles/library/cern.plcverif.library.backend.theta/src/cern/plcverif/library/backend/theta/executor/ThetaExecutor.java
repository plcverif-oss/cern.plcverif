/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Akos Hajdu - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *******************************************************************************/
package cern.plcverif.library.backend.theta.executor;

import java.io.IOException;
import java.util.List;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

import cern.plcverif.base.common.logging.IPlcverifLogger;
import cern.plcverif.base.common.process.ConsoleOutputGobbler;
import cern.plcverif.base.common.process.TimeoutExecutor;
import cern.plcverif.base.common.progress.ICanceling;
import cern.plcverif.library.backend.theta.ThetaContext;
import cern.plcverif.library.backend.theta.ThetaSettings;
import cern.plcverif.library.backend.theta.exception.ThetaException;
import cern.plcverif.library.backend.theta.exception.ThetaGracefulInterruptionException;

/**
 * Executor class for the Theta tool.
 */
public final class ThetaExecutor {
	private final ThetaContext context;
	private final ICanceling canceler;

	/**
	 * Create a new executor.
	 * 
	 * @param context
	 *            Context
	 * @param canceler 
	 */
	public ThetaExecutor(final ThetaContext context, ICanceling canceler) {
		this.context = Preconditions.checkNotNull(context);
		this.canceler = Preconditions.checkNotNull(canceler);
	}

	/**
	 * Execute the Theta backend.
	 * 
	 * @return Console output of Theta as a string
	 * @throws ThetaException
	 *             in case of a timeout, cancellation or interruption
	 */
	public String execute() throws ThetaException {
		final ThetaSettings settings = context.getSettings();
		final IPlcverifLogger stage = context.getVerifResult().currentStage();

		Process thetaProcess = null;
		try {
			final List<String> args = getArgs(context.getSettings());
			ProcessBuilder procBuilder = new ProcessBuilder(args);
			procBuilder.environment().put("LD_LIBRARY_PATH", context.getAbsoluteLibraryPath().toString());
			procBuilder.directory(context.getAbsoluteLibraryPath().toFile());
			thetaProcess = procBuilder.start();
			stage.logInfo("Theta execution started with the following arguments: %s. Timeout: %s sec.",
					String.join(" ", args), Integer.toString(settings.getTimeoutInSec()));

			final TimeoutExecutor executor = TimeoutExecutor.execute(thetaProcess, settings.getTimeoutInSec(), canceler);
			final ConsoleOutputGobbler stdOutput = new ConsoleOutputGobbler(thetaProcess.getInputStream(), true);
			stdOutput.start();
			thetaProcess.waitFor();

			if (executor.isTimeout()) {
				stage.logWarning(String.format("Timeout during the execution of Theta model checking (%s sec).",
						settings.getTimeoutInSec()));
				throw new ThetaGracefulInterruptionException("Timeout.");
			}
			if (executor.isCanceled()) {
				stage.logWarning("The execution of Theta model checking was cancelled.");
				throw new ThetaGracefulInterruptionException("User cancellation.");
			}

			String stdOutContent = stdOutput.getReadString();
			context.getVerifResult().setBackendStdout(stdOutContent);
			return stdOutContent;

		} catch (final InterruptedException e) {
			Preconditions.checkNotNull(thetaProcess);
			thetaProcess.destroy();
			stage.logWarning("The execution of Theta was interrupted.");
			throw new ThetaException("The execution of Theta was interrupted.", e);
		} catch (final IOException e) {
			stage.logError("Error on Theta execution.", e);
			throw new ThetaException("Error on Theta execution.", e);
		}
	}

	private List<String> getArgs(final ThetaSettings settings) {
		final Builder<String> args = ImmutableList.builder();
		args.add("java");
		args.add("-Djava.library.path=" + context.getAbsoluteLibraryPath());
		args.add("-jar").add(context.getAbsoluteBinaryPath().toString());
		args.add("--model").add(context.getModelFile().toAbsolutePath().toString());
		args.add("--domain").add(settings.getDomain());
		args.add("--refinement").add(settings.getRefinement());
		addIfNonEmpty(args, "--search", settings.getSearch());
		addIfNonEmpty(args, "--precgranularity", settings.getPrecGranularity());
		addIfNonEmpty(args, "--predsplit", settings.getPredSplit());
		addIfNonEmpty(args, "--encoding", settings.getEncoding());
		addIfNonEmpty(args, "--maxenum", settings.getMaxEnum());
		addIfNonEmpty(args, "--initprec", settings.getInitPrec());
		args.add("--loglevel").add("RESULT");
		args.add("--stacktrace");
		args.add("--cex").add(context.getCexFile().toAbsolutePath().toString());
		return args.build();
	}

	private static void addIfNonEmpty(final Builder<String> args, final String option, final String value) {
		if (value != null && !"".equals(value)) {
			args.add(option).add(value);
		}
	}
}
