/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Tamas Toth - minor stylistic modifications
 *******************************************************************************/
package cern.plcverif.library.backend.theta;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsSerializer;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.help.SettingsHelp;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.verif.interfaces.IBackend;
import cern.plcverif.verif.interfaces.IBackendExtension;
import cern.plcverif.verif.job.VerificationJobOptions;

public class ThetaBackendExtension implements IBackendExtension {
	public static final String CMD_ID = "theta";
	// Should match 'cmd_id' in plugin.xml

	@Override
	public IBackend createBackend() {
		return new ThetaBackend(loadDefaultSettings());
	}

	@Override
	public IBackend createBackend(final SettingsElement settings) throws SettingsParserException {
		final Settings compositeSettings = loadDefaultSettings();
		compositeSettings.overrideWith(settings);
		return new ThetaBackend(compositeSettings);
	}

	public SettingsElement loadDefaultSettings() {
		try {
			final String settingString = IoUtils.readResourceFile("/settings/default.settings",
					this.getClass().getClassLoader());
			final SettingsElement ret = SettingsSerializer.parseSettingsFromString(settingString).toSingle();
			ret.setValue(CMD_ID);
			return ret;
		} catch (final SettingsParserException e) {
			return SettingsElement.createRootElement(VerificationJobOptions.BACKEND, CMD_ID);
		}
	}

	@Override
	public void fillSettingsHelp(SettingsHelp help) {
		help.addPluginDetails(CMD_ID, "Theta verification backend",
				"Solves the verification problem by using the Theta model checker. Only applicable if job=verif.",
				ThetaBackendExtension.class);

		// Try to load the default settings
		ThetaSettings defaults;
		try {
			defaults = SpecificSettingsSerializer.parse(loadDefaultSettings(), ThetaSettings.class);
		} catch (SettingsParserException e) {
			// best effort (don't die on help creation)
			defaults = new ThetaSettings();
		}

		// Fill the 'SettingsHelp' object
		SpecificSettingsSerializer.fillSettingsHelp(help, ThetaSettings.class, defaults);
	}
}
