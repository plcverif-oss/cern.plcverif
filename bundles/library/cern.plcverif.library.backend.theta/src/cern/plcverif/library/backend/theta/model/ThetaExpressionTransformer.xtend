/******************************************************************************
 * (C) Copyright CERN 2017-2021. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Akos Hajdu - further improvements and additions
 *   Tamas Toth - minor stylistic modifications
 *   Mihaly Dobos-Kovacs - integer representation, array representation support
 *****************************************************************************/
package cern.plcverif.library.backend.theta.model

import cern.plcverif.base.models.cfa.cfabase.ArrayType
import cern.plcverif.base.models.cfa.cfabase.ElseExpression
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariable
import cern.plcverif.base.models.cfa.cfainstance.VariableRef
import cern.plcverif.base.models.expr.BinaryArithmeticExpression
import cern.plcverif.base.models.expr.BinaryArithmeticOperator
import cern.plcverif.base.models.expr.BinaryCtlExpression
import cern.plcverif.base.models.expr.BinaryLogicExpression
import cern.plcverif.base.models.expr.BinaryLogicOperator
import cern.plcverif.base.models.expr.BinaryLtlExpression
import cern.plcverif.base.models.expr.BoolLiteral
import cern.plcverif.base.models.expr.BoolType
import cern.plcverif.base.models.expr.ComparisonExpression
import cern.plcverif.base.models.expr.ComparisonOperator
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.FloatType
import cern.plcverif.base.models.expr.IntLiteral
import cern.plcverif.base.models.expr.IntType
import cern.plcverif.base.models.expr.Nondeterministic
import cern.plcverif.base.models.expr.Type
import cern.plcverif.base.models.expr.TypeConversion
import cern.plcverif.base.models.expr.UnaryArithmeticExpression
import cern.plcverif.base.models.expr.UnaryCtlExpression
import cern.plcverif.base.models.expr.UnaryLogicExpression
import cern.plcverif.base.models.expr.UnaryLtlExpression
import cern.plcverif.base.models.expr.UnknownType
import cern.plcverif.base.models.expr.utils.ExprUtils
import cern.plcverif.base.models.expr.utils.TypeUtils
import cern.plcverif.library.backend.theta.ThetaContext
import cern.plcverif.library.backend.theta.exception.ThetaException
import cern.plcverif.verif.utils.backend.BackendNameTrace
import com.google.common.base.Preconditions

import static cern.plcverif.base.models.expr.string.ExprToString.toDiagString
import static com.google.common.base.Preconditions.checkState
import cern.plcverif.base.models.expr.ExprFactory
import cern.plcverif.base.models.cfa.cfainstance.ArrayElementRef
import cern.plcverif.base.models.cfa.cfabase.ArrayDimension
import java.util.List

/**
 * Transformator class from {@link Expression}s to the input language of Theta.
 */
final class ThetaExpressionTransformer {
	
	final ThetaContext context
	final BackendNameTrace<AbstractVariable> varTrace

	new (ThetaContext context) {
		this.context = context
		this.varTrace = context.varTrace
	}
	
	// Expressions
	
	def dispatch CharSequence toThetaExpression(Void expr) {
		throw new ThetaException("Unsupported expression: NULL")
	}
	
	def dispatch CharSequence toThetaExpression(Expression expr) {
		throw new ThetaException('''Unsupported expression: «toDiagString(expr)» («expr») of «expr.class.name»''')
	}
	
	def dispatch CharSequence toThetaExpression(ElseExpression expr) {
		throw new ThetaException('''The Theta backend does not support 'else' expressions. Use the 'ElseEliminator.transform()' method before generating the Theta model.''')
	}

	private def binary(Expression left, String thetaOp, Expression right) {
		return '''(«left.toThetaExpression») «thetaOp» («right.toThetaExpression»)'''
	}

	private def binaryMod(Expression left, String thetaOp, Expression right, IntType type) {
		val minValue = TypeUtils.minValue(type)
		val maxValue = TypeUtils.maxValue(type)
		return '''((«left.toThetaExpression») «thetaOp» («right.toThetaExpression») mod «maxValue - minValue + 1»)'''
	}

	def dispatch CharSequence toThetaExpression(BinaryLogicExpression expr) {
		return binary(expr.leftOperand, expr.operator.toThetaOp, expr.rightOperand)
	}

	def dispatch CharSequence toThetaExpression(BinaryArithmeticExpression expr) {
		if (expr.operator == BinaryArithmeticOperator.POWER && ExprUtils.isIntLiteral(expr.rightOperand)) {
			return powerToConstant(expr.leftOperand, (expr.rightOperand as IntLiteral).value as int);
		}
		
		return toThetaExpression(expr, expr.type)
	}
	
	def dispatch CharSequence toThetaExpression(BinaryArithmeticExpression expr, Type type) {
		return binary(expr.leftOperand, expr.operator.toThetaOp, expr.rightOperand)
	}
	
	def dispatch CharSequence toThetaExpression(BinaryArithmeticExpression expr, IntType type) {
		switch(ThetaTransformationUtils.parseIntegerRepresentation(context.settings.integerRepresentation)) {
			case UNBOUNDED: {
				return binary(expr.leftOperand, expr.operator.toThetaOp, expr.rightOperand)
			}
			case MODULO: {
				return binaryMod(expr.leftOperand, expr.operator.toThetaOp, expr.rightOperand, type)
			}
			case BITVECTOR: {
				return binary(expr.leftOperand, expr.operator.toThetaBvOp(type), expr.rightOperand)
			}
		}
	}
	
	def dispatch CharSequence toThetaExpression(ComparisonExpression expr) {
		return toThetaExpression(expr, expr.type)
	}
	
	def dispatch CharSequence toThetaExpression(ComparisonExpression expr, Type type) {
		return binary(expr.leftOperand, expr.operator.toThetaOp, expr.rightOperand)
	}
	
	def dispatch CharSequence toThetaExpression(ComparisonExpression expr, IntType type) {
		switch(ThetaTransformationUtils.parseIntegerRepresentation(context.settings.integerRepresentation)) {
			case MODULO,
			case UNBOUNDED: {
				return binary(expr.leftOperand, expr.operator.toThetaOp, expr.rightOperand)
			}
			case BITVECTOR: {
				return binary(expr.leftOperand, expr.operator.toThetaBvOp(type), expr.rightOperand)
			}
		}
	}

	def dispatch CharSequence toThetaExpression(BinaryLtlExpression expr) {
		throw new ThetaException("Unsupported expression: binary LTL")
	}

	def dispatch CharSequence toThetaExpression(BinaryCtlExpression expr) {
		throw new ThetaException("Unsupported expression: binary CTL")
	}

	def dispatch CharSequence toThetaExpression(UnaryLogicExpression expr) {
		switch (expr.operator) { 
			case NEG: return prefixUnary("not ", expr.operand)
			default: throw new UnsupportedOperationException("Unknown UnaryLogicExpression: " + expr.operator)
		}
	}
	
	def dispatch CharSequence toThetaExpression(UnaryArithmeticExpression expr) {
		return toThetaExpression(expr, expr.type)
	}
	
	def dispatch CharSequence toThetaExpression(UnaryArithmeticExpression expr, Type type) {
		switch (expr.operator) {
			case BITWISE_NOT: throw new ThetaException("Unsupported expression: " + expr.operator)
			case MINUS: return prefixUnary("-", expr.operand)
			default: throw new UnsupportedOperationException("Unknown UnaryArithmeticExpression: " + expr.operator)
		}
	}
	
	def dispatch CharSequence toThetaExpression(UnaryArithmeticExpression expr, IntType type) {
		switch(ThetaTransformationUtils.parseIntegerRepresentation(context.settings.integerRepresentation)) {
			case MODULO,
			case UNBOUNDED: {
				switch (expr.operator) {
					case BITWISE_NOT: throw new ThetaException("Unsupported expression: " + expr.operator)
					case MINUS: return prefixUnary("-", expr.operand)
					default: throw new UnsupportedOperationException("Unknown UnaryArithmeticExpression: " + expr.operator)
				}
			}
			case BITVECTOR: {
				switch (expr.operator) {
					case BITWISE_NOT: return prefixUnary("bvnot", expr.operand)
					case MINUS: return prefixUnary("bvneg", expr.operand)
					default: throw new UnsupportedOperationException("Unknown UnaryArithmeticExpression: " + expr.operator)
				}
			}
		}
	}

	def dispatch CharSequence toThetaExpression(UnaryLtlExpression expr) {
		throw new ThetaException("Unsupported expression: unary LTL")
	}

	def dispatch CharSequence toThetaExpression(UnaryCtlExpression expr) {
		throw new ThetaException("Unsupported expression: unary CTL")
	}

	def dispatch CharSequence toThetaExpression(TypeConversion expr) {
		val sourceType = expr.operand.type
		val targetType = expr.type
		if (sourceType instanceof IntType && targetType instanceof IntType) {
			switch(ThetaTransformationUtils.parseIntegerRepresentation(context.settings.integerRepresentation)) {
				case UNBOUNDED: {
					context.verifResult.currentStage.logWarningAtMostOnce('''Conversion from «toDiagString(sourceType)» to «toDiagString(targetType)» omitted as Theta uses unbounded integers.''')
					return toThetaExpression(expr.operand)
				}
				case MODULO: {
					val minValue = TypeUtils.minValue(targetType as IntType)
					val maxValue = TypeUtils.maxValue(targetType as IntType)
					return '''(«expr.operand.toThetaExpression» mod «maxValue - minValue + 1»)'''
				}
				case BITVECTOR: {
					val sourceBits = (sourceType as IntType).bits
					val targetBits = (targetType as IntType).bits
					if (targetBits > sourceBits) {
						return '''((«expr.operand.toThetaExpression») bv_sign_extend «targetType.toThetaType»)''' // TODO: maybe zext?
					} else if (sourceBits > targetBits) {
						return '''(«expr.operand.toThetaExpression»)[«targetBits»:0]'''
					} else {
						return '''(«expr.operand.toThetaExpression»)'''
					}
				}
			}
		}
		throw new ThetaException('''Unsupported expression: conversion from «toDiagString(sourceType)» to «toDiagString(targetType)»''')
	}

	def dispatch CharSequence toThetaExpression(IntLiteral expr) {
		Preconditions.checkArgument(expr.type !== null)
		Preconditions.checkArgument(!(expr.type instanceof UnknownType))
		Preconditions.checkArgument(expr.type instanceof IntType)

		val type = expr.type as IntType
		switch(ThetaTransformationUtils.parseIntegerRepresentation(context.settings.integerRepresentation)) {
			case MODULO,
			case UNBOUNDED: {
				return Long.toString(expr.value)
			}
			case BITVECTOR: {
				var str = Long.toBinaryString(expr.value)
				if(str.length > type.bits) str = str.substring(str.length - type.bits)
				return '''«type.bits»'b«str»'''
			}
		}
	}

	def dispatch CharSequence toThetaExpression(BoolLiteral expr) {
		return if (expr.value) "true" else "false"
	}

	def dispatch CharSequence toThetaExpression(VariableRef expr) {
		return varTrace.toName(expr.variable)
	}

	def dispatch CharSequence toThetaExpression(Nondeterministic expr) {
		throw new ThetaException("Unsupported expression: nondeterministic. Use havoc statement.")
	}

	def dispatch CharSequence toThetaExpression(ArrayElementRef expr) {
		return '''«varTrace.toName(expr.variable)»[«expr.indices.get(0).toThetaExpression»]'''
	}
	
	// Types

	def dispatch CharSequence toThetaType(Type type) {
		throw new ThetaException('''Unsupported type: «toDiagString(type)».''') 
	}

	def dispatch CharSequence toThetaType(IntType type) {
		switch (ThetaTransformationUtils.parseIntegerRepresentation(context.settings.integerRepresentation)) {
			case BITVECTOR: {
				return '''bv[«type.bits»]'''
			}
			case MODULO: {
				if(type.signed) {
					throw new ThetaException("Modulo integer representation is only supported for unsigned integers")
					// return '''int'''
				} else {
					return '''int'''
				}
			}
			case UNBOUNDED: {
				context.verifResult.currentStage.logWarningAtMostOnce('''Type «toDiagString(type)» mapped to unbounded integer in Theta.''')
				return '''int'''
			}	
		}
	}

	def dispatch CharSequence toThetaType(BoolType type) {
		return '''bool'''
	}
	
	def dispatch CharSequence toThetaType(FloatType type) {
		throw new ThetaException('''The Theta backend does not support floating-point numbers («toDiagString(type)»).''') 
	}
	
	def dispatch CharSequence toThetaType(ArrayType type) {
		checkState(type.dimension.size == 1, "The Theta backend only supports 1D arrays")
		val indexType = ExprFactory.eINSTANCE.createIntType()
		indexType.signed = true
		indexType.bits = 16
		return '''[«indexType.toThetaType»] -> «type.elementType.toThetaType»'''
	}
	
	def CharSequence toThetaType(Type type, List<ArrayDimension> dimensions) {
		checkState(dimensions.size == 1, "The Theta backend only supports 1D arrays")
		val indexType = ExprFactory.eINSTANCE.createIntType()
		indexType.signed = true
		indexType.bits = 16
		return '''[«indexType.toThetaType»] -> «type.toThetaType»'''
	}
	
	
	// Helper methods
	
	private def prefixUnary(String thetaOp, Expression expr) {
		return '''«thetaOp»(«expr.toThetaExpression»)'''
	}

	private def static toThetaOp(BinaryLogicOperator operator) {
		switch (operator) {
			case AND: return " and "
			case OR: return " or "
			case XOR: return " /= "
			case IMPLIES: return " imply "
			default: throw new UnsupportedOperationException("Unknown BinaryLogicOperator: " + operator)
		}
	}

	private def static toThetaOp(BinaryArithmeticOperator operator) {
		switch (operator) {
			case PLUS: return "+"
			case MINUS: return "-"
			case MULTIPLICATION: return "*"
			case DIVISION: return "/"
			case MODULO: return " mod "
			case INTEGER_DIVISION: return "/"
			case BITSHIFT_LEFT,
			case BITSHIFT_RIGHT,
			case BITROTATE,
			case BITWISE_OR,
			case BITWISE_AND,
			case BITWISE_XOR: throw new ThetaException("This operator is only supported with bitvector integer representation: " + operator)
			case POWER: throw new ThetaException("Unsupported operator: " + operator)
			default: throw new UnsupportedOperationException("Unknown BinaryArithmeticOperator: " + operator)
		}
	}

	private def static toThetaBvOp(BinaryArithmeticOperator operator, IntType type) {
		switch (operator) {
			case PLUS: return "bvadd"
			case MINUS: return "bvsub"
			case MULTIPLICATION: return "bvmul"
			case DIVISION: return type.signed ? "bvsdiv" : "bvudiv"
			case MODULO: return type.signed ? " bvsmod " : " bvurem "
			case INTEGER_DIVISION: return type.signed ? "bvsdiv" : "bvudiv"
			case BITSHIFT_LEFT: return "bvshl"
			case BITSHIFT_RIGHT: return "bvashr" /* TODO: maybe bvlshr */
			case BITROTATE: return "bvrol" /* TODO: maybe bvror */
			case BITWISE_OR: return "bvor"
			case BITWISE_AND: return "bvand"
			case BITWISE_XOR: return "bvxor"
			case POWER: throw new ThetaException("Unsupported operator: " + operator)
			default: throw new UnsupportedOperationException("Unknown BinaryArithmeticOperator: " + operator)
		}
	}

	private def static toThetaOp(ComparisonOperator operator) {
		switch (operator) {
			case EQUALS: return "="
			case NOT_EQUALS: return "/="
			case LESS_THAN: return "<"
			case GREATER_THAN: return ">"
			case LESS_EQ: return "<="
			case GREATER_EQ: return ">="
			default: throw new UnsupportedOperationException("Unknown ComparisonOperator: " + operator)
		}
	}

	private def static toThetaBvOp(ComparisonOperator operator, IntType type) {
		switch (operator) {
			case EQUALS: return "="
			case NOT_EQUALS: return "/="
			case LESS_THAN: return type.signed ? "bvslt" : "bvult"
			case GREATER_THAN: return type.signed ? "bvsgt" : "bvugt"
			case LESS_EQ: return type.signed ? "bvsle" : "bvule"
			case GREATER_EQ: return type.signed ? "bvsge" : "bvuge"
			default: throw new UnsupportedOperationException("Unknown ComparisonOperator: " + operator)
		}
	}
	
	private def powerToConstant(Expression base, int exponent) {
		Preconditions.checkNotNull(base, "base");
		Preconditions.checkArgument(exponent >= 0, "Negative exponents are not supported.");
		
		if (exponent == 0) {
			return "1";
		} else {
			// '..' operator is inclusive
			return '''(«FOR i : 1..exponent SEPARATOR toThetaOp(BinaryArithmeticOperator.MULTIPLICATION)»(«toThetaExpression(base)»)«ENDFOR»)'''
		}
	}
}
