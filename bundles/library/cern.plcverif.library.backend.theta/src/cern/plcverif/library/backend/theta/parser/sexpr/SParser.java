/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.theta.parser.sexpr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public final class SParser {
	private static final String LEFT_PAREN = "((?<=\\()|(?=\\())";
	private static final String RIGHT_PAREN = "((?<=\\))|(?=\\)))";
	private static final String WHITESPACE = "\\s+";

	private SParser() {
		// Utility class.
	}
	
	public static SExpr parse(String stringToParse) throws SExprParsingException {
		// split into tokens
		String[] tokens = stringToParse.split(String.format("%s|%s|%s", LEFT_PAREN, RIGHT_PAREN, WHITESPACE));
		List<String> tokenList = new ArrayList<>(Arrays.asList(tokens));
		tokenList.removeIf(it -> it.trim().equals(""));

		PeekListIterator<String> iterator = new PeekListIterator<>(tokenList);
		try {
			return parseInternal(iterator);
		} catch (NoSuchElementException e) {
			throw new SExprParsingException("Invalid input.");
		}
	}

	private static SExpr parseInternal(PeekListIterator<String> iterator) {
		if (!iterator.hasNext()) {
			System.err.println("Warining @ SParser.parseInternal: Empty.");
			return null;
		} else {
			String token = nextNonEmpty(iterator);

			if (token.equals("(")) {
				// Start of a list
				SList ret = new SList();
				while (iterator.peek().trim().equals(")") == false) {
					ret.add(parseInternal(iterator));
				}
				String rightParen = iterator.next(); // consume ")"
				assert rightParen.equals(")");
				return ret;
			} else {
				// If not starting a new list, it must be a token
				return new SAtom(token.trim());
			}
		}
	}

	/**
	 * Returns the next element that is not empty, nor containing whitespace only.
	 * 
	 * @param iterator
	 *            Iterator.
	 * @return Next non-empty element.
	 */
	private static String nextNonEmpty(Iterator<String> iterator) {
		String token;
		do {
			token = iterator.next();
		} while (token.matches("\\s+") || token.isEmpty());
		return token;
	}

	/**
	 * List iterator that supports peek operation too.
	 * 
	 * @param <T>
	 *            Type of the list on which iteration is performed.
	 */
	static class PeekListIterator<T> implements Iterator<T> {
		private List<T> list;
		private int current = -1;

		public PeekListIterator(List<T> list) {
			this.list = list;
		}

		@Override
		public boolean hasNext() {
			return (list.size() - 1) > current;
		}

		@Override
		public T next() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			}

			current++;
			return list.get(current);
		}

		public T peek() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			}
			return list.get(current + 1);
		}
	}

	public static class SExprParsingException extends Exception {
		private static final long serialVersionUID = 99925455643699374L;

		public SExprParsingException(String message) {
			super(message);
		}
	}
}
