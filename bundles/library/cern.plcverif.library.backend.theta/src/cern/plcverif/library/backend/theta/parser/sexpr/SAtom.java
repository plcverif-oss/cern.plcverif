/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.theta.parser.sexpr;

public class SAtom implements SExpr {
	private String data;

	public SAtom(String data) {
		this.data = data;
	}

	public String getData() {
		return data;
	}

	@Override
	public String toString() {
		return String.format("%s", data);
	}

	@Override
	public boolean isAtom() {
		return true;
	}

	@Override
	public boolean isList() {
		return false;
	}

	@Override
	public SAtom asAtom() {
		return this;
	}

	@Override
	public SList asList() {
		throw new UnsupportedOperationException();
	}
}
