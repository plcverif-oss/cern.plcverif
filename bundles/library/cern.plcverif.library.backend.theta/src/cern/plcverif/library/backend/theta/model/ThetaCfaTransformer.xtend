/******************************************************************************
 * (C) Copyright CERN 2017-2021. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Akos Hajdu - further improvements and additions
 *   Tamas Toth - minor stylistic modifications
 *   Mihaly Dobos-Kovacs - integer representation, array representation support
 *****************************************************************************/
package cern.plcverif.library.backend.theta.model

import cern.plcverif.base.common.utils.IoUtils
import cern.plcverif.base.models.cfa.cfabase.Location
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariable
import cern.plcverif.base.models.cfa.cfainstance.AssignmentTransition
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance
import cern.plcverif.base.models.cfa.cfainstance.Variable
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.ExpressionSafeFactory
import cern.plcverif.base.models.expr.IntType
import cern.plcverif.base.models.expr.Nondeterministic
import cern.plcverif.base.models.expr.UnaryCtlExpression
import cern.plcverif.base.models.expr.UnaryCtlOperator
import cern.plcverif.base.models.expr.utils.ExprUtils
import cern.plcverif.base.models.expr.utils.TypeUtils
import cern.plcverif.library.backend.theta.ThetaContext
import cern.plcverif.library.backend.theta.exception.ThetaException
import cern.plcverif.verif.interfaces.data.VerificationProblem
import cern.plcverif.verif.utils.backend.BackendNameTrace
import java.io.IOException

import static com.google.common.base.Preconditions.checkNotNull
import static com.google.common.base.Preconditions.checkState
import cern.plcverif.base.models.cfa.cfainstance.ArrayVariable
import cern.plcverif.base.models.cfa.cfainstance.ArrayElementRef
import cern.plcverif.base.models.expr.UnaryLtlExpression
import cern.plcverif.base.models.expr.UnaryLtlOperator

/**
 * Transformator from {@link VerificationProblem} to the input language of Theta.
 */
final class ThetaCfaTransformer {
	static final String ERROR_LOC_NAME = "theta_error"
	static final String INIT_LOC_NAME = "theta_init"

	final ThetaContext context
	final BackendNameTrace<Location> locToName
	final BackendNameTrace<AbstractVariable> varTrace
	final ThetaExpressionTransformer exprToTheta

	private new(ThetaContext context) {
		checkNotNull(context)
		checkState(context.verifProblem.model instanceof CfaNetworkInstance, "The Theta backend supports only instance CFAs.")
		checkState((context.verifProblem.model as CfaNetworkInstance).automata.size() == 1, "The Theta backend supports only inlined CFAs.")
		checkState((context.verifProblem.model as CfaNetworkInstance).automata.get(0) ===
			(context.verifProblem.model as CfaNetworkInstance).mainAutomaton)

		this.varTrace = new BackendNameTrace[ThetaTransformationUtils::makeValidName(it)]
		this.locToName = new BackendNameTrace[ThetaTransformationUtils::makeValidName(it)]
		this.context = context
		context.varTrace = varTrace
		context.locTrace = locToName
		this.exprToTheta = new ThetaExpressionTransformer(context)
	}

	/**
	 * Apply the transformation. Writes the output into a file.
	 * 
	 * @param context Context
	 * @throws ThetaException if a problem occurs during transformation, for example due to some unsupported feature
	 */
	static def transform(ThetaContext context) throws ThetaException {
		val modelContent = new ThetaCfaTransformer(context).represent()

		try {
			IoUtils.writeAllContent(context.modelFile, modelContent)
			val logMessage = '''Theta model has been written to '«context.modelFile.toAbsolutePath.toString»'.'''
			context.verifResult.currentStage.logInfo(logMessage)
		} catch (IOException e) {
			val errorMessage = '''Error occurred while writing Theta model into file '«context.modelFile.toAbsolutePath»'.'''
			throw new ThetaException(errorMessage, e)
		}
	}

	private def CharSequence represent() {
		representInternal(context.verifProblem.model as CfaNetworkInstance, context.verifProblem.requirement)
	}

	private def representInternal(CfaNetworkInstance cfa, Expression requirement) '''
		main process «cfa.name» {
			// Variables
			// ----------------------------------------
			«FOR variable : cfa.variables»
				«IF variable instanceof ArrayVariable»
					var «context.varTrace.toName(variable)» : «exprToTheta.toThetaType(variable.type, (variable as ArrayVariable).dimensions)»
				«ELSE»
					var «context.varTrace.toName(variable)» : «exprToTheta.toThetaType(variable.type)»
				«ENDIF»
			«ENDFOR»
			
			// Locations
			// ----------------------------------------
			init loc «INIT_LOC_NAME» // Extra location for setting initial values of variables
			final loc «locToName.toName(cfa.mainAutomaton.endLocation)»
			error loc «ERROR_LOC_NAME»
			«FOR loc : cfa.mainAutomaton.locations.filter[it | it !== cfa.mainAutomaton.endLocation]»
				loc «locToName.toName(loc)»
			«ENDFOR»
			
			// Set the initial values of variables and go to the original initial location
			// ----------------------------------------
			«INIT_LOC_NAME» -> «locToName.toName(cfa.mainAutomaton.initialLocation)» {
				«FOR variable : cfa.variables»
					«representInitialValue(variable)»
				«ENDFOR»
			}
		
			// Transitions
			// ----------------------------------------
			«FOR transition : cfa.mainAutomaton.transitions.filter(AssignmentTransition)»
				
				// «transition.name»
				«locToName.toName(transition.source)» -> «locToName.toName(transition.target)» {
					«IF transition.source == context.verifProblem.eocLocation»
						«representGuard(ThetaCfaTransformer.extractInvariant(requirement))» // Requirement holds
					«ENDIF»
					«representGuard(transition.condition)»
					«FOR amt : transition.assignments»
						«representAssignment(amt.leftValue.variable, amt.rightValue)»
					«ENDFOR»
				}
			«ENDFOR»
			
			// Requirement violation
			// ----------------------------------------
			«locToName.toName(context.verifProblem.eocLocation)» -> «ERROR_LOC_NAME» { 
				«representGuard(ExpressionSafeFactory.INSTANCE.neg(ThetaCfaTransformer.extractInvariant(requirement)))»
			}
		}
	'''

	private def representInitialValue(AbstractVariable variable) {
		if (variable instanceof Variable) {
			return representAssignment(variable, (variable as Variable).initialValue)
		} else if (variable instanceof ArrayVariable) {
			val arrayVariable = variable as ArrayVariable
			return '''
				«FOR initialValue : arrayVariable.initialValues»
					«context.varTrace.toName(variable)» := «context.varTrace.toName(variable)»[«exprToTheta.toThetaExpression(initialValue.indices.get(0))» <- «exprToTheta.toThetaExpression(initialValue.initialValue)»]
				«ENDFOR»
			'''
		} else {
			throw new UnsupportedOperationException("Initial value representation is not supported for " +
				variable.class)
		}
	}

	private def representGuard(Expression guard) {
		if (ExprUtils.isTrueLiteral(guard)) {
			return "" // no guard (constant true guard)
		} else {
			return '''assume «exprToTheta.toThetaExpression(guard)»'''
		}
	}

	private def representAssignment(AbstractVariable variable, Expression rightValue) {
		val variableType = variable.type;
		
		if (variable instanceof ArrayVariable) {
			return '''havoc «context.varTrace.toName(variable)»'''
		} else if (variable instanceof ArrayElementRef) {
			val arrayElement = variable as ArrayElementRef
			return '''«context.varTrace.toName(variable)» := «context.varTrace.toName(variable)»[«exprToTheta.toThetaExpression(arrayElement.indices.get(0))» <- «exprToTheta.toThetaExpression(rightValue)»]'''
		} else if (rightValue instanceof Nondeterministic) {
			val havocStmt = '''havoc «context.varTrace.toName(variable)»'''
			
			if (variableType instanceof IntType) {
				val minValue = TypeUtils.minValue(variableType)
				val maxValue = TypeUtils.maxValue(variableType)
				
				switch (ThetaTransformationUtils.parseIntegerRepresentation(context.settings.integerRepresentation)) {
					case UNBOUNDED: {
						// Modulo not necessary, only present to pass regression tests
						return '''
							«havocStmt»
							«IF variableType instanceof IntType»
								«IF outOfThetaRange(minValue) || outOfThetaRange(maxValue) || outOfThetaRange(maxValue - minValue + 1)»
«««									We cannot do this trick after the havoc if the literals would result in a literal that cannot be represented by Theta internally in an int variable. 
									«context.verifResult.currentStage.logWarningAtMostOnce("One of the random assignments cannot be restricted to the necessary value range as it would result in a too large literal in the Theta model.")»
								«ELSE»
									«context.varTrace.toName(variable)» := («context.varTrace.toName(variable)» mod «maxValue - minValue + 1»)«IF minValue != 0» «IF minValue > 0»+«ELSE»-«ENDIF» «Math.abs(minValue)»«ENDIF» 
								«ENDIF» 
							«ENDIF»
						'''
					}
					case MODULO: {
						if (variableType.signed) {
							throw new ThetaException("Modulo integer representation is only supported for unsigned integers")
							/* return '''
							 *	«havocStmt»
							 * 	«context.varTrace.toName(variable)» := («context.varTrace.toName(variable)» mod «maxValue - minValue + 1»)«IF minValue != 0» «IF minValue > 0»+«ELSE»-«ENDIF» «Math.abs(minValue)»«ENDIF»
							 * ''' */
						} else {
							return '''
								«havocStmt»
								«context.varTrace.toName(variable)» := («context.varTrace.toName(variable)» mod «maxValue - minValue + 1»)
							'''
						}
					}
					case BITVECTOR: {
						return havocStmt
					}
				}
			} else {
				return havocStmt;
			}
		} else {
			return '''«context.varTrace.toName(variable)» := «exprToTheta.toThetaExpression(rightValue)»'''
		}
	}
	
	private def boolean outOfThetaRange(long value) {
		return value > Integer.MAX_VALUE || value < Integer.MIN_VALUE;
	}

	private static def Expression extractInvariant(Expression requirement) {
		if (requirement instanceof UnaryCtlExpression &&
			(requirement as UnaryCtlExpression).operator == UnaryCtlOperator.AG) {
			return (requirement as UnaryCtlExpression).operand
		} else if(requirement instanceof UnaryLtlExpression &&
			(requirement as UnaryLtlExpression).operator == UnaryLtlOperator.G) {
			return (requirement as UnaryLtlExpression).operand
		} else {
			throw new ThetaException("Theta only supports safety requirements (AG/G).")
		}
	}

}
