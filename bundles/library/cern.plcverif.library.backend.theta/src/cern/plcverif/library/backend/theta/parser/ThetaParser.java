/*******************************************************************************
 * (C) Copyright CERN 2017-2021. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Mihaly Dobos-Kovacs - array representation support
 *******************************************************************************/
package cern.plcverif.library.backend.theta.parser;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.io.Files;

import cern.plcverif.base.common.logging.IPlcverifLogger;
import cern.plcverif.base.models.cfa.CfaInstanceSafeFactory;
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariable;
import cern.plcverif.base.models.cfa.cfainstance.ArrayElementRef;
import cern.plcverif.base.models.cfa.cfainstance.ArrayVariable;
import cern.plcverif.base.models.cfa.cfainstance.Variable;
import cern.plcverif.base.models.cfa.cfainstance.VariableRef;
import cern.plcverif.base.models.expr.ExpressionSafeFactory;
import cern.plcverif.base.models.expr.IntType;
import cern.plcverif.base.models.expr.Literal;
import cern.plcverif.base.models.expr.Type;
import cern.plcverif.base.models.expr.utils.ExprUtils;
import cern.plcverif.base.models.expr.utils.TypeUtils;
import cern.plcverif.library.backend.theta.ThetaContext;
import cern.plcverif.library.backend.theta.exception.ThetaException;
import cern.plcverif.library.backend.theta.model.ThetaTransformationUtils;
import cern.plcverif.library.backend.theta.parser.sexpr.SExpr;
import cern.plcverif.library.backend.theta.parser.sexpr.SList;
import cern.plcverif.library.backend.theta.parser.sexpr.SParser;
import cern.plcverif.library.backend.theta.parser.sexpr.SParser.SExprParsingException;
import cern.plcverif.verif.interfaces.data.ResultEnum;
import cern.plcverif.verif.interfaces.data.cex.InstanceCexStep;
import cern.plcverif.verif.interfaces.data.cex.InstanceCounterexample;

/**
 * Parser for the command line output of Theta.
 */
public final class ThetaParser {

	private final ThetaContext context;
	private final IPlcverifLogger logger;

	/**
	 * Create a new parser.
	 * 
	 * @param context
	 *            Context
	 */
	public ThetaParser(final ThetaContext context) {
		this.context = context;
		this.logger = context.getVerifResult().currentStage();
	}

	/**
	 * Parse the output of Theta. The result will be available in the context.
	 * 
	 * @param output
	 *            Output of Theta
	 * @throws ThetaException
	 *             if the output cannot be parsed or an inner exception occurred
	 *             in Theta
	 */
	public void parse(final String output) throws ThetaException {
		if (output.startsWith("(SafetyResult Safe)")) {
			context.getVerifResult().setResult(ResultEnum.Satisfied);
			context.getVerifResult().currentStage().logWarning(
					"Note that the Theta backend uses arithmetic integers, thus violations related to integer over- or underflows are not considered in the verification.");
		} else if (output.startsWith("(SafetyResult Unsafe")) {
			context.getVerifResult().setResult(ResultEnum.Violated);
			parseCex();
		} else if (output.startsWith("Exception of type")) {
			Pattern pattern = Pattern.compile("^Exception of type (.+) occurred.*");
			Matcher matcher = pattern.matcher(output);
			if (matcher.find()) {
				String exceptionType = matcher.group(1);
				
				// Try to diagnose the problem
				if (exceptionType.equals("UnsatisfiedLinkError")) {
					logger.logError("Theta does not find the required libraries. Check that the libraries are at '%s' or adjust the corresponding `lib_path` setting for the backend.", 
							context.getAbsoluteLibraryPath());
				} else if (exceptionType.equals("OutOfMemoryError")) {
					logger.logWarning("The Theta verification backend did not provide any result as it ran out of memory.", exceptionType);
					context.getVerifResult().setResult(ResultEnum.Unknown);
					return;
				} else {
					logger.logError("An inner exception '%s' occurred during the execution of Theta.", exceptionType);
				}
			}
			
			throw new ThetaException("Inner Theta exception.");
		} else if (output.startsWith("Invalid parameters")) {
			logger.logError("Theta was called with invalid parameters.");
			throw new ThetaException("Invalid Theta parameters.");
		} else {
			logger.logError("The result of Theta could not be parsed as satisfied or violated.");
			throw new ThetaException("Could not parse the results of Theta backend.");
		}
	}

	private void parseCex() throws ThetaException {
		try {
			final String cexLines = Files.asCharSource(context.getCexFile().toFile(), Charset.defaultCharset()).read();
			final List<SExpr> exprs = SParser.parse(cexLines).asList().getContent();
			if (exprs.isEmpty() || !exprs.get(0).isAtom() || !exprs.get(0).toString().equalsIgnoreCase("trace")) {
				logger.logError("Counterexample could not be parsed: output does not start with 'trace'.");
				throw new ThetaException("Could not parse counterexample.");
			}
			if (exprs.size() == 1) {
				logger.logError("Counterexample is empty.");
				throw new ThetaException("Empty counterexample.");
			}
			if ((exprs.size() - 1) % 2 == 0) {
				logger.logError("Counterexample could not be parsed: its length must be odd.");
				throw new ThetaException("Counterexample with wrong length.");
			}
			final InstanceCounterexample instanceCex = parseTrace(exprs);
			context.getVerifResult().setInstanceCounterexample(instanceCex);
		} catch (SExprParsingException ex) {
			throw new ThetaException("Unable to parse the counterexample, invalid format.", ex);
		} catch (IOException ex) {
			throw new ThetaException("Unable to open counterexample file", ex);
		}
	}

	private InstanceCounterexample parseTrace(final List<SExpr> exprs) throws ThetaException {
		final InstanceCounterexample instanceCex = new InstanceCounterexample();
		final String eocName = context.getLocTrace().toName(context.getVerifProblem().getEocLocation());
		final String bocName = context.getLocTrace().toName(context.getVerifProblem().getBocLocation());
		for (int i = 1; i < exprs.size(); i += 2) {
			if (!exprs.get(i).isList()) {
				logger.logError("Counterexample could not be parsed: was expecting a list at position " + i + ".");
				throw new ThetaException("Could not parse counterexample.");
			}
			final List<SExpr> exprList = exprs.get(i).asList().getContent();
			if (exprList.isEmpty() || !exprList.get(0).isAtom()
					|| !"CfaState".equalsIgnoreCase(exprList.get(0).toString())) {
				logger.logError("Counterexample could not be parsed: was expecting 'CfaState' at position " + i + ".");
				throw new ThetaException("Could not parse counterexample.");
			}
			if (exprList.size() < 2 || !exprList.get(1).isAtom()) {
				logger.logDebug("Counterexample could not be parsed: could not extract location at position " + i + ": " + exprList.toString());
				continue;
			}

			final String locName = exprList.get(1).toString();
			if (locName.equals(eocName) || locName.equals(bocName)) {
				parseStep(instanceCex, exprList, i);
			}
		}
		return instanceCex;
	}

	private InstanceCexStep parseStep(InstanceCounterexample parentCex, final List<SExpr> exprList, final int i)
			throws ThetaException {
		if (exprList.size() < 3 || !exprList.get(2).isList()) {
			logger.logError("Counterexample could not be parsed: no valuations at position " + i + ".");
			throw new ThetaException("Could not parse counterexample.");
		}

		final List<SExpr> valList = exprList.get(2).asList().getContent();
		if (valList.isEmpty() || !valList.get(0).isAtom() || !"ExplState".equalsIgnoreCase(valList.get(0).toString())) {
			logger.logError("Counterexample could not be parsed: expecting ExplState at position " + i + ".");
			throw new ThetaException("Could not parse counterexample.");
		}
		
		final InstanceCexStep step = new InstanceCexStep("Cycle " + i, parentCex);
		
		for (final SExpr valItem : valList.subList(1, valList.size())) {
			if (!valItem.isList()) {
				logger.logError("Counterexample could not be parsed: expecting valuation as list at position " + i + ".");
				throw new ThetaException("Could not parse counterexample.");
			}
			final List<SExpr> varValuePair = valItem.asList().getContent();
			if (varValuePair.size() != 2) {
				logger.logError("Counterexample could not be parsed: expecting pair in valuation at position " + i + ".");
				throw new ThetaException("Could not parse counterexample.");
			}

			final AbstractVariable var = context.getVarTrace().toModelElement(varValuePair.get(0).toString());
			if (var instanceof ArrayVariable) {
				final ArrayVariable arrayVar = (ArrayVariable) var;
				if(!varValuePair.get(1).asList().get(0).asAtom().getData().equals("array")) {
					logger.logError("Counterexample could not be parsed: expecting array literal at position " + i + ".");
					throw new ThetaException("Could not parse counterexample.");
				}
				
				final SList list = varValuePair.get(1).asList();
				for(int j = 1; j < list.size() - 1; j++) {
					final ArrayElementRef elementRef = CfaInstanceSafeFactory.INSTANCE.createArrayElementRef(
						arrayVar, 
						Arrays.asList(createLiteralFromString(list.get(j).asList().get(0).toString(), ExpressionSafeFactory.INSTANCE.createIntType(true, 16)))
					);
					final Literal value = createLiteralFromString(list.get(j).asList().get(1).toString(), arrayVar.getType());

					step.setValue(elementRef, value);
				}
			} else if(var instanceof Variable) {
				final Literal value = createLiteralFromString(varValuePair.get(1).toString(), var.getType());
				final VariableRef varRef = CfaInstanceSafeFactory.INSTANCE.createVariableRef((Variable) var);
				
				step.setValue(varRef, value);
			} else {
				logger.logError("Counterexample could not be parsed: Unsupported variable type: " + var.getType());
				throw new ThetaException("Unsupported variable type: " + var.getType());
			}
		}
		return step;
	}
	
	private Literal createLiteralFromString(final String lit, final Type type) throws ThetaException {
		if (type instanceof IntType) {
			switch(ThetaTransformationUtils.parseIntegerRepresentation(context.getSettings().getIntegerRepresentation())) {
				case MODULO:
				case UNBOUNDED:
					return ExprUtils.createLiteralFromString(lit, type);
				case BITVECTOR:
					return createBvLiteralFromString(lit, (IntType) type);
				default:
					throw new ThetaException("Unknown integer representation");
			}
		} else {
			return ExprUtils.createLiteralFromString(lit, type);
		}
	}
	
	private Literal createBvLiteralFromString(final String lit, final IntType type) {
		final long minValue = TypeUtils.minValue(type);
		
		final String stringValue = lit.split("'")[1];
		final long longValue = Long.parseLong(stringValue.substring(1), 2) + (stringValue.charAt(1) == '0' ? 0 : minValue);
				
		return ExpressionSafeFactory.INSTANCE.createIntLiteral(longValue, EcoreUtil.copy(type));
	}
}
