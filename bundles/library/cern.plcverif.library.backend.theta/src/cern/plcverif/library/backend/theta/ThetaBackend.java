/*******************************************************************************
 * (C) Copyright CERN 2017-2021. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Akos Hajdu - further improvements and additions
 *   Mihaly Dobos-Kovacs - array representation support
 *******************************************************************************/
package cern.plcverif.library.backend.theta;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.nio.file.Path;

import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.progress.ICanceling;
import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.exceptions.SettingsSerializerException;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.interfaces.data.result.StageStatus;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.cfa.transformation.CfaTransformationGoal;
import cern.plcverif.base.models.cfa.transformation.ElseEliminator;
import cern.plcverif.library.backend.theta.exception.ThetaException;
import cern.plcverif.library.backend.theta.exception.ThetaGracefulInterruptionException;
import cern.plcverif.library.backend.theta.exception.ThetaRuntimeException;
import cern.plcverif.library.backend.theta.executor.ThetaExecutor;
import cern.plcverif.library.backend.theta.model.ThetaCfaTransformer;
import cern.plcverif.library.backend.theta.model.TryRemoveBitwiseOps;
import cern.plcverif.library.backend.theta.parser.ThetaParser;
import cern.plcverif.verif.interfaces.IBackend;
import cern.plcverif.verif.interfaces.data.RequirementRepresentationStrategy;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.VerificationResult;
import cern.plcverif.verif.interfaces.data.VerificationStageTags;

/**
 * Main entry point for the Theta backend.
 */
public final class ThetaBackend implements IBackend {
	private final ThetaSettings thetaSettings;

	/**
	 * Create a new instance with settings.
	 * 
	 * @param settings
	 *            Settings
	 */
	public ThetaBackend(final Settings settings) {
		if (settings instanceof SettingsElement) {
			try {
				this.thetaSettings = SpecificSettingsSerializer.parse(settings.toSingle(), ThetaSettings.class);
			} catch (final SettingsParserException e) {
				throw new PlcverifPlatformException("Unable to parse the settings of the Theta backend.", e);
			}
		} else {
			throw new PlcverifPlatformException("Unexpected settings type: " + settings);
			// OK, better error handling would be needed here, to be done.
			// (Normally if you don't get a SettingsElement here, you did
			// something rather weird in your BackendExtension.)
		}
	}

	@Override
	public SettingsElement retrieveSettings() {
		try {
			return SpecificSettingsSerializer.toGenericSettings(thetaSettings);
		} catch (SettingsParserException | SettingsSerializerException e) {
			throw new PlcverifPlatformException("Unable to serialize the settings of the Theta backend.", e);
		}
	}

	@Override
	public void execute(final VerificationProblem input, final VerificationResult result, ICanceling canceler) {
		try {
			final Path targetDir = result.getOutputDirectoryOrCreateTemp();
			final StageStatus status = execute(input, result, targetDir, canceler == null ? ICanceling.NEVER_CANCELING_INSTANCE : canceler);
			result.currentStage().setStatus(status);
		} catch (final IOException e) {
			throw new PlcverifPlatformException("Unexpected IO exception during Theta backend execution: " + e.getMessage(), e);
		}
	}

	/**
	 * Execute the Theta backend.
	 * 
	 * @param input
	 *            Input problem to be checked
	 * @param result
	 *            Result of checking the problem
	 * @param targetDir
	 *            Target directory where models and other files are generated
	 * @param canceler 
	 * @return Status of the stage
	 */
	public StageStatus execute(final VerificationProblem input, final VerificationResult result, final Path targetDir, ICanceling canceler) {
		checkNotNull(input, "The given verification problem shall not be null.");
		checkNotNull(input.getModel(), "The model in the given verification problem shall not be null.");
		checkArgument(input.getModel() instanceof CfaNetworkInstance, "The model in the given verification problem has not been instantiated.");
		checkNotNull(input.getRequirement(), "The requirement in the given verification problem shall not be null.");
		checkNotNull(result, "The given verification result shall not be null.");
		checkNotNull(canceler, "The canceler shall not be null.");

		// Pre-computation
		result.switchToStage("Theta model building");
		ElseEliminator.transform(input.getModel());
		try {
			new TryRemoveBitwiseOps(result.currentStage()).transformCfi((CfaNetworkInstance) input.getModel());			
		} catch (ThetaRuntimeException e) {
			result.currentStage().logError("Error happened in Theta backend that prevented the model checking. " + e.getMessage(), e);
			return StageStatus.Unsuccessful;
		}
		
		final ThetaContext context = new ThetaContext(input, result, targetDir, thetaSettings);
		result.setBackendAlgorithmId(thetaSettings.getAlgorithmConfigString());

		try {
			// Model generation
			ThetaCfaTransformer.transform(context);
			result.currentStage().setStatus(StageStatus.Successful);
			if (canceler.isCanceled()) {
				return StageStatus.Skipped;
			}

			// Execution
			result.switchToStage("Theta execution", VerificationStageTags.BACKEND_EXECUTION);
			final String output = new ThetaExecutor(context, canceler).execute();
			result.currentStage().setStatus(StageStatus.Successful);

			// Parsing the output
			result.switchToStage("Theta output parsing");
			new ThetaParser(context).parse(output);
			result.currentStage().setStatus(StageStatus.Successful);

			return StageStatus.Successful;
		} catch (ThetaGracefulInterruptionException e) {
			// reason was already logged
			return StageStatus.Unsuccessful;
		} catch (final ThetaException e) {
			result.currentStage().logError("Error happened in Theta backend that prevented the model checking. " + e.getMessage(), e);
			return StageStatus.Unsuccessful;
		}
	}

	@Override
	public CfaTransformationGoal getPreferredModelFormat() {
		switch(thetaSettings.getArrayRepresentation()) {
			case "DYNAMIC":
				return CfaTransformationGoal.INSTANCE_ARRAYS;
			case "ENUMERATED":
				return CfaTransformationGoal.INSTANCE_ENUMERATED;
			default:
				throw new UnsupportedOperationException("Array representation type not supported");
		}
	}

	@Override
	public boolean isInliningRequired() {
		return true;
	}

	@Override
	public RequirementRepresentationStrategy getPreferredRequirementFormat() {
		return RequirementRepresentationStrategy.IMPLICIT;
	}
	
	@Override
	public boolean areCfaAnnotationsRequired() {
		return false;
	}
}
