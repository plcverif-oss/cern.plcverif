/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Akos Hajdu - refactoring and documentation
 *   Tamas Toth - minor stylistic modifications
 *******************************************************************************/
package cern.plcverif.library.backend.theta.exception;

/**
 * Custom runtime exception for Theta.
 */
public class ThetaRuntimeException extends RuntimeException {
	private static final long serialVersionUID = -6567017956348615986L;

	/**
	 * Create a new exception with a message.
	 * 
	 * @param message
	 *            Message
	 */
	public ThetaRuntimeException(final String message) {
		super(message);
	}

	/**
	 * Create a new exception with a message and a cause.
	 * 
	 * @param message
	 *            Message
	 * @param cause
	 *            Cause
	 */
	public ThetaRuntimeException(final String message, final Throwable cause) {
		super(message, cause);
	}
}
