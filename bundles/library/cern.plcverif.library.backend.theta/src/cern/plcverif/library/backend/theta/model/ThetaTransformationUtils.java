/******************************************************************************
 * (C) Copyright CERN 2017-2021. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Akos Hajdu - further improvements and additions
 *   Mihaly Dobos-Kovacs - integer representation
 *****************************************************************************/
package cern.plcverif.library.backend.theta.model;

import static com.google.common.base.Preconditions.checkState;

import java.util.Collection;

import com.google.common.collect.Sets;

import cern.plcverif.library.backend.theta.exception.ThetaException;

/**
 * Utility class related to the model transformation.
 */
public final class ThetaTransformationUtils {
	private static final String VALID_CHARS = "a-zA-Z0-9_";
	private static final String INVALID_CHAR_REGEX = String.format("[^%s]", VALID_CHARS);
	private static final String VALID_NAME_REGEX = String.format("[%s]+", VALID_CHARS);
	private static final Collection<String> KEYWORDS = Sets.newHashSet("main", "process", "init", "error", "end", "loc",
			"var", "bool", "rat", "int", "assume", "havoc", "return", "if", "then", "else", "iff", "imply", "forall",
			"exists", "or", "and", "not", "mod", "rem", "true", "false");
	
	public enum IntegerRepresentation {
		UNBOUNDED, MODULO, BITVECTOR
	}

	private ThetaTransformationUtils() {
	}

	/**
	 * Checks if a name would be valid in the Theta model.
	 * 
	 * @param name
	 *            Name to be checked
	 * @return True if the name is valid, false otherwise
	 */
	public static boolean isValidName(final String name) {
		if (KEYWORDS.contains(name)) {
			return false;
		}

		return name.matches(VALID_NAME_REGEX);
	}

	/**
	 * Transforms a name into a valid name (if it is not already valid).
	 * 
	 * @param name
	 *            Name to be transformed
	 * @return Transformed name that is ensured to be valid
	 */
	public static String makeValidName(final String name) {
		String ret = name.replaceAll("\\.", "#");
		ret = ret.replaceAll(INVALID_CHAR_REGEX, "_");
		if (KEYWORDS.contains(ret)) {
			ret = ret + "_pv";
		}

		checkState(ret.matches(VALID_NAME_REGEX));

		return ret;
	}
	
	/**
	 * Parses the textual integer representation setting
	 * 
	 * @param name The value of the said setting
	 * @return The textual setting parsed to an enum
	 * @throws ThetaException If the setting is invalid / unsupported, an exception is thrown
	 */
	public static IntegerRepresentation parseIntegerRepresentation(final String name) throws ThetaException {
		switch(name) {
			case "UNBOUNDED": 
				return IntegerRepresentation.UNBOUNDED;
			case "MODULO": 
				return IntegerRepresentation.MODULO;
			case "BITVECTOR": 
				return IntegerRepresentation.BITVECTOR;
			default:
				throw new ThetaException("Unsupported integer representation: " + name);
		}
	}
}
