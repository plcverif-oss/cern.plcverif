/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.library.backend.theta.model

import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.base.interfaces.data.result.JobStage
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance
import cern.plcverif.base.models.cfa.textual.CfaToString
import cern.plcverif.base.models.cfa.transformation.ICfaInstanceTransformation
import cern.plcverif.base.models.expr.BinaryArithmeticExpression
import cern.plcverif.base.models.expr.BinaryArithmeticOperator
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.ExpressionSafeFactory
import cern.plcverif.base.models.expr.IntLiteral
import cern.plcverif.base.models.expr.IntType
import cern.plcverif.base.models.expr.utils.ExprUtils
import cern.plcverif.library.backend.theta.exception.ThetaRuntimeException
import com.google.common.base.Preconditions
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.util.EcoreUtil

import static org.eclipse.emf.ecore.util.EcoreUtil.copy

class TryRemoveBitwiseOps implements ICfaInstanceTransformation {
	ExpressionSafeFactory factory = ExpressionSafeFactory.INSTANCE;
	JobStage stageLog;
	
	new(JobStage stageLog) {
		this.stageLog = Preconditions.checkNotNull(stageLog);
	}

	override transformCfi(CfaNetworkInstance cfaToTransform) {
		val bitwiseExprs = EmfHelper.getAllContentsOfType(cfaToTransform, BinaryArithmeticExpression, false, [it |
			ExprUtils.isBitwiseOp(it.operator)
		]);

		if (!bitwiseExprs.isEmpty) {			
			stageLog.logInfo("Bitwise operation removal started. Candidates: %s.", bitwiseExprs.size);
		}
		
		var change = false;
		for (bwExpr : bitwiseExprs) {
			change = tryRemoveBitwiseOp(bwExpr) || change;
		}

		return change;
	}
	

	private def tryRemoveBitwiseOp(BinaryArithmeticExpression expression) {
		if (expression.rightOperand instanceof IntLiteral) {
			val rightLiteralBinaryString = toBinaryString(expression.rightOperand as IntLiteral);
			 
			if (expression.operator == BinaryArithmeticOperator.BITWISE_AND &&
				isExactlyOneOne(rightLiteralBinaryString)
			) {
				// x BW_AND 0..0100..0;
				// the only '1' is at position 'n' (indexed from 0 from the right)
				// --> replace with '(x % 2^(n+1)) - (x % 2^n)'
				
				val n = lowestOneBitIndex(rightLiteralBinaryString);
				replaceAndLog(expression, selection(expression.leftOperand, n))		
				return true;
			}
			
			if (expression.operator == BinaryArithmeticOperator.BITWISE_OR &&
				isExactlyOneOne(rightLiteralBinaryString)
			) {
				// x BW_OR 0..0100..0;
				// the only '1' is at position 'n' (indexed from 0 from the right)
				// --> replace with 'x + 2^n - ((x % 2^(n+1)) - (x % 2^n))'
				
				val n = lowestOneBitIndex(rightLiteralBinaryString);
				val intType = expression.leftOperand.type as IntType;
				replaceAndLog(
					expression,
					factory.plus(
						copy(expression.leftOperand),
						factory.minus(
							factory.pow(
								factory.createIntLiteral(2, copy(intType)),
								factory.createIntLiteral(n, copy(intType))
							),
							selection(expression.leftOperand, n)
						)
					)
				);		
				return true;
			}
			
			if (expression.operator == BinaryArithmeticOperator.BITWISE_AND &&
				isExactlyOneZero(rightLiteralBinaryString)
			) {
				// x BW_AND 1..1011..1;
				// the only '0' is at position 'n' (indexed from 0 from the right)
				// --> replace with 'x - ((x % 2^(n+1)) - (x % 2^n))'
				
				val n = lowestOneBitIndex(rightLiteralBinaryString);
				replaceAndLog(
					expression,
					factory.minus(
						copy(expression.leftOperand),
						selection(expression.leftOperand, n)
					)
				);
				return true;
			}
			
			stageLog.logInfo("Unable to replace the following bitwise expression: '%s'.", 
				CfaToString.toDiagString(expression)
			);
		}
		
		return false;
	}
	
	
	private def void replaceAndLog(EObject toBeReplaced, EObject replacement) {
		EcoreUtil.replace(toBeReplaced, replacement);
		stageLog.logInfo(String.format("To avoid having bitwise operators, '%s' has been replaced with '%s'.",
			CfaToString.toDiagString(toBeReplaced),
			CfaToString.toDiagString(replacement)
		));
	}
	
	/**
	 * Returns '(x % 2^(n+1)) - (x % 2^n)'
	 */
	private def selection(Expression expr, long n) {
		Preconditions.checkNotNull(expr, "expr");
		Preconditions.checkArgument(expr.type instanceof IntType, "expr must be of IntType");
				
		val intType = expr.type as IntType;
		Preconditions.checkArgument(n >= 0 && n < intType.bits, "Invalid n value");
		
		// Special case: if n==intType.bits-1, then x % 2^(n+1) == x
		return factory.minus(
			if (n == intType.bits - 1) copy(expr) 
			else
				factory.mod(
					copy(expr),
					factory.pow(
						factory.createIntLiteral(2, copy(intType)),
						factory.createIntLiteral(n + 1, copy(intType))
					)
				),
			factory.mod(
				copy(expr),
				factory.pow(
					factory.createIntLiteral(2, copy(intType)),
					factory.createIntLiteral(n, copy(intType))
				)
			)
		);
	}

	static def boolean isExactlyOneOne(String binaryString) {
		return onesInBinary(binaryString) == 1;
	}

	static def boolean isExactlyOneZero(String binaryString) {
		return onesInBinary(binaryString) == (binaryString.length - 1);
	}

	static def long onesInBinary(String binaryString) {
		return binaryString.chars.filter([it | it === toChar('1')]).count;
	}
	
	static def long lowestOneBitIndex(String binaryString) {
		return lowestBitIndex(binaryString, '1');
	}
	
	static def long lowestBitIndex(String binaryString, char bitVal) {
		for (var i = 0; i < binaryString.length; i++) {
			if (binaryString.charAt(binaryString.length - 1 - i) === bitVal) {
				return i;
			}
		}
		return -1;
	}
	

	/**
	 * Converts the given literal value to binary string.
	 * The length of the returned string will match the bit width of the literal.
	 */
	static def String toBinaryString(IntLiteral intLiteral) {
		if (intLiteral.value < 0) {
			throw new ThetaRuntimeException("Removing bitwise operations failed. It is not possible yet to deal with bitwise operation on negative literal values. It was not possible to eliminate the bitwise operations in the CFA for Theta execution.");			
		}

		// TODO support negative values?
		var ret = Long.toBinaryString(intLiteral.value);
		while (ret.length < (intLiteral.type as IntType).bits) {
			ret = "0" + ret;
		}
		
		if (ret.length != (intLiteral.type as IntType).bits) {		
			throw new ThetaRuntimeException("Padding resulted in unexpected string length at TryRemoveBitwiseOps.toBinaryString. It was not possible to eliminate the bitwise operations in the CFA for Theta execution.");
		}
		return ret;
	}
	
	private def static char toChar(String str) {
		Preconditions.checkArgument(str.length == 1, "A char literal string must have length of 1");
        return str.charAt(0);
    }

}
