/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Akos Hajdu - further implementation
 *   Tamas Toth - minor stylistic modifications
 *******************************************************************************/
package cern.plcverif.library.backend.theta;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.nio.file.Path;

import com.google.common.base.Preconditions;

import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariable;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.VerificationResult;
import cern.plcverif.verif.utils.backend.BackendNameTrace;

/**
 * Context for the Theta backend that is used to communicate between the phases.
 */
public final class ThetaContext {

	private final VerificationProblem verifProblem;
	private final VerificationResult result;
	private final ThetaSettings settings;

	private final String modelId; // This string can be used as a filename.
	private final Path modelFile;
	private final Path cexFile;

	private BackendNameTrace<AbstractVariable> varTrace = null;
	private BackendNameTrace<Location> locTrace = null;

	/**
	 * Create a new context.
	 * 
	 * @param verifProblem
	 *            Problem to be checked
	 * @param result
	 *            Result of checking the problem
	 * @param targetDir
	 *            Target directory where models and other files are generated
	 * @param settings
	 *            Settings for the backend
	 */
	public ThetaContext(final VerificationProblem verifProblem, final VerificationResult result, final Path targetDir,
			final ThetaSettings settings) {
		this.verifProblem = checkNotNull(verifProblem);
		this.result = checkNotNull(result);
		this.settings = checkNotNull(settings);
		this.modelId = result.getJobMetadata().getId().replace("[^a-zA-Z0-9_\\.]", "_");
		
		this.modelFile = checkNotNull(targetDir).resolve(modelId + ".theta");
		this.cexFile = checkNotNull(targetDir).resolve(modelId + ".cex");
	}

	/**
	 * Gets the path of the Theta model file
	 * 
	 * @return Path of the Theta model file
	 */
	public Path getModelFile() {
		return modelFile;
	}
	
	/**
	 * Gets the path of the Theta counterexample file
	 * 
	 * @return Path of the Theta counterexample file
	 */
	public Path getCexFile() {
		return cexFile;
	}

	/**
	 * Gets the mapping between the PLCverif and Theta variables
	 * 
	 * @return Variable trace
	 */
	public BackendNameTrace<AbstractVariable> getVarTrace() {
		checkState(varTrace != null, "Variable trace has not been set yet.");
		return varTrace;
	}

	/**
	 * Sets the mapping between the PLCverif and Theta variables
	 * 
	 * @param varTrace
	 *            Variable trace
	 */
	public void setVarTrace(final BackendNameTrace<AbstractVariable> varTrace) {
		checkState(this.varTrace == null, "Variable trace has already been set.");
		this.varTrace = varTrace;
	}

	/**
	 * Gets the mapping between the PLCverif and Theta locations
	 * 
	 * @return Location trace
	 */
	public BackendNameTrace<Location> getLocTrace() {
		checkState(locTrace != null, "Location trace has not been set yet.");
		return locTrace;
	}

	/**
	 * Sets the mapping between the PLCverif and Theta locations
	 * 
	 * @param locTrace
	 *            Location trace
	 */
	public void setLocTrace(final BackendNameTrace<Location> locTrace) {
		checkState(this.locTrace == null, "Location trace has already been set.");
		this.locTrace = locTrace;
	}

	/**
	 * Gets the verification problem
	 * 
	 * @return Verification problem
	 */
	public VerificationProblem getVerifProblem() {
		return verifProblem;
	}

	/**
	 * Gets the result of verification
	 * 
	 * @return Verification result
	 */
	public VerificationResult getVerifResult() {
		return result;
	}

	/**
	 * Gets the ID of the model
	 * 
	 * @return Model ID
	 */
	public String getModelId() {
		return modelId;
	}

	/**
	 * Gets the settings
	 * 
	 * @return Settings
	 */
	public ThetaSettings getSettings() {
		return settings;
	}

	/**
	 * Returns the absolute library path ({@link ThetaSettings#getLibraryPath()}), 
	 * using the {@link VerificationResult#makeAbsoluteToProgramDirectory(Path)} method.
	 * @return The absolute path to the library location.
	 */
	public Path getAbsoluteLibraryPath() {
		Preconditions.checkNotNull(getVerifResult(), "ThetaContext.verifResult");
		Preconditions.checkNotNull(getSettings(), "ThetaContext.settings");
		return this.getVerifResult().makeAbsoluteToProgramDirectory(this.getSettings().getLibraryPath()).normalize();
	}
	
	/**
	 * Returns the absolute path to the Theta binary ({@link ThetaSettings#getBinaryPath()}), 
	 * using the {@link VerificationResult#makeAbsoluteToProgramDirectory(Path)} method.
	 * @return The absolute path to the library location.
	 */
	public Path getAbsoluteBinaryPath() {
		Preconditions.checkNotNull(getVerifResult(), "ThetaContext.verifResult");
		Preconditions.checkNotNull(getSettings(), "ThetaContext.settings");
		return this.getVerifResult().makeAbsoluteToProgramDirectory(this.getSettings().getBinaryPath());
	}
}
