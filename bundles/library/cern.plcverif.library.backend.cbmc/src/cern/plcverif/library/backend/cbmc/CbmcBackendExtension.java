/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.cbmc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsSerializer;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.help.SettingsHelp;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.verif.interfaces.IBackend;
import cern.plcverif.verif.interfaces.IBackendExtension;
import cern.plcverif.verif.job.VerificationJobOptions;

/**
 * Extension point to provide CBMC backend.
 */
public class CbmcBackendExtension implements IBackendExtension {
	private static final Logger LOGGER = LogManager.getLogger(CbmcBackendExtension.class);
	public static final String CMD_ID = "cbmc";

	@Override
	public IBackend createBackend() {
		return new CbmcBackend(loadDefaultSettings());
	}

	@Override
	public IBackend createBackend(SettingsElement settings) throws SettingsParserException {
		Settings compositeSettings = loadDefaultSettings();
		compositeSettings.overrideWith(settings);
		return new CbmcBackend(compositeSettings);
	}

	public SettingsElement loadDefaultSettings() {
		String settingsPath = SettingsSerializer.replaceOsPlaceholder("/settings/default.${OS}.settings");
		try {
			String settingString = IoUtils.readResourceFile(
					settingsPath,
					this.getClass().getClassLoader());
			SettingsElement ret = SettingsSerializer.parseSettingsFromString(settingString).toSingle();
			ret.setValue(CMD_ID);
			return ret;
		} catch (SettingsParserException e) {
			LOGGER.debug("Unable to load the default settings from resource " + settingsPath + ".");
			return SettingsElement.createRootElement(VerificationJobOptions.BACKEND, CMD_ID);
		}
	}

	@Override
	public void fillSettingsHelp(SettingsHelp help) {
		help.addPluginDetails(CMD_ID, "CBMC verification backend", 
			"Solves the verification problem by using the CBMC model checker. Only applicable if job=verif.",	
			CbmcBackendExtension.class);
		
		// Try to load the default settings
		CbmcSettings defaults;
		try {
			defaults = SpecificSettingsSerializer.parse(loadDefaultSettings(), CbmcSettings.class);
		} catch (SettingsParserException e) {
			// best effort (don't die on help creation)
			defaults = new CbmcSettings();
		}
		
		// Fill the 'SettingsHelp' object
		SpecificSettingsSerializer.fillSettingsHelp(help, CbmcSettings.class, defaults);
	}
}
