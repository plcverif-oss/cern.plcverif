/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 * 
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.library.backend.cbmc.model

import cern.plcverif.base.models.cfa.cfabase.Transition
import cern.plcverif.base.models.cfa.cfadeclaration.AssignmentTransition
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.CallTransition
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructure
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructureRef
import cern.plcverif.base.models.cfa.cfadeclaration.Field
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef
import cern.plcverif.base.models.cfa.transformation.CfdInitialAssignmentsToTransition
import cern.plcverif.base.models.cfa.utils.CfaDeclarationUtils
import cern.plcverif.base.models.expr.utils.ExprUtils
import cern.plcverif.library.backend.cbmc.CbmcSettings
import cern.plcverif.support.codegen.generator.CFamilyExprTransformer
import cern.plcverif.support.codegen.generator.c.StructNames
import cern.plcverif.support.codegen.generator.c.TypeToCType
import cern.plcverif.verif.interfaces.data.VerificationProblem
import cern.plcverif.verif.interfaces.data.VerificationResult
import cern.plcverif.verif.interfaces.data.cex.DeclarationCexStep
import cern.plcverif.verif.interfaces.data.cex.DeclarationCounterexample
import cern.plcverif.verif.interfaces.data.cex.ICounterexample
import cern.plcverif.verif.utils.backend.BackendNameTrace
import com.google.common.base.Preconditions
import java.util.Comparator
import java.util.List
import java.util.Map
import java.util.TreeMap

import static extension cern.plcverif.base.models.cfa.utils.CfaDeclarationUtils.isEmpty
import static extension cern.plcverif.support.codegen.util.GeneratorUtils.isTempField
import cern.plcverif.base.models.cfa.cfabase.ArrayType

class CbmcModelBuilderCfd extends CbmcModelBuilder {
	private static final class CbmcMethodNameTrace extends BackendNameTrace<AutomatonDeclaration> {
		new() {
			super([it|BackendNameTrace::basicNameSanitizer(it)]);
		}

		override protected isReservedWord(String name) {
			return C_KEYWORDS.contains(name);
		}
	}
	
	CfaNetworkDeclaration cfd;

	public val static String CONTEXT_VARIABLE = "__context";

	StructNames structNames = new StructNames();
	TypeToCType typeTransformer = new TypeToCType(structNames);
	CbmcMethodNameTrace methodNameTrace = new CbmcMethodNameTrace();
	CFamilyExprTransformer exprTransformer

	protected new(VerificationProblem verifProblem, VerificationResult result, CbmcSettings settings) {
		super(verifProblem, result, settings);

		Preconditions.checkNotNull(typeTransformer);
		exprTransformer = CFamilyExprTransformer.createForCfd(CONTEXT_VARIABLE, true, typeTransformer);

		val cfa = verifProblem.model;
		Preconditions.checkArgument(cfa instanceof CfaNetworkDeclaration,
			"CbmcModelBuilderCfi is only applicable to CFAs of type CfaNetworkDeclaration.")
		this.cfd = cfa as CfaNetworkDeclaration;
	}

	override build() '''
		«fileHeader()»
		
		// Root data structure
		«FOR nestedDs : topologicalOrder(cfd.rootDataStructure.eAllContents.filter(DataStructure).toList)»
			«representDataStructure(nestedDs)»
		«ENDFOR»
		
		// Global variables
		«FOR field : cfd.rootDataStructure.fields»
			«representField(field)»
		«ENDFOR»
		bool «BOC_MARKER»;
		bool «EOC_MARKER»;
		
		// Automata declarations
		«FOR automaton : cfd.automata»
			«declareAutomaton(automaton)»
		«ENDFOR»
		
		// Automata
		«FOR automaton : cfd.automata»
			«representAutomaton(automaton)»
		«ENDFOR»
		
		// Main
		void main() {
			// Initial values
			«FOR initAmt : CfdInitialAssignmentsToTransition.getInstantiatedInitAmts(cfd).filter[it | !it.leftValue.isRefToTemp]»
				«val initValExprToStr = CFamilyExprTransformer.createForCfd("", false, typeTransformer)»
				«initValExprToStr.toString(initAmt.leftValue)» = «initValExprToStr.toString(initAmt.rightValue)»;
			«ENDFOR»
			
			«representCallTo(cfd.mainAutomaton, cfd.mainContext)»
		}
	'''
	

	private def boolean isRefToTemp(DataRef ref) {
		// Extract field
		var DataRef current = ref;
		while (current !== null) {
			if (current instanceof FieldRef) {
				if (current.field.isTempField) {
					return true;
				}
			}
			current = current.prefix;
		}

		return false;
	}

	private def representAutomaton(AutomatonDeclaration automaton) {
		return '''
			void «methodNameTrace.toName(automaton)»(«automaton.contextVarParameter») {
				// Temporary variables
				«FOR f : automaton.localDataStructure.definition.fields.filter[it | it.isTempField]»
					«representField(f)»
				«ENDFOR»
				
				// Start with initial location
				goto «labelNames.toName(automaton.initialLocation)»;
				«FOR loc : automaton.locations»
					«representLocation(loc)» 
				«ENDFOR»
				
				// End of automaton
				«END_OF_AUTOMATON_LABEL»: ;
			}
		''';
	}


	/**
	 * E.g. {@code void automatonName(__ContextType context);}
	 */
	private def declareAutomaton(AutomatonDeclaration automaton) {
		return '''void «methodNameTrace.toName(automaton)»(«automaton.contextVarParameter»);''';
	}

	/**
	 * E.g. {@code ContextType *context}
	 */
	private def contextVarParameter(AutomatonDeclaration automaton) {
		if (automaton.hasContext) {
			return '''«structNames.toName(automaton.localDataStructure.definition)» *«CONTEXT_VARIABLE»'''
		} else {
			return ''''''
		}
	}

	/**
	 * E.g. {@code typedef struct { ... } DataStructureName;}
	 */
	private def representDataStructure(DataStructure ds) {
		if (ds.isEmpty) {
			return ""
		} else {
			return '''
				typedef struct {
					«FOR field : ds.fields»
						«IF !field.isTempField»«representField(field)»«ENDIF»
					«ENDFOR»
				} «structNames.toName(ds)»;
			'''
		}
	}

	/**
	 * E.g. {@code FieldType fieldName};
	 * If the field's data structure type is empty, empty string is returned.
	 */
	private def representField(Field field) {
		val type = field.type;
		if (type instanceof DataStructureRef) {
			if (type.definition.isEmpty) {
				return "";
			}
		}

		// Initial values represented in the main() function
		return typeTransformer.variableDeclaration(field.type, field.name, null);
	}

	override ICounterexample parseTextualCex(List<Map<String, String>> valuemaps) {
		val ret = new DeclarationCounterexample();
		var i = 0;

		val allFieldNames = valuemaps.map[it|it.keySet].flatten.toSet;

		for (Map<String, String> map : valuemaps) {
			i++;
			val step = new DeclarationCexStep(i.toString, ret);

			for (fieldName : allFieldNames) {
				val varValuation = map.get(fieldName);
				try {
					val field = CfaDeclarationUtils.parseHierarchicalDataRef(fieldName, cfd, [field|field.name]);
					if (varValuation !== null) {
						val parseSpecificValue = parseSpecificValue(varValuation, field.type);
						if (parseSpecificValue.isPresent) {
							step.setValue(field, parseSpecificValue.get);
						} else {
							step.setValue(field, ExprUtils.createLiteralFromString(varValuation, field.type));
						}
					} else {
						// It is not expected that a variable does not have an assigned value, as 
						// initial values are set in the main() function.
						// Leave it blank.
//						val initValue = fetchInitialValue(field);
//						if (initValue instanceof Literal) {
//							step.setValue(field, EcoreUtil.copy(initValue as Literal));
//						} else {
//							System.err.println("Not sure what to do.");
//						}
					}
				} catch (IllegalArgumentException e) {
					// Parsing the hierarchical data reference was unsuccessful -- log it
					verifResult.currentStage.logDebug(
						"Unable to parse a line in the CBMC counterexample. Concerned field: '%s', value: '%s', reason: %s. This is expected for temporary variables.",
						fieldName, varValuation, e.message);
				}
			}
		}

		return ret;
	}
	
	/**
	 * E.g. {@code automatonName(&context);} or {@code automatonName();}
	 */
	protected def representCallTo(AutomatonDeclaration automaton, DataRef calleeContext) {
		if (automaton.hasContext) {
			return '''«methodNameTrace.toName(automaton)»(&«exprTransformer.toString(calleeContext)»);'''
		} else {
			return '''«methodNameTrace.toName(automaton)»();'''
		}
	}
	

	// Helpers
	protected def static hasContext(AutomatonDeclaration automaton) {
		return (automaton.localDataStructure.definition.isEmpty == false);
	}

	/**
	 * Returns a topological order of the given data structures,
	 * i.e. a sorted list of them in which there is no forward reference of data structures.
	 * (If a data structure {@code A} contains a field with the data structure type {@code B},
	 * {@code A} will be strictly <b>after</b> {@code B} in the returned list. 
	 */
	private def topologicalOrder(List<DataStructure> listToOrder) {
		// very basic algorithm to create the topological order
		// Create a map that will store the indegrees
		val Comparator<DataStructure> byName = [ DataStructure o1, DataStructure o2 |
			structNames.toName(o1).compareTo(structNames.toName(o2))
		];
		val Map<DataStructure, Integer> indegree = new TreeMap(byName);

		// Fill with values
		for (DataStructure ds : listToOrder) {
			indegree.put(ds, ds.fields.filter[it|it.type instanceof DataStructureRef].size);
		}
		Preconditions.checkState(indegree.size == listToOrder.size);

		val List<DataStructure> ret = newArrayList();

		while (!indegree.empty) {
			// Find one with zero indegree
			val item = indegree.entrySet.findFirst[it|it.value == 0];
			Preconditions.checkNotNull(item, "Loop detected in data structure references.");
			Preconditions.checkState(item.value == 0);
			val key = item.key;
			indegree.remove(key);
			ret.add(key);

			// Maintain the reference counts (slow and dirty)
			for (k : indegree.keySet) {
				indegree.put(k, indegree.get(k) - k.fields.filter [ it |
					it.type instanceof DataStructureRef && (it.type as DataStructureRef).definition == key
				].size);
			}
		}

		return ret;
	}
	
	override protected getExprTransformer() {
		return this.exprTransformer;
	}
	
	protected override CharSequence representTransitionAssignmentsAndCalls(Transition transition) {
		if (transition instanceof AssignmentTransition) {
			return '''
			«FOR amt : transition.assignments»
				«IF amt.leftValue instanceof FieldRef && (amt.leftValue as FieldRef).field.type instanceof ArrayType»
					memmove(&(«exprTransformer.toString(amt.leftValue)»), &(«exprTransformer.toString(amt.rightValue)»), sizeof «exprTransformer.toString(amt.leftValue)»);
				«ELSE»
					«exprTransformer.toString(amt.leftValue)» = «exprTransformer.toString(amt.rightValue)»;
				«ENDIF»
			«ENDFOR»
			«FOR amt : transition.assumeBounds»
					__CPROVER_assume(«exprTransformer.toString(amt)»);
			«ENDFOR»'''
		} else if (transition instanceof CallTransition) {
			return '''
				«FOR call : transition.calls»
					«val calleeExprTransformer = CFamilyExprTransformer.createForCfd(exprTransformer.toString(call.calleeContext), false, typeTransformer)»
					«IF call.calledAutomaton.hasContext»
						// Assign inputs
						«FOR inAmt : call.inputAssignments»
							«calleeExprTransformer.toString(inAmt.leftValue)» = «exprTransformer.toString(inAmt.rightValue)»;
						«ENDFOR»
					«ENDIF»
					«representCallTo(call.calledAutomaton, call.calleeContext)»
					«IF call.calledAutomaton.hasContext»
						// Assign outputs
						«FOR outAmt : call.outputAssignments»
							«exprTransformer.toString(outAmt.leftValue)» = «calleeExprTransformer.toString(outAmt.rightValue)»;
						«ENDFOR»
					«ENDIF»
				«ENDFOR»
			''';
		} else {
			throw new UnsupportedOperationException("Unsupported CFD transition: " + transition);
		}
	}


}
