/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.library.backend.cbmc.model

import cern.plcverif.base.models.cfa.cfabase.Location
import cern.plcverif.base.models.cfa.cfabase.Transition
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance
import cern.plcverif.base.models.expr.ExpressionSafeFactory
import cern.plcverif.base.models.expr.FloatType
import cern.plcverif.base.models.expr.Literal
import cern.plcverif.base.models.expr.Type
import cern.plcverif.base.models.expr.UnaryCtlExpression
import cern.plcverif.base.models.expr.UnaryCtlOperator
import cern.plcverif.base.models.expr.UnaryLtlExpression
import cern.plcverif.base.models.expr.UnaryLtlOperator
import cern.plcverif.base.models.expr.utils.ExprUtils
import cern.plcverif.library.backend.cbmc.CbmcSettings
import cern.plcverif.library.backend.cbmc.exception.CbmcException
import cern.plcverif.support.codegen.generator.CFamilyExprTransformer
import cern.plcverif.verif.interfaces.data.VerificationProblem
import cern.plcverif.verif.interfaces.data.VerificationResult
import cern.plcverif.verif.interfaces.data.cex.ICounterexample
import cern.plcverif.verif.utils.backend.BackendNameTrace
import com.google.common.base.Preconditions
import java.util.List
import java.util.Map
import java.util.Optional
import java.util.Set
import org.eclipse.emf.ecore.util.EcoreUtil

import static cern.plcverif.verif.utils.backend.BackendNameTrace.basicNameSanitizer
import cern.plcverif.library.backend.cbmc.CbmcSettings.CbmcModelVariant
import cern.plcverif.base.models.expr.utils.TlExprUtils

/**
 * Class to build C verification models for CBMC.
 */
abstract class CbmcModelBuilder implements ITextualCexParser {	
	VerificationProblem verifProblem;
	VerificationResult result;
	CbmcSettings settings;   

	protected BackendNameTrace<Location> labelNames = new BackendNameTrace<Location>([basicNameSanitizer(it)]);

	protected val static String END_OF_AUTOMATON_LABEL = "__end_of_automaton";
	public val static String BOC_MARKER = "__cbmc_boc_marker";
	public val static String EOC_MARKER = "__cbmc_eoc_marker";
	
	public val static Set<String> C_KEYWORDS = #{"alignas", "alignof", "and", "and_eq", "asm", "atomic_cancel",
		"atomic_commit", "atomic_noexcept", "auto", "bitand", "bitor", "bool", "break", "case", "catch", "char",
		"char8_t", "char16_t", "char32_t", "class", "compl", "concept", "const", "consteval", "constexpr", "const_cast",
		"continue", "co_await", "co_return", "co_yield", "decltype", "default", "delete", "do", "double",
		"dynamic_cast", "else", "enum", "explicit", "export", "extern", "false", "float", "for", "friend", "goto", "if",
		"inline", "int", "long", "mutable", "namespace", "new", "noexcept", "not", "not_eq", "nullptr", "operator",
		"or", "or_eq", "private", "protected", "public", "reflexpr", "register", "reinterpret_cast", "requires",
		"return", "short", "signed", "sizeof", "static", "static_assert", "static_cast", "struct", "switch",
		"synchronized", "template", "this", "thread_local", "throw", "true", "try", "typedef", "typeid", "typename",
		"union", "unsigned", "using", "virtual", "void", "volatile", "wchar_t", "while", "xor_eq", "xor", "main"};

	protected new(VerificationProblem verifProblem, VerificationResult result, CbmcSettings settings) {
		this.verifProblem = Preconditions.checkNotNull(verifProblem, "verifProblem");
		this.result = Preconditions.checkNotNull(result, "result");
		this.settings = Preconditions.checkNotNull(settings, "settings");
	}

	def static CharSequence buildModel(VerificationProblem verifProblem, VerificationResult result,
		CbmcSettings settings) {
		return createBuider(verifProblem, result, settings).build();
	}

	def static CbmcModelBuilder createBuider(VerificationProblem verifProblem, VerificationResult result,
		CbmcSettings settings) {
		
		if (settings.modelVariant == CbmcModelVariant.CFD_STRUCTURED) {
			Preconditions.checkArgument(verifProblem.model instanceof CfaNetworkDeclaration);
			return new CbmcModelBuilderStructuredCfd(verifProblem, result, settings);
		} else if (settings.modelVariant == CbmcModelVariant.CFD) {
			Preconditions.checkArgument(verifProblem.model instanceof CfaNetworkDeclaration);
			return new CbmcModelBuilderCfd(verifProblem, result, settings);			
		} else if (settings.modelVariant == CbmcModelVariant.CFI) {
			Preconditions.checkArgument(verifProblem.model instanceof CfaNetworkInstance);
			return new CbmcModelBuilderCfi(verifProblem, result, settings);			
		} else {
			throw new UnsupportedOperationException("Unknown CFA type: " + verifProblem.model.class.name);
		}
	}

	def CharSequence build();

	protected def CFamilyExprTransformer getExprTransformer();

	override ICounterexample parseTextualCex(List<Map<String, String>> valuemaps);

	protected abstract def CharSequence representTransitionAssignmentsAndCalls(Transition transition);

	protected def fileHeader() '''
		#include <stdbool.h>
		#include <stdint.h>
		#include <assert.h>
		#include <math.h>
		
		// Declare nondet assignment functions
		bool nondet_bool();
		«FOR prefix : #['u', '']»
			«FOR bits : #[8, 16, 32, 64] »
				«prefix»int«bits»_t nondet_«prefix»int«bits»_t();
			«ENDFOR»
		«ENDFOR»
		double nondet_float();
		double nondet_double();
	'''

	protected def representLocation(Location location) {
		return '''		
			«labelNames.toName(location)»: {
				«IF location == verifProblem.bocLocation»
					«BOC_MARKER» = true; // to indicate the beginning of the loop for the counterexample parser
					«BOC_MARKER» = false;
				«ENDIF»
				«IF location == verifProblem.eocLocation»
					«representRequirementAsAssertion()»
					«EOC_MARKER» = true; // to indicate the end of the loop for the counterexample parser
					«EOC_MARKER» = false;
				«ENDIF»
				«FOR transition : location.outgoing»
					«representTransition(transition)» 
				«ENDFOR»
				«IF location == location.parentAutomaton.endLocation»
					goto «END_OF_AUTOMATON_LABEL»;
				«ENDIF»
				//assert(false);
				return;  «««// To prevent spurious loops
			}
		''';
	}

	private def representTransition(Transition transition) {
		return '''
			«IF !ExprUtils.isTrueLiteral(transition.condition)»if («getExprTransformer().toString(transition.condition)») {«ENDIF»
				«representTransitionAssignmentsAndCalls(transition)»
				goto «labelNames.toName(transition.target)»;
			«IF !ExprUtils.isTrueLiteral(transition.condition)»}«ENDIF»
		'''
	}

	private def representRequirementAsAssertion() {
		val req = verifProblem.requirement;
		if (req instanceof UnaryCtlExpression) {
			if (req.operator == UnaryCtlOperator.AG) {
				return '''assert(«exprTransformer.toString(req.operand)»);'''
			}
		} else if (req instanceof UnaryLtlExpression) {
			if (req.operator == UnaryLtlOperator.G) {
				return '''assert(«exprTransformer.toString(req.operand)»);'''
			}
		} else if (TlExprUtils.containsNoTl(req) && !TlExprUtils.isLtl(req)){
			return '''assert(«exprTransformer.toString(req)»);'''
		}
		
		throw new CbmcException("Unable to represent the requirement. The requirement is not an AG CTL or G LTL requirement.");

	}

	/**
	 * Returns the verification result object. Never null.
	 */	
	protected def getVerifResult() {
		return result;
	}
	
	protected def Optional<Literal> parseSpecificValue(String str, Type type) {
		Preconditions.checkNotNull(str, "Unable to parse the null string.");
		
		if (type instanceof FloatType) {
			if (str.equalsIgnoreCase("+INFINITY")) {
				return Optional.of(ExpressionSafeFactory.INSTANCE.createFloatLiteral(Double.POSITIVE_INFINITY, EcoreUtil.copy(type))); 	
			} else if (str.equalsIgnoreCase("-INFINITY")) {
				return Optional.of(ExpressionSafeFactory.INSTANCE.createFloatLiteral(Double.NEGATIVE_INFINITY, EcoreUtil.copy(type))); 	
			} else if (str.equalsIgnoreCase("+NAN") || str.equalsIgnoreCase("-NAN")) {
				return Optional.of(ExpressionSafeFactory.INSTANCE.createFloatLiteral(Double.NaN, EcoreUtil.copy(type)));
			}
			
		}
		
		return Optional.empty();
	}
}
