/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.cbmc;

import java.io.IOException;
import java.math.RoundingMode;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.core.config.plugins.util.ResolverUtil.Test;

import com.google.common.base.Preconditions;
import com.google.common.math.IntMath;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.progress.ICanceling;
import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.exceptions.SettingsSerializerException;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.base.common.utils.OsUtils;
import cern.plcverif.base.common.utils.OsUtils.OperatingSystem;
import cern.plcverif.base.common.utils.PlaceholderReplacement;
import cern.plcverif.base.interfaces.data.result.JobStage;
import cern.plcverif.base.interfaces.data.result.StageStatus;
import cern.plcverif.base.models.cfa.cfabase.CfaNetworkBase;
import cern.plcverif.base.models.cfa.cfabase.ElseExpression;
import cern.plcverif.base.models.cfa.transformation.CfaTransformationGoal;
import cern.plcverif.base.models.cfa.transformation.ElseEliminator;
import cern.plcverif.library.backend.cbmc.CbmcSettings.CbmcModelVariant;
import cern.plcverif.library.backend.cbmc.exception.CbmcException;
import cern.plcverif.library.backend.cbmc.model.CbmcModelBuilder;
import cern.plcverif.library.backend.cbmc.parser.CbmcOutputParser;
import cern.plcverif.verif.interfaces.IBackend;
import cern.plcverif.verif.interfaces.data.RequirementRepresentationStrategy;
import cern.plcverif.verif.interfaces.data.ResultEnum;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.VerificationResult;
import cern.plcverif.verif.interfaces.data.VerificationStageTags;
import cern.plcverif.verif.utils.backend.BackendExecutor;
import cern.plcverif.verif.utils.backend.BackendExecutor.BackendExecutionException;
import cern.plcverif.verif.utils.backend.BackendExecutor.BackendExecutionStatus;
import cern.plcverif.verif.utils.backend.BackendExecutor.BackendExecutorResult;

/**
 * CBMC backend representation.
 *
 */
public class CbmcBackend implements IBackend {
	private CbmcModelVariant variant;
	private CbmcSettings settings;

	public CbmcBackend(Settings settings) {
		if (settings instanceof SettingsElement) {
			try {
				this.settings = SpecificSettingsSerializer.parse(settings.toSingle(), CbmcSettings.class);
				this.variant = this.settings.getModelVariant();
			} catch (SettingsParserException e) {
				throw new PlcverifPlatformException(
						"Unable to parse the settings of the CBMC backend. " + e.getMessage(), e);
			}
		} else {
			throw new PlcverifPlatformException(
					"Unable to parse the settings of the CBMC backend. The given settings node is not a single node.");
		}
	}

	@Override
	public SettingsElement retrieveSettings() {
		try {
			return SpecificSettingsSerializer.toGenericSettings(settings);
		} catch (SettingsParserException | SettingsSerializerException e) {
			throw new PlcverifPlatformException("Unable to serialize the settings of the NuSMV backend.", e);
		}
	}

	@Override
	public void execute(VerificationProblem input, VerificationResult result, ICanceling canceler) {
		Preconditions.checkNotNull(result, "result");
		Preconditions.checkNotNull(result.getJobMetadata(), "result.jobMetadata");
		Preconditions.checkNotNull(result.getJobMetadata().getId(), "result.jobMetadata.id");
		Preconditions.checkNotNull(input, "The given verification problem shall not be null.");
		Preconditions.checkNotNull(input.getModel(), "The model in the given verification problem shall not be null.");
		Preconditions.checkNotNull(input.getRequirement(),
				"The requirement in the given verification problem shall not be null.");
		Preconditions.checkNotNull(result, "The given verification result shall not be null.");

		// Precomputation
		CfaNetworkBase cfa = input.getModel();

		ElseEliminator.transform(cfa);
		Preconditions.checkState(EmfHelper.getAllContentsOfType(cfa, ElseExpression.class, false).isEmpty(),
				"There are some ElseExpressions in the CFA even after executing the ElseEliminator.");

		try {
			int unwindLimit = settings.getUnwind();
			int timeoutSec = settings.getTimeoutInSec();
			int remainingTime = timeoutSec;
			ICanceling cbmcCanceler = canceler;
			
			if (cbmcCanceler == null) {
				cbmcCanceler = ICanceling.NEVER_CANCELING_INSTANCE;
			}
			
			if (settings.isIncremental()) {
				unwindLimit = 1;
			}
			
			result.switchToStage("CBMC model building");
			CbmcModelBuilder modelBuilder = CbmcModelBuilder.createBuider(input, result, settings);
			CharSequence modelContent = modelBuilder.build();
			Path cFile = result.getOutputDirectoryOrCreateTemp()
					.resolve(result.getJobMetadata().getIdWithoutSpecialChars() + ".c");
			IoUtils.writeAllContent(cFile, modelContent);
			
			do {
				result.currentStage().logInfo("Current unrolling limit %s.", unwindLimit);
				result.switchToStage("CBMC execution: k="+unwindLimit, VerificationStageTags.BACKEND_EXECUTION);
				BackendExecutorResult execResult;
				execResult = execute(result, cFile, cbmcCanceler, unwindLimit, remainingTime);
				
				if (execResult.getStatus() == BackendExecutionStatus.Canceled) {
					result.currentStage().logInfo("CBMC output parsing has been skipped as the result of the CBMC execution was %s.", execResult.getStatus());
				} else {
					result.switchToStage("CBMC output parsing");
					new CbmcOutputParser(execResult.getStdout(), settings, result.currentStage()).parseAndFillResult(result, modelBuilder);
				}
				
				if (settings.isIncremental()) {
					unwindLimit += settings.getUnwindStep();
					remainingTime = timeoutSec - IntMath.divide((int)JobStage.sumLengthMs(result.getAllStages()), 1000, RoundingMode.CEILING) ;
					if (remainingTime <= 0) {
						result.currentStage().logWarning("CBMC did timeout at unrolling limit %s.", unwindLimit);
					}
				}
			} while (!cbmcCanceler.isCanceled() && result.getResult() != ResultEnum.Violated && settings.isIncremental() && settings.getUnwind() >= unwindLimit && remainingTime > 0) ;
			
		} catch (CbmcException e) {
			result.currentStage().setStatus(StageStatus.Unsuccessful);
			result.currentStage().logError("Error happened in CBMC backend that prevented the model checking. " + e.getMessage());
		} catch (IOException e) {
			throw new PlcverifPlatformException("Unable to write file for verification. ", e);
		}
	}

	private BackendExecutorResult execute(VerificationResult result, Path cFile, ICanceling canceler, int unwindLimit, int timeoutSec)
			throws CbmcException {
		// Locate the CBMC binary
		final Path executablePath = result.makeAbsoluteToProgramDirectory(this.settings.getCbmcBinaryPath());
		if (!executablePath.toFile().exists()) {
			throw new CbmcException(String.format("The CBMC binary is not found at '%s'.", executablePath.toString()));
		}

		// Assemble executable process
		int effectiveVerbosityLevel = Math.max(1, Math.min(settings.getCbmcVerbosity(), 10));

		List<String> argumentsTemplate;
		if (OsUtils.getCurrentOs() == OperatingSystem.Win32) {
			if (this.settings.getTimeoutExecutorPath() == null) {
				throw new CbmcException(
						"The 'timeout_executor_path' parameter is missing that is mandatory on Windows.");
			}
			argumentsTemplate = Arrays.asList("cmd.exe",
					("/c call '${vs_bat}' & '${timeout_executor_path}' ${timeout} "
							+ "'${cbmc_path}' '${model_path} --unwind ${unwind} --slice-formula --verbosity ${verbosity} --trace'") // --depth
																																	// 9999999
									.replace('\'', '"'));
		} else {
			argumentsTemplate = Arrays.asList("${cbmc_path}", "${model_path}", "--unwind", "${unwind}", "--verbosity",
					"${verbosity}", "--trace"); // "--depth",
																	// "9999999",
		}

		//@formatter:off
		List<String> procArguments = PlaceholderReplacement.list(argumentsTemplate)
			.replaceIfNotNull("vs_bat", toAbsolutePathStringIfNotNull(settings.getVisualStudioInitBatch()))
			.replaceIfNotNull("timeout_executor_path", toAbsolutePathStringIfNotNull(result.makeAbsoluteToProgramDirectory(this.settings.getTimeoutExecutorPath())))
			.replace("timeout", Integer.toString(timeoutSec))
			.replace("cbmc_path", toAbsolutePathStringIfNotNull(executablePath))
			.replace("model_path", toAbsolutePathStringIfNotNull(cFile))
			.replace("unwind", Integer.toString(unwindLimit))
			.replace("verbosity", Integer.toString(effectiveVerbosityLevel))
			.toList();
		//@formatter:on

		// '--partial-loops' is required as there may be some spurious loops
		// due to the "irregular" order of location representations.

		result.currentStage().logInfo("CBMC command line parameters: " + String.join(" ", procArguments));
		result.setBackendAlgorithmId(settings.getAlgorithmId());

		try {
			BackendExecutor executor = BackendExecutor.create(procArguments, timeoutSec).canceler(canceler)
					.killForcibly(true)
					// With SIGTERM it crashes the parent process on Linux, SIGKILL is fine.
					// NOTE This workaround should not be required from CBMC 5.11
					// See: https://github.com/diffblue/cbmc/pull/2918
					.logger(result.currentStage());
			if (!settings.isPrintExecOutputToConsole()) {
				// Treat all stdout as comment, don't print it to the console
				executor.stdoutCommentLinePattern(".*");
			}
			BackendExecutorResult execResult = executor.execute();

			result.setBackendStdout(execResult.getStdout());
			result.setBackendStderr(execResult.getStderr());
			
			// Write stdout to file if needed
			if (settings.isPrintExecOutputToFile()) {
				try {
					Path outFile = result.getOutputDirectoryOrCreateTemp()
							.resolve(result.getJobMetadata().getIdWithoutSpecialChars() + ".c.out");
					IoUtils.writeAllContent(outFile, execResult.getStdout());
					result.currentStage().logInfo("Console output (stdout) of CBMC has been written to '%s'.",
							outFile.toAbsolutePath().toString());
				} catch (IOException e) {
					result.currentStage().logInfo("Unable to write the console output (stdout) of CBMC to file. %s",
							e.getMessage());
				}
			}

			switch (execResult.getStatus()) {
			case Canceled: {
				result.currentStage().logWarning("The execution of CBMC was canceled.");
				result.setResult(ResultEnum.Unknown);
				return execResult;
			}
			case Timeout: {
				result.currentStage().logWarning("The execution of CBMC was interrupted due to timeout.");
				result.setResult(ResultEnum.Unknown);
				return execResult;
			}
			case Successful:
				return execResult;
			case Error: {
				result.setResult(ResultEnum.Error);
				throw new CbmcException(
						"The execution of the verification backend was not successful: " + execResult.getStatus());
			}
			default:
				throw new UnsupportedOperationException("Unknown execution result status: " + execResult.getStatus());
			}
		} catch (BackendExecutionException e) {
			throw new CbmcException(e.getMessage(), e);
		}
	}

	private static String toAbsolutePathStringIfNotNull(Path path) {
		if (path == null) {
			return null;
		} else {
			return path.toAbsolutePath().toString();
		}
	}

	@Override
	public CfaTransformationGoal getPreferredModelFormat() {
		if (this.variant == CbmcModelVariant.CFD || this.variant == CbmcModelVariant.CFD_STRUCTURED) {
			return CfaTransformationGoal.DECLARATION;
		} else if (this.variant == CbmcModelVariant.CFI) {
			return CfaTransformationGoal.INSTANCE_ENUMERATED;
		} else {
			throw new UnsupportedOperationException("Unknown CBMC backend variant: " + variant);
		}
	}

	@Override
	public RequirementRepresentationStrategy getPreferredRequirementFormat() {
		return RequirementRepresentationStrategy.IMPLICIT;
	}

	@Override
	public boolean isInliningRequired() {
		if (this.variant == CbmcModelVariant.CFD || this.variant == CbmcModelVariant.CFD_STRUCTURED) {
			return false;
		} else if (this.variant == CbmcModelVariant.CFI) {
			return true;
		} else {
			throw new UnsupportedOperationException("Unknown CBMC backend variant: " + variant);
		}
	}

	@Override
	public boolean areCfaAnnotationsRequired() {
		if (this.variant == CbmcModelVariant.CFD_STRUCTURED) {
			return true;
		} else if (this.variant == CbmcModelVariant.CFI || this.variant == CbmcModelVariant.CFD) {
			return false;
		} else {
			throw new UnsupportedOperationException("Unknown CBMC backend variant: " + variant);
		}
	}
}
