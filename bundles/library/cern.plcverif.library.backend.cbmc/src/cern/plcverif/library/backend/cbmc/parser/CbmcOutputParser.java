/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.cbmc.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.logging.IPlcverifLogger;
import cern.plcverif.base.interfaces.data.result.JobStage;
import cern.plcverif.base.interfaces.data.result.StageStatus;
import cern.plcverif.library.backend.cbmc.CbmcSettings;
import cern.plcverif.library.backend.cbmc.model.CbmcModelBuilder;
import cern.plcverif.library.backend.cbmc.model.CbmcModelBuilderCfd;
import cern.plcverif.library.backend.cbmc.model.ITextualCexParser;
import cern.plcverif.verif.interfaces.data.ResultEnum;
import cern.plcverif.verif.interfaces.data.VerificationResult;
import cern.plcverif.verif.interfaces.data.cex.DeclarationCounterexample;
import cern.plcverif.verif.interfaces.data.cex.ICounterexample;
import cern.plcverif.verif.interfaces.data.cex.InstanceCounterexample;

/**
 * CBMC stdout output parser.
 */
public class CbmcOutputParser {
	private String stdout;
	private List<String> linesToParse;
	private CbmcSettings settings;
	private IPlcverifLogger logger;

	/**
	 * Creates a new CBMC backend output parser.
	 * 
	 * @param cbmcStdout
	 *            The stdout output of CBMC. Shall not be {@code null}.
	 * @param settings
	 *            Settings used for the CBMC plug-in. Shall not be {@code null}.
	 * @param logger
	 *            Logger to be used. Shall not be {@code null}.
	 */
	public CbmcOutputParser(CharSequence cbmcStdout, CbmcSettings settings, IPlcverifLogger logger) {
		this.stdout = Preconditions.checkNotNull(cbmcStdout, "null output is not valid to parse").toString();
		this.linesToParse = Arrays.asList(stdout.split(System.lineSeparator()));
		this.settings = Preconditions.checkNotNull(settings);
		this.logger = Preconditions.checkNotNull(logger);
	}

	// public only for testing
	public List<Map<String, String>> parseCounterexampleToMaps() {
		int step = 0; // 2 steps per cycle - for BoC and EoC
		boolean nextLineIsAssignment = false;
		String currentContext = "?";

		Pattern pattern = Pattern.compile("\\s*([^=]+)=(\\S+)\\s+\\(.*\\)");

		List<Map<String, String>> variableValueMaps = new ArrayList<>();
		variableValueMaps.add(new HashMap<>());

		for (String line : linesToParse) {
			// Check if it starts a new cycle
			if (isEndOfCycle(line) || isBeginningOfCycle(line)) {
				step++;
				// carry on all previous values for the next step (they are not
				// repeated unless changed)
				variableValueMaps.add(new HashMap<>(variableValueMaps.get(step - 1)));
			}
			
			// Check if it is a variable assignment line
			if (nextLineIsAssignment) {
				Matcher matcher = pattern.matcher(line);
				if (matcher.find()) {
					String variable = matcher.group(1);
					String value = matcher.group(2);

					if (variable.equals(CbmcModelBuilderCfd.CONTEXT_VARIABLE)) {
						// context update
						currentContext = value.replace("&", "");
					} else {
						variable = variable.replace(CbmcModelBuilderCfd.CONTEXT_VARIABLE + "\\.", currentContext + ".");
						
						if (variable.matches(".+\\[\\d+ll?\\]")) {
							
							// [5ll] --> [5]
							variable = variable.replaceAll("\\[(\\d+)ll?\\]", "[$1]");
						}
						
						parseAssignmentLine(variable, value, variableValueMaps.get(step));
					}
				} else {
					logger.logDebug("Unable to parse the following line: '%s'.", line);
				}
				nextLineIsAssignment = false;
			}
			
			// Check if the next line will be an assignment line
			if (line.contains("-----------------")) {
				nextLineIsAssignment = true;
				continue;
			}
		}

		return variableValueMaps;
	}

	private static boolean isEndOfCycle(String line) {
		return line.contains(CbmcModelBuilder.EOC_MARKER + "=TRUE");
	}

	private static boolean isBeginningOfCycle(String line) {
		return line.contains(CbmcModelBuilder.BOC_MARKER + "=TRUE");
	}

	private static void parseAssignmentLine(String variable, String value, Map<String, String> currentVariableValueMap) {
		if (!variable.equals(CbmcModelBuilder.EOC_MARKER) && !variable.equals(CbmcModelBuilder.BOC_MARKER)) {
			currentVariableValueMap.put(variable, value);
		}
	}

	/**
	 * Parses the stdout output of the CBMC execution and fills the given
	 * {@link VerificationResult} object.
	 * <ul>
	 * <li>It fills the result of the verification
	 * ({@link VerificationResult#setResult(ResultEnum)}),
	 * <li>If the result is {@link ResultEnum#Unknown}, it tries to diagnose the
	 * problem and fills adds log messages if possible,
	 * <li>If the result is {@link ResultEnum#Violated}, it tries to parse the
	 * counterexample.
	 * </ul>
	 * 
	 * @param result
	 *            The verification result object to be filled.
	 * @param textCexParser
	 *            The object to be used to resolve the textual representation of
	 *            variables/fields and literals.
	 */
	public void parseAndFillResult(VerificationResult result, ITextualCexParser textCexParser) {
		// Fetch result from stdout
		result.setResult(parseResult());

		if (result.getResult() == ResultEnum.Unknown) {
			diagnoseProblem(result);

			if (settings.getCbmcVerbosity() < 4) {
				result.currentStage().logWarning("The verbosity parameter for the CBMC backend is very low (%s). This may make parsing the result impossible.", settings.getCbmcVerbosity());
			}
		}
		
		if (result.getResult() == ResultEnum.Satisfied) {
			result.currentStage().logWarning("CBMC reported that the given requirement is satisfied. However, take into account that CBMC performs bounded model checking with a limited bound, thus it may not find violations which require many execution cycles to reach.");
			if (settings.getUnwind() < 3) {
				result.currentStage().logWarning("The unwind parameter for the CBMC backend is very low (%s). This may affect the result.", settings.getUnwind());
			}
		}

		if (result.getResult() == ResultEnum.Violated) {
			// parse counterexample
			List<Map<String, String>> valueMaps = parseCounterexampleToMaps();

			ICounterexample cex = textCexParser.parseTextualCex(valueMaps);
			if (cex instanceof InstanceCounterexample) {
				result.setInstanceCounterexample((InstanceCounterexample) cex);
			} else if (cex instanceof DeclarationCounterexample) {
				result.setDeclarationCounterexample((DeclarationCounterexample) cex);
			} else {
				throw new UnsupportedOperationException("Unknown counterexample type: " + cex);
			}

			result.currentStage().logWarning("Make sure to check whether the counterexample is valid. In rare cases, due to the partial loop unfolding in CBMC, the reported counterexample may not be realizable in the original program.");
		}
		
	}

	/**
	 * Determines (based on the given stdout output of the CBMC backend) whether
	 * the verification was successful or not, and if the requirement was
	 * violated or not.
	 * 
	 * @return The {@link ResultEnum} representing the result.
	 */
	public ResultEnum parseResult() {
		if (stdout.contains("VERIFICATION FAILED") || stdout.contains("Violated property:")) {
			return ResultEnum.Violated;
		} else if (stdout.contains("VERIFICATION SUCCESSFUL")) {
			return ResultEnum.Satisfied;
		} else {
			return ResultEnum.Unknown;
		}
	}

	/**
	 * Attempts to log the reason of {@link ResultEnum#Unknown} result.
	 */
	private void diagnoseProblem(VerificationResult result) {
		JobStage stage = Preconditions.checkNotNull(result.currentStage());
		Optional<CharSequence> backendStderr = result.getBackendStderr();
		String combinedOutput = stdout + backendStderr.orElse("");
		if (backendStderr.isPresent() && backendStderr.get().toString().contains("Timeout.")) {
			// Timeout, nothing to do, it was already logged.
		} else if (combinedOutput.contains("'CL' is not recognized as an internal or external command") || combinedOutput.contains("CL Preprocessing failed")) {
			stage.logError(
					"Unable to execute the CBMC backend: 'CL' is not found. Check if the C compiler is installed and set up correctly. On Windows, you have to install Visual Studio with the C++ command line tools and set up the path of the vcvars64.bat file.");
			stage.setStatus(StageStatus.Unsuccessful);
		} else if (Pattern.compile("Cannot open include file: '.*\\.h': No such file or directory", Pattern.DOTALL)
				.matcher(combinedOutput).find()) {
			stage.logError(
					"Unable to execute the CBMC backend: some include files are not found. Check if the C compiler is installed and set up correctly.");
			stage.setStatus(StageStatus.Unsuccessful);
		} else if (Pattern.compile("fatal error C1085: Cannot write.*No space left on device", Pattern.DOTALL)
				.matcher(combinedOutput).find()) {
			stage.logError(
					"Unable to execute the CBMC backend: file writing failed due to lack of free disk space.");
			stage.setStatus(StageStatus.Unsuccessful);
		} else {
			stage.logWarning("Unknown error occurred during the execution of CBMC.");
		}
	}
}
