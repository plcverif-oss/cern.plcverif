/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.cbmc;

import static cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory.OPTIONAL;

import java.nio.file.Path;

import cern.plcverif.base.common.settings.specific.AbstractSpecificSettings;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsElement;

/**
 * CBMC backend settings
 */
public class CbmcSettings extends AbstractSpecificSettings {	
	public enum CbmcModelVariant {
		CFI, CFD, CFD_STRUCTURED
	}
	
	public static final String TIMEOUT = "timeout";
	@PlcverifSettingsElement(name = TIMEOUT, description = "Timeout of the verification backend, in seconds.", mandatory = OPTIONAL)
	private int timeoutInSec = 60;
	
	public static final String BINARY_PATH = "binary_path";
	@PlcverifSettingsElement(name = BINARY_PATH, description = "Full path of the CBMC binary.")
	private Path cbmcBinaryPath = null;
	
	public static final String TIMEOUT_EXECUTOR_PATH = "timeout_executor_path";
	@PlcverifSettingsElement(name = TIMEOUT_EXECUTOR_PATH, description = "Full path of the TimeoutExecutor binary.", mandatory = OPTIONAL)
	private Path timeoutExecutorPath = null;
	
	public static final String VS_INIT_BATCH = "vs_init_batch";
	@PlcverifSettingsElement(name = VS_INIT_BATCH, description = "Path to the vcvars64.bat of Visual Studio (Windows only, not taken into account on other platforms).", mandatory = OPTIONAL)
	private Path visualStudioInitBatch = null;
	
	public static final String EXEC_OUTPUT_TO_CONSOLE = "exec_output_to_console";
	@PlcverifSettingsElement(name = EXEC_OUTPUT_TO_CONSOLE, description = "If true, the output of the backend execution's output will be shown on the console.", mandatory = OPTIONAL)
	private boolean printExecOutputToConsole = false;
	
	public static final String PRINT_STDOUT_TO_FILE = "exec_output_to_file";
	@PlcverifSettingsElement(name = PRINT_STDOUT_TO_FILE, description = "If true, the output of the backend execution's output will be written into a file, next to the model.", mandatory = OPTIONAL)
	private boolean printExecOutputToFile = false;
	
	public static final String INCREMENTAL_BMC = "incremental_bmc";
	@PlcverifSettingsElement(name = INCREMENTAL_BMC, description = "If true, bounded model checking is performed incrementally up to the unwind limit.", mandatory = OPTIONAL)
	private boolean incremental = false;
	
	public static final String UNWIND_STEP = "unwind_step";
	@PlcverifSettingsElement(name = UNWIND_STEP, description = "Sets the increment step size of incremental bmc.", mandatory = OPTIONAL)
	private int unwindStep = 1;
	
	public static final String UNWIND = "unwind";
	@PlcverifSettingsElement(name = UNWIND, description = "Loop unwind", mandatory = OPTIONAL)
	private int unwind = 10;
	
	public static final String VERBOSITY = "verbosity";
	@PlcverifSettingsElement(name = VERBOSITY, description = "CBMC output verbosity (1..10). With verbosity levels 1..3 the result of verification is not printed, thus it cannot be parsed by PLCverif.", mandatory = OPTIONAL)
	private int cbmcVerbosity = 7;
	
	public static final String MODEL_VARIANT = "model_variant";
	@PlcverifSettingsElement(name = MODEL_VARIANT, description = "Model variant (CFA declaration or instance) to be used for the the generation of the C representation.", mandatory = OPTIONAL)
	private CbmcModelVariant modelVariant = CbmcModelVariant.CFD;
	
	public CbmcSettings() {}
	
	public CbmcSettings(CbmcModelVariant modelVariant) {
		this.modelVariant = modelVariant;
	}
	
	public int getTimeoutInSec() {
		return timeoutInSec;
	}
	
	public Path getCbmcBinaryPath() {
		return cbmcBinaryPath;
	}
	
	public Path getTimeoutExecutorPath() {
		return timeoutExecutorPath;
	}
	
	public Path getVisualStudioInitBatch() {
		return visualStudioInitBatch;
	}
	
	public boolean isPrintExecOutputToConsole() {
		return printExecOutputToConsole;
	}
	
	public boolean isPrintExecOutputToFile() {
		return printExecOutputToFile;
	}
	
	public int getUnwind() {
		return unwind;
	}
	
	public int getUnwindStep() {
		return unwindStep;
	}
	
	public CbmcModelVariant getModelVariant() {
		return modelVariant;
	}
	
	public void setModelVariant(CbmcModelVariant modelVariant) {
		this.modelVariant = modelVariant;
	}
	
	public int getCbmcVerbosity() {
		return cbmcVerbosity;
	}
	
	public boolean isIncremental() {
		return incremental;
	}

	public String getAlgorithmId() {
		return String.format("%s-UW%s", modelVariant, Integer.toString(unwind));
	}
}
