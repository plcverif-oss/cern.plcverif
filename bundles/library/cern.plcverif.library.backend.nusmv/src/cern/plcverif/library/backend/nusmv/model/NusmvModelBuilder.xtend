/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Vince Molnar - fixes
 *****************************************************************************/
package cern.plcverif.library.backend.nusmv.model

import cern.plcverif.base.common.utils.IoUtils
import cern.plcverif.base.models.cfa.cfabase.Location
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariable
import cern.plcverif.base.models.cfa.cfainstance.ArrayVariable
import cern.plcverif.base.models.cfa.cfainstance.AssignmentTransition
import cern.plcverif.base.models.cfa.cfainstance.AutomatonInstance
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance
import cern.plcverif.base.models.cfa.cfainstance.Variable
import cern.plcverif.base.models.cfa.cfainstance.VariableAssignment
import cern.plcverif.base.models.expr.BoolType
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.Nondeterministic
import cern.plcverif.library.backend.nusmv.NusmvContext
import cern.plcverif.library.backend.nusmv.exception.NusmvModelGenerationException
import cern.plcverif.verif.interfaces.data.VerificationProblem
import cern.plcverif.verif.interfaces.data.VerificationResult
import cern.plcverif.verif.utils.backend.BackendNameTrace
import com.google.common.base.Preconditions
import java.io.IOException
import java.util.Map

import static extension cern.plcverif.base.models.cfa.utils.CfaInstanceUtils.refersTo
import static extension cern.plcverif.base.models.expr.utils.TlExprUtils.*
import static extension cern.plcverif.library.backend.nusmv.model.NusmvExpression.toNusmvType
import static extension cern.plcverif.library.backend.nusmv.model.NusmvExpression.toNusmvOp
import cern.plcverif.library.backend.nusmv.NusmvContext.NusmvExpressionType
import cern.plcverif.library.backend.nusmv.NusmvSettings.NusmvAlgorithm
import cern.plcverif.base.models.expr.UnaryCtlExpression
import cern.plcverif.base.models.expr.UnaryCtlOperator
import cern.plcverif.base.models.expr.ExpressionSafeFactory
import cern.plcverif.base.models.expr.UnaryLtlExpression
import cern.plcverif.base.models.expr.UnaryLtlOperator
import cern.plcverif.base.models.expr.UnaryExpression
import cern.plcverif.base.models.expr.ComparisonExpression
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariableRef

class NusmvModelBuilder {
	VerificationProblem verifProblem;
	VerificationResult result;

	public static final String LOC_VAR = "loc";
	public static final String RANDOM_VAR_PREFIX = "__random";
	
	NusmvExpression exprTranslator = new NusmvExpression();
	BackendNameTrace<Location> locNameTrace = new BackendNameTrace([NusmvUtils.makeValidName(it)]);
	
	Map<VariableAssignment, String> nonBoolNondetAmtsToNusmvVar = null;
	NusmvContext context;

	new(VerificationProblem verifProblem, VerificationResult result, NusmvContext context) {
		this.verifProblem = verifProblem;
		this.result = result;
		this.context = context;
	}
	
	def CharSequence represent() {
		Preconditions.checkState(verifProblem.model instanceof CfaNetworkInstance);
		Preconditions.checkState((verifProblem.model as CfaNetworkInstance).automata.size() == 1);

		// quickfix to support IC3 for assertion checks
		if (context.settings.algorithm == NusmvAlgorithm.Ic3 && verifProblem.requirement instanceof UnaryCtlExpression) {
			val req = verifProblem.requirement as UnaryCtlExpression;
			if (req.operator == UnaryCtlOperator.AG) {
				val content = req.operand;
				req.operand = null;
				val ltlReq = ExpressionSafeFactory.INSTANCE.G(content);
				verifProblem.setRequirement(ltlReq, verifProblem.requirementRepresentation);
			}
			// if the requirement became mixed CTL-LTL, later it will be handle			
		}
		
		exprTranslator.setBeginningOfCycleExpression('''«LOC_VAR» = «verifProblem.bocLocation.toNusmvLocationName»''');
		exprTranslator.setEndOfCycleExpression('''«LOC_VAR» = «verifProblem.eocLocation.toNusmvLocationName»''');
		represent(verifProblem.model as CfaNetworkInstance, verifProblem.requirement);
		
	}
	
	static def buildModel(NusmvContext context) throws NusmvModelGenerationException {
		val builder = new NusmvModelBuilder(context.verifProblem, context.verifResult, context);
		val modelContent = builder.represent();
		
		try {
			IoUtils.writeAllContent(context.modelFile, modelContent);
			context.setVarTrace(builder.exprTranslator.varTrace);
			context.logInfo('''NuSMV model has been written to '«context.modelFile.absolutePath»'.''');
		} catch (IOException e) {
			throw new NusmvModelGenerationException(
			String.format("Error occured while writing NuSMV model into file '%s'.", 
				context.modelFile.absolutePath), e);
		}
	}
	
	def represent(CfaNetworkInstance cfa, Expression requirement) '''
		-- Model«exploreNeededRandomVars(cfa)»
		«represent(cfa)»
		
		-- Requirement
		«representRequirement(requirement)»;
	'''

	private def represent(CfaNetworkInstance cfa) '''
		MODULE main
			VAR
				«LOC_VAR» : {«FOR l : cfa.mainAutomaton.locations SEPARATOR ', '»«l.toNusmvLocationName»«ENDFOR»};
				«FOR v : cfa.variables»
					«representVariable(v)»;«IF v.isFrozen» -- frozen«ENDIF»
				«ENDFOR»
				«IF nonBoolNondetAmtsToNusmvVar !== null && !nonBoolNondetAmtsToNusmvVar.isEmpty»
					-- Random vars for nondeterministic INTs
					«FOR it : nonBoolNondetAmtsToNusmvVar.entrySet.sortBy[it.value]»
						«it.value» : «NusmvExpression.toNusmvType(it.key.leftValue.type)»;
					«ENDFOR»
				«ENDIF»
			
			ASSIGN
				-- CFA structure («LOC_VAR»)
				init(«LOC_VAR») := «cfa.mainAutomaton.initialLocation.toNusmvLocationName»;
				next(«LOC_VAR») := case
					«FOR transition : cfa.mainAutomaton.transitions»
						«LOC_VAR» = «transition.source.toNusmvLocationName» & («transition.condition.toNusmvExpr») : «transition.target.toNusmvLocationName»;
					«ENDFOR»
					TRUE: «LOC_VAR»;
				esac;
				
				«FOR v : cfa.variables»
					«representAssignmentsOf(v as Variable, cfa.mainAutomaton)»
				«ENDFOR»
	'''
	
	private def representRequirement(Expression expr) {
		//TODO: handle the case when the expression is pure logic or atomic (and should be checked on the initial state) (?)
		if (context !== null && context.settings.isReqAsInvar && canBeRepresentedAsInvar(expr)) {
			// Represent safety TL requirement as invariant
			context.exprType = NusmvExpressionType.Invar;
			return '''INVARSPEC «exprTranslator.toNusmvExpression(tlexprAsInvar(expr))»''';
		} else if (expr.containsOnlyLtl) {
			if (context !== null) {
				context.exprType = NusmvExpressionType.Ltl;
			}
			return '''LTLSPEC «exprTranslator.toNusmvExpression(expr)»''';
		} else if (expr.containsOnlyCtl) {
			if (context !== null) {
				context.exprType = NusmvExpressionType.Ctl;
			}
			return '''CTLSPEC «exprTranslator.toNusmvExpression(expr)»''';
		} else {
			throw new UnsupportedOperationException("Only (non-mixed) temporal logic expressions are supported as requirements.");
		}
	}
	
	/**
	 * Returns true iff the given expression can be represented as an invariant.
	 * <p>
	 * It is true if the expression is in form {@code AG} &alpha; or {@code G} &alpha;,
	 * where &alpha; does not contain any temporal logic operator.
	 */
	private def canBeRepresentedAsInvar(Expression expression) {
		if (expression instanceof UnaryCtlExpression) {
			if (expression.operator == UnaryCtlOperator.AG && expression.operand.containsNoTl) {
				return true;
			}
		}
		
		if (expression instanceof UnaryLtlExpression) {
			if (expression.operator == UnaryLtlOperator.G && expression.operand.containsNoTl) {
				return true;
			}
		}
		
		if (expression instanceof UnaryLtlExpression) {
			if (expression.operator == UnaryLtlOperator.H && expression.operand.containsNoTl) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Returns &alpha; if the expression is in form {@code AG} &alpha; or {@code G} &alpha;,
	 * where &alpha; does not contain any temporal logic operator.
	 * @throws IllegalArgumentException if the given expression cannot be represented as invariant.
	 */
	private def tlexprAsInvar(Expression expression) {
		Preconditions.checkArgument(expression.canBeRepresentedAsInvar, "The given expression cannot be represented as invariant.");
		Preconditions.checkArgument(expression instanceof UnaryExpression, "The given expression cannot be represented as invariant.");
		return (expression as UnaryExpression).operand;
	}
	
	// Partial representations
	private def representAssignmentsOf(Variable variable, AutomatonInstance automaton) '''
		init(«variable.toNusmvVarName») := «exprTranslator.toNusmvExpression(variable.initialValue)»;
		next(«variable.toNusmvVarName») := case
			«FOR t : automaton.transitions.filter(AssignmentTransition)»
				«FOR amt : t.assignments.filter[it | it.leftValue.refersTo(variable)]»
					«IF !variable.isBoolean && amt.rightValue instanceof Nondeterministic»
					«LOC_VAR» = «t.source.toNusmvLocationName» & («t.condition.toNusmvExpr»)«FOR amb : t.assumeBounds.filter[it | ((it as ComparisonExpression).leftOperand as AbstractVariableRef).refersTo(variable)]» & («getNonIntNondetRandomVar(automaton.container, amt)» «(amb as ComparisonExpression).operator.toNusmvOp» «(amb as ComparisonExpression).rightOperand.toNusmvExpr»)«ENDFOR» : «getNonIntNondetRandomVar(automaton.container, amt)»; -- Nondeterministic
					«FOR amb : t.assumeBounds.filter[it | ((it as ComparisonExpression).leftOperand as AbstractVariableRef).refersTo(variable)]»
						«LOC_VAR» = «t.source.toNusmvLocationName» & («t.condition.toNusmvExpr») & !(«getNonIntNondetRandomVar(automaton.container, amt)» «(amb as ComparisonExpression).operator.toNusmvOp» «(amb as ComparisonExpression).rightOperand.toNusmvExpr») : «(amb as ComparisonExpression).rightOperand.toNusmvExpr»;
					«ENDFOR»
					«ELSE»
						«LOC_VAR» = «t.source.toNusmvLocationName» & («t.condition.toNusmvExpr») : «amt.rightValue.toNusmvExpr»;
					«ENDIF»
				«ENDFOR»
			«ENDFOR» 
			TRUE  : «variable.toNusmvVarName»;
		esac;
	'''
	
	private def CharSequence representVariable(AbstractVariable variable) {
		switch (variable) {
			ArrayVariable: throw new UnsupportedOperationException(
				"Array variables are not supported, unless they are unfolded/enumerated.")
			Variable: return '''«variable.toNusmvVarName» : «variable.type.toNusmvType»'''
		}
		throw new UnsupportedOperationException("Unknown variable type: " + variable);
	}
	
	// Special cases
	private def exploreNeededRandomVars(CfaNetworkInstance cfa) {
		nonBoolNondetAmtsToNusmvVar = newHashMap();
		for (automaton : cfa.automata) {
			for (t : automaton.transitions.filter(AssignmentTransition)) {
				for (amt : t.assignments) {
					if (isNonBoolNondetAmt(amt)) {
						val nusmvVarName = RANDOM_VAR_PREFIX + "_" + exprTranslator.varTrace.toName(amt.leftValue.variable as Variable);
						exprTranslator.varTrace.addOtherKnownVariable(nusmvVarName);
						nonBoolNondetAmtsToNusmvVar.put(amt, nusmvVarName);										
					}
				}
			}
		}
	}
	
	private def isNonBoolNondetAmt(VariableAssignment amt) {
		return 
			// Right value is nondeterministic
			amt.rightValue instanceof Nondeterministic && 
			// Left value is a variable (we assume there are no arrays!)
			amt.leftValue.variable instanceof Variable && 
			// Left value is not Boolean (we can handle that using {FALSE,TRUE})
			!(amt.leftValue.type instanceof BoolType);
	} 
	
	private def getNonIntNondetRandomVar(CfaNetworkInstance cfa, VariableAssignment varAmt) {
		if (nonBoolNondetAmtsToNusmvVar === null) {
			exploreNeededRandomVars(cfa);
		}
		
		return nonBoolNondetAmtsToNusmvVar.getOrDefault(varAmt, null);
	}
	
	// Shorthands
	private def CharSequence toNusmvExpr(Expression expr) {
		return exprTranslator.toNusmvExpression(expr);
	}

	private def CharSequence toNusmvLocationName(Location location) {
		return locNameTrace.toName(location);
	}

	private def CharSequence toNusmvVarName(Variable variable) {
		return exprTranslator.varTrace.toName(variable);
	}
	
	private def isBoolean(Variable v) {
		return (v.type instanceof BoolType);
	}
}
