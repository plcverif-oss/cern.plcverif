/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.nusmv.parser.counterexample;

import java.util.ArrayList;
import java.util.List;

public class NusmvCexStep {
	private final String name;
	private final List<VariableValuePair> values = new ArrayList<>();
	private boolean startOfLoop = false;

	public NusmvCexStep(final String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void addValue(final VariableValuePair value){
		this.values.add(value);
	}

	public List<VariableValuePair> getValues() {
		return this.values;
	}

	private VariableValuePair findVariableValuePair(final String variableFqn) {
		for (final VariableValuePair vvp : this.values) {
			if (vvp.getVariableName().equals(variableFqn)) {
				return vvp;
			}
		}

		return null;
	}

	public String getVariableValue(final String variableFqn) {
		final VariableValuePair vvp = findVariableValuePair(variableFqn);
		if (vvp != null) {
			return vvp.getValue();
		}

		return null;
	}

	public boolean isStartOfLoop() {
		return this.startOfLoop;
	}

	public void setStartOfLoop(final boolean startOfLoop) {
		this.startOfLoop = startOfLoop;
	}
}
