/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Xaver Fink - BMC addition
 *******************************************************************************/
package cern.plcverif.library.backend.nusmv.executor;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Preconditions;

import cern.plcverif.library.backend.nusmv.NusmvContext;
import cern.plcverif.library.backend.nusmv.NusmvContext.NusmvExpressionType;
import cern.plcverif.library.backend.nusmv.NusmvSettings;
import cern.plcverif.library.backend.nusmv.exception.NusmvExecutionException;

public abstract class NusmvExecutionDetails implements IExecutionDetails {
	/**
	 * Settings to be used. Never {@code null}.
	 */
	private final NusmvSettings settings;

	/**
	 * Context to be used. Never {@code null}.
	 */
	private final NusmvContext context;

	protected NusmvExecutionDetails(NusmvContext context) {
		Preconditions.checkNotNull(context, "context");
		Preconditions.checkNotNull(context.getSettings(), "context.settings");

		this.context = context;
		this.settings = context.getSettings();
	}

	public static NusmvExecutionDetails create(NusmvContext context) throws NusmvExecutionException {
		Preconditions.checkNotNull(context, "context");
		Preconditions.checkNotNull(context.getSettings(), "context.settings");

		switch (context.getSettings().getAlgorithm()) {
		case Classic:
			return new ClassicNusmvExecutionDetails(context);
		case Ic3:
			return new Ic3NusmvExecutionDetails(context);
		case Bmc:
			return new BmcNusmvExecutionDetails(context);
		default:
			throw new NusmvExecutionException("Unknown algorithm: " + context.getSettings().getAlgorithm());
		}
	}

	/**
	 * Returns the settings to be used. Never {@code null}.
	 */
	protected NusmvSettings getSettings() {
		return settings;
	}

	/**
	 * Returns the context to be used. Never {@code null}.
	 */
	protected NusmvContext getContext() {
		return context;
	}

	@Override
	public Path getBinaryLocation() {
		return settings.getNuxmvBinaryPath();
	}

	/**
	 * Returns the command line options needed to call the verification backend.
	 * Structure: <verification option switches> -source {@code scriptPath}
	 * {@code modelPath}
	 */
	@Override
	public List<String> getCommandLineOptions(final String scriptPath, final String modelPath) {
		List<String> options = new ArrayList<>();

		// switches
		if (settings.isDynamic()) {
			options.add("-dynamic");
		}
		if (settings.isDf()) {
			options.add("-df");
		}

		// script location
		options.add("-source");
		options.add(scriptPath);

		// model location
		options.add(modelPath);

		return options;
	}

	/**
	 * Returns an identified of the used backend configuration, e.g.
	 * {@code nusmv-Classic-dynamic-df} or {@code nuxmv-Ic3}.
	 */
	@Override
	public String getBackendConfigurationId() {
		StringBuilder ret = new StringBuilder();

		// Determine whether NuSMV or nuXmv is used
		String lastSegment = settings.getNuxmvBinaryPath().getFileName().toString();
		boolean isNusmv = lastSegment.toLowerCase().contains("nusmv");
		boolean isNuxmv = lastSegment.toLowerCase().contains("nuxmv");
		if (isNusmv && !isNuxmv) {
			ret.append("nusmv");
		} else if (!isNusmv && isNuxmv) {
			ret.append("nuxmv");
		} else {
			ret.append("unknown");
		}

		ret.append("-");
		ret.append(settings.getAlgorithm());

		if (settings.isDynamic()) {
			ret.append("-dynamic");
		}
		if (settings.isDf()) {
			ret.append("-df");
		}
		if (context.getExprType() == NusmvExpressionType.Invar) {
			ret.append("-invar");
		}

		return ret.toString();
	}
}
