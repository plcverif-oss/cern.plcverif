/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.library.backend.nusmv.executor

import cern.plcverif.library.backend.nusmv.NusmvContext
import cern.plcverif.library.backend.nusmv.exception.NusmvExecutionException
import com.google.common.base.Preconditions
import cern.plcverif.library.backend.nusmv.NusmvSettings.NusmvAlgorithm
import java.io.File

class ClassicNusmvExecutionDetails extends NusmvExecutionDetails {
	protected new(NusmvContext context) {
		super(context);
	}

	override String getExecScriptContent(File cexFile) throws NusmvExecutionException {
		Preconditions.checkState(this.getSettings().getAlgorithm() == NusmvAlgorithm.Classic);

		val script = '''
			set on_failure_script_quits
			set traces_hiding_prefix ___
			set default_trace_plugin 1
			go
			«determineCheckCommand» -o "«cexFile.toPath().toAbsolutePath()»"
			quit
		'''
		return script;
	}

	private def String determineCheckCommand() throws NusmvExecutionException {
		switch (getContext().getExprType()) {
			case Ctl: return "check_ctlspec"
			case Ltl: return "check_ltlspec"
			case Invar: return "check_invar"
			default: throw new NusmvExecutionException("Unknown requirement type: " + getContext().getExprType())
		}
	}
}
