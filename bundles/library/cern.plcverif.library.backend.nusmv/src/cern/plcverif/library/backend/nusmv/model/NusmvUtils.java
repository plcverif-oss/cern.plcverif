/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.nusmv.model;

import java.util.Arrays;
import java.util.Collection;

import com.google.common.base.Preconditions;

public final class NusmvUtils {
	private NusmvUtils() {}
	
	private static final Collection<String> KEYWORDS = Arrays.asList("MODULE", "DEFINE", "MDEFINE", "CONSTANTS", "VAR", "IVAR", "FROZENVAR", "INIT", "TRANS", 
		"INVAR", "SPEC", "CTLSPEC", "LTLSPEC", "PSLSPEC", "COMPUTE", "NAME", "INVARSPEC", "FAIRNESS", "JUSTICE", "COMPASSION", 
		"ISA", "ASSIGN", "CONSTRAINT", "SIMPWFF", "CTLWFF", "LTLWFF", "PSLWFF", "COMPWFF", "IN", "MIN", "MAX", "MIRROR", "PRED", 
		"PREDICATES", "process", "array", "of", "boolean", "integer", "real", "word", "word1", "bool", "signed", "unsigned", 
		"extend", "resize", "sizeof", "uwconst", "swconst", "EX", "AX", "EF", "AF", "EG", "AG", "E", "F", "O", "G", "H", "X", 
		"Y", "Z", "A", "U", "S", "V", "T", "BU", "EBF", "ABF", "EBG", "ABG", "case", "esac", "mod", "next", "init", "union", 
		"in", "xor", "xnor", "self", "TRUE", "FALSE", "count");
	
	private static final String _VALID_CHARS = "a-zA-Z0-9_#";
	private static final String INVALID_CHARS_REGEX = String.format("[^%s]", _VALID_CHARS);
	private static final String VALID_NAME_REGEX = String.format("[%s]+", _VALID_CHARS);
	
	public static boolean isValidName(String name) {
		if (KEYWORDS.contains(name)) {
			return false;
		}
		
		return name.matches(VALID_NAME_REGEX);
	}
	
	public static String makeValidName(String name) {
		String ret = name.replaceAll("\\.", "#"); 
		ret = ret.replaceAll(INVALID_CHARS_REGEX, "_");
		if (KEYWORDS.contains(ret)) {
			ret = ret + "_pv";
		}
		Preconditions.checkState(ret.matches(VALID_NAME_REGEX));
		return ret;
	}
}
