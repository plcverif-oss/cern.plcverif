/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.nusmv.parser;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import com.google.common.base.Preconditions;
import com.google.common.io.Files;

import cern.plcverif.base.models.cfa.cfainstance.VariableRef;
import cern.plcverif.base.models.expr.Literal;
import cern.plcverif.library.backend.nusmv.NusmvContext;
import cern.plcverif.library.backend.nusmv.exception.NusmvException;
import cern.plcverif.library.backend.nusmv.exception.NusmvParsingException;
import cern.plcverif.library.backend.nusmv.model.NusmvExpression;
import cern.plcverif.library.backend.nusmv.model.NusmvModelBuilder;
import cern.plcverif.library.backend.nusmv.parser.counterexample.NusmvCexStep;
import cern.plcverif.library.backend.nusmv.parser.counterexample.NusmvCounterexample;
import cern.plcverif.library.backend.nusmv.parser.counterexample.VariableValuePair;
import cern.plcverif.verif.interfaces.data.ResultEnum;
import cern.plcverif.verif.interfaces.data.cex.DeclarationCexStep;
import cern.plcverif.verif.interfaces.data.cex.InstanceCexStep;
import cern.plcverif.verif.interfaces.data.cex.InstanceCounterexample;
import cern.plcverif.verif.requirement.PlcCycleRepresentation;

public class NusmvOutputParser {
	private final NusmvContext context;
	private final File outputToParse;

	private ResultEnum result = ResultEnum.Unknown;
	private NusmvCounterexample counterexample = null;
	private boolean hasCounterexample = false;

	public NusmvOutputParser(NusmvContext context) {
		Preconditions.checkNotNull(context);
		this.context = context;
		this.outputToParse = context.getCexFile();
	}

	public static void parseCounterexample(NusmvContext context) throws NusmvException {
		Preconditions.checkNotNull(context, "context");

		NusmvOutputParser parser = new NusmvOutputParser(context);
		parser.parse();

		// Save satisfaction to verifResult
		context.getVerifResult().setResult(parser.getResult());

		// Try to parse the counterexample to VariableRefs and Literals
		NusmvExprToExpr exprParser = new NusmvExprToExpr(context.getVarTrace());
		if (parser.hasCounterexample()) {
			InstanceCounterexample cex = new InstanceCounterexample();

			int stepCounter = 0;
			for (NusmvCexStep step : parser.getCounterexample().getStates()) {
				stepCounter++;
				InstanceCexStep cfaStep = new InstanceCexStep("Cycle " + stepCounter, cex);
				cfaStep.setStartOfLoop(step.isStartOfLoop());

				for (VariableValuePair vvp : step.getValues()) {
					if (!vvp.isHiddenForUser()) {
						Optional<VariableRef> cfaVarRef = exprParser.parseVarRef(vvp.getVariableName());
						if (cfaVarRef.isPresent()) {
							Literal cfaValue = exprParser.parseLiteral(vvp.getValue());
							cfaStep.setValue(cfaVarRef.get(), cfaValue);
						}
						// exprParser.parseLiteral will throw an exception if
						// the CFA variable is not found and it is not a known
						// exception
					}
				}
			}

			context.getVerifResult().setInstanceCounterexample(cex);
		}
	}

	private void parse() throws NusmvException {
		try {
			final String firstLine = Files.asCharSource(outputToParse, Charset.defaultCharset()).readFirstLine();

			if (firstLine == null) {
				context.logWarning("The output of NuSMV does not contain any content.");
				return;
			}

			if (firstLine.contains("-- specification")) {
				if (firstLine.contains("is true")) {
					this.result = ResultEnum.Satisfied;
				} else if (firstLine.contains("is false")) {
					this.result = ResultEnum.Violated;
				} else {
					context.logWarning(String.format(
							"It is not possible to determine the verification result based on the file output of NuSMV (%s).",
							firstLine));
					// non-fatal error, maybe we can still parse the
					// counterexample
					tryParseResultFromConsole();
				}
			} else {
				tryParseResultFromConsole();
				if (this.result == ResultEnum.Unknown) {
					context.logError(
						"It is not possible to determine the verification result based on the console output of NuSMV. First line: "
								+ firstLine);
				}
			}

			final List<String> allLines = Files.readLines(outputToParse, Charset.defaultCharset());
			if (allLines.size() > 1 && (allLines.get(1).contains("as demonstrated by the following")
					|| allLines.get(1).contains("Type: Counterexample"))) {
				this.counterexample = parseCex(allLines);
				this.hasCounterexample = true;
			} else {
				this.hasCounterexample = false;
			}

		} catch (IOException e) {
			// try another approach (based on the console output; mainly due to
			// IC3)
			tryParseResultFromConsole();
			if (this.result == ResultEnum.Unknown) {
				throw new NusmvParsingException("Unable to parse the output of NuSMV (based on the console output or the output file).", e);
			}
		}
	}
	
	private void tryParseResultFromConsole() {
		final Pattern isTrue = Pattern.compile("TL specification .* is true$", Pattern.MULTILINE);
		final Pattern isFalse = Pattern.compile("TL specification .* is false$", Pattern.MULTILINE);

		Optional<CharSequence> backendStdout = context.getVerifResult().getBackendStdout();
		if (backendStdout.isPresent()) {
			CharSequence execOutput = backendStdout.get();
			if (isTrue.matcher(execOutput).find()) {
				this.result = ResultEnum.Satisfied;
			} else if (isFalse.matcher(execOutput).find()) {
				this.result = ResultEnum.Violated;
			} else {
				this.result = ResultEnum.Unknown;
			}
		} else {
			context.logInfo("The NuSMV backend did not store any console output, it is not possible to determine the result based on it.");
			// No output defined, it is not possible to determine the result.
			// However, we shall not throw an exception, it was just a trial.
		}
	}

	public ResultEnum getResult() {
		return this.result;
	}

	public boolean hasCounterexample() {
		return this.hasCounterexample;
	}

	public NusmvCounterexample getCounterexample() {
		return this.counterexample;
	}

	private static NusmvCounterexample parseCex(final List<String> input) {
		final NusmvCounterexample cex = new NusmvCounterexample();

		NusmvCexStep currentState = null;
		boolean loopStarts = false;
		for (final String line : input) {
			if (line.trim().startsWith("->")) {
				// new state!
				currentState = new NusmvCexStep(line);
				cex.addState(currentState);

				if (loopStarts) {
					currentState.setStartOfLoop(true);
					loopStarts = false;
				}

				continue;
			}

			if ("-- Loop starts here".trim().equalsIgnoreCase(line)) {
				loopStarts = true;
			}

			if (currentState != null) {
				assert (line.startsWith("  "));

				if (line.indexOf('=') == -1) {
					continue;
				}

				final String var = line.substring(0, line.indexOf('=')).trim();
				final String val = line.substring(line.indexOf('=') + 1).trim();
				currentState.addValue(new VariableValuePair(var, val));
			}
		}

		// we want to keep states in which either BoC or EoC is true
		List<VariableValuePair> requiredVariableValuePairs = Arrays.asList(
				new VariableValuePair(PlcCycleRepresentation.EOC_FIELD_NAME, NusmvExpression.toNusmvLiteral(true)), 
				new VariableValuePair(PlcCycleRepresentation.BOC_FIELD_NAME, NusmvExpression.toNusmvLiteral(true)));

		NusmvCexModifier.filter(cex, requiredVariableValuePairs); 
		// TODO maybe store which variables are not for the user (e.g. __random,
		// EoC, ...)
		NusmvCexModifier.removeVariable(cex, NusmvModelBuilder.LOC_VAR);
		NusmvCexModifier.removeVariable(cex, PlcCycleRepresentation.EOC_FIELD_NAME);
		NusmvCexModifier.removeVariable(cex, PlcCycleRepresentation.BOC_FIELD_NAME);
		return cex;
	}
}
