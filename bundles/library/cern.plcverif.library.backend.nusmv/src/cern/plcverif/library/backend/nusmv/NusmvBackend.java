/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.nusmv;

import java.io.File;
import java.io.IOException;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.progress.ICanceling;
import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.exceptions.SettingsSerializerException;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.interfaces.data.result.StageStatus;
import cern.plcverif.base.models.cfa.cfabase.ElseExpression;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.cfa.transformation.CfaTransformationGoal;
import cern.plcverif.base.models.cfa.transformation.ElseEliminator;
import cern.plcverif.base.models.expr.FloatType;
import cern.plcverif.library.backend.nusmv.exception.NusmvException;
import cern.plcverif.library.backend.nusmv.exception.NusmvGracefulInterruptionException;
import cern.plcverif.library.backend.nusmv.executor.NusmvExecutionDetails;
import cern.plcverif.library.backend.nusmv.executor.NusmvExecutor;
import cern.plcverif.library.backend.nusmv.model.NusmvModelBuilder;
import cern.plcverif.library.backend.nusmv.model.RemoveFloatsFromCfa;
import cern.plcverif.library.backend.nusmv.parser.NusmvOutputParser;
import cern.plcverif.verif.interfaces.IBackend;
import cern.plcverif.verif.interfaces.data.RequirementRepresentationStrategy;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.VerificationResult;
import cern.plcverif.verif.interfaces.data.VerificationStageTags;

public class NusmvBackend implements IBackend {
	private NusmvSettings settings;

	public NusmvBackend(Settings settings) {
		if (settings instanceof SettingsElement) {
			try {
				this.settings = SpecificSettingsSerializer.parse(settings.toSingle(), NusmvSettings.class);
			} catch (SettingsParserException e) {
				throw new PlcverifPlatformException(String.format("Unable to parse the settings of the NuSMV backend. (%s)", e.getMessage()), e);
			}
		} else {
			throw new PlcverifPlatformException("Unable to parse the settings of the NuSMV backend. The given settings node is not a single node.");
		}
	}

	@Override
	public SettingsElement retrieveSettings() {
		try {
			return SpecificSettingsSerializer.toGenericSettings(settings);
		} catch (SettingsParserException | SettingsSerializerException e) {
			throw new PlcverifPlatformException("Unable to serialize the settings of the NuSMV backend.", e);
		}
	}

	@Override
	public void execute(VerificationProblem input, VerificationResult result, ICanceling canceler) {
		Preconditions.checkNotNull(result, "result");
		Preconditions.checkNotNull(result.getJobMetadata(), "result.jobMetadata");
		Preconditions.checkNotNull(result.getJobMetadata().getId(), "result.jobMetadata.id");
		try {
			File baseFile;
			if (result.getOutputDirectory() == null) {
				baseFile = File.createTempFile("PLCverif", "model");
			} else {
				baseFile = result.getOutputDirectory().resolve(result.getJobMetadata().getIdWithoutSpecialChars()).toFile();
			}
			execute(input, result, baseFile, canceler == null ? ICanceling.NEVER_CANCELING_INSTANCE : canceler);
		} catch (IOException e) {
			throw new PlcverifPlatformException("Unexpected IO exception during NuSMV backend execution: " + e.getMessage(), e);
		}
	}

	public StageStatus execute(VerificationProblem input, VerificationResult result, File baseFile, ICanceling canceler) {
		Preconditions.checkNotNull(input, "The given verification problem shall not be null.");
		Preconditions.checkNotNull(input.getModel(), "The model in the given verification problem shall not be null.");
		Preconditions.checkNotNull(input.getRequirement(), "The requirement in the given verification problem shall not be null.");
		Preconditions.checkNotNull(result, "The given verification result shall not be null.");
		Preconditions.checkArgument(input.getModel() instanceof CfaNetworkInstance, "The NuSMV backend only supports CFA instances.");
		Preconditions.checkNotNull(canceler, "The canceler shall not be null.");
		
		// Precomputation
		CfaNetworkInstance cfa = (CfaNetworkInstance) input.getModel();
		
		ElseEliminator.transform(cfa);
		Preconditions.checkState(EmfHelper.getAllContentsOfType(cfa, ElseExpression.class, false).isEmpty(),
				"There are some ElseExpressions in the CFA even after executing the ElseEliminator.");
		
		RemoveFloatsFromCfa.execute(cfa);
		Preconditions.checkState(EmfHelper.getAllContentsOfType(cfa, FloatType.class, false).isEmpty(),
				"There are some FloatTypes in the CFA even after executing the RemoveFloatsFromCfa.");
		
		if (canceler.isCanceled()) {
			return StageStatus.Skipped;
		}
		
		NusmvContext context = NusmvContext.create(input, result, baseFile, settings);
		context.startStage("NuSMV model building");
		
		try {
			NusmvModelBuilder.buildModel(context);

			context.startStage("NuSMV execution", VerificationStageTags.BACKEND_EXECUTION);
			NusmvExecutor.execute(context, NusmvExecutionDetails.create(context), canceler);
			if (canceler.isCanceled()) {
				return StageStatus.Skipped;
			}

			context.startStage("NuSMV output parsing");
			NusmvOutputParser.parseCounterexample(context);
			return StageStatus.Successful;
		} catch (NusmvGracefulInterruptionException e) {
			context.logWarning("The NuSMV backend execution has not been successful: " + e.getMessage());
			return StageStatus.Unsuccessful;
		} catch (NusmvException e) {
			context.logError("Error occurred in NuSMV backend: " + e.getMessage(), e);
			return StageStatus.Unsuccessful;
		}
	}

	@Override
	public CfaTransformationGoal getPreferredModelFormat() {
		return CfaTransformationGoal.INSTANCE_ENUMERATED;
	}

	@Override
	public RequirementRepresentationStrategy getPreferredRequirementFormat() {
		if (settings.isLocrefReqStrategy()) {
			return RequirementRepresentationStrategy.EXPLICIT_WITH_LOCATIONREF;
		} else {
			return RequirementRepresentationStrategy.EXPLICIT_WITH_VARIABLE;
		}
	}

	@Override
	public boolean isInliningRequired() {
		return true;
	}
	
	@Override
	public boolean areCfaAnnotationsRequired() {
		return false;
	}
}
