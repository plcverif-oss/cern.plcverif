/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.nusmv;

import java.io.File;

import com.google.common.base.Preconditions;

import cern.plcverif.base.interfaces.data.result.JobStage;
import cern.plcverif.base.interfaces.data.result.LogItem;
import cern.plcverif.base.common.logging.PlcverifSeverity;
import cern.plcverif.base.interfaces.data.result.StageTag;
import cern.plcverif.library.backend.nusmv.model.NusmvVariableTrace;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.VerificationResult;

public final class NusmvContext {
	public enum NusmvExpressionType {
		Ltl, Ctl, Invar
	}

	private VerificationProblem verifProblem;
	private VerificationResult result;

	private File modelFile = null;
	private File scriptFile = null;
	private File cexFile = null;

	private NusmvVariableTrace varTrace = null;
	private JobStage currentStage = null;
	private NusmvSettings settings;

	private NusmvExpressionType exprType;

	private NusmvContext(VerificationProblem verifProblem, VerificationResult result, File baseFile,
			NusmvSettings settings) {
		this.verifProblem = Preconditions.checkNotNull(verifProblem);
		this.result = Preconditions.checkNotNull(result);
		this.settings = Preconditions.checkNotNull(settings);

		this.modelFile = new File(baseFile.getAbsolutePath() + ".smv");
		this.scriptFile = new File(baseFile.getAbsolutePath() + ".smv.script");
		this.cexFile = new File(baseFile.getAbsolutePath() + ".smv.cex");
	}

	/**
	 * Creates a new NuSMV verification context.
	 * 
	 * @param verifProblem
	 *            Verification problem to be analyzed.
	 * @param result
	 *            Representation of the verification result (to be filled).
	 * @param baseFile
	 *            Base file name for outputs. The actual output files will be
	 *            created using this file name, appended with various
	 *            extensions.
	 * @param settings
	 *            Backend settings to be used.
	 * @return The created context.
	 */
	public static NusmvContext create(VerificationProblem verifProblem, VerificationResult result, File baseFile,
			NusmvSettings settings) {
		return new NusmvContext(verifProblem, result, baseFile, settings);
	}

	/**
	 * Returns the path to be used for the NuSMV model file.
	 * <p>
	 * Created based on the base file provided at the creation of the context.
	 * 
	 * @return Path for NuSMV model file.
	 */
	public File getModelFile() {
		return modelFile;
	}

	/**
	 * Returns the path to be used for the NuSMV execution script file.
	 * <p>
	 * Created based on the base file provided at the creation of the context.
	 * 
	 * @return Path for NuSMV execution script file.
	 */
	public File getScriptFile() {
		return scriptFile;
	}

	/**
	 * Returns the path to be used for the NuSMV counterexample file.
	 * <p>
	 * Created based on the base file provided at the creation of the context.
	 * 
	 * @return Path for NuSMV counterexample file.
	 */
	public File getCexFile() {
		return cexFile;
	}

	/**
	 * Returns the variable trace used for and build during the NuSMV model
	 * generation.
	 * 
	 * @return Variable trace.
	 * @throws UnsupportedOperationException
	 *             if the variable trace has not been set.
	 */
	public NusmvVariableTrace getVarTrace() {
		if (varTrace == null) {
			throw new UnsupportedOperationException("Variable trace was not set yet.");
		}
		return varTrace;
	}

	/**
	 * Sets the variable trace used for and build during the NuSMV model
	 * generation.
	 * 
	 * @param varTrace
	 *            Variable trace.
	 */
	public void setVarTrace(NusmvVariableTrace varTrace) {
		this.varTrace = varTrace;
	}

	/**
	 * Returns the verification problem.
	 */
	public VerificationProblem getVerifProblem() {
		return verifProblem;
	}

	/**
	 * Returns the verification result.
	 */
	public VerificationResult getVerifResult() {
		return result;
	}

	/**
	 * Starts a stage using the given name and tags.
	 * 
	 * @param stage
	 *            Name of the stage to be started.
	 * @param tags
	 *            Tags for teh stage to be started. Optional.
	 */
	public void startStage(String stage, StageTag... tags) {
		currentStage = result.switchToStage(stage, tags);
	}

	/**
	 * Adds a log message with the given severity and message to the currently
	 * active stage.
	 * 
	 * @param severity
	 *            Severity of the log item.
	 * @param message
	 *            Log message.
	 */
	public void log(PlcverifSeverity severity, String message) {
		Preconditions.checkNotNull(currentStage, "There is no stage set to log.");
		currentStage.log(LogItem.create(currentStage, severity, message));
	}

	/**
	 * Adds a log message with the given severity, message and throwable to the
	 * currently active stage.
	 * 
	 * @param severity
	 *            Severity of the log item.
	 * @param message
	 *            Log message.
	 * @param throwable
	 *            Throwable to be registered for this log item.
	 */
	public void log(PlcverifSeverity severity, String message, Throwable throwable) {
		Preconditions.checkNotNull(currentStage, "There is no stage set to log.");
		currentStage.log(LogItem.create(currentStage, severity, message, throwable));
	}

	/**
	 * Adds a log message with {@link PlcverifSeverity#Info} severity and the
	 * given message to the currently active stage.
	 * 
	 * @param message
	 *            Log message.
	 */
	public void logInfo(String message) {
		log(PlcverifSeverity.Info, message);
	}

	/**
	 * Adds a log message with {@link PlcverifSeverity#Warning} severity and the
	 * given message to the currently active stage.
	 * 
	 * @param message
	 *            Log message.
	 */
	public void logWarning(String message) {
		log(PlcverifSeverity.Warning, message);
	}

	/**
	 * Adds a log message with {@link PlcverifSeverity#Error} severity and the
	 * given message to the currently active stage.
	 * 
	 * @param message
	 *            Log message.
	 */
	public void logError(String message) {
		log(PlcverifSeverity.Error, message);
	}

	/**
	 * Adds a log message with {@link PlcverifSeverity#Error} severity, the
	 * given message and throwable to the currently active stage.
	 * 
	 * @param message
	 *            Log message.
	 * @param throwable
	 *            Throwable to be registered for this log item.
	 */
	public void logError(String message, Throwable throwable) {
		log(PlcverifSeverity.Error, message, throwable);
	}

	/**
	 * Returns the settings to be used by the backend plug-in.
	 */
	public NusmvSettings getSettings() {
		return settings;
	}

	/**
	 * Returns the expression type of the requirement. May be {@code null} if
	 * not set.
	 */
	public NusmvExpressionType getExprType() {
		return exprType;
	}

	/**
	 * Sets the expression type of the requirement.
	 */
	public void setExprType(NusmvExpressionType exprType) {
		this.exprType = exprType;
		this.result.getValueStore().addValue(NusmvBackendExtension.CMD_ID, "expression_type", exprType.toString());
	}
}
