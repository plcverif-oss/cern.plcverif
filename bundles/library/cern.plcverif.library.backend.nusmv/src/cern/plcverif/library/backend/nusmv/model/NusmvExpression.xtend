/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.library.backend.nusmv.model

import cern.plcverif.base.models.cfa.CfaInstanceSafeFactory
import cern.plcverif.base.models.cfa.cfainstance.VariableRef
import cern.plcverif.base.models.expr.BinaryArithmeticExpression
import cern.plcverif.base.models.expr.BinaryArithmeticOperator
import cern.plcverif.base.models.expr.BinaryCtlExpression
import cern.plcverif.base.models.expr.BinaryLogicExpression
import cern.plcverif.base.models.expr.BinaryLogicOperator
import cern.plcverif.base.models.expr.BinaryLtlExpression
import cern.plcverif.base.models.expr.BinaryLtlOperator
import cern.plcverif.base.models.expr.BoolLiteral
import cern.plcverif.base.models.expr.BoolType
import cern.plcverif.base.models.expr.ComparisonExpression
import cern.plcverif.base.models.expr.ComparisonOperator
import cern.plcverif.base.models.expr.ElementaryType
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.FloatType
import cern.plcverif.base.models.expr.IntLiteral
import cern.plcverif.base.models.expr.IntType
import cern.plcverif.base.models.expr.Nondeterministic
import cern.plcverif.base.models.expr.Type
import cern.plcverif.base.models.expr.TypeConversion
import cern.plcverif.base.models.expr.UnaryArithmeticExpression
import cern.plcverif.base.models.expr.UnaryCtlExpression
import cern.plcverif.base.models.expr.UnaryCtlOperator
import cern.plcverif.base.models.expr.UnaryLogicExpression
import cern.plcverif.base.models.expr.UnaryLtlExpression
import cern.plcverif.base.models.expr.UnaryLtlOperator
import cern.plcverif.base.models.expr.UnknownType
import cern.plcverif.library.backend.nusmv.exception.NusmvModelGenerationException
import com.google.common.base.Preconditions
import org.eclipse.emf.ecore.util.EcoreUtil
import cern.plcverif.base.models.expr.string.ExprToString
import cern.plcverif.base.models.expr.LibraryFunction
import cern.plcverif.base.models.expr.BeginningOfCycle
import cern.plcverif.base.models.expr.EndOfCycle
import org.eclipse.xtend.lib.annotations.Accessors
import cern.plcverif.base.models.expr.StringLiteral
import java.util.regex.Pattern

class NusmvExpression {
	NusmvVariableTrace varTrace = new NusmvVariableTrace();
	
	@Accessors(PUBLIC_SETTER)
	String beginningOfCycleExpression = null;
	@Accessors(PUBLIC_SETTER)
	String endOfCycleExpression = null;

	def getVarTrace() {
		return varTrace;
	}

	def dispatch CharSequence toNusmvExpression(Void expr) {
		throw new NusmvModelGenerationException("Unexpected NULL expression to be represented.");
	}
	
	def dispatch CharSequence toNusmvExpression(Expression expr) {
		throw new NusmvModelGenerationException('''Unsupported expression «ExprToString.toDiagString(expr)» of type «expr.class.name».''');
	}
	
	def dispatch CharSequence toNusmvExpression(BeginningOfCycle expr) {
		if (beginningOfCycleExpression === null) {
			throw new NusmvModelGenerationException('Unable to represent BeginningOfCycle in the expression as it was not set up in the NuSMV expression translator.');
		} else {
			return '''(«beginningOfCycleExpression»)''';
		}
	}
	
	def dispatch CharSequence toNusmvExpression(EndOfCycle expr) {
		if (endOfCycleExpression === null) {
			throw new NusmvModelGenerationException('Unable to represent EndOfCycle in the expression as it was not set up in the NuSMV expression translator.');
		} else {
			return '''(«endOfCycleExpression»)''';
		}
	}

	private def binary(Expression left, String nusmvOp, Expression right) {
		return '''(«left.toNusmvExpression») «nusmvOp» («right.toNusmvExpression»)''';
	}

	def dispatch CharSequence toNusmvExpression(BinaryLogicExpression expr) {
		return binary(expr.leftOperand, expr.operator.toNusmvOp, expr.rightOperand);
	}

	def dispatch CharSequence toNusmvExpression(BinaryArithmeticExpression expr) {
		return binary(expr.leftOperand, expr.operator.toNusmvOp, expr.rightOperand);
	}
	
	def dispatch CharSequence toNusmvExpression(ComparisonExpression expr) {
		return binary(expr.leftOperand, expr.operator.toNusmvOp, expr.rightOperand);
	}

	def dispatch CharSequence toNusmvExpression(BinaryLtlExpression expr) {
		return '''(«binary(expr.leftOperand, expr.operator.toNusmvOp, expr.rightOperand)»)''';
	}

	def dispatch CharSequence toNusmvExpression(BinaryCtlExpression expr) {
		switch (expr.operator) {
			case AU: return '''A [«binary(expr.leftOperand, " U ", expr.rightOperand)»]'''
			case EU: return '''E [«binary(expr.leftOperand, " U ", expr.rightOperand)»]'''
			default: throw new UnsupportedOperationException("Unknown BinaryCtlExpression: " + expr.operator)
		}
	}

	private def unary(String nusmvOp, Expression expr) {
		return '''«nusmvOp»(«expr.toNusmvExpression»)''';
	}

	def dispatch CharSequence toNusmvExpression(UnaryLogicExpression expr) {
		switch (expr.operator) {
			case NEG: return unary("!", expr.operand)
			default: throw new UnsupportedOperationException("Unknown UnaryLogicExpression: " + expr.operator)
		}
	}
	
	def dispatch CharSequence toNusmvExpression(UnaryArithmeticExpression expr) {
		switch (expr.operator) {
			case BITWISE_NOT: return unary("!", expr.operand)
			case MINUS: return unary("-", expr.operand)
			default: throw new UnsupportedOperationException("Unknown UnaryArithmeticExpression: " + expr.operator)
		}
	}

	def dispatch CharSequence toNusmvExpression(UnaryLtlExpression expr) {
		return unary(toNusmvOp(expr.operator), expr.operand);
	}

	def dispatch CharSequence toNusmvExpression(UnaryCtlExpression expr) {
		return unary(toNusmvOp(expr.operator), expr.operand);
	}

	def dispatch CharSequence toNusmvExpression(TypeConversion expr) {
		val sourceType = expr.operand.type;
		val targetType = expr.type;

		if (targetType instanceof IntType) {
			if (sourceType instanceof IntType) {
				if (targetType.isSigned && sourceType.isSigned && targetType.bits < sourceType.bits) {
					// When signed integers are downcasted, we need to temporarily
					// remove the sign for the size change to obtain the desired results.
					return '''signed(resize(unsigned(«expr.operand.toNusmvExpression»), «targetType.bits»))''';
				}
				
				
				var signum = "";
				if (targetType.isSigned && !sourceType.isSigned) { 
					signum = "signed"; 
					sourceType.bits = sourceType.bits + 1;
				}
				if (!targetType.isSigned && sourceType.isSigned) { signum = "unsigned" }
				return '''resize(«signum»(«expr.operand.toNusmvExpression»), «targetType.bits»)''';
			} else if (sourceType instanceof BoolType) {
				val signum = if (targetType.isSigned) "signed" else "";
				return '''«signum»(resize(word1(«expr.operand.toNusmvExpression»), «targetType.bits»))''';
			}
		}

		throw new UnsupportedOperationException(String.format(
			"Unsupported type conversion in NuSMV from '%s' to '%s'.",
			sourceType,
			targetType
		));
	}

	def dispatch CharSequence toNusmvExpression(LibraryFunction expr) {
		throw new NusmvModelGenerationException('''The NuSMV backend does not support the library function '«ExprToString.toDiagString(expr)»'.''');
	}

	def dispatch CharSequence toNusmvExpression(IntLiteral expr) {
		Preconditions.checkArgument(expr.type !== null);
		Preconditions.checkArgument(!(expr.type instanceof UnknownType));
		Preconditions.checkArgument(expr.type instanceof IntType, "An IntLiteral is expected to have an IntType.");

		val intType = expr.type as IntType;
		val signumStr = if (expr.value >= 0) "" else "-"; 
		return '''«signumStr»0«if (intType.signed) 's' else 'u'»d«intType.bits»_«Math.abs(expr.value)»''';
	}

	def dispatch CharSequence toNusmvExpression(BoolLiteral expr) {
		return toNusmvLiteral(expr.value);
	}	
	
	def static String toNusmvLiteral(boolean boolValue) {
		if (boolValue) {
			return "TRUE";
		} else {
			return "FALSE";
		}
	}

	def dispatch CharSequence toNusmvExpression(VariableRef expr) {
		return varTrace.toName(expr.variable);
	}

	def dispatch CharSequence toNusmvExpression(Nondeterministic expr) {
		val type = expr.type;
		switch (type) {
			BoolType: return "{TRUE, FALSE}"
			IntType: throw new NusmvModelGenerationException("It is impossible to represent Nondeterministic of IntType in NuSMV.")
			FloatType: throw new NusmvModelGenerationException("It is impossible to represent Nondeterministic of FloatType in NuSMV.")
			default: throw new UnsupportedOperationException("Unsupported type: " + type)
		}
	}

	def static dispatch CharSequence toNusmvType(Type type) {
		return '''**Unknown type: «type»'''; 
	}

	def static dispatch CharSequence toNusmvType(FloatType type) {
		throw new NusmvModelGenerationException("FloatTypes are not handled by the NuSMV model generator directly. By this phase, all floats should have been eliminated using 'RemoveFloatsFromCfa'.");
	}

	def static dispatch CharSequence toNusmvType(IntType type) {
		return '''«if (type.signed) 'signed' else 'unsigned'» word[«type.bits»]''';
	}

	def static dispatch CharSequence toNusmvType(BoolType type) {
		return 'boolean';
	}

	def static dispatch toNusmvOp(BinaryLogicOperator operator) {
		switch (operator) {
			case AND: return "&"
			case OR: return "|"
			case XOR: return " xor "
			case IMPLIES: return "->"
			default: throw new UnsupportedOperationException("Unknown BinaryLogicOperator: " + operator)
		}
	}

	def static dispatch toNusmvOp(BinaryArithmeticOperator operator) {
		switch (operator) {
			case PLUS: return "+"
			case MINUS: return "-"
			case MULTIPLICATION: return "*"
			case DIVISION: return "/"
			case MODULO: return " mod "
			case INTEGER_DIVISION: return "/"
//			case POWER: 
			case BITSHIFT_LEFT: return "<< unsigned " //NuSMV requires the shift operator to be unsigned
			case BITSHIFT_RIGHT: return ">> unsigned " //NuSMV requires the shift operator to be unsigned
//			case BITROTATE: 
			case BITWISE_OR: return "|"
			case BITWISE_AND: return "&"
			case BITWISE_XOR: return " xor "
			default: throw new UnsupportedOperationException("Unknown BinaryArithmeticOperator: " + operator)
		}
	}

	def static dispatch toNusmvOp(ComparisonOperator operator) {
		switch (operator) {
			case EQUALS: return "="
			case NOT_EQUALS: return "!="
			case LESS_THAN: return "<"
			case GREATER_THAN: return ">"
			case LESS_EQ: return "<="
			case GREATER_EQ: return ">="
			default: throw new UnsupportedOperationException("Unknown ComparisonOperator: " + operator)
		}
	}

	def static dispatch toNusmvOp(BinaryLtlOperator operator) {
		switch (operator) {
			case U: return " U "
			case R: return " R "
			case S: return " S "
			case V: return " V "
			default: throw new UnsupportedOperationException("Unknown BinaryLtlOperator: " + operator)
		}
	}

	def static dispatch toNusmvOp(UnaryLtlOperator operator) {
		switch (operator) {
			case X: return "X"
			case F: return "F"
			case G: return "G"
			case O: return "O"
			case H: return "H"
			case Y: return "Y"
			default: throw new UnsupportedOperationException("Unknown UnaryLtlOperator: " + operator)
		}
	}

	def static dispatch toNusmvOp(UnaryCtlOperator operator) {
		switch (operator) {
			case AX: return "AX"
			case AF: return "AF"
			case AG: return "AG"
			case EX: return "EX"
			case EF: return "EF"
			case EG: return "EG"
			default: throw new UnsupportedOperationException("Unknown UnaryCtlOperator: " + operator)
		}
	}
	
	def zero(ElementaryType type) {
		switch (type) {
			BoolType: return "FALSE"
			IntType: return toNusmvExpression(
				CfaInstanceSafeFactory.INSTANCE.createIntLiteral(0, EcoreUtil.copy(type)))
			FloatType: return toNusmvExpression(
				CfaInstanceSafeFactory.INSTANCE.createFloatLiteral(0, EcoreUtil.copy(type)))
		}
	}
	
}
