/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.nusmv.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cern.plcverif.library.backend.nusmv.parser.counterexample.NusmvCexStep;
import cern.plcverif.library.backend.nusmv.parser.counterexample.NusmvCounterexample;
import cern.plcverif.library.backend.nusmv.parser.counterexample.VariableValuePair;


public final class NusmvCexModifier {
	private NusmvCexModifier() {
		// Utility class.
	}
	
	/**
	 * Remove all CexSteps from the counterexample, where 'variable' != 'value'.
	 *
	 * @param cex
	 *            The counterexample to be modified.
	 * @param variable
	 *            The variable for which there is a constraint.
	 * @param value
	 *            The required value of the given variable.
	 */
	public static void filter(final NusmvCounterexample cex, final String variable, final String value){
		List<VariableValuePair> requiredVariableValuePairs = Arrays.asList(new VariableValuePair(variable, value));
		NusmvCexModifier.filter(cex, requiredVariableValuePairs);
	}
	
	/**
	 * Remove all CexSteps from the counterexample, where none of variable-value pairs is present
	 *
	 * @param cex
	 *            The counterexample to be modified.
	 * @param variableValuePairs
	 *            The list of variable-value constraints.
	 */
	public static void filter(final NusmvCounterexample cex, final List<VariableValuePair> variableValuePairs){
		final List<NusmvCexStep> toBeDeleted = new ArrayList<>();

		for (final NusmvCexStep state : cex.getStates()) {
			if (!NusmvCexModifier.stateMeetsVVConstraint(state, variableValuePairs)){
				toBeDeleted.add(state);
			}
		}

		//remove
		for (final NusmvCexStep state : toBeDeleted) {
			cex.removeState(state);
		}
	}
	
	/**
	 * Check if the CexStep contains at least one required variable-value constraint
	 *
	 * @param state
	 *            The counterexample step to be checked.
	 * @param variableValuePairs
	 *            The list of variable-value constraints.
	 */
	private static boolean stateMeetsVVConstraint(final NusmvCexStep state, final List<VariableValuePair> variableValuePairs) {
		for (final VariableValuePair stateVV : state.getValues()) {
			for (final VariableValuePair requiredVV : variableValuePairs) {
				if (stateVV.getVariableName().equalsIgnoreCase(requiredVV.getVariableName()) 
						&& stateVV.getValue().equalsIgnoreCase(requiredVV.getValue())) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * Remove all Variables from the counterexample step matching the regex.
	 *
	 * @param cex
	 *            The counterexample to be modified.
	 * @param varNameRegex
	 *            The regex to match variable names.
	 */
	public static void removeVariable(final NusmvCounterexample cex, final String varNameRegex) {
		for (final NusmvCexStep step : cex.getStates()) {
			for (final VariableValuePair vvp : new ArrayList<>(step.getValues())) {
				if (vvp.getVariableName().matches(varNameRegex)) {
					step.getValues().remove(vvp);
				}
			}
		}
	}
}
