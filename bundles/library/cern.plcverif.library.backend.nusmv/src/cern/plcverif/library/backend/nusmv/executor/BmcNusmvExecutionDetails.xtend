/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Xaver Fink - BMC addition
 *****************************************************************************/
package cern.plcverif.library.backend.nusmv.executor

import cern.plcverif.library.backend.nusmv.NusmvContext
import cern.plcverif.library.backend.nusmv.exception.NusmvExecutionException
import com.google.common.base.Preconditions
import cern.plcverif.library.backend.nusmv.NusmvSettings.NusmvAlgorithm
import java.io.File

class BmcNusmvExecutionDetails extends NusmvExecutionDetails {
	protected new(NusmvContext context) {
		super(context);
	}

	override String getExecScriptContent(File cexFile) throws NusmvExecutionException {
		Preconditions.checkState(this.getSettings().getAlgorithm() == NusmvAlgorithm.Bmc);

		// We hide everything to avoid pringint cex on stdout.
		// Then we print explicitly trace 1 if exists.
		val script = '''
			set on_failure_script_quits
			set default_trace_plugin 1
			set traces_hiding_prefix ""
			set verbose_level 4
			go_bmc
			«determineCheckCommand» -k 1000
			echo *** PLCverif: checking phase finished. Next phase: printing counterexample trace.
			set traces_hiding_prefix ___
			show_traces -o "«cexFile.toPath().toAbsolutePath()»" 1
			echo *** PLCverif: execution finished, nuXmv quits.
			quit
		'''
		return script;
	}

	private def String determineCheckCommand() throws NusmvExecutionException {
		switch (getContext().getExprType()) {
			case Ctl: {
				context.logError("The nuXmv BMC backend does not support CTL expressions.");
				throw new NusmvExecutionException("The nuXmv BMC backend does not support CTL expressions.");
			}
			case Ltl:
				return "check_ltlspec_bmc_inc"
			case Invar:
				return "check_invar_bmc"
			default:
				throw new NusmvExecutionException("Unknown requirement type: " + getContext().getExprType())
		}
	}
}
