/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.nusmv;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsSerializer;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.help.SettingsHelp;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.verif.interfaces.IBackend;
import cern.plcverif.verif.interfaces.IBackendExtension;
import cern.plcverif.verif.job.VerificationJobOptions;

public class NusmvBackendExtension implements IBackendExtension {
	public static final String CMD_ID = "nusmv";
	
	@Override
	public IBackend createBackend() {
		return new NusmvBackend(loadDefaultSettings());
	}

	@Override
	public IBackend createBackend(SettingsElement settings) throws SettingsParserException {
		Settings compositeSettings = loadDefaultSettings();
		compositeSettings.overrideWith(settings);
		return new NusmvBackend(compositeSettings);
	}

	public SettingsElement loadDefaultSettings() {
		try {
			String settingString = IoUtils.readResourceFile(
					SettingsSerializer.replaceOsPlaceholder("/settings/default.${OS}.settings"),
					this.getClass().getClassLoader());
			SettingsElement ret = SettingsSerializer.parseSettingsFromString(settingString).toSingle();
			ret.setValue(CMD_ID);
			return ret;
		} catch (SettingsParserException e) {
			return SettingsElement.createRootElement(VerificationJobOptions.BACKEND, CMD_ID);
		}
	}

	@Override
	public void fillSettingsHelp(SettingsHelp help) {
		help.addPluginDetails(CMD_ID, "NuSMV verification backend",
				"Solves the verification problem by using the NuSMV/nuXmv model checker. Only applicable if job=verif.",	
				NusmvBackendExtension.class);
		
		// Try to load the default settings
		NusmvSettings defaults;
		try {
			defaults = SpecificSettingsSerializer.parse(loadDefaultSettings(), NusmvSettings.class);
		} catch (SettingsParserException e) {
			// best effort (don't die on help creation)
			defaults = new NusmvSettings();
		}
		
		// Fill the 'SettingsHelp' object
		SpecificSettingsSerializer.fillSettingsHelp(help, NusmvSettings.class, defaults);
	}
}
