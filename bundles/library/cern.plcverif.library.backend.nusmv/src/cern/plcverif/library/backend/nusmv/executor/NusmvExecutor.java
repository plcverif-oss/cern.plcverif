/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.nusmv.executor;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import cern.plcverif.base.common.progress.ICanceling;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.base.common.utils.OsUtils;
import cern.plcverif.library.backend.nusmv.NusmvContext;
import cern.plcverif.library.backend.nusmv.exception.NusmvException;
import cern.plcverif.library.backend.nusmv.exception.NusmvExecutionException;
import cern.plcverif.library.backend.nusmv.exception.NusmvGracefulInterruptionException;
import cern.plcverif.verif.interfaces.data.VerificationResult;
import cern.plcverif.verif.utils.backend.BackendExecutor;
import cern.plcverif.verif.utils.backend.BackendExecutor.BackendExecutionException;
import cern.plcverif.verif.utils.backend.BackendExecutor.BackendExecutionStatus;
import cern.plcverif.verif.utils.backend.BackendExecutor.BackendExecutorResult;

public class NusmvExecutor {
	private final NusmvContext context;
	private final VerificationResult verifResult;
	private IExecutionDetails execDetails;
	private ICanceling canceler;

	public NusmvExecutor(NusmvContext context, IExecutionDetails execDetails, ICanceling canceler) {
		this.context = context;
		this.verifResult = context.getVerifResult();
		this.execDetails = execDetails;
		this.canceler = canceler;
	}

	public static void execute(NusmvContext context, IExecutionDetails execDetails, ICanceling canceler)
			throws NusmvException {
		NusmvExecutor executor = new NusmvExecutor(context, execDetails, canceler);
		executor.execute();
	}

	public void execute() throws NusmvException {
		verifResult.setBackendAlgorithmId(execDetails.getBackendConfigurationId());

		// Locate the NuSMV binary
		final String executablePath = verifResult.makeAbsoluteToProgramDirectory(this.execDetails.getBinaryLocation())
				.toString();
		if (executablePath == null || executablePath.length() == 0) {
			throw new NusmvExecutionException("The path of NuSMV is unknown.");
		}
		if (!Paths.get(executablePath).toFile().exists()) {
			throw new NusmvExecutionException(
					String.format("The NuSMV binary is not found at the expected '%s' location.", executablePath));
		}

		// Assemble script
		generateScriptFile();

		// Assemble executable process
		final int timeoutSec = context.getSettings().getTimeoutInSec();
		final List<String> procArguments = new ArrayList<>(Arrays.asList(executablePath));
		procArguments.addAll(this.execDetails.getCommandLineOptions(context.getScriptFile().getAbsolutePath(),
				context.getModelFile().getAbsolutePath()));

		BackendExecutor executorBuilder = BackendExecutor.create(procArguments, timeoutSec)
				.logger(context.getVerifResult().currentStage()).canceler(Optional.ofNullable(canceler))
				.stdoutCommentLinePattern("^((\\s*\\*\\*\\*.+)|(\\s*))$");

		try {
			removeOldOutputFile();

			BackendExecutorResult execResult = executorBuilder.execute();
			verifResult.setBackendStdout(execResult.getStdout());
			verifResult.setBackendStderr(execResult.getStderr());
			context.logInfo(String.format("Backend execution status: %s", execResult.getStatus()));

			if (execResult.getStatus() == BackendExecutionStatus.Timeout) {
				// Logging already done by BackendExecutor
				throw new NusmvGracefulInterruptionException("Timeout.");
			}
			if (execResult.getStatus() == BackendExecutionStatus.Canceled) {
				// Logging already done by BackendExecutor
				throw new NusmvGracefulInterruptionException("User cancellation.");
			}

			// Error handling
			String stdErrContent = execResult.getStderr().toString();
			if (stdErrContent.contains("error") || stdErrContent.contains("aborting")
					|| stdErrContent.contains("reached invalid code")
					|| execResult.getStatus() == BackendExecutionStatus.Error) {
				throw new NusmvExecutionException("NuSMV ERROR: " + stdErrContent);
			}
		} catch (BackendExecutionException e) {
			// Try to improve the error message
			String additionalMessage = "";
			if (OsUtils.getCurrentOs() != OsUtils.OperatingSystem.Win32 && (e.getMessage().contains("Permission denied") || e.getMessage().contains("Access denied"))) {
				additionalMessage = " Maybe the NuSMV binary is not set as executable (chmod +x)?";
			}
			
			throw new NusmvExecutionException("Error during NuSMV execution." + additionalMessage, e);
		}
	}

	/**
	 * Generates a script file to the location
	 * {@link NusmvContext#getScriptFile()}, based on the execution details of
	 * this executor ({@link #execDetails}).
	 * 
	 * @throws NusmvExecutionException
	 *             if it was unsuccessful to generate the file.
	 */
	private void generateScriptFile() throws NusmvExecutionException {
		final String scriptContent = this.execDetails.getExecScriptContent(context.getCexFile());
		if (scriptContent == null) {
			throw new NusmvExecutionException("Unable to generate executor script for NuSMV.");
		}
		try {
			IoUtils.writeAllContent(context.getScriptFile(), scriptContent);
		} catch (IOException e) {
			throw new NusmvExecutionException("Error occured while writing NuSMV script.", e);
		}
	}

	/**
	 * Removes the previous execution output file, located at
	 * {@link NusmvContext#getCexFile()}, if it exists.
	 * 
	 * @throws NusmvExecutionException
	 *             if it was unsuccessful to remove the file.
	 */
	private void removeOldOutputFile() throws NusmvExecutionException {
		final File oldOutput = context.getCexFile();
		if (oldOutput.exists()) {
			try {
				Files.delete(oldOutput.toPath());
			} catch (IOException exception) {
				String message = String.format("Unable to delete existing old output file '%s'. %s", oldOutput.toString(), exception.getMessage());
				throw new NusmvExecutionException(message, exception);
			}
		}
	}
}
