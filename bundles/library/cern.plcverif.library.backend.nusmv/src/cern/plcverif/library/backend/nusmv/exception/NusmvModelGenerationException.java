/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.nusmv.exception;

public class NusmvModelGenerationException extends NusmvException {
	private static final long serialVersionUID = -217727753453766506L;
	
	public NusmvModelGenerationException(String message) {
		super(message);
	}
	
	public NusmvModelGenerationException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
