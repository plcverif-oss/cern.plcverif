/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.nusmv.parser.counterexample;

public class VariableValuePair {
	private String variableName;
	private String value;
	private boolean hiddenForUser = false;

	public VariableValuePair(final String variableFqn, final String value) {
		this.variableName = variableFqn;
		this.value = value;
	}
	
	public String getVariableName() {
		return this.variableName;
	}

	public String getValue() {
		return this.value;
	}

	public boolean isHiddenForUser() {
		return this.hiddenForUser;
	}

	public void setVariableName(final String variableFqn) {
		this.variableName = variableFqn;
	}

	public void setValue(final String value) {
		this.value = value;
	}

	public void setHiddenForUser(final boolean hiddenForUser) {
		this.hiddenForUser = hiddenForUser;
	}
}
