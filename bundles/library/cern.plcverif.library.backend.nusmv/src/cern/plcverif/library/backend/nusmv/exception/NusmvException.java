/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.nusmv.exception;

public abstract class NusmvException extends Exception {
	private static final long serialVersionUID = 5555949551446186975L;

	public NusmvException(String message) {
		super(message);
	}
	
	public NusmvException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
