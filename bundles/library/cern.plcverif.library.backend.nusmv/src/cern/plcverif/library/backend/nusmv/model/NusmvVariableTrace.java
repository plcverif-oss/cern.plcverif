/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.nusmv.model;

import java.util.HashSet;
import java.util.Set;

import cern.plcverif.base.models.cfa.cfainstance.Variable;
import cern.plcverif.verif.utils.backend.BackendNameTrace;

public class NusmvVariableTrace {
	private BackendNameTrace<Variable> trace = new BackendNameTrace<>(NusmvUtils::makeValidName);
	private Set<String> otherKnownVars = new HashSet<>();
	
	public String toName(Variable v) {
		return trace.toName(v);
	}
	
	public Variable toVariable(String name) {
		return trace.toModelElement(name);
	}
	
	public void addOtherKnownVariable(String varName) {
		this.otherKnownVars.add(varName);
	}
	
	public boolean isOtherKnownVariable(String varName) {
		return otherKnownVars.contains(varName);
	}

}
