/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.library.backend.nusmv.parser

import cern.plcverif.base.models.cfa.CfaInstanceSafeFactory
import cern.plcverif.base.models.cfa.cfainstance.VariableRef
import cern.plcverif.base.models.expr.Literal
import cern.plcverif.library.backend.nusmv.exception.NusmvParsingException
import cern.plcverif.library.backend.nusmv.model.NusmvVariableTrace
import com.google.common.base.Preconditions
import java.util.regex.Pattern
import java.util.Optional

class NusmvExprToExpr {
	static CfaInstanceSafeFactory factory = CfaInstanceSafeFactory.INSTANCE;
	static String INT_LITERAL_REGEX = "(-?)0([su])d(\\d+)_(\\d+)";
	static Pattern INT_LITERAL_PATTERN = Pattern.compile(INT_LITERAL_REGEX);
	
	NusmvVariableTrace varTrace;
	
	new(NusmvVariableTrace varTrace) {
		Preconditions.checkNotNull(varTrace, "The NuSMV variable trace should not be null.");
		this.varTrace = varTrace;
	}
	
	def Literal parseLiteral(String str) {
		if (str.equals("TRUE")) {
			return factory.trueLiteral();
		} else if (str.equals("FALSE")) {
			return factory.falseLiteral();
		} else if (str.matches(INT_LITERAL_REGEX)) {
			val matcher = INT_LITERAL_PATTERN.matcher(str);
			
			if (!matcher.find()) {
				throw new NusmvParsingException("Unable to parse the following NuSMV literal as IntLiteral: " + str);
			}
			val isNegative = (matcher.group(1).equals("-"));
			val isSigned = (matcher.group(2).equals("s"));
			val bits = Integer.parseInt(matcher.group(3));
			Preconditions.checkState(bits > 1);
			val rawValue = Long.parseLong(matcher.group(4));
			Preconditions.checkState(!isNegative || isSigned, "Only signed literals can be negative.");
			val value = if (isNegative) -rawValue else rawValue;
			
			return factory.createIntLiteral(value, isSigned, bits);
		} else {
			throw new NusmvParsingException("Unable to parse the following NuSMV literal: " + str);
		}
	}
	
	def Optional<VariableRef> parseVarRef(String varName) {
		val cfiVar = varTrace.toVariable(varName);
		if (cfiVar === null) {
			if (varTrace.isOtherKnownVariable(varName)) {
				println("NuSMV counterexample variable that is known but not present in CFA: " + varName);
				return Optional.empty();	
			}
			throw new NusmvParsingException("Unable to parse the following NuSMV variable: " + varName);
		} else {
			return Optional.of(factory.createVariableRef(cfiVar));
		}
	}
}