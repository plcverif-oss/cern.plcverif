/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.nusmv;

import java.nio.file.Path;

import cern.plcverif.base.common.settings.specific.AbstractSpecificSettings;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsElement;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory;

public class NusmvSettings extends AbstractSpecificSettings {
	public enum NusmvAlgorithm {
		/**
		 * Uses the classic algorithms from NuSMV, i.e., it performs the model checking using the {@code check_ctlspec} or {@code check_ltlspec} commands. 
		 */
		Classic,
		
		/**
		 * Uses the IC3 k-liveness algorithm from nuXmv, i.e., it performs the model checking using the {@code check_ltlspec_klive} command. Does not support CTL model checking. 
		 */
		Ic3,
		
		/**
		 * Uses the BMC algorithm from nuXmv, i.e., it performs the model checking using the {@code check_ltlspec_klive} command. Does not support CTL model checking. 
		 */
		Bmc,
	}
	
	public static final String TIMEOUT = "timeout";
	@PlcverifSettingsElement(name = TIMEOUT, description = "Timeout of the verification backend, in seconds.", mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private int timeoutInSec = 0;
	
	public static final String BINARY_PATH = "binary_path";
	@PlcverifSettingsElement(name = BINARY_PATH, description = "Full path of the NuSMV/nuXmv binary.")
	private Path nuxmvBinaryPath = null;
	
	public static final String ALGORITHM = "algorithm";
	@PlcverifSettingsElement(name = ALGORITHM, description = "Selects the algorithm (family) to be used by NuSMV.", mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private NusmvAlgorithm algorithm = NusmvAlgorithm.Classic;
	
	public static final String DYNAMIC = "dynamic";
	@PlcverifSettingsElement(name = DYNAMIC, description = "Enables dynamic reordering of variables in the backend.", mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private boolean dynamicVarReordering = true;
	
	public static final String DF = "df";
	@PlcverifSettingsElement(name = DF, description = "Disables the computation of the set of reachable states.", mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private boolean disableReachableStateComputation = true;
	
	public static final String REQ_AS_INVAR = "req_as_invar";
	@PlcverifSettingsElement(name = REQ_AS_INVAR, description = "Enables the representation of the requirements as invariants, if possible.", mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private boolean reqAsInvar = false;
	
	public static final String LOCREF_REQ_STRATEGY = "locref_req_strategy";
	@PlcverifSettingsElement(name = LOCREF_REQ_STRATEGY, description = "Use location reference in the requirement (instead of variable-based cycle end detection).", mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private boolean locrefReqStrategy = false;
	
	
	public int getTimeoutInSec() {
		return timeoutInSec;
	}
	
	public Path getNuxmvBinaryPath() {
		return nuxmvBinaryPath;
	}
	
	public boolean isDf() {
		return disableReachableStateComputation;
	}
	
	public boolean isDynamic() {
		return dynamicVarReordering;
	}
	
	public NusmvAlgorithm getAlgorithm() {
		return algorithm;
	}
	
	public boolean isReqAsInvar() {
		return reqAsInvar;
	}
	
	public boolean isLocrefReqStrategy() {
		return locrefReqStrategy;
	}
}
