/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.library.backend.nusmv.model

import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance
import cern.plcverif.base.models.expr.BinaryArithmeticExpression
import cern.plcverif.base.models.expr.BinaryArithmeticOperator
import cern.plcverif.base.models.expr.ExprFactory
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.ExpressionSafeFactory
import cern.plcverif.base.models.expr.FloatLiteral
import cern.plcverif.base.models.expr.FloatType
import cern.plcverif.base.models.expr.FloatWidth
import cern.plcverif.base.models.expr.IntType
import cern.plcverif.base.models.expr.Type
import cern.plcverif.base.models.expr.TypeConversion
import cern.plcverif.library.backend.nusmv.exception.NusmvModelGenerationException
import com.google.common.base.Preconditions
import java.util.Collection
import org.eclipse.emf.ecore.util.EcoreUtil
import cern.plcverif.base.models.expr.ExplicitlyTyped
import cern.plcverif.base.models.cfa.transformation.ICfaInstanceTransformation

/**
 * Transformation that replaces each floating point literal, type 
 * and operation with corresponding fixed point representations in a given CFA instance. 
 * The precision can be configured.
 * 
 * After this transformation the given CFA should not contain any expression with FloatType.
 * 
 * TODO: this may have to be registered in some trace model
 */
final class RemoveFloatsFromCfa implements ICfaInstanceTransformation {
	/**
	 * Default precision for fixed-point representation of floating point numbers.
	 * Each floating point number will be represented as round(FLOATING * precision) 
	 * integer. 
	 */
	static final long DEFAULT_FLOAT_PRECISION = 1000;
	static final ExpressionSafeFactory FACTORY = ExpressionSafeFactory.INSTANCE;

	/**
	 * Precision for the current transformation
	 */
	long precision;

	new() {
		this(DEFAULT_FLOAT_PRECISION);
	}

	new(long precision) {
		this.precision = precision;
	}

	/**
	 * Performs the replacement of floating point values and types with 
	 * corresponding fixed point values and types using the default precision.
	 */
	def static execute(CfaNetworkInstance cfa) {
		new RemoveFloatsFromCfa(DEFAULT_FLOAT_PRECISION).executeInternal(cfa);
	}

	/**
	 * Performs the replacement of floating point values and types with 
	 * corresponding fixed point values and types using the given precision 
	 * ({@code precision}).
	 */
	def static execute(CfaNetworkInstance cfa, long precision) {
		new RemoveFloatsFromCfa(precision).executeInternal(cfa);
	}

	private def boolean executeInternal(CfaNetworkInstance cfa) {
		var changed = false;
		
		// Collect all integer-to-float and float-to-integer conversions
		val intToFloatConversions = EmfHelper.getAllContentsOfType(cfa, TypeConversion, false, [it |
			it.operand.type instanceof IntType && it.type instanceof FloatType
		]);
		val floatToIntConversions = EmfHelper.getAllContentsOfType(cfa, TypeConversion, false, [it |
			it.operand.type instanceof FloatType && it.type instanceof IntType
		]);

		// Collect all expressions that had FloatType. This will be useful later when
		// modifications are required due to the fixed point representations.
		val floatExpressions = EmfHelper.getAllContentsOfType(cfa, Expression,
			false, [it|it.type instanceof FloatType]);

		// (1) Replace each FloatLiteral with a corresponding IntLiteral
		for (floatLiteral : EmfHelper.getAllContentsOfType(cfa, FloatLiteral, false)) {
			val fixedLiteral = FACTORY.createIntLiteral(
				Math.round(floatLiteral.value * precision),
				getRepresentingFixedPointType(floatLiteral.type)
			);

			// Even though 'fixedLiteral' has an IntType, it represents 
			// an originally floating-point number.
			floatExpressions.add(fixedLiteral);
			EcoreUtil.replace(floatLiteral, fixedLiteral);
			changed = true;
		}
		Preconditions.checkState(EmfHelper.getAllContentsOfType(cfa, FloatLiteral, false).isEmpty);

		// (2) Change the type of each variable with FloatType to the appropriate IntType.
		for (variable : cfa.variables) {
			if (variable.type instanceof FloatType) {
				EcoreUtil.replace(variable.type, getRepresentingFixedPointType(variable.type));
				changed = true;
			}
		}

		// (3) Fix the precision at TypeConversions
		// (3a) Int-to-float: replace the type and multiply by precision 
		for (typeConv : intToFloatConversions) {
			if (typeConv.type instanceof FloatType) {
				EcoreUtil.replace(typeConv.type, getRepresentingFixedPointType(typeConv.type));
				// floatMadeFixed.add(typeConv);
				Preconditions.checkState(floatExpressions.contains(typeConv));
				multiplyByPrecision(typeConv, floatExpressions);
				changed = true;
			}
		}

		// (3b) Float-to-int: divide by precision
		for (typeConv : floatToIntConversions) {
			divideByPrecision(typeConv.operand);
			changed = true;
		}

		// (4) Fix the parent expressions of ex-floats: there can be an issue 
		// with multiplicative expressions.
		
		// (4a) Check all multiplication (*) expressions: they should be divided by
		// 'precision' if both arguments were floats.
		for (multiplication : EmfHelper.getAllContentsOfType(
			cfa,
			BinaryArithmeticExpression,
			false,
			[it|it.operator == BinaryArithmeticOperator.MULTIPLICATION]
		)) {
			if (floatExpressions.contains(multiplication.leftOperand) &&
				floatExpressions.contains(multiplication.rightOperand)) {
				// If both operands are floats:
				// ((a * PREC) * (b * PREC)) / PREC = (a * b) * PREC 
				divideByPrecision(multiplication);
				changed = true;
			}
		}

		// (4b) Check all division () expressions: they should be multiplied by
		// 'precision' if both arguments were floats.
		for (divExpr : EmfHelper.getAllContentsOfType(
			cfa,
			BinaryArithmeticExpression,
			false,
			[ it |
				it.operator == BinaryArithmeticOperator.DIVISION ||
					it.operator == BinaryArithmeticOperator.INTEGER_DIVISION
			]
		)) {
			if (floatExpressions.contains(divExpr.leftOperand) && floatExpressions.contains(divExpr.rightOperand)) {
				// If both operands are floats:
				// ((a * PREC) / (b * PREC)) * PREC = (a / b) * PREC 
				multiplyByPrecision(divExpr.leftOperand, floatExpressions);
				changed = true;
			}
		}

		// TODO: raise exception at PowerExpression, bitshifts and other unsupported features.
		// (5) Some explicitly typed values (e.g. nondeterministic) may still have FloatTypes. Change them now.
		for (expr : EmfHelper.getAllContentsOfType(cfa, ExplicitlyTyped, false, [it | it.type instanceof FloatType])){
			expr.type = getRepresentingFixedPointType(expr.type);
			changed = true;
		}
		
		
		// Hopefully there are no more float types.
		val floatTypedExprs = EmfHelper.getAllContentsOfType(cfa, Expression, false, [it|it.type instanceof FloatType]);
		Preconditions.checkState(floatTypedExprs.empty, "There should not be any float expression after RemoveFloats.");
		return changed;
	}

	/**
	 * Replaces the given {@code expr} with "{@code expr} / {@code precision}" in the CFA.
	 */
	private def void divideByPrecision(Expression expr) {
		val divByPrecision = ExprFactory.eINSTANCE.createBinaryArithmeticExpression();
		divByPrecision.operator = BinaryArithmeticOperator.DIVISION;
		divByPrecision.rightOperand = FACTORY.createIntLiteral(precision, EcoreUtil.copy(expr.type) as IntType);

		EcoreUtil.replace(expr, divByPrecision);
		divByPrecision.leftOperand = expr;
	}

	/**
	 * Replaces the given {@code expr} with "{@code expr} * {@code precision}" in the CFA.
	 * The newly created expression represents a floating point number, thus it will be added 
	 * to the given {@code floatExpressions} collection.
	 */
	private def void multiplyByPrecision(Expression expr, Collection<Expression> floatExpressions) {
		val mulByPrecision = ExprFactory.eINSTANCE.createBinaryArithmeticExpression();
		mulByPrecision.operator = BinaryArithmeticOperator.MULTIPLICATION;
		mulByPrecision.rightOperand = FACTORY.createIntLiteral(precision, EcoreUtil.copy(expr.type) as IntType);

		EcoreUtil.replace(expr, mulByPrecision);
		mulByPrecision.leftOperand = expr;

		// The newly created expression node 'mulByPrecision' represents a floating point number, 
		// thus shall be added to the appropriate collection.
		floatExpressions.add(mulByPrecision);
	}

	/**
	 * Returns the {@link IntType} that will represent the given {@code floatType} 
	 * floating point type.
	 */
	private static def IntType getRepresentingFixedPointType(Type floatType) {
		if (floatType instanceof FloatType) {
			return getRepresentingFixedPointType(floatType);
		}
		throw new NusmvModelGenerationException("Unknown float type: " + floatType);
	}

	/**
	 * Returns the {@link IntType} that will represent the given {@code floatType} 
	 * floating point type.
	 */
	private static def IntType getRepresentingFixedPointType(FloatType floatType) {
		switch (floatType.bits) {
			case FloatWidth.B16: return ExpressionSafeFactory.INSTANCE.createIntType(true, 32)
			case FloatWidth.B32: return ExpressionSafeFactory.INSTANCE.createIntType(true, 64)
			case FloatWidth.B64: return ExpressionSafeFactory.INSTANCE.createIntType(true, 64)
		}
		throw new NusmvModelGenerationException("Unknown float width: " + floatType.bits);
	}
	
	override transformCfi(CfaNetworkInstance cfaToTransform) {
		return executeInternal(cfaToTransform);
	}
}
