/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.nusmv.parser.counterexample;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class NusmvCounterexample {
	private final List<NusmvCexStep> states = new ArrayList<>();

	public void addState(NusmvCexStep value) {
		this.states.add(value);
	}

	public List<NusmvCexStep> getStates() {
		return Collections.unmodifiableList(this.states);
	}

	public void removeState(NusmvCexStep state) {
		this.states.remove(state);
	}

	public int getStepsCount() {
		return this.states.size();
	}

	public Collection<String> collectAllVariables() {
		HashSet<String> variables = new HashSet<>();

		for (NusmvCexStep step : this.states) {
			for (VariableValuePair vvp : step.getValues()) {
				variables.add(vvp.getVariableName());
			}
		}
		return variables;
	}
}
