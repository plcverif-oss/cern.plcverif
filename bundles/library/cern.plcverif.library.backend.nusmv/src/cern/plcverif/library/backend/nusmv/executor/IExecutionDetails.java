/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.nusmv.executor;

import java.io.File;
import java.nio.file.Path;
import java.util.List;

import cern.plcverif.library.backend.nusmv.exception.NusmvExecutionException;

public interface IExecutionDetails {
	/**
	 * Returns the content for the NuSMV execution script.
	 * @param cexFile The path where the counterexample file shall be written. 
	 * @return The content for the NuSMV execution script.
	 * @throws NusmvExecutionException if it is not possible to generate an execution script with the given data.
	 */
	public String getExecScriptContent(File cexFile) throws NusmvExecutionException;
	
	public String getBackendConfigurationId();

	public Path getBinaryLocation();

	public List<String> getCommandLineOptions(String scriptPath, String modelPath);
}
