/*******************************************************************************
 * (C) Copyright CERN 2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jean-Charles Tournier - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.requirement.divbyzero;

import static cern.plcverif.verif.requirement.PlcCycleRepresentation.representPlcCycle;

import java.util.List;
import java.util.function.Predicate;

import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.formattedstring.BasicFormattedString;
import cern.plcverif.base.common.formattedstring.Formats.ItalicFormat;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.exceptions.SettingsSerializerException;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.interfaces.IParserLazyResult;
import cern.plcverif.base.interfaces.data.result.StageStatus;
import cern.plcverif.base.interfaces.exceptions.AtomParsingException;
import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory;
import cern.plcverif.base.models.cfa.cfabase.AssertionAnnotation;
import cern.plcverif.base.models.cfa.cfabase.AutomatonBase;
import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfabase.Transition;
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.transformation.assertion.AssertInliner;
import cern.plcverif.base.models.cfa.transformation.assertion.AssertInliningResult;
import cern.plcverif.base.models.cfa.transformation.assertion.AssertInliningStrategy;
import cern.plcverif.base.models.cfa.utils.CfaUtils;
import cern.plcverif.base.models.expr.BinaryArithmeticExpression;
import cern.plcverif.base.models.expr.BinaryArithmeticOperator;
import cern.plcverif.base.models.expr.ComparisonOperator;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.IntType;
import cern.plcverif.library.requirement.divbyzero.impl.DivByZeroAssertDiagnosisPrinter;
import cern.plcverif.verif.interfaces.IRequirement;
import cern.plcverif.verif.interfaces.data.RequirementRepresentationStrategy;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.VerificationResult;
import cern.plcverif.verif.requirement.PlcCycleRepresentation;
import cern.plcverif.verif.requirement.PlcCycleRepresentation.FieldsInCategories;
import cern.plcverif.verif.utils.diagnoser.AssertDiagnoser;
import cern.plcverif.verif.utils.diagnoser.IntAssertDiagnoser;
import cern.plcverif.verif.utils.requirement.AssertRequirementUtil;

public class DivByZeroRequirement implements IRequirement {
	private static final CfaDeclarationSafeFactory FACTORY = CfaDeclarationSafeFactory.INSTANCE;

	private final DivByZeroRequirementSettings settings;

	/** Assertion diagnoser created based on the selected strategy. */
	private AssertDiagnoser diagnoser = null;

	/** Result of the assertion inlining. */
	private AssertInliningResult assertionInliningResult = null;

	public DivByZeroRequirement(DivByZeroRequirementSettings settings) {
		Preconditions.checkNotNull(settings, "settings");

		this.settings = settings;
	}

	@Override
	public SettingsElement retrieveSettings() {
		try {
			SettingsElement ret = SpecificSettingsSerializer.toGenericSettings(settings);
			ret.setValue(DivByZeroRequirementExtension.CMD_ID);
			return ret;
		} catch (SettingsSerializerException | SettingsParserException e) {
			throw new PlcverifPlatformException(
					"Error occurred during the retrieval of the division by zero requirement's settings.", e);
		}
	}

	/**
	 * {@inheritDoc} This method performs the following steps:
	 * <ul>
	 * <li>Loads and resolves the list of fields treated as inputs, parameters and
	 * the eventual input bindings,
	 * <li>Explores the variables which should be represented as inputs (even if
	 * they are not explicitly mentioned in the given verification problem),
	 * <li>Represents the PLC scan cycle (i.e. it encloses the main automaton in an
	 * infinite loop),
	 * <li>Creates an {@link Expression} representing the described property as a
	 * temporal logic expression and adds it to the given
	 * {@link VerificationProblem},
	 * </ul>
	 */
	@Override
	public void fillVerificationProblem(VerificationProblem verifProblem, IParserLazyResult parserResult,
			VerificationResult result, RequirementRepresentationStrategy reqRepresentation) {
		Preconditions.checkNotNull(verifProblem, "verifProblem");
		Preconditions.checkNotNull(parserResult, "parserResult");
		Preconditions.checkNotNull(result, "result");
		Preconditions.checkNotNull(reqRepresentation, "reqRepresentation");

		try {
			CfaNetworkDeclaration cfa = (CfaNetworkDeclaration) verifProblem.getModel();
			FieldsInCategories fields = PlcCycleRepresentation.categorizeFields(cfa, parserResult, settings,
					result.currentStage());
			transformModel(verifProblem, result, fields);
			AssertRequirementUtil.createTemporalRequirement(verifProblem, result, reqRepresentation,
					assertionInliningResult);
			verifProblem.setRequirementDescription(new BasicFormattedString("No division by zero error occurs."));
		} catch (AtomParsingException exception) {
			result.currentStage().logError(exception.getMessage(), exception);
			result.currentStage().setStatus(StageStatus.Unsuccessful);
			throw new PlcverifPlatformException(exception.getMessage(), exception);
		}

	}

	@Override
	public void diagnoseResult(VerificationProblem verifProblem, VerificationResult result) {
		if (diagnoser == null) {
			result.setResultDiagnosis(new BasicFormattedString("No diagnosis is available.", ItalicFormat.INSTANCE));
		} else {
			diagnoser.diagnoseResult(verifProblem, result);
		}
	}

	/**
	 * Transform the initial CFA to identify the division expressions and add the
	 * needed assertions, to check that the dividend is never equals to 0
	 * 
	 * The main idea behind the model transformation is if there is a division
	 * between the location A-->B of the cfa the function will create an additional
	 * location A' and 're-wire' the cfa such that it becomes A-->A'-->B Then on the
	 * location A' an assertion annotation is created to check that the dividend of
	 * the division is not 0 Finally, the assignments which where done between A and
	 * B are moved to A'-->B. The transition between A-->A' is either empty or
	 * contains the original guard present between A-->B
	 * 
	 */
	private void transformModel(VerificationProblem data, VerificationResult result, FieldsInCategories fields) {

		Preconditions.checkArgument(data.getModel() instanceof CfaNetworkDeclaration,
				"Requirement expected a CFA network declaration but instead got " + data.getModel());

		// First we need to remove all the assertion defined by the user in the
		// code itself
		filterUserAssertions(data, result);

		CfaNetworkDeclaration cfa = (CfaNetworkDeclaration) data.getModel();

		// Create the predicates for the DIVISION operator
		Predicate<BinaryArithmeticExpression> predicateDivision = (
				expression) -> expression.getOperator() == BinaryArithmeticOperator.DIVISION;
		Predicate<BinaryArithmeticExpression> predicateIntegerDivision = (
				expression) -> expression.getOperator() == BinaryArithmeticOperator.INTEGER_DIVISION;
		Predicate<BinaryArithmeticExpression> predicateDivisionAll;

		try {
			predicateDivisionAll = predicateDivision.or(predicateIntegerDivision);
		} catch (NullPointerException e) {
			throw new PlcverifPlatformException("Error occurred during the identification of division expression.", e);
		}

		// Get all arithmetic expressions (the divisions are filtered by the
		// predicate)
		List<BinaryArithmeticExpression> expressions = EmfHelper.getAllContentsOfType(data.getModel(),
				BinaryArithmeticExpression.class, false, predicateDivisionAll);

		for (BinaryArithmeticExpression expression : expressions) {
			// Get the first EMF container of the expression of type Transition
			Transition transition = EmfHelper.getContainerOfType(expression, Transition.class);
			if (transition == null) {
				// The expression is not a transition i.e. not part of the
				// automaton
				continue;
			}

			// Get the right part of the expression
			Expression eRight = expression.getRightOperand();

			// Create the expression which will be used for the assertion
			Expression expDividendNotZero = createExpressionDivisorNotZero(eRight);

			// Create the new location with annotation
			Location newLocation = createNewLocation(transition.getTarget().getName() + "_divByZero",
					transition.getParentAutomaton());
			addAnnotationToLocation(expDividendNotZero, newLocation);

			// "Rewire" the transition between the original and source locations
			// to go through the new location
			insertLocation(transition, newLocation, cfa);
		}

		// Finally transform the CFA to inline the assertion annotations
		this.assertionInliningResult = AssertInliner.transform(cfa, AssertInliningStrategy.INT_STRATEGY);
		result.getAdditionalVariables().add(assertionInliningResult.getErrorField());
		diagnoser = new IntAssertDiagnoser(assertionInliningResult,
				new DivByZeroAssertDiagnosisPrinter(assertionInliningResult));

		representPlcCycle(data, result, fields);
	}

	private Location createNewLocation(String locationName, AutomatonBase parentAutomaton) {
		return FACTORY.createLocation(locationName, parentAutomaton);
	}

	private void addAnnotationToLocation(Expression exp, Location location) {
		FACTORY.createAssertionAnnotation(location, exp);
	}

	/**
	 * 
	 * Given a transition and the new location, modifies the transition so that the
	 * original source points to the new location and the new location points to the
	 * original destination. The assignment made on the original transition is moved
	 * to the new transition between new location and original destination
	 * 
	 */
	private void insertLocation(Transition transition, Location newLocation, CfaNetworkDeclaration cfa) {
		Expression condition = FACTORY.createBoolLiteral(true);
		AutomatonDeclaration automatonDeclaration = cfa.getMainAutomaton();

		// create the new transition
		FACTORY.createAssignmentTransition(transition.getDisplayName()+"_divByZeroReq", automatonDeclaration, transition.getSource(), newLocation, condition);

		// Reassign the target of the original transition to the new location
		transition.setSource(newLocation);

	}

	/**
	 * Given a dividend, simply builds an expression to compare the dividend to the
	 * value 0 e.g. dividend=b, returns the expression b!=0
	 */
	private static Expression createExpressionDivisorNotZero(Expression dividend) {
		Expression eZero = FACTORY.createIntLiteral(0, (IntType) EcoreUtil.copy(dividend.getType()));
		Expression expDividendNotZero = FACTORY.createComparisonExpression(EcoreUtil.copy(dividend), eZero,
				ComparisonOperator.NOT_EQUALS);
		return expDividendNotZero;
	}

	/**
	 * Remove all assertion annotations corresponding the assertions entered by the
	 * user in the code directly (using //#ASSERT...)
	 */
	private void filterUserAssertions(VerificationProblem data, VerificationResult result) {
		List<AssertionAnnotation> assertions = EmfHelper.getAllContentsOfType(data.getModel(),
				AssertionAnnotation.class, false);

		for (AssertionAnnotation assertion : assertions) {
			CfaUtils.delete(assertion);
		}
	}
}
