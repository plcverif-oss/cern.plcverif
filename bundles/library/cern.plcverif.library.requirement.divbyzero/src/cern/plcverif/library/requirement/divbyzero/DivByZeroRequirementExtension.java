/*******************************************************************************
 * (C) Copyright CERN 2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jean-Charles Tournier - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.requirement.divbyzero;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.help.SettingsHelp;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.verif.interfaces.IRequirementExtension;

public class DivByZeroRequirementExtension implements IRequirementExtension {
	public static final String CMD_ID = "divbyzero";

	@Override
	public DivByZeroRequirement createRequirement() {
		return new DivByZeroRequirement(new DivByZeroRequirementSettings());
	}

	@Override
	public DivByZeroRequirement createRequirement(SettingsElement settings) throws SettingsParserException {
		Preconditions.checkNotNull(settings, "settings");

		DivByZeroRequirementSettings specificSettings = SpecificSettingsSerializer.parse(settings,
				DivByZeroRequirementSettings.class);
		return new DivByZeroRequirement(specificSettings);
	}

	@Override
	public void fillSettingsHelp(SettingsHelp help) {
		help.addPluginDetails(CMD_ID, "Division by zero requirement",
				"Represents the division by zero requirement based on the division operator found in the code.",
				DivByZeroRequirementExtension.class);
		SpecificSettingsSerializer.fillSettingsHelp(help, DivByZeroRequirementSettings.class);
	}
}
