/*******************************************************************************
 * (C) Copyright CERN 2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jean-Charles Tournier - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.requirement.divbyzero.impl;

import java.util.Optional;

import cern.plcverif.base.common.formattedstring.BasicFormattedString;
import cern.plcverif.base.common.formattedstring.Formats.ItalicFormat;
import cern.plcverif.base.models.cfa.textual.CfaToString;
import cern.plcverif.base.models.cfa.transformation.assertion.AssertInliningResult;
import cern.plcverif.base.models.cfa.transformation.assertion.AssertInliningResult.InlinedAssertion;
import cern.plcverif.verif.utils.diagnoser.AssertDiagnosisPrinter;

/**
 * Class that produces human-readable diagnosis for a verification execution
 * with division by zero requirement.
 */
public class DivByZeroAssertDiagnosisPrinter extends AssertDiagnosisPrinter {
	public DivByZeroAssertDiagnosisPrinter(AssertInliningResult assertionInliningResult) {
		super(assertionInliningResult);
	}

	@Override
	public void appendAssertionViolatedMessage(Optional<InlinedAssertion> assertion, int cexStep,
			BasicFormattedString previousDiagnosis) {
		String currentCexStepName = String.format("Cycle %s", Integer.toString(cexStep + 1));

		if (assertion.isPresent()) {
			// We know which assertion has been violated.

			previousDiagnosis.append("The divisor not zero '");
			previousDiagnosis.append(CfaToString.INSTANCE.toString(assertion.get().getAssertionInvariant()).toString());
			previousDiagnosis.append("' ");
			if (assertion.get().getTag().isPresent()) {
				previousDiagnosis.append(String.format("(tag: %s) ", assertion.get().getTag().orElse("-")));
			}
			previousDiagnosis.append(String.format("requirement has been violated in %s. ", currentCexStepName));
		} else {
			// We do not know which assertion has been violated. This is not a
			// typical case for INT strategy.

			previousDiagnosis.append(String.format(
					"A division by zero occurred in %s, but it is not possible to determine where in the code.",
					currentCexStepName));
		}
	}

	@Override
	public BasicFormattedString noDiagnosisAvailable() {
		return new BasicFormattedString("No diagnosis is available.", ItalicFormat.INSTANCE);
	}
}
