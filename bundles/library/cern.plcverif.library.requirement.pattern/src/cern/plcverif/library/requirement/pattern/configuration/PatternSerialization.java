/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.requirement.pattern.configuration;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import cern.plcverif.library.requirement.pattern.configuration.jaxb.Patterns;

public final class PatternSerialization {
	public static final String PATTERNS_JAXB_PACKAGE_NAME = "cern.plcverif.library.requirement.pattern.configuration.jaxb";

	private PatternSerialization() {
		// Utility class.
	}
	
	public static class PatternLoadingException extends Exception {
		private static final long serialVersionUID = 6880956715818948015L;

		public PatternLoadingException(String message) {
			super(message);
		}

		public PatternLoadingException(String message, Throwable inner) {
			super(message, inner);
		}
	}

	public static Patterns loadFromXml(String xml) throws PatternLoadingException {
		try {
			StringReader stringReader = new StringReader(xml);

			JAXBContext jc = JAXBContext.newInstance(cern.plcverif.library.requirement.pattern.configuration.jaxb.Patterns.class.getPackageName(),
					cern.plcverif.library.requirement.pattern.configuration.jaxb.Patterns.class.getClassLoader());
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			Object result = unmarshaller.unmarshal(stringReader);

			if (result instanceof Patterns) {
				return (Patterns) result;
			} else {
				throw new PatternLoadingException(
						"The root of the loaded pattern definition XML is not a '<patterns>' tag.");
			}
		} catch (JAXBException je) {
			throw new PatternLoadingException("Error occured while parsing the given pattern definition XML.", je);
		}
	}

	/**
	 * Returns a basic human-readable HTML representation of the given pattern
	 * registry.
	 * 
	 * It uses the {@link PatternRegistry#getOriginalXml()} to fetch the XML
	 * representation, then it uses an XSLT transformation to produce the HTML.
	 * Therefore a pattern registry without original XML set will result in
	 * unexpected result.
	 * 
	 * @param patternRegistry
	 *            registry to be represented
	 * @return HTML content
	 */
	public static String asHtml(PatternRegistry patternRegistry) throws TransformerException {
		Source xslt = new StreamSource(
				PatternSerialization.class.getClassLoader().getResourceAsStream("/transform-patterns-to-html.xslt"));
		Source text = new StreamSource(new StringReader(patternRegistry.getOriginalXml()));
		StringWriter outputWriter = new StringWriter();

		TransformerFactory factory = TransformerFactory.newInstance();
		factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
		Transformer transformer = factory.newTransformer(xslt);
		transformer.transform(text, new StreamResult(outputWriter));

		return outputWriter.toString();
	}
}
