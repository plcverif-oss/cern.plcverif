/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.requirement.pattern;

import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsSerializer;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.help.SettingsHelp;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory;
import cern.plcverif.library.requirement.pattern.configuration.PatternRegistry;
import cern.plcverif.library.requirement.pattern.configuration.PatternSerialization.PatternLoadingException;
import cern.plcverif.verif.interfaces.IRequirementExtension;
import cern.plcverif.verif.requirement.AbstractRequirementSettings;

public class PatternRequirementExtension implements IRequirementExtension {
	public static final String CMD_ID = "pattern";

	@Override
	public PatternRequirement createRequirement() {
		try {
			return createRequirement(null);
		} catch (SettingsParserException e) {
			throw new PlcverifPlatformException(
					"Unable to load the default settings for the 'requirement.pattern' plug-in.", e);
		}
	}

	@Override
	public PatternRequirement createRequirement(SettingsElement settings) throws SettingsParserException {
		// Load default settings
		SettingsElement compositeSettings = SettingsSerializer
				.loadDefaultSettings("/settings", CMD_ID, this.getClass().getClassLoader()).toSingle();
		if (settings != null) {
			compositeSettings.overrideWith(settings);
		}

		// Parse to plugin-specific format
		PatternRequirementSettings patternSettings = SpecificSettingsSerializer.parse(compositeSettings,
				PatternRequirementSettings.class);

		try {
			return new PatternRequirement(patternSettings);
		} catch (PatternLoadingException e) {
			throw new SettingsParserException("Unable to load the given pattern definition file.", e);
		}
	}

	@Override
	public void fillSettingsHelp(SettingsHelp help) {
		help.addPluginDetails(CMD_ID, "Pattern requirement",
				"Represents the requirement based on a selected and filled requirement pattern.",
				PatternRequirementExtension.class);
		help.addSettingItem(PatternRequirementSettings.PATTERN_FILE,
				"Pattern definition XML file to be used. Leave empty to use the built-in patterns.", Path.class,
				PlcverifSettingsMandatory.OPTIONAL, null);
		help.addSettingItem(PatternRequirementSettings.PATTERN_ID,
				"ID of the requirement pattern to be used. (The given permitted values do not apply if not the default patterns are used.)",
				String.class, PlcverifSettingsMandatory.MANDATORY, defaultPatternIds());
		help.addSettingItem(PatternRequirementSettings.PATTERN_PARAMS,
				"Parameters to fill the selected pattern. The required number of parameters depends on the selected pattern.",
				String.class, PlcverifSettingsMandatory.MANDATORY, null);

		// The settings specific to this plug-in (i.e., not in
		// AbstractRequirementSettings) are
		// represented manually to be able to include the pattern IDs as
		// permitted values.
		SpecificSettingsSerializer.fillSettingsHelp(help, AbstractRequirementSettings.class);
	}

	private static List<String> defaultPatternIds() {
		try {
			return PatternRegistry.getDefaultPatternRegistry().getAllPatterns().stream().map(it -> it.getId())
					.collect(Collectors.toList());
		} catch (PatternLoadingException e) {
			return Collections.emptyList();
		}
	}
}
