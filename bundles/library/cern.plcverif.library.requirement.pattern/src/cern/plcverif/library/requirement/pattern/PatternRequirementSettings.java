/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.requirement.pattern;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsElement;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsList;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.library.requirement.pattern.configuration.PatternRegistry;
import cern.plcverif.library.requirement.pattern.configuration.PatternSerialization.PatternLoadingException;
import cern.plcverif.verif.requirement.AbstractRequirementSettings;

public class PatternRequirementSettings extends AbstractRequirementSettings {
	public static final String PATTERN_FILE = "pattern_file";
	@PlcverifSettingsElement(name = PATTERN_FILE, description = "Pattern definition XML file to be used. Leave empty to use the built-in patterns.", mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private Path patternDefinitionFile = null;

	public static final String PATTERN_ID = "pattern_id";
	@PlcverifSettingsElement(name = PATTERN_ID, description = "ID of the pattern to be checked.", mandatory = PlcverifSettingsMandatory.MANDATORY)
	private String patternId = null;

	public static final String PATTERN_PARAMS = "pattern_params";
	@PlcverifSettingsList(name = PATTERN_PARAMS, description = "Parameters (expressions) to be substituted in the placeholders of the pattern. The number of given parameters shall match the number of placeholders in the selected pattern.", mandatory = PlcverifSettingsMandatory.OPTIONAL, serializeIfEmpty = true)
	private List<String> patternParams = null;

	private Map<String, String> patternParamsMap = null;

	public String getPatternId() {
		return patternId;
	}

	public void setPatternId(String patternId) {
		this.patternId = patternId;
	}

	public Map<String, String> getPatternParams() {
		if (patternParamsMap == null) {
			patternParamsMap = new HashMap<>();
			if (patternParams == null || patternId.isEmpty()) {
				return patternParamsMap;
			}

			for (int i = 1; i < patternParams.size(); i++) {
				if (patternParams.get(i) != null) {
					patternParamsMap.put(Integer.toString(i), patternParams.get(i));
				}
			}
		}

		return patternParamsMap;
	}

	public void setPatternParams(Map<String, String> patternParams) {
		this.patternParamsMap = patternParams;
	}

	/**
	 * Creates a pattern registry containing the patterns from the given pattern
	 * file (or containing the default patterns if not set).
	 * 
	 * @return Pattern registry corresponding to the `pattern_file` setting.
	 */
	public PatternRegistry createPatternRegistry() throws PatternLoadingException {
		if (this.patternDefinitionFile == null || "".equals(this.patternDefinitionFile.toString())) {
			// The patterDefinitionFile may represent the Path "" too. 
			return PatternRegistry.getDefaultPatternRegistry();
		} else {
			try {
				String patternsXml = IoUtils.readFile(this.patternDefinitionFile);
				return PatternRegistry.create(patternsXml);
			} catch (IOException ex) {
				throw new PatternLoadingException(
						String.format("Unable to load the pattern definition file '%s'.", patternDefinitionFile), ex);
			}
		}
	}
}
