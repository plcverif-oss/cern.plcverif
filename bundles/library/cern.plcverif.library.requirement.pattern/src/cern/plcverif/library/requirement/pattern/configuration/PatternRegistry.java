/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.requirement.pattern.configuration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.library.requirement.pattern.configuration.PatternSerialization.PatternLoadingException;
import cern.plcverif.library.requirement.pattern.configuration.PatternUtils.PatternValidationException;
import cern.plcverif.library.requirement.pattern.configuration.jaxb.Pattern;
import cern.plcverif.library.requirement.pattern.configuration.jaxb.Patterns;
import cern.plcverif.library.requirement.pattern.configuration.jaxb.Tltype;

public final class PatternRegistry {
	private static PatternRegistry defaultPatternRegistry = null;
	private Map<String, Pattern> patternLookupByName = new HashMap<>();
	private List<Pattern> patterns = new ArrayList<>();
	private String originalXml = null;

	private PatternRegistry() {
	}

	/**
	 * Creates a pattern registry by parsing the given string containing the XML
	 * representation of the patterns.
	 */
	public static PatternRegistry create(String xml) throws PatternLoadingException {
		PatternRegistry ret = new PatternRegistry();
		Patterns content = PatternSerialization.loadFromXml(xml);
		ret.originalXml = xml;
		ret.store(content);
		return ret;
	}

	/**
	 * Returns the pattern registry instance representing the default patterns
	 * stored in the resources of the plug-in. The registry instance is cached,
	 * thus subsequent calls will return the same instance.
	 * 
	 * @return Default pattern registry
	 * @throws PatternLoadingException
	 *             if it is not possible to parse the default patterns
	 */
	public static PatternRegistry getDefaultPatternRegistry() throws PatternLoadingException {
		if (defaultPatternRegistry == null) {
			String patternsXml = IoUtils.readResourceFile("default-patterns.xml",
					PatternRegistry.class.getClassLoader());
			defaultPatternRegistry = PatternRegistry.create(patternsXml);
		}
		return defaultPatternRegistry;
	}

	private void store(Patterns patterns) {
		for (Pattern pattern : patterns.getPattern()) {
			if (patternLookupByName.containsKey(pattern.getId())) {
				throw new IllegalStateException("Duplicated pattern ID: " + pattern.getId());
			}

			patternLookupByName.put(pattern.getId(), pattern);
			this.patterns.add(pattern);
		}
	}

	public Optional<Pattern> getPattern(String patternId) {
		return Optional.ofNullable(patternLookupByName.get(patternId));
	}

	public Optional<Pattern> getPattern(Optional<String> patternId) {
		if (patternId.isPresent()) {
			return getPattern(patternId.get());
		} else {
			return Optional.empty();
		}
	}

	public List<Pattern> getAllPatterns() {
		return Collections.unmodifiableList(this.patterns);
	}

	public List<Pattern> getAllPatterns(Tltype type) {
		return this.patterns.stream().filter(it -> it.getTlexpr().getType() == type).collect(Collectors.toList());
	}
	
	String getOriginalXml() {
		return originalXml == null ? "" : originalXml;
	}
	
	/**
	 * Validates the patterns in this pattern registry using {@link PatternUtils#validate(Pattern)}.
	 * @throws PatternValidationException if at least one validation fails
	 */
	public void validate() throws PatternValidationException {
		for (Pattern pattern : patterns) {
			PatternUtils.validate(pattern);
		}
	}
}
