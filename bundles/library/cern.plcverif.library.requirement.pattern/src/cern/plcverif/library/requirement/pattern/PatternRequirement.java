/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.requirement.pattern;

import static cern.plcverif.verif.requirement.PlcCycleRepresentation.createBocField;
import static cern.plcverif.verif.requirement.PlcCycleRepresentation.createEocField;
import static cern.plcverif.verif.requirement.PlcCycleRepresentation.representPlcCycle;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.formattedstring.BasicFormattedString;
import cern.plcverif.base.common.formattedstring.Formats.BoldFormat;
import cern.plcverif.base.common.formattedstring.Formats.ItalicFormat;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.exceptions.SettingsSerializerException;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.interfaces.IParserLazyResult;
import cern.plcverif.base.interfaces.data.result.StageStatus;
import cern.plcverif.base.interfaces.exceptions.AtomParsingException;
import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.Field;
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef;
import cern.plcverif.base.models.expr.BeginningOfCycle;
import cern.plcverif.base.models.expr.EndOfCycle;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.exprparser.ExpressionParser;
import cern.plcverif.base.models.exprparser.exceptions.ExpressionParsingException;
import cern.plcverif.base.models.exprparser.exprLang.PatternParameter;
import cern.plcverif.library.requirement.pattern.configuration.PatternRegistry;
import cern.plcverif.library.requirement.pattern.configuration.PatternSerialization.PatternLoadingException;
import cern.plcverif.library.requirement.pattern.configuration.PatternUtils;
import cern.plcverif.library.requirement.pattern.configuration.jaxb.Pattern;
import cern.plcverif.library.requirement.pattern.exceptions.PatternRequirementException;
import cern.plcverif.library.requirement.pattern.exceptions.PatternRequirementRuntimeException;
import cern.plcverif.verif.interfaces.IRequirement;
import cern.plcverif.verif.interfaces.data.RequirementRepresentationStrategy;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.VerificationResult;
import cern.plcverif.verif.requirement.PlcCycleRepresentation;
import cern.plcverif.verif.requirement.PlcCycleRepresentation.FieldsInCategories;

public class PatternRequirement implements IRequirement {
	private final PatternRegistry patternRegistry;
	private final ExpressionParser exprParser = new ExpressionParser();
	private static final CfaDeclarationSafeFactory FACTORY = CfaDeclarationSafeFactory.INSTANCE;

	private PatternRequirementSettings settings;

	public PatternRequirement(PatternRequirementSettings settings) throws PatternLoadingException {
		this.settings = settings;
		this.patternRegistry = settings.createPatternRegistry();
	}

	public PatternRequirementSettings getSettings() {
		return settings;
	}

	@Override
	public SettingsElement retrieveSettings() {
		try {
			SettingsElement ret = SpecificSettingsSerializer.toGenericSettings(settings);
			ret.setValue(PatternRequirementExtension.CMD_ID);
			return ret;
		} catch (SettingsParserException | SettingsSerializerException e) {
			throw new PlcverifPlatformException("Unable to retreive the settings of the requirement.pattern plugin.");
		}
	}

	/**
	 * This method performs the following steps:
	 * <ul>
	 * <li>Loads and resolves the list of fields treated as inputs, parameters
	 * and the eventual input bindings,
	 * <li>Explores the variables which should be represented as inputs (even if
	 * they are not explicitly mentioned in the given verification problem),
	 * <li>Represents the PLC scan cycle (i.e. it encloses the main automaton in
	 * an infinite loop),
	 * <li>Creates an {@link Expression} representing the described property as
	 * a temporal logic expression and adds it to the given
	 * {@link VerificationProblem},
	 * </ul>
	 */
	@Override
	public void fillVerificationProblem(VerificationProblem data, IParserLazyResult parserResult,
			VerificationResult result, RequirementRepresentationStrategy reqRepresentation) {
		Preconditions.checkNotNull(data, "data");
		Preconditions.checkNotNull(parserResult, "parserResult");
		Preconditions.checkNotNull(result, "result");
		Preconditions.checkNotNull(reqRepresentation, "reqRepresentation");

		try {
			CfaNetworkDeclaration cfa = (CfaNetworkDeclaration) data.getModel();
			PatternPreprocessor.addPatternFields(settings.getPatternId(), cfa);
			FieldsInCategories fields = PlcCycleRepresentation.categorizeFields(cfa, parserResult, settings, result.currentStage());	
			representPlcCycle(data, result, fields);
			createTemporalRequirement(data, result, settings.getPatternId(), settings.getPatternParams(),
					reqRepresentation, parserResult);
			// TODO update settings with input variables?
		} catch (PatternRequirementException | PatternRequirementRuntimeException
				| ExpressionParsingException exception) {
			result.currentStage().setStatus(StageStatus.Unsuccessful);
			throw new PlcverifPlatformException(
					String.format("Problem occurred during the pattern-based requirement representation: %s",
							exception.getMessage()),
					exception);
		} catch (AtomParsingException exception) {
			result.currentStage().logError(exception.getMessage(), exception);
			result.currentStage().setStatus(StageStatus.Unsuccessful);
		}
	}
	
	@Override
	public void diagnoseResult(VerificationProblem verifProblem, VerificationResult result) {
		result.setResultDiagnosis(new BasicFormattedString("No diagnosis is available.", ItalicFormat.INSTANCE));
	}
	
	private void createTemporalRequirement(VerificationProblem verifProblem, VerificationResult result,
			String patternId, Map<String, String> patternAstParams, RequirementRepresentationStrategy reqRepresentation,
			IParserLazyResult parserResult) throws PatternRequirementException, ExpressionParsingException {

		// Find the corresponding pattern
		Optional<Pattern> pattern = patternRegistry.getPattern(patternId);
		if (pattern.isPresent()) {
			// Fill pattern
			String tlExprStr = PatternUtils.fillTemporalLogic(pattern.get(), patternAstParams);
			
			// Preprocess Stuff
			PatternPreprocessor.preprocessForPattern(pattern.get(), patternAstParams, verifProblem, parserResult);

			// Parse TL expression
			Expression tlExpr = exprParser.parseExpression(tlExprStr);
			

			// Check if there is any leftover PatternParameter in it
			Collection<PatternParameter> unresolvedParameters = EmfHelper.getAllContentsOfType(tlExpr,
					PatternParameter.class, true);
			if (!unresolvedParameters.isEmpty()) {
				throw new PatternRequirementException(
						String.format("Unresolved pattern parameters: %s. Known pattern parameters: %s.",
								unresolvedParameters.stream().map(it -> Integer.toString(it.getKey()))
										.collect(Collectors.joining(", ")),
								patternAstParams == null ? "null"
										: patternAstParams.entrySet().stream()
												.map(it -> String.format("%s=%s", it.getKey(), it.getValue()))
												.collect(Collectors.joining(", "))));
			}

			// Resolve all value placeholders (`ValuePlaceholder`)
			tlExpr = PatternUtils.resolveValuePlaceholders(tlExpr, parserResult);

			// Infer unknown types
			ExpressionParser.inferUnknownTypes(tlExpr);

			// Modify the TL expression according to the needed representation
			switch (reqRepresentation) {
			case EXPLICIT_WITH_LOCATIONREF: {
				// nothing to do for the moment
				break;
			}
			case IMPLICIT: {
				result.currentStage().logInfo(
						"IMPLICIT requirement representation strategy is not supported yet. Using EXPLICIT_WITH_VARIABLE instead.");
				// fall through on purpose
			}
			case EXPLICIT_WITH_VARIABLE: {
				// End of cycle
				List<EndOfCycle> eocPredicates = EmfHelper.getAllContentsOfType(tlExpr, EndOfCycle.class, true);
				Field eocField = createEocField(verifProblem, result);
				FieldRef eocFieldRef = FACTORY.createFieldRef(eocField);
				for (EndOfCycle eocPredicate : eocPredicates) {
					EcoreUtil.replace(eocPredicate, EcoreUtil.copy(eocFieldRef));
				}
				
				// Beginning of cycle
				List<BeginningOfCycle> bocPredicates = EmfHelper.getAllContentsOfType(tlExpr, BeginningOfCycle.class, true);
				Field bocField = createBocField(verifProblem, result);
				FieldRef bocFieldRef = FACTORY.createFieldRef(bocField);
				for (BeginningOfCycle bocPredicate : bocPredicates) {
					EcoreUtil.replace(bocPredicate, EcoreUtil.copy(bocFieldRef));
				}
				
				break;
			}
			default:
				throw new UnsupportedOperationException(
						"Unknown RequirementRepresentationStrategy: " + reqRepresentation);
			}

			verifProblem.setRequirement(tlExpr, reqRepresentation);
			verifProblem
					.setRequirementDescription(PatternUtils.fillFormatted(pattern.get().getHumanreadable(), patternAstParams, BoldFormat.INSTANCE));
		} else {
			throw new PatternRequirementException("Unable to find the following pattern: " + patternId);
		}
	}
}
