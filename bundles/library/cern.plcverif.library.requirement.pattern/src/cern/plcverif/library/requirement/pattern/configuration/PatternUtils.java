/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.requirement.pattern.configuration;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.common.formattedstring.BasicFormattedString;
import cern.plcverif.base.common.formattedstring.BasicFormattedString.Format;
import cern.plcverif.base.interfaces.IParserLazyResult;
import cern.plcverif.base.interfaces.exceptions.AtomParsingException;
import cern.plcverif.base.models.expr.AtomicExpression;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.utils.TlExprUtils;
import cern.plcverif.base.models.exprparser.ExpressionParser;
import cern.plcverif.base.models.exprparser.exceptions.ExpressionParsingException;
import cern.plcverif.base.models.exprparser.exprLang.PatternParameter;
import cern.plcverif.base.models.exprparser.exprLang.ValuePlaceholder;
import cern.plcverif.library.requirement.pattern.configuration.jaxb.Pattern;
import cern.plcverif.library.requirement.pattern.configuration.jaxb.Tltype;
import cern.plcverif.library.requirement.pattern.exceptions.PatternRequirementException;

public final class PatternUtils {
	private static final ExpressionParser EXPR_PARSER = new ExpressionParser();

	private PatternUtils() {
		// Utility class.
	}
	
	/**
	 * Returns a filled copy of the temporal logic expression of the given
	 * pattern, i.e. it replaces each {@code {key}} with the string that has
	 * been mapped to {@code key} in the map {@code placeholders}.
	 * 
	 * @param pattern
	 *            Pattern to be filled out
	 * @param placeholders
	 *            Mapping used for the replacement of placeholders
	 * @return String representation of the pattern's temporal logic expression,
	 *         without placeholders.
	 */
	public static String fillTemporalLogic(Pattern pattern, Map<String, String> placeholders) {
		return fill(pattern.getTlexpr().getValue(), placeholders);
	}

	/**
	 * Replaces the occurrences of {@code {<key>}} with the string mapped
	 * {@code key} in {@code mapping}.
	 */
	public static String fill(String patternStr, Map<String, String> mappings) {
		String ret = patternStr;

		for (Entry<String, String> mapping : mappings.entrySet()) {
			ret = ret.replace(keyToPlaceholder(mapping.getKey()), mapping.getValue());
		}

		return ret;
	}

	/**
	 * Replaces the occurrences of {@code {<key>}} with the string mapped
	 * {@code key} in {@code mapping}. The pattern parameters will have the
	 * given format.
	 */
	public static BasicFormattedString fillFormatted(String patternStr, Map<String, String> mappings,
			Format patternParamFormat) {
		BasicFormattedString ret = new BasicFormattedString();
		Matcher matcher = java.util.regex.Pattern.compile("([^\\{\\}]+|\\{(.*?)\\})").matcher(patternStr);
		while (matcher.find()) {
			String toAppend = matcher.group(1);
			if (toAppend.matches("\\{.*?\\}")) {
				// This is a placeholder, to be replaced and formatted
				String key = matcher.group(2);
				String mappedValue = mappings.getOrDefault(key, key + " with unknown mapping");
				ret.append(mappedValue, patternParamFormat);
			} else {
				ret.append(toAppend);
			}
		}

		return ret;
	}

	/**
	 * Translates the given key into placeholder format.
	 * 
	 * @param key
	 *            Key
	 * @return Placeholder
	 */
	private static String keyToPlaceholder(String key) {
		return String.format("{%s}", key);
	}

	public static Expression resolveValuePlaceholders(Expression tlExpr, IParserLazyResult parserResult)
			throws PatternRequirementException {
		// Special case if Expression itself is a ValuePlaceholder
		if (tlExpr instanceof ValuePlaceholder) {
			try {
				AtomicExpression parsedSubExpr = parserResult.parseAtom(((ValuePlaceholder)tlExpr).getValue());
				if (parsedSubExpr == null) {
					throw new PatternRequirementException(
							"The parser frontent was unable to parse the following atomic value: "
									+ ((ValuePlaceholder)tlExpr).getValue());
				}
				return parsedSubExpr;
			} catch (AtomParsingException exception) {
				throw new PatternRequirementException(exception.getMessage(), exception);
			}
		}
		for (ValuePlaceholder placeholder : EmfHelper.getAllContentsOfType(tlExpr, ValuePlaceholder.class, true)) {
			try {
				AtomicExpression parsedSubExpr = parserResult.parseAtom(placeholder.getValue());
				if (parsedSubExpr == null) {
					throw new PatternRequirementException(
							"The parser frontent was unable to parse the following atomic value: "
									+ placeholder.getValue());
				}
				EcoreUtil.replace(placeholder, parsedSubExpr);
			} catch (AtomParsingException exception) {
				throw new PatternRequirementException(exception.getMessage(), exception);
			}
		}
		return tlExpr;
	}

	/**
	 * Exception to be thrown in case the pattern validation
	 * ({@link PatternUtils#validate(Pattern)}) fails.
	 */
	public static class PatternValidationException extends Exception {
		private static final long serialVersionUID = -7102040170267557864L;

		public PatternValidationException(String message) {
			super(message);
		}

		public PatternValidationException(String message, Throwable throwable) {
			super(message, throwable);
		}
	}

	/**
	 * Validates the given pattern.
	 * 
	 * The following checks will be performed:
	 * <ul>
	 * <li>The temporal logic representation of the pattern can be parsed,
	 * <li>The temporal logic representation of the pattern refers only known
	 * parameters,
	 * <li>The type (CTL or LTL) of the temporal logic representation of the
	 * pattern matches the defined type,
	 * <li>All defined parameters are referred in the temporal logic
	 * representation of the pattern,
	 * <li>All defined parameters are referred in the human-readable
	 * representation of the pattern.
	 * </ul>
	 * 
	 * @param pattern
	 *            Pattern to be validated
	 * @throws PatternValidationException
	 *             if one of the validations fail
	 */
	public static void validate(Pattern pattern) throws PatternValidationException {
		Set<String> knownPatternKeys = pattern.getParameter().stream().map(it -> it.getKey())
				.collect(Collectors.toSet());

		// Parse TL expression
		try {
			Expression tlExpr = EXPR_PARSER.parseExpression(pattern.getTlexpr().getValue());
			for (EObject e : EmfHelper.toIterable(tlExpr.eAllContents())) {
				if (e instanceof PatternParameter) {
					String placeholderKey = Integer.toString(((PatternParameter) e).getKey());
					if (!knownPatternKeys.contains(placeholderKey)) {
						throw new PatternValidationException(String.format(
								"The temporal logic representation of pattern '%s' refers the unknown parameter '%s'.",
								pattern.getId(), placeholderKey));
					}
				}
			}

			// Check TL type
			if (pattern.getTlexpr().getType() == Tltype.CTL && !TlExprUtils.isCtl(tlExpr)) {
				throw new PatternValidationException(String.format(
						"The temporal logic representation of pattern '%s' is not a CTL expression as defined.",
						pattern.getId()));
			}
			if (pattern.getTlexpr().getType() == Tltype.LTL && !TlExprUtils.isLtl(tlExpr)) {
				throw new PatternValidationException(String.format(
						"The temporal logic representation of pattern '%s' is not a LTL expression as defined.",
						pattern.getId()));
			}

		} catch (ExpressionParsingException ex) {
			throw new PatternValidationException(String.format(
					"Not possible to parse the temporal logic representation of pattern '%s'.", pattern.getId()), ex);
		}

		// Check if keys are referred
		for (String key : knownPatternKeys) {
			if (!pattern.getHumanreadable().contains(keyToPlaceholder(key))) {
				throw new PatternValidationException(String.format(
						"The human-readable representation of pattern '%s' does not refer to the parameter with key %s.",
						pattern.getId(), key));
			}
			if (!pattern.getTlexpr().getValue().contains(keyToPlaceholder(key))) {
				throw new PatternValidationException(String.format(
						"The temporal logic representation of pattern '%s' does not refer to the parameter with key %s.",
						pattern.getId(), key));
			}
		}

	}
}
