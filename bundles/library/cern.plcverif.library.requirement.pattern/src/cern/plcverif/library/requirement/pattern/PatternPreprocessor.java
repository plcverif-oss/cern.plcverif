/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Xaver Fink - initial implementation
 *******************************************************************************/
package cern.plcverif.library.requirement.pattern;

import static org.eclipse.emf.ecore.util.EcoreUtil.copy;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cern.plcverif.base.interfaces.IParserLazyResult;
import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory;
import cern.plcverif.base.models.cfa.cfabase.DataDirection;
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructureRef;
import cern.plcverif.base.models.cfa.cfadeclaration.Field;
import cern.plcverif.base.models.cfa.utils.CfaDeclarationUtils;
import cern.plcverif.base.models.expr.ComparisonOperator;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.IntType;
import cern.plcverif.base.models.expr.Type;
import cern.plcverif.base.models.exprparser.ExpressionParser;
import cern.plcverif.base.models.exprparser.exceptions.ExpressionParsingException;
import cern.plcverif.library.requirement.pattern.configuration.PatternUtils;
import cern.plcverif.library.requirement.pattern.configuration.jaxb.Pattern;
import cern.plcverif.library.requirement.pattern.exceptions.PatternRequirementException;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.requirement.PlcCycleRepresentation;


public final class PatternPreprocessor {
	
	private static final ExpressionParser EXPR_PARSER = new ExpressionParser();
	
	private static CfaDeclarationSafeFactory factory = CfaDeclarationSafeFactory.INSTANCE;
	
	private PatternPreprocessor() {
		// Utility class.
	}
	
	/** Modifies the VerificationProblem and ParserResult as a preprocessing step for pattern requirements.
	 * The concrete modifications depend on the Pattern. 
	 * 
	 * 
	 * @param pattern
	 *            Pattern for which the preprocessing occurs
	 * @param verifProblem
	 *            Verification problem that is getting modified
	 * @return parserResult
	 *         ParserResult that is getting modified.
	 * @throws ExpressionParsingException 
	 * @throws PatternRequirementException 
	 */
	public static void preprocessForPattern(Pattern pattern, Map<String, String> patternAstParams, VerificationProblem verifProblem, IParserLazyResult parserResult) throws ExpressionParsingException, PatternRequirementException {
		
		CfaNetworkDeclaration cfaNetwork = (CfaNetworkDeclaration) verifProblem.getModel();
		
		List<Expression> patternParameterExpressions = new ArrayList<>();
		for(Map.Entry<String, String> entry : patternAstParams.entrySet()) {
			Expression expr = EXPR_PARSER.parseExpression(entry.getValue());
			expr = PatternUtils.resolveValuePlaceholders(expr, parserResult);
			ExpressionParser.inferUnknownTypes(expr);
			patternParameterExpressions.add(expr);
		}
				
		
		if (pattern.getId().equals("timed-trigger")) {
			AutomatonDeclaration timerAutomatonDeclaration = PlcCycleRepresentation.createTimerAutomaton(verifProblem, ((DataStructureRef)cfaNetwork.getMainContext().getType()).getDefinition(), patternParameterExpressions.get(0), patternParameterExpressions.get(1));
			PlcCycleRepresentation.insertPropertyAutomaton(cfaNetwork.getMainAutomaton(), timerAutomatonDeclaration, cfaNetwork.getMainContext(), verifProblem.getEocLocation().getIncoming().get(0));
		}
	}
	
	
	/** Adds fields neccessary for specific pattern types if they are not part of the parsed input files, e.g. T_CYCLE for timed patterns.
	 */
	public static void addPatternFields(String patternId, CfaNetworkDeclaration cfa) {
		//TODO: improve the pattern class to include helper functions such as isTimed() or getRequiredVariableList(), hardcoding this here is ugly
		if(patternId.equals("timed-trigger")) {
			Field tCycle = createFieldIfNotExist("T_CYCLE", factory.createIntType(true, 32), cfa);
			factory.createDirectionFieldAnnotation(tCycle, DataDirection.INPUT);
			factory.createFieldBound(tCycle, factory.createIntLiteral(5, (IntType) copy(tCycle.getType())), ComparisonOperator.GREATER_EQ);
			factory.createFieldBound(tCycle, factory.createIntLiteral(100, (IntType) copy(tCycle.getType())), ComparisonOperator.LESS_EQ);
		}
	}
	
	/** Adds fields neccessary for specific pattern types if they are not part of the parsed input files, e.g. T_CYCLE for timed patterns.
	 */
	private static Field createFieldIfNotExist(String name, Type type, CfaNetworkDeclaration cfa) {
		Field field = null;
		try {
			field = (Field) CfaDeclarationUtils.parseHierarchicalDataRef(name, cfa, (e -> e.getName()));
		} catch (Exception e) {
			field = factory.createField(name, cfa.getRootDataStructure(), type);
			factory.createInitialAssignment(field, factory.createDefaultLiteral(copy(field.getType())));
		}
		return field;
	}

}
