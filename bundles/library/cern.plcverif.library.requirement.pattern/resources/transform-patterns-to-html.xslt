<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<head><title>Requirement patterns overview</title></head>
			<body>
				<h1>Requirement patterns</h1>
				<xsl:for-each select="patterns/pattern">
					<h2><a><xsl:attribute name="name"><xsl:value-of select="@id"/></xsl:attribute><xsl:value-of select="description"/> (ID: <xsl:value-of select="@id"/>)</a></h2>
					<ul>
						<li><b>Informal requirement:</b>&#160;<xsl:value-of select="humanreadable"/></li>
						<li><b>Formal requirement:</b>&#160;<code><xsl:value-of select="tlexpr"/></code> (type: <xsl:value-of select="tlexpr/@type"/>)</li>
						<li><b>Parameters:</b>
							<ul>
								<xsl:for-each select="parameter">
									<li><b>{<xsl:value-of select="@key"/>}</b>:&#160;<xsl:value-of select="description"/></li>
								</xsl:for-each>
							</ul>
						</li>
					</ul>
				</xsl:for-each>
			</body>
		</html>
	</xsl:template>

</xsl:stylesheet>