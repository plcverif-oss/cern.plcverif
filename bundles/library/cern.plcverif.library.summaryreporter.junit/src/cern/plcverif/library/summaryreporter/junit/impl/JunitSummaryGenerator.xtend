/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.summaryreporter.junit.impl

import java.util.List
import cern.plcverif.verif.summary.extensions.parsernodes.VerificationMementoNode
import cern.plcverif.library.summaryreporter.junit.JunitSummaryReporterSettings
import com.google.common.base.Preconditions
import cern.plcverif.verif.interfaces.data.ResultEnum

class JunitSummaryGenerator {
	JunitSummaryReporterSettings settings;

	private new(JunitSummaryReporterSettings settings) {
		this.settings = settings;
	}

	static def represent(List<VerificationMementoNode> mementos, JunitSummaryReporterSettings settings) {
		return new JunitSummaryGenerator(settings).represent(mementos);
	}

	def represent(List<VerificationMementoNode> mementos) {
		Preconditions.checkNotNull(mementos, "mementos");
		return representInternal(mementos);
	}

	private def representInternal(List<VerificationMementoNode> mementos) '''
		<?xml version="1.0" encoding="UTF-8"?>
		<testsuite tests="«mementos.size»">
			«FOR memento : mementos »
				<testcase classname="«memento.getName»" name="«memento.getId»" time="«(memento.resultNode.runtimeMs as double) / 1000.0»">
					«IF handleAsVerificationFailure(memento.result)»
						<failure type="«memento.result.toString»">
							Verification result for requirement '«memento.infoNode.requirementText»' was «memento.result.toString.toUpperCase».
						</failure>  
					«ELSEIF memento.result != ResultEnum.Satisfied»
						<skipped/>
					«ENDIF»
					<system-out>
						Backend: «memento.infoNode.backendName»<br/>
						Backend algorithm: «memento.infoNode.backendAlgo»<br/>
						Requirement: «memento.infoNode.requirementText»<br/>
					</system-out>
				</testcase>
			«ENDFOR»
		</testsuite>
	'''

	private def boolean handleAsVerificationFailure(ResultEnum result) {
		switch (result) {
			case Satisfied: return false
			case Violated: return true
			case Unknown: return settings.isUnknownAsError
			case Error: return true
		}
		throw new UnsupportedOperationException("Unknown result type: " + result);
	}
}
