/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.summaryreporter.junit;

import cern.plcverif.base.common.settings.specific.AbstractSpecificSettings;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsElement;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory;

public class JunitSummaryReporterSettings extends AbstractSpecificSettings {
	@PlcverifSettingsElement(name = "unknown_as_error", 
			description = "Treat 'Unknown' results as 'Error'.", mandatory = PlcverifSettingsMandatory.OPTIONAL)
	boolean unknownAsError = false;
	
	public boolean isUnknownAsError() {
		return unknownAsError;
	}
}
