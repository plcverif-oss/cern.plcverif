/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.summaryreporter.junit;

import java.util.List;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.exceptions.SettingsSerializerException;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.library.summaryreporter.junit.impl.JunitSummaryGenerator;
import cern.plcverif.verif.summary.extensions.interfaces.ISummaryReporter;
import cern.plcverif.verif.summary.extensions.interfaces.data.SummaryJobResult;
import cern.plcverif.verif.summary.extensions.parsernodes.VerificationMementoNode;

public class JunitSummaryReporter implements ISummaryReporter {
	private JunitSummaryReporterSettings settings;
	
	public JunitSummaryReporter() {
		this(new JunitSummaryReporterSettings());
	}
	
	public JunitSummaryReporter(JunitSummaryReporterSettings settings) {
		this.settings = Preconditions.checkNotNull(settings);
	}

	@Override
	public Settings retrieveSettings() {
		try {
			return SpecificSettingsSerializer.toGenericSettings(settings);
		} catch (SettingsSerializerException | SettingsParserException e) {
			throw new PlcverifPlatformException("Unable to retrieve settings in JUnit summary reporter.", e);
		}
	}

	@Override
	public void execute(List<VerificationMementoNode> input, SummaryJobResult result) {
		result.addFileToSave(result.getSummaryName() + ".xml",
				JunitSummaryReporterExtension.CMD_ID,
				JunitSummaryGenerator.represent(input, settings).toString());
	}
}
