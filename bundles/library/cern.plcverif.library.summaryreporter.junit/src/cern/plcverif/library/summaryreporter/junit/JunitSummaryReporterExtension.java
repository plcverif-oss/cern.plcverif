/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.summaryreporter.junit;

import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.help.SettingsHelp;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.verif.summary.extensions.interfaces.ISummaryReporter;
import cern.plcverif.verif.summary.extensions.interfaces.ISummaryReporterExtension;

public class JunitSummaryReporterExtension implements ISummaryReporterExtension {
	public static final String CMD_ID = "junit-summary";

	@Override
	public JunitSummaryReporter createSummaryReporter() {
		return new JunitSummaryReporter(new JunitSummaryReporterSettings());
	}

	@Override
	public ISummaryReporter createSummaryReporter(SettingsElement settings) throws SettingsParserException {
		JunitSummaryReporterSettings specificSettings = SpecificSettingsSerializer.parse(settings,
				JunitSummaryReporterSettings.class);
		return new JunitSummaryReporter(specificSettings);
	}

	@Override
	public void fillSettingsHelp(SettingsHelp help) {
		help.addPluginDetails(CMD_ID, "JUnit summary reporter",
				"Summarizes the outputs of previous verification jobs into a summary report in JUnit format. Applicable only if job=summary.",
				JunitSummaryReporterExtension.class);

		// Fill the 'SettingsHelp' object
		SpecificSettingsSerializer.fillSettingsHelp(help, JunitSummaryReporterSettings.class,
				new JunitSummaryReporterSettings());
	}
}
