/*******************************************************************************
 * (C) Copyright CERN 2024. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Xaver Fink - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reporter.graphviz.exceptions;

public class GraphvizRuntimeException extends RuntimeException {
	
	private static final long serialVersionUID = 1123113235199488909L;

	public GraphvizRuntimeException(String message) {
		super(message);
	}
	
	public GraphvizRuntimeException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
