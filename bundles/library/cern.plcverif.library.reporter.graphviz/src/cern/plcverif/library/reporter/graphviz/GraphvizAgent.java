/*******************************************************************************
 * (C) Copyright CERN 2024. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Xaver Fink - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reporter.graphviz;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.exceptions.SettingsSerializerException;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.common.utils.OsUtils;
import cern.plcverif.library.reporter.graphviz.exceptions.GraphvizException;
import cern.plcverif.library.reporter.graphviz.exceptions.GraphvizRuntimeException;
import cern.plcverif.verif.interfaces.IVerificationReporter;
import cern.plcverif.verif.interfaces.data.VerificationResult;
import cern.plcverif.verif.utils.backend.BackendExecutor;
import cern.plcverif.verif.utils.backend.BackendExecutor.BackendExecutionException;
import cern.plcverif.verif.utils.backend.BackendExecutor.BackendExecutionStatus;
import cern.plcverif.verif.utils.backend.BackendExecutor.BackendExecutorResult;

public class GraphvizAgent implements IVerificationReporter {
	private GraphvizSettings settings;
	
	public GraphvizAgent(GraphvizSettings settings) {
		this.settings = settings;
	}
	
	@Override
	public SettingsElement retrieveSettings() {
		try {
			SettingsElement ret = SpecificSettingsSerializer.toGenericSettings(settings);
			ret.setValue(GraphvizExtension.CMD_ID);
			return ret;
		} catch (SettingsParserException | SettingsSerializerException e) {
			throw new PlcverifPlatformException("Unable to retrieve the settings of the requirement.fret plugin: "+e.getMessage());
		}	
	}

	@Override
	public void generateReport(VerificationResult result) {
		List<Path> paths = result.getSavedGraphs();
		
		for(Path path : paths) {
			try {
				executeDotToGraph(result, path);
			} catch (GraphvizException e) {
				e.printStackTrace();
			}
		}		
	}
	
	
	public void executeDotToGraph(VerificationResult result, Path dotFile) throws GraphvizException {

		// Locate the NuSMV binary
		String executablePath = this.settings.getGraphvizPath();
		if (executablePath == null || executablePath.length() == 0) {
			throw new GraphvizException("The path of Graphviz is unknown.");
		}
		if (!Paths.get(executablePath).toFile().exists()) {
			throw new GraphvizException(
					String.format("The Graphviz binary is not found at the expected '%s' location.", executablePath));
		}
		executablePath = Path.of(executablePath, "dot").toString();

		// Assemble executable process
		final List<String> procArguments = new ArrayList<>(Arrays.asList(executablePath));
		procArguments.addAll(List.of("-Tsvg", dotFile.toAbsolutePath().toString()));

		BackendExecutor executorBuilder = BackendExecutor.create(procArguments, 120, false)
				.stdoutCommentLinePattern("^((\\s*\\*\\*\\*.+)|(\\s*))$");

		try {
			BackendExecutorResult execResult = executorBuilder.execute();
			String svgAsString = execResult.getStdout().toString();
			result.addFileToSave(dotFile.getFileName().toString().replace(".dot", ".svg"),
					GraphvizExtension.CMD_ID, svgAsString);

			if (execResult.getStatus() == BackendExecutionStatus.Timeout) {
				// Logging already done by BackendExecutor
				throw new GraphvizRuntimeException("Timeout.");
			}
			if (execResult.getStatus() == BackendExecutionStatus.Canceled) {
				// Logging already done by BackendExecutor
				throw new GraphvizRuntimeException("User cancellation.");
			}

			// Error handling
			String stdErrContent = execResult.getStderr().toString();
			if (stdErrContent.contains("error") || stdErrContent.contains("aborting")
					|| stdErrContent.contains("reached invalid code")
					|| execResult.getStatus() == BackendExecutionStatus.Error) {
				throw new GraphvizRuntimeException("Graphviz ERROR: " + stdErrContent);
			}
		} catch (BackendExecutionException e) {
			// Try to improve the error message
			String additionalMessage = "";
			if (OsUtils.getCurrentOs() != OsUtils.OperatingSystem.Win32 && (e.getMessage().contains("Permission denied") || e.getMessage().contains("Access denied"))) {
				additionalMessage = " Maybe the Graphviz binary is not set as executable (chmod +x)?";
			}
			
			throw new GraphvizException("Error during Graphviz execution." + additionalMessage, e);
		}
	}


}
