/*******************************************************************************
 * (C) Copyright CERN 2024. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Xaver Fink - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reporter.graphviz;

import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsSerializer;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.help.SettingsHelp;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.verif.interfaces.IVerificationReporter;
import cern.plcverif.verif.interfaces.IVerificationReporterExtension;
import cern.plcverif.verif.requirement.AbstractRequirementSettings;

public class GraphvizExtension implements IVerificationReporterExtension {
	public static final String CMD_ID = "graphviz";

	@Override
	public IVerificationReporter createReporter() {
		try {
			return createReporter(null);
		} catch (SettingsParserException e) {
			throw new PlcverifPlatformException(
					"Unable to load the default settings for the 'graphviz' plug-in.", e);
		}
	}

	@Override
	public IVerificationReporter createReporter(SettingsElement settings) throws SettingsParserException {
		// Load default settings
		SettingsElement compositeSettings = SettingsSerializer
				.loadDefaultSettings("/settings", CMD_ID, this.getClass().getClassLoader()).toSingle();
		if (settings != null) {
			compositeSettings.overrideWith(settings);
		}

		// Parse to plugin-specific format
		GraphvizSettings graphvizSettings = SpecificSettingsSerializer.parse(compositeSettings,
				GraphvizSettings.class);

		return new GraphvizAgent(graphvizSettings);	
	}

	@Override
	public void fillSettingsHelp(SettingsHelp help) {
		help.addPluginDetails(CMD_ID, "Graphviz requirement",
				"Represents the requirement based on a requirement created in FRET.",
				GraphvizExtension.class);

		// The settings specific to this plug-in (i.e., not in
		// AbstractRequirementSettings) are
		// represented manually to be able to include the pattern IDs as
		// permitted values.
		SpecificSettingsSerializer.fillSettingsHelp(help, AbstractRequirementSettings.class);
	}
}
