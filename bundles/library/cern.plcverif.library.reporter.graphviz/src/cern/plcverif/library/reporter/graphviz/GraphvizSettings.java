/*******************************************************************************
 * (C) Copyright CERN 2024. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Xaver Fink - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reporter.graphviz;

import cern.plcverif.base.common.settings.specific.AbstractSpecificSettings;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsElement;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory;

public class GraphvizSettings extends AbstractSpecificSettings {

	public static final String GRAPHVIZ_PATH = "graphviz_path";
	@PlcverifSettingsElement(name = GRAPHVIZ_PATH, description = "path to the graphviz directory.", mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private String graphPath = "";
	
	public GraphvizSettings() {
	}

	
	public String getGraphvizPath() {
		return graphPath;
	}

	public void setGraphvizPath(String graphPath) {
		this.graphPath = graphPath;
	}


}
