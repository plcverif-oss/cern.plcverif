/*******************************************************************************
 * (C) Copyright CERN 2024. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Xaver Fink - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reporter.graphviz.exceptions;

public class GraphvizException extends Exception{
	private static final long serialVersionUID = -2053683522198480231L;
	
	public GraphvizException(String message) {
		super(message);
	}
	
	public GraphvizException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
