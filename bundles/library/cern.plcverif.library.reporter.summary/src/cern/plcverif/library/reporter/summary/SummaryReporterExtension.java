/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reporter.summary;

import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.help.SettingsHelp;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.verif.interfaces.IVerificationReporterExtension;

public class SummaryReporterExtension implements IVerificationReporterExtension {
	public static final String CMD_ID = "summary";

	@Override
	public SummaryReporter createReporter() {
		return new SummaryReporter();
	}

	@Override
	public SummaryReporter createReporter(SettingsElement settings) throws SettingsParserException {
		return new SummaryReporter(settings);
	}

	@Override
	public void fillSettingsHelp(SettingsHelp help) {
		help.addPluginDetails(CMD_ID, "Summary report serializer",
				"Provides the verification summary job. It can collect the results of previous verification jobs and produce a summary report.",
				SummaryReporterExtension.class);
		SpecificSettingsSerializer.fillSettingsHelp(help, SummaryReporterSettings.class);
	}
}
