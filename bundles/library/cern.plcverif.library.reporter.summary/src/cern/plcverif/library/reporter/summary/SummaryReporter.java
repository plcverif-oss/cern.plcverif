/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reporter.summary;

import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.logging.PlatformLogger;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.exceptions.SettingsSerializerException;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.library.reporter.summary.exception.SummaryReporterException;
import cern.plcverif.library.reporter.summary.impl.SummaryMementoGenerator;
import cern.plcverif.verif.interfaces.IVerificationReporter;
import cern.plcverif.verif.interfaces.data.VerificationResult;

public class SummaryReporter implements IVerificationReporter {
	private SummaryReporterSettings settings;

	private String reportContent = null;
	
	public SummaryReporter() {
		this.settings = new SummaryReporterSettings();
	}

	public SummaryReporter(SettingsElement settings) throws SettingsParserException {
		this.settings = SpecificSettingsSerializer.parse(settings, SummaryReporterSettings.class);
	}

	@Override
	public SettingsElement retrieveSettings() {
		try {
			SettingsElement ret = SpecificSettingsSerializer.toGenericSettings(settings).toSingle();
			ret.setValue(SummaryReporterExtension.CMD_ID);
			return ret;
		} catch (SettingsParserException | SettingsSerializerException e) {
			throw new PlcverifPlatformException("Unable to retrieve settings.", e);
		}
	}

	@Override
	public void generateReport(VerificationResult result) {
		try {
			this.reportContent = SummaryMementoGenerator.represent(result, settings);
			result.addFileToSave(result.getJobMetadata().getIdWithoutSpecialChars() + ".report.summary",
					SummaryReporterExtension.CMD_ID,
					reportContent);
		} catch (SummaryReporterException e) {
			PlatformLogger.logError("Unable to generate summary report: " + e.getMessage(), e);
		}
	}
	
	/**
	 * Returns the contents of the created summary memento.
	 * @return Summary memento (textual representation).
	 */
	public String getResult() {
		return reportContent;
	}
}
