/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reporter.summary.impl;

import java.time.LocalDateTime;
import java.util.Optional;

import cern.plcverif.base.common.formattedstring.BasicFormattedString;
import cern.plcverif.base.common.formattedstring.PlaintextFormatter;
import cern.plcverif.base.common.settings.SettingsUtils;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.exceptions.SettingsSerializerException;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.interfaces.data.result.JobStage;
import cern.plcverif.library.reporter.summary.SummaryReporterSettings;
import cern.plcverif.library.reporter.summary.exception.SummaryReporterException;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.VerificationResult;
import cern.plcverif.verif.interfaces.data.VerificationStageTags;
import cern.plcverif.verif.summary.extensions.parsernodes.VerificationMementoNode;

public final class SummaryMementoGenerator {
	private SummaryMementoGenerator() {
	}

	public static String represent(VerificationResult result, SummaryReporterSettings settings)
			throws SummaryReporterException {
		try {
			VerificationMementoNode rootNode = representInternal(result, settings);

			// Remove some unnecessary settings
			rootNode.setOutputDirectory(null);
			rootNode.getVerifJobNode().setValue(null);

			String ret = SpecificSettingsSerializer.serialize(rootNode, "_root");
			return ret.replaceAll("(?m)^-_root\\.", "-");
			// not the nicest solution
		} catch (SettingsParserException | SettingsSerializerException e) {
			throw new SummaryReporterException(e.getMessage(), e);
		}
	}

	private static VerificationMementoNode representInternal(VerificationResult result, SummaryReporterSettings settings)
			throws SettingsParserException, SummaryReporterException {
		VerificationMementoNode ret;
		if (result.getSettings() == null) {
			throw new SummaryReporterException("Unable to fetch the settings used for verification.");
		} else {
			ret = SpecificSettingsSerializer.parse(SettingsUtils.getRootSettingsNode(result.getSettings()).toSingle(),
					VerificationMementoNode.class);
		}

		ret.getInfoNode().setBackendAlgo(emptyStringIfNull(result.getBackendAlgorithmId()));
		ret.getInfoNode().setBackendName(emptyStringIfNull(result.getBackendName()));
		if (result.getValueStore() != null) {
			ret.getInfoNode().setPluginKeyValueStore(result.getValueStore());
		}
		
		Optional<VerificationProblem> verifProblem = result.getVerifProblem();
		if (verifProblem.isPresent()) {
			ret.getInfoNode()
					.setRequirementText(emptyStringIfNull(verifProblem.get().getRequirementDescription()));
		}
		ret.getResultNode().setBackendExecTimeMs(
				JobStage.sumLengthMs(result.getStages(VerificationStageTags.BACKEND_EXECUTION)));
		ret.getResultNode().setExecutionDate(LocalDateTime.now());
		ret.getResultNode().setCexExists(
				result.getInstanceCounterexample().isPresent() || result.getDeclarationCounterexample().isPresent());
		ret.getResultNode().setResult(result.getResult());
		ret.getResultNode().setRuntimeMs(JobStage.sumLengthMs(result.getAllStages()));

		return ret;
	}

	private static String emptyStringIfNull(String str) {
		if (str == null) {
			return "";
		} else {
			return str;
		}
	}
	
	private static String emptyStringIfNull(BasicFormattedString str) {
		if (str == null) {
			return "";
		} else {
			return str.format(new PlaintextFormatter());
		}
	}
}
