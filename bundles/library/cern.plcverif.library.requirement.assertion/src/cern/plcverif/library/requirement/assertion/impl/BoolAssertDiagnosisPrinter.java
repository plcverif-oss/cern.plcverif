/*******************************************************************************
 * (C) Copyright CERN 2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.requirement.assertion.impl;

import java.util.Optional;

import cern.plcverif.base.common.formattedstring.BasicFormattedString;
import cern.plcverif.base.common.formattedstring.Formats.ItalicFormat;
import cern.plcverif.base.models.cfa.transformation.assertion.AssertInliningResult;
import cern.plcverif.base.models.cfa.transformation.assertion.AssertInliningResult.InlinedAssertion;
import cern.plcverif.verif.utils.diagnoser.AssertDiagnosisPrinter;

/**
 * Class that produces human-readable diagnosis for a verification execution
 * with assertion requirement where the assertion inlining was done using the
 * Boolean strategy.
 */
public class BoolAssertDiagnosisPrinter extends AssertDiagnosisPrinter {
	public BoolAssertDiagnosisPrinter(AssertInliningResult assertionInliningResult) {
		super(assertionInliningResult);
	}

	@Override
	public void appendAssertionViolatedMessage(Optional<InlinedAssertion> assertion, int cexStep,
			BasicFormattedString previousDiagnosis) {
		String currentCexStepName = String.format("Cycle %s", Integer.toString(cexStep + 1));

		if (this.assertionInliningResult.getInlinedAssertionCount() == 1) {
			previousDiagnosis.append(
					String.format("The single checked assertion has been violated in %s.%n", currentCexStepName));
		} else {
			previousDiagnosis.append(String.format(
					"One of the assertions has been violated in %s. Due to the selected assertion representation strategy, it cannot be determined which assertion has been violated.%n",
					currentCexStepName));
		}
	}

	@Override
	public BasicFormattedString noDiagnosisAvailable() {
		return new BasicFormattedString("No diagnosis is available.", ItalicFormat.INSTANCE);
	}
}
