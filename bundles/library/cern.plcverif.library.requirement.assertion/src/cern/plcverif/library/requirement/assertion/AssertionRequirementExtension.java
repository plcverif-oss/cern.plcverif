/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - improvements and modified settings handling
 *******************************************************************************/
package cern.plcverif.library.requirement.assertion;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.help.SettingsHelp;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.verif.interfaces.IRequirementExtension;

public class AssertionRequirementExtension implements IRequirementExtension {
	public static final String CMD_ID = "assertion";
	
	@Override
	public AssertionRequirement createRequirement() {
		return new AssertionRequirement(new AssertionRequirementSettings());
	}

	@Override
	public AssertionRequirement createRequirement(SettingsElement settings) throws SettingsParserException {
		Preconditions.checkNotNull(settings, "settings");

		AssertionRequirementSettings specificSettings = SpecificSettingsSerializer.parse(settings, AssertionRequirementSettings.class); 
		return new AssertionRequirement(specificSettings);
	}

	@Override
	public void fillSettingsHelp(SettingsHelp help) {
		help.addPluginDetails(CMD_ID, "Assertion requirement",
				"Represents the requirement based on the assertions present in the source code.",
				AssertionRequirementExtension.class);
		SpecificSettingsSerializer.fillSettingsHelp(help, AssertionRequirementSettings.class);
	}
}
