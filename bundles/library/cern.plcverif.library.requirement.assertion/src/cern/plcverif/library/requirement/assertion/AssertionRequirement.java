/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Ignacio D. Lopez-Miguel - modified global time insertion
 *******************************************************************************/
package cern.plcverif.library.requirement.assertion;

import static cern.plcverif.verif.requirement.PlcCycleRepresentation.representPlcCycle;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.formattedstring.BasicFormattedString;
import cern.plcverif.base.common.formattedstring.Formats.ItalicFormat;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.exceptions.SettingsSerializerException;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.interfaces.IParserLazyResult;
import cern.plcverif.base.interfaces.data.result.StageStatus;
import cern.plcverif.base.interfaces.exceptions.AtomParsingException;
import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory;
import cern.plcverif.base.models.cfa.cfabase.Annotation;
import cern.plcverif.base.models.cfa.cfabase.AssertionAnnotation;
import cern.plcverif.base.models.cfa.cfabase.IfLocationAnnotation;
import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfabase.LocationAnnotation;
import cern.plcverif.base.models.cfa.cfabase.Transition;
import cern.plcverif.base.models.cfa.cfadeclaration.AssignmentTransition;
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.Field;
import cern.plcverif.base.models.cfa.textual.CfaToString;
import cern.plcverif.base.models.cfa.transformation.assertion.AssertInliner;
import cern.plcverif.base.models.cfa.transformation.assertion.AssertInliningResult;
import cern.plcverif.base.models.cfa.transformation.assertion.AssertInliningStrategy;
import cern.plcverif.base.models.cfa.utils.CfaUtils;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.verif.interfaces.IRequirement;
import cern.plcverif.verif.interfaces.data.RequirementRepresentationStrategy;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.VerificationResult;
import cern.plcverif.verif.requirement.PlcCycleRepresentation;
import cern.plcverif.verif.requirement.PlcCycleRepresentation.FieldsInCategories;
import cern.plcverif.verif.utils.diagnoser.AssertDiagnoser;
import cern.plcverif.verif.utils.requirement.AssertRequirementUtil;

//import cern.plcverif.plc.step7.transformation.Step7AstTransformationException;

public class AssertionRequirement implements IRequirement {
	private final AssertionRequirementSettings settings;
	
	/** Strategy to be followed in assertion inlining. */
	private final AssertInliningStrategy assertInliningStrategy;

	/** Assertion diagnoser created based on the selected strategy. */
	private AssertDiagnoser diagnoser = null;
	
	/** Result of the assertion inlining. */
	private AssertInliningResult assertionInliningResult = null;

	public AssertionRequirement(AssertionRequirementSettings settings) {
		Preconditions.checkNotNull(settings, "settings");
		Preconditions.checkNotNull(settings.getAssertRepresentationStrategy(),
				"settings.getAssertRepresentationStrategy()");

		this.settings = settings;
		this.assertInliningStrategy = settings.getAssertRepresentationStrategy().getRepresentedInliningStrategy();
	}

	@Override
	public SettingsElement retrieveSettings() {
		try {
			SettingsElement ret = SpecificSettingsSerializer.toGenericSettings(settings);
			ret.setValue(AssertionRequirementExtension.CMD_ID);
			return ret;
		} catch (SettingsSerializerException | SettingsParserException e) {
			throw new PlcverifPlatformException(
					"Error occurred during the retrieval of the assertion requirement's settings.", e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * This method performs the following steps:
	 * <ul>
	 * <li>Loads and resolves the list of fields treated as inputs, parameters
	 * and the eventual input bindings,
	 * <li>Explores the variables which should be represented as inputs (even if
	 * they are not explicitly mentioned in the given verification problem),
	 * <li>Represents the PLC scan cycle (i.e. it encloses the main automaton in
	 * an infinite loop),
	 * <li>Creates an {@link Expression} representing the described property as
	 * a temporal logic expression and adds it to the given
	 * {@link VerificationProblem},
	 * </ul>
	 */
	@Override
	public void fillVerificationProblem(VerificationProblem verifProblem, IParserLazyResult parserResult,
			VerificationResult result, RequirementRepresentationStrategy reqRepresentation) {
		Preconditions.checkNotNull(verifProblem, "verifProblem");
		Preconditions.checkNotNull(parserResult, "parserResult");
		Preconditions.checkNotNull(result, "result");
		Preconditions.checkNotNull(reqRepresentation, "reqRepresentation");

		try {
			CfaNetworkDeclaration cfa = (CfaNetworkDeclaration) verifProblem.getModel();
			FieldsInCategories fields = PlcCycleRepresentation.categorizeFields(cfa, parserResult, settings,
					result.currentStage());
			transformModel(verifProblem, result, fields);
			AssertRequirementUtil.createTemporalRequirement(verifProblem, result, reqRepresentation,
					assertionInliningResult);

		} catch (AtomParsingException exception) {
			result.currentStage().logError(exception.getMessage(), exception);
			result.currentStage().setStatus(StageStatus.Unsuccessful);
			throw new PlcverifPlatformException(exception.getMessage(), exception);
		}
	}

	@Override
	public void diagnoseResult(VerificationProblem verifProblem, VerificationResult result) {
		if (diagnoser == null) {
			result.setResultDiagnosis(new BasicFormattedString("No diagnosis is available.", ItalicFormat.INSTANCE));
		} else {
			diagnoser.diagnoseResult(verifProblem, result);
		}
	}

	private void transformModel(VerificationProblem data, VerificationResult result, FieldsInCategories fields) {
		Preconditions.checkArgument(data.getModel() instanceof CfaNetworkDeclaration,
				"Requirement expected a CFA network declaration but instead got " + data.getModel());
		CfaNetworkDeclaration cfaNetwork = (CfaNetworkDeclaration) data.getModel();

		// Filter the assertions if needed
		// (i.e., remove the assertions from the CFA if they are not within the
		// ones to be checked)
		filterAssertions(data, result);

		// Inline assertions (should not be performed on loop automaton, as an
		// error would make it jump to the end location, skipping the EoC
		// location)
		this.assertionInliningResult = AssertInliner.transform(cfaNetwork, assertInliningStrategy);
		
		result.getAdditionalVariables().add(assertionInliningResult.getErrorField());
		createDiagnoser(assertionInliningResult, result);

		representPlcCycle(data, result, fields);
	}

	private void filterAssertions(VerificationProblem data, VerificationResult result) {
		if (!settings.allAssertionsToBeChecked()) {
			Preconditions.checkState(settings.getAssertionsToCheck() != null);
			List<AssertionAnnotation> assertions = EmfHelper.getAllContentsOfType(data.getModel(),
					AssertionAnnotation.class, false);
			long assertionsWithGivenName = assertions.stream()
					.filter(it -> settings.getAssertionsToCheck().contains(it.getName())).count();
			
			boolean allAssertionsWildcardSelected = settings.getAssertionsToCheck().stream().anyMatch(it -> AssertionRequirementSettings.ALL_ASSERTIONS.equals(it));
			List<String> assertionsToBeChecked = new ArrayList<>();

			for (AssertionAnnotation assertion : assertions) {
				if (settings.getAssertionsToCheck().contains(assertion.getName()) || allAssertionsWildcardSelected) {
					assertionsToBeChecked.add( assertion.getName() );
				}
			}
			
			
			if (settings.getAssertionsToCheck() == null) {
				throw new PlcverifPlatformException(
						String.format("No assertion is given to check. Use the '%s' wildcard to check all assertions.",
								AssertionRequirementSettings.ALL_ASSERTIONS));
			}
			
			if (allAssertionsWildcardSelected) {
				result.currentStage().logWarning(
						"The list of assertions to be checked contains the wildcard '%s' (meaning all assertions), but some additional assertion tags too. "+ 
						"Check if your verification case is correct. If the wildcard is selected, it is unnecessary to select additional assertions.",
						AssertionRequirementSettings.ALL_ASSERTIONS);
				data.setRequirementDescription(new BasicFormattedString(
						String.format("All the assertions are to be checked ('%s').",
								String.join(", ", assertionsToBeChecked))));
				return;
			}
			
			
			if (assertionsWithGivenName == 0) {
				// no assertion found with the given name
				result.currentStage().logWarning(
						"No assertion was found with the tag '%s'. Checking all assertions instead.",
						String.join(", ", settings.getAssertionsToCheck()));
				// Do not delete any assertion in this case.
				data.setRequirementDescription(new BasicFormattedString(
						"No assertions to be checked. (Failed to find the assertion with the given tag.)"));
			} else {
				// there is at least one assertion with the given name, remove
				// all others
				for (AssertionAnnotation assertion : assertions) {
					if (!settings.getAssertionsToCheck().contains(assertion.getName())) {
						result.currentStage().logDebug("Assertion '%s' (tag: '%s') has been removed from the CFA.",
								CfaToString.toDiagString(assertion.getInvariant()), assertion.getName());
						CfaUtils.delete(assertion);
					}
				}

				if (assertionsWithGivenName == 1) {
					data.setRequirementDescription(
							new BasicFormattedString(String.format("The assertion '%s' is to be checked.",
									assertionsToBeChecked.get(0))));
				} else {
					data.setRequirementDescription(new BasicFormattedString(
							String.format("The assertions '%s' (in total '%s' assertions) are to be checked.",
									String.join(", ", assertionsToBeChecked), assertionsWithGivenName)));
				}
			}
		} else {
			data.setRequirementDescription(new BasicFormattedString("All the assertions are to be checked."));
		}
	}

	private void createDiagnoser(AssertInliningResult assertionInliningResult, VerificationResult verifResult) {
		this.diagnoser = settings.getAssertRepresentationStrategy().createDiagnoser(settings, assertionInliningResult,
				verifResult);
	}


}
