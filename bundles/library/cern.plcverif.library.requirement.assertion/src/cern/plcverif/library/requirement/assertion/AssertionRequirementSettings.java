/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.requirement.assertion;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsElement;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsList;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory;
import cern.plcverif.verif.requirement.AbstractRequirementSettings;

public class AssertionRequirementSettings extends AbstractRequirementSettings {
	public static final String ALL_ASSERTIONS = "*";
	
	public static final String ASSERTION_TO_CHECK = "assertion_to_check";
	@PlcverifSettingsList(
			name = ASSERTION_TO_CHECK, 
			description = "The tag(s) of the assertion(s) to be checked, or '*' if all assertions shall be checked.",
			mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private List<String> assertionToCheck = Collections.singletonList(ALL_ASSERTIONS);
	
	public static final String MAX_EXPR_SIZE_FOR_DETAILED_DIAGNOSIS = "max_expr_size_for_detailed_diagnosis";
	@PlcverifSettingsElement(
			name = MAX_EXPR_SIZE_FOR_DETAILED_DIAGNOSIS, 
			description = "Maximum expression size that is considered for detailed diagnosis if the assertion has been violated.",
			mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private int maxDiagnosedExprSize = 250;

	public static final String ASSERT_REPRESENTATION_STRATEGY = "assert_representation_strategy";
	@PlcverifSettingsElement(
			name = ASSERT_REPRESENTATION_STRATEGY, 
			description = "Assertion (error field) representation strategy. If it is 'INT', the violated assertion can be determined, but the state space will be bigger. If it is 'BOOL', the violated assertion cannot be determined (only the fact that an assertion has been violated), but the state space will be smaller.",
			mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private AssertRepresentationStrategy assertRepresentationStrategy = AssertRepresentationStrategy.INTEGER;
	
	public Set<String> getAssertionsToCheck() {
		return new HashSet<>(assertionToCheck);
	}
	
	public boolean allAssertionsToBeChecked() {
		return assertionToCheck != null &&
				assertionToCheck.size() == 1 && 
				ALL_ASSERTIONS.equals(assertionToCheck.get(0));
	}
	
	/**
	 * Maximum expression size that is considered for detailed diagnosis if violated.
	 */
	public int getMaxDiagnosedExprSize() {
		return maxDiagnosedExprSize;
	}
	
	/**
	 * Returns the assertion representation strategy, i.e., how should the error field be represented.
	 * @return Strategy
	 */
	public AssertRepresentationStrategy getAssertRepresentationStrategy() {
		return assertRepresentationStrategy;
	}
}
