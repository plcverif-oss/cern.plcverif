/*******************************************************************************
 * (C) Copyright CERN 2018-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.requirement.assertion;

import com.google.common.base.Preconditions;

import cern.plcverif.base.models.cfa.transformation.assertion.AssertInliner;
import cern.plcverif.base.models.cfa.transformation.assertion.AssertInliningResult;
import cern.plcverif.base.models.cfa.transformation.assertion.AssertInliningStrategy;
import cern.plcverif.library.requirement.assertion.impl.BoolAssertDiagnosisPrinter;
import cern.plcverif.library.requirement.assertion.impl.IntAssertDiagnosisPrinter;
import cern.plcverif.verif.interfaces.data.VerificationResult;
import cern.plcverif.verif.utils.diagnoser.AssertDiagnoser;
import cern.plcverif.verif.utils.diagnoser.BoolAssertDiagnoser;
import cern.plcverif.verif.utils.diagnoser.IntAssertDiagnoser;

/**
 * Assertion representation strategies. To be used in the assertion requirement
 * settings.
 */
public enum AssertRepresentationStrategy {
	//@formatter:off
	/**
	 * Assertion representation strategy that uses an integer error field, 
	 * therefore the violated assertion can be determined.
	 */
	INTEGER(AssertInliningStrategy.INT_STRATEGY,
		(settings, inliningResult, verifResult) ->
			new IntAssertDiagnoser(inliningResult, new IntAssertDiagnosisPrinter(inliningResult, verifResult, settings))),
	
	/**
	 * Assertion representation strategy that uses a Boolean error field, 
	 * therefore the violated assertion cannot be determined (if there are 
	 * multiple assertions checked).
	 */
	BOOLEAN(AssertInliningStrategy.BOOL_STRATEGY, 
		(settings, inliningResult, verifResult) -> 
			new BoolAssertDiagnoser(inliningResult, new BoolAssertDiagnosisPrinter(inliningResult)));
	//@formatter:on

	private AssertInliningStrategy representedInliningStrategy;
	private AssertDiagnoserFactory diagnoserFactory;

	private AssertRepresentationStrategy(AssertInliningStrategy representedInliningStrategy,
			AssertDiagnoserFactory diagnoserFactory) {
		this.representedInliningStrategy = Preconditions.checkNotNull(representedInliningStrategy);
		this.diagnoserFactory = Preconditions.checkNotNull(diagnoserFactory);
	}

	/**
	 * Returns the represented inlining strategy that can be used by
	 * {@link AssertInliner}.
	 * 
	 * @return Assertion inlining strategy
	 */
	public AssertInliningStrategy getRepresentedInliningStrategy() {
		return representedInliningStrategy;
	}

	/**
	 * Creates an assertion requirement verification result diagnoser that can
	 * provide more detailed information about the satisfaction or violation of
	 * the requirement. The created diagnoser will match the strategy determined
	 * by this enumeration.
	 * 
	 * @param settings
	 *            Assertion requirement settings
	 * @param assertionInliningResult
	 *            Result of assertion inlining
	 * @param verifResult
	 *            Result object of the verification job
	 * @return Diagnoser to be used once the counterexample is available
	 */
	public AssertDiagnoser createDiagnoser(AssertionRequirementSettings settings,
			AssertInliningResult assertionInliningResult, VerificationResult verifResult) {
		return diagnoserFactory.createDiagnoser(settings, assertionInliningResult, verifResult);
	}

	/**
	 * Assertion diagnoser factory interface.
	 */
	@FunctionalInterface
	private interface AssertDiagnoserFactory {
		AssertDiagnoser createDiagnoser(AssertionRequirementSettings settings,
				AssertInliningResult assertionInliningResult, VerificationResult verifResult);
	}
}