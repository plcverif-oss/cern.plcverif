/*******************************************************************************
 * (C) Copyright CERN 2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.requirement.assertion.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.formattedstring.BasicFormattedString;
import cern.plcverif.base.common.formattedstring.Formats.ItalicFormat;
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef;
import cern.plcverif.base.models.cfa.textual.CfaExprToFormattedString;
import cern.plcverif.base.models.cfa.transformation.assertion.AssertInliningResult;
import cern.plcverif.base.models.cfa.transformation.assertion.AssertInliningResult.InlinedAssertion;
import cern.plcverif.base.models.expr.BoolLiteral;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.Literal;
import cern.plcverif.base.models.expr.utils.ExprEval;
import cern.plcverif.base.models.expr.utils.ExprUtils;
import cern.plcverif.base.models.expr.utils.NormalFormUtils;
import cern.plcverif.library.requirement.assertion.AssertionRequirementSettings;
import cern.plcverif.verif.interfaces.data.VerificationResult;
import cern.plcverif.verif.interfaces.data.cex.DeclarationCounterexample;
import cern.plcverif.verif.utils.diagnoser.AssertDiagnosisPrinter;

/**
 * Class that produces human-readable diagnosis for a verification execution
 * with assertion requirement where the assertion inlining was done using the
 * integer strategy.
 */
public class IntAssertDiagnosisPrinter extends AssertDiagnosisPrinter {
	protected final VerificationResult verifResult;
	private AssertionRequirementSettings settings;

	public IntAssertDiagnosisPrinter(AssertInliningResult assertionInliningResult, VerificationResult verifResult, AssertionRequirementSettings settings) {
		super(assertionInliningResult);
		this.verifResult = Preconditions.checkNotNull(verifResult);
		this.settings = Preconditions.checkNotNull(settings);
	}

	@Override
	public void appendAssertionViolatedMessage(Optional<InlinedAssertion> assertion, int cexStep,
			BasicFormattedString previousDiagnosis) {
		String currentCexStepName = String.format("Cycle %s", Integer.toString(cexStep/2 + 1));	// divided by 2 as there are two steps in each cycle (BoC and Eoc)

		Map<DataRef, Literal> valuations = getValuationsFor(cexStep);

		if (assertion.isPresent()) {
			// We know which assertion has been violated.
			
			previousDiagnosis.append("The assertion '");
			previousDiagnosis
					.append(new CfaExprToFormattedString(valuations).toString(assertion.get().getAssertionInvariant()));
			previousDiagnosis.append("' ");
			if (assertion.get().getTag().isPresent()) {
				previousDiagnosis.append(String.format("(tag: %s) ", assertion.get().getTag().orElse("-")));
			}
			previousDiagnosis.append(String.format("has been violated in %s. ", currentCexStepName));

			/* removed due to PV-102 - explosion of clauses - useless for the user
			int invariantSize = ExprUtils.exprTreeSize(assertion.get().getAssertionInvariant());
			if (invariantSize <= settings.getMaxDiagnosedExprSize()) {
				previousDiagnosis.append(violatedClausesToString(assertion.get().getAssertionInvariant(), valuations));
			} else {
				verifResult.currentStage().logInfo(
						"Detailed assertion diagnosis (identification of violated clauses) was skipped as the invariant's size is %s, while the maximum permitted size is %s. This threshold is provided by the '%s' setting of the plug-in.",
						invariantSize, settings.getMaxDiagnosedExprSize(),
						AssertionRequirementSettings.MAX_EXPR_SIZE_FOR_DETAILED_DIAGNOSIS);
			}*/
		} else {
			// We do not know which assertion has been violated. This is not a typical case for INT strategy.
			
			previousDiagnosis.append(String.format(
					"An assertion has been violated in %s, but it is not possible to determine which assertion.",
					currentCexStepName));
		}
	}

	private Map<DataRef, Literal> getValuationsFor(int cexStep) {
		if (verifResult.getDeclarationCounterexample().isPresent()) {
			DeclarationCounterexample declarationCounterexample = verifResult.getDeclarationCounterexample().get();
			return declarationCounterexample.getSteps().get(cexStep).getValuationsAsMap();
		} else {
			return new HashMap<>();
		}

	}
	
	private static BasicFormattedString violatedClausesToString(Expression assertionInvariant,
			Map<DataRef, Literal> valuations) {
		Preconditions.checkNotNull(assertionInvariant);
		Preconditions.checkNotNull(valuations);

		BasicFormattedString diagnosis = new BasicFormattedString();
		ExprEval exprEval = new ExprEval(valuations);

		// Show violated CNF clauses
		List<Expression> cnfClauses = NormalFormUtils.getCnfClauses(assertionInvariant);
		boolean first = true;
		for (Expression clause : cnfClauses) {
			// try to calculate the value (eval)
			if (exprEval.isComputableConstant(clause)) {
				Literal computedLiteral = exprEval.literalRepresentation(clause);
				if (computedLiteral instanceof BoolLiteral && ((BoolLiteral) computedLiteral).isValue() == false) {
					if (!first) {
						diagnosis.append(",\n");
					} else {
						diagnosis.append("\n\nViolated clauses:\n");
					}
					// Evaluating the clause with the given valuations was
					// successful and it is violated.
					diagnosis.append(new CfaExprToFormattedString(valuations).toString(clause));
					first = false;
				}
			}
		}
		if (!first) {
			diagnosis.append(".");
		}

		return diagnosis;
	}

	@Override
	public BasicFormattedString noDiagnosisAvailable() {
		return new BasicFormattedString("No diagnosis is available.", ItalicFormat.INSTANCE);
	}
}
