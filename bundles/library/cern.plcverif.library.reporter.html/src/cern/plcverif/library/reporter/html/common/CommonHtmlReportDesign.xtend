/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.library.reporter.html.common

import cern.plcverif.base.common.logging.PlcverifSeverity
import cern.plcverif.base.common.utils.IoUtils
import cern.plcverif.base.common.utils.UiUtils
import cern.plcverif.verif.interfaces.data.ResultEnum
import com.google.common.html.HtmlEscapers
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Base64
import java.util.Calendar
import cern.plcverif.base.common.utils.OsUtils

class CommonHtmlReportDesign {
	public static final String SATISFIED_RESULT_BGCOLOR = "#22b14c";
	public static final String VIOLATED_RESULT_BGCOLOR = "#E34234";
	public static final String ERROR_RESULT_BGCOLOR = "#FA58F4";
	public static final String UNKNOWN_RESULT_BGCOLOR = "#FFC40C";
	public static final String REPORT_FILENAME_KEY = "report_filename";
		
	/**
	 * Regular expression pattern that matches absolute Windows paths
	 * that do not contain whitespaces in the names.
	 * In the match the first group will be the full path of the file.
	 */
	package static final String FILEPATH_PATTERN_WINDOWS = "(?<![\"'/>]|&quot;)\\b(((\\S:\\\\))([\\w\\.\\\\/\\-\\(\\)])+)\\b" 
	
	/**
	 * Regular expression pattern that matches absolute Windows paths
	 * that may contain whitespaces but are surrounded with quotation marks.
	 * In the match the first group will be the full path of the file.
	 */
	package static final String FILEPATH_PATTERN_WINDOWS_QUOTED = "(?:\"|&quot;)(((\\S:\\\\))([\\w\\.\\\\/\\-\\(\\) ])+)(?:\"|&quot;)";
	
	/**
	 * Regular expression pattern that matches absolute Linux paths.
	 * In the match the first group will be the full path of the file.
	 */
	package static final String FILEPATH_PATTERN_LINUX = "(?<=\\s)(/[\\w\\.\\-][\\w/\\.\\(\\)\\-]+)";
	
	/**
	 * Regular expression pattern that matches HTTP(S) links.
	 * In the match the first group will be the full path of the URL.
	 */
	 package static final String HTTP_URL_PATTERN = "(http(s)?://[^\\s<>&)]*)";
	
	

	static final String PLCVERIF_VERSION = "3.0"
	static final String PLCVERIF_COPYRIGHT = "CERN BE-ICS-TMA"
	
	static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	/**
	 * Returns the {@code <style>...</style>} part of the report 
	 * that contains the CSS to be used by the rest of the report.
	 */
	def static style() '''
		<style>
			body {
				font-family: Helvetica, sans-serif;
				font-size:11pt;
				background-color: #fff;
			}
			table,th,td
			{
				border:1px solid #999;
				border-collapse:collapse;
				padding:3px;
				font-size:11pt;
			}
			tr.emph { background-color:#F2F2F2;	}
			tr.separator { border-bottom:2px solid #000000;	}
			tr.input { background-color: beige; }
			tr.frozenvar { background-color: pink; }
			.lnheader { font-weight: bold; vertical-align:top;}
			small.header { font-style:italic; font-size:8pt; }
			.summary_description { font-style:italic; font-size:9pt; }
			.hidden { display: none; }
			#logoimg {position: absolute; top:15px; right:15px; display:block;}
			a.black { color: #000; }
			.tiny { font-size:8pt; }
		</style>
	'''

	/**
	 * Returns the {@code <script>...</script>} part of the report that 
	 * contains the Javascript used by the rest of the report.
	 */
	def static scripts() '''
		<script type="text/javascript">
			function toggleVisibility(divID) {
				var elem = document.getElementById(divID);
				if (elem) {
					elem.className=(elem.className=='hidden')?'':'hidden';
				}
			}
		</script>
	'''

	/**
	 * Returns the report header, containing the logo and the H1 title.
	 */
	def static header() '''
		«logo()»
				<h1>PLCverif &mdash; Verification report</h1>
				<small class="header">Generated on «DATE_FORMAT.format(Calendar.getInstance().getTime())» | PLCverif v«PLCVERIF_VERSION» | (C) «PLCVERIF_COPYRIGHT» | <a class="black" href="javascript:toggleVisibility('expertDetails')">Show/hide expert details</a></small>
				<hr/>
	'''
	
	/**
	 * Returns PLCverif logo in a {@code <div>}.
	 */
	private static def logo() '''
		<div>
		«imgFromResource("images/plcverif_icon1_48x48.png", "logoimg", "PLCverif")»
		</div>
	'''
	
	/**
	 * Returns the resource image as base64 {@code <img>}.
	 */
	static def imgFromResource(String resourcePath, String id, String alt) '''
		<img id="«id»" alt="«alt»" src="data:image/png;base64,«Base64.encoder.encodeToString(IoUtils.readBinaryResourceFile(resourcePath, CommonHtmlReportDesign.classLoader))»"/>
	'''
	
	/**
	 * Returns the result as a human-readable string.
	 */
	static def String resultString(ResultEnum result) {
		switch (result) {
			case Satisfied: return "Satisfied"
			case Violated: return "Violated"
			case Unknown: return "Unknown"
			case Error: return "ERROR"
			default: throw new UnsupportedOperationException("Unknown result: " + result)
		}
	}

	/**
	 * Returns the color representing the given result.
	 */
	static def resultBgColor(ResultEnum result) {
		switch (result) {
			case Satisfied: return SATISFIED_RESULT_BGCOLOR
			case Violated: return VIOLATED_RESULT_BGCOLOR
			case Unknown: return UNKNOWN_RESULT_BGCOLOR
			case Error: return ERROR_RESULT_BGCOLOR
			default: throw new UnsupportedOperationException("Unknown result: " + result)
		}
	}

	/**
	 * Returns given run time (in ms) as a human-readable HTML string. It is shown in a convenient unit, e.g., 
	 * instead of 5000000 ms, 1.39 h is shown.
	 * @param msSmall If true, the run time in milliseconds is also included, within {@code <small>} tag.
	 */
	def static displayRuntime(long runtime_ms, boolean msSmall) {
		val ret = UiUtils.displayRuntime(runtime_ms);
		
		if (msSmall) {
			return ret.replaceAll("(\\(.+\\))", "<small>$1</small>");
		} else {
			return ret;
		}
	}
	
	/**
	 * Returns the HTML color string that represents the given severity level.
	 */
	static def severityColor(PlcverifSeverity severity) {
		switch (severity) {
			case Error: return "#ffadad"
			case Warning: return "#ffde6c"
			default: return "white"
		}
	}
	
	/**
	 * Returns the given string as a HTML-escaped string.
	 */
	def static String toHtmlString(CharSequence text) {
		if (text === null) {
			return "";
		}
		
		// HTML escaping (necessary for e-mail addresses for example in form of <alpha@beta.com>.)
		var ret = HtmlEscapers.htmlEscaper.escape(text.toString);
		
		// line breaks
		ret = ret.replace("\n ", "<br/>&nbsp;");
		ret = ret.replace("\n", "<br/>");
		
		// whitespaces
		ret = ret.replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;");
		ret = ret.replace("  ", "&nbsp;&nbsp;");
		
		
		// Making Windows (absolute) paths clickable on Windoes
		val currentOs = OsUtils.currentOs;
		if (currentOs == OsUtils.OperatingSystem.Win32) {
 			ret = ret.replaceAll(FILEPATH_PATTERN_WINDOWS_QUOTED, "<a href=\"file://$1\">$1</a>");
 			ret = ret.replaceAll(FILEPATH_PATTERN_WINDOWS, "<a href=\"file://$1\">$1</a>");
 		}
 		
 		// Make *nux (absolute) paths clickable on Linux and Mac
 		if (currentOs == OsUtils.OperatingSystem.Linux || currentOs == OsUtils.OperatingSystem.MacOs) { 
 			ret = ret.replaceAll(FILEPATH_PATTERN_LINUX, "<a href=\"file://$1\">$1</a>");
 		}
 
		// links
		ret = ret.replaceAll(HTTP_URL_PATTERN, "<a href=\"$1\">$1</a>");
		
		return ret;	
	}
	
	def static String toFileLink(String filePath) {
		return '''<a href="file://«filePath»">«filePath»</a>''';
	}
}
