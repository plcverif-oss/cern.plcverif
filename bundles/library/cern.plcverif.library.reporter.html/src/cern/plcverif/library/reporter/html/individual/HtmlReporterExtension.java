/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reporter.html.individual;

import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.help.SettingsHelp;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.verif.interfaces.IVerificationReporter;
import cern.plcverif.verif.interfaces.IVerificationReporterExtension;

public class HtmlReporterExtension implements IVerificationReporterExtension {
	public static final String CMD_ID = "html";

	@Override
	public IVerificationReporter createReporter() {
		return new HtmlReporter();
	}

	@Override
	public IVerificationReporter createReporter(SettingsElement settings) throws SettingsParserException {
		return new HtmlReporter(settings);
	}

	@Override
	public void fillSettingsHelp(SettingsHelp help) {
		help.addPluginDetails(CMD_ID, "HTML reporter",
				"Generates a HTML representation of the verification results, including the metadata, configuration and the eventual counterexample. Only applicable if job=verif.",
				HtmlReporterExtension.class);
		SpecificSettingsSerializer.fillSettingsHelp(help, HtmlReporterSettings.class);
	}
}
