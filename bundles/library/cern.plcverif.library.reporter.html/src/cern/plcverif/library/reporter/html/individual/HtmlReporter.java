/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reporter.html.individual;

import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.exceptions.SettingsSerializerException;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.library.reporter.html.common.CommonHtmlReportDesign;
import cern.plcverif.library.reporter.html.individual.impl.HtmlReportGenerator;
import cern.plcverif.verif.interfaces.IVerificationReporter;
import cern.plcverif.verif.interfaces.data.VerificationResult;

public class HtmlReporter implements IVerificationReporter {
	public static final String HTML_REPORT_FILE_SUFFIX = ".report.html";
	private HtmlReporterSettings settings;

	public HtmlReporter() {
		this.settings = new HtmlReporterSettings();
	}

	public HtmlReporter(SettingsElement settings) throws SettingsParserException {
		this.settings = SpecificSettingsSerializer.parse(settings, HtmlReporterSettings.class);
	}

	@Override
	public SettingsElement retrieveSettings() {
		try {
			SettingsElement ret = SpecificSettingsSerializer.toGenericSettings(settings).toSingle();
			ret.setValue(HtmlReporterExtension.CMD_ID);
			return ret;
		} catch (SettingsParserException | SettingsSerializerException e) {
			throw new PlcverifPlatformException("Unable to retrieve settings.", e);
		}
	}

	@Override
	public void generateReport(VerificationResult result) {
		String reportContent = HtmlReportGenerator.represent(result, settings).toString();
		String reportFilename = result.getJobMetadata().getIdWithoutSpecialChars() + HTML_REPORT_FILE_SUFFIX;

		result.addFileToSave(reportFilename,
				HtmlReporterExtension.CMD_ID, reportContent);
		result.getValueStore().addValue(HtmlReporterExtension.CMD_ID, CommonHtmlReportDesign.REPORT_FILENAME_KEY, reportFilename);
	}
}
