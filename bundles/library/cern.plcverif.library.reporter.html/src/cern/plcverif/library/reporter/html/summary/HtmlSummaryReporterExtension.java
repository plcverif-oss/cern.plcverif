/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reporter.html.summary;

import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.help.SettingsHelp;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.verif.summary.extensions.interfaces.ISummaryReporter;
import cern.plcverif.verif.summary.extensions.interfaces.ISummaryReporterExtension;

public class HtmlSummaryReporterExtension implements ISummaryReporterExtension {
	public static final String CMD_ID = "html-summary";

	@Override
	public HtmlSummaryReporter createSummaryReporter() {
		return new HtmlSummaryReporter(new HtmlSummaryReporterSettings());
	}

	@Override
	public ISummaryReporter createSummaryReporter(SettingsElement settings) throws SettingsParserException {
		HtmlSummaryReporterSettings specificSettings = SpecificSettingsSerializer.parse(settings,
				HtmlSummaryReporterSettings.class);
		return new HtmlSummaryReporter(specificSettings);
	}

	@Override
	public void fillSettingsHelp(SettingsHelp help) {
		help.addPluginDetails(CMD_ID, "HTML summary reporter",
				"Summarizes the outputs of previous verification jobs into a summary report in human-readable HTML format. Applicable only if job=summary.",
				HtmlSummaryReporterExtension.class);

		// Fill the 'SettingsHelp' object
		SpecificSettingsSerializer.fillSettingsHelp(help, HtmlSummaryReporterSettings.class,
				new HtmlSummaryReporterSettings());
	}
}
