/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.library.reporter.html.individual.impl

import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.base.common.formattedstring.HtmlFormatter
import cern.plcverif.base.common.logging.PlcverifSeverity
import cern.plcverif.base.common.settings.SettingsElement
import cern.plcverif.base.common.settings.SettingsSerializer
import cern.plcverif.base.interfaces.data.result.JobStage
import cern.plcverif.base.models.cfa.cfabase.DataDirection
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef
import cern.plcverif.base.models.cfa.cfadeclaration.DirectionFieldAnnotation
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef
import cern.plcverif.base.models.cfa.cfadeclaration.Indexing
import cern.plcverif.base.models.cfa.cfadeclaration.OriginalDataTypeFieldAnnotation
import cern.plcverif.base.models.cfa.utils.CfaDeclarationUtils
import cern.plcverif.base.models.cfa.textual.CfaToString
import cern.plcverif.base.models.cfa.utils.CfaUtils
import cern.plcverif.base.models.expr.AtomicExpression
import cern.plcverif.base.models.expr.InitialValue
import cern.plcverif.library.reporter.html.individual.HtmlReporterSettings
import cern.plcverif.verif.interfaces.data.VerificationProblem
import cern.plcverif.verif.interfaces.data.VerificationResult
import cern.plcverif.verif.interfaces.data.VerificationStageTags
import com.google.common.base.Preconditions
import com.google.common.base.Throwables
import java.util.Comparator
import java.util.List
import java.util.Map

import static cern.plcverif.library.reporter.html.common.CommonHtmlReportDesign.*
import cern.plcverif.base.models.expr.Literal

/**
 * Class to generate the HTML report representation of the given verification result. 
 */
final class HtmlReportGenerator {
	/**
	 * <p>
	 * Guaranteed to be non-null after the constructor has been executed.
	 */
	VerificationResult result;

	/**
	 * <p>
	 * Guaranteed to be non-null after the constructor has been executed.
	 */
	HtmlReporterSettings settings;

	private new(VerificationResult result, HtmlReporterSettings settings) {
		this.result = Preconditions.checkNotNull(result);
		this.settings = Preconditions.checkNotNull(settings);
	}

	/**
	 * Returns the HTML report for the given verification result with the given settings.
	 */
	static def CharSequence represent(VerificationResult result, HtmlReporterSettings settings) {
		return new HtmlReportGenerator(result, settings).represent();
	}

	/**
	 * Represents the whole content of the HTML report for the given {@code result} ({@code <html>...</html>}).
	 */
	private def CharSequence represent() '''
		<html>
			«representHead»
			«representBody»
		</html>
	'''

	/**
	 * Represents the {@code <head>...</head>} part of the HTML report.
	 */
	private def representHead() '''
		<head>
			<meta charset="UTF-8">
			<title>«result.jobMetadata?.id» verification report</title>
			«style»
			«scripts»
		</head>
	'''

	/**
	 * Represents the {@code <body>...</body>} part of the HTML report.
	 */
	private def representBody() '''
		<body>
			«header»
			«mainInfo»
			«warnings»
			«counterexample»
			
			<hr/>	
			<p><b><a class="black" href="javascript:toggleVisibility('expertDetails')">Show/hide more details</a></b></p>
			<div id="expertDetails" class="hidden">
				«stageResultLog»
				«log»
				«outputs»
				«pluginKeyValueStore»
				«effectiveSettings»
			</div>
		</body>
	'''
	
	/**
	 * Represents the main summary part of the HTML report ({@code <table>...</table>}).
	 */
	private def mainInfo() '''
		<table>
			«mainInfoRow("ID:", result.jobMetadata?.id, "", "lnheader")»
			«mainInfoRow("Name:", result.jobMetadata?.name, "emph", "")»
			«mainInfoRow("Description:", toHtmlString(result.jobMetadata?.description), "", "")»
			«mainInfoRow("Source file(s):", sourceFiles(result.inputFiles), "", "")»
			«IF result.verifProblem.isPresent»
				«mainInfoRow("Requirement:", result.verifProblem.get.requirementDescription?.format(new HtmlFormatter()), "emph", "")»
			«ELSE»
				«mainInfoRow("Requirement:", "UNKNOWN", "emph", "")»
			«ENDIF»
			«IF !result.paramFields.isEmpty»
				«mainInfoRow("Parameters:", paramsString(result.paramFields), "", "")»
			«ENDIF»
			«IF !result.bindings.isEmpty»
				«mainInfoRow("Mode selection:", modeSelectionString(result.bindings), "", "")»
			«ENDIF»
			<tr><td class="lnheader">Result:</td><td bgcolor="«resultBgColor(result.result)»"><b>«resultString(result.result)»</b></td></tr>
			«mainInfoRow("Verification backend:", '''«result.backendName» («result.backendAlgorithmId»)''', "", "")»
			«mainInfoRow("Total run time:", '''«displayRuntime(JobStage.sumLengthMs(result.getAllStages), true)»''', "emph", "")»
			«mainInfoRow("Backend run time:", '''«displayRuntime(JobStage.sumLengthMs(result.getStages(VerificationStageTags.BACKEND_EXECUTION)), true)»''', "emph", "")»
		</table>
	'''
	
	/**
	 * HTML representation of source files.
	 */
	private def sourceFiles(List<String> sourceFiles) {
		if (sourceFiles.isNullOrEmpty) {
			return "<i>unknown</i>";
		} else if (sourceFiles.size === 1) {
			return toHtmlString(sourceFiles.get(0));
		} else {
			return '''
				<ul>
					«FOR sourceFile : sourceFiles»
						<li>«toFileLink(sourceFile)»</li>
					«ENDFOR»
				</ul>
			'''
		}
	}
	
	/**
	 * Represents one row in the main summary table ({@code <tr>...</tr>}).
	 * @param name The name of the property (human-readable).
	 * @param content The value of the property (human-readable).
	 * @param rowclass The class to be used for the row ({@code <tr>}).
	 * @param valueCellClass The class to be used for the cell ({@code <td>}) of the property value.
	 */
	private def mainInfoRow(String name, String content, String rowclass,
		String valueCellClass
	) '''
		<tr class="«rowclass»"><td class="lnheader">«name»</td><td class="«valueCellClass»">«content»</td></tr>
	'''

	/**
	 * Represents the eventual warnings
	 */
	private def CharSequence warnings() {
		val warningsAndErrors = result.getAllStages.map[it|it.getLogItems(PlcverifSeverity.Warning)].flatten;

		return '''
			«IF !warningsAndErrors.isEmpty»
				<h2>Warnings and errors</h2>
				Please take the following warnings and errors into account when considering the above verification result.
				<ul>
					«FOR logItem : warningsAndErrors»
						<li><b>«logItem.message»</b> («logItem.stage.getStageName»)</li>
					«ENDFOR»
				</ul>
			«ENDIF»
		'''
	}
	
	/**
	 * Represents the counterexample, if exists. If the counterexample does not exist 
	 * ({@code result.declarationCounterexample == null}), an empty string is returned.
	 */
	private def counterexample() '''
		«IF result.declarationCounterexample.isPresent»
			<h2>Counterexample</h2>
			<table>
			<!-- header -->
				<tr>
					<td></td>
					<td>Variable</td>
					«FOR i : 1 .. result.declarationCounterexample.get.stepsCount/2»
						<td width="70">Beginning of <b>Cycle «i»</b></td>
						<td width="70">End of <b>Cycle «i»</b></td>
					«ENDFOR»
				</tr>

				«FOR v : result.declarationCounterexample.get.collectAllVariables
					.sortWith(Comparator.comparing([it | nonnullStringKey(firstPrefixName(it).toString())])
						.thenComparing([it | nonnullStringKey(getFieldDirection(it), "ZZZ")])
						.thenComparing([it | nonnullStringKey(atomToDisplayString(it), "")])
					)»
					«IF !CfaUtils.refersToInternalGenerated(v) || !settings.isHideInternalVariables»
						<tr>
							«val rowColor = cexRowColor(v)»
							<td bgcolor="«rowColor»"><small><i>«getFieldDirection(v)?.literal» «varPlcType(v)»</i></small></td>
							<td bgcolor="«rowColor»"><b>«atomToDisplayString(v)»</b>«IF CfaUtils.refersToInternalGenerated(v)» (internal)«ENDIF»</td>
							«FOR i : 1 .. result.declarationCounterexample.get.stepsCount»
								<td bgcolor="«rowColor»">«cexValueCell(result.declarationCounterexample.get.steps.get(i - 1).getValue(v), v)»</td>
							«ENDFOR»									
						</tr>
					«ELSE»
						«result.currentStage.logInfo("The variable '%s' is excluded from the counterexample as it is an internal generated variable. This behavior can be changed in the settings (cf. %s).",
							atomToDisplayString(v),
							HtmlReporterSettings.HIDE_INTERNAL_VARIABLES
						)»
					«ENDIF»
				«ENDFOR»
			</table>
		«ENDIF»
		«IF result.resultDiagnosis.isPresent»
			<h2>Diagnosis</h2>
			«result.resultDiagnosis.get.format(new HtmlFormatter())»
		«ENDIF»
	'''
	
	private def cexValueCell(Literal literal, DataRef field) {
		if (result.parserResult === null) {
			// failback solution
			return CfaToString.INSTANCE.toString(literal);
		}
		
		val asString = result.parserResult.serializeAtom(literal);
		val referredField = CfaDeclarationUtils.getReferredField(field);
		val OriginalDataTypeFieldAnnotation annotation = CfaUtils.getFirstAnnotationOfType(referredField, OriginalDataTypeFieldAnnotation);
		val asStringWithHint = result.parserResult.serializeAtom(literal, annotation);
		
		if (annotation === null || !settings.isUseLfValueRepresentation || asString.equals(asStringWithHint)) {
			// There is no need or no possibility to provide a hint
			return asString;
		} else {
			return '''«asStringWithHint»<br><div class="tiny">«asString»</div>''' 
		}
	}
	
	/**
	 * Returns the section with the stage results table.
	 */
	private def stageResultLog() '''
		<h3>Verification stages</h3>
			<table>
			<tr>
				<td><b>Phase</b></td>
				<td width="100"><b>Result</b></td>
				<td width="100"><b>Runtime</b></td>
			</tr>
			
			«FOR stage : result.getAllStages»
				<tr>
					<td>«stage.getStageName»</td>
					<td>«stage.getStatus»</td>
					<td>«IF stage.getLengthNs > 0»«stage.lengthMs» ms«ENDIF»</td>
				</tr>
			«ENDFOR»
		</table>
	'''
	
	/**
	 * Returns the section with log items in each stage.
	 */
	private def log() '''
		<h3>Log</h3>	
		<table>
			<tr>
				<td><b>Message</b></td>
				<td width="100"><b>Severity</b></td>
				<td width="100"><b>Stage</b></td>
				«IF settings.isShowLogitemTimestamps»<td width="150"><b>Date</b></td>«ENDIF»
			</tr>
			«FOR logItem : result.getAllStages.map[it | it.getLogItems(settings.minLogLevel)].flatten»
				<tr>
					<td>«toHtmlString(logItem.message)»«stackTraceIfNeeded(logItem.exception)»</td>
					<td bgcolor="«severityColor(logItem.severity)»">«logItem.severity.toString»</td>
					<td>«logItem.stage.getStageName»</td>
					«IF settings.isShowLogitemTimestamps»<td>«logItem.time»</td>«ENDIF»
				</tr>
			«ENDFOR»		
		</table>
	'''
	
	private def stackTraceIfNeeded(Throwable throwable) {
		if (throwable !== null && settings.isIncludeStackTrace) {
			val stackTrace = Throwables.getStackTraceAsString(throwable);			
			return '''<br><br><code>«toHtmlString(stackTrace)»</code>'''
		}
		return "";
	}
	
	/**
	 * Returns the backend outputs (stdout and stderr).
	 */
	private def outputs() '''
		«IF settings.isShowVerificationConsoleOutput() && (result.backendStderr.isPresent || result.backendStdout.isPresent)»
			<h2>Verification tool outputs</h2>
			«IF result.backendStderr.isPresent»
				<h3>STDERR</h3>
				<code>«toHtmlString(result.backendStderr.get)»</code>
			«ENDIF»
			
			«IF result.backendStdout.isPresent»
				<h3>STDOUT</h3>
				<code>«toHtmlString(result.backendStdout.get)»</code>
			«ENDIF»
		«ENDIF»
	'''
	
	private def pluginKeyValueStore() '''
		«IF result.valueStore.entrySet.isEmpty == false»
			<h2>Plug-in key-value store</h2>
			<ul>
			«FOR entry : result.valueStore.entrySet»
				<li>«entry.key» = «entry.value»</li>
			«ENDFOR»
			</ul>
		«ENDIF»
	'''
	
	private def effectiveSettings() '''
		«IF settings.includeSettings»
			<h2>Effective settings used for the verification</h2>
			«IF result.settings instanceof SettingsElement»
				<code>
					«toHtmlString(SettingsSerializer.serialize(result.settings.toSingle))»
				</code>
			«ELSE»
				ERROR: Unable to serialize the settings, it does not have the expected type. 
				The effective settings is «result.settings?.class?.name» instead of «typeof(SettingsElement).name».
			«ENDIF»
		«ENDIF»
	'''
	
	// Helpers
	private def modeSelectionString(Map<DataRef, InitialValue> bindings) {
		val boundFields = bindings.keySet.toList.sortBy[it | it.atomToDisplayString.toString];
		
		return '''«FOR boundField : boundFields SEPARATOR ', '»«boundField.atomToDisplayString» = «bindings.get(boundField).atomToDisplayString»«ENDFOR»'''
	}
	
	private def paramsString(List<DataRef> paramFields) {
		return '''«FOR param : paramFields SEPARATOR ', '»«param.atomToDisplayString»«ENDFOR»'''
	}
	
	private def <T> String nonnullStringKey(T key, String valueIfNull) {
		Preconditions.checkNotNull(valueIfNull);
		if (key === null) {
			return valueIfNull;
		} else {
			return key.toString;
		}
	}
	
	private def int fieldDepth(DataRef ref) {
		if (ref === null) {
			return 0;
		} else if (ref instanceof FieldRef) {
			return 1 + fieldDepth(ref.prefix);
		} else if (ref instanceof Indexing) {
			return fieldDepth(ref.prefix);
		}
		
		throw new UnsupportedOperationException("Unknown case in fieldDepth");
	}
	
	private def cexRowColor(DataRef ref) {
		val referredField = CfaDeclarationUtils.getReferredField(ref);
		
		if (CfaUtils.hasInternalGeneratedAnnotation(referredField)) {
			return "#cccccc";
		}
		
		val fieldDepth = fieldDepth(ref);
		val entryBlockContext = result.verifProblem.orElse(new VerificationProblem()).entryBlockContext.orElse(EmfHelper.getContainerOfType(referredField, CfaNetworkDeclaration)?.mainContext);
		val entryBlockField = if (entryBlockContext !== null) CfaDeclarationUtils.getReferredField(entryBlockContext) else null;
		// isRootLevel: global variable or entry block's variable
		val isRootLevel = fieldDepth == 1 ||
			(fieldDepth >= 2 && CfaDeclarationUtils.getReferredField(CfaDeclarationUtils.getRoot(ref)) === entryBlockField);
		val isRootLevelInput = (getFieldDirection(ref) == DataDirection.INPUT ||
			getFieldDirection(ref) == DataDirection.INOUT) && isRootLevel;
		val isRootLevelOutput = (getFieldDirection(ref) == DataDirection.OUTPUT) && isRootLevel;
		val isFrozen = referredField.isFrozen; 
		
		if (isFrozen) {
			if (isRootLevelInput) {
				// frozen input
				return "#ffb9b9";
			} else if (isRootLevelOutput) {
				// frozen output
				return "#c8e3ba";
			} else {
				// frozen other
				return "#dbffff";
			}
		} else {
			if (isRootLevelInput) {
				return "#ffe8e8";
			} else if (isRootLevelOutput) {
				return "#eff9e9";
			} else {
				return "white";
			}
		}
	}
	
	private def varPlcType(DataRef ref) {
		val field = CfaDeclarationUtils.getReferredField(ref);
		val directionAnnotation = CfaUtils.getAnnotationStreamOfType(field, OriginalDataTypeFieldAnnotation).findFirst;
		if (directionAnnotation.isPresent && directionAnnotation.get.plcDataType !== null) {
			return directionAnnotation.get.plcDataType.toUpperCase;
		} else {
			return CfaToString.INSTANCE.toString(ref.type);
		}
	}
	
	private def DataDirection getFieldDirection(DataRef ref) {
		val field = CfaDeclarationUtils.getReferredField(ref);
		val directionAnnotation = CfaUtils.getAnnotationStreamOfType(field, DirectionFieldAnnotation).findFirst;
		if (directionAnnotation.isPresent) {
			return directionAnnotation.get.direction;
		} else if (ref.prefix !== null) {
			return getFieldDirection(ref.prefix);
		} else {
			return null;
		}
	}
	
	/**
	 * Returns a human-readable representation of the given atomic
	 * expression (value or data reference typically).
	 */
	private def atomToDisplayString(AtomicExpression value) {
		if (result.parserResult !== null) {
			return result.parserResult.serializeAtom(value);
		} else {
			// failback solution
			return CfaToString.INSTANCE.toString(value);
		}
	}
	
	
	private def CharSequence firstPrefixName(DataRef ref) {
		if (ref === null || ref.prefix === null) {
			return "";
		} else {
			return previousPrefixName(ref);
		}
	}
	
	private def CharSequence previousPrefixName(DataRef ref) {
		if (ref === null) {
			return "";
		} else if (ref.prefix === null) {
			return ref.atomToDisplayString;
		} else {
			return previousPrefixName(ref.prefix);
		}
	}
}
