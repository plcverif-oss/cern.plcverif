/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.library.reporter.html.summary.impl

import cern.plcverif.library.reporter.html.common.CommonHtmlReportDesign
import cern.plcverif.library.reporter.html.individual.HtmlReporterExtension
import cern.plcverif.library.reporter.html.summary.HtmlSummaryReporterSettings
import cern.plcverif.verif.summary.extensions.parsernodes.VerificationMementoNode
import com.google.common.base.Preconditions
import java.util.List

import static cern.plcverif.library.reporter.html.common.CommonHtmlReportDesign.*
import static extension cern.plcverif.library.reporter.html.common.CommonHtmlReportDesign.toHtmlString

class HtmlSummaryGenerator {
	HtmlSummaryReporterSettings settings;
	List<VerificationMementoNode> mementos;

	private new(HtmlSummaryReporterSettings settings, List<VerificationMementoNode> mementos) {
		this.settings = settings;
		this.mementos = mementos;
	}

	static def represent(List<VerificationMementoNode> mementos, HtmlSummaryReporterSettings settings) {
		Preconditions.checkNotNull(mementos, "mementos");
		return new HtmlSummaryGenerator(settings, mementos).represent();
	}

	def represent() {
		return representInternal();
	}

	/**
	 * Represents the whole content of the HTML report for the given {@code result} ({@code <html>...</html>}).
	 */
	private def CharSequence representInternal() '''
		<html>
			«representHead»
			«representBody»
		</html>
	'''

	/**
	 * Represents the {@code <head>...</head>} part of the HTML report.
	 */
	private def representHead() ''' 
		<head>
			<meta charset="UTF-8">
			<title>Summary verification report</title>
			«style»
			«scripts»
		</head>
	'''

	/**
	 * Represents the {@code <body>...</body>} part of the HTML report.
	 */
	private def representBody() '''
		<body>
			«header»
			
			«FOR memento : mementos.sortBy[it | it.getId()]»
				«representSingleMemento(memento)»
			«ENDFOR»
		</body>
	'''
	
	/**
	 * Represents the result of a single verification execution
	 */
	private def representSingleMemento(VerificationMementoNode memento) '''
		<table width="90%">
			<tr class="emph"><td colspan="4"><b>«memento.getId.toHtmlString»</b> : «memento.getName.toHtmlString»</td></tr>
			<tr><td colspan="4">
			«IF !memento.getDescription.isNullOrEmpty»
				<span class="summary_description">«memento.getDescription.toHtmlString»</span><br>
			«ENDIF»
			«memento.infoNode.requirementText.toHtmlString»
			</td></tr>

			«val inputBindings = memento.requirementNode.inputBindings»
			«IF inputBindings.size > 0»
				<tr><td colspan="4">Input binding: 
				«FOR inputBinding : memento.requirementNode.inputBindings SEPARATOR ', '»«inputBinding.input» = «inputBinding.value»«ENDFOR»
				</td></tr>
			«ENDIF»
			
			<!-- Result -->
			<tr>
				<td bgcolor="«resultBgColor(memento.result)»" width="20%">«resultString(memento.result)»</td>
				<td width="30%">Total: «displayRuntime(memento.resultNode.runtimeMs, true)» (MChk: «displayRuntime(memento.resultNode.backendExecTimeMs, true)»)</td>
				<td>«memento.infoNode.backendName» («memento.infoNode.backendAlgo»)</td>
			<td>«linkToIndivReportIfKnown(memento)»</td></tr>
		</table>
		<br>
	'''
	
	def linkToIndivReportIfKnown(VerificationMementoNode memento) {
		val valueStore = memento.infoNode.pluginKeyValueStore;
		val reportFileName = valueStore.getValue(HtmlReporterExtension.CMD_ID, CommonHtmlReportDesign.REPORT_FILENAME_KEY);
		if (reportFileName.isPresent) {
			return '''<a href="«reportFileName.get»">Open the Verification Report</a>''';
		} else {
			return "";
		}
	}
}
