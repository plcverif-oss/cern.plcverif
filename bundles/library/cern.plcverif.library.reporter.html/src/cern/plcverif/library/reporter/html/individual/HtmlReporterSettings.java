/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reporter.html.individual;

import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsElement;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory;
import cern.plcverif.verif.interfaces.AbstractHumanReporterSettings;

public class HtmlReporterSettings extends AbstractHumanReporterSettings {
	public static final String HIDE_INTERNAL_VARIABLES = "hide_internal_variables";
	@PlcverifSettingsElement(name = HIDE_INTERNAL_VARIABLES, description = "Hides the variables generated for verification purposes, which do not have a corresponding variable in the source code.", mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private boolean hideInternalVariables = true;

	/**
	 * If true, the variables generated for verification purposes should be
	 * hidden in the report.
	 */
	public boolean isHideInternalVariables() {
		return hideInternalVariables;
	}

	public static final String INCLUDE_STACK_TRACE = "include_stack_trace";
	@PlcverifSettingsElement(name = INCLUDE_STACK_TRACE, description = "Includes stack traces in the log when available.", mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private boolean includeStackTrace = false;

	/**
	 * If true, the stack traces for the log items should be shown in the report
	 * (if available).
	 */
	public boolean isIncludeStackTrace() {
		return includeStackTrace;
	}

	public static final String USE_LF_VALUE_REPRESENTATION = "use_lf_value_representation";
	@PlcverifSettingsElement(name = USE_LF_VALUE_REPRESENTATION, description = "Uses hints to provide type-specific value representation in the counterexample.", mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private boolean useLfValueRepresentation = false;

	/**
	 * If true, type-specific value representations (i.e., give hints to the
	 * language frontend when the atoms are serialized) should be used in the
	 * counterexample of the report.
	 */
	public boolean isUseLfValueRepresentation() {
		return useLfValueRepresentation;
	}

	public static final String SHOW_VERIFICATION_CONSOLE_OUTPUT = "show_verification_console_output";
	@PlcverifSettingsElement(name = SHOW_VERIFICATION_CONSOLE_OUTPUT, description = "Shows the console output of the verification tool.", mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private boolean showVerificationConsoleOutput = true;

	/**
	 * If true, the console output of the verification tools should be shown in
	 * the report.
	 */
	public boolean isShowVerificationConsoleOutput() {
		return showVerificationConsoleOutput;
	}
}
