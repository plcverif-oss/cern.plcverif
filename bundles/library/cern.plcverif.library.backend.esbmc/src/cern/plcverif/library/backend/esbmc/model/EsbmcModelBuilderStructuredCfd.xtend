/*******************************************************************************
 * (C) Copyright CERN 2017-2023. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *   Xaver Fink	- modifications for ESBMC
 *******************************************************************************/
package cern.plcverif.library.backend.esbmc.model

import cern.plcverif.base.models.cfa.cfabase.Transition
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration
import cern.plcverif.base.models.cfa.utils.CfaDeclarationUtils
import cern.plcverif.base.models.expr.UnaryCtlExpression
import cern.plcverif.base.models.expr.UnaryCtlOperator
import cern.plcverif.base.models.expr.utils.ExprUtils
import cern.plcverif.library.backend.esbmc.EsbmcSettings
import cern.plcverif.library.backend.esbmc.exception.EsbmcException
import cern.plcverif.support.codegen.CfaToProgramTransformer
import cern.plcverif.support.codegen.generator.c.FormattedCCodeGenerator
import cern.plcverif.support.codegen.generator.c.StructNames
import cern.plcverif.support.codegen.generator.c.TypeToCType
import cern.plcverif.support.codegen.pass.EmptyElseEliminationPass
import cern.plcverif.support.codegen.pass.LabelDiscovery
import cern.plcverif.support.codegen.pass.ModulePass
import cern.plcverif.support.codegen.pass.UnusedLabelEliminationVisitor
import cern.plcverif.support.codegen.program.Module
import cern.plcverif.support.codegen.program.Procedure
import cern.plcverif.support.codegen.program.statement.AssertStatement
import cern.plcverif.support.codegen.program.statement.BlockStatement
import cern.plcverif.support.codegen.program.statement.InlineStatement
import cern.plcverif.verif.interfaces.data.VerificationProblem
import cern.plcverif.verif.interfaces.data.VerificationResult
import cern.plcverif.verif.interfaces.data.cex.DeclarationCexStep
import cern.plcverif.verif.interfaces.data.cex.DeclarationCounterexample
import cern.plcverif.verif.interfaces.data.cex.ICounterexample
import cern.plcverif.verif.utils.backend.BackendNameTrace
import com.google.common.base.Preconditions
import java.util.List
import java.util.Map
import cern.plcverif.support.codegen.util.GeneratorUtils
import cern.plcverif.support.codegen.program.statement.AssumeBoundStatement
import cern.plcverif.base.models.expr.UnaryLtlExpression
import cern.plcverif.base.models.expr.UnaryLtlOperator
import cern.plcverif.base.models.expr.utils.TlExprUtils

class EsbmcModelBuilderStructuredCfd extends EsbmcModelBuilder {
	private static final class EsbmcMethodNameTrace extends BackendNameTrace<AutomatonDeclaration> {
		new() {
			super([it|BackendNameTrace::basicNameSanitizer(it)]);
		}

		override protected isReservedWord(String name) {
			return C_KEYWORDS.contains(name);
		}
	}
	
	private static final class EsbmcCCodeGenerator extends FormattedCCodeGenerator {
		new(Module module) {
			super(module)
		}
		
		override visit(AssertStatement assertStatement) {
			return '''__ESBMC_assert(«transformExpression(assertStatement.condition)», "assertion_error");'''
		}
		
		override protected generateGlobalDefinitions() {
			return '''
				« super.generateGlobalDefinitions »
				bool « BOC_MARKER »;
				bool « EOC_MARKER »;
			''';
		}
		
		override protected generateMainLogic() {
			val main = module.mainProcedure;
			
			return '''
				« methodNameTrace.toName(main.correspondingAutomaton) »();
			''';
		}
		
		override protected def String generateNondetDefinitions() {
			return '''
				// No nondet declaration for ESBMC
			''';
		}
		
		override String visit(AssumeBoundStatement assumeBoundStatement) {
	    	val bound = assumeBoundStatement.bound;    	
	    	return '''__ESBMC_assume(«transformExpression(bound)»);'''
    	}	
	}
	
	private static final class EmbedRequirementPass implements ModulePass {		
		VerificationProblem problem;
		
		new(VerificationProblem problem) {
			this.problem = problem;
		}
		
		override run(Module module) {
			this.run(module.mainProcedure);
		}
		
		override run(Procedure function) {
			// Stop the requirement instrumentation if the BoC or EoC location is not available.
			if (problem.eocLocation === null || problem.bocLocation === null) {
				return;
			}

			val eocLabelOpt = LabelDiscovery.getLabelByName(GeneratorUtils.getLocationLabelName(problem.eocLocation), function);
			Preconditions.checkState(eocLabelOpt.isPresent, "The VerificationLoop must have a label with the EoC location name!");

			val bocLabelOpt = LabelDiscovery.getLabelByName(GeneratorUtils.getLocationLabelName(problem.bocLocation), function);
			Preconditions.checkState(bocLabelOpt.isPresent, "The VerificationLoop must have a label with the BoC location name!");

			val eocLabel = eocLabelOpt.get();
			val bocLabel = bocLabelOpt.get();	
			
			val req = problem.requirement;
			val isNotAG = !(req instanceof UnaryCtlExpression) || (req as UnaryCtlExpression).operator != UnaryCtlOperator.AG
			val isNotG = !(req instanceof UnaryLtlExpression) || (req as UnaryLtlExpression).operator != UnaryLtlOperator.G
			val isNotBool = !(TlExprUtils.containsNoTl(req) && !TlExprUtils.isLtl(req))
			if (isNotAG && isNotG && isNotBool) {
				throw new EsbmcException("Unable to represent the requirement. The requirement is not an AG CTL or G LTL requirement.");
			}
			
			
			val bocBlock = new BlockStatement();
			if (bocLabel.innerStatement.isPresent) {
				bocBlock.statements.add(bocLabel.innerStatement.get());
			}

			// Mark the end of the cycle for the CEX parser
			bocBlock.statements.add(new InlineStatement('''«BOC_MARKER» = true; // to indicate the beginning of the loop for the counterexample parser'''));
			bocBlock.statements.add(new InlineStatement('''«BOC_MARKER» = false;'''));
			bocLabel.innerStatement = bocBlock;

			
			val eocBlock = new BlockStatement();
			if (eocLabel.innerStatement.isPresent) {
				eocBlock.statements.add(eocLabel.innerStatement.get());
			}

			if (req instanceof UnaryCtlExpression){
				eocBlock.statements.add(new AssertStatement(req.operand));
			}else if (req instanceof UnaryLtlExpression){
				eocBlock.statements.add(new AssertStatement(req.operand));
			}else{
				eocBlock.statements.add(new AssertStatement(req));
			}

			// Mark the end of the cycle for the CEX parser
			eocBlock.statements.add(new InlineStatement('''«EOC_MARKER» = true; // to indicate the end of the loop for the counterexample parser'''));
			eocBlock.statements.add(new InlineStatement('''«EOC_MARKER» = false;'''));

			eocLabel.innerStatement = eocBlock;
		}
	}
	
	CfaNetworkDeclaration cfd;

	public val static String CONTEXT_VARIABLE = "__context";

	EsbmcCCodeGenerator generator;

	StructNames structNames = new StructNames();
	TypeToCType typeTransformer = new TypeToCType(structNames);
	
	protected new(VerificationProblem verifProblem, VerificationResult result, EsbmcSettings settings) {
		super(verifProblem, result, settings);

		Preconditions.checkNotNull(typeTransformer);

		val cfa = verifProblem.model;
		Preconditions.checkArgument(cfa instanceof CfaNetworkDeclaration,
			"EsbmcModelBuilderCfd is only applicable to CFAs of type CfaNetworkDeclaration.")
		this.cfd = cfa as CfaNetworkDeclaration;
		
		val transformer = CfaToProgramTransformer.create(this.cfd);
		transformer.ignoreAssertions = true;
		
		val module = transformer.createModule();
		
		new EmbedRequirementPass(verifProblem).run(module);
		new UnusedLabelEliminationVisitor().run(module);
		new EmptyElseEliminationPass().run(module);
		
		this.generator = new EsbmcCCodeGenerator(module); 
	}

	override String build() {
		return this.generator.generate();
	}
	
	override protected getExprTransformer() {
		return this.generator.exprTransformer;
	}
	
	override protected representTransitionAssignmentsAndCalls(Transition transition) {
		return ""
	}
	
	override ICounterexample parseTextualCex(List<Map<String, String>> valuemaps) {
		val ret = new DeclarationCounterexample();
		var i = 0;

		val allFieldNames = valuemaps.map[it|it.keySet].flatten.toSet;

		for (Map<String, String> map : valuemaps) {
			i++;
			val step = new DeclarationCexStep(i.toString, ret);

			for (fieldName : allFieldNames) {
				val varValuation = map.get(fieldName);
				try {
					val field = CfaDeclarationUtils.parseHierarchicalDataRef(fieldName, cfd, [field|field.name]);
					if (varValuation !== null) {
						val parseSpecificValue = parseSpecificValue(varValuation, field.type);
						if (parseSpecificValue.isPresent) {
							step.setValue(field, parseSpecificValue.get);
						} else {
							step.setValue(field, ExprUtils.createLiteralFromString(varValuation, field.type));
						}
					}
				} catch (IllegalArgumentException e) {
					// Parsing the hierarchical data reference was unsuccessful -- log it
					verifResult.currentStage.logDebug(
						"Unable to parse a line in the ESBMC counterexample. Concerned field: '%s', value: '%s', reason: %s. This is expected for temporary variables.",
						fieldName, varValuation, e.message);
				}
			}
		}

		return ret;
	}
}