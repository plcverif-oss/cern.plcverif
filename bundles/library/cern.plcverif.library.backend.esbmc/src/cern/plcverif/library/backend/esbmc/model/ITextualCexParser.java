/*******************************************************************************
 * (C) Copyright CERN 2017-2023. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Xaver Fink	- modifications for ESBMC
 *******************************************************************************/
package cern.plcverif.library.backend.esbmc.model;

import java.util.List;
import java.util.Map;

import cern.plcverif.verif.interfaces.data.cex.DeclarationCounterexample;
import cern.plcverif.verif.interfaces.data.cex.ICounterexample;
import cern.plcverif.verif.interfaces.data.cex.InstanceCounterexample;

public interface ITextualCexParser {
	/**
	 * Translates the raw, textual counterexample into a
	 * {@link InstanceCounterexample} or {@link DeclarationCounterexample} using
	 * the name mappings applied in the model generation.
	 * 
	 * @param valuemaps
	 *            Raw, textual counterexample. Each {@link List} element is a
	 *            step, in which each entry of the map is a (variable/field,
	 *            value) pair.
	 * @return The parsed counterexample.
	 */
	ICounterexample parseTextualCex(List<Map<String, String>> valuemaps);
}
