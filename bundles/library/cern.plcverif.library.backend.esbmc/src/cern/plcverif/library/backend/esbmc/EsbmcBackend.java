/*******************************************************************************
 * (C) Copyright CERN 2017-2023. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Xaver Fink	- modifications for ESBMC
 *******************************************************************************/
package cern.plcverif.library.backend.esbmc;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.progress.ICanceling;
import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.exceptions.SettingsSerializerException;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.base.common.utils.OsUtils;
import cern.plcverif.base.common.utils.OsUtils.OperatingSystem;
import cern.plcverif.base.common.utils.PlaceholderReplacement;
import cern.plcverif.base.interfaces.data.result.StageStatus;
import cern.plcverif.base.models.cfa.cfabase.CfaNetworkBase;
import cern.plcverif.base.models.cfa.cfabase.ElseExpression;
import cern.plcverif.base.models.cfa.transformation.CfaTransformationGoal;
import cern.plcverif.base.models.cfa.transformation.ElseEliminator;
import cern.plcverif.library.backend.esbmc.EsbmcSettings.EsbmcModelVariant;
import cern.plcverif.library.backend.esbmc.EsbmcSettings.EsbmcVerifyCommand;
import cern.plcverif.library.backend.esbmc.exception.EsbmcException;
import cern.plcverif.library.backend.esbmc.model.EsbmcModelBuilder;
import cern.plcverif.library.backend.esbmc.parser.EsbmcOutputParser;
import cern.plcverif.verif.interfaces.IBackend;
import cern.plcverif.verif.interfaces.data.RequirementRepresentationStrategy;
import cern.plcverif.verif.interfaces.data.ResultEnum;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.VerificationResult;
import cern.plcverif.verif.interfaces.data.VerificationStageTags;
import cern.plcverif.verif.utils.backend.BackendExecutor;
import cern.plcverif.verif.utils.backend.BackendExecutor.BackendExecutionException;
import cern.plcverif.verif.utils.backend.BackendExecutor.BackendExecutionStatus;
import cern.plcverif.verif.utils.backend.BackendExecutor.BackendExecutorResult;

/**
 * ESBMC backend representation.
 *
 */
public class EsbmcBackend implements IBackend {
	private EsbmcModelVariant variant;
	private EsbmcSettings settings;

	public EsbmcBackend(Settings settings) {
		if (settings instanceof SettingsElement) {
			try {
				this.settings = SpecificSettingsSerializer.parse(settings.toSingle(), EsbmcSettings.class);
				this.variant = this.settings.getModelVariant();
			} catch (SettingsParserException e) {
				throw new PlcverifPlatformException(
						"Unable to parse the settings of the ESBMC backend. " + e.getMessage(), e);
			}
		} else {
			throw new PlcverifPlatformException(
					"Unable to parse the settings of the ESBMC backend. The given settings node is not a single node.");
		}
	}

	@Override
	public SettingsElement retrieveSettings() {
		try {
			return SpecificSettingsSerializer.toGenericSettings(settings);
		} catch (SettingsParserException | SettingsSerializerException e) {
			throw new PlcverifPlatformException("Unable to serialize the settings of the NuSMV backend.", e);
		}
	}

	@Override
	public void execute(VerificationProblem input, VerificationResult result, ICanceling canceler) {
		Preconditions.checkNotNull(result, "result");
		Preconditions.checkNotNull(result.getJobMetadata(), "result.jobMetadata");
		Preconditions.checkNotNull(result.getJobMetadata().getId(), "result.jobMetadata.id");
		Preconditions.checkNotNull(input, "The given verification problem shall not be null.");
		Preconditions.checkNotNull(input.getModel(), "The model in the given verification problem shall not be null.");
		Preconditions.checkNotNull(input.getRequirement(),
				"The requirement in the given verification problem shall not be null.");
		Preconditions.checkNotNull(result, "The given verification result shall not be null.");

		// Precomputation
		CfaNetworkBase cfa = input.getModel();

		ElseEliminator.transform(cfa);
		Preconditions.checkState(EmfHelper.getAllContentsOfType(cfa, ElseExpression.class, false).isEmpty(),
				"There are some ElseExpressions in the CFA even after executing the ElseEliminator.");

		try {
			result.switchToStage("ESBMC model building");
			EsbmcModelBuilder modelBuilder = EsbmcModelBuilder.createBuider(input, result, settings);
			CharSequence modelContent = modelBuilder.build();
			Path cFile = result.getOutputDirectoryOrCreateTemp()
					.resolve(result.getJobMetadata().getIdWithoutSpecialChars() + ".c");
			IoUtils.writeAllContent(cFile, modelContent);

			result.switchToStage("ESBMC execution", VerificationStageTags.BACKEND_EXECUTION);
			BackendExecutorResult execResult;
			execResult = execute(result, cFile, canceler == null ? ICanceling.NEVER_CANCELING_INSTANCE : canceler);
			
			if (execResult.getStatus() == BackendExecutionStatus.Canceled) {
				result.currentStage().logInfo("ESBMC output parsing has been skipped as the result of the ESBMC execution was %s.", execResult.getStatus());
			} else {
				result.switchToStage("ESBMC output parsing");
				new EsbmcOutputParser(execResult.getStderr(), settings, result.currentStage()).parseAndFillResult(result, modelBuilder);
			}
		} catch (EsbmcException e) {
			result.currentStage().setStatus(StageStatus.Unsuccessful);
			result.currentStage().logError("Error happened in ESBMC backend that prevented the model checking. " + e.getMessage());
			return;
		} catch (IOException e) {
			throw new PlcverifPlatformException("Unable to write file for verification. ", e);
		}
	}

	private BackendExecutorResult execute(VerificationResult result, Path cFile, ICanceling canceler)
			throws EsbmcException {
		// Locate the ESBMC binary
		final Path executablePath = result.makeAbsoluteToProgramDirectory(this.settings.getEsbmcBinaryPath());
		if (!executablePath.toFile().exists()) {
			throw new EsbmcException(String.format("The ESBMC binary is not found at '%s'.", executablePath.toString()));
		}

		// Assemble executable process
		final int timeoutSec = settings.getTimeoutInSec();
		int effectiveVerbosityLevel = Math.max(1, Math.min(settings.getEsbmcVerbosity(), 10));

		List<String> argumentsTemplate;
		argumentsTemplate = new ArrayList<>(Arrays.asList("${esbmc_path}", "${model_path}", "${verify_command}", "--no-div-by-zero-check", "--force-malloc-success", "--state-hashing", 
				"--add-symex-value-sets", "--no-align-check", "--k-step", "2", "--floatbv", "--unlimited-k-steps", "--verbosity", "${verbosity}", "--no-pointer-check",
				"--no-bounds-check", "--interval-analysis", "--no-slice"));
		
		if(settings.isParallelizeComputation()) {
			argumentsTemplate.add("--k-induction-parallel");
		}

		//@formatter:off
		List<String> procArguments = PlaceholderReplacement.list(argumentsTemplate)
			.replace("timeout", Integer.toString(settings.getTimeoutInSec()))
			.replace("esbmc_path", toAbsolutePathStringIfNotNull(executablePath))
			.replace("model_path", toAbsolutePathStringIfNotNull(cFile))
			.replace("verbosity", Integer.toString(effectiveVerbosityLevel))
			.replace("verify_command", settings.getVerifyCommand() == EsbmcVerifyCommand.K_INDUCTION ? "--k-induction" : "--falsification")
			.toList();
		
		//@formatter:on

		// '--partial-loops' is required as there may be some spurious loops
		// due to the "irregular" order of location representations.
		
		result.currentStage().logInfo("ESBMC command line parameters: " + String.join(" ", procArguments));
		result.setBackendAlgorithmId(settings.getAlgorithmId());

		try {
			BackendExecutor executor = BackendExecutor.create(procArguments, timeoutSec).canceler(canceler)
					.killForcibly(true)
					.logger(result.currentStage());
			if (!settings.isPrintExecOutputToConsole()) {
				// Treat all stdout as comment, don't print it to the console
				executor.stdoutCommentLinePattern(".*");
			}
			BackendExecutorResult execResult = executor.execute();

			result.setBackendStdout(execResult.getStdout());
			result.setBackendStderr(execResult.getStderr());
			
			// Write stdout to file if needed
			if (settings.isPrintExecOutputToFile()) {
				try {
					Path outFile = result.getOutputDirectoryOrCreateTemp()
							.resolve(result.getJobMetadata().getIdWithoutSpecialChars() + ".c.out");
					IoUtils.writeAllContent(outFile, execResult.getStdout());
					result.currentStage().logInfo("Console output (stdout) of ESBMC has been written to '%s'.",
							outFile.toAbsolutePath().toString());
				} catch (IOException e) {
					result.currentStage().logInfo("Unable to write the console output (stdout) of ESBMC to file. %s",
							e.getMessage());
				}
			}

			switch (execResult.getStatus()) {
			case Canceled: {
				result.currentStage().logWarning("The execution of ESBMC was canceled.");
				result.setResult(ResultEnum.Unknown);
				return execResult;
			}
			case Timeout: {
				result.currentStage().logWarning("The execution of ESBMC was interrupted due to timeout.");
				result.setResult(ResultEnum.Unknown);
				return execResult;
			}
			case Successful:
				return execResult;
			case Error: {
				result.setResult(ResultEnum.Error);
				throw new EsbmcException(
						"The execution of the verification backend was not successful: " + execResult.getStatus());
			}
			default:
				throw new UnsupportedOperationException("Unknown execution result status: " + execResult.getStatus());
			}
		} catch (BackendExecutionException e) {
			throw new EsbmcException(e.getMessage(), e);
		}
	}

	private static String toAbsolutePathStringIfNotNull(Path path) {
		if (path == null) {
			return null;
		} else {
			return path.toAbsolutePath().toString();
		}
	}

	@Override
	public CfaTransformationGoal getPreferredModelFormat() {
		if (this.variant == EsbmcModelVariant.CFD || this.variant == EsbmcModelVariant.CFD_STRUCTURED) {
			return CfaTransformationGoal.DECLARATION;
		} else if (this.variant == EsbmcModelVariant.CFI) {
			return CfaTransformationGoal.INSTANCE_ENUMERATED;
		} else {
			throw new UnsupportedOperationException("Unknown ESBMC backend variant: " + variant);
		}
	}

	@Override
	public RequirementRepresentationStrategy getPreferredRequirementFormat() {
		return RequirementRepresentationStrategy.IMPLICIT;
	}

	@Override
	public boolean isInliningRequired() {
		if (this.variant == EsbmcModelVariant.CFD || this.variant == EsbmcModelVariant.CFD_STRUCTURED) {
			return false;
		} else if (this.variant == EsbmcModelVariant.CFI) {
			return true;
		} else {
			throw new UnsupportedOperationException("Unknown ESBMC backend variant: " + variant);
		}
	}

	@Override
	public boolean areCfaAnnotationsRequired() {
		if (this.variant == EsbmcModelVariant.CFD_STRUCTURED) {
			return true;
		} else if (this.variant == EsbmcModelVariant.CFI || this.variant == EsbmcModelVariant.CFD) {
			return false;
		} else {
			throw new UnsupportedOperationException("Unknown ESBMC backend variant: " + variant);
		}
	}
}
