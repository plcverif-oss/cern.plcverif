/******************************************************************************
 * (C) Copyright CERN 2017-2023. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Xaver Fink	- modifications for ESBMC
 *****************************************************************************/
package cern.plcverif.library.backend.esbmc.model

import cern.plcverif.base.models.cfa.CfaInstanceSafeFactory
import cern.plcverif.base.models.cfa.cfabase.Transition
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariable
import cern.plcverif.base.models.cfa.cfainstance.AssignmentTransition
import cern.plcverif.base.models.cfa.cfainstance.AutomatonInstance
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance
import cern.plcverif.base.models.cfa.cfainstance.Variable
import cern.plcverif.base.models.expr.Literal
import cern.plcverif.base.models.expr.utils.ExprUtils
import cern.plcverif.library.backend.esbmc.EsbmcSettings
import cern.plcverif.support.codegen.generator.CFamilyExprTransformer
import cern.plcverif.support.codegen.generator.c.TypeToCType
import cern.plcverif.verif.interfaces.data.VerificationProblem
import cern.plcverif.verif.interfaces.data.VerificationResult
import cern.plcverif.verif.interfaces.data.cex.ICounterexample
import cern.plcverif.verif.interfaces.data.cex.InstanceCexStep
import cern.plcverif.verif.interfaces.data.cex.InstanceCounterexample
import cern.plcverif.verif.utils.backend.BackendNameTrace
import com.google.common.base.Preconditions
import java.util.List
import java.util.Map
import org.eclipse.emf.ecore.util.EcoreUtil

/**
 * MISSING_DOCS
 * 
 * Note: no calls are expected in the CFI. Therefore the method name sanitizer is not necessary either.
 */
class EsbmcModelBuilderCfi extends EsbmcModelBuilder {
	BackendNameTrace<AbstractVariable> variableNames = new BackendNameTrace([
		BackendNameTrace.basicNameSanitizer(it)
	]);

	CfaNetworkInstance cfi;
	TypeToCType typeTransformer = new TypeToCType();
	CFamilyExprTransformer exprTransformer;

	protected new(VerificationProblem verifProblem, VerificationResult result, EsbmcSettings settings) {
		super(verifProblem, result, settings);

		Preconditions.checkNotNull(typeTransformer);
		exprTransformer = CFamilyExprTransformer.createForCfi(typeTransformer);

		val cfa = verifProblem.model;
		Preconditions.checkArgument(cfa instanceof CfaNetworkInstance,
			"EsbmcModelBuilderCfi is only applicable to CFAs of type CfaNetworkInstance.")
		this.cfi = cfa as CfaNetworkInstance;
	}

	override protected getExprTransformer() {
		return exprTransformer;
	}

	override build() '''
		«fileHeader()»
		
		// Variables
		«FOR v : cfi.variables»
			«representVariable(v)»
		«ENDFOR»
		bool «BOC_MARKER»;
		bool «EOC_MARKER»;
		
		// Main
		void main() {
			«representAutomatonInstanceBody(cfi.mainAutomaton)»
		}
	'''

	private def representAutomatonInstanceBody(AutomatonInstance automaton) {
		return '''
			// Start with initial location
			goto «labelNames.toName(automaton.initialLocation)»;
			«FOR loc : automaton.locations»
				«representLocation(loc)» 
			«ENDFOR»
			
			// End of automaton
			«END_OF_AUTOMATON_LABEL»: ;
		''';
	}

	protected override CharSequence representTransitionAssignmentsAndCalls(Transition transition) {
		if (transition instanceof AssignmentTransition) {
			return '''
				«FOR amt : transition.assignments»
					«exprTransformer.toString(amt.leftValue)» = «exprTransformer.toString(amt.rightValue)»;
				«ENDFOR»
				«FOR amt : transition.assumeBounds»
					__ESBMC_assume(«exprTransformer.toString(amt)»);
				«ENDFOR»
			'''
		} else {
			throw new UnsupportedOperationException("Unsupported CFI transition: " + transition);
		}
	}

	/**
	 * E.g. {@code VarType varName};
	 */
	private def representVariable(AbstractVariable variable) {
		val varName = variableNames.toName(variable);
		val initValueStr = if (variable instanceof Variable) exprTransformer.toString(variable.initialValue).toString else null;
		// NOTE initial values of arrays are not handled, however it is assumed to have an enumerated model.
		return typeTransformer.variableDeclaration(variable.type, varName, initValueStr);
	}

	override ICounterexample parseTextualCex(List<Map<String, String>> valuemaps) {
		val ret = new InstanceCounterexample();
		var i = 0;

		val allVariableNames = valuemaps.map[it|it.keySet].flatten.toSet;

		for (Map<String, String> map : valuemaps) {
			i++;
			val step = new InstanceCexStep("Cycle " + i.toString, ret);

			for (varName : allVariableNames) {
				val varValuation = map.get(varName);
				val variable = variableNames.toModelElement(varName);
				// Array elements are not supported here (yet), thus we rely on array enumeration!
				if (variable === null || !(variable instanceof Variable)) {
					System.out.println("Skipped: " + varName);
				} else {
					if (varValuation !== null) {
						val parseSpecificValue = parseSpecificValue(varValuation, (variable as Variable).type);
						if (parseSpecificValue.isPresent) {
							step.setValue(CfaInstanceSafeFactory.INSTANCE.createVariableRef(variable as Variable),
								parseSpecificValue.get);
						} else {
							step.setValue(CfaInstanceSafeFactory.INSTANCE.createVariableRef(variable as Variable),
								ExprUtils.createLiteralFromString(varValuation, (variable as Variable).type));
						}
					} else {
						// assume that it is not present in the cex because it kept its original value
						val initValue = (variable as Variable).initialValue;
						if (initValue instanceof Literal) {
							step.setValue(CfaInstanceSafeFactory.INSTANCE.createVariableRef(variable as Variable),
								EcoreUtil.copy(initValue));
						} else {
							System.err.println("Not sure what to do.");
						}
					}
				}
			}
		}

		return ret;
	}
}
