/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.esbmc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsSerializer;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.help.SettingsHelp;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.verif.interfaces.IBackend;
import cern.plcverif.verif.interfaces.IBackendExtension;
import cern.plcverif.verif.job.VerificationJobOptions;
/*******************************************************************************
 * (C) Copyright CERN 2017-2023. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Xaver Fink	- modifications for ESBMC
 *******************************************************************************/
/**
 * Extension point to provide ESBMC backend.
 */
public class EsbmcBackendExtension implements IBackendExtension {
	private static final Logger LOGGER = LogManager.getLogger(EsbmcBackendExtension.class);
	public static final String CMD_ID = "esbmc";

	@Override
	public IBackend createBackend() {
		return new EsbmcBackend(loadDefaultSettings());
	}

	@Override
	public IBackend createBackend(SettingsElement settings) throws SettingsParserException {
		Settings compositeSettings = loadDefaultSettings();
		compositeSettings.overrideWith(settings);
		return new EsbmcBackend(compositeSettings);
	}

	public SettingsElement loadDefaultSettings() {
		String settingsPath = SettingsSerializer.replaceOsPlaceholder("/settings/default.${OS}.settings");
		try {
			String settingString = IoUtils.readResourceFile(
					settingsPath,
					this.getClass().getClassLoader());
			SettingsElement ret = SettingsSerializer.parseSettingsFromString(settingString).toSingle();
			ret.setValue(CMD_ID);
			return ret;
		} catch (SettingsParserException e) {
			LOGGER.debug("Unable to load the default settings from resource " + settingsPath + ".");
			return SettingsElement.createRootElement(VerificationJobOptions.BACKEND, CMD_ID);
		}
	}

	@Override
	public void fillSettingsHelp(SettingsHelp help) {
		help.addPluginDetails(CMD_ID, "ESBMC verification backend", 
			"Solves the verification problem by using the ESBMC model checker. Only applicable if job=verif.",	
			EsbmcBackendExtension.class);
		
		// Try to load the default settings
		EsbmcSettings defaults;
		try {
			defaults = SpecificSettingsSerializer.parse(loadDefaultSettings(), EsbmcSettings.class);
		} catch (SettingsParserException e) {
			// best effort (don't die on help creation)
			defaults = new EsbmcSettings();
		}
		
		// Fill the 'SettingsHelp' object
		SpecificSettingsSerializer.fillSettingsHelp(help, EsbmcSettings.class, defaults);
	}
}
