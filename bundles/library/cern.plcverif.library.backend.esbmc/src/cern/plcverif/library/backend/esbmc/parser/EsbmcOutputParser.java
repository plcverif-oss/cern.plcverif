/*******************************************************************************
 * (C) Copyright CERN 2017-2023. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Xaver Fink	- modifications for ESBMC
 *******************************************************************************/
package cern.plcverif.library.backend.esbmc.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.spi.LocaleNameProvider;

import org.eclipse.xtext.xbase.lib.Pair;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.logging.IPlcverifLogger;
import cern.plcverif.base.interfaces.data.result.JobStage;
import cern.plcverif.base.interfaces.data.result.StageStatus;
import cern.plcverif.library.backend.esbmc.EsbmcSettings;
import cern.plcverif.library.backend.esbmc.model.EsbmcModelBuilder;
import cern.plcverif.library.backend.esbmc.model.EsbmcModelBuilderCfd;
import cern.plcverif.library.backend.esbmc.model.ITextualCexParser;
import cern.plcverif.verif.interfaces.data.ResultEnum;
import cern.plcverif.verif.interfaces.data.VerificationResult;
import cern.plcverif.verif.interfaces.data.cex.DeclarationCounterexample;
import cern.plcverif.verif.interfaces.data.cex.ICounterexample;
import cern.plcverif.verif.interfaces.data.cex.InstanceCounterexample;

/**
 * ESBMC stdout output parser.
 */
public class EsbmcOutputParser {
	private String stdout;
	private List<String> linesToParse;
	private EsbmcSettings settings;
	private IPlcverifLogger logger;

	/**
	 * Creates a new ESBMC backend output parser.
	 * 
	 * @param esbmcStdout
	 *            The stdout output of ESBMC. Shall not be {@code null}.
	 * @param settings
	 *            Settings used for the ESBMC plug-in. Shall not be {@code null}.
	 * @param logger
	 *            Logger to be used. Shall not be {@code null}.
	 */
	public EsbmcOutputParser(CharSequence esbmcStdout, EsbmcSettings settings, IPlcverifLogger logger) {
		this.stdout = Preconditions.checkNotNull(esbmcStdout, "null output is not valid to parse").toString();
		this.linesToParse = Arrays.asList(stdout.split(System.lineSeparator()));
		this.settings = Preconditions.checkNotNull(settings);
		this.logger = Preconditions.checkNotNull(logger);
	}

	// public only for testing
	public List<Map<String, String>> parseCounterexampleToMaps() {
		int cycle = 0; // 2 steps per cycle - for BoC and EoC
		boolean nextLineIsAssignment = false;
		String currentContext = "?";

		Pattern pattern = Pattern.compile("([^=]+)\\s+=\\s+([^\\n]+)");

		List<Map<String, String>> variableValueMaps = new ArrayList<>();
		variableValueMaps.add(new HashMap<>());

		Set<String> seenVariables = new HashSet<>();
		for (String line : linesToParse) {
			// Check if it is a variable assignment line
			if (nextLineIsAssignment) {
				line = line.trim();
				Matcher matcher = pattern.matcher(line);
				if (matcher.find()) {
					List<String> variables = new ArrayList<>();
					List<String> values = new ArrayList<>();
					
					String variable = matcher.group(1);
					String value = matcher.group(2);
					Pattern structPattern = Pattern.compile("\\{\\s((\\S+\\s)+)\\}");
					Matcher structMatcher = structPattern.matcher(value);
					if (structMatcher.find()) {
						String structMatcherResult = structMatcher.group(1);
						List<String> structPairs = Arrays.asList(structMatcherResult.split(",\\s"));
						for (String pair : structPairs) {
							List<String> varAndValue = Arrays.asList(structMatcherResult.split("="));
							if (varAndValue.size() == 1) {
								variables.add(variable.trim());
								values.add(varAndValue.get(0).trim());
							} else {
								variables.add((variable+varAndValue.get(0)).trim());
								values.add(varAndValue.get(1).trim());
							}
						}
					} else {
						variables.add(matcher.group(1).trim());
						values.add(matcher.group(2).trim());
					}
					for (int i = 0; i<variables.size();i++) {
						variable = variables.get(i);
						value = values.get(i);
						
						// Turn 0 (00000000 00000000 00000000 00000000) to 0
						if (value.matches("\\d+\\s\\((\\d+\\s*)+\\)")) {
							value = value.split("\\s")[0];
						}
						
						if (seenVariables.contains(variable)) {
							cycle++;
							variableValueMaps.add(new HashMap<>(variableValueMaps.get(cycle - 1)));
							seenVariables.clear();
						}
						seenVariables.add(variable);
	
						if (variable.equals(EsbmcModelBuilderCfd.CONTEXT_VARIABLE)) {
							// context update
							currentContext = value.replace("&", "");
						} else {
							variable = variable.replace(EsbmcModelBuilderCfd.CONTEXT_VARIABLE + "\\.", currentContext + ".");
							
							if (variable.matches(".+\\[\\d+ll?\\]")) {
								
								// [5ll] --> [5]
								variable = variable.replaceAll("\\[(\\d+)ll?\\]", "[$1]");
							}
							
							parseAssignmentLine(variable, value, variableValueMaps.get(cycle));
						}
					}
					
				} else {
					logger.logDebug("Unable to parse the following line: '%s'.", line);
				}
				nextLineIsAssignment = false;
			}

			// Check if the next line will be an assignment line
			if (line.contains("-----------------------------")) {
				nextLineIsAssignment = true;
				continue;
			}
		}

		return variableValueMaps;
	}

	private static void parseAssignmentLine(String variable, String value, Map<String, String> currentVariableValueMap) {
		currentVariableValueMap.put(variable, value);
	}

	/**
	 * Parses the stdout output of the ESBMC execution and fills the given
	 * {@link VerificationResult} object.
	 * <ul>
	 * <li>It fills the result of the verification
	 * ({@link VerificationResult#setResult(ResultEnum)}),
	 * <li>If the result is {@link ResultEnum#Unknown}, it tries to diagnose the
	 * problem and fills adds log messages if possible,
	 * <li>If the result is {@link ResultEnum#Violated}, it tries to parse the
	 * counterexample.
	 * </ul>
	 * 
	 * @param result
	 *            The verification result object to be filled.
	 * @param textCexParser
	 *            The object to be used to resolve the textual representation of
	 *            variables/fields and literals.
	 */
	public void parseAndFillResult(VerificationResult result, ITextualCexParser textCexParser) {
		// Fetch result from stdout
		result.setResult(parseResult());

		if (result.getResult() == ResultEnum.Unknown) {
			diagnoseProblem(result);

			if (settings.getEsbmcVerbosity() < 4) {
				result.currentStage().logWarning("The verbosity parameter for the ESBMC backend is very low (%s). This may make parsing the result impossible.", settings.getEsbmcVerbosity());
			}
		}

		if (result.getResult() == ResultEnum.Violated) {
			// parse counterexample
			List<Map<String, String>> valueMaps = parseCounterexampleToMaps();

			ICounterexample cex = textCexParser.parseTextualCex(valueMaps);
			if (cex instanceof InstanceCounterexample) {
				result.setInstanceCounterexample((InstanceCounterexample) cex);
			} else if (cex instanceof DeclarationCounterexample) {
				result.setDeclarationCounterexample((DeclarationCounterexample) cex);
			} else {
				throw new UnsupportedOperationException("Unknown counterexample type: " + cex);
			}
			
			result.currentStage().logWarning("Make sure to check whether the counterexample is valid. In rare cases the reported counterexample may not be realizable in the original program.");
		}
		
		if (result.getResult() == ResultEnum.Satisfied) {
			result.currentStage().logWarning("In rare cases ESBMC may produce false positives due to experimental invariant calculation features. This result does not provide full safety.");
		}
		
	}

	/**
	 * Determines (based on the given stdout output of the ESBMC backend) whether
	 * the verification was successful or not, and if the requirement was
	 * violated or not.
	 * 
	 * @return The {@link ResultEnum} representing the result.
	 */
	public ResultEnum parseResult() {
		if (stdout.contains("VERIFICATION FAILED") || stdout.contains("Violated property:")) {
			return ResultEnum.Violated;
		} else if (stdout.contains("VERIFICATION SUCCESSFUL")) {
			return ResultEnum.Satisfied;
		} else {
			return ResultEnum.Unknown;
		}
	}

	/**
	 * Attempts to log the reason of {@link ResultEnum#Unknown} result.
	 */
	private void diagnoseProblem(VerificationResult result) {
		JobStage stage = Preconditions.checkNotNull(result.currentStage());
		Optional<CharSequence> backendStderr = result.getBackendStderr();
		String combinedOutput = stdout + backendStderr.orElse("");
		if (backendStderr.isPresent() && backendStderr.get().toString().contains("Timeout.")) {
			// Timeout, nothing to do, it was already logged.
		} else if (combinedOutput.contains("'CL' is not recognized as an internal or external command") || combinedOutput.contains("CL Preprocessing failed")) {
			stage.logError(
					"Unable to execute the ESBMC backend: 'CL' is not found. Check if the C compiler is installed and set up correctly. On Windows, you have to install Visual Studio with the C++ command line tools and set up the path of the vcvars64.bat file.");
			stage.setStatus(StageStatus.Unsuccessful);
		} else if (Pattern.compile("Cannot open include file: '.*\\.h': No such file or directory", Pattern.DOTALL)
				.matcher(combinedOutput).find()) {
			stage.logError(
					"Unable to execute the ESBMC backend: some include files are not found. Check if the C compiler is installed and set up correctly.");
			stage.setStatus(StageStatus.Unsuccessful);
		} else if (Pattern.compile("fatal error C1085: Cannot write.*No space left on device", Pattern.DOTALL)
				.matcher(combinedOutput).find()) {
			stage.logError(
					"Unable to execute the ESBMC backend: file writing failed due to lack of free disk space.");
			stage.setStatus(StageStatus.Unsuccessful);
		} else {
			stage.logWarning("Unknown error occurred during the execution of ESBMC.");
		}
	}
}
