/*******************************************************************************
 * (C) Copyright CERN 2017-2023. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Xaver Fink
 *******************************************************************************/
package cern.plcverif.library.backend.esbmc.model

import cern.plcverif.support.codegen.generator.CFamilyExprTransformer
import cern.plcverif.support.codegen.generator.c.TypeToCType
import cern.plcverif.base.models.expr.Nondeterministic

class EsbmcExprTransformer extends CFamilyExprTransformer {
	new(CharSequence targetContextName, boolean targetContextIsPointer, TypeToCType typeToString) {
		super(targetContextName, targetContextIsPointer, typeToString)
	}
	
	static def createForCfi(TypeToCType typeTransformer) {
		return new EsbmcExprTransformer(null, false, typeTransformer);
	}
	
	static def createForCfd(CharSequence targetContextName, boolean targetContextIsPointer, TypeToCType typeToString) {
		return new EsbmcExprTransformer(targetContextName, targetContextIsPointer, typeToString);
	}
		
	override dispatch CharSequence toString(Nondeterministic expr) {
		return '''nondet_«super.typeToString.typeName(expr.type)»()'''
	}
}