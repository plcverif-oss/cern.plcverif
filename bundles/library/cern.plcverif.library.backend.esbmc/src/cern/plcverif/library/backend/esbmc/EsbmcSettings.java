/*******************************************************************************
 * (C) Copyright CERN 2017-2023. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Xaver Fink	- modifications for ESBMC
 *******************************************************************************/
package cern.plcverif.library.backend.esbmc;

import static cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory.OPTIONAL;

import java.nio.file.Path;

import cern.plcverif.base.common.settings.specific.AbstractSpecificSettings;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsElement;

/**
 * ESBMC backend settings
 */
public class EsbmcSettings extends AbstractSpecificSettings {	
	public enum EsbmcModelVariant {
		CFI, CFD, CFD_STRUCTURED
	}
	
	public enum EsbmcVerifyCommand {
		K_INDUCTION, FALSIFICATION
	}
	
	public static final String TIMEOUT = "timeout";
	@PlcverifSettingsElement(name = TIMEOUT, description = "Timeout of the verification backend, in seconds.", mandatory = OPTIONAL)
	private int timeoutInSec = 60;
	
	public static final String VERIFY_COMMAND = "verify_command";
	@PlcverifSettingsElement(name = VERIFY_COMMAND, description = "The verify command we are using.", mandatory = OPTIONAL)
	private EsbmcVerifyCommand verifyCommand = EsbmcVerifyCommand.K_INDUCTION;
	
	public static final String BINARY_PATH = "binary_path";
	@PlcverifSettingsElement(name = BINARY_PATH, description = "Full path of the ESBMC binary.")
	private Path esbmcBinaryPath = null;
	
	public static final String EXEC_OUTPUT_TO_CONSOLE = "exec_output_to_console";
	@PlcverifSettingsElement(name = EXEC_OUTPUT_TO_CONSOLE, description = "If true, the output of the backend execution's output will be shown on the console.", mandatory = OPTIONAL)
	private boolean printExecOutputToConsole = false;
	
	public static final String PRINT_STDOUT_TO_FILE = "exec_output_to_file";
	@PlcverifSettingsElement(name = PRINT_STDOUT_TO_FILE, description = "If true, the output of the backend execution's output will be written into a file, next to the model.", mandatory = OPTIONAL)
	private boolean printExecOutputToFile = false;
	
	public static final String VERBOSITY = "verbosity";
	@PlcverifSettingsElement(name = VERBOSITY, description = "ESBMC output verbosity (1..10). With verbosity levels 1..3 the result of verification is not printed, thus it cannot be parsed by PLCverif.", mandatory = OPTIONAL)
	private int esbmcVerbosity = 7;
	
	public static final String PARALLELIZE_COMPUTATION = "parallelize";
	@PlcverifSettingsElement(name = PARALLELIZE_COMPUTATION, description = "If true, ESBMC will try to use parallelization for the k-induction branches.", mandatory = OPTIONAL)
	private boolean parallelizeComputation = false;
	
	public static final String MODEL_VARIANT = "model_variant";
	@PlcverifSettingsElement(name = MODEL_VARIANT, description = "Model variant (CFA declaration or instance) to be used for the the generation of the C representation.", mandatory = OPTIONAL)
	private EsbmcModelVariant modelVariant = EsbmcModelVariant.CFD;
	
	public EsbmcSettings() {}
	
	public EsbmcSettings(EsbmcModelVariant modelVariant) {
		this.modelVariant = modelVariant;
	}
	
	public EsbmcVerifyCommand getVerifyCommand() {
		return verifyCommand;
	}
	
	public void setVerifyCommand(EsbmcVerifyCommand verifyCommand) {
		this.verifyCommand = verifyCommand;
	}
	
	public int getTimeoutInSec() {
		return timeoutInSec;
	}
	
	public Path getEsbmcBinaryPath() {
		return esbmcBinaryPath;
	}
	
	public boolean isPrintExecOutputToConsole() {
		return printExecOutputToConsole;
	}
	
	public boolean isPrintExecOutputToFile() {
		return printExecOutputToFile;
	}
	
	public boolean isParallelizeComputation() {
		return parallelizeComputation;
	}
	
	public EsbmcModelVariant getModelVariant() {
		return modelVariant;
	}
	
	public void setModelVariant(EsbmcModelVariant modelVariant) {
		this.modelVariant = modelVariant;
	}
	
	public int getEsbmcVerbosity() {
		return esbmcVerbosity;
	}

	public String getAlgorithmId() {
		return String.format("%s", modelVariant);
	}
}
