/*******************************************************************************
 * (C) Copyright CERN 2017-2023. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Xaver Fink	- modifications for ESBMC
 *******************************************************************************/
package cern.plcverif.library.backend.esbmc.exception;

/**
 * Generic wrapper of an irreparable error happening in the ESBMC backend plug-in.
 *
 */
public class EsbmcException extends Exception {
	private static final long serialVersionUID = 8447493168839094309L;

	public EsbmcException(String message) {
		super(message);
	}
	
	public EsbmcException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
