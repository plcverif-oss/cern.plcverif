/******************************************************************************
 * (C) Copyright CERN 2023-2024. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Xaver Fink - initial implementation
 *****************************************************************************/
package cern.plcverif.library.requirement.fret.ltlmonitors;

import java.io.FileDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.collect.Lists;

import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory;
import cern.plcverif.base.models.cfa.builder.VariableAssignmentSkeleton;
import cern.plcverif.base.models.cfa.cfabase.IfLocationAnnotation;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.Field;
import cern.plcverif.base.models.expr.BinaryExpression;
import cern.plcverif.base.models.expr.BinaryLtlExpression;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.UnaryExpression;
import cern.plcverif.base.models.expr.UnaryLtlExpression;
import cern.plcverif.base.models.expr.UnaryLtlOperator;
import cern.plcverif.base.models.expr.utils.ExprUtils;
import cern.plcverif.base.models.expr.utils.TlExprUtils;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.requirement.PlcCycleRepresentation;

/**
 * This class is used to generate monitors for LTL properties
 */
public class MonitorFactory {
	
	private static int varIndex = 0;
	
	public enum LTLExpression {
		HISTORICALLY("H", HistoricallyMonitor.class), 
		LAST("Y", LastMonitor.class),
		WEAK_LAST("Z", WeakLastMonitor.class),
		ONCE("O", OnceMonitor.class),
		SINCE("S", SinceMonitor.class),
		NON_LTL("NON_LTL", NonLtlMonitor.class);
		
		private String stringRepresentation;
		private Class<? extends MonitorOperation> monitorClass;
		
		LTLExpression(String stringRepresentation, Class<? extends MonitorOperation> monitorClass) {
	        this.stringRepresentation = stringRepresentation;
	        this.monitorClass = monitorClass;
	    }
	 
	    public String getStringRepresentation() {
	        return stringRepresentation;
	    }
	    
	    public Class<? extends MonitorOperation> getMonitorClass() {
	        return monitorClass;
	    }
	 
	    public static Optional<LTLExpression> get(Expression expression) {
	    	String stringRepresentation;
	    	if (TlExprUtils.isLtl(expression)) {
				if (expression instanceof UnaryLtlExpression) {
					stringRepresentation = ((UnaryLtlExpression)expression).getOperator().getLiteral();
				}else {
					stringRepresentation = ((BinaryLtlExpression)expression).getOperator().getLiteral();
				}
	    	} else {
	    		stringRepresentation = "NON_LTL";
	    	}
	    	
	    	
	        return Arrays.stream(LTLExpression.values())
	            .filter(ltlexpr -> ltlexpr.stringRepresentation.equals(stringRepresentation))
	            .findFirst();
	    }
	}
	
	private static CfaDeclarationSafeFactory factory = CfaDeclarationSafeFactory.INSTANCE;
	
	public static void preprocess(Expression expressionTree, Expression parent, int parentIndex) {
		
		if (LTLExpression.get(expressionTree).orElseGet(null) == LTLExpression.ONCE && ((UnaryLtlExpression)expressionTree).isBounded() && ((UnaryLtlExpression)expressionTree).getLowerBound() == ((UnaryLtlExpression)expressionTree).getUpperBound()) {
			int n = ((UnaryLtlExpression)expressionTree).getLowerBound();
			if (n == 0) {
				throw new UnsupportedOperationException("Not implemented O[0,0] as there is no reason to use it.");
			} else if (parent == null) {
				throw new UnsupportedOperationException("Not implemented O[0,0] as outermost operation.");
			}
			
			Expression child = ExprUtils.exprCopy(((UnaryExpression)expressionTree).getOperand());
			Expression currentChild = factory.createUnaryLtlExpression(child, UnaryLtlOperator.Y);
			preprocess(child, currentChild, 0);	
			for(int i = 0; i < n-1; i++) {
				currentChild = factory.createUnaryLtlExpression(currentChild, UnaryLtlOperator.Y);
			}
			if (parent instanceof UnaryExpression) {
				((UnaryExpression) parent).setOperand(currentChild);
			}else if (parent instanceof BinaryExpression) {
				if (parentIndex == 0) {
					((BinaryExpression) parent).setLeftOperand(currentChild);
				}else {
					((BinaryExpression) parent).setRightOperand(currentChild);
				}
			}
		} else if (expressionTree instanceof UnaryExpression) {
			preprocess(((UnaryExpression)expressionTree).getOperand(), expressionTree, 0);
		} else if (expressionTree instanceof BinaryExpression) {
			preprocess(((BinaryExpression)expressionTree).getLeftOperand(), expressionTree, 0);
			preprocess(((BinaryExpression)expressionTree).getRightOperand(), expressionTree, 1);
		}
	}
	
	
	/** 
	 * This function receives the ptLTL requirement as input and recursively parses it and creates monitor updates to translate the expression into pure boolean.
	 * 
	 * @param the assignment transition on which we create the monitor updates
	 * @param the ptLTL requirement as a parsed expression
	 * @param the main CFA network
	 * @return the expression to be put into the assert at EoC
	 */
	public static List<Expression> createMonitorRecursively(List<VariableAssignmentSkeleton> initAssignments, List<VariableAssignmentSkeleton> preAssignments, List<VariableAssignmentSkeleton> postAssignments, Expression expressionTree, CfaNetworkDeclaration cfaNetwork, boolean useSeconds, VerificationProblem problem) {		
		LTLExpression expression = LTLExpression.get(expressionTree).orElseGet(null);
		MonitorOperation operation = null;
		Class[] cArg = new Class[3]; 
		cArg[0] = CfaNetworkDeclaration.class; 
		cArg[1] = boolean.class;
		cArg[2] = VerificationProblem.class;
		try {
			operation = expression.monitorClass.getDeclaredConstructor(cArg).newInstance(cfaNetwork, useSeconds, problem);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		
		if (! PlcCycleRepresentation.findGlobalField(cfaNetwork, "isFirstCycle").isPresent()) {
			Field isFirstCycleField = factory.createField("isFirstCycle", cfaNetwork.getRootDataStructure(),
					factory.createBoolType());
			factory.createInitialAssignment(isFirstCycleField, factory.createBoolLiteral(true));
		}
		
		List<Field> monitorFields = createMonitorVariablesWrapper(operation, cfaNetwork, expressionTree);
		operation.createUpdateExpression(monitorFields, initAssignments, preAssignments, postAssignments, expressionTree);
		
		return Lists.newArrayList(factory.createFieldRef(monitorFields.get(0)), factory.createFieldRef(monitorFields.get(1)));
	}
	
	
	public static void addIsFirstCycleAssignment(List<VariableAssignmentSkeleton> assignments, CfaNetworkDeclaration cfaNetwork) {
		assignments.add(VariableAssignmentSkeleton.create(factory.createFieldRef(PlcCycleRepresentation.findGlobalField(cfaNetwork, "isFirstCycle").orElseGet(null)), factory.createBoolLiteral(false)));
	}

	
	private static List<Field> createMonitorVariablesWrapper(MonitorOperation operation, CfaNetworkDeclaration cfaNetwork, Expression expressionTree) {
		int index = nextVarIndex();
		String prefix = operation.getVariablePrefix();
		
		Field fieldPre = factory.createField(prefix+"_pre_"+index, cfaNetwork.getRootDataStructure(),
				factory.createBoolType());
		Field fieldPost = factory.createField(prefix+"_post_"+index, cfaNetwork.getRootDataStructure(),
				factory.createBoolType());
		
		return operation.createInitialization(fieldPre, fieldPost, expressionTree, index);
	}
	
	

// TODO: fix this and put into some kind of Ltl reducer class
//	private static boolean removeTautology(BinaryExpression parent, Expression result) {
//		if (parent instanceof BinaryLogicExpression) {
//			if (((BinaryLogicExpression) parent).getOperator().equals(BinaryLogicOperator.AND)) {
//				return (LtlReducer.is parent.getLeftOperand() || parent.getRightOperand());
//			}
//			if (((BinaryLogicExpression) parent).getOperator().equals(BinaryLogicOperator.OR)) {
//				return (parent.getLeftOperand() || parent.getRightOperand());
//			}
//		}
//		
//		return false;
//	}	
	
	
	private static int nextVarIndex() {
		varIndex += 1;
		return varIndex-1;
	}
	
}
