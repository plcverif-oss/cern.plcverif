/******************************************************************************
 * (C) Copyright CERN 2023-2024. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Xaver Fink - initial implementation
 *****************************************************************************/
package cern.plcverif.library.requirement.fret.ltlmonitors;

import java.util.ArrayList;
import java.util.List;

import cern.plcverif.base.models.cfa.builder.VariableAssignmentSkeleton;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.Field;
import cern.plcverif.base.models.expr.BinaryLtlExpression;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.utils.ExprUtils;
import cern.plcverif.verif.interfaces.data.VerificationProblem;

public class SinceMonitor extends MonitorOperation {

	protected SinceMonitor(CfaNetworkDeclaration cfaNetwork, boolean useExplicitTimeForBounds, VerificationProblem problem) {
		super(cfaNetwork, useExplicitTimeForBounds, problem);
	}

	@Override
	public List<Field> createInitialization(Field fieldPre, Field fieldPost, Expression expressionTree, int index) {
		List<Field> monitorFields = new ArrayList<>();
		monitorFields.add(fieldPre);
		monitorFields.add(fieldPost);
		
		factory.createInitialAssignment(fieldPre, factory.createBoolLiteral(true));
		factory.createInitialAssignment(fieldPost, factory.createBoolLiteral(true));
		
		return monitorFields;
	}

	@Override
	public void createUpdateExpression(List<Field> monitorVariables, List<VariableAssignmentSkeleton> initAssignments, List<VariableAssignmentSkeleton> preAssignments, List<VariableAssignmentSkeleton> postAssignments, Expression expressionTree) {
		Expression assignment;
		Expression initAssignment;
		
		List<Expression> childLeft = MonitorFactory.createMonitorRecursively(initAssignments, preAssignments, postAssignments, ((BinaryLtlExpression)expressionTree).getLeftOperand(), cfaNetwork, useExplicitTimeForBounds, problem);
		List<Expression> childRight = MonitorFactory.createMonitorRecursively(initAssignments, preAssignments, postAssignments, ((BinaryLtlExpression)expressionTree).getRightOperand(), cfaNetwork, useExplicitTimeForBounds, problem);
		assignment = factory.or(factory.and(factory.createFieldRef(monitorVariables.get(0)), childLeft.get(1)), childRight.get(1));
		initAssignment = ExprUtils.exprCopy(assignment);
		
		postAssignments.add(VariableAssignmentSkeleton.create(factory.createFieldRef(monitorVariables.get(1)), assignment));
		preAssignments.add(VariableAssignmentSkeleton.create(factory.createFieldRef(monitorVariables.get(0)), factory.createFieldRef(monitorVariables.get(1))));
		initAssignments.add(VariableAssignmentSkeleton.create(factory.createFieldRef(monitorVariables.get(1)), initAssignment));
		
	}
	
	@Override
	public String getVariablePrefix() {
		return "since";
	}

}
