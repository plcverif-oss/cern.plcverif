/******************************************************************************
 * (C) Copyright CERN 2023-2024. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Xaver Fink - initial implementation
 *****************************************************************************/
package cern.plcverif.library.requirement.fret.ltlmonitors;

import java.util.ArrayList;
import java.util.List;

import cern.plcverif.base.models.cfa.builder.VariableAssignmentSkeleton;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.Field;
import cern.plcverif.base.models.expr.BinaryExpression;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.UnaryExpression;
import cern.plcverif.base.models.expr.utils.ExprUtils;
import cern.plcverif.base.models.expr.utils.TlExprUtils;
import cern.plcverif.library.requirement.fret.exceptions.FretRequirementException;
import cern.plcverif.library.requirement.fret.exceptions.FretRequirementRuntimeException;
import cern.plcverif.verif.interfaces.data.VerificationProblem;

public class NonLtlMonitor extends MonitorOperation {

	protected NonLtlMonitor(CfaNetworkDeclaration cfaNetwork, boolean useExplicitTimeForBounds, VerificationProblem problem) {
		super(cfaNetwork, useExplicitTimeForBounds, problem);
	}

	@Override
	public List<Field> createInitialization(Field fieldPre, Field fieldPost, Expression expressionTree, int index) {
		List<Field> monitorFields = new ArrayList<>();
		monitorFields.add(fieldPre);
		monitorFields.add(fieldPost);
		
		factory.createInitialAssignment(fieldPre, factory.createBoolLiteral(true));
		factory.createInitialAssignment(fieldPost, factory.createBoolLiteral(true));
		
		return monitorFields;
	}

	@Override
	public void createUpdateExpression(List<Field> monitorVariables, List<VariableAssignmentSkeleton> initAssignments, List<VariableAssignmentSkeleton> preAssignments, List<VariableAssignmentSkeleton> postAssignments, Expression expressionTree) {
		// Break if remaining tree is simple boolean expression
		if (TlExprUtils.containsNoTl(expressionTree)) {
			initAssignments.add(VariableAssignmentSkeleton.create(factory.createFieldRef(monitorVariables.get(1)), ExprUtils.exprCopy(expressionTree)));
			postAssignments.add(VariableAssignmentSkeleton.create(factory.createFieldRef(monitorVariables.get(1)), ExprUtils.exprCopy(expressionTree)));
			preAssignments.add(VariableAssignmentSkeleton.create(factory.createFieldRef(monitorVariables.get(0)), factory.createFieldRef(monitorVariables.get(1))));
			return;
		}
		
		// Else recursively parse tree to deal with LTL expressions
		Expression result;
		if (expressionTree instanceof BinaryExpression) {
			
//			if(removeTautology((BinaryExpression)expressionTree, result)) {
//				break;
//			}
			
			List<Expression> childLeft = MonitorFactory.createMonitorRecursively(initAssignments, preAssignments, postAssignments, ((BinaryExpression)expressionTree).getLeftOperand(), cfaNetwork, useExplicitTimeForBounds, problem);
			List<Expression> childRight = MonitorFactory.createMonitorRecursively(initAssignments, preAssignments, postAssignments, ((BinaryExpression)expressionTree).getRightOperand(), cfaNetwork, useExplicitTimeForBounds, problem);
			result = ExprUtils.exprCopy(expressionTree);
			((BinaryExpression)result).setLeftOperand(childLeft.get(1));
			((BinaryExpression)result).setRightOperand(childRight.get(1));
		} else if (expressionTree instanceof UnaryExpression) {
			List<Expression> child = MonitorFactory.createMonitorRecursively(initAssignments, preAssignments, postAssignments, ((UnaryExpression)expressionTree).getOperand(), cfaNetwork, useExplicitTimeForBounds, problem);
			result = ExprUtils.exprCopy(expressionTree);
			((UnaryExpression)result).setOperand(child.get(1));
		} else {
			throw new FretRequirementRuntimeException("This should never be reached.");
		}
		initAssignments.add(VariableAssignmentSkeleton.create(factory.createFieldRef(monitorVariables.get(1)), ExprUtils.exprCopy(result)));
		postAssignments.add(VariableAssignmentSkeleton.create(factory.createFieldRef(monitorVariables.get(1)), result));
		preAssignments.add(VariableAssignmentSkeleton.create(factory.createFieldRef(monitorVariables.get(0)), factory.createFieldRef(monitorVariables.get(1))));
			
	}
	
	@Override
	public String getVariablePrefix() {
		return "non_ltl";
	}

}
