/******************************************************************************
 * (C) Copyright CERN 2023-2024. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Xaver Fink - initial implementation
 *****************************************************************************/
package cern.plcverif.library.requirement.fret.ltlmonitors;

import java.util.List;

import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory;
import cern.plcverif.base.models.cfa.builder.VariableAssignmentSkeleton;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.Field;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.IntType;
import cern.plcverif.base.models.expr.UnaryLtlExpression;
import cern.plcverif.verif.interfaces.data.VerificationProblem;

public abstract class MonitorOperation {
	
	CfaDeclarationSafeFactory factory = CfaDeclarationSafeFactory.INSTANCE;
	boolean useExplicitTimeForBounds = false;
	CfaNetworkDeclaration cfaNetwork;
	VerificationProblem problem;
	
	protected MonitorOperation(CfaNetworkDeclaration cfaNetwork, boolean useExplicitTimeForBounds, VerificationProblem problem) {
		this.cfaNetwork = cfaNetwork;
		this.useExplicitTimeForBounds = useExplicitTimeForBounds;
		this.problem = problem;
	}
	
	abstract String getVariablePrefix();
	
	abstract List<Field> createInitialization(Field fieldPre, Field fieldPost, Expression expressionTree, int index);
	
	abstract void createUpdateExpression(List<Field> monitorVariables, List<VariableAssignmentSkeleton> initAssignments, List<VariableAssignmentSkeleton> preAssignments, List<VariableAssignmentSkeleton> postAssignments, Expression expressionTree);
	
	
	void createBoundedInitialization(UnaryLtlExpression expressionTree, int namingIndex, List<Field> monitorFields) {
		throw new UnsupportedOperationException("The operation "+this.getVariablePrefix()+" does not have a bounded version implemented.");
	}
	
	
	void createBoundedUpdate(List<VariableAssignmentSkeleton> postAssignments, List<Field> monitorVariables, UnaryLtlExpression expressionTree, Expression phi) {
		throw new UnsupportedOperationException("The operation "+this.getVariablePrefix()+" does not have a bounded version implemented.");
	}
	
	IntType createCounterType() {
		return factory.createIntType(useExplicitTimeForBounds,(useExplicitTimeForBounds ? 32 : 8));
	}
}
