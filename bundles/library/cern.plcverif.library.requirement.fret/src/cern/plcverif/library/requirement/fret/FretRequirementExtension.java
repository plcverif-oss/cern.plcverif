/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Zsofia Adam - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.requirement.fret;

import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsSerializer;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.help.SettingsHelp;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.verif.interfaces.IRequirementExtension;
import cern.plcverif.verif.requirement.AbstractRequirementSettings;

public class FretRequirementExtension implements IRequirementExtension {
	public static final String CMD_ID = "fret";

	@Override
	public FretRequirement createRequirement() {
		try {
			return createRequirement(null);
		} catch (SettingsParserException e) {
			throw new PlcverifPlatformException(
					"Unable to load the default settings for the 'requirement.fret' plug-in.", e);
		}
	}

	@Override
	public FretRequirement createRequirement(SettingsElement settings) throws SettingsParserException {
		// Load default settings
		SettingsElement compositeSettings = SettingsSerializer
				.loadDefaultSettings("/settings", CMD_ID, this.getClass().getClassLoader()).toSingle();
		if (settings != null) {
			compositeSettings.overrideWith(settings);
		}

		// Parse to plugin-specific format
		FretRequirementSettings fretSettings = SpecificSettingsSerializer.parse(compositeSettings,
				FretRequirementSettings.class);

		return new FretRequirement(fretSettings);	
	}

	@Override
	public void fillSettingsHelp(SettingsHelp help) {
		help.addPluginDetails(CMD_ID, "Fret requirement",
				"Represents the requirement based on a requirement created in FRET.",
				FretRequirementExtension.class);

		// The settings specific to this plug-in (i.e., not in
		// AbstractRequirementSettings) are
		// represented manually to be able to include the pattern IDs as
		// permitted values.
		SpecificSettingsSerializer.fillSettingsHelp(help, AbstractRequirementSettings.class);
	}
}
