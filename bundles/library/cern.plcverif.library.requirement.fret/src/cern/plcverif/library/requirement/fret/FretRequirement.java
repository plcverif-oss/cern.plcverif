/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Zsofia Adam - initial API and implementation
 *   Xaver Fink - monitor generation
 *******************************************************************************/
package cern.plcverif.library.requirement.fret;

import static cern.plcverif.verif.requirement.PlcCycleRepresentation.createBocField;
import static cern.plcverif.verif.requirement.PlcCycleRepresentation.createEocField;
import static cern.plcverif.verif.requirement.PlcCycleRepresentation.representPlcCycle;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableBiMap.Builder;
import com.google.common.collect.Lists;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.formattedstring.BasicFormattedString;
import cern.plcverif.base.common.formattedstring.Formats.ItalicFormat;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.exceptions.SettingsSerializerException;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.interfaces.IParserLazyResult;
import cern.plcverif.base.interfaces.data.result.StageStatus;
import cern.plcverif.base.interfaces.exceptions.AtomParsingException;
import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory;
import cern.plcverif.base.models.cfa.builder.TransitionBuilder;
import cern.plcverif.base.models.cfa.builder.VariableAssignmentSkeleton;
import cern.plcverif.base.models.cfa.cfabase.Transition;
import cern.plcverif.base.models.cfa.cfadeclaration.AssignmentTransition;
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.Field;
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef;
import cern.plcverif.base.models.cfa.cfadeclaration.VariableAssignment;
import cern.plcverif.base.models.expr.AtomicExpression;
import cern.plcverif.base.models.expr.BeginningOfCycle;
import cern.plcverif.base.models.expr.EndOfCycle;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.string.ExprToString;
import cern.plcverif.base.models.expr.utils.TlExprUtils;
import cern.plcverif.base.models.exprparser.ExpressionParser;
import cern.plcverif.base.models.exprparser.exceptions.ExpressionParsingException;
import cern.plcverif.base.models.exprparser.exprLang.ValuePlaceholder;
import cern.plcverif.library.requirement.fret.exceptions.FretRequirementException;
import cern.plcverif.library.requirement.fret.ltlmonitors.MonitorFactory;
import cern.plcverif.verif.interfaces.IRequirement;
import cern.plcverif.verif.interfaces.data.RequirementRepresentationStrategy;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.VerificationResult;
import cern.plcverif.verif.requirement.PlcCycleRepresentation;
import cern.plcverif.verif.requirement.PlcCycleRepresentation.FieldsInCategories;

public class FretRequirement implements IRequirement {
	private static final CfaDeclarationSafeFactory FACTORY = CfaDeclarationSafeFactory.INSTANCE;
	private FretRequirementSettings settings;
	
	public FretRequirement(FretRequirementSettings settings) {
		this.settings = settings;
	}
	
	@Override
	public SettingsElement retrieveSettings() {
		try {
			SettingsElement ret = SpecificSettingsSerializer.toGenericSettings(settings);
			ret.setValue(FretRequirementExtension.CMD_ID);
			return ret;
		} catch (SettingsParserException | SettingsSerializerException e) {
			throw new PlcverifPlatformException("Unable to retrieve the settings of the requirement.fret plugin: "+e.getMessage());
		}	
	}

	/**
	 * This method is almost the same as the method of {@link PatternRequirement}
	 * with the same name.
	 * This method performs the following steps:
	 * <ul>
	 * <li>Loads and resolves the list of fields treated as inputs, parameters
	 * and the eventual input bindings,
	 * <li>Explores the variables which should be represented as inputs (even if
	 * they are not explicitly mentioned in the given verification problem),
	 * <li>Represents the PLC scan cycle (i.e. it encloses the main automaton in
	 * an infinite loop),
	 * <li>Creates an {@link Expression} representing the described property as
	 * a temporal logic expression and adds it to the given
	 * {@link VerificationProblem},
	 * </ul>
	 */
	@Override
	public void fillVerificationProblem(VerificationProblem data, IParserLazyResult parserResult,
			VerificationResult result, RequirementRepresentationStrategy reqRepresentation) {
		Preconditions.checkNotNull(data, "data");
		Preconditions.checkNotNull(parserResult, "parserResult");
		Preconditions.checkNotNull(result, "result");
		Preconditions.checkNotNull(reqRepresentation, "reqRepresentation");

		CfaNetworkDeclaration cfa = (CfaNetworkDeclaration) data.getModel();
		FieldsInCategories fields;
		try {
			fields = PlcCycleRepresentation.categorizeFields(cfa, parserResult, settings, result.currentStage());
			representPlcCycle(data, result, fields);
		} catch (AtomParsingException e) {
			result.currentStage().logError(e.getMessage(), e);
			result.currentStage().setStatus(StageStatus.Unsuccessful);
		}
		try {
			createTemporalRequirement(data, result, settings.getTlRequirement(), reqRepresentation, parserResult);
		} catch (ExpressionParsingException | FretRequirementException exception) {
			result.currentStage().setStatus(StageStatus.Unsuccessful);
			throw new PlcverifPlatformException(
					String.format("Problem occurred during the parsing of the FRET requirement: %s",
							exception.getMessage()),
					exception);				
		}
	}

	@Override
	public void diagnoseResult(VerificationProblem verifProblem, VerificationResult result) {
		result.setResultDiagnosis(new BasicFormattedString("No diagnosis is available.", ItalicFormat.INSTANCE));
	}
	
	private void createTemporalRequirement(VerificationProblem verifProblem, VerificationResult result,
			String tlExprStr, RequirementRepresentationStrategy reqRepresentation, IParserLazyResult parserResult) throws ExpressionParsingException, FretRequirementException {
		ExpressionParser exprParser = new ExpressionParser();
		// Parse TL expression
		// there are differences inbetween frets expr and ours (e.g. & or &&) - so we transform it here
		// this is essentially a naive solution, but it might be enough
		String tlExprPlcVerifStr = tlExprStr; // transformation already done in gui component
		Expression tlExpr = exprParser.parseExpression(tlExprPlcVerifStr);

		// Resolve all value placeholders (`ValuePlaceholder`)
		resolveValuePlaceholders(tlExpr, parserResult);
		
		// Check what engine we are using
		boolean isLtlEngine = reqRepresentation == RequirementRepresentationStrategy.EXPLICIT_WITH_VARIABLE;

		// Infer unknown types
		ExpressionParser.inferUnknownTypes(tlExpr);
		List<VariableAssignmentSkeleton> postAssignments = new ArrayList<>();
		List<VariableAssignmentSkeleton> preAssignments = new ArrayList<>();
		List<VariableAssignmentSkeleton> initAssignments = new ArrayList<>();
		if (TlExprUtils.isptLtl(tlExpr) && !isLtlEngine) {
			boolean useSeconds = settings.getTimeFormat().equals("MS");
			// Get(1) because we want the variable after update not past.
			Expression ptExpression = null;
			MonitorFactory.preprocess(tlExpr, null, 0);
			ptExpression = MonitorFactory.createMonitorRecursively(initAssignments, preAssignments, postAssignments, tlExpr,(CfaNetworkDeclaration)verifProblem.getModel(), useSeconds, verifProblem).get(1);
			// Add "isFirstCycle = False" assignment
			MonitorFactory.addIsFirstCycleAssignment(postAssignments, (CfaNetworkDeclaration)verifProblem.getModel());
			
			result.currentStage().logWarning("Converted ptLTL to monitored variable: "+ExprToString.toDiagString(ptExpression).replace(" ", "_"));
			tlExpr = ptExpression;
		}
		// Modify the TL expression according to the needed representation
		switch (reqRepresentation) {
		case EXPLICIT_WITH_LOCATIONREF: {
			// nothing to do for the moment
			break;
		}
		case IMPLICIT: {
			result.currentStage().logInfo(
					"IMPLICIT requirement representation strategy is not supported yet. Using EXPLICIT_WITH_VARIABLE instead.");
			// fall through on purpose
		}
		case EXPLICIT_WITH_VARIABLE: {
			if (!preAssignments.isEmpty()) {
				PlcCycleRepresentation.insertMonitorAtBoC(verifProblem, preAssignments);
			}
			if (!postAssignments.isEmpty()) {
				PlcCycleRepresentation.insertMonitorAtEoC(verifProblem, postAssignments);
			}
			if (!initAssignments.isEmpty()) {
				//PlcCycleRepresentation.insertMonitorBeforeLoop(verifProblem, initAssignments);
			}
			// End of cycle
			Field eocField = createEocField(verifProblem, result);
			FieldRef eocFieldRef = FACTORY.createFieldRef(eocField);
			List<Expression> toReplaceExpression = Lists.newArrayList(tlExpr);
			// If we created monitors we have to replace EoC/BoC for each assignment expression
			if (!isLtlEngine) {
				postAssignments.forEach(e -> toReplaceExpression.add(e.getRightValue()));
				preAssignments.forEach(e -> toReplaceExpression.add(e.getRightValue()));
			}
			
			for (Expression expr : toReplaceExpression) {
				for (EndOfCycle eocPredicate : EmfHelper.getAllContentsOfType(expr, EndOfCycle.class, true)) {
					if (isLtlEngine) {
						EcoreUtil.replace(eocPredicate, EcoreUtil.copy(eocFieldRef));
					} else {
						EcoreUtil.replace(eocPredicate, FACTORY.trueLiteral());
					}
				}
				
				// Beginning of cycle
				List<BeginningOfCycle> bocPredicates = EmfHelper.getAllContentsOfType(expr, BeginningOfCycle.class, true);
				if (!bocPredicates.isEmpty()) {
					Field bocField = createBocField(verifProblem, result);
					FieldRef bocFieldRef = FACTORY.createFieldRef(bocField);
					for (BeginningOfCycle bocPredicate : bocPredicates) {
						if (isLtlEngine) {
							EcoreUtil.replace(bocPredicate, EcoreUtil.copy(bocFieldRef));
						} else {
							EcoreUtil.replace(bocPredicate, FACTORY.trueLiteral());
						}
					}
				}
			}
			
			break;
		}
		default:
			throw new UnsupportedOperationException(
					"Unknown RequirementRepresentationStrategy: " + reqRepresentation);
		}

		verifProblem.setRequirement(tlExpr, reqRepresentation);
		verifProblem
				.setRequirementDescription(new BasicFormattedString(settings.getFretishRequirement()));
	
	}
	
	/**
	 * Basically the same as the method with the same name in PatternUtils
	 * @param tlExpr
	 * @param parserResult
	 * @throws FretRequirementException 
	 * @throws PatternRequirementException
	 */
	public static void resolveValuePlaceholders(Expression tlExpr, IParserLazyResult parserResult) throws FretRequirementException {
		for (ValuePlaceholder placeholder : EmfHelper.getAllContentsOfType(tlExpr, ValuePlaceholder.class, true)) {
			try {
				AtomicExpression parsedSubExpr = parserResult.parseAtom(placeholder.getValue());
				if (parsedSubExpr == null) {
					throw new FretRequirementException(
							"The parser frontent was unable to parse the following atomic value: "
									+ placeholder.getValue());
				}
				EcoreUtil.replace(placeholder, parsedSubExpr);
			} catch (AtomParsingException exception) {
				throw new FretRequirementException(exception.getMessage(), exception);
			}
		}
	}
}
