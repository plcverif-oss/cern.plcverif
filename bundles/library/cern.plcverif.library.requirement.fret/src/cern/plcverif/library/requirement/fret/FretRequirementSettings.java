/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Zsofia Adam - initial API and implementation
 *   Xaver Fink - monitor generation and ptLTL
 *******************************************************************************/
package cern.plcverif.library.requirement.fret;

import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsElement;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory;
import cern.plcverif.verif.requirement.AbstractRequirementSettings;

public class FretRequirementSettings extends AbstractRequirementSettings {
	public static final String TL_REQUIREMENT = "tl_requirement";
	@PlcverifSettingsElement(name = TL_REQUIREMENT, description = "The requirement in a TL (NuSMV format).", mandatory = PlcverifSettingsMandatory.MANDATORY)
	private String tlRequirement = null;

	public static final String TL_REQUIREMENT_FORMAT = "tl_requirement_format";
	@PlcverifSettingsElement(name = TL_REQUIREMENT_FORMAT, description = "The format of the requirement in a TL (ptLTL, LTL, or finite LTL).", mandatory = PlcverifSettingsMandatory.MANDATORY)
	private String tlRequirementFormat = null;
	
	public static final String TL_TIME_FORMAT = "tl_timing_usage";
	@PlcverifSettingsElement(name = TL_TIME_FORMAT, description = "The format of how time in bounded LTL is supposed to be represented.", mandatory = PlcverifSettingsMandatory.MANDATORY)
	private String timeFormat = null;
	
	public static final String FRETISH_REQUIREMENT = "fretish_requirement";
	@PlcverifSettingsElement(name = FRETISH_REQUIREMENT, description = "The requirement in fretish.")
	private String fretishRequirement = null;

	public static final String FRET_PATH = "fret_path";
	@PlcverifSettingsElement(name = FRET_PATH, description = "path to the fret-electron directory.")
	private String fretPath = "";

	public static final String CYGWIN_PATH = "cygwin_path";
	@PlcverifSettingsElement(name = CYGWIN_PATH, description = "bin directory of cygwin.")
	private String cygwinPath = "";
	
	public FretRequirementSettings() {
		if (System.getProperty("os.name").toLowerCase().contains("win")) {
			this.cygwinPath = "C:/cygwin64/bin";
		} 
	}

	public String getTlRequirement() {
		return tlRequirement;
	}
	
	public String getTimeFormat() {
		return timeFormat;
	}
	
	public void setTlRequirement(String tlRequirement) {
		this.tlRequirement = tlRequirement;
	}

	public String getFretishRequirement() {
		return fretishRequirement;
	}

	public void setFretishRequirement(String fretishRequirement) {
		this.fretishRequirement = fretishRequirement;
	}
	
	public String getFretPath() {
		return fretPath;
	}

	public void setFretPath(String fretPath) {
		this.fretPath = fretPath;
	}

	public String getCygwinPath() {
		return cygwinPath;
	}

	public void setCygwinPath(String cygwinPath) {
		this.cygwinPath = cygwinPath;
	}
}
