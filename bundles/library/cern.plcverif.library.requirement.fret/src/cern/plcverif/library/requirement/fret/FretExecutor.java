package cern.plcverif.library.requirement.fret;

import java.io.File;
import java.io.IOException;

import cern.plcverif.base.common.utils.OsUtils;
import cern.plcverif.library.requirement.fret.exceptions.FretRequirementRuntimeException;

public class FretExecutor {
	
	private String fretGlossaryPath;
	private String fretRequirementPath;
	private String fretElectronDir;
	private String cygwinPath;


	public FretExecutor(String fretGlossaryPath, String fretRequirementPath, String fretElectronDir, String cygwinPath) {
		this.fretGlossaryPath = fretGlossaryPath;
		this.fretRequirementPath = fretRequirementPath;
		this.fretElectronDir = fretElectronDir;
		this.cygwinPath = cygwinPath;
	}

	public static void execute(String fretGlossaryPath, String fretRequirementPath, String fretElectronDir, String cygwinPath) {
		FretExecutor executor = new FretExecutor(fretGlossaryPath, fretRequirementPath, fretElectronDir, cygwinPath);
		executor.execute();
	}

	public void execute() {
		ProcessBuilder pb;

		// in the case of windows, FRET has to be started from cygwin in order for the simulator to work
		if(OsUtils.getCurrentOs() == OsUtils.OperatingSystem.Win32) {
		    try {
		    	String fretElectronDirCygwin = fretElectronDir.replace('\\', '/'); // not the nicest, but works
		    	String externalImpJson = "EXTERNAL_IMP_JSON="+fretGlossaryPath.replace('\\', '/').replaceAll("\\.json$", "");
		    	String externalExpJson = "EXTERNAL_EXP_JSON="+fretRequirementPath.replace('\\', '/').replaceAll("\\.json$", "");
		    	String command = "\"cd "+fretElectronDirCygwin+"; " + externalImpJson + " " + externalExpJson +" npm run ext\"";

		    	pb = new ProcessBuilder(cygwinPath+File.separator+"/bash", "-l", "-i", "-c", command); // TODO hardcoded
				pb.redirectError();
				Process p = pb.start();
				
				/* for debugging purposes, output of the FRET process
				BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			    String readline;
			    int i = 0;
			    while ((readline = reader.readLine()) != null) {
			        System.out.println(++i + " " + readline);
			    }*/
				String errorStreamResult = new String(p.getErrorStream().readAllBytes());
				p.waitFor();
				if (errorStreamResult != "") {
					throw new FretRequirementRuntimeException("FRET error");
				}
			} catch (IOException e) {
				throw new FretRequirementRuntimeException("Exception in FRET process while editing requirement: " + e.getMessage());
			} catch (InterruptedException e) {
				throw new FretRequirementRuntimeException("FRET was interrupted while editing requirement: " + e.getMessage());
			} catch (Exception e) {
				throw new FretRequirementRuntimeException("test");
			}
		} else if (OsUtils.getCurrentOs() != OsUtils.OperatingSystem.Linux) {
			pb = new ProcessBuilder("npm", "run", "ext");
			pb.directory(new File(fretElectronDir));			
			try {
				pb.environment().put("EXTERNAL_IMP_JSON", fretGlossaryPath.replaceAll("\\.json$", "")); // FRET adds the .json extension, so we remove it here
				pb.environment().put("EXTERNAL_EXP_JSON", fretRequirementPath.replaceAll("\\.json$", "")); // FRET adds the .json extension, so we remove it here
				
				Process p = pb.start();

				/* for debugging purposes, output of the FRET process
				BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			    String readline;
			    int i = 0;
			    while ((readline = reader.readLine()) != null) {
			        System.out.println(++i + " " + readline);
			    }
			    */
				p.waitFor();
			} catch (InterruptedException e) {
				throw new FretRequirementRuntimeException("FRET was interrupted while editing requirement: " + e.getMessage());
			} catch (IOException e) {
				throw new FretRequirementRuntimeException("Exception in FRET process while editing requirement: " + e.getMessage());
			}
		} else {
			throw new FretRequirementRuntimeException("Only windows and linux are supported.");
		}
	}

}
