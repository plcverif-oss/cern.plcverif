/******************************************************************************
 * (C) Copyright CERN 2023-2024. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Xaver Fink - initial implementation
 *****************************************************************************/
package cern.plcverif.library.requirement.fret.ltlmonitors;

import cern.plcverif.base.models.expr.BinaryLogicExpression;
import cern.plcverif.base.models.expr.BinaryLtlExpression;
import cern.plcverif.base.models.expr.BoolLiteral;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.UnaryLogicExpression;
import cern.plcverif.base.models.expr.UnaryLtlExpression;

public class LtlReducer {
	
	public static boolean isTautology(Expression expr) {
		return false;
	}
	
	private static boolean isTautology(BoolLiteral expr) {
		return expr.isValue();
	}
	
	private static boolean isTautology(BinaryLogicExpression expr) {
		switch (expr.getOperator()) {
		case AND:
			return isTautology(expr.getLeftOperand()) && isTautology(expr.getRightOperand());
		case OR:
			return isTautology(expr.getLeftOperand()) || isTautology(expr.getRightOperand());
		case XOR:
			return (isTautology(expr.getLeftOperand()) && isContradiction(expr.getRightOperand())) || (isTautology(expr.getRightOperand()) && isContradiction(expr.getLeftOperand()));
		case IMPLIES:
			return isContradiction(expr.getLeftOperand()) || isTautology(expr.getRightOperand());
		default:
			return false;
		}
	}
	
	private static boolean isTautology(UnaryLogicExpression expr) {
		switch (expr.getOperator()) {
		case NEG:
			return isContradiction(expr.getOperand()) ;
		default:
			return false;
		}
	}
	
	private static boolean isTautology(UnaryLtlExpression expr) {
		return isTautology(expr.getOperand());
	}
	
	public static boolean isContradiction(Expression expr) {
		return false;
	}
	
	
	public static boolean isContradiction(BoolLiteral expr) {
		return !expr.isValue();
	}
	
	public static boolean isContradiction(BinaryLogicExpression expr) {
		switch (expr.getOperator()) {
		case AND:
			return isContradiction(expr.getLeftOperand()) || isContradiction(expr.getRightOperand());
		case OR:
			return isContradiction(expr.getLeftOperand()) && isContradiction(expr.getRightOperand());
		case XOR:
			return (isTautology(expr.getLeftOperand()) && isTautology(expr.getRightOperand())) || (isContradiction(expr.getRightOperand()) || isContradiction(expr.getLeftOperand()));
		case IMPLIES:
			return isContradiction(expr.getLeftOperand()) || isTautology(expr.getRightOperand());
		default:
			return false;
		}
	}
	
	private static boolean isContradiction(UnaryLogicExpression expr) {
		switch (expr.getOperator()) {
		case NEG:
			return isTautology(expr.getOperand()) ;
		default:
			return false;
		}
	}
	
	private static boolean isContradiction(UnaryLtlExpression expr) {
		return isContradiction(expr.getOperand());
	}

}
