/******************************************************************************
 * (C) Copyright CERN 2023-2024. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Xaver Fink - initial implementation
 *****************************************************************************/

package cern.plcverif.library.requirement.fret.ltlmonitors;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.emf.ecore.util.EcoreUtil;

import cern.plcverif.base.models.cfa.builder.VariableAssignmentSkeleton;
import cern.plcverif.base.models.cfa.cfabase.IfLocationAnnotation;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.Field;
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.IntType;
import cern.plcverif.base.models.expr.UnaryLtlExpression;
import cern.plcverif.base.models.expr.utils.ExprUtils;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.requirement.PlcCycleRepresentation;

public class HistoricallyMonitor extends MonitorOperation {

	protected HistoricallyMonitor(CfaNetworkDeclaration cfaNetwork, boolean useExplicitTimeForBounds, VerificationProblem problem) {
		super(cfaNetwork, useExplicitTimeForBounds, problem);
	}

	@Override
	public List<Field> createInitialization(Field fieldPre, Field fieldPost, Expression expressionTree, int index) {
		List<Field> monitorFields = new ArrayList<>();
		monitorFields.add(fieldPre);
		monitorFields.add(fieldPost);
		
		if (((UnaryLtlExpression)expressionTree).isBounded()) {
			createBoundedInitialization((UnaryLtlExpression)expressionTree, index, monitorFields);
		}
		factory.createInitialAssignment(fieldPre, factory.createBoolLiteral(true));
		factory.createInitialAssignment(fieldPost, factory.createBoolLiteral(true));
		
		return monitorFields;
	}

	@Override
	public void createUpdateExpression(List<Field> monitorVariables, List<VariableAssignmentSkeleton> initAssignments, List<VariableAssignmentSkeleton> preAssignments, List<VariableAssignmentSkeleton> postAssignments, Expression expressionTree) {
		Expression assignment;
		Expression initAssignment;
		
		FieldRef isFirstCycle = factory.createFieldRef(PlcCycleRepresentation.findGlobalField(cfaNetwork, "isFirstCycle").orElseGet(null));
		
		List<Expression> child = MonitorFactory.createMonitorRecursively(initAssignments, preAssignments, postAssignments, ((UnaryLtlExpression)expressionTree).getOperand(), super.cfaNetwork, super.useExplicitTimeForBounds, problem);
		assignment = factory.and(factory.createFieldRef(monitorVariables.get(0)), child.get(1));
		// Historically(t_0) = true && phi
		initAssignment = ExprUtils.exprCopy(child.get(1));
		assignment = factory.ternary(isFirstCycle, ExprUtils.exprCopy(initAssignment), assignment);
		
		
		
		if (((UnaryLtlExpression)expressionTree).isBounded()) {
			createBoundedUpdate(postAssignments, monitorVariables, (UnaryLtlExpression)expressionTree, child.get(1));
		}else {
			postAssignments.add(VariableAssignmentSkeleton.create(factory.createFieldRef(monitorVariables.get(1)), assignment));
		}
		
		preAssignments.add(VariableAssignmentSkeleton.create(factory.createFieldRef(monitorVariables.get(0)), factory.createFieldRef(monitorVariables.get(1))));
		initAssignments.add(VariableAssignmentSkeleton.create(factory.createFieldRef(monitorVariables.get(1)), initAssignment));
	}
	
	
	@Override
	public String getVariablePrefix() {
		return "historically";
	}

	@Override
	public void createBoundedInitialization(UnaryLtlExpression expressionTree, int namingIndex, List<Field> monitorFields) {
		if (expressionTree.getLowerBound() != 0) {
			throw new UnsupportedOperationException("Currently only supporting lower bound of zero.");
		}
		if (expressionTree.getUpperBound() >= 256 && !useExplicitTimeForBounds) {
			throw new UnsupportedOperationException("Currently only supporting time until 256 limit.");
		}
		
		IntType type = createCounterType();
		Field timerField = factory.createField(getVariablePrefix()+"_timer_"+namingIndex, cfaNetwork.getRootDataStructure(), type);
		
		factory.createInitialAssignment(timerField, factory.createIntLiteral(expressionTree.getUpperBound(), EcoreUtil.copy(type)));
		
		monitorFields.add(timerField);
		
	}
	
	@Override
	public void createBoundedUpdate(List<VariableAssignmentSkeleton> postAssignments, List<Field> monitorVariables, UnaryLtlExpression expressionTree, Expression phi) {
		if (expressionTree.getLowerBound() != 0) {
			throw new UnsupportedOperationException("Currently only supporting lower bound of zero.");
		}
		
		Field timerField = monitorVariables.get(2);
		
		final String T_CYCLE_NAME = "T_CYCLE";
		Field tCycle = null;
		if (useExplicitTimeForBounds) {
			tCycle = PlcCycleRepresentation.findGlobalField(cfaNetwork, T_CYCLE_NAME).orElseGet(null);
			Expression assumption = factory.and(factory.geq(factory.createFieldRef(timerField), factory.createIntLiteral(-100, createCounterType())), factory.leq(factory.createFieldRef(timerField), factory.createIntLiteral((expressionTree).getUpperBound(), createCounterType())));
			PlcCycleRepresentation.addAssumptionAtBoC(problem, assumption);
		}
		
		Expression decrementExpression = factory.minus(factory.createFieldRef(monitorVariables.get(2)), useExplicitTimeForBounds ? factory.createTypeConversionIfNeeded(factory.createFieldRef(tCycle), createCounterType()) : factory.createIntLiteral(1, createCounterType()));
		Expression notLowerZero = factory.neg(factory.leq(factory.createFieldRef(monitorVariables.get(2)), factory.createIntLiteral(0, createCounterType())));
		Expression decrementIfNotSmallerZero = factory.ternary(notLowerZero, decrementExpression, factory.createIntLiteral(0, createCounterType()));
		Expression timerUpdate = factory.ternary(factory.neg(ExprUtils.exprCopy(phi)), factory.createIntLiteral((expressionTree).getUpperBound(), createCounterType()), decrementIfNotSmallerZero);
		
//		Expression notExpressionResetTimer = factory.multiply(factory.createTypeConversionIfNeeded(factory.neg(ExprUtils.exprCopy(phi)), createCounterType()), factory.createIntLiteral((expressionTree).getUpperBound(), createCounterType()));
//		Expression expressionCountDownTimer = factory.multiply(factory.createTypeConversionIfNeeded(ExprUtils.exprCopy(phi), createCounterType()), factory.minus(factory.createFieldRef(monitorVariables.get(2)), useExplicitTimeForBounds ? factory.createTypeConversionIfNeeded(factory.createFieldRef(tCycle), createCounterType()) : factory.createIntLiteral(1, createCounterType())));
//		Expression expressionCountDownTimerMinZeroExpression = factory.multiply(factory.createTypeConversionIfNeeded(factory.neg(factory.leq(factory.createFieldRef(monitorVariables.get(2)), factory.createIntLiteral(0, createCounterType()))), createCounterType()), expressionCountDownTimer);
//		Expression combineExpression = factory.plus(notExpressionResetTimer, expressionCountDownTimerMinZeroExpression);
		postAssignments.add(VariableAssignmentSkeleton.create(factory.createFieldRef(monitorVariables.get(2)), timerUpdate));
		postAssignments.add(VariableAssignmentSkeleton.create(factory.createFieldRef(monitorVariables.get(1)), factory.neg(factory.gt(factory.createFieldRef(monitorVariables.get(2)), factory.createIntLiteral(0, createCounterType())))));
		
	}

}
