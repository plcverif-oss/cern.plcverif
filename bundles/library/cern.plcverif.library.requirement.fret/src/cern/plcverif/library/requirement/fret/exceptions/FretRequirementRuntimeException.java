/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Zsofia Adam - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.requirement.fret.exceptions;

public class FretRequirementRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 1123113175199488909L;

	public FretRequirementRuntimeException(String message) {
		super(message);
	}
	
	public FretRequirementRuntimeException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
