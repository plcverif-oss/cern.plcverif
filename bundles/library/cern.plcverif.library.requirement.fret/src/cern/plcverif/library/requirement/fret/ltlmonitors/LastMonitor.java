/******************************************************************************
 * (C) Copyright CERN 2023-2024. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Xaver Fink - initial implementation
 *****************************************************************************/
package cern.plcverif.library.requirement.fret.ltlmonitors;

import java.util.ArrayList;
import java.util.List;

import cern.plcverif.base.models.cfa.builder.VariableAssignmentSkeleton;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.Field;
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.UnaryLtlExpression;
import cern.plcverif.base.models.expr.utils.ExprUtils;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.requirement.PlcCycleRepresentation;

public class LastMonitor extends MonitorOperation {

	protected LastMonitor(CfaNetworkDeclaration cfaNetwork, boolean useExplicitTimeForBounds, VerificationProblem problem) {
		super(cfaNetwork, useExplicitTimeForBounds, problem);
	}

	@Override
	public List<Field> createInitialization(Field fieldPre, Field fieldPost, Expression expressionTree, int index) {
		List<Field> monitorFields = new ArrayList<>();
		monitorFields.add(fieldPre);
		monitorFields.add(fieldPost);
		
		if (((UnaryLtlExpression)expressionTree).isBounded()) {
			createBoundedInitialization((UnaryLtlExpression)expressionTree, index, monitorFields);
		}
		
		factory.createInitialAssignment(fieldPre, factory.createBoolLiteral(false));
		factory.createInitialAssignment(fieldPost, factory.createBoolLiteral(false));
		
		return monitorFields;
	}

	@Override
	public void createUpdateExpression(List<Field> monitorVariables, List<VariableAssignmentSkeleton> initAssignments, List<VariableAssignmentSkeleton> preAssignments, List<VariableAssignmentSkeleton> postAssignments, Expression expressionTree) {
		Expression assignment;
		
		FieldRef isFirstCycle = factory.createFieldRef(PlcCycleRepresentation.findGlobalField(cfaNetwork, "isFirstCycle").orElseGet(null));
		
		List<Expression> child = MonitorFactory.createMonitorRecursively(initAssignments, preAssignments, postAssignments, ((UnaryLtlExpression)expressionTree).getOperand(), super.cfaNetwork, super.useExplicitTimeForBounds, problem);
		assignment = child.get(0);
		assignment = factory.ternary(isFirstCycle, factory.createBoolLiteral(false), assignment);
		
		if (((UnaryLtlExpression)expressionTree).isBounded()) {
			createBoundedUpdate(postAssignments, monitorVariables, (UnaryLtlExpression)expressionTree, child.get(1));
		}else {
			postAssignments.add(VariableAssignmentSkeleton.create(factory.createFieldRef(monitorVariables.get(1)), assignment));
		}
		
		preAssignments.add(VariableAssignmentSkeleton.create(factory.createFieldRef(monitorVariables.get(0)), factory.createFieldRef(monitorVariables.get(1))));
		initAssignments.add(VariableAssignmentSkeleton.create(factory.createFieldRef(monitorVariables.get(1)), factory.createBoolLiteral(false)));
	}
	
	@Override
	public String getVariablePrefix() {
		return "last";
	}

}
