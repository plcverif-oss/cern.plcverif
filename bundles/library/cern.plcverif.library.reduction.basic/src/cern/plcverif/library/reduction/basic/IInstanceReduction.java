/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic;

import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;

public interface IInstanceReduction {
	/**
	 * Executes the reduction on the given control flow automata instance.
	 * @param cfa CFA instance to reduce.
	 * @param result The job results to be written during reductions 
	 *               (e.g., for logging).
	 * @return True, if there was any modification done in {@code cfa}.
	 */
	boolean reduce(CfaNetworkInstance cfa, JobResult result);

	/**
	 * Sets whether fine-grained logging is enabled, i.e., whether each reduction step shall be logged.
	 * If false, even the log messages should not be generated.
	 * @param enabled True, if fine-grained logging is enabled.
	 */
	void setFineLogging(boolean enabled);
}
