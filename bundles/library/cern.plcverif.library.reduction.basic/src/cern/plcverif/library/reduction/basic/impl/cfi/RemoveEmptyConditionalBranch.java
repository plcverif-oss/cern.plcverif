/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic.impl.cfi;

import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.CollectionDeleter;
import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfabase.Transition;
import cern.plcverif.base.models.cfa.cfainstance.AssignmentTransition;
import cern.plcverif.base.models.cfa.cfainstance.AutomatonInstance;
import cern.plcverif.base.models.cfa.utils.CfaCollectionDeleter;
import cern.plcverif.base.models.cfa.utils.CfaUtils;
import cern.plcverif.base.models.expr.ExpressionSafeFactory;

/**
 * Reduction to remove conditional branches which do not have any assignments
 * (i.e., parallel transitions which are unnecessary).
 */
public class RemoveEmptyConditionalBranch extends AbstractAutomatonReduction {

	@Override
	protected boolean reduce(AutomatonInstance automaton, JobResult result) {
		boolean changed = false;

		CfaCollectionDeleter deleter = new CfaCollectionDeleter();
		for (Location loc : automaton.getLocations()) {
			if (loc.getOutgoing().size() == 2) {
				Transition t1 = loc.getOutgoing().get(0);
				Transition t2 = loc.getOutgoing().get(1);

				if (noAssignments(t1) && noAssignments(t2) && t1.getTarget() == t2.getTarget()) {
					if (CfaUtils.isNegatedOf(t1.getCondition(), t2.getCondition())) {
						if (!t1.isFrozen()) {
							changed = true;
							mergeInto(t1, t2, deleter);
							// t2 will me modified, t1 will be removed
						} else if (!t2.isFrozen()) {
							changed = true;
							mergeInto(t2, t1, deleter);
							// t1 will me modified, t2 will be removed
						}
					}
				}
			}
		}
		deleter.deleteAll();

		return changed;
	}

	/**
	 * Merges transition 't1' into transition 't2'. Effectively it removes the
	 * guard of 't2' and deletes 't1'. It is assumed that 't1' and 't2' do not
	 * contain any assignments and their guards are complements of each other.
	 */
	private static void mergeInto(Transition t1, Transition t2, CollectionDeleter deleter) {
		Preconditions.checkArgument(t1.getSource() == t2.getSource());
		Preconditions.checkArgument(t1.getTarget() == t2.getTarget());

		EcoreUtil.replace(t2.getCondition(), ExpressionSafeFactory.INSTANCE.trueLiteral());
		deleter.registerForDeletion(t1);
	}

	private static boolean noAssignments(Transition t) {
		if (t instanceof AssignmentTransition) {
			return ((AssignmentTransition) t).getAssignments().isEmpty();
		}
		return false;
	}

}
