/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic.impl.cfi.coi;

import java.math.BigInteger;

/**
 * Class to represent big rational numbers (fractions).
 * 
 * @author Daniel DARVAS (CERN BE-ICS-AP)
 * 
 */
public class BigFraction {
	private BigInteger numerator;
	private BigInteger denominator;

	/**
	 * Creates fraction "number/1".
	 */
	public BigFraction(long number) {
		this(BigInteger.valueOf(number));
	}

	/**
	 * Creates fraction "number/1".
	 */
	public BigFraction(BigInteger number) {
		this(number, BigInteger.ONE);
		this.denominator = BigInteger.ONE;
	}

	/**
	 * Creates fraction "numerator/denominator".
	 */

	public BigFraction(BigInteger numerator, BigInteger denominator) {
		this.numerator = numerator;
		this.denominator = denominator;
		simplify();
	}

	public BigInteger getDenominator() {
		return denominator;
	}

	public BigInteger getNumerator() {
		return numerator;
	}

	/**
	 * Returns the value as a double.
	 * This may be an approximation of the real value.
	 * 
	 * @return Value as a double.
	 */
	public double asDouble() {
		return numerator.doubleValue() / denominator.doubleValue();
	}

	/**
	 * Returns frac1+frac2.
	 */
	public static BigFraction add(BigFraction frac1, BigFraction frac2) {
		BigFraction ret = new BigFraction(frac1.numerator.multiply(frac2.denominator).add(frac2.numerator.multiply(frac1.denominator)),
				frac1.denominator.multiply(frac2.denominator));
		ret.simplify();
		return ret;
	}

	/**
	 * Returns frac/divider.
	 */
	public static BigFraction divide(BigFraction frac, int divisor) {
		BigFraction ret = new BigFraction(frac.numerator, frac.denominator.multiply(BigInteger.valueOf(divisor)));
		ret.simplify();
		return ret;
	}

	/**
	 * Determines if the represented fraction is an integer.
	 */
	public boolean isInteger() {
		if (this.denominator.equals(BigInteger.ZERO)) {
			return false; // NaN
		}

		if (this.numerator.equals(BigInteger.ZERO)) {
			return true; // 0
		}

		return (this.numerator.mod(this.denominator).equals(BigInteger.ZERO));
	}

	public boolean equalsTo(long number) {
		if (this.isInteger()) {
			return (numerator.divide(denominator).equals(BigInteger.valueOf(number)));
		}

		return false;
	}

	@Override
	public String toString() {
		return String.format("%s/%s", this.numerator, this.denominator);
	}

	private void simplify() {
		if (this.denominator.equals(BigInteger.ZERO)) {
			return;
		}

		if (this.numerator.equals(BigInteger.ZERO)) {
			this.denominator = BigInteger.ONE;
			return;
		}

		BigInteger gcd = this.numerator.gcd(this.denominator);

		this.numerator = this.numerator.divide(gcd);
		this.denominator = this.denominator.divide(gcd);
	}
}