/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic.impl.cfi;

import static cern.plcverif.base.models.expr.utils.ExprUtils.isFalseLiteral;

import java.util.List;
import java.util.stream.Collectors;

import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.models.cfa.cfainstance.AssignmentTransition;
import cern.plcverif.base.models.cfa.cfainstance.AutomatonInstance;
import cern.plcverif.base.models.cfa.utils.CfaInstanceUtils;

public class RemoveFalseTransitions extends AbstractAutomatonReduction {

	@Override
	protected boolean reduce(AutomatonInstance automaton, JobResult result) {
		boolean modificationDone = false;

		List<AssignmentTransition> falseTransitions = automaton.getTransitions().stream()
				.filter(it -> it instanceof AssignmentTransition).map(it -> (AssignmentTransition) it)
				.filter(it -> isFalseLiteral(it.getCondition()) && !it.isFrozen()).collect(Collectors.toList());

		for (AssignmentTransition t : falseTransitions) {
			CfaInstanceUtils.deleteTransitionNoAnnotation(t);
			modificationDone = true;
		}

		return modificationDone;
	}
}