/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.progress.ICanceling;
import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.exceptions.SettingsSerializerException;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.interfaces.IReduction;
import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.models.cfa.cfabase.Annotation;
import cern.plcverif.base.models.cfa.cfabase.CfaNetworkBase;
import cern.plcverif.base.models.cfa.cfabase.Freezable;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariableRef;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.cfa.transformation.ElseEliminator;
import cern.plcverif.base.models.cfa.utils.CfaUtils;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.library.reduction.basic.impl.cfa.RemoveLocationsWithNoIncomingTransition;
import cern.plcverif.library.reduction.basic.impl.cfd.OverlapAssignments;
import cern.plcverif.library.reduction.basic.impl.cfd.RemoveEmptyTransitions;
import cern.plcverif.library.reduction.basic.impl.cfi.ConditionPushDown;
import cern.plcverif.library.reduction.basic.impl.cfi.ExpressionSimplification;
import cern.plcverif.library.reduction.basic.impl.cfi.OverlapVarAssignments;
import cern.plcverif.library.reduction.basic.impl.cfi.RemoveEmptyConditionalBranch;
import cern.plcverif.library.reduction.basic.impl.cfi.RemoveFalseTransitions;
import cern.plcverif.library.reduction.basic.impl.cfi.RemoveIdentityAssignments;
import cern.plcverif.library.reduction.basic.impl.cfi.RemoveUnreadVariables;
import cern.plcverif.library.reduction.basic.impl.cfi.SimplifyTrivialConditionalBranches;
import cern.plcverif.library.reduction.basic.impl.cfi.coi.CoiReduction;
import cern.plcverif.library.reduction.basic.impl.cfi.propagation.ExpressionPropagation;
import cern.plcverif.library.reduction.basic.impl.cfi.unreadamt.RemoveUnreadAssignments;

public class BasicReductions implements IReduction {
	private BasicReductionSettings settings;

	private final List<IDeclarationReduction> declarationReductions;
	private final List<IInstanceReduction> instanceReductions;

	public BasicReductions() {
		this(new BasicReductionSettings());
	}

	public BasicReductions(Settings settings) throws SettingsParserException {
		this(SpecificSettingsSerializer.parse(settings.toSingle(), BasicReductionSettings.class));
	}

	public BasicReductions(BasicReductionSettings settings) {
		this.settings = settings;

		this.declarationReductions = Arrays.asList(new OverlapAssignments(),
				new RemoveLocationsWithNoIncomingTransition(), new RemoveEmptyTransitions());
		this.declarationReductions.forEach(it -> it.setFineLogging(settings.isFineLogging()));

		this.instanceReductions = Arrays.asList(new OverlapVarAssignments(), new SimplifyTrivialConditionalBranches(),
				new cern.plcverif.library.reduction.basic.impl.cfi.RemoveEmptyTransitions(),
				new RemoveFalseTransitions(), new ConditionPushDown(), new ExpressionPropagation(true, settings), 
				new ExpressionSimplification(), new RemoveUnreadVariables(), new CoiReduction(settings),
				new RemoveEmptyConditionalBranch(), new ExpressionPropagation(false, settings), new OverlapVarAssignments(),
				new RemoveLocationsWithNoIncomingTransition(), new RemoveIdentityAssignments(), new RemoveUnreadAssignments());
		this.instanceReductions.forEach(it -> it.setFineLogging(settings.isFineLogging()));
	}

	@Override
	public Settings retrieveSettings() {
		try {
			SettingsElement ret = SpecificSettingsSerializer.toGenericSettings(settings);
			ret.setValue(BasicReductionsExtension.CMD_ID);
			return ret;
		} catch (SettingsSerializerException | SettingsParserException e) {
			throw new PlcverifPlatformException("Unable to retrieve the settings for BasicReductions.", e);
		}
	}

	@Override
	public boolean reduce(CfaNetworkBase cfa, JobResult result, ICanceling canceler) {
		Preconditions.checkArgument(cfa != null, "The CFA given to the basic reductions is null.");
		Preconditions.checkArgument(result != null, "The job result given to the basic reductions is null.");

		ICanceling cancelerNotNull = canceler == null ? ICanceling.NEVER_CANCELING_INSTANCE : canceler;

		if (cfa instanceof CfaNetworkInstance) {
			return instanceReductions((CfaNetworkInstance) cfa, result, cancelerNotNull);
		} else {
			return declarationReductions((CfaNetworkDeclaration) cfa, result, cancelerNotNull);
		}
	}

	@Override
	public boolean reduce(CfaNetworkBase cfa, Expression requirement, JobResult result, ICanceling canceler) {
		Preconditions.checkArgument(cfa != null, "The CFA given to the basic reductions is null.");
		Preconditions.checkArgument(requirement != null, "The requirement given to the basic reductions is null.");
		Preconditions.checkArgument(result != null, "The job result given to the basic reductions is null.");
		// The basic reductions are requirement-independent (except for frozen
		// variables --> COI)

		// Make sure that the variables present in the requirement are frozen
		// We don't freeze CFD fields as they are not going to be removed.
		freezeReqVarsInstance(requirement, result);

		return reduce(cfa, result, canceler);
	}

	private static void freezeReqVarsInstance(Expression requirement, JobResult result) {
		for (AbstractVariableRef varRef : EmfHelper.getAllContentsOfType(requirement, AbstractVariableRef.class,
				true)) {
			if (!varRef.getVariable().isFrozen()) {
				// If the variable is NOT frozen
				result.currentStage().logDebug(
						"Variable '%s' is present in the requirement, but it was not frozen. It has been made frozen now.",
						varRef.getVariable().getName());
				varRef.getVariable().setFrozen(true);
			}
		}
	}

	private boolean declarationReductions(CfaNetworkDeclaration cfa, JobResult result, ICanceling canceler) {
		Preconditions.checkNotNull(canceler);

		// We cannot maintain the annotations at this point. In the following,
		// it is assumed that there are no annotations in the CFI, except
		// eventual local annotations (referring only to primitives, but no
		// model elements).
		CfaUtils.removeAllNonlocalAnnotations(cfa);

		// Else expressions may cause issues with some of the reductions
		ElseEliminator.transform(cfa);
		
		// We don't freeze CFD fields as they are not going to be removed.
		// This needs to be changed if in the future fields can be removed by CFD reductions. 

		boolean isModified = true;
		boolean wasAnyModification = false;
		
		int iteration = 0;
		while (isModified && !canceler.isCanceled()) {
			isModified = false;
			iteration++;

			result.currentStage().logInfo("Iteration %s started.", iteration);
			for (IDeclarationReduction item : declarationReductions) {
				result.currentStage().logDebug("Performing reduction: " + item.getClass().getSimpleName());
				isModified = item.reduce(cfa, result) || isModified;
				wasAnyModification = wasAnyModification || isModified;
			}
		}
		if (isModified && canceler.isCanceled()) {
			result.currentStage().logInfo("Declaration reductions have been cancelled.");
		}

		return wasAnyModification;
	}

	@SuppressWarnings("unused")
	private static List<Freezable> freezeAnnotationRefs(CfaNetworkDeclaration cfa) {
		List<Freezable> objectsMadeFrozen = new ArrayList<>();

		for (Annotation annotation : EmfHelper.getAllContentsOfType(cfa, Annotation.class, false)) {
			for (EObject ref : annotation.eCrossReferences()) {
				if (ref instanceof Freezable && ((Freezable) ref).isFrozen() == false) {
					((Freezable) ref).setFrozen(true);
					objectsMadeFrozen.add(((Freezable) ref));
				}
			}
		}

		return objectsMadeFrozen;
	}

	private boolean instanceReductions(CfaNetworkInstance cfa, JobResult result, ICanceling canceler) {
		Preconditions.checkNotNull(canceler);

		// We cannot maintain the annotations at this point. In the following,
		// it is assumed that there are no annotations in the CFI, except
		// eventual local annotations (referring only to primitives, but no
		// model elements).
		CfaUtils.removeAllNonlocalAnnotations(cfa);

		// Else expressions may cause issues with some of the reductions
		ElseEliminator.transform(cfa);

		boolean isModified = true;
		boolean wasAnyModification = false;
		
		int iteration = 0;
		while (isModified && !canceler.isCanceled()) {
			isModified = false;
			iteration++;

			result.currentStage().logInfo("Iteration %s started.", iteration);
			for (IInstanceReduction item : instanceReductions) {
				result.currentStage().logDebug("Performing reduction: " + item.getClass().getSimpleName());
				long startTime = System.nanoTime();
				boolean thisReductionMadeModification = item.reduce(cfa, result);
				isModified = thisReductionMadeModification || isModified;
				wasAnyModification = wasAnyModification || isModified;
				if (settings.isShowProgress()) {
					long elapsedMs = (System.nanoTime() - startTime) / 1_000_000L;
					System.out.printf("   Reduction %-41s iteration %3s took %6s ms.%s%n",
							item.getClass().getSimpleName() + ",", iteration, elapsedMs,
							thisReductionMadeModification ? "" : "  (no modification)");
				}
			}
		}

		if (isModified && canceler.isCanceled()) {
			result.currentStage().logInfo("Instance reductions have been cancelled.");
		}
		
		return wasAnyModification;
	}

	/**
	 * Returns all CFI reductions.
	 * 
	 * For testing.
	 */
	Collection<IInstanceReduction> allInstanceReductions() {
		return Collections.unmodifiableList(instanceReductions);
	}

	/**
	 * Returns all CFD reductions.
	 * 
	 * For testing.
	 */
	Collection<IDeclarationReduction> allDeclarationReductions() {
		return Collections.unmodifiableList(declarationReductions);
	}
}
