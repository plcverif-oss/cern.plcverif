/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic.impl.cfi.propagation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.common.logging.IPlcverifLogger;
import cern.plcverif.base.common.logging.PlcverifSeverity;
import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.models.cfa.CfaInstanceSafeFactory;
import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfabase.Transition;
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariableRef;
import cern.plcverif.base.models.cfa.cfainstance.AssignmentTransition;
import cern.plcverif.base.models.cfa.cfainstance.AutomatonInstance;
import cern.plcverif.base.models.cfa.cfainstance.CallTransition;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.cfa.cfainstance.VariableAssignment;
import cern.plcverif.base.models.cfa.textual.CfaToString;
import cern.plcverif.base.models.cfa.utils.CfaUtils;
import cern.plcverif.base.models.cfa.validation.CfaInstanceValidation;
import cern.plcverif.base.models.cfa.validation.CfaValidation;
import cern.plcverif.base.models.expr.BinaryLogicExpression;
import cern.plcverif.base.models.expr.ComparisonExpression;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.UnaryLogicExpression;
import cern.plcverif.base.models.expr.utils.ExprUtils;
import cern.plcverif.base.models.expr.utils.TypeUtils;
import cern.plcverif.library.reduction.basic.BasicReductionSettings;
import cern.plcverif.library.reduction.basic.IInstanceReduction;
import cern.plcverif.library.reduction.basic.impl.AbstractLoggingReduction;
import cern.plcverif.library.reduction.basic.impl.cfi.ExpressionSimplification;

public class ExpressionPropagation extends AbstractLoggingReduction implements IInstanceReduction {
	private Set<Location> visitedLocations = null;
	private CfaInstanceValidation validator;
	boolean isModified;
	private IPlcverifLogger log = null; // only for diagnostics, to be removed
	private BasicReductionSettings settings;
	private Queue<Task> tasks = null;
	private Map<Transition, PropagationTable> tableForTransition = null;

	private ExpressionPropagationSettings propagationSettings;
	private final ExpressionSimplification simplifier = new ExpressionSimplification();

	private static class Task {
		private Location loc;

		public Task(Location loc) {
			this.loc = loc;
		}

		public Location getLocation() {
			return loc;
		}
	}

	public ExpressionPropagation(boolean constantPropagationOnly, BasicReductionSettings settings) {
		Preconditions.checkNotNull(settings, "settings");
		this.settings = settings;
		this.propagationSettings = new ExpressionPropagationSettings(constantPropagationOnly, settings.getExprPropagationMaxAge(),
				settings.getExprPropagationMaxExprSize());
	}

	public final boolean reduce(CfaNetworkInstance cfa, JobResult result) {
		isModified = false;
		validator = new CfaInstanceValidation(cfa, new CfaValidation.CfaValidationExceptionLogger());
		log = result.currentStage();
		simplifier.setFineLogging(this.isFineLoggingEnabled());
		simplifier.setLogger(new IPlcverifLogger() {
			@Override
			public void log(PlcverifSeverity severity, String message, Throwable throwable) {
				log.log(severity, message + " (called from ExpressionPropagation)", throwable);
			}
			
			@Override
			public void log(PlcverifSeverity severity, String messageFormat, Object... args) {
				log.log(severity, messageFormat + " (called from ExpressionPropagation)", args);
			}
		});
		visitedLocations = new HashSet<>();

		for (AutomatonInstance automaton : cfa.getAutomata()) {
			reduce(automaton, result);
		}

		return isModified;
	}

	protected void reduce(AutomatonInstance automaton, JobResult result) {
		if (automaton.getLocations().size() > settings.getExprPropagationMaxLocations()) {
			return;
		}

		tableForTransition = new HashMap<>(automaton.getTransitions().size() * 2);
		visitedLocations.clear();
		tasks = new LinkedList<>();
		tasks.add(new Task(automaton.getInitialLocation()));
		
		while (!tasks.isEmpty()) {
			Task currentTask = tasks.peek(); // peeks the head (do not remove!)
			if (isBlocked(currentTask.getLocation())) {
				if (visitedLocations.contains(currentTask.getLocation())) {
					// this should not happen, but be defensive
					tasks.remove();
					continue;
				} else if (allTasksBlocked()) {
					// If all known tasks are blocked, we should compute it
					// anyways with empty propagation table.
					// Otherwise we could not enter a loop.
					tasks.remove();
					propagateToLocation(currentTask.getLocation(), PropagationTable.createEmpty(propagationSettings));
				} else {
					// There is another task that can be executed, let's find
					// that
					Task firstNonBlocked = getFirstNonblockedTask();
					Preconditions.checkState(firstNonBlocked != null, "Null found in the tasks queue for ExpressionPropagation.");
					tasks.remove(firstNonBlocked);
					propagateToLocation(firstNonBlocked.getLocation(),
							combinePropTablesFor(firstNonBlocked.getLocation()));
				}
			} else {
				// All incoming propagation tables are known, no problem, let's
				// calculate
				tasks.remove();
				propagateToLocation(currentTask.getLocation(), combinePropTablesFor(currentTask.getLocation()));
			}
		}
		
		// Diagnostics
		if (this.isFineLoggingEnabled() && visitedLocations.size() < automaton.getLocations().size()) {
			log.logDebug("ExpressionPropagation: in automaton '%s', only %s out of %s locations have been visited.",
					automaton.getName(), visitedLocations.size(), automaton.getLocations().size());
		}
		
		tableForTransition = null;
		tasks = null;
	}

	private void propagateToLocation(Location loc, PropagationTable propTable) {
		if (visitedLocations.contains(loc)) {
			// Already visited
			return;
		}
		visitedLocations.add(loc);

		// cleanup the incoming transitions' tables for efficiency
		// (we will not need those propagation tables anymore)
		loc.getIncoming().forEach(t -> tableForTransition.remove(t));

		// distribute
		for (Transition t :loc.getOutgoing()) {
			propagate(t, propTable.createCopy());
		}
	}

	private void propagate(Transition t, PropagationTable propTable) {
		if (t instanceof AssignmentTransition) {
			propagate((AssignmentTransition) t, propTable);
		} else if (t instanceof CallTransition) {
			propagate((CallTransition) t, propTable);
		} else {
			throw new UnsupportedOperationException("Unable to propagate through " + t);
		}
	}

	/**
	 * ...
	 * 
	 * Will not create a copy of the given propagation table! The table given as parameter will be updated.
	 */
	private void propagate(AssignmentTransition t, PropagationTable propTable) {
		// Check assignment validity
		validator.check(t);

		// Modify guard using the propagation table
		internalModifyGuard(t, propTable);

		// Update the propagation table with the guard
		internalUpdatePropTableWithEqualities(t.getCondition(), propTable);

		// Modify right values using the propagation table
		internalModifyRightValues(t.getAssignments(), propTable);

		// Update propagation table (removes previous propagation values for the
		// assigned variables and adds the new values if they can be propagated)
		internalUpdatePropTableWithLeftValues(t.getAssignments(), propTable);

		tasks.add(new Task(t.getTarget()));
		tableForTransition.put(t, propTable);
	}

	private void propagate(CallTransition t, PropagationTable propTable) {
		// to be on the safe side, do not propagate through call transitions for
		// the moment
		tasks.add(new Task(t.getTarget()));
		tableForTransition.put(t, PropagationTable.createEmpty(propagationSettings));
	}

	private void internalModifyGuard(AssignmentTransition t, PropagationTable propTable) {
		modifyExpression(t.getCondition(), propTable, Collections.emptyList());
	}

	private void internalModifyRightValues(List<VariableAssignment> assignments, PropagationTable propTable) {
		for (VariableAssignment amt : assignments) {
			// forbiddenVars: left values in the assignments, except the one of the expression to be modified
			List<AbstractVariableRef> forbiddenVars = assignments.stream().map(it -> it.getLeftValue())
					.filter(it -> it != amt.getLeftValue()).collect(Collectors.toList());
			modifyExpression(amt.getRightValue(), propTable, forbiddenVars);
		}
	}

	private static void internalUpdatePropTableWithEqualities(Expression expr, PropagationTable propTable) {
		if (ExprUtils.isEquality(expr)) {
			ComparisonExpression eqExpr = (ComparisonExpression) expr;
			if (eqExpr.getLeftOperand() instanceof AbstractVariableRef) {
				propTable.updateWithEquality((AbstractVariableRef) eqExpr.getLeftOperand(), eqExpr.getRightOperand());
			} else if (eqExpr.getRightOperand() instanceof AbstractVariableRef) {
				propTable.updateWithEquality((AbstractVariableRef) eqExpr.getRightOperand(), eqExpr.getLeftOperand());
			}
		} else if (ExprUtils.isAndOperation(expr)) {
			BinaryLogicExpression andExpr = (BinaryLogicExpression) expr;
			internalUpdatePropTableWithEqualities(andExpr.getLeftOperand(), propTable);
			internalUpdatePropTableWithEqualities(andExpr.getRightOperand(), propTable);
		} else if (expr instanceof AbstractVariableRef && TypeUtils.hasBoolType(expr)) {
			// reference to a Boolean variable, e.g. [I0.0],
			// equivalent to [I0.0 = TRUE]
			propTable.updateWithEquality((AbstractVariableRef) expr, CfaInstanceSafeFactory.INSTANCE.trueLiteral());
		} else if (ExprUtils.isUnaryNegation(expr)
				&& ((UnaryLogicExpression) expr).getOperand() instanceof AbstractVariableRef
				&& TypeUtils.hasBoolType(((UnaryLogicExpression) expr).getOperand())) {
			// negated reference to a Boolean variable, e.g. [NOT I0.0],
			// equivalent to [I0.0 = FALSE]
			AbstractVariableRef operand = (AbstractVariableRef) ((UnaryLogicExpression) expr).getOperand();
			propTable.updateWithEquality(operand, CfaInstanceSafeFactory.INSTANCE.falseLiteral());
		} else {
			// Do nothing.
		}
	}

	private static void internalUpdatePropTableWithLeftValues(List<VariableAssignment> assignments,
			PropagationTable propTable) {
		for (VariableAssignment amt : assignments) {
			propTable.updateWithAssignment(amt);
		}
	}

	// 'forbiddenVars': these variables should not be included in the
	// new expression (i.e. if a propagation would cause the introduction
	// of any of these vars, don't perform the propagation)
	private void modifyExpression(Expression expr, PropagationTable propTable,
			List<AbstractVariableRef> forbiddenVars) {
		exprLoop: for (AbstractVariableRef varRef : varRefsOfExprTree(expr)) {
			if (propTable.hasValueFor(varRef)) {
				Expression replacementValue = propTable.valueFor(varRef);
				// Check if there is any forbidden variable in the replacement
				// value

				if (!forbiddenVars.isEmpty()) {
					for (AbstractVariableRef replacementRef : varRefsOfExprTree(replacementValue)) {
						for (AbstractVariableRef forbiddenVar : forbiddenVars) {
							if (CfaUtils.conflictingReferences(replacementRef, forbiddenVar)) {
								// A conflict would be introduced by this
								// propagation, therefore it should be skipped
								if (this.isFineLoggingEnabled()) {
									log.logDebug(String.format(
											"ExpressionPropagation: '%s' has not been replaced by '%s' as it contains the forbidden variable '%s'.",
											CfaToString.toDiagString(varRef),
											CfaToString.toDiagString(replacementValue),
											CfaToString.toDiagString(forbiddenVar)));
								}
								continue exprLoop;
							}
						}
					}
				}

				if (ExprUtils.exprTreeSize(expr) + ExprUtils.exprTreeSize(replacementValue) <= propagationSettings
						.getMaxExprSize()) {
					if (this.isFineLoggingEnabled()) {
						String replacementStr = CfaToString.toDiagString(replacementValue);
						Transition transition = EmfHelper.getContainerOfType(varRef, Transition.class);
						String transitionName = transition == null ? "N/A" : transition.getName();
						log.logDebug(String.format("ExpressionPropagation: '%s' has been replaced by '%s' on transition '%s'.",
								CfaToString.toDiagString(varRef), replacementStr, transitionName));
					}
					EcoreUtil.replace(varRef, replacementValue);
					simplifyReplacement(replacementValue);
					isModified = true;
				} else {
					if (this.isFineLoggingEnabled()) {
						log.logDebug(String.format(
								"ExpressionPropagation: '%s' has NOT been replaced by '%s' as it would result in a too big expression.",
								CfaToString.toDiagString(varRef), CfaToString.toDiagString(replacementValue)));
					}
				}
			}
		}
	}

	private void simplifyReplacement(Expression replacement) {
		EObject current = replacement.eContainer();
		while (current instanceof Expression) {
			EObject currentContainer = current.eContainer();
			boolean result = simplifier.trySimplify((Expression) current);
			if (!result) {
				// No need to go further up
				return;
			} else {
				current = currentContainer;
			}
		}
	}

	/**
	 * The computation of a location is blocked iff at least one of its incoming
	 * transitions does not have the propagation computed according to the
	 * {@code tableForTransition} map.
	 */
	private boolean isBlocked(Location loc) {
		return loc.getIncoming().stream().anyMatch(it -> !tableForTransition.containsKey(it));
	}

	/**
	 * Returns the first non-blocked task. If all tasks are blocked, it returns
	 * {@code null}.
	 */
	private Task getFirstNonblockedTask() {
		for (Task task : tasks) {
			if (!isBlocked(task.getLocation())) {
				return task;
			}
		}
		return null;
	}

	private boolean allTasksBlocked() {
		return getFirstNonblockedTask() == null;
	}

	/**
	 * Combines the propagation tables defined for each of the incoming
	 * transitions. It assumes that all incoming transitions have available
	 * propagation tables.
	 */
	private PropagationTable combinePropTablesFor(Location location) {
		List<PropagationTable> tablesToCombine = new ArrayList<>();
		for (Transition t : location.getIncoming()) {
			Preconditions.checkState(tableForTransition.containsKey(t), "Unavailable propagation table.");
			tablesToCombine.add(tableForTransition.get(t));
		}
		return PropagationTable.combine(tablesToCombine, propagationSettings);
	}

	private static Iterable<AbstractVariableRef> varRefsOfExprTree(Expression e) {
		return EmfHelper.getAllContentsOfTypeStream(e, AbstractVariableRef.class, true)::iterator;
	}
}
