/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.library.reduction.basic.impl.cfi

import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.base.common.logging.IPlcverifLogger
import cern.plcverif.base.interfaces.data.JobResult
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance
import cern.plcverif.base.models.cfa.textual.CfaToString
import cern.plcverif.base.models.expr.BinaryArithmeticExpression
import cern.plcverif.base.models.expr.BinaryLogicExpression
import cern.plcverif.base.models.expr.BinaryLogicOperator
import cern.plcverif.base.models.expr.BoolLiteral
import cern.plcverif.base.models.expr.ComparisonExpression
import cern.plcverif.base.models.expr.ComparisonOperator
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.ExpressionSafeFactory
import cern.plcverif.base.models.expr.UnaryLogicExpression
import cern.plcverif.base.models.expr.UnaryLogicOperator
import cern.plcverif.base.models.expr.utils.ExprUtils
import cern.plcverif.library.reduction.basic.IInstanceReduction
import cern.plcverif.library.reduction.basic.impl.AbstractLoggingReduction
import com.google.common.base.Preconditions
import java.util.List
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.util.EcoreUtil

import static extension cern.plcverif.base.models.expr.utils.ExprUtils.isBoolLiteral
import static extension cern.plcverif.base.models.expr.utils.ExprUtils.isFalseLiteral
import static extension cern.plcverif.base.models.expr.utils.ExprUtils.isTrueLiteral

class ExpressionSimplification extends AbstractLoggingReduction implements IInstanceReduction {
	static ExpressionSafeFactory factory = ExpressionSafeFactory.INSTANCE;

	IPlcverifLogger log = null;

	override reduce(CfaNetworkInstance cfa, JobResult result) {		
		var atLeastOneChange = false;
		
		// Calling 'reduceOnce' exhaustively
		while (reduceOnce(cfa, result)) {
			atLeastOneChange = true;
		}
		
		return atLeastOneChange;
	}
	
	def setLogger(IPlcverifLogger log) {
		this.log = log;
	}
	
	def reduceOnce(CfaNetworkInstance cfa, JobResult result) {
		var change = false;
		this.log = result.currentStage;

		// Collect candidates
		val List<UnaryLogicExpression> negations = newArrayList();
		val List<BinaryLogicExpression> andOps = newArrayList();
		val List<BinaryLogicExpression> orOps = newArrayList();
		val List<BinaryLogicExpression> impliesOps = newArrayList();
		val List<ComparisonExpression> eqExprs = newArrayList();
		val List<ComparisonExpression> neqExprs = newArrayList();
		val List<ComparisonExpression> comparisonExprs = newArrayList();
		val List<BinaryArithmeticExpression> arithmeticExprs = newArrayList();

		// TODO merge with 'trySimplify'
		for (EObject e : EmfHelper.toIterable(cfa.eAllContents())) {
			switch (e) {
				UnaryLogicExpression: {
					if (e.operator == UnaryLogicOperator.NEG && e.operand.isBoolLiteral) {
						negations.add(e);
					}
				}
				BinaryLogicExpression: {
					if (e.operator == BinaryLogicOperator.AND) {
						andOps.add(e);
					} else if (e.operator == BinaryLogicOperator.OR) {
						orOps.add(e);
					} else if (e.operator == BinaryLogicOperator.IMPLIES) {
						impliesOps.add(e);
					}
				}
				ComparisonExpression: {
					comparisonExprs.add(e);
					if (e.operator == ComparisonOperator.EQUALS) {
						eqExprs.add(e);
					} else if (e.operator == ComparisonOperator.NOT_EQUALS) {
						neqExprs.add(e);
					}
				}
				BinaryArithmeticExpression: {
					arithmeticExprs.add(e);
				}
			}
		}

		for (negation : negations) {
			change = negationSimplification(negation) || change;
		}

		for (andOp : andOps) {
			change = andSimplification(andOp) || change;
		}

		for (orOp : orOps) {
			change = orSimplification(orOp) || change;
		}

		for (impliesOp : impliesOps) {
			change = impliesSimplification(impliesOp) || change;
		}

		for (comparisonExpr : comparisonExprs.reverseView) {
			change = comparisonSimplification(comparisonExpr) || change;
		}

		for (eqExpr : eqExprs.filter[it|it.eContainer !== null]) {
			change = eqSimplification(eqExpr) || change;
		}

		for (neqExpr : neqExprs.filter[it|it.eContainer !== null]) {
			change = neqSimplification(neqExpr) || change;
		}

		for (arithmeticExpr : arithmeticExprs.reverseView.filter[it|it.eContainer !== null]) {
			change = arithmeticSimplification(arithmeticExpr) || change;
		}

		this.log = null;
		return change;
	}

	/**
	 * Tries to simplify the given expression <b>and its whole subtree</b>. If possible, the expression itself will
	 * be modified and {@code true} result will be returned.
	 */
	def boolean trySimplifySubtree(Expression expr) {
		if (expr.eContents.isEmpty) {
			return false;
		}
		
		var modification = false;
		for (e : expr.eContents) {
			if (e instanceof Expression) {
				modification = trySimplifySubtree(e) || modification;
			}
		}
		
		return trySimplify(expr) || modification;
	}

	/**
	 * Tries to simplify the given expression. If possible, the expression itself will
	 * be modified and {@code true} result will be returned.
	 */
	def boolean trySimplify(Expression expr) {
		var changed = false;
		switch (expr) {
			UnaryLogicExpression: {
				if (expr.operator == UnaryLogicOperator.NEG && expr.operand.isBoolLiteral) {
					changed = negationSimplification(expr) || changed;
				}
			}
			BinaryLogicExpression: {
				if (expr.operator == BinaryLogicOperator.AND) {
					changed = andSimplification(expr) || changed;
				} else if (expr.operator == BinaryLogicOperator.OR) {
					changed = orSimplification(expr) || changed;
				} else if (expr.operator == BinaryLogicOperator.IMPLIES) {
					changed = impliesSimplification(expr) || changed;
				}
			}
			ComparisonExpression: {
				changed = comparisonSimplification(expr) || changed;
				if (expr.operator == ComparisonOperator.EQUALS) {
					changed = eqSimplification(expr) || changed;
				} else if (expr.operator == ComparisonOperator.NOT_EQUALS) {
					changed = neqSimplification(expr) || changed;
				}
			}
			BinaryArithmeticExpression: {
				changed = arithmeticSimplification(expr) || changed;
			}
		}

		return changed;
	}

	private def boolean negationSimplification(UnaryLogicExpression expression) {
		// !TRUE --> FALSE or !FALSE --> TRUE
		if (expression.operand.isBoolLiteral) {
			val boolOperand = expression.operand as BoolLiteral;
			val replacement = factory.createBoolLiteral(!boolOperand.value);

			logAndReplace(expression, replacement);
			return true;
		}

		return false;
	}

	private def boolean andSimplification(BinaryLogicExpression expression) {
		Preconditions.checkArgument(expression.operator == BinaryLogicOperator.AND);

		val left = expression.leftOperand;
		val right = expression.rightOperand;

		// TRUE AND right --> right
		if (left.isTrueLiteral) {
			logAndReplace(expression, right);
			return true;
		}

		// left AND TRUE --> left
		if (right.isTrueLiteral) {
			logAndReplace(expression, left);
			return true;
		}

		// left AND FALSE --> FALSE
		// FALSE AND right --> FALSE
		if (left.isFalseLiteral || right.isFalseLiteral) {
			logAndReplace(expression, factory.falseLiteral);
			return true;
		}

		// left AND left --> left
		if (EcoreUtil.equals(left, right)) {
			logAndReplace(expression, left);
			return true;
		}

		// _left_ AND (NOT _left_) --> FALSE
		if (ExprUtils.isUnaryNegation(right) && EcoreUtil.equals((right as UnaryLogicExpression).operand, left)) {
			logAndReplace(expression, factory.falseLiteral);
			return true;
		}

		// (NOT _right_) AND _right_ --> FALSE
		if (ExprUtils.isUnaryNegation(left) && EcoreUtil.equals((left as UnaryLogicExpression).operand, right)) {
			logAndReplace(expression, factory.falseLiteral);
			return true;
		}

		if (left instanceof BinaryLogicExpression && right instanceof BinaryLogicExpression) {
			// (e1 * e2) AND (e3 * e4)
			val e1 = (left as BinaryLogicExpression).leftOperand;
			val e2 = (left as BinaryLogicExpression).rightOperand;
			val e3 = (right as BinaryLogicExpression).leftOperand;
			val e4 = (right as BinaryLogicExpression).rightOperand;

			if (ExprUtils.isOrOperation(left) && ExprUtils.isOrOperation(right)) {
				// (e1 OR e2) AND (e3 OR e4)
				
				if (EcoreUtil.equals(e1, e3)) {
					// e1=e3:  (a OR b) AND (a OR c) --> a OR (b AND c)
					// newRight: b AND c === e2 AND e4
					val newRight = factory.and(e2, e4);

					logAndModify(expression, e1, newRight, BinaryLogicOperator.OR);
					return true;
				} else if (EcoreUtil.equals(e1, e4)) {
					// e1=e4:  (a OR b) AND (c OR a) --> a OR (b AND c)
					// newRight: b AND c === e2 AND e3
					val newRight = factory.and(e2, e3);

					logAndModify(expression, e1, newRight, BinaryLogicOperator.OR);
					return true;
				} else if (EcoreUtil.equals(e2, e3)) {
					// e2=e3:  (a OR b) AND (b OR c) --> b OR (a AND c)	
					// newRight: a AND c === e1 AND e4
					val newRight = factory.and(e1, e4);

					logAndModify(expression, e2, newRight, BinaryLogicOperator.OR);
					return true;
				} else if (EcoreUtil.equals(e2, e4)) {
					// e2=e4:  (a OR b) AND (c OR b) --> b OR (a AND c)
					// newRight: a AND c === e1 AND e3
					val newRight = factory.and(e1, e3);

					logAndModify(expression, e2, newRight, BinaryLogicOperator.OR);
					return true;
				}
			} else if (ExprUtils.isAndOperation(left) && ExprUtils.isAndOperation(right)) {
				// (e1 AND e2) AND (e3 AND e4)
				
				if (EcoreUtil.equals(e1, e3) || EcoreUtil.equals(e2, e3)) {
					// e1=e3:  (a AND b) AND (a AND c) --> (a AND b) AND c
					// e2=e3:  (a AND b) AND (b AND c) --> (a AND b) AND c
					logAndModify(expression, left, e4, BinaryLogicOperator.AND);
					return true;
				} else if (EcoreUtil.equals(e1, e4) || EcoreUtil.equals(e2, e4)) {
					// e1=e4:  (a AND b) AND (c AND a) --> (a AND b) AND c
					// e2=e4:  (a AND b) AND (c AND b) --> (a AND b) AND c
					logAndModify(expression, left, e3, BinaryLogicOperator.AND);
					return true;
				}
			}
		}
		

		return false;
	}

	private def boolean orSimplification(BinaryLogicExpression expression) {
		Preconditions.checkArgument(expression.operator == BinaryLogicOperator.OR);
		val left = expression.leftOperand;
		val right = expression.rightOperand;

		// FALSE OR right --> right
		if (expression.leftOperand.isFalseLiteral) {
			logAndReplace(expression, expression.rightOperand);
			return true;
		}

		// left OR FALSE --> left
		if (expression.rightOperand.isFalseLiteral) {
			logAndReplace(expression, expression.leftOperand);
			return true;
		}

		// left OR TRUE --> TRUE
		// TRUE OR right --> TRUE
		if (expression.leftOperand.isTrueLiteral || expression.rightOperand.isTrueLiteral) {
			logAndReplace(expression, factory.trueLiteral);
			return true;
		}

		// left OR left --> left
		if (EcoreUtil.equals(expression.leftOperand, expression.rightOperand)) {
			logAndReplace(expression, expression.leftOperand);
			return true;
		}

		// left OR (NOT left) --> TRUE
		if (ExprUtils.isUnaryNegation(right) && EcoreUtil.equals((right as UnaryLogicExpression).operand, left)) {
			logAndReplace(expression, factory.trueLiteral);
			return true;
		}

		// (NOT right) OR right --> TRUE
		if (ExprUtils.isUnaryNegation(left) && EcoreUtil.equals((left as UnaryLogicExpression).operand, right)) {
			logAndReplace(expression, factory.trueLiteral);
			return true;
		}

		if (ExprUtils.isOrOperation(left) && ExprUtils.isOrOperation(right)) {
			// (e1 OR e2) OR (e3 OR e4)
			val e1 = (left as BinaryLogicExpression).leftOperand;
			val e2 = (left as BinaryLogicExpression).rightOperand;
			val e3 = (right as BinaryLogicExpression).leftOperand;
			val e4 = (right as BinaryLogicExpression).rightOperand;

			if (EcoreUtil.equals(e1, e3) || EcoreUtil.equals(e2, e3)) {
				// e1=e3:  (a OR b) OR (a OR c) --> (a OR b) OR c
				// e2=e3:  (a OR b) OR (b OR c) --> (a OR b) OR c
				
				logAndModify(expression, left, e4, BinaryLogicOperator.OR);
				return true;
			} else if (EcoreUtil.equals(e1, e4) || EcoreUtil.equals(e2, e4)) {
				// e1=e4: (a OR b) OR (c OR a) --> (a OR b) OR c
				// e2=e4: (a OR b) OR (c OR b) --> (a OR b) OR c
				
				logAndModify(expression, left, e3, BinaryLogicOperator.OR);
				return true;
			}
		}

		return false;
	}

	private def boolean impliesSimplification(BinaryLogicExpression expression) {
		Preconditions.checkArgument(expression.operator == BinaryLogicOperator.IMPLIES);

		// FALSE IMPLIES right --> TRUE
		if (expression.leftOperand.isFalseLiteral) {
			logAndReplace(expression, factory.trueLiteral);
			return true;
		}

		// TRUE IMPLIES right --> right
		if (expression.leftOperand.isTrueLiteral) {
			logAndReplace(expression, expression.rightOperand);
			return true;
		}

		return false;
	}

	private def comparisonSimplification(ComparisonExpression expression) {
		if (ExprUtils.isComputableConstant(expression)) {
			val boolean result = ExprUtils.toBoolean(expression);
			logAndReplace(expression, factory.createBoolLiteral(result));
			return true;
		}

		return false;
	}

	private def boolean eqSimplification(ComparisonExpression expression) {
		// a == a --> TRUE
		Preconditions.checkArgument(expression.operator == ComparisonOperator.EQUALS);
		if (EcoreUtil.equals(expression.leftOperand, expression.rightOperand)) {
			// replace with true literal
			logAndReplace(expression, factory.trueLiteral);
			return true;
		}

		// a == TRUE --> a
		if (expression.rightOperand.isTrueLiteral) {
			logAndReplace(expression, expression.leftOperand);
			return true;
		}

		return false;
	}

	private def boolean neqSimplification(ComparisonExpression expression) {
		// a != a --> FALSE
		Preconditions.checkArgument(expression.operator == ComparisonOperator.NOT_EQUALS);
		if (EcoreUtil.equals(expression.leftOperand, expression.rightOperand)) {
			// replace with false literal
			logAndReplace(expression, factory.falseLiteral);
			return true;
		}

		return false;
	}

	private def boolean arithmeticSimplification(BinaryArithmeticExpression expression) {
		// literal OP literal --> new literal
		if (expression.leftOperand.type.dataTypeEquals(expression.rightOperand.type)) {
			if (ExprUtils.isComputableConstant(expression)) {
				val result = ExprUtils.compute(expression);
				if (result !== expression) {
					logAndReplace(expression, result);
					return true;
				}
			}
		}

		return false;
	}

	// Helper methods
	private def void logAndReplace(EObject toBeReplaced, EObject replacement) {
		if (this.isFineLoggingEnabled() && this.log !== null) {
			log.logDebug(
				"ExpressionSimplification: '%s' has been simplified to '%s'.",
				CfaToString.toDiagString(toBeReplaced),
				CfaToString.toDiagString(replacement)
			);
		}

		EcoreUtil.replace(toBeReplaced, replacement);
	}

	private def void logAndModify(BinaryLogicExpression toModify, Expression newLeft, Expression newRight,
		BinaryLogicOperator newOp) {
		val oldExprString = CfaToString.toDiagString(toModify);

		toModify.leftOperand = newLeft;
		toModify.rightOperand = newRight;
		toModify.operator = newOp;

		if (this.isFineLoggingEnabled() && this.log !== null) {
			log.logDebug(
				"ExpressionSimplification: '%s' has been modified to '%s'.",
				oldExprString,
				CfaToString.toDiagString(toModify)
			);
		}
	}
}
