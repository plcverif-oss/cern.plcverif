/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic.impl.cfi;

import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.models.cfa.cfainstance.AutomatonInstance;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.library.reduction.basic.IInstanceReduction;
import cern.plcverif.library.reduction.basic.impl.AbstractLoggingReduction;

public abstract class AbstractAutomatonReduction extends AbstractLoggingReduction implements IInstanceReduction {
	
	@Override
	public final boolean reduce(CfaNetworkInstance cfa, JobResult result) {
		boolean ret = false;
		
		for (AutomatonInstance automaton : cfa.getAutomata()) {
			// The reduce() shall be executed even if ret == false.
			ret = reduce(automaton, result) || ret;
		}
		return ret;
	}

	protected abstract boolean reduce(AutomatonInstance automaton, JobResult result);
}
