/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic.impl.cfi;

import org.eclipse.emf.ecore.util.EcoreUtil;

import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.models.cfa.cfabase.Transition;
import cern.plcverif.base.models.cfa.cfainstance.AssignmentTransition;
import cern.plcverif.base.models.cfa.cfainstance.AutomatonInstance;
import cern.plcverif.base.models.cfa.cfainstance.VariableAssignment;
import cern.plcverif.base.models.cfa.utils.CfaCollectionDeleter;

/**
 * Reduction to remove the unnecessary assignments in the form of {@code var1 := var1}.
 */
public class RemoveIdentityAssignments extends AbstractAutomatonReduction {

	@Override
	protected boolean reduce(AutomatonInstance automaton, JobResult result) {
		CfaCollectionDeleter deleter = new CfaCollectionDeleter();
		
		for (Transition t : automaton.getTransitions()) {
			if (t instanceof AssignmentTransition) {
				for (VariableAssignment assignment : ((AssignmentTransition) t).getAssignments()) {
					if (!assignment.isFrozen() && EcoreUtil.equals(assignment.getLeftValue(), assignment.getRightValue())) {
						deleter.registerForDeletion(assignment);
					}
				}
			}
		}
		
		boolean amtToBeDeleted = !deleter.isEmpty();
		deleter.deleteAll();
		return amtToBeDeleted;
	}

}
