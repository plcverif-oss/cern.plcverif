/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic.impl.cfi.coi;

import java.util.HashMap;
import java.util.Stack;

import com.google.common.collect.Lists;

import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfabase.Transition;
import cern.plcverif.base.models.cfa.cfainstance.AutomatonInstance;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;

/**
 * Identifies all states occurring on every possible trace from the initial
 * state to the predecessor of the initial state.
 * 
 * @author Daniel DARVAS (CERN BE-ICS-AP)
 * 
 */
public final class UnconditionalLocations {
	/**
	 * Fraction of paths/traces going through each location.
	 */
	private final HashMap<Location, BigFraction> pathFractions = new HashMap<>();

	/**
	 * Private constructor. The class can only be instantiated through the
	 * static functions.
	 */
	private UnconditionalLocations() {
	}

	/**
	 * Determines which states are unconditional in the given CFA network.
	 * 
	 * @param cfaNetwork
	 *            CFA network to analyze.
	 * @return Unconditional locations descriptor.
	 */
	public static UnconditionalLocations compute(CfaNetworkInstance cfaNetwork) {
		UnconditionalLocations ret = new UnconditionalLocations();
		for (AutomatonInstance automaton : cfaNetwork.getAutomata()) {
			ret.computeInternal(automaton);
		}
		return ret;
	}

	/**
	 * Determines which locations are unconditional in the given automaton.
	 * 
	 * @param automaton
	 *            Automaton.
	 * @return Unconditional states descriptor.
	 */
	public static UnconditionalLocations compute(AutomatonInstance automaton) {
		UnconditionalLocations ret = new UnconditionalLocations();
		ret.computeInternal(automaton);
		return ret;
	}

	/**
	 * Returns the string representation of the fraction of paths going through
	 * the given location.
	 * 
	 * @param location
	 *            Location.
	 * @return Fraction of paths going through the given location.
	 */
	public String getValue(Location location) {
		if (this.pathFractions.containsKey(location) == false) {
			return "-/-";
		} else {
			return this.pathFractions.get(location).toString();
		}
	}

	/**
	 * Determines if the given location is unconditional (i.e. it is present on
	 * every path/trace).
	 * 
	 * @return True, iff the state is unconditional.
	 */
	public boolean isPresentOnEveryComputationPath(Location location) {
		BigFraction f = this.pathFractions.get(location);
		return (f != null && f.equalsTo(1));
	}

	/**
	 * Computes fractions for the given automaton.
	 */
	private void computeInternal(AutomatonInstance automaton) {
		this.pathFractions.put(automaton.getInitialLocation(), new BigFraction(1));
		for (Transition t : automaton.getInitialLocation().getOutgoing()) {
			computeInternal(automaton, t.getTarget());
		}
	}

	/**
	 * Computes fractions for the given location in the given automaton.
	 */
	private void computeInternal(AutomatonInstance automaton, Location startLocation) {
		Stack<Location> locsToCompute = new Stack<>();
		locsToCompute.push(startLocation);

		mainLoop:
		while (!locsToCompute.isEmpty()) {
			Location location = locsToCompute.pop();
			
			if (this.pathFractions.containsKey(location)) {
				// this is a loop
				continue mainLoop;
			}

			boolean anyUncondInLoc = (location.getIncoming().stream()
					.anyMatch(it -> pathFractions.containsKey(it.getSource())
							&& pathFractions.get(it.getSource()).equalsTo(1)
							&& it.getSource().getOutgoing().size() == 1));

			BigFraction f = new BigFraction(0);
			if (anyUncondInLoc) {
				// This is a workaround for main loop
				f = new BigFraction(1);
			} else {
				for (Transition t : location.getIncoming()) {
					if (this.pathFractions.containsKey(t.getSource()) == false) {
						// not every predecessor state is computed yet
						// It will be computed later.
						continue mainLoop;
					}

					BigFraction tf = BigFraction.divide(this.pathFractions.get(t.getSource()),
							t.getSource().getOutgoing().size());
					f = BigFraction.add(f, tf);
				}
			}

			this.pathFractions.put(location, f);

			// reverse is to have the same execution order as with the previous recursive solution
			for (Transition t : Lists.reverse(location.getOutgoing())) {
				locsToCompute.push(t.getTarget());
			}
		}
	}
}
