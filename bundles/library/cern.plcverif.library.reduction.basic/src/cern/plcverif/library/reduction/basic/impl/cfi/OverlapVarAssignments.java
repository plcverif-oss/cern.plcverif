/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic.impl.cfi;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfabase.Transition;
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariableRef;
import cern.plcverif.base.models.cfa.cfainstance.AssignmentTransition;
import cern.plcverif.base.models.cfa.cfainstance.AutomatonInstance;
import cern.plcverif.base.models.cfa.utils.CfaInstanceUtils;
import cern.plcverif.base.models.cfa.utils.CfaUtils;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.utils.ExprUtils;

/**
 * Reduction that tries to merge successor transitions if there is no overlap
 * between their variable assignments.
 */
public class OverlapVarAssignments extends AbstractAutomatonReduction {

	private static class Candidate {
		private final Transition tA;
		private final Transition tB;
		private final Location loc2;

		public Candidate(Transition tA, Transition tB, Location loc2) {
			Preconditions.checkArgument(tA.getTarget() == loc2);
			Preconditions.checkArgument(tB.getSource() == loc2);

			this.tA = tA;
			this.tB = tB;
			this.loc2 = loc2;
		}
	}

	@Override
	protected boolean reduce(AutomatonInstance automaton, JobResult result) {
		boolean modificationDone = false;

		List<Candidate> candidates = automaton.getLocations().stream()
				.filter(it -> it.getIncoming().size() == 1 && it.getOutgoing().size() == 1)
				.map(it -> new Candidate(it.getIncoming().get(0), it.getOutgoing().get(0), it))
				.collect(Collectors.toList());

		for (Candidate candidate : candidates) {
			modificationDone = tryReduce(candidate) || modificationDone;
		}

		return modificationDone;
	}

	private static boolean tryReduce(Candidate candidate) {
		// Check if it is a good match

		if (!(candidate.tA instanceof AssignmentTransition && candidate.tB instanceof AssignmentTransition)) {
			return false;
		}

		if (candidate.tA.eContainer() == null || candidate.tB.eContainer() == null
				|| candidate.loc2.eContainer() == null) {
			// already deleted
			return false;
		}

		AssignmentTransition tA = (AssignmentTransition) candidate.tA;
		AssignmentTransition tB = (AssignmentTransition) candidate.tB;

		Location loc2 = candidate.loc2;
		Location loc3 = tB.getTarget();
		Preconditions.checkState(loc2.getIncoming().size() == 1);
		Preconditions.checkState(loc2.getOutgoing().size() == 1);

		if (CfaUtils.isFrozenInitOrEnd(loc2) || tA.isFrozen() || tB.isFrozen()) {
			// Frozen object should not be modified.
			return false;
		}

		if (!ExprUtils.isTrueLiteral(tB.getCondition())) {
			// The guard of tB shall be 'TRUE'.
			return false;
		}

		List<AbstractVariableRef> varsVa = tA.getAssignments().stream().map(it -> it.getLeftValue())
				.collect(Collectors.toList());
		List<AbstractVariableRef> varsVb = tB.getAssignments().stream().map(it -> it.getLeftValue())
				.collect(Collectors.toList());
		List<AbstractVariableRef> varsEa = collectVarRefs(tA.getAssignments().stream().map(it -> it.getRightValue()));
		List<AbstractVariableRef> varsEb = collectVarRefs(tB.getAssignments().stream().map(it -> it.getRightValue()));

		boolean overlap = false;
		overlap = containsCommonItem(varsVa, varsEb);
		overlap = overlap || containsCommonItem(varsVb, varsEa);
		overlap = overlap || containsCommonItem(varsVa, varsVb);

		if (overlap) {
			// There is an overlap, we cannot merge the two transitions.
			return false;
		}

		// ------------------------------------------------
		// Complete match, we can merge the two transitions.

		// Move all assignments of tB to tA
		tA.getAssignments().addAll(tB.getAssignments());

		// change target of tA to loc3
		tA.setTarget(loc3);

		// Remove tB and loc2
		CfaInstanceUtils.deleteTransitionNoAnnotation(tB);
		CfaInstanceUtils.deleteLocationNoAnnotation(loc2);

		return true;
	}

	private static boolean containsCommonItem(List<AbstractVariableRef> refList1, List<AbstractVariableRef> refList2) {
		// We check if there is a reference to the same variable.
		// Note that currently 'a[1]' and 'a[2]' refers to the same variable,
		// thus the assignments 'a[1] := TRUE' and 'a[2] := TRUE' cannot be
		// merged.

		for (AbstractVariableRef ref1 : refList1) {
			for (AbstractVariableRef ref2 : refList2) {
				if (CfaUtils.conflictingReferences(ref1, ref2)) {
					return true;
				}
			}
		}

		return false;
	}

	private static List<AbstractVariableRef> collectVarRefs(Stream<Expression> expressions) {
		List<AbstractVariableRef> ret = new ArrayList<>();
		expressions.forEach(expr -> ret.addAll(EmfHelper.getAllContentsOfType(expr, AbstractVariableRef.class, true)));
		return ret;
	}
}