/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic.impl.cfd;

import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.library.reduction.basic.IDeclarationReduction;
import cern.plcverif.library.reduction.basic.impl.AbstractLoggingReduction;

public abstract class AbstractAutomatonReduction extends AbstractLoggingReduction implements IDeclarationReduction {
	@Override
	public final boolean reduce(CfaNetworkDeclaration cfa, JobResult result) {
		boolean ret = false;
		
		for (AutomatonDeclaration automaton : cfa.getAutomata()) {
			// The reduce() shall be executed even if ret == false.
			ret = reduce(automaton, result) || ret;
		}
		return ret;
	}

	protected abstract boolean reduce(AutomatonDeclaration automaton, JobResult result);
}
