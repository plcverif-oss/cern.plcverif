/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic.impl.cfi.propagation;

public class ExpressionPropagationSettings {
	private final boolean constantPropagationOnly;
	
	private final int maxAge;
	private final int maxExprSize;
	
	public ExpressionPropagationSettings(boolean constantPropagationOnly, int maxAge, int maxExprSize) {
		this.constantPropagationOnly = constantPropagationOnly;
		this.maxAge = maxAge;
		this.maxExprSize = maxExprSize;
	}

	public int getMaxAge() {
		return maxAge;
	}
	
	public int getMaxExprSize() {
		return maxExprSize;
	}
	
	public boolean isConstantPropagationOnly() {
		return constantPropagationOnly;
	}
}
