/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic.impl.cfi.coi;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EObjectSet;
import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.interfaces.data.result.JobStage;
import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfabase.Transition;
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariable;
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariableRef;
import cern.plcverif.base.models.cfa.cfainstance.AssignmentTransition;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.cfa.cfainstance.VariableAssignment;
import cern.plcverif.base.models.cfa.utils.CfaCollectionDeleter;
import cern.plcverif.base.models.cfa.utils.CfaInstanceUtils;
import cern.plcverif.base.models.cfa.utils.CfaUtils;
import cern.plcverif.base.models.expr.ExpressionSafeFactory;
import cern.plcverif.library.reduction.basic.BasicReductionSettings;
import cern.plcverif.library.reduction.basic.IInstanceReduction;
import cern.plcverif.library.reduction.basic.impl.AbstractLoggingReduction;

public class CoiReduction extends AbstractLoggingReduction implements IInstanceReduction {
	private final BasicReductionSettings settings;
	private int callCounter = 0;
	
	public CoiReduction(BasicReductionSettings settings) {
		Preconditions.checkNotNull(settings, "settings");
		this.settings = settings;
	}

	@Override
	public boolean reduce(CfaNetworkInstance cfaNetwork, JobResult result) {
		callCounter++;
		
		boolean changed = false;
		JobStage stageLog = result.currentStage();
		
		// Precomputation: a variable present in a frozen assignment should also
		// be frozen
		makeVarsInFrozenAmtsFrozen(cfaNetwork);

		// (1) Collect all needed variables
		Set<AbstractVariable> neededVars = collectNeededVars(cfaNetwork, result);

		// (2) Removal
		changed = removeUnnecessaryElements(cfaNetwork, neededVars, stageLog);

		return changed;
	}

	private static void makeVarsInFrozenAmtsFrozen(CfaNetworkInstance cfaNetwork) {
		// TODO: maybe this should not be set globally, just for COI?
		for (VariableAssignment frozenAmt : EmfHelper.getAllContentsOfType(cfaNetwork, VariableAssignment.class, false,
				it -> it.isFrozen())) {
			for (AbstractVariableRef varRef : EmfHelper.getAllContentsOfType(frozenAmt, AbstractVariableRef.class,
					false)) {
				varRef.setFrozen(true);
				varRef.getVariable().setFrozen(true);
			}
		}
	}

	private Set<AbstractVariable> collectNeededVars(CfaNetworkInstance cfaNetwork, JobResult result) {
		VariableDependencies dependencies = VariableDependencies.collect(cfaNetwork);
		
		// Print variable dependency graph if needed
		if (this.settings.isPrintVariableDependencies()) {
			try {
				Path path = result.getOutputDirectoryOrCreateTemp().resolve(String.format("%s.vardep-%s.dot",
						result.getJobMetadata().getIdWithoutSpecialChars(), callCounter));
				IoUtils.writeAllContent(path, PrintVarDepGraph.print(dependencies));
			} catch (IOException e) {
				result.currentStage().logWarning("Unable to serialize the variable dependency graph.", e);
			}
		}

		// 1.1 All frozen variables are needed
		Set<AbstractVariable> neededVars = cfaNetwork.getVariables().stream().filter(it -> it.isFrozen())
				.collect(Collectors.toSet());
		Set<AbstractVariable> varsToBeChecked = new HashSet<>(neededVars);

		// 1.2 Find variables in frozen assignments
		Collection<VariableAssignment> frozenAssignments = EmfHelper.getAllContentsOfType(cfaNetwork,
				VariableAssignment.class, false, it -> it.isFrozen());
		for (VariableAssignment frozenAmt : frozenAssignments) {
			EmfHelper.getAllContentsOfTypeStream(frozenAmt, AbstractVariableRef.class,
					false).forEach(it -> neededVars.add(it.getVariable()));
		}

		// 1.3 Find the fixed point of needed variables
		while (!varsToBeChecked.isEmpty()) {
			Set<AbstractVariable> newlyExploredNeededVars = new HashSet<>();
			for (AbstractVariable neededVar : neededVars) {
				Set<AbstractVariable> prerequisites = dependencies.getPrerequisitesOf(neededVar);
				newlyExploredNeededVars.addAll(prerequisites);
			}
			newlyExploredNeededVars.removeAll(neededVars);
			neededVars.addAll(newlyExploredNeededVars);
			varsToBeChecked = newlyExploredNeededVars;
		}

		return neededVars;
	}

	private static boolean removeUnnecessaryElements(CfaNetworkInstance cfaNetwork, Set<AbstractVariable> neededVars, JobStage stageLog) {
		boolean changed = false;

		// 3.1 Remove all assignments assigning unnecessary variables
		List<VariableAssignment> amtToRemove = CfaInstanceUtils.getAllVariableAssignments(cfaNetwork,
				it -> !neededVars.contains(it.getLeftValue().getVariable()));
		amtToRemove.forEach(amt -> CfaInstanceUtils.deleteVariableAssignment(amt));

		// 3.2 Replace each guard with 'true' that contains unnecessary
		// variables
		Collection<Transition> allTransitions = CfaUtils.getAllTransitions(cfaNetwork);
		Set<Location> locWithModifiedOutTrans = new HashSet<>();
		for (Transition t : allTransitions) {
			boolean containsUnnecessaryVar = false;
			for (AbstractVariableRef varRef : EmfHelper.getAllContentsOfType(t.getCondition(),
					AbstractVariableRef.class, true)) {
				if (!neededVars.contains(varRef.getVariable())) {
					containsUnnecessaryVar = true;
					break;
				}
			}

			if (containsUnnecessaryVar) {
				Preconditions.checkState(t instanceof AssignmentTransition);
				Preconditions.checkState(((AssignmentTransition) t).getAssignments().isEmpty());
				EcoreUtil.replace(t.getCondition(), ExpressionSafeFactory.INSTANCE.trueLiteral());

				locWithModifiedOutTrans.add(t.getSource());
			}
		}

		// 3.3 Merge potentially equivalent transitions
		// (we don't need several parallel transitions with 'true' literal and
		// without assignment
		CfaCollectionDeleter deleter = new CfaCollectionDeleter();
		for (Location l : locWithModifiedOutTrans) {
			boolean hasEmptyTrans = false;
			for (Transition t : new ArrayList<>(l.getOutgoing())) {
				if (CfaInstanceUtils.isEmptyTransition(t)) {
					if (hasEmptyTrans) {
						deleter.registerForDeletion(t);
					} else {
						hasEmptyTrans = true;
					}
				}
			}
		}

		// 3.4 Remove the variables themselves
		EObjectSet<AbstractVariable> nonNeededVars = new EObjectSet<>();
		cfaNetwork.getVariables().stream().filter(var -> !neededVars.contains(var)).forEach(nonNeededVars::add);
		
		// 3.4.1 Check if the variables are in use anywhere; register uses for deletion too
		for (AbstractVariableRef ref : EmfHelper.getAllContentsOfType(cfaNetwork, AbstractVariableRef.class,
				false, it -> nonNeededVars.contains(it.getVariable()))) {
			stageLog.logWarning("COI: variable '%s' is about to be deleted, but is used in %s.", ref.getVariable().getDisplayName(), ref.eContainer());
			
			VariableAssignment containingAmt = EmfHelper.getContainerOfType(ref, VariableAssignment.class);
			if (containingAmt != null) {
				deleter.registerForDeletion(containingAmt);
			}
		}
		
		// 3.4.2 Delete the non-needed variables
		for (AbstractVariable nonNeededVar : nonNeededVars) {
			// delete 'var'
			stageLog.logDebug("COI deleted the variable '%s'.", nonNeededVar.getName());
			changed = true;
			deleter.registerForDeletion(nonNeededVar);
		}
	
		deleter.deleteAll();
		return changed;
	}
}
