/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.library.reduction.basic.impl.cfi.coi

import cern.plcverif.library.reduction.basic.impl.cfi.coi.VariableDependency.DepencencyType

class PrintVarDepGraph {
	static def print(VariableDependencies varDeps) '''
		digraph G {
			
			«FOR dep : varDeps.allDependencies»
				«dep.print()»
			«ENDFOR»
		}
	'''

	private static def print(VariableDependency varDep) '''
		"«varDep.dependent»" -> "«varDep.prerequisite»" [ color = "«varDepColor(varDep.dependencyType)»" ];
	'''

	private def static varDepColor(DepencencyType type) {
		switch (type) {
			case ASSIGNMENT: return "red"
			case CONTROL: return "blue"
			default: throw new UnsupportedOperationException("Unknown dependency type: " + type)
		}
	}

}
