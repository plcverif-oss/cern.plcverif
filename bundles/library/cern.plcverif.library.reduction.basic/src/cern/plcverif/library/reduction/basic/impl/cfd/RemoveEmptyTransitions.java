/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic.impl.cfd;
import static cern.plcverif.base.models.cfa.utils.CfaUtils.isFrozenInitOrEnd;

import java.util.List;
import java.util.stream.Collectors;

import com.google.common.base.Preconditions;

import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfadeclaration.AssignmentTransition;
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration;
import cern.plcverif.base.models.cfa.utils.CfaDeclarationUtils;
import cern.plcverif.base.models.cfa.utils.CfaUtils;

/**
 * Reduction to remove the transitions without guard and assignment.
 */
public class RemoveEmptyTransitions extends AbstractAutomatonReduction {

	@Override
	protected boolean reduce(AutomatonDeclaration automaton, JobResult result) {
		boolean modificationDone = false;

		List<AssignmentTransition> emptyTransitions = automaton.getTransitions().stream()
				.filter(it -> it instanceof AssignmentTransition).map(it -> (AssignmentTransition) it)
				.filter(it -> CfaDeclarationUtils.isEmptyTransition(it)).filter(it -> it.getSource() != null)
				.filter(it -> it.getSource().getOutgoing().size() == 1 && it.getTarget().getIncoming().size() == 1)
				.collect(Collectors.toList());

		for (AssignmentTransition t : emptyTransitions) {
			modificationDone = collapseTransition(t) || modificationDone;
		}

		return modificationDone;
	}

	private static boolean collapseTransition(AssignmentTransition t) {
		// check if source or target is 'frozen'
		if (!isFrozenInitOrEnd(t.getSource()) && !isFrozenInitOrEnd(t.getTarget()) && !t.isFrozen()) {
			Location source = t.getSource();
			Location target = t.getTarget();

			Preconditions.checkState(source.getOutgoing().size() == 1);
			Preconditions.checkState(target.getIncoming().size() == 1);

			// Move all outgoing transitions from 'target' to 'source'
			source.getOutgoing().addAll(target.getOutgoing());

			// Move all annotations of 'target' to 'source'
			source.getAnnotations().addAll(target.getAnnotations());
			Preconditions.checkState(target.getAnnotations().isEmpty());

			CfaUtils.delete(t);
			Preconditions.checkState(t.eContainer() == null);

			Preconditions.checkState(target.getIncoming().isEmpty(),
					"There should be no incoming transition in a location to be deleted.");
			Preconditions.checkState(target.getOutgoing().isEmpty(),
					"There should be no outgoing transition in a location to be deleted.");
			CfaUtils.delete(target);

			return true;
		}

		return false;
	}
}
