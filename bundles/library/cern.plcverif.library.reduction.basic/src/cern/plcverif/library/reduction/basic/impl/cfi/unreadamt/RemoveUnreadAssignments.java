/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic.impl.cfi.unreadamt;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfabase.Transition;
import cern.plcverif.base.models.cfa.cfainstance.AssignmentTransition;
import cern.plcverif.base.models.cfa.cfainstance.AutomatonInstance;
import cern.plcverif.base.models.cfa.cfainstance.CallTransition;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.cfa.cfainstance.Variable;
import cern.plcverif.base.models.cfa.cfainstance.VariableAssignment;
import cern.plcverif.base.models.cfa.cfainstance.VariableRef;
import cern.plcverif.base.models.cfa.textual.CfaToString;
import cern.plcverif.base.models.cfa.utils.CfaCollectionDeleter;
import cern.plcverif.library.reduction.basic.IInstanceReduction;
import cern.plcverif.library.reduction.basic.impl.AbstractLoggingReduction;

public class RemoveUnreadAssignments extends AbstractLoggingReduction implements IInstanceReduction {
	private boolean isModified = false;
	private JobResult result = null;
	private Set<Location> visitedLocations = null;
	
	private Queue<Task> tasks = new LinkedList<>();
	private CfaCollectionDeleter deleter = null;

	private static class Task {
		private Location loc;
		private UnreadVariables unreadVars;
		
		Task(Location loc, UnreadVariables unreadVars) {
			this.loc = loc;
			this.unreadVars = unreadVars;
		}
		
		public Location getLocation() {
			return loc;
		}
		
		public UnreadVariables getUnreadVars() {
			return unreadVars;
		}
	}
	
	public final boolean reduce(CfaNetworkInstance cfa, JobResult result) {
		this.isModified = false;
		this.result = result;
		this.visitedLocations = new HashSet<>();

		for (AutomatonInstance automaton : cfa.getAutomata()) {
			reduce(automaton, result);
		}

		return isModified;
	}

	protected void reduce(AutomatonInstance automaton, JobResult result) {
		tasks.clear();
		deleter = new CfaCollectionDeleter();
		
		tasks.add(new Task(automaton.getInitialLocation(), UnreadVariables.createEmpty()));
		while (!tasks.isEmpty()) {
			Task current = tasks.poll();
			tryReduceLoc(current.getLocation(), current.getUnreadVars());			
		}
		
		deleter.deleteAll();
	}

	private void tryReduceLoc(Location loc, UnreadVariables unreadVars) {
		if (visitedLocations.contains(loc)) {
			return;
		}
		visitedLocations.add(loc);
		
		if (loc.getIncoming().size() > 1) {
			// Here we could merge the different incoming unreadVars...
			for (Transition outTrans : loc.getOutgoing()) {
				// TODO this is too conservative, we don't have to drop the contents
				tryReduce(outTrans, UnreadVariables.createEmpty());
			}
		} else {
			if (loc.getOutgoing().size() == 1) {
				// No need to copy 'unreadVars' if there is no divergence
				tryReduce(loc.getOutgoing().get(0), unreadVars);
			} else {
				// Simple implementation: for the moment we give up at branches
				for (Transition outTrans : loc.getOutgoing()) {
					tryReduce(outTrans, UnreadVariables.createEmpty());
				}
			}
		}
	}

	private void tryReduce(Transition t, UnreadVariables unreadVars) {
		if (t instanceof AssignmentTransition) {
			tryReduce((AssignmentTransition) t, unreadVars);
		} else if (t instanceof CallTransition) {
			tryReduce((CallTransition) t, unreadVars);
		} else {
			throw new UnsupportedOperationException("Unknown transition type: " + t);
		}
	}

	private void tryReduce(AssignmentTransition t, UnreadVariables unreadVars) {
		// notify unreadVars about reads on 't'
		for (VariableAssignment amt : t.getAssignments()) {
			// for the moment, arrays are skipped here
			for (VariableRef varRef : EmfHelper.getAllContentsOfType(amt.getRightValue(), VariableRef.class, true)) {
				unreadVars.variableRead(varRef.getVariable());
			}
		}

		// Check which variables are written. If the previous assignment was not
		// read, it can be removed.
		for (VariableAssignment amt : t.getAssignments()) {
			if (amt.getLeftValue() instanceof VariableRef) {
				Variable writtenVar = ((VariableRef) amt.getLeftValue()).getVariable();

				if (unreadVars.hasUnreadWrite(writtenVar)) {
					VariableAssignment unreadAmt = unreadVars.getLastUnreadAssignment(writtenVar);
					if (!unreadAmt.isFrozen()) {
						isModified = true;
						if (this.isFineLoggingEnabled()) {
							result.currentStage()
								.logDebug(String.format("RemoveUnreadAssignments: The assignment '%s' has been removed as it is never read.",
										CfaToString.INSTANCE.toString(unreadAmt)));
						}
						deleter.registerForDeletion(unreadAmt);
					}
				}

				unreadVars.variableWritten(writtenVar, amt);
			}
		}

		if (visitedLocations.contains(t.getTarget()) == false) {
			tasks.add(new Task(t.getTarget(), unreadVars));
		}
	}

	private void tryReduce(CallTransition t, UnreadVariables unreadVars) {
		// for simplicity, drop all
		tasks.add(new Task(t.getTarget(), UnreadVariables.createEmpty()));
	}

}
