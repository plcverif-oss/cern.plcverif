/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic.impl.cfi.coi;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfabase.Transition;
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariable;
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariableRef;
import cern.plcverif.base.models.cfa.cfainstance.AssignmentTransition;
import cern.plcverif.base.models.cfa.cfainstance.AutomatonInstance;
import cern.plcverif.base.models.cfa.cfainstance.CallTransition;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.cfa.cfainstance.VariableAssignment;

public final class VariableDependencies {
	private final CfaNetworkInstance cfaNetwork;
	private final Set<VariableDependency> varDeps;
	private Map<Location, Collection<AbstractVariable>> affectingGuardVars = new HashMap<>();
	private Map<AbstractVariable, Collection<VariableDependency>> dependenciesOfVar = new HashMap<>();
	private final UnconditionalLocations unconditionalLocations;

	private Stack<Task> tasks = null;

	private static class Task {
		private Location location;
		private Collection<AbstractVariable> newVars;

		Task(Location location, Collection<AbstractVariable> newVars) {
			this.location = location;
			this.newVars = newVars;
		}

		public Location getLocation() {
			return location;
		}

		public Collection<AbstractVariable> getNewVars() {
			return newVars;
		}
	}

	private VariableDependencies(CfaNetworkInstance cfaNetwork) {
		this.cfaNetwork = cfaNetwork;
		this.varDeps = new HashSet<>();
		this.unconditionalLocations = UnconditionalLocations.compute(cfaNetwork);
	}

	public static VariableDependencies collect(CfaNetworkInstance cfaNetwork) {
		VariableDependencies ret = new VariableDependencies(cfaNetwork);
		ret.collectDependencies();
		return ret;
	}

	private Collection<AbstractVariable> getAffectingGuardVars(Location loc) {
		if (affectingGuardVars.containsKey(loc)) {
			return Collections.unmodifiableCollection(affectingGuardVars.get(loc));
		} else {
			return Collections.emptySet();
		}
	}

	private void collectDependencies() {
		// Explore affecting guard variables in all automata
		for (AutomatonInstance automaton : cfaNetwork.getAutomata()) {
			tasks = new Stack<>();
			tasks.push(new Task(automaton.getInitialLocation(), new ArrayList<AbstractVariable>()));
			while (!tasks.isEmpty()) {
				Task currentTask = tasks.pop();
				newAffectingGuardVarsForLocation(currentTask.getLocation(), currentTask.getNewVars());
			}
			tasks = null;
		}

		// Collect guard and assignment dependencies
		for (AutomatonInstance automaton : cfaNetwork.getAutomata()) {
			for (Transition t : automaton.getTransitions()) {
				Set<AbstractVariable> guardVars = new HashSet<>(getAffectingGuardVars(t.getSource()));
				guardVars.addAll(varsInGuard(t));
				if (t instanceof AssignmentTransition) {
					addDependencies(VariableDependency.explore(((AssignmentTransition) t).getAssignments(), guardVars));
				} else if (t instanceof CallTransition) {
					addDependencies(VariableDependency
							.explore(EmfHelper.getAllContentsOfType(t, VariableAssignment.class, false), guardVars));
				} else {
					throw new UnsupportedOperationException("Unknown transition type: " + t);
				}
			}
		}
	}

	private void newAffectingGuardVarsForLocation(Location location, Collection<AbstractVariable> newVars) {
		Preconditions.checkNotNull(newVars);

		boolean alreadyVisited = affectingGuardVars.containsKey(location);
		if (!alreadyVisited) {
			affectingGuardVars.put(location, new HashSet<>());
		}

		if (unconditionalLocations.isPresentOnEveryComputationPath(location)) {
			// an unconditional location cannot have any guard dependency
			newVars.clear();
		} else {
			// add newly explored vars to guard dependencies and propagate what
			// have to be propagated
			Collection<AbstractVariable> alreadyExplored = affectingGuardVars.get(location);
			newVars.removeAll(alreadyExplored);
			alreadyExplored.addAll(newVars);
		}

		if (!newVars.isEmpty() || !alreadyVisited) {
			// there is something to be propagated
			for (Transition outTrans : location.getOutgoing()) {
				newAffectingGuardVars(outTrans, new ArrayList<>(newVars));
			}
		}
	}

	private void newAffectingGuardVars(Transition transition, Collection<AbstractVariable> newVars) {
		newVars.addAll(varsInGuard(transition));
		tasks.add(new Task(transition.getTarget(), newVars));
	}

	private static Collection<AbstractVariable> varsInGuard(Transition transition) {
		final Set<AbstractVariable> varsInGuard = new HashSet<>();
		EmfHelper.getAllContentsOfTypeStream(transition.getCondition(), AbstractVariableRef.class, true)
				.forEach(guardVarRef -> varsInGuard.add(guardVarRef.getVariable()));
		return varsInGuard;
	}

	private void addDependencies(Collection<VariableDependency> varDeps) {
		for (VariableDependency varDep : varDeps) {
			addDepencendy(varDep);
		}
	}

	private void addDepencendy(VariableDependency varDep) {
		if (!varDeps.contains(varDep)) {
			varDeps.add(varDep);

			if (dependenciesOfVar.containsKey(varDep.getDependent()) == false) {
				dependenciesOfVar.put(varDep.getDependent(), new ArrayList<>());
			}
			dependenciesOfVar.get(varDep.getDependent()).add(varDep);
		}
	}

	public Set<AbstractVariable> getPrerequisitesOf(AbstractVariable dependentVar) {
		if (!dependenciesOfVar.containsKey(dependentVar)) {
			return Collections.emptySet();
		}

		return dependenciesOfVar.get(dependentVar).stream().map(it -> it.getPrerequisite()).collect(Collectors.toSet());
	}

	public Collection<VariableDependency> getAllDependencies() {
		return Collections.unmodifiableSet(this.varDeps);
	}
}
