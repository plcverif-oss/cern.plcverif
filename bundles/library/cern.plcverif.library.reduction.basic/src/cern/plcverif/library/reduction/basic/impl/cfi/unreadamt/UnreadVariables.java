/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic.impl.cfi.unreadamt;

import java.util.Map;
import java.util.Map.Entry;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EObjectMap;
import cern.plcverif.base.models.cfa.cfainstance.Variable;
import cern.plcverif.base.models.cfa.cfainstance.VariableAssignment;

public final class UnreadVariables {
	private Map<Variable, VariableAssignment> unreadAssignments = null;
	
	private UnreadVariables() {
	}

	private UnreadVariables(UnreadVariables tableToCopy) {
		this();
		createMapIfNotExists();
		for (Entry<Variable, VariableAssignment> item : tableToCopy.unreadAssignments.entrySet()) {
			unreadAssignments.put(item.getKey(), item.getValue());
		}
	}

	private void createMapIfNotExists() {
		if (this.unreadAssignments == null) {
			this.unreadAssignments = new EObjectMap<>();
		}
	}

	public static UnreadVariables createEmpty() {
		return new UnreadVariables();
	}

	public UnreadVariables createCopy() {
		return new UnreadVariables(this);
	}
	
	public void variableWritten(Variable v, VariableAssignment writingAssignment) {
		createMapIfNotExists();
		unreadAssignments.put(v, writingAssignment);
	}
	
	public void variableRead(Variable v) {
		if (unreadAssignments != null) {
			unreadAssignments.remove(v);
		}
	}
	
	public boolean hasUnreadWrite(Variable v) {
		if (unreadAssignments != null) {
			return unreadAssignments.containsKey(v);
		} else {
			return false;
		}
	}
	
	public VariableAssignment getLastUnreadAssignment(Variable v) {
		Preconditions.checkArgument(hasUnreadWrite(v));
		
		return unreadAssignments.get(v);
	}
}
