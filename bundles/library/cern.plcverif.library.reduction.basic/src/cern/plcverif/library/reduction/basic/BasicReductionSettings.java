/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic;

import cern.plcverif.base.common.settings.specific.AbstractSpecificSettings;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsElement;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory;

public class BasicReductionSettings extends AbstractSpecificSettings {
	
	@PlcverifSettingsElement(name = "ExpressionPropagation_maxlocations",
			description = "The number of locations in the largest supported automaton instances for the ExpressionPropagation reduction.",
			mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private int exprPropagationMaxLocations = 50_000; 
	
	@PlcverifSettingsElement(name = "ExpressionPropagation_maxage",
			description = "The number of transitions through which the expressions will be propagated.",
			mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private int exprPropagationMaxAge = 50; 
	
	@PlcverifSettingsElement(name = "ExpressionPropagation_maxexprsize",
			description = "The maximum size of expression tree that can be created by expression propagation.",
			mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private int exprPropagationMaxExprSize = 100; 
	
	@PlcverifSettingsElement(name = "show_progress",
			description = "If set, it shows the progress of reductions (each reduction in each iteration) on the console.",
			mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private boolean showProgress = false; 
	
	@PlcverifSettingsElement(name = "fine_logging",
			description = "Enables the fine-grained logging of reduction applications. It may cause significant performance penalty.",
			mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private boolean fineLogging = false; 
	
	@PlcverifSettingsElement(name = "print_vardep_graph",
			description = "Enables the diagnostic output of variable dependency graphs used for the cone of influence reduction.",
			mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private boolean printVariableDependencies = false;
	
	public int getExprPropagationMaxLocations() {
		return exprPropagationMaxLocations;
	}
	
	public int getExprPropagationMaxAge() {
		return exprPropagationMaxAge;
	}
	
	public int getExprPropagationMaxExprSize() {
		return exprPropagationMaxExprSize;
	}
	
	public boolean isShowProgress() {
		return showProgress;
	}
	
	public boolean isFineLogging() {
		return fineLogging;
	}
	
	public boolean isPrintVariableDependencies() {
		return printVariableDependencies;
	}
}
