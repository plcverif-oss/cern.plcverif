/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic.impl.cfi;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EObjectMap;
import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.common.logging.IPlcverifLogger;
import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariable;
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariableRef;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.cfa.cfainstance.VariableAssignment;
import cern.plcverif.base.models.cfa.textual.CfaToString;
import cern.plcverif.base.models.cfa.utils.CfaCollectionDeleter;
import cern.plcverif.library.reduction.basic.IInstanceReduction;
import cern.plcverif.library.reduction.basic.impl.AbstractLoggingReduction;

/**
 * Reduction that removes the unread variables of the given CFA.
 * 
 * A variable is considered to be unread if it is never read (and not frozen).
 * If the variable is assigned and none of its assignments are frozen, the
 * assignments are also removed.
 * 
 * It is assumed that the variables in the requirement are frozen, thus they
 * cannot be removed.
 * 
 */
public class RemoveUnreadVariables extends AbstractLoggingReduction implements IInstanceReduction {
	private static class ReadsAndWrites {
		private AbstractVariable variable;
		private int reads = 0;
		private List<VariableAssignment> assignments = new ArrayList<>();
		// PERF We don't need all variable assignments; it is only used if there
		// is exactly 1 assignment to 'variable'.

		public ReadsAndWrites(AbstractVariable variable) {
			this.variable = variable;
		}

		public void addRead() {
			reads++;
		}

		public void addAssignment(VariableAssignment assignment) {
			Preconditions.checkArgument(assignment.getLeftValue().getVariable() == variable);
			assignments.add(assignment);
		}

		public boolean canVariableBeDeleted() {
			if (reads == 0) {
				if (assignments.isEmpty()) {
					// Never read, never assigned
					return !variable.isFrozen();
				} else if (assignments.size() == 1) {
					// Written once, never read
					// (for optimization, as this is the typical case, otherwise
					// the next case would cover this as well)
					return !assignments.get(0).isFrozen() && !variable.isFrozen();
				} else {
					// Written more than once, never assigned
					return assignments.stream().noneMatch(it -> it.isFrozen()) && !variable.isFrozen();
				}
			} else {
				// Variable cannot be deleted if there are reads.
				return false;
			}
		}

		/**
		 * Registers the variable (and its eventual assignments) for deletion if
		 * it is permitted (i.e., {@link #canVariableBeDeleted()}==true).
		 * 
		 * @param logger
		 *            Log to be used. Can be null, then logging is disabled.
		 */
		public void registerVarForDeletionIfPermitted(IPlcverifLogger logger, CfaCollectionDeleter deleter) {
			if (canVariableBeDeleted()) {
				for (VariableAssignment amt : assignments) {
					if (logger != null) {
						logger.logDebug(
								"RemoveUnreadVariables: assignment '%s' has been removed as the assigned variable is not used.",
								CfaToString.INSTANCE.toString(amt));
					}
				}
				deleter.registerForDeletion(assignments);

				if (logger != null) {
					logger.logDebug("RemoveUnreadVariables: variable '%s' has been removed as it is not used.",
							CfaToString.INSTANCE.toString(variable));
				}
				deleter.registerForDeletion(variable);
			} else {
				// nothing to do
			}
		}
	}

	private Map<AbstractVariable, ReadsAndWrites> readsAndWrites = new EObjectMap<AbstractVariable, ReadsAndWrites>();

	@Override
	public boolean reduce(CfaNetworkInstance cfa, JobResult result) {
		boolean change = false;
		// Collect all writes.
		Set<AbstractVariableRef> assignmentLeftValues = new HashSet<>();
		for (VariableAssignment amt : EmfHelper.getAllContentsOfTypeIterable(cfa, VariableAssignment.class, false)) {
			AbstractVariable assignedVariable = amt.getLeftValue().getVariable();
			assignmentLeftValues.add(amt.getLeftValue());
			getReadsWrites(assignedVariable).addAssignment(amt);
		}

		// Collect all reads. (This can not only be the RHS of an assignment,
		// but also a guard, annotation, etc.)
		// We assume that the variables read in the requirement are frozen, thus
		// they cannot be deleted here.
		for (AbstractVariableRef varRef : EmfHelper.getAllContentsOfTypeIterable(cfa, AbstractVariableRef.class,
				false)) {
			if (!assignmentLeftValues.contains(varRef)) {
				// It is not an assigned variable, it's really a variable read
				getReadsWrites(varRef.getVariable()).addRead();
			}
		}

		// Delete all variables which are not used.
		CfaCollectionDeleter deleter = new CfaCollectionDeleter();
		for (AbstractVariable v : new ArrayList<>(cfa.getVariables())) {
			getReadsWrites(v).registerVarForDeletionIfPermitted(result.currentStage(), deleter);
		}
		change = !(deleter.isEmpty());
		deleter.deleteAll();
		return change;
	}

	/**
	 * Returns the reads-and-writes descriptor for the given variable. An empty
	 * descriptor is created if it does not exist.
	 */
	private ReadsAndWrites getReadsWrites(AbstractVariable v) {
		if (!readsAndWrites.containsKey(v)) {
			readsAndWrites.put(v, new ReadsAndWrites(v));
		}
		ReadsAndWrites rawForVariable = readsAndWrites.get(v);
		Preconditions.checkState(rawForVariable != null);
		return rawForVariable;
	}
}
