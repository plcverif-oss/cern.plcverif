/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic.impl.cfi;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.base.Preconditions;

import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfabase.Transition;
import cern.plcverif.base.models.cfa.cfainstance.AssignmentTransition;
import cern.plcverif.base.models.cfa.cfainstance.AutomatonInstance;
import cern.plcverif.base.models.cfa.utils.CfaCollectionDeleter;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.ExpressionSafeFactory;

/**
 * Reduction to push guard conditions to successor transitions, merging
 * consecutive branches in the CFA.
 */
public class ConditionPushDown extends AbstractAutomatonReduction {

	@Override
	protected boolean reduce(AutomatonInstance automaton, JobResult result) {
		boolean modificationDone = false;

		List<AssignmentTransition> transitionsWithoutAmt = automaton.getTransitions().stream()
				.filter(it -> it instanceof AssignmentTransition).map(it -> (AssignmentTransition) it)
				.filter(it -> it.getAssignments().isEmpty()).collect(Collectors.toList());

		CfaCollectionDeleter deleter = new CfaCollectionDeleter();

		for (AssignmentTransition t : transitionsWithoutAmt) {
			modificationDone = tryToReduce(t, result, deleter) || modificationDone;
		}

		deleter.deleteAll();

		return modificationDone;
	}

	private boolean tryToReduce(AssignmentTransition t, JobResult result, CfaCollectionDeleter deleter) {
		// Check if it is applicable
		Preconditions.checkState(t.getAssignments().isEmpty());

		Expression c = t.getCondition();
		Location loc1 = t.getSource();
		Location loc2 = t.getTarget();

		if (loc1.isFrozen() || loc2.isFrozen() || t.isFrozen()) {
			return false;
		}

		Preconditions.checkState(loc1.getOutgoing().contains(t));

		if (loc2.getIncoming().size() != 1 || loc2.getOutgoing().size() < 2) {
			return false;
		}
		Preconditions.checkState(loc2.getIncoming().size() == 1);
		Preconditions.checkState(loc2.getIncoming().get(0) == t);

		// It is applicable, perform the reduction
		for (Transition out : new ArrayList<>(loc2.getOutgoing())) {
			out.setSource(loc1);

			Expression oldCondition = out.getCondition();
			out.setCondition(null);
			Expression newCondition = ExpressionSafeFactory.INSTANCE.and(EcoreUtil.copy(c), oldCondition);
			out.setCondition(newCondition);
		}

		if (this.isFineLoggingEnabled()) {
			result.currentStage()
					.logDebug(String.format("'ConditionPushDown' has removed transition %s and location %s.",
							t.getDisplayName(), loc2.getDisplayName()));
		}

		deleter.registerForDeletion(t);
		deleter.registerForDeletion(loc2);
		return true;
	}

}
