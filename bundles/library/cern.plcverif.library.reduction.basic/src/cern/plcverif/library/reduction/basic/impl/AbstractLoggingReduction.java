/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic.impl;

public abstract class AbstractLoggingReduction {
	private boolean fineLogging = false;
	
	/**
	 * Sets whether fine-grained logging is enabled, i.e., whether each reduction step shall be logged.
	 * If false, even the log messages should not be generated.
	 * @param enabled True, if fine-grained logging is enabled.
	 */
	public void setFineLogging(boolean enabled) {
		this.fineLogging = enabled;
	}
	
	/**
	 * Returns whether fine-grained logging is enabled, i.e., whether each reduction step shall be logged.
	 * If false, even the log messages should not be generated.
	 * @return True, if fine-grained logging is enabled.
	 */
	public boolean isFineLoggingEnabled() {
		return fineLogging;
	}
}
