/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic.impl.cfi;

import static cern.plcverif.base.models.cfa.utils.CfaUtils.isFrozenInitOrEnd;
import static org.eclipse.emf.ecore.util.EcoreUtil.copy;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.base.Optional;
import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.common.logging.IPlcverifLogger;
import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.models.cfa.CfaInstanceSafeFactory;
import cern.plcverif.base.models.cfa.cfabase.ElseExpression;
import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariableRef;
import cern.plcverif.base.models.cfa.cfainstance.AssignmentTransition;
import cern.plcverif.base.models.cfa.cfainstance.AutomatonInstance;
import cern.plcverif.base.models.cfa.cfainstance.VariableAssignment;
import cern.plcverif.base.models.cfa.textual.CfaToString;
import cern.plcverif.base.models.cfa.utils.CfaInstanceUtils;
import cern.plcverif.base.models.cfa.utils.CfaUtils;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.utils.TypeUtils;
/**
 * Reduction to translate trivial conditional branches assigning Boolean
 * variables into (more complex) variable assignments by including the guard
 * directly in the assignment.
 */
public class SimplifyTrivialConditionalBranches extends AbstractAutomatonReduction {
	private static final CfaInstanceSafeFactory FACTORY = CfaInstanceSafeFactory.INSTANCE;

	@Override
	protected boolean reduce(AutomatonInstance automaton, JobResult result) {
		boolean changed = false;

		// Find all locations which are the head of simple branches, i.e. with
		// two outgoing transitions having guards [G] and [NOT G]
		// @formatter:off
		List<Location> simpleBranches = automaton.getLocations().stream()
				.filter(it -> !isFrozenInitOrEnd(it) && it.getOutgoing().size() == 2
						&& it.getOutgoing().get(0) instanceof AssignmentTransition
						&& it.getOutgoing().get(1) instanceof AssignmentTransition && CfaUtils.isNegatedOf(
								it.getOutgoing().get(0).getCondition(), it.getOutgoing().get(1).getCondition()))
				.collect(Collectors.toList());
		// @formatter:on

		for (Location loc1 : simpleBranches) {
			AssignmentTransition tA = (AssignmentTransition) loc1.getOutgoing().get(0);
			AssignmentTransition tB = (AssignmentTransition) loc1.getOutgoing().get(1);

			for (AbstractVariableRef varCandidate : CfaInstanceUtils.getAssignedByAny(tA, tB)) {
				if (TypeUtils.hasBoolType(varCandidate)) {
					// 'varCandidate' is a Boolean variable assigned in both
					// branches, thus can be extracted

					// The assignment of variable 'varCandidate' cannot be
					// extracted, if
					// if alters the satisfaction of the guard, i.e. if the
					// guard refers
					// to the 'varCandidate'.
					Stream<AbstractVariableRef> varRefsInGuard = getVarRefsInGuard(tA, tB);
					boolean inConflict = varRefsInGuard
							.anyMatch(it -> CfaUtils.conflictingReferences(it, varCandidate));
					if (inConflict) {
						// This candidate is in conflict; step to a next one
						continue;
					}

					Optional<VariableAssignment> amt1 = CfaInstanceUtils.getAssignmentOf(varCandidate,
							tA.getAssignments());
					Optional<VariableAssignment> amt2 = CfaInstanceUtils.getAssignmentOf(varCandidate,
							tB.getAssignments());

					if ((amt1.isPresent() && amt1.get().isFrozen()) || (amt2.isPresent() && amt2.get().isFrozen())) {
						continue;
					}

					extract(varCandidate, loc1, amt1, amt2, tA.getCondition(), tB.getCondition(),
							result.currentStage());
					changed = true;
				}
			}
		}

		return changed;
	}

	/**
	 * Returns the variables present in the guard of any of the given
	 * transitions.
	 * 
	 * @param tA
	 *            Transition 1
	 * @param tB
	 *            Transition 2
	 * @return Variables present in the guard of any of the given transitions
	 */
	private static Stream<AbstractVariableRef> getVarRefsInGuard(AssignmentTransition tA, AssignmentTransition tB) {
		return Stream
				.concat(EmfHelper.getAllContentsOfTypeStream(tA.getCondition(), AbstractVariableRef.class, true),
						EmfHelper.getAllContentsOfTypeStream(tB.getCondition(), AbstractVariableRef.class, true))
				.distinct();
	}

	private void extract(AbstractVariableRef boolVar, Location loc, Optional<VariableAssignment> amt1,
			Optional<VariableAssignment> amt2, Expression guard1, Expression guard2, IPlcverifLogger log) {
		Preconditions.checkNotNull(loc);
		Preconditions.checkArgument(!amt1.isPresent() || amt1.get().eContainer() == loc.getOutgoing().get(0)
				|| amt1.get().eContainer() == loc.getOutgoing().get(1));
		Preconditions.checkArgument(!amt2.isPresent() || amt2.get().eContainer() == loc.getOutgoing().get(0)
				|| amt2.get().eContainer() == loc.getOutgoing().get(1));
		Preconditions.checkArgument(!amt1.isPresent() || EcoreUtil.equals(amt1.get().getLeftValue(), boolVar));
		Preconditions.checkArgument(!amt2.isPresent() || EcoreUtil.equals(amt2.get().getLeftValue(), boolVar));
		Preconditions.checkArgument(TypeUtils.hasBoolType(boolVar));
		Preconditions.checkArgument(amt1.isPresent() || amt2.isPresent());

		// Create a new location before 'loc'
		Location newLoc = FACTORY.createLocation("x", loc.getParentAutomaton());

		// Move all incoming transitions of 'loc' to 'newLoc'
		newLoc.getIncoming().addAll(loc.getIncoming());

		// Create a new assignment transition newLoc->loc
		AssignmentTransition newTrans = FACTORY.createAssignmentTransition("x",
				(AutomatonInstance) loc.getParentAutomaton(), newLoc, loc, FACTORY.trueLiteral());

		// Copy guards and eliminate else if needed
		Expression guard1Copy = copyAndResolveElse(guard1, guard2);
		Expression guard2Copy = copyAndResolveElse(guard2, guard1);

		// Create a new assignment representing the previous two assignments of
		// 'boolVar'
		Expression rightValue = FACTORY.or(
				FACTORY.and(guard1Copy, amt1.isPresent() ? copy(amt1.get().getRightValue()) : copy(boolVar)),
				FACTORY.and(guard2Copy, amt2.isPresent() ? copy(amt2.get().getRightValue()) : copy(boolVar)));
		VariableAssignment newAssignment = FACTORY.createAssignment(copy(boolVar), rightValue);
		newTrans.getAssignments().add(newAssignment);

		if (this.isFineLoggingEnabled()) {
			log.logDebug("SimplifyTrivialConditionalBranches: [%s] '%s' and [%s] '%s' has been simplified to '%s'.",
					CfaToString.toDiagString(guard1),
					amt1.isPresent() ? CfaToString.toDiagString(amt1.get()) : "no assignment",

					CfaToString.toDiagString(guard2),
					amt2.isPresent() ? CfaToString.toDiagString(amt2.get()) : "no assignment",

					CfaToString.toDiagString(newAssignment));
		}

		// Remove old assignments
		if (amt1.isPresent()) {
			CfaUtils.delete(amt1.get());
		}
		if (amt2.isPresent()) {
			CfaUtils.delete(amt2.get());
		}
	}

	/**
	 * Copies the given 'condition'. If the 'condition' is an ELSE, then the
	 * result is the copy of the negation of the given 'otherCondition'.
	 * 
	 * @param condition
	 *            Expression to copy.
	 * @param otherCondition
	 *            Expression used to resolve potential ELSE expressions.
	 */
	private static Expression copyAndResolveElse(Expression condition, Expression otherCondition) {
		if (condition instanceof ElseExpression) {
			return FACTORY.neg(copy(otherCondition));
		}
		return copy(condition);
	}

}
