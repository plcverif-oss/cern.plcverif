/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic.impl.cfi.coi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.common.base.Objects;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariable;
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariableRef;
import cern.plcverif.base.models.cfa.cfainstance.VariableAssignment;

/**
 * Class to represent a dependency between two CFA variables.
 * 
 * If variable <i>'a'</i> depends on the value of variable <i>'b'</i> (e.g.
 * {@code a := NOT b;}), then <i>'a'</i> will be a <i>dependent variable</i> and
 * it <i>depends on</i> <i>'b'</i> (or <i>'b'</i> is a prerequisite).
 * <p>
 * Two types of dependencies can be described:
 * <ul>
 * <li>There is an <i>assignment dependency</i> between the variables {@code a}
 * and {@code b} if {@code a} has an assignment which refers to the value of
 * {@code b} (e.g., {@code a := 2 * b;}). In this case {@code a} is the
 * dependent variable, which depends on {@code b}. The variable {@code b} is
 * also called as prerequisite.</li>
 * <li>There is a <i>control dependency</i> between the variables {@code a} and
 * {@code b} if {@code a} has an assignment which is conditional, depending on
 * the value of {@code b}. For example, if there is an assignment
 * {@code a := c + d;} on a transition which includes {@code b} in its guard.
 * Note that a variable {@code v} can have control dependency on variables which
 * are not present in the guard of the transition assigning {@code v}.</li>
 * </ul>
 * <p>
 * Immutable.
 */
public class VariableDependency {
	public enum DepencencyType {
		CONTROL, ASSIGNMENT
	}

	private static final int NO_HASH_COMPUTED = -1;

	private final AbstractVariable dependentVar;
	private final AbstractVariable dependsOnVar; // prerequisite
	private final DepencencyType dependencyType;
	private int hash = NO_HASH_COMPUTED;

	public VariableDependency(AbstractVariable dependentVar, AbstractVariable dependsOnVar, DepencencyType type) {
		this.dependentVar = dependentVar;
		this.dependsOnVar = dependsOnVar;
		this.dependencyType = type;
	}

	public AbstractVariable getDependent() {
		return dependentVar;
	}

	public AbstractVariable getPrerequisite() {
		return dependsOnVar;
	}

	public DepencencyType getDependencyType() {
		return dependencyType;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj == null) {
			return false;
		}

		if (this.getClass() == obj.getClass()) {
			VariableDependency other = (VariableDependency) obj;
			return equalsCustom(other);
		}
		return false;
	}

	public boolean equalsCustom(VariableDependency other) {
		return other.dependentVar == this.dependentVar && other.dependsOnVar == this.dependsOnVar
				&& other.dependencyType == this.dependencyType;
	}

	@Override
	public int hashCode() {
		if (this.hash == NO_HASH_COMPUTED) {
			this.hash = Objects.hashCode(dependentVar, dependsOnVar, dependencyType);
		}
		return this.hash;
	}

	public static List<VariableDependency> explore(VariableAssignment assignment) {
		return explore(Arrays.asList(assignment));
	}

	public static List<VariableDependency> explore(Collection<VariableAssignment> assignments) {
		return explore(assignments, new ArrayList<>());
	}

	public static List<VariableDependency> explore(Collection<VariableAssignment> assignments,
			Collection<AbstractVariable> affectingGuardVars) {
		List<VariableDependency> ret = new ArrayList<>();

		for (VariableAssignment amt : assignments) {
			AbstractVariable assignedVar = amt.getLeftValue().getVariable();

			Set<AbstractVariable> readVars = new HashSet<>();
			readVars.add(assignedVar);
			for (AbstractVariableRef ref : EmfHelper.getAllContentsOfType(amt.getRightValue(),
					AbstractVariableRef.class, true)) {
				if (!readVars.contains(ref.getVariable())) {
					readVars.add(ref.getVariable());
					ret.add(new VariableDependency(assignedVar, ref.getVariable(), DepencencyType.ASSIGNMENT));
				}
			}

			for (AbstractVariable guardVar : affectingGuardVars) {
				ret.add(new VariableDependency(assignedVar, guardVar, DepencencyType.CONTROL));
			}
		}
		return ret;
	}
}
