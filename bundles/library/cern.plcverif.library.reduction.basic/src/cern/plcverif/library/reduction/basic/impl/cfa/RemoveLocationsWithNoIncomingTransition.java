/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic.impl.cfa;

import static cern.plcverif.base.models.cfa.utils.CfaUtils.isFrozenInitOrEnd;

import java.util.List;
import java.util.stream.Collectors;

import com.google.common.base.Preconditions;

import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.models.cfa.cfabase.AutomatonBase;
import cern.plcverif.base.models.cfa.cfabase.Location;
import cern.plcverif.base.models.cfa.cfabase.Transition;
import cern.plcverif.base.models.cfa.cfadeclaration.AutomatonDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfainstance.AutomatonInstance;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.cfa.utils.CfaCollectionDeleter;
import cern.plcverif.base.models.cfa.utils.CfaInstanceUtils;
import cern.plcverif.library.reduction.basic.IDeclarationReduction;
import cern.plcverif.library.reduction.basic.IInstanceReduction;
import cern.plcverif.library.reduction.basic.impl.AbstractLoggingReduction;

/**
 * Reduction to remove locations with no incoming transition (i.e., unreachable
 * transitions).
 */
public class RemoveLocationsWithNoIncomingTransition extends AbstractLoggingReduction
		implements IDeclarationReduction, IInstanceReduction {

	@Override
	public boolean reduce(CfaNetworkInstance cfa, JobResult result) {
		boolean ret = false;

		for (AutomatonInstance automaton : cfa.getAutomata()) {
			ret = reduce(automaton, result) || ret;
		}
		return ret;
	}

	@Override
	public boolean reduce(CfaNetworkDeclaration cfa, JobResult result) {
		boolean ret = false;

		for (AutomatonDeclaration automaton : cfa.getAutomata()) {
			ret = reduce(automaton, result) || ret;
		}
		return ret;
	}

	protected boolean reduce(AutomatonBase automaton, JobResult result) {
		//@formatter:off
		List<Location> locsToRemove = automaton.getLocations().stream()
				.filter(it -> it.getIncoming().isEmpty())
				.filter(it -> !isFrozenInitOrEnd(it))
				.filter(it -> !anyFrozen(it.getOutgoing()))
				.collect(Collectors.toList());
		//@formatter:on

		CfaCollectionDeleter deleter = new CfaCollectionDeleter();
		for (Location loc : locsToRemove) {
			// remove its outgoing transitions first
			deleter.registerForDeletion(loc.getOutgoing());

			if (isFineLoggingEnabled()) {
				for (Transition t : loc.getOutgoing()) {
					result.currentStage().logDebug(
							"RemoveLocationsWithNoIncomingTransition: transition '%s' is removed as its source location will be removed.",
							t.getName());
				}
			}
		}
		deleter.deleteAll();

		for (Location loc : locsToRemove) {
			if (isFineLoggingEnabled()) {
				result.currentStage().logDebug(
						"RemoveLocationsWithNoIncomingTransition: location '%s' is removed as it has no incoming transition.",
						loc.getName());
			}

			Preconditions.checkState(loc.getIncoming().isEmpty());
			Preconditions.checkState(loc.getOutgoing().isEmpty());
			CfaInstanceUtils.deleteLocationNoAnnotation(loc);
		}

		return !locsToRemove.isEmpty();
	}

	private static boolean anyFrozen(List<Transition> transitionList) {
		return transitionList.stream().anyMatch(it -> it.isFrozen());
	}
}
