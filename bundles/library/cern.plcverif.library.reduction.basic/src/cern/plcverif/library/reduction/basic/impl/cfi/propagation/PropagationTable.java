/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic.impl.cfi.propagation;

import static cern.plcverif.base.models.cfa.utils.CfaUtils.conflictingReferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.ecore.util.EcoreUtil;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.emf.EObjectMap;
import cern.plcverif.base.common.emf.EObjectMap.IMapFactory;
import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.common.emf.EquatableEObject;
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariableRef;
import cern.plcverif.base.models.cfa.cfainstance.VariableAssignment;
import cern.plcverif.base.models.cfa.textual.CfaToString;
import cern.plcverif.base.models.cfa.utils.EquatableCfiVariableRef;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.Literal;
import cern.plcverif.base.models.expr.Nondeterministic;
import cern.plcverif.base.models.expr.utils.ExprUtils;
import cern.plcverif.library.reduction.basic.impl.cfi.ExpressionSimplification;

public final class PropagationTable {
	private final int maxAge;
	private final boolean constantPropagationOnly;

	private static class ExprAgePair {
		private Expression expr;
		private int age;

		public ExprAgePair(Expression expr, int age) {
			this.expr = expr;
			this.age = age;
		}

		public Expression getExpr() {
			return expr;
		}

		public int getAge() {
			return age;
		}
	}

	private EObjectMap<AbstractVariableRef, ExprAgePair> data = createDataMap();
	private static ExpressionSimplification simplifier = new ExpressionSimplification();

	private PropagationTable(int maxAge, boolean constantPropagationOnly) {
		this.maxAge = maxAge;
		this.constantPropagationOnly = constantPropagationOnly;
	}

	private static EObjectMap<AbstractVariableRef, ExprAgePair> createDataMap() {
		return new EObjectMap<AbstractVariableRef, ExprAgePair>(new IMapFactory<ExprAgePair>() {
			@Override
			public Map<EquatableEObject, ExprAgePair> createMap() {
				return new HashMap<EquatableEObject, ExprAgePair>();
			}
		}, objToWrap -> new EquatableCfiVariableRef((AbstractVariableRef)objToWrap));
	}

	private PropagationTable(PropagationTable tableToCopy) {
		this(tableToCopy.maxAge, tableToCopy.constantPropagationOnly);
		for (Entry<EquatableEObject, ExprAgePair> item : tableToCopy.data.wrappedEntrySet()) {
			// wrappedEntrySet and putWrapped used to avoid unnecessary
			// re-wrapping
			if (item.getValue().getAge() < this.maxAge) {
				// As copies are made both in 'addEntry' and 'valueFor',
				// there is no need for copying the expressions here.
				ExprAgePair value = new ExprAgePair(item.getValue().getExpr(), item.getValue().getAge() + 1);
				// No need to re-wrap the key
				data.putWrapped(item.getKey(), value);
			}
		}
	}

	public static PropagationTable createEmpty(ExpressionPropagationSettings settings) {
		return new PropagationTable(settings.getMaxAge(), settings.isConstantPropagationOnly());
	}

	public PropagationTable createCopy() {
		return new PropagationTable(this);
	}

	public boolean hasValueFor(AbstractVariableRef varRef) {
		return data.containsKey(varRef);
	}

	/**
	 * Returns the value stored for the given variable reference in the propagation
	 * table. It is expected that a value is available for the given reference, 
	 * otherwise an exception is thrown.
	 * <p>
	 * The returned value is guaranteed not to be contained yet (i.e. it is a
	 * copy that can be used directly).
	 * @see #hasValueFor(AbstractVariableRef)
	 * @return The expression stored for the given reference in the propagation table.
	 *         It is guaranteed that its container is null.
	 */
	public Expression valueFor(AbstractVariableRef varRef) {
		Preconditions.checkState(hasValueFor(varRef),
				"This propagation table does not contain a value for the given variable reference.");
		return ExprUtils.exprCopy(data.get(varRef).getExpr());
	}

	private int ageFor(AbstractVariableRef varRef) {
		Preconditions.checkState(hasValueFor(varRef),
				"This propagation table does not contain a value for the given variable reference.");
		return data.get(varRef).getAge();
	}

	/**
	 * Removes all entries which contain the given varRef, either as the left or
	 * the right value.
	 */
	public void removeContaining(AbstractVariableRef varRef) {
		List<AbstractVariableRef> toBeRemoved = new ArrayList<>();

		for (Entry<AbstractVariableRef, ExprAgePair> entry : data.entrySet()) {
			Expression rhsExpr = entry.getValue().getExpr();

			// Check conflict on left side
			if (conflictingReferences(entry.getKey(), varRef)) {
				toBeRemoved.add(entry.getKey());
			} else if (rhsExpr instanceof Literal) {
				// RHS is a literal, no conflict possible
				continue;
			} else if (rhsExpr instanceof AbstractVariableRef) {
				// If the RHS is a single variable reference, let's avoid the
				// iteration (performance optimization)
				if (conflictingReferences((AbstractVariableRef) rhsExpr, varRef)) {
					toBeRemoved.add(entry.getKey());
					continue;
				}
			} else {
				// Check conflict on right side
				// (We don't check the root element as it was checked in the
				// case above already.)
				for (AbstractVariableRef rhsRef : EmfHelper.getAllContentsOfType(rhsExpr, AbstractVariableRef.class,
						false)) {
					if (conflictingReferences(rhsRef, varRef)) {
						toBeRemoved.add(entry.getKey());
						continue;
					}
				}
			}
		}

		toBeRemoved.forEach(it -> data.remove(it));
	}

	public void updateWithAssignment(VariableAssignment amt) {
		AbstractVariableRef leftValue = amt.getLeftValue();
		Expression rightValue = amt.getRightValue();

		// Remove any previous entries
		removeContaining(leftValue);

		// Check if it can be propagated
		if (canBePropagated(amt)) {
			// Add, now we know that it is possible
			addEntry(leftValue, rightValue, 0);
		}
	}

	public void updateWithEquality(AbstractVariableRef leftValue, Expression rightValue) {
		removeContaining(leftValue);

		// Check if it can be propagated
		if (canBePropagated(leftValue, rightValue)) {
			// Add, now we know that it is possible
			addEntry(leftValue, rightValue, 0);
		}
	}

	private void addEntry(AbstractVariableRef leftValue, Expression rightValue, int age) {
		Expression copyOfRightValue = ExprUtils.exprCopy(rightValue);
		simplifier.trySimplifySubtree(copyOfRightValue);

		data.put(EcoreUtil.copy(leftValue), new ExprAgePair(copyOfRightValue, age));
	}

	private boolean canBePropagated(VariableAssignment amt) {
		if (constantPropagationOnly && !(amt.getRightValue() instanceof Literal)) {
			return false;
		}
		
		return !amt.isFrozen() && !amt.getLeftValue().getVariable().isFrozen()
				&& canBePropagated(amt.getLeftValue(), amt.getRightValue());
	}

	private static boolean canBePropagated(AbstractVariableRef leftValue, Expression rightValue) {
		// It CANNOT be propagated if:
		// - rightValue contains reference to leftValue,
		// - rightValue contains nondeterministic value

		if (EmfHelper.getAllContentsOfType(rightValue, Nondeterministic.class, true).isEmpty() == false) {
			return false;
		}

		for (AbstractVariableRef varRef : EmfHelper.getAllContentsOfType(rightValue, AbstractVariableRef.class, true)) {
			if (conflictingReferences(varRef, leftValue)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Returns the combination of the given propagation tables. The result will
	 * contain values only for those variables, which are present in all of the
	 * tables, with the same value.
	 * 
	 * E.g. {@code [a=1, b=2]} and {@code [a=1, b=3, c=4]} will be combined into
	 * {@code [a=1]}.
	 * 
	 * @param tables
	 *            Propagation tables to combine.
	 * @return The combined propagation table. This will be a new instance, the
	 *         tables from {@code tables} are not modified.
	 */
	public static PropagationTable combine(List<PropagationTable> tables, ExpressionPropagationSettings settings) {
		Preconditions.checkNotNull(tables, "tables");

		if (tables.isEmpty()) {
			return PropagationTable.createEmpty(settings);
		} else if (tables.size() == 1) {
			return tables.get(0).createCopy();
		} else {
			PropagationTable ret = PropagationTable.createEmpty(settings);
			PropagationTable firstTable = tables.get(0);
			outer: for (AbstractVariableRef varRef : firstTable.data.keySet()) {
				for (PropagationTable table : tables) {
					if (table.hasValueFor(varRef) == false) {
						// this variable cannot be part of the combined
						continue outer;
					}
				}
				
				Expression value = firstTable.valueFor(varRef);
				int newAge = 1;
				
				for (PropagationTable table : tables) {
					if (EcoreUtil.equals(value, table.valueFor(varRef)) == false) {
						// this variable cannot be part of the combined,
						// 'table' has a value for it different than 'firstTable'
						continue outer;
					}
					newAge = Math.min(newAge, table.ageFor(varRef) + 1);
				}

				// no show-stoppers, include 'varRef=value' in ret
				ret.addEntry(ExprUtils.exprCopy(varRef), ExprUtils.exprCopy(value), newAge);
			}

			return ret;
		}
	}

	/**
	 * String representation of all contents of the current propagation table.
	 * <p>
	 * May be expensive. For diagnostic purposes only.
	 */
	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder();
		for (Map.Entry<AbstractVariableRef, PropagationTable.ExprAgePair> entry : data.entrySet()) {
			ret.append(CfaToString.toDiagString(entry.getKey()));
			ret.append(" = ");
			ret.append(CfaToString.toDiagString(entry.getValue().getExpr()));
			ret.append(" (age: ");
			ret.append(entry.getValue().getAge());
			ret.append(")");
			ret.append(System.lineSeparator());
		}
		return ret.toString();
	}
}
