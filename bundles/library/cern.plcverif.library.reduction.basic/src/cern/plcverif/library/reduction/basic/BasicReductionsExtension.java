/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic;

import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.help.SettingsHelp;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.interfaces.IReduction;
import cern.plcverif.base.interfaces.IReductionExtension;

public class BasicReductionsExtension implements IReductionExtension {
	public static final String CMD_ID = "basic_reductions";

	@Override
	public IReduction createReduction() {
		return new BasicReductions();
	}

	@Override
	public IReduction createReduction(SettingsElement settings) throws SettingsParserException {
		return new BasicReductions(settings);
	}

	@Override
	public void fillSettingsHelp(SettingsHelp help) {
		help.addPluginDetails("basic_reductions", 
				"Basic reductions", 
				"Provides basic CFA reductions.",
				BasicReductionsExtension.class);
		SpecificSettingsSerializer.fillSettingsHelp(help, BasicReductionSettings.class);
	}
}
