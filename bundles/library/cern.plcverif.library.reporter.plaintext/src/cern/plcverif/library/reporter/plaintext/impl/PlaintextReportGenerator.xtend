/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.library.reporter.plaintext.impl

import cern.plcverif.base.common.settings.SettingsElement
import cern.plcverif.base.common.settings.SettingsSerializer
import cern.plcverif.base.interfaces.data.result.LogItem
import cern.plcverif.base.models.cfa.textual.CfaToString
import cern.plcverif.library.reporter.plaintext.PlaintextReporterSettings
import cern.plcverif.verif.interfaces.data.VerificationResult
import com.google.common.base.Preconditions
import java.util.Collections

import static extension cern.plcverif.base.models.cfa.textual.CfaToString.toDiagString
import cern.plcverif.base.common.formattedstring.MarkdownFormatter

final class PlaintextReportGenerator {
	VerificationResult result;
	PlaintextReporterSettings settings;
	
	private new(VerificationResult result, PlaintextReporterSettings settings) {
		this.result = result;
		this.settings = settings;
	}
	
	static def represent(VerificationResult result, PlaintextReporterSettings settings) {
		// Extensive checking
		Preconditions.checkNotNull(settings, "Plaintext reporter settings must not be null.");
		Preconditions.checkNotNull(result, "VerificationResult should not be null.");
		Preconditions.checkNotNull(result.jobMetadata, "result.jobMetadata should not be null.");
		Preconditions.checkNotNull(result.jobMetadata.name, "result.jobMetadata.name should not be null.");
		Preconditions.checkNotNull(result.result, "result.result should not be null.");
		
		if (result.declarationCounterexample.isPresent) {
			for (step : result.declarationCounterexample.get.getSteps) {
				Preconditions.checkNotNull(step, "result.declarationCounterexample.steps should not contain any null step.");
				Preconditions.checkNotNull(step.name, "result.declarationCounterexample.steps should not contain any null-named step.");
			}
		}
		Preconditions.checkNotNull(result.getAllStages, "result.stageLogs should not be null.");
		for (stage : result.getAllStages) {
			Preconditions.checkNotNull(stage, "result.stageLogs should not contain any null stage.");
			Preconditions.checkNotNull(stage.getStageName, "stage.stageName is null");
		}
		
		// Do it!
		new PlaintextReportGenerator(result, settings).representInternal();
	}
	
	private def representInternal() '''
		«representHeader»
		
		«representOverview»
		
		
		«representCexIfPresent»
		
		«representStagesAndLog»
		
		«representSettings»
		
		«representConsoleOutput»
	'''
	
	private def representHeader() '''
		«result.jobMetadata.name»
		«String.join("", Collections.nCopies(result.jobMetadata.name.length, "="))»
	'''
	
	private def representOverview() '''
		«IF result.verifProblem.isPresent»
			«IF result.verifProblem.get.requirementDescription !== null»
			Requirement: «result.verifProblem.get.requirementDescription?.format(new MarkdownFormatter())»
			             «CfaToString.INSTANCE.toString(result.verifProblem.get.requirement)»
			«ENDIF»
		«ELSE»
			** THE VERIFICATION PROBLEM IS NOT AVAILABLE. **
		«ENDIF»
		
		**Result: the requirement is «result.result.toString.toUpperCase»**
		
		Backend: «if (result.backendName === null) "unknown" else result.backendName»
		Parameters: «FOR paramStr : result.paramFields.map[it | it.toDiagString].sort SEPARATOR ', '»«paramStr»«ENDFOR»
		Input bindings: «FOR boundFieldStr : result.bindings.entrySet.map[it | '''«it.key.toDiagString» = «it.value.toDiagString»'''].sort SEPARATOR ', '»«boundFieldStr»«ENDFOR»
	'''
	
	private def representCexIfPresent() '''
		«IF result.declarationCounterexample.isPresent»
			Counterexample/witness:
			«FOR step : result.declarationCounterexample.get.getSteps»
				Step «step.name»
				«FOR valuation : step.valuations»
					«"  "»- «valuation.variable.toDiagString» = «valuation.value.toDiagString» «IF result.parserResult !== null»(«result.parserResult.serializeAtom(valuation.variable)» = «result.parserResult.serializeAtom(valuation.value)»)«ENDIF»
				«ENDFOR»
			«ENDFOR»
		«ENDIF»
	'''
	
	private def representStagesAndLog() '''
		Stages
		------
		«FOR stage : result.getAllStages.filter[it | it !== null]»
			* «stage.getStageName»«IF stage.getSummary != ""» -- «stage.getSummary»«ENDIF» («stage.getLengthMs» ms): «stage.getStatus»
			«FOR logitem : stage.getLogItems(settings.minLogLevel)»
				«"    - "»«representLogitem(logitem)»
			«ENDFOR»
		«ENDFOR»
	'''
	
	private def representSettings() '''
		«IF result.settings !== null && settings.isIncludeSettings»
			Settings
			--------
			  «IF result.settings instanceof SettingsElement»«SettingsSerializer.serialize(result.settings.toSingle)»«ENDIF»
		«ENDIF»
	'''
	
	private def representConsoleOutput() '''
		«IF result.backendStdout.isPresent || result.backendStderr.isPresent»
			Console output
			--------------
			«IF result.backendStdout.isPresent»
				STDOUT
				
				«result.backendStdout.get.truncateIfLonger(settings.maxOutputLength)»
				
			«ENDIF»
			«IF result.backendStderr.isPresent && result.backendStderr.get.toString.trim.length > 0»
				STDERR
				
				«result.backendStderr.get.truncateIfLonger(settings.maxOutputLength)»
			«ENDIF»
		«ENDIF»
	'''
	
	private def representLogitem(LogItem item) {
		return '''[«item.severity»] «item.message»«IF settings.isShowLogitemTimestamps» («item.time»)«ENDIF»''';
	} 
	
	private def static truncateIfLonger(CharSequence str, int maxLen) {
		Preconditions.checkNotNull(str);
		Preconditions.checkArgument(maxLen > 0);
		
		if (str.length <= maxLen) {
			return str;
		} else {
			str.toString.substring(0, Math.min(maxLen - 5, str.length)) + "[...]";
		}
	}
}