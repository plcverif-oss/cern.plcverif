/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reporter.plaintext;

import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.help.SettingsHelp;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.verif.interfaces.IVerificationReporter;
import cern.plcverif.verif.interfaces.IVerificationReporterExtension;

public class PlaintextReporterExtension implements IVerificationReporterExtension {
	public static final String CMD_ID = "plaintext";

	@Override
	public IVerificationReporter createReporter() {
		return new PlaintextReporter();
	}

	@Override
	public IVerificationReporter createReporter(SettingsElement settings) throws SettingsParserException {
		return new PlaintextReporter(settings);
	}

	@Override
	public void fillSettingsHelp(SettingsHelp help) {
		help.addPluginDetails(CMD_ID, "Plain text reporter",
				"Generates a plain text representation of the verification results, including the metadata, configuration and the eventual counterexample. Only applicable if job=verif.",
				PlaintextReporterExtension.class);
		SpecificSettingsSerializer.fillSettingsHelp(help, PlaintextReporterSettings.class);
	}
}
