/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.library.reporter.plaintext.impl

import cern.plcverif.base.interfaces.data.result.JobStage
import cern.plcverif.library.reporter.plaintext.PlaintextReporterSettings
import cern.plcverif.verif.interfaces.data.VerificationResult
import cern.plcverif.verif.interfaces.data.VerificationStageTags
import com.google.common.base.Preconditions
import java.time.OffsetDateTime

final class CsvSummaryGenerator {
	//val static DATE_FORMAT_STRING = "yyyy-MM-dd HH:mm:ss z"; // e.g. '1954-09-21 01:23:45 CET'
	
	private new() {}
	
	static def represent(VerificationResult result, PlaintextReporterSettings settings) {
		Preconditions.checkNotNull(settings, "settings");
		Preconditions.checkNotNull(result, "VerificationResult should not be null.");
		Preconditions.checkNotNull(result.jobMetadata, "result.jobMetadata should not be null.");
		Preconditions.checkNotNull(result.jobMetadata.id, "result.jobMetadata.id should not be null.");
		Preconditions.checkNotNull(result.result, "result.result should not be null.");
		if (result.declarationCounterexample.isPresent) {
			for (step : result.declarationCounterexample.get.getSteps) {
				Preconditions.checkNotNull(step, "result.declarationCounterexample.steps should not contain any null step.");
				Preconditions.checkNotNull(step.name, "result.declarationCounterexample.steps should not contain any null-named step.");
			}
		}
		Preconditions.checkNotNull(result.getAllStages, "result.stageLogs should not be null.");
		for (stage : result.getAllStages) {
			Preconditions.checkNotNull(stage, "result.stageLogs should not contain any null stage.");
			Preconditions.checkNotNull(stage.getStageName, "stage.stageName is null");
		}
		
		return representInternal(result);	
	}
	
	private static def representInternal(VerificationResult result) '''
		"ModelID","TotalVerifTime_ms","McExecTime_ms","Result","CexLength","Backend","BackendAlgorithm","Timestamp"
		"«result.jobMetadata.id»","«totalVerifTime(result)»","«modelChekerExecTime(result)»","«result.result»","«cexLength(result)»","«if (result.backendName === null) "unknown" else result.backendName»","«result.backendAlgorithmId»","«now()»"
	'''
	
	/**
	 * Returns the current date and time in ISO-8601 format.
	 * See: https://docs.oracle.com/javase/8/docs/api/java/time/OffsetDateTime.html
	 */
	private def static now() {
		return OffsetDateTime.now().toString();
	}
	
	def static String cexLength(VerificationResult result) {
		if (result.declarationCounterexample.isPresent) {
			return Integer.toString(result.declarationCounterexample.get.getSteps.size);
		} else {
			return "";
		}
	}
	
	def static long modelChekerExecTime(VerificationResult result) {
		val backendExecStages = result.getStages(VerificationStageTags.BACKEND_EXECUTION);
		
		if (backendExecStages.isEmpty) {
			return -1;
		} else {
			return JobStage.sumLengthMs(backendExecStages);
		}
	}
	
	def static long totalVerifTime(VerificationResult result) {
		return JobStage.sumLengthMs(result.allStages);
	}
}