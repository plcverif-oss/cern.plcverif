/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reporter.plaintext;

import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.exceptions.SettingsSerializerException;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.library.reporter.plaintext.impl.CsvSummaryGenerator;
import cern.plcverif.library.reporter.plaintext.impl.PlaintextReportGenerator;
import cern.plcverif.verif.interfaces.IVerificationReporter;
import cern.plcverif.verif.interfaces.data.VerificationResult;

public class PlaintextReporter implements IVerificationReporter {
	private PlaintextReporterSettings settings;

	public PlaintextReporter() {
		this.settings = new PlaintextReporterSettings();
	}

	public PlaintextReporter(SettingsElement settings) throws SettingsParserException {
		this.settings = SpecificSettingsSerializer.parse(settings, PlaintextReporterSettings.class);
	}

	@Override
	public SettingsElement retrieveSettings() {
		try {
			SettingsElement ret = SpecificSettingsSerializer.toGenericSettings(settings).toSingle();
			ret.setValue(PlaintextReporterExtension.CMD_ID);
			return ret;
		} catch (SettingsParserException | SettingsSerializerException e) {
			throw new PlcverifPlatformException("Unable to retrieve settings.", e);
		}
	}

	@Override
	public void generateReport(VerificationResult result) {
		if (result == null) {
			throw new PlcverifPlatformException("Unable to report. Verification result is not available.");
		}
		
		String reportContent = PlaintextReportGenerator.represent(result, settings).toString();
			
		if (settings.isWriteToConsole()) {
			System.out.println(reportContent);
		}

		if (settings.isWriteToFile()) {
			result.addFileToSave(result.getJobMetadata().getIdWithoutSpecialChars() + ".report.txt",
					PlaintextReporterExtension.CMD_ID, reportContent);

			String csvSummaryContent = CsvSummaryGenerator.represent(result, settings).toString();
			result.addFileToSave(result.getJobMetadata().getIdWithoutSpecialChars() + ".summary.csv",
					PlaintextReporterExtension.CMD_ID, csvSummaryContent);
		}
	}
}
