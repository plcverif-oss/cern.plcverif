/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reporter.plaintext;

import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsElement;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory;
import cern.plcverif.verif.interfaces.AbstractHumanReporterSettings;

public class PlaintextReporterSettings extends AbstractHumanReporterSettings {
	@PlcverifSettingsElement(name = "write_to_console", description = "If true, writing the report to console is enabled.", mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private boolean writeToConsole = true;
	
	@PlcverifSettingsElement(name = "write_to_file", description = "If true, writing the report to file is enabled.", mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private boolean writeToFile = true;
	
	public static final String MAX_OUTPUT_LENGTH = "max_output_length";
	@PlcverifSettingsElement(name = MAX_OUTPUT_LENGTH, description = "Maximum console output length to be included. Longer console outputs will be truncated.", mandatory = PlcverifSettingsMandatory.OPTIONAL)
	private int maxOutputLength = 5000;
	
	public boolean isWriteToConsole() {
		return writeToConsole;
	}
	
	public boolean isWriteToFile() {
		return writeToFile;
	}
	
	/**
	 * Maximum console output length (stdout and stderr outputs) to be included. Longer console outputs shall be truncated.
	 */
	public int getMaxOutputLength() {
		return maxOutputLength;
	}
}
