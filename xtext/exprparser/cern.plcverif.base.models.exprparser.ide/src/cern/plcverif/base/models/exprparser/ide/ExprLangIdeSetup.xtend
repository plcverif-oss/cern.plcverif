/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *****************************************************************************/
package cern.plcverif.base.models.exprparser.ide

import cern.plcverif.base.models.exprparser.ExprLangRuntimeModule
import cern.plcverif.base.models.exprparser.ExprLangStandaloneSetup
import com.google.inject.Guice
import org.eclipse.xtext.util.Modules2

/**
 * Initialization support for running Xtext languages as language servers.
 */
class ExprLangIdeSetup extends ExprLangStandaloneSetup {

	override createInjector() {
		Guice.createInjector(Modules2.mixin(new ExprLangRuntimeModule, new ExprLangIdeModule))
	}
}
