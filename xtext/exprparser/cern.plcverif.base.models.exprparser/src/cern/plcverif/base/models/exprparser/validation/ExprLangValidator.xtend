/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.exprparser.validation

import cern.plcverif.base.models.expr.BinaryArithmeticExpression
import cern.plcverif.base.models.expr.BinaryLogicExpression
import cern.plcverif.base.models.expr.ComparisonExpression
import cern.plcverif.base.models.expr.ComparisonOperator
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.Literal
import cern.plcverif.base.models.expr.UnaryArithmeticExpression
import cern.plcverif.base.models.expr.UnaryLogicExpression
import cern.plcverif.base.models.expr.utils.TypeUtils
import cern.plcverif.base.models.exprparser.ExprLangVariableList
import cern.plcverif.base.models.exprparser.ExpressionParser
import cern.plcverif.base.models.exprparser.exprLang.ValuePlaceholder
import com.google.common.base.Preconditions
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.validation.Check

import static cern.plcverif.base.models.expr.utils.TypeUtils.*

/**
 * This class contains custom validation rules. 
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#validation
 */
class ExprLangValidator extends AbstractExprLangValidator {
	@Check
	def checkNameValidity(ValuePlaceholder e) {
		val variableList = getVariableList(e);

		if (variableList !== null && !variableList.isEmpty) {
			// We check the name validity only if there is a provided list of acceptable names
			if (e.value !== null && variableList.hasVariableName(e.value) == false) {
				// We have to check whether the variable reference here is a bit access to a word variable
				// This is needed because the variableList does not include the individual Byte variables as names
				var isWordAccess = false
				val byteAccessIndex = e.value.lastIndexOf("%");
				if (byteAccessIndex != -1){
					val wordVariable = e.value.substring(0, byteAccessIndex-1)
					if (variableList.hasVariableName(wordVariable) == true){
						val wordVariableInstance = variableList.variables.filter(it | it.variableName == wordVariable).toList.get(0)
						if(wordVariableInstance.plcType == 'WORD' || wordVariableInstance.plcType == 'BYTE'){
							isWordAccess = true
						}
					}
				}
				if (!isWordAccess){
					warning('''It seems that the name '«e.value»' is not known. «giveHint(e.value, variableList)»''', e, null);
				}
			}
		}
	}
	
	/**
	 * Try to give a hint with the list of similar variable names. Returns empty string if no hint can be given.
	 * @param varName Name of the variable not found
	 */
	private def String giveHint(String varName, ExprLangVariableList variableList) {
		Preconditions.checkNotNull(varName, "varName");
		Preconditions.checkNotNull(variableList, "variableList");
		
		// Try to give a list of similar variable names
		val proposals = variableList.variableNames.filter[it | it.endsWith("." + varName)].toList;
		
		if (proposals.isEmpty) {
			return "";
		} else {
			return '''Did you mean: «String.join(", ", proposals)»?'''
		}
	}
	
	@Check
	def checkType(Expression e) {
		// We don't check anything here, just try to infer the unknown types
		ExpressionParser.inferUnknownTypes(e);
	}
	
	@Check
	def checkType(BinaryLogicExpression e) {
		ExpressionParser.inferUnknownTypes(e);
		checkIfBoolean(e.leftOperand);
		checkIfBoolean(e.rightOperand);
	}
	
	@Check
	def checkType(UnaryLogicExpression e) {
		ExpressionParser.inferUnknownTypes(e);
		checkIfBoolean(e.operand);
	}
	
	def checkIfBoolean(Expression e) {
		if (!hasTemporalOrRegularBoolType(e)) {
			if (e instanceof Literal || !hasUnknownType(e)) {
				error('''Unexpected type, it should be Boolean or temporal.''', e, null);
			}
		}
	}
	
	@Check
	def checkType(BinaryArithmeticExpression e) {
		ExpressionParser.inferUnknownTypes(e);
		checkIfNonBoolean(e.leftOperand);
		checkIfNonBoolean(e.rightOperand);
	}
	
	@Check
	def checkType(ComparisonExpression e) {
		ExpressionParser.inferUnknownTypes(e);
		
		if (e.operator == ComparisonOperator.EQUALS || e.operator == ComparisonOperator.NOT_EQUALS) {
			// Left and right should be both Boolean or neither	
			if (TypeUtils.hasBoolType(e.leftOperand)) {
				checkIfBoolean(e.rightOperand);
			} else if (!TypeUtils.hasUnknownType(e.leftOperand) && !TypeUtils.hasBoolType(e.leftOperand)) {
				checkIfNonBoolean(e.rightOperand);
			}  
		} else {
			// Should not be Boolean
			checkIfNonBoolean(e.leftOperand);
			checkIfNonBoolean(e.rightOperand);	
		}
	}
	
	@Check
	def checkType(UnaryArithmeticExpression e) {
		ExpressionParser.inferUnknownTypes(e);
		checkIfNonBoolean(e.operand);
	}
	
	
	
	def checkIfNonBoolean(Expression e) {
		if (hasTemporalOrRegularBoolType(e)) {
			error('''Unexpected type, it should not be Boolean or temporal.''', e, null);
		}
	}
	

	/**
	 * Tries to load the list of variables from the resource set.
	 * Returns null if unsuccessful.
	 */
	private def ExprLangVariableList getVariableList(EObject e) {
		val resourceSet = e.eResource.resourceSet;
		val value = resourceSet.loadOptions.get(ExprLangVariableList);
		if (value instanceof ExprLangVariableList) {
			return value;
		}
		
		return null;

	}
}
