/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.exprparser;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.parser.IParseResult;
import org.eclipse.xtext.parser.IParser;
import org.eclipse.xtext.parser.ParseException;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.io.CharStreams;
import com.google.inject.Inject;
import com.google.inject.Injector;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.models.expr.BinaryArithmeticOperator;
import cern.plcverif.base.models.expr.BinaryExpression;
import cern.plcverif.base.models.expr.BinaryLogicExpression;
import cern.plcverif.base.models.expr.BoolLiteral;
import cern.plcverif.base.models.expr.ExplicitlyTyped;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.ExpressionSafeFactory;
import cern.plcverif.base.models.expr.FloatLiteral;
import cern.plcverif.base.models.expr.FloatWidth;
import cern.plcverif.base.models.expr.IntLiteral;
import cern.plcverif.base.models.expr.IntType;
import cern.plcverif.base.models.expr.StringLiteral;
import cern.plcverif.base.models.expr.Type;
import cern.plcverif.base.models.expr.string.ExprToString;
import cern.plcverif.base.models.expr.utils.ExprUtils;
import cern.plcverif.base.models.expr.utils.TypeUtils;
import cern.plcverif.base.models.exprparser.exceptions.ExpressionParsingException;
import cern.plcverif.base.models.exprparser.services.ExprLangGrammarAccess;

/**
 * Class for parsing expressions.
 * 
 * This implementation was partially inspired by
 * http://www.davehofmann.de/?p=101.
 * 
 * Helpful comments were found at
 * https://groups.google.com/forum/#!topic/xtend-lang/3gcjaPWyudM.
 */

public class ExpressionParser {
	@Inject
	private IParser parser;

	@Inject
	private ExprLangGrammarAccess grammarAccess;

	private static Injector injector = null;

	/**
	 * Sets the injector to be used for expression parsing. It is expected to be
	 * set up before creating the first {@link ExpressionParser} instance if
	 * there was an injector created for the UI already. If it is not set, a new
	 * injector is created using
	 * {@link ExprLangStandaloneSetup#createInjectorAndDoEMFRegistration()} upon
	 * the first expression parsing.
	 * 
	 * It is not permitted to set the injector after it was set already or
	 * created upon creating an expression parser instance.
	 * 
	 * @param exprInjector
	 *            Injector to be used to parse expressions.
	 */
	public static void setInjector(Injector exprInjector) {
		Preconditions.checkState(injector == null, "The injector was set already, setting it again is not permitted.");
		Preconditions.checkNotNull(exprInjector, "exprInjector");
		injector = exprInjector;
	}

	/**
	 * Creates a new expression parser. If a previous call of
	 * {@link #setInjector(Injector)} set the injector, that injector will be
	 * used to inject
	 */
	public ExpressionParser() {
		if (injector == null) {
			// Sebastian Zarnekow: "Please do never ever use the StandaloneSetup
			// in an Equinox OSGi container,
			// e.g. when running in Eclipse. It will be the cause for a bunch of
			// funny effects."
			injector = new ExprLangStandaloneSetup().createInjectorAndDoEMFRegistration();
		}

		injector.injectMembers(this);
	}

	public Expression parseExpression(StringReader exprString) throws ExpressionParsingException {
		try {
			return parseExpression(CharStreams.toString(exprString));
		} catch (IOException e) {
			throw new ExpressionParsingException("Unable to read the given reader.", e);
		}

	}

	public Expression parseExpression(String exprString) throws ExpressionParsingException {
		try (StringReader stringReader = new StringReader(exprString)) {
			IParseResult result = parser.parse(grammarAccess.getExpressionRule(), stringReader);
			
			

			if (result.hasSyntaxErrors()) {
				String firstError = "";
				for (INode error : result.getSyntaxErrors()) {
					if ("".equals(firstError)) {
						firstError = String.format("'%s' at %s: %s", error.getText(), error.getOffset(),
								error.getSyntaxErrorMessage());
					}
				}
				throw new ExpressionParsingException(
						String.format("Unable to parse the expression '%s', as it contains syntax errors. %s",
								exprString, firstError));
			}

			if (!(result.getRootASTElement() instanceof Expression)) {
				throw new ParseException("The root element of the parsed expression is not an 'Expression' node: "
						+ result.getRootASTElement());
			}
			
			
			Expression rootExpression = (Expression) result.getRootASTElement();
			parseSemanticSpecialCases(rootExpression);
			
			
			return rootExpression;
		}
	}

	public static void inferUnknownTypes(Expression expression) throws ExpressionParsingException {
		parseSemanticSpecialCases(expression);
		// bottom-up approach
		List<ExplicitlyTyped> nodesWithUnknownType = EmfHelper.getAllContentsOfType(expression, ExplicitlyTyped.class,
				true, it -> TypeUtils.hasUnknownType(it));
		// NOTE Actually the order is not guaranteed. If the nodes with unknown
		// type are not returned top-down, the computation may fail.

		for (ExplicitlyTyped it : Lists.reverse(nodesWithUnknownType)) {		
			// a) if it is in a binary operation, use the other node's type
			if (it.eContainer() != null && it.eContainer() instanceof BinaryExpression) {
				BinaryExpression container = (BinaryExpression) it.eContainer();
				if (container.getLeftOperand() == it && !TypeUtils.hasUnknownType(container.getRightOperand())) {
					trySetType(it, EcoreUtil.copy(container.getRightOperand().getType()));
					continue;
				} else if (container.getRightOperand() == it && !TypeUtils.hasUnknownType(container.getLeftOperand())) {
					trySetType(it, EcoreUtil.copy(container.getLeftOperand().getType()));
					continue;
				} else {
					// The other is unknown too.
				}
			}

			// b) The previous attempt failed. For the moment let's fail back to
			// INT/REAL.
			// TIA improve the solution if needed
			if (it instanceof BoolLiteral) {
				it.setType(ExpressionSafeFactory.INSTANCE.createBoolType());
			} else if (it instanceof IntLiteral) {
				it.setType(ExpressionSafeFactory.INSTANCE.createIntType(true, 16));
			} else if (it instanceof FloatLiteral) {
				it.setType(ExpressionSafeFactory.INSTANCE.createFloatType(FloatWidth.B16));
			} else {
				throw new ExpressionParsingException(String.format("Unable to infer type for '%s' in '%s'.",
						ExprToString.INSTANCE.toString(it), ExprToString.INSTANCE.toString(expression)));
			}
		}
	}
	
	private static void parseSemanticSpecialCases(Expression expr) {
		// Handle integer variables defined in different bases
		List<ExplicitlyTyped> nodesWithUnknownType = EmfHelper.getAllContentsOfType(expr, ExplicitlyTyped.class,
				true, it -> TypeUtils.hasUnknownType(it));
		for (ExplicitlyTyped it : Lists.reverse(nodesWithUnknownType)) {		
			if (it instanceof StringLiteral && ((StringLiteral)it).getValue().matches("(2|8|16)#((\\d|[a-zA-Z])+)")) {
				Matcher matcher = Pattern.compile("(2|8|16)#((?:\\d|[a-zA-Z])+)").matcher(((StringLiteral)it).getValue());
				matcher.find();
				int literalValue = Integer.parseInt(matcher.group(2), Integer.parseInt(matcher.group(1)));
				EcoreUtil.replace(it, ExpressionSafeFactory.INSTANCE.createIntLiteralWithUnknownType(literalValue));
			}
		}
		// Handle binary logic operations that should be binary arithmetic operations (language parsing problems) 
		List<BinaryLogicExpression> toReplaceLogicExpression = EmfHelper.getAllContentsOfType(expr, BinaryLogicExpression.class,
				true, it -> TypeUtils.isIntType(it.getLeftOperand().getType()));
		toReplaceLogicExpression.forEach(e -> EcoreUtil.replace(e, ExpressionSafeFactory.INSTANCE.createBinaryArithmeticExpression(ExprUtils.exprCopy(e.getLeftOperand()), ExprUtils.exprCopy(e.getRightOperand()), BinaryArithmeticOperator.valueOf("BITWISE_"+e.getOperator().toString()))));

	}
	
	private static void trySetType(ExplicitlyTyped e, Type type) {
		// Setting unknown type should always be fine
		if (TypeUtils.isUnknownType(type)) {
			e.setType(type);
			return;
		}
		
		if (e instanceof BoolLiteral && !TypeUtils.isBoolType(type)) {
			// Setting non-Bool type for a Boolean literal is not valid 
			return;
		}
		if (e instanceof IntLiteral && !TypeUtils.isIntType(type)) {
			// Setting non-integer type for an integer literal is not valid 
			return;
		}
		if (e instanceof FloatLiteral && !TypeUtils.isFloatType(type)) {
			// Setting non-float type for a float literal is not valid 
			return;
		}
		
		e.setType(type);
	}
}
