/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.exprparser;

import static java.util.stream.Collectors.toCollection;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.base.Preconditions;

import cern.plcverif.base.models.expr.utils.TypedAstVariable;

/**
 * Class to contain extra information to specialise the content assist of an
 * expression editor.
 */
public final class ExprLangVariableList {
	private List<TypedAstVariable> variables;

	/**
	 * List of variable names to be proposed
	 */
	private List<String> variableNames = Collections.emptyList();

	private Set<String> varNameLookup = null;

	private ExprLangVariableList() {
		// workaround to avoid automated JIT binding creation
	}

	public static ExprLangVariableList createInstance() {
		return new ExprLangVariableList();
	}

	/**
	 * Returns the list of variables (with their type information) to be
	 * proposed.
	 * 
	 * @return Variables to propose
	 */
	public List<TypedAstVariable> getVariables() {
		return variables;
	}

	/**
	 * Returns the list of variable names to be proposed.
	 * 
	 * @return Variable names to be proposed.
	 */
	public List<String> getVariableNames() {
		return variableNames;
	}

	/**
	 * Sets the list of variables to be proposed.
	 * 
	 * @param variables
	 *            Variables to be proposed. Shall not be {@code null}.
	 */
	public void setVariables(List<TypedAstVariable> variables) {
		this.variables = Collections.unmodifiableList(Preconditions.checkNotNull(variables));
		this.variableNames = Collections
				.unmodifiableList(variables.stream().map(it -> it.getVariableName()).collect(Collectors.toList()));
		this.varNameLookup = this.variableNames.stream().map(it -> it.toLowerCase())
				.collect(toCollection(HashSet::new));
	}

	/**
	 * Checks if the container contains the given name. The comparison will be
	 * case insensitive.
	 * 
	 * @param name
	 *            Variable name to look up
	 * @return True iff this container contains a variable with the given name
	 *         (case insensitive).
	 */
	public boolean hasVariableName(String name) {
		Preconditions.checkNotNull(name, "null name is not valid");
		return varNameLookup != null && varNameLookup.contains(name.toLowerCase());
	}

	/**
	 * Returns true iff there is no variable to propose.
	 */
	public boolean isEmpty() {
		return variables.isEmpty();
	}
}
