/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.exprparser.tests

import cern.plcverif.base.common.emf.textual.EmfModelPrinter
import cern.plcverif.base.common.emf.textual.SimpleEObjectToText
import cern.plcverif.base.common.utils.IoUtils
import cern.plcverif.base.models.exprparser.exprLang.ExpressionFile
import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.XtextRunner
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(XtextRunner)
@InjectWith(ExprLangInjectorProvider)

class BatchRegressionTest {
	@Inject
	ParseHelper<ExpressionFile> parseHelper
	
	@Test
	def void batchRegressionTest() {
		regressionTest("ExprParserRegressionTest1");
	}
	
	private def regressionTest(String fileBaseName) {
		val inputContent = IoUtils.readResourceFile(fileBaseName + ".input.txt", this.class.classLoader);
		val parsedExprModel = parseHelper.parse(inputContent);
		Assert.assertNotNull(parsedExprModel);
		if (!parsedExprModel.eResource.errors.isEmpty) {
			parsedExprModel.eResource.errors.forEach[it | println(it)];
		}
		Assert.assertTrue(parsedExprModel.eResource.errors.isEmpty);
		
		val expectedText = IoUtils.readResourceFile(fileBaseName + ".output.txt", this.class.classLoader).trim();
		val actualText = EmfModelPrinter.print(parsedExprModel, SimpleEObjectToText.INSTANCE).toString().trim();
		
		Assert.assertEquals(expectedText, actualText);
	}
	
}
