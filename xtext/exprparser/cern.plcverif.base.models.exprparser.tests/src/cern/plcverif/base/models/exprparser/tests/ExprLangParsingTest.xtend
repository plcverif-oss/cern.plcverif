/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.exprparser.tests

import cern.plcverif.base.models.exprparser.exprLang.ExpressionFile
import com.google.inject.Inject
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.XtextRunner
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(XtextRunner)
@InjectWith(ExprLangInjectorProvider)
class ExprLangParsingTest {
	@Inject
	ParseHelper<ExpressionFile> parseHelper
	
	@Test
	def void loadModel() {
		val result = parseHelper.parse('''
			a + b * 5;
			AG(true OR false);
			INT#25;
			a.b.c + 12;
			a != !a;
		''')
		Assert.assertNotNull(result);
		if (!result.eResource.errors.isEmpty) {
			result.eResource.errors.forEach[it | System.err.println(it)];
		}
		Assert.assertTrue(result.eResource.errors.isEmpty);
	}
}
