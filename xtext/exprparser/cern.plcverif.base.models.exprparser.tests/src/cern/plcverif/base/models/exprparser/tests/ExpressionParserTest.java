/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.exprparser.tests;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.junit.Assert;
import org.junit.Test;

import cern.plcverif.base.models.expr.BinaryLogicExpression;
import cern.plcverif.base.models.expr.ComparisonExpression;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.ExpressionSafeFactory;
import cern.plcverif.base.models.expr.FloatLiteral;
import cern.plcverif.base.models.expr.FloatType;
import cern.plcverif.base.models.expr.IntLiteral;
import cern.plcverif.base.models.expr.IntType;
import cern.plcverif.base.models.expr.UnknownType;
import cern.plcverif.base.models.exprparser.ExpressionParser;
import cern.plcverif.base.models.exprparser.exceptions.ExpressionParsingException;
import cern.plcverif.base.models.exprparser.exprLang.ValuePlaceholder;

public class ExpressionParserTest {
	@Test
	public void smokeTest1() throws ExpressionParsingException {
		String stringToParse = "(a + b) > 5";
		
		ExpressionParser parser = new ExpressionParser();
		Expression result = parser.parseExpression(stringToParse);
		Assert.assertNotNull(result);
		Assert.assertTrue(result instanceof ComparisonExpression);
	}
	
	@Test
	public void smokeTest2() throws ExpressionParsingException {
		String stringToParse = "I0.0 and I0.1";
		
		Expression result = new ExpressionParser().parseExpression(stringToParse);
		Assert.assertNotNull(result);
		Assert.assertTrue(result instanceof BinaryLogicExpression);
		Assert.assertTrue(((BinaryLogicExpression)result).getLeftOperand() instanceof ValuePlaceholder);
		Assert.assertTrue(((BinaryLogicExpression)result).getRightOperand() instanceof ValuePlaceholder);
	}
	
	@Test
	public void smokeTest3() throws ExpressionParsingException {
		String stringToParse = "1234";
		
		Expression result = new ExpressionParser().parseExpression(stringToParse);
		Assert.assertNotNull(result);
		Assert.assertTrue(result instanceof IntLiteral);
		
		ExpressionParser.inferUnknownTypes(result);
		Assert.assertTrue(result.getType() instanceof IntType);
	}
	
	@Test
	public void smokeTest4() throws ExpressionParsingException {
		String stringToParse = "10.0";
		
		Expression result = new ExpressionParser().parseExpression(stringToParse);
		Assert.assertNotNull(result);
		Assert.assertTrue(result instanceof FloatLiteral);
		
		ExpressionParser.inferUnknownTypes(result);
		Assert.assertTrue(result.getType() instanceof FloatType);
	}
	
	@Test
	public void smokeTest5() throws ExpressionParsingException {
		String stringToParse = "intVar > 5";
		
		Expression result = new ExpressionParser().parseExpression(stringToParse);
		Assert.assertNotNull(result);
		Assert.assertTrue(result instanceof ComparisonExpression);
		ComparisonExpression comparison = (ComparisonExpression) result;
		
		Assert.assertTrue(comparison.getLeftOperand() instanceof ValuePlaceholder);
		EcoreUtil.replace(comparison.getLeftOperand(), ExpressionSafeFactory.INSTANCE.createIntLiteral(123, false, 32));
		Assert.assertTrue(comparison.getRightOperand().getType() instanceof UnknownType);
		
		ExpressionParser.inferUnknownTypes(result);
		
		Assert.assertTrue(comparison.getLeftOperand().getType() instanceof IntType);
		Assert.assertTrue(comparison.getRightOperand().getType() instanceof IntType);
		Assert.assertTrue(comparison.getLeftOperand().getType().dataTypeEquals(comparison.getRightOperand().getType()));
	}
}
