package cern.plcverif.base.models.exprparser.ui;

import org.osgi.framework.BundleContext;

import com.google.inject.Injector;

import cern.plcverif.base.models.exprparser.ExpressionParser;
import cern.plcverif.base.models.exprparser.ui.internal.ExprparserActivator;

public class ExprLangUiCustomActivator extends ExprparserActivator {
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		
		// Pass the injector to the ExpressionParser
		Injector injector = getInjector(CERN_PLCVERIF_BASE_MODELS_EXPRPARSER_EXPRLANG);
		ExpressionParser.setInjector(injector);
	}
}
