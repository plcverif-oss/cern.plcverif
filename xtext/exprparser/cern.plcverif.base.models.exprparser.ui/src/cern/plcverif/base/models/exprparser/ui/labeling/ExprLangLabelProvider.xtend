/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *****************************************************************************/
package cern.plcverif.base.models.exprparser.ui.labeling

import com.google.inject.Inject
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider
import org.eclipse.xtext.Keyword
import org.eclipse.xtext.ui.label.DefaultEObjectLabelProvider

/**
 * Provides labels for EObjects.
 * 
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#label-provider
 */
class ExprLangLabelProvider extends DefaultEObjectLabelProvider {

	@Inject
	new(AdapterFactoryLabelProvider delegate) {
		super(delegate);
	}

	// Labels and icons can be computed like this:
	
//	def text(Greeting ele) {
//		'A greeting to ' + ele.name
//	}
//
//	def image(Greeting ele) {
//		'Greeting.gif'
//	}

	def image(Keyword ele) {
		switch (ele.value) {
			case "AG",
			case "AF",
			case "AX",
			case "EG",
			case "EF",
			case "EX",
			case "A",
			case "E":
				return ctlImage()
			
			case "G",
			case "F",
			case "X":
				return ltlImage()
				
			case "!",
			case "\u00AC", // negation sign
			case "NOT",
			case "&&",
			case "||",
			case "-->",
			case "\u2192", // -->
			case "\u21D2", // ==>
			case "\u2228", // logical or
			case "\u2227", // logical and
			case "AND",
			case "OR",
			case "XOR":
				return boolImage()
				
			case "+",
			case "-",
			case "*",
			case "/",
			case "**",
			case "^",
			case "DIV",
			case "MOD":
				return arithmeticImage()
				
			case ">",
			case "<",
			case ">=",
			case "\u2265", // >=
			case "<=",
			case "\u2264", // <=
			case "=",
			case "!=",
			case "\u2260", // =/=
			case "<>":
				return comparisonImage()
		} 
		
		return null;
	}
	
	def ctlImage() {
		'CtlExpression.png'
	}
	
	def ltlImage() {
		'LtlExpression.png'
	}
	
	def boolImage() {
		'Boolean.png'
	}
	
	def comparisonImage() {
		'Comparison.png'
	}
	
	def arithmeticImage() {
		'Arithmetic.png'
	}
}
