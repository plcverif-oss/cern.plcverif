/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.exprparser.ui.contentassist

import cern.plcverif.base.models.exprparser.ExprLangVariableList
import com.google.inject.Inject
import org.eclipse.emf.ecore.EObject
import org.eclipse.jface.viewers.StyledString
import org.eclipse.swt.graphics.Image
import org.eclipse.xtext.RuleCall
import org.eclipse.xtext.ui.IImageHelper
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor

/**
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#content-assist
 * on how to customize the content assistant.
 */
class ExprLangProposalProvider extends AbstractExprLangProposalProvider {
	@Inject
    IImageHelper imageHelper;
	
	override complete_ValueString(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		val ExprLangVariableList knownVariables = context.resource.resourceSet.loadOptions.get(ExprLangVariableList) as ExprLangVariableList;
		
		if (knownVariables !== null && knownVariables.variables !== null) {
			val varImage = imageHelper.getImage("variable.png");
			for (v : knownVariables.variables) {
				val varName = v.getVariableName;
				val varPlcType = v.getPlcType;

				acceptor.accept(createCompletionProposal(
					varName + " ",
					String.format("%s (%s variable)", varName, varPlcType),
					varImage,
					context
				));
			}
		} else {
			// nothing to do
		}
		
		val boolImage = imageHelper.getImage("Boolean.png");
		acceptor.accept(createCompletionProposal("true ", "true", boolImage, context));
		acceptor.accept(createCompletionProposal("false ", "false", boolImage, context));
		
		super.complete_ValueString(model, ruleCall, context, acceptor)
	}	
	
	override protected doCreateProposal(String proposal, StyledString displayString, Image image, int priority, ContentAssistContext context) {
		if (proposal.equals(";")) {
			// Do not propose ';'
			return null;
		}
		
		super.doCreateProposal(proposal, displayString, image, priority, context)
	}
	
}
