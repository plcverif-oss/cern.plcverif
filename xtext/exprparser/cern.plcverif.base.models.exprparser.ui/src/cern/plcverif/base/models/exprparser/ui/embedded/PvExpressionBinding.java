/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.exprparser.ui.embedded;

import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocumentListener;

import com.google.common.base.Preconditions;

import cern.plcverif.base.gui.binding.PvDataBinding;
import cern.plcverif.base.gui.binding.PvTextBinding;

public class PvExpressionBinding extends PvTextBinding {
	private ExpressionEditor expressionEditor;

	@SuppressWarnings("restriction")
	public PvExpressionBinding(ExpressionEditor editor, String settingsAttribute, PvDataBinding parent) {
		super(settingsAttribute, parent);
		this.expressionEditor = Preconditions.checkNotNull(editor, "The represented expression editor cannot be null.");

		// Register for modification event
		this.expressionEditor.getEditor().getDocument().addDocumentListener(new IDocumentListener() {
			@Override
			public void documentChanged(DocumentEvent event) {
				setDirty();
				fireChangeListeners();
				refresh();
			}

			@Override
			public void documentAboutToBeChanged(DocumentEvent event) {
			}
		});
	}

	@Override
	public String getValue() {
		return expressionEditor.getText();
	}

	@Override
	public void setValue(String value) {
		if (value == null) {
			expressionEditor.setText("");
		} else {
			expressionEditor.setText(value);
		}
		
		expressionEditor.unselectText();
	}

	@Override
	protected boolean isThisValid() {
		return expressionEditor.getText().isEmpty() == false;
	}
}
