/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/

package cern.plcverif.base.models.exprparser.ui.syntaxcoloring


import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.xtext.ide.editor.syntaxcoloring.DefaultSemanticHighlightingCalculator
import org.eclipse.xtext.ide.editor.syntaxcoloring.HighlightingStyles
import org.eclipse.xtext.ide.editor.syntaxcoloring.IHighlightedPositionAcceptor
import org.eclipse.xtext.util.CancelIndicator
import cern.plcverif.base.models.exprparser.exprLang.ValuePlaceholder
import cern.plcverif.base.models.exprparser.exprLang.ExprLangPackage

class ExprLangSemanticHighlightingCalculator extends DefaultSemanticHighlightingCalculator {
	// Based on https://blogs.itemis.com/en/xtext-hint-identifiers-conflicting-with-keywords
	
	private static class ClassFeaturePair {
		Class<?> clazz;
		EStructuralFeature feature;
		new (Class<?> clazz, EStructuralFeature feature) {
			this.clazz = clazz;
			this.feature = feature;
		}
	}
	
	val NAMELIKE_FEATURES =
		#[
			new ClassFeaturePair(ValuePlaceholder, ExprLangPackage.eINSTANCE.valuePlaceholder_Value)
		];
	
	override protected highlightElement(EObject object, IHighlightedPositionAcceptor acceptor, CancelIndicator cancelIndicator) {
		for (item : NAMELIKE_FEATURES) {
			if (item.clazz.isInstance(object)) {
				highlightFeature(acceptor, object, item.feature, HighlightingStyles.DEFAULT_ID);
			}
		}
		return false;
	}
}
