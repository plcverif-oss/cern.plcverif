/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.exprparser.ui.embedded;

import java.util.List;
import java.util.Optional;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.xtext.resource.FileExtensionProvider;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.embedded.EmbeddedEditor;
import org.eclipse.xtext.ui.editor.embedded.EmbeddedEditorFactory;
import org.eclipse.xtext.ui.editor.embedded.EmbeddedEditorModelAccess;
import org.eclipse.xtext.ui.editor.embedded.IEditedResourceProvider;
import org.eclipse.xtext.ui.resource.IResourceSetProvider;

import com.google.common.base.Preconditions;
import com.google.inject.Inject;
import com.google.inject.Injector;

import cern.plcverif.base.models.expr.utils.TypedAstVariable;
import cern.plcverif.base.models.exprparser.ExprLangVariableList;
import cern.plcverif.base.models.exprparser.ui.internal.ExprparserActivator;

/**
 * Expression editor GUI component based on embedded Xtext editor for the
 * ExprLang.
 */
@SuppressWarnings("restriction")
public final class ExpressionEditor {
	private EmbeddedEditorModelAccess editorModelAccess;
	private EmbeddedEditor editor;
	private Composite enclosingComposite;
	private ExprLangVariableList varList;

	/**
	 * Creates a new expression editor within the given composite.
	 * 
	 * @param parent
	 *            Parent composite
	 */
	public ExpressionEditor(Composite parent) {
		this(parent, "");
	}

	/**
	 * Creates a new expression editor within the given composite with the given
	 * default value.
	 * 
	 * @param parent
	 *            Parent composite
	 * @param defaultValue
	 *            Default text
	 */
	public ExpressionEditor(Composite parent, String defaultValue) {
		// Get injector for the language
		ExprparserActivator activator = ExprparserActivator.getInstance();
		Injector injector = activator.getInjector(ExprparserActivator.CERN_PLCVERIF_BASE_MODELS_EXPRPARSER_EXPRLANG);

		// Create enclosing composite
		enclosingComposite = new Composite(parent, SWT.BORDER);
		enclosingComposite.setLayout(GridLayoutFactory.fillDefaults().create());

		varList = ExprLangVariableList.createInstance();

		// Create control
		ExprResourceProvider provider = injector.getInstance(ExprResourceProvider.class);
		provider.setVariableList(varList);
		EmbeddedEditorFactory editorFactory = injector.getInstance(EmbeddedEditorFactory.class);
		this.editor = editorFactory.newEditor(provider).showErrorAndWarningAnnotations().withParent(enclosingComposite);
		this.editorModelAccess = editor.createPartialEditor("", defaultValue, "", false);

		tryGetStyledTextChild().ifPresent(c -> c.setAlwaysShowScrollBars(false));
	}

	/**
	 * Returns the current text in the editor.
	 * 
	 * @return Current text in the editor. Never {@code null}.
	 */
	public String getText() {
		String ret = this.editorModelAccess.getEditablePart();
		if (ret == null) {
			return "";
		} else {
			// remove new line characters
			return ret.replaceAll("\n", " ").replaceAll("\r", "");
		}
	}

	/**
	 * Sets the text in the editor to the given one.
	 * 
	 * @param newText
	 *            New text to be displayed in the editor. Shall not be
	 *            {@code null}.
	 */
	public void setText(String newText) {
		this.editorModelAccess.updateModel(
				Preconditions.checkNotNull(newText, "null is an invalid input for ExpressionEditor.setText"));
	}

	/**
	 * Returns the SWT control representing the editor.
	 * 
	 * @return SWT control
	 */
	public Control getSwtControl() {
		return this.enclosingComposite;
	}

	/**
	 * Returns the list of variable names used in the editor's content assist.
	 * 
	 * @return Variable names in content assist
	 */
	public List<String> getVariablesForContentAssist() {
		return varList.getVariableNames();
	}

	/**
	 * Sets the list of variables to be used in the editor's content assist.
	 * 
	 * @param variableList
	 *            Variables to be used in content assist
	 */
	public void setVariablesForContentAssist(List<TypedAstVariable> variableList) {
		varList.setVariables(variableList);
	}

	EmbeddedEditor getEditor() {
		return editor;
	}

	private static class ExprResourceProvider implements IEditedResourceProvider {
		@Inject
		private IResourceSetProvider resourceSetProvider;
		@Inject
		private FileExtensionProvider ext;

		private ExprLangVariableList variableList = null;

		public void setVariableList(ExprLangVariableList variableList) {
			this.variableList = variableList;
		}

		@Override
		public XtextResource createResource() {
			ResourceSet resourceSet = resourceSetProvider.get(null);
			URI uri = URI.createURI("synthetic:/expr." + ext.getPrimaryFileExtension());
			XtextResource result = (XtextResource) resourceSet.createResource(uri);
			resourceSet.getLoadOptions().put(ExprLangVariableList.class, variableList);
			resourceSet.getResources().add(result);
			return result;
		}
	}

	public void unselectText() {
		tryGetStyledTextChild().ifPresent(c -> c.setSelectionRange(0, 0));
	}

	private Optional<StyledText> tryGetStyledTextChild() {
		if (enclosingComposite.getChildren().length > 0 && enclosingComposite.getChildren()[0] instanceof Canvas) {
			Canvas canvas = (Canvas) enclosingComposite.getChildren()[0];

			for (Control child : canvas.getChildren()) {
				if (child instanceof StyledText) {
					return Optional.of((StyledText) child);
				}
			}
		}

		return Optional.empty();
	}
}
