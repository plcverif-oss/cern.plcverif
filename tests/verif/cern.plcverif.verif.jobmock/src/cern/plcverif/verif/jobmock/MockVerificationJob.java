/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.verif.jobmock;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;

import com.google.common.base.Preconditions;

import cern.plcverif.base.common.progress.ICanceling;
import cern.plcverif.base.common.progress.NullProgressReporter;
import cern.plcverif.base.common.settings.Settings;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsSerializer;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer;
import cern.plcverif.base.interfaces.IAstReference;
import cern.plcverif.base.interfaces.IParserLazyResult;
import cern.plcverif.base.interfaces.IReduction;
import cern.plcverif.base.interfaces.IReductionExtension;
import cern.plcverif.base.interfaces.data.CfaJobSettings;
import cern.plcverif.base.interfaces.data.JobMetadata;
import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.interfaces.data.ModelTasks;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.OriginalDataTypeFieldAnnotation;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.cfa.trace.CfaInstantiationTrace;
import cern.plcverif.base.models.cfa.transformation.CallInliner;
import cern.plcverif.base.models.cfa.transformation.CfaInstantiator;
import cern.plcverif.base.models.cfa.validation.CfaValidation;
import cern.plcverif.base.models.expr.AtomicExpression;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.library.backend.nusmv.NusmvBackendExtension;
import cern.plcverif.library.reduction.basic.BasicReductionsExtension;
import cern.plcverif.library.reporter.plaintext.PlaintextReporterExtension;
import cern.plcverif.library.requirement.assertion.AssertionRequirementExtension;
import cern.plcverif.verif.interfaces.IBackend;
import cern.plcverif.verif.interfaces.IBackendExtension;
import cern.plcverif.verif.interfaces.IRequirement;
import cern.plcverif.verif.interfaces.IRequirementExtension;
import cern.plcverif.verif.interfaces.IVerificationReporterExtension;
import cern.plcverif.verif.interfaces.data.VerificationJobSettings;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.VerificationResult;
import cern.plcverif.verif.job.VerificationJob;
import cern.plcverif.verif.job.VerificationJobOptions;

public class MockVerificationJob {
	public static class MockVerificationJobConfig {
		private IBackendExtension backendExtension = new NusmvBackendExtension();
		private Collection<IReductionExtension> reductionExtensions = Arrays.asList(new BasicReductionsExtension());
		private IVerificationReporterExtension reporterExtension = new PlaintextReporterExtension();
		private IRequirementExtension requirementExtension = new AssertionRequirementExtension();
		private Consumer<IRequirement> requirementConfiguration = it -> {
		};
		private String settings = "";

		/**
		 * Returns the backend extension to be used in the mock verification
		 * job.
		 * <p>
		 * If not set, by default it returns an instance of
		 * {@link NusmvBackendExtension}.
		 * 
		 * @return Backend extension to be used. Never {@code null}.
		 */
		public IBackendExtension getBackendExtension() {
			return backendExtension;
		}

		/**
		 * Sets the backend extension to be used in the mock verification job.
		 * 
		 * @param backendExtension
		 *            Backend extension to be used. Shall not be {@code null}.
		 * @throws NullPointerException
		 *             if the given extension is null.
		 */
		public void setBackendExtension(IBackendExtension backendExtension) {
			this.backendExtension = Preconditions.checkNotNull(backendExtension);
		}

		/**
		 * Returns the collection of reductions to be used in the mock
		 * verification job.
		 * 
		 * @return Reductions to be used. Never {@code null}.
		 */
		public Collection<IReductionExtension> getReductionExtensions() {
			return reductionExtensions;
		}

		/**
		 * Adds a new reduction to the collection of reductions to be used in
		 * the mock verification job.
		 * 
		 * @param reductionExtension
		 *            Reduction extension to be added. Shall not be {@code null}.
		 * @throws NullPointerException
		 *             if the given extension is null.
		 */
		public void addReductionExtension(IReductionExtension reductionExtension) {
			this.reductionExtensions.add(Preconditions.checkNotNull(reductionExtension));
		}

		/**
		 * Returns the reporter extension to be used in the mock verification
		 * job.
		 * <p>
		 * If not set, by default it returns an instance of
		 * {@link PlaintextReporterExtension}.
		 * 
		 * @return Backend extension to be used. Never {@code null}, but can be {@link Optional#empty()}.
		 */
		public Optional<IVerificationReporterExtension> getReporterExtension() {
			return Optional.ofNullable(reporterExtension);
		}

		/**
		 * Sets the reporter extension to be used in the mock verification job.
		 * 
		 * @param reporterExtension
		 *            Reporter extension to be used. May be {@code null}, in that case the reporting step will be skipped.
		 */
		public void setReporterExtension(IVerificationReporterExtension reporterExtension) {
			this.reporterExtension = reporterExtension;
		}

		/**
		 * Returns the requirement extension to be used in the mock verification
		 * job.
		 * <p>
		 * If not set, by default it returns an instance of
		 * {@link AssertionRequirementExtension}.
		 * 
		 * @return Requirement extension to be used. Never {@code null}.
		 */
		public IRequirementExtension getRequirementExtension() {
			return requirementExtension;
		}

		/**
		 * Sets the requirement extension to be used in the mock verification job.
		 * 
		 * @param requirementExtension
		 *            Requirement extension to be used. Shall not be {@code null}.
		 * @throws NullPointerException
		 *             if the given extension is null.
		 */
		public void setRequirementExtension(IRequirementExtension requirementExtension) {
			this.requirementExtension = Preconditions.checkNotNull(requirementExtension);
		}

		protected Consumer<IRequirement> getRequirementConfiguration() {
			return requirementConfiguration;
		}

		/**
		 * Sets a requirement configuration, i.e., a function that will be invoked with the {@link IRequirement} instance after its creation.
		 * This function can make modification on the created {@link IRequirement} before it is used.
		 * @param requirementConfiguration Requirement configuration function. Shall not be {@code null}.
		 * @throws NullPointerException
		 *             if the given consumer is null.
		 */
		public void setRequirementConfiguration(Consumer<IRequirement> requirementConfiguration) {
			this.requirementConfiguration = Preconditions.checkNotNull(requirementConfiguration);
		}

		/**
		 * Creates a new mock verification job configuration instance that does not contain any reductions.
		 * @return Configuration.
		 */
		public static MockVerificationJobConfig noReduction() {
			MockVerificationJobConfig ret = new MockVerificationJobConfig();
			ret.reductionExtensions = Collections.emptyList();
			return ret;
		}

		/**
		 * Returns the settings to be used in the verification job in a textual representation.
		 * @return Settings to be used. Never {@code null}.
		 */
		public String getSettings() {
			return settings;
		}
		
		/**
		 * Sets the settings to be used in the verification job, given in a textual representation.
		 * @param settings Settings to be used. Shall not be {@code null}.
		 * @throws NullPointerException
		 *             if the given settings string is null.
		 */
		public void setSettings(String settings) {
			this.settings = Preconditions.checkNotNull(settings);
		}
	}

	/**
	 * This function mocks the full PLCverif verification job with its default
	 * settings. (It is a simplified version of VerificationJob.execute, without
	 * extension points.)
	 * <p>
	 * FOR TESTING AND DIAGNOSTICS ONLY.
	 * <p>
	 * Default settings are assumed, but they can be changed by passing an
	 * appropriate {@code config} parameter.
	 *
	 * @see VerificationJob
	 * @param cfd
	 *            CFD to be verified.
	 * @param modelName
	 *            Name to be used as model identifier.
	 * @param outputDir
	 *            Output directory to be used.
	 * @param config
	 *            Configuration
	 * @param executeBackend
	 *            If false, the backend execution will be skipped.
	 * @throws SettingsParserException
	 *             if problem occurred while parsing
	 *             {@code config.getSettings()}.
	 */
	public static VerificationResult run(CfaNetworkDeclaration cfd, String modelName, Path outputDir,
			MockVerificationJobConfig config, final boolean executeBackend) throws SettingsParserException {
		CfaValidation.validate(cfd);

		VerificationJob job = new VerificationJob() {
			protected IParserLazyResult parseCode(VerificationResult result) {
				return dummyParserResult();
			}

			@Override
			protected CfaNetworkDeclaration generateCfa(VerificationResult result, IParserLazyResult parserResult) {
				return cfd;
			}

			@Override
			protected void executeBackend(VerificationResult result, ICanceling canceler) {
				if (executeBackend) {
					super.executeBackend(result, canceler);
				}
			}
		};

		// Setup VerificationJob based on MockVerificationJobConfig
		job.setMetadata(new JobMetadata());
		job.getMetadata().setId(modelName);
		job.getMetadata().setName(modelName);
		job.setOutputDirectory(outputDir);
		job.setProgramDirectory(Paths.get("."));

		SettingsElement settings = SettingsSerializer.parseSettingsFromString(config.getSettings()).toSingle();
		SettingsElement jobSettings = SettingsElement.empty();
		if (settings != null) {
			jobSettings = settings.toSingle().getAttribute(CfaJobSettings.JOB)
					.orElse(SettingsElement.empty()).toSingle();
			job.setSettings(SpecificSettingsSerializer.parse(jobSettings, VerificationJobSettings.class));
		}

		job.setModelTasks(new ModelTasks());
		SettingsElement reductionSettings = SettingsElement.empty();
		if (settings != null) {
			reductionSettings = settings.toSingle().getAttribute(CfaJobSettings.REDUCTIONS)
					.orElse(SettingsElement.empty()).toSingle();
		}
		List<IReduction> reductions = new ArrayList<>();
		for (IReductionExtension redExt : config.getReductionExtensions()) {
			reductions.add(redExt.createReduction(reductionSettings));
		}
		job.getModelTasks().setReductions(reductions);

		job.setRequirement(createRequirement(config, jobSettings.getAttribute(VerificationJobOptions.REQUIREMENT)));
		if (config.getRequirementConfiguration() != null) {
			config.getRequirementConfiguration().accept(job.getRequirement());
		}
		job.setBackend(createBackend(config, jobSettings.getAttribute(VerificationJobOptions.BACKEND)));

		if (config.getReporterExtension().isPresent()) {
			job.setReporters(Collections.singletonList(config.getReporterExtension().get().createReporter()));
		} else {
			job.setReporters(Collections.emptyList());
		}

		VerificationResult result = job.execute(NullProgressReporter.getInstance());
		return result;
	}

	private static IRequirement createRequirement(MockVerificationJobConfig config, Optional<Settings> reqSettings)
			throws SettingsParserException {
		if (reqSettings.isPresent()) {
			return config.getRequirementExtension().createRequirement(reqSettings.get().toSingle());
		} else {
			return config.getRequirementExtension().createRequirement();
		}
	}

	private static IBackend createBackend(MockVerificationJobConfig config, Optional<Settings> backendSettings)
			throws SettingsParserException {
		if (backendSettings.isPresent()) {
			return config.getBackendExtension().createBackend(backendSettings.get().toSingle());
		} else {
			return config.getBackendExtension().createBackend();
		}
	}

	public static IParserLazyResult dummyParserResult() {
		return new IParserLazyResult() {
			@Override
			public AtomicExpression parseAtom(String atom) {
				return null;
			}

			@Override
			public Set<String> getFileNames() {
				return null;
			}

			@Override
			public IAstReference<?> getAst() {
				return null;
			}

			@Override
			public CfaNetworkDeclaration generateCfa(JobResult result) {
				return null;
			}

			@Override
			public String serializeAtom(AtomicExpression atom) {
				return null;
			}
			
			@Override
			public String serializeAtom(AtomicExpression atom, OriginalDataTypeFieldAnnotation hint) {
				return null;
			}
		};
	}

	public static void instantiateAndEnumerateModel(VerificationProblem problem, boolean inline,
			VerificationResult result, boolean enumerateArrays) {
		// Instantiate CFA network
		CfaInstantiationTrace traceModel = CfaInstantiator
				.transformCfaNetwork((CfaNetworkDeclaration) problem.getModel(), enumerateArrays);
		Expression requirement = CfaInstantiator.transformExpression(traceModel, problem.getRequirement());

		problem.setModel(traceModel.getInstance());
		problem.setRequirement(requirement, problem.getRequirementRepresentation());
		problem.setEocLocation(
				traceModel.getInstanceOf(problem.getEocLocation(), traceModel.getInstance().getMainAutomaton()));

		result.setCfaInstance(traceModel.getInstance());
		result.setVariableTrace(traceModel.getVariableTrace());

		if (inline) {
			CallInliner.transform((CfaNetworkInstance) problem.getModel());
		}
	}
}
