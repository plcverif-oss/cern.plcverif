/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.verif.summary.job.tests

import cern.plcverif.base.common.utils.IoUtils
import cern.plcverif.library.summaryreporter.junit.JunitSummaryReporter
import cern.plcverif.verif.summary.extensions.interfaces.ISummaryReporter
import cern.plcverif.verif.summary.job.SummaryJob
import cern.plcverif.verif.summary.job.SummaryJobSettings
import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.util.Arrays
import java.util.List
import org.junit.Assert
import org.junit.BeforeClass
import org.junit.Test
import cern.plcverif.base.common.progress.NullProgressReporter

class SummaryJobSmokeTest {
	static Path DIRECTORY;
	static Path FILE1;
	static Path FILE2;
	static final String ID1 = "verifcase1";
	static final String ID2 = "verifcase2";
	
	@BeforeClass
	def static void setup() {
		DIRECTORY = Files.createTempDirectory("plcverif-summary");
		FILE1 = Files.createTempFile(DIRECTORY, "plcverif-summary", "1.summary");
		IoUtils.writeAllContent(FILE1,
			'''
			-id = "«ID1»"
			-name = "model_name"
			-description = "model description is here"
			-sourcefiles.0 = "model.scl"
			-lf = "step7"
			-inline = "false"
			-reductions = {}
			-job = "verif"
			-info.backend_name = "NusmvBackend"
			-info.backend_algorithm = "nuxmv-Classic-dynamic-df"
			-info.requirement_text = "None of the assertions is violated."
			-result = "Satisfied"
			-result.runtime_ms = "1172"
			-result.backend_exectime_ms = "458"
			-result.cex_exists = "false"
			-req = "?"
			'''
		);
		
		FILE2 = Files.createTempFile(DIRECTORY, "plcverif-summary", "2.summary");
		IoUtils.writeAllContent(FILE2,
			'''
			-id = "«ID2»"
			-name = "model_name"
			-description = "model description is here"
			-sourcefiles.0 = "model.scl"
			-lf = "step7"
			-inline = "false"
			-reductions = {}
			-job = "verif"
			-info.backend_name = "NusmvBackend"
			-info.backend_algorithm = "nuxmv-Classic-dynamic-df"
			-info.requirement_text = "None of the assertions is violated."
			-result = "Satisfied"
			-result.runtime_ms = "1172"
			-result.backend_exectime_ms = "458"
			-result.cex_exists = "false"
			-req = "?"
			'''
		);
	}
	
	
	@Test
	def void test1() {
		smokeTest(
			Arrays.asList(FILE1.toAbsolutePath.toString, FILE2.toAbsolutePath.toString),
			Arrays.asList(new JunitSummaryReporter())
		);
	}
	
	@Test
	def void test2() {
		val pattern = DIRECTORY.toAbsolutePath.toString + File.separator + "*.summary";
		
		smokeTest(
			Arrays.asList(pattern),
			Arrays.asList(new JunitSummaryReporter())
		);
	}
	
	private def void smokeTest(List<String> summaryFiles, List<ISummaryReporter> summaryReporters){
		val settings = new SummaryJobSettings();
		settings.summaryMementoFiles = summaryFiles;
		val job = new SummaryJob(settings); 
		job.reporters = summaryReporters;
		
		val result = job.execute(NullProgressReporter.instance);
		
		Assert.assertNotNull(result);
		Assert.assertFalse(result.hasError);
		Assert.assertFalse(result.outputFilesToSave.empty);
		
		for (contentToSave : result.outputFilesToSave) {
			Assert.assertTrue(contentToSave.content.contains(ID1));	
			Assert.assertTrue(contentToSave.content.contains(ID2));
		}
	}
}