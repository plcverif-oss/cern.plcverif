/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gyula Sallai - initial API and implementation
 *******************************************************************************/
package cern.plcverif.support.codegen.generator.c;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.library.testmodels.TestModel;
import cern.plcverif.library.testmodels.TestModel.TestModelTag;
import cern.plcverif.library.testmodels.TestModelCollection;
import cern.plcverif.support.codegen.CfaToProgramTransformer;
import cern.plcverif.support.codegen.pass.EmptyElseEliminationPass;
import cern.plcverif.support.codegen.pass.UnusedLabelEliminationVisitor;
import cern.plcverif.support.codegen.program.Module;

public class FormattedCCodeGeneratorRegressionTest {
	private static class DummyCCodeGenerator extends FormattedCCodeGenerator {
		public DummyCCodeGenerator(Module module) {
			super(module);
		}

		@Override
		protected String generateMainLogic() {
			return "";
		}
	}

	@Test
	public void generatorRegressionTests() throws IOException {
		for (TestModel model : TestModelCollection.getInstance().getAllModelsWithTag(TestModelTag.SCL_TO_CFA_SUITE)) {
			System.out.println(String.format("=== %s (Formatted C code generator regression tests) ===", model.getModelId()));
			this.check(model);
		}
	}
	
	private void check(TestModel model) throws IOException {
		Module module = CfaToProgramTransformer.transform(model.getCfd());
		Assert.assertNotNull(module);		

		// Run standard clean-up passes
		new UnusedLabelEliminationVisitor().run(module);
		new EmptyElseEliminationPass().run(module);

		String actual = new DummyCCodeGenerator(module).generate().trim();
		
		String expected = IoUtils.readResourceFile(
			String.format("outputs/c/%s.c", model.getModelId()),
			FormattedCCodeGeneratorRegressionTest.class.getClassLoader()
		).trim();

		Assert.assertEquals("The generated C code does not match with expected", expected, actual);	
	}
}
