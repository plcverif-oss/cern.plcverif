#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>
 
// Struct definitions
typedef struct {
	bool boolVar1;
	bool boolVar2;
	int16_t intVar1;
	int16_t intVar2;
} __namedconst_FC1;

typedef struct {
	int16_t arr1[11];
	bool arr2[11];
	bool arr3[11];
	int16_t intVar;
} __namedconst_FC2;

typedef struct {
} __namedconst_OB1;

// Global variables
bool MX0_0;
bool MX0_1;
bool MX0_2;
__namedconst_OB1 namedconst_OB11;
__namedconst_FC1 namedconst_FC11;
__namedconst_FC2 namedconst_FC21;

// Forward declarations of the generated functions
void namedconst_OB1();
void namedconst_FC1(__namedconst_FC1 *__context);
void namedconst_FC2(__namedconst_FC2 *__context);

// Declare nondet assignment functions
bool nondet_bool(void);
uint8_t nondet_uint8_t(void);
uint16_t nondet_uint16_t(void);
uint32_t nondet_uint32_t(void);
uint64_t nondet_uint64_t(void);
int8_t nondet_int8_t(void);
int16_t nondet_int16_t(void);
int32_t nondet_int32_t(void);
int64_t nondet_int64_t(void);
double nondet_float(void);
double nondet_double(void);

// Translated functions
void namedconst_OB1() {
	// Temporary variables

	{
		// Assign inputs
		namedconst_FC1(&namedconst_FC11);
		// Assign outputs
		// Assign inputs
		namedconst_FC2(&namedconst_FC21);
		// Assign outputs
		return;
	}
}

void namedconst_FC1(__namedconst_FC1 *__context) {
	// Temporary variables

	{
		__context->intVar1 = 1;
		__context->intVar1 = (1 + __context->intVar2);
		__context->intVar1 = (1 + 2);
		__context->boolVar1 = true;
		__context->boolVar1 = (true || __context->boolVar2);
		return;
	}
}

void namedconst_FC2(__namedconst_FC2 *__context) {
	// Temporary variables

	{
		if ((__context->intVar == 1)) {
			MX0_0 = true;
		}
		else if ((((__context->intVar >= 2) && (__context->intVar <= 3)) || (__context->intVar == 10))) {
			MX0_1 = true;
		}
		else {
			MX0_2 = true;
		}
		return;
	}
}

// Entry point
int main(void) {
	// Initial values
	MX0_0 = false;
	MX0_1 = false;
	MX0_2 = false;
	namedconst_FC11.boolVar1 = false;
	namedconst_FC11.boolVar2 = false;
	namedconst_FC11.intVar1 = 0;
	namedconst_FC11.intVar2 = 0;
	namedconst_FC21.arr1[1] = 5;
	namedconst_FC21.arr1[2] = 5;
	namedconst_FC21.arr1[3] = 5;
	namedconst_FC21.arr1[4] = 5;
	namedconst_FC21.arr1[5] = 5;
	namedconst_FC21.arr1[6] = 5;
	namedconst_FC21.arr1[7] = 5;
	namedconst_FC21.arr1[8] = 5;
	namedconst_FC21.arr1[9] = 5;
	namedconst_FC21.arr1[10] = 5;
	namedconst_FC21.arr2[1] = true;
	namedconst_FC21.arr2[2] = true;
	namedconst_FC21.arr2[3] = true;
	namedconst_FC21.arr2[4] = true;
	namedconst_FC21.arr2[5] = true;
	namedconst_FC21.arr2[6] = true;
	namedconst_FC21.arr2[7] = true;
	namedconst_FC21.arr2[8] = true;
	namedconst_FC21.arr2[9] = true;
	namedconst_FC21.arr2[10] = true;
	namedconst_FC21.arr3[1] = true;
	namedconst_FC21.arr3[2] = true;
	namedconst_FC21.arr3[3] = true;
	namedconst_FC21.arr3[4] = true;
	namedconst_FC21.arr3[5] = true;
	namedconst_FC21.arr3[6] = true;
	namedconst_FC21.arr3[7] = true;
	namedconst_FC21.arr3[8] = true;
	namedconst_FC21.arr3[9] = true;
	namedconst_FC21.arr3[10] = true;
	namedconst_FC21.intVar = 0;

	// Custom entry logic

	return 0;
}