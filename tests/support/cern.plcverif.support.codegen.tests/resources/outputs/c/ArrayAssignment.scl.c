#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>
 
// Struct definitions
typedef struct {
	int16_t a[3];
	int16_t b[3];
} __pv186;

// Global variables
__pv186 instance;

// Forward declarations of the generated functions
void pv186(__pv186 *__context);

// Declare nondet assignment functions
bool nondet_bool(void);
uint8_t nondet_uint8_t(void);
uint16_t nondet_uint16_t(void);
uint32_t nondet_uint32_t(void);
uint64_t nondet_uint64_t(void);
int8_t nondet_int8_t(void);
int16_t nondet_int16_t(void);
int32_t nondet_int32_t(void);
int64_t nondet_int64_t(void);
double nondet_float(void);
double nondet_double(void);

// Translated functions
void pv186(__pv186 *__context) {
	// Temporary variables

	{
		__context->b[1] = __context->a[1];
		memmove(&(__context->b), &(__context->a), sizeof __context->b);
		assert((__context->a[1] == __context->b[1]));
		return;
	}
}

// Entry point
int main(void) {
	// Initial values
	instance.a[1] = 1;
	instance.a[2] = 2;
	instance.b[1] = 0;
	instance.b[2] = 0;

	// Custom entry logic

	return 0;
}
