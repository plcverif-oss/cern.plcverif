#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>
 
// Struct definitions
typedef struct {
	int16_t I;
} __forFunction1;

typedef struct {
	int16_t I;
} __forFunction2;

typedef struct {
	int16_t I;
} __forFunction3;

typedef struct {
	int16_t I;
} __forFunction4;

typedef struct {
	int16_t I;
} __forFunction5;

typedef struct {
} __forFunction_OB1;

// Global variables
bool MX0_1;
bool MX0_2;
bool MX0_3;
bool MX1_1;
bool MX1_2;
bool MX1_3;
bool MX2_1;
bool MX2_2;
bool MX2_3;
bool MX3_1;
bool MX3_2;
bool MX3_3;
bool MX3_4;
bool MX4_1;
bool MX4_2;
bool MX4_3;
bool MX4_4;
__forFunction1 forFunction11;
__forFunction2 forFunction21;
__forFunction3 forFunction31;
__forFunction4 forFunction41;
__forFunction5 forFunction51;
__forFunction_OB1 forFunction_OB11;

// Forward declarations of the generated functions
void forFunction1(__forFunction1 *__context);
void forFunction2(__forFunction2 *__context);
void forFunction3(__forFunction3 *__context);
void forFunction4(__forFunction4 *__context);
void forFunction5(__forFunction5 *__context);
void forFunction_OB1();

// Declare nondet assignment functions
bool nondet_bool(void);
uint8_t nondet_uint8_t(void);
uint16_t nondet_uint16_t(void);
uint32_t nondet_uint32_t(void);
uint64_t nondet_uint64_t(void);
int8_t nondet_int8_t(void);
int16_t nondet_int16_t(void);
int32_t nondet_int32_t(void);
int64_t nondet_int64_t(void);
double nondet_float(void);
double nondet_double(void);

// Translated functions
void forFunction1(__forFunction1 *__context) {
	// Temporary variables

	{
		MX0_1 = true;
		for (__context->I = 1; (__context->I <= 27); __context->I = (__context->I + 1)) {
			MX0_2 = true;
		}
		MX0_3 = true;
		return;
	}
}

void forFunction2(__forFunction2 *__context) {
	// Temporary variables

	{
		MX1_1 = true;
		for (__context->I = 1; (__context->I <= 27); __context->I = (__context->I + 3)) {
			MX1_2 = true;
		}
		MX1_3 = true;
		return;
	}
}

void forFunction3(__forFunction3 *__context) {
	// Temporary variables

	{
		MX2_1 = true;
		for (__context->I = 27; (__context->I >= 1); __context->I = (__context->I + (- 2))) {
			MX2_2 = true;
		}
		MX2_3 = true;
		return;
	}
}

void forFunction4(__forFunction4 *__context) {
	// Temporary variables

	{
		MX3_1 = true;
		for (__context->I = 1; (__context->I <= 10); __context->I = (__context->I + 1)) {
			MX3_2 = true;
			if (((__context->I / 2) == 0)) {
				continue;
			}
			MX3_3 = true;
			goto forFunction4_l8;
			forFunction4_l8: ;
		}
		MX3_4 = true;
		return;
	}
}

void forFunction5(__forFunction5 *__context) {
	// Temporary variables

	{
		MX4_1 = true;
		for (__context->I = 1; (__context->I <= 10); __context->I = (__context->I + 1)) {
			MX4_2 = true;
			if (((__context->I / 2) == 0)) {
				break;
			}
			MX4_3 = true;
		}
		MX4_4 = true;
		return;
	}
}

void forFunction_OB1() {
	// Temporary variables

	{
		// Assign inputs
		forFunction1(&forFunction11);
		// Assign outputs
		// Assign inputs
		forFunction2(&forFunction21);
		// Assign outputs
		// Assign inputs
		forFunction3(&forFunction31);
		// Assign outputs
		// Assign inputs
		forFunction4(&forFunction41);
		// Assign outputs
		// Assign inputs
		forFunction5(&forFunction51);
		// Assign outputs
		return;
	}
}

// Entry point
int main(void) {
	// Initial values
	MX0_1 = false;
	MX0_2 = false;
	MX0_3 = false;
	MX1_1 = false;
	MX1_2 = false;
	MX1_3 = false;
	MX2_1 = false;
	MX2_2 = false;
	MX2_3 = false;
	MX3_1 = false;
	MX3_2 = false;
	MX3_3 = false;
	MX3_4 = false;
	MX4_1 = false;
	MX4_2 = false;
	MX4_3 = false;
	MX4_4 = false;
	forFunction11.I = 0;
	forFunction21.I = 0;
	forFunction31.I = 0;
	forFunction41.I = 0;
	forFunction51.I = 0;

	// Custom entry logic

	return 0;
}