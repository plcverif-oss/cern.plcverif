#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>
 
// Struct definitions
typedef struct {
} __returnFunction1;

// Global variables
bool MX0_0;
bool MX0_1;
__returnFunction1 returnFunction11;

// Forward declarations of the generated functions
void returnFunction1();

// Declare nondet assignment functions
bool nondet_bool(void);
uint8_t nondet_uint8_t(void);
uint16_t nondet_uint16_t(void);
uint32_t nondet_uint32_t(void);
uint64_t nondet_uint64_t(void);
int8_t nondet_int8_t(void);
int16_t nondet_int16_t(void);
int32_t nondet_int32_t(void);
int64_t nondet_int64_t(void);
double nondet_float(void);
double nondet_double(void);

// Translated functions
void returnFunction1() {
	// Temporary variables

	{
		MX0_0 = true;
		return;
	}
}

// Entry point
int main(void) {
	// Initial values
	MX0_0 = false;
	MX0_1 = false;

	// Custom entry logic

	return 0;
}