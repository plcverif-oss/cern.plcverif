#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>
 
// Struct definitions
typedef struct {
	bool in1;
	bool in2;
	int16_t out;
} __simple_cex_fb1;

// Global variables
__simple_cex_fb1 instance;

// Forward declarations of the generated functions
void simple_cex_fb1(__simple_cex_fb1 *__context);

// Declare nondet assignment functions
bool nondet_bool(void);
uint8_t nondet_uint8_t(void);
uint16_t nondet_uint16_t(void);
uint32_t nondet_uint32_t(void);
uint64_t nondet_uint64_t(void);
int8_t nondet_int8_t(void);
int16_t nondet_int16_t(void);
int32_t nondet_int32_t(void);
int64_t nondet_int64_t(void);
double nondet_float(void);
double nondet_double(void);

// Translated functions
void simple_cex_fb1(__simple_cex_fb1 *__context) {
	// Temporary variables

	{
		if ((__context->in1 && (! __context->in2))) {
			__context->out = (__context->out + 1);
		}
		assert((__context->out < 3));
		return;
	}
}

// Entry point
int main(void) {
	// Initial values
	instance.in1 = false;
	instance.in2 = false;
	instance.out = 0;

	// Custom entry logic

	return 0;
}