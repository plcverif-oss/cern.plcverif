#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>
 
// Struct definitions
typedef struct {
	bool x;
	bool y;
} __anonymous_type;

typedef struct {
	bool a;
} __anonymous_type1;

typedef struct {
	bool b;
	bool c;
} __anonymous_type2;

typedef struct {
	bool d;
	bool e;
} __anonymous_type4;

typedef struct {
	__anonymous_type4 s3a;
} __anonymous_type3;

typedef struct {
	bool s4arr[11];
} __anonymous_type41;

typedef struct {
} __compound_OB1;

typedef struct {
	int16_t c1;
	bool c2;
} __compound_udt1;

typedef struct {
	bool in1;
	int16_t in2;
	bool arr1[4];
	int16_t[8] arr2[4];
	__anonymous_type arr3[11];
	__compound_udt1 arr4[11];
	__anonymous_type1 s1;
	__anonymous_type2 s2;
	__anonymous_type3 s3;
	__anonymous_type41 s4;
	__compound_udt1 s5;
} __compound_fb1;

// Global variables
__compound_OB1 compound_OB11;
__compound_fb1 compound_db1;
__compound_udt1 compound_db2;
__compound_fb1 compound_db3;
__compound_fb1 compound_db1_inlined_1;
__compound_fb1 compound_db3_inlined_2;

// Forward declarations of the generated functions
void compound_OB1();
void compound_fb1(__compound_fb1 *__context);

// Declare nondet assignment functions
bool nondet_bool(void);
uint8_t nondet_uint8_t(void);
uint16_t nondet_uint16_t(void);
uint32_t nondet_uint32_t(void);
uint64_t nondet_uint64_t(void);
int8_t nondet_int8_t(void);
int16_t nondet_int16_t(void);
int32_t nondet_int32_t(void);
int64_t nondet_int64_t(void);
double nondet_float(void);
double nondet_double(void);

// Translated functions
void compound_OB1() {
	// Temporary variables

	{
		// Assign inputs
		compound_fb1(&compound_db1_inlined_1);
		// Assign outputs
		// Assign inputs
		compound_fb1(&compound_db3_inlined_2);
		// Assign outputs
		return;
	}
}

void compound_fb1(__compound_fb1 *__context) {
	// Temporary variables

	{
		__context->arr4[8].c1 = __context->in2;
		return;
	}
}

// Entry point
int main(void) {
	// Initial values
	compound_db1.in1 = false;
	compound_db1.in2 = 0;
	compound_db1.arr1[1] = false;
	compound_db1.arr1[2] = false;
	compound_db1.arr1[3] = false;
	compound_db1.arr2[1][5] = 0;
	compound_db1.arr2[1][6] = 0;
	compound_db1.arr2[1][7] = 0;
	compound_db1.arr2[2][5] = 0;
	compound_db1.arr2[2][6] = 0;
	compound_db1.arr2[2][7] = 0;
	compound_db1.arr2[3][5] = 0;
	compound_db1.arr2[3][6] = 0;
	compound_db1.arr2[3][7] = 0;
	compound_db1.s1.a = false;
	compound_db1.s2.b = false;
	compound_db1.s2.c = false;
	compound_db1.s3.s3a.d = false;
	compound_db1.s3.s3a.e = false;
	compound_db1.s4.s4arr[1] = false;
	compound_db1.s4.s4arr[2] = false;
	compound_db1.s4.s4arr[3] = false;
	compound_db1.s4.s4arr[4] = false;
	compound_db1.s4.s4arr[5] = false;
	compound_db1.s4.s4arr[6] = false;
	compound_db1.s4.s4arr[7] = false;
	compound_db1.s4.s4arr[8] = false;
	compound_db1.s4.s4arr[9] = false;
	compound_db1.s4.s4arr[10] = false;
	compound_db1.s5.c1 = 0;
	compound_db1.s5.c2 = false;
	compound_db2.c1 = 333;
	compound_db2.c2 = true;
	compound_db3.in2 = 987;
	compound_db3.in1 = false;
	compound_db3.arr1[1] = false;
	compound_db3.arr1[2] = false;
	compound_db3.arr1[3] = false;
	compound_db3.arr2[1][5] = 0;
	compound_db3.arr2[1][6] = 0;
	compound_db3.arr2[1][7] = 0;
	compound_db3.arr2[2][5] = 0;
	compound_db3.arr2[2][6] = 0;
	compound_db3.arr2[2][7] = 0;
	compound_db3.arr2[3][5] = 0;
	compound_db3.arr2[3][6] = 0;
	compound_db3.arr2[3][7] = 0;
	compound_db3.s1.a = false;
	compound_db3.s2.b = false;
	compound_db3.s2.c = false;
	compound_db3.s3.s3a.d = false;
	compound_db3.s3.s3a.e = false;
	compound_db3.s4.s4arr[1] = false;
	compound_db3.s4.s4arr[2] = false;
	compound_db3.s4.s4arr[3] = false;
	compound_db3.s4.s4arr[4] = false;
	compound_db3.s4.s4arr[5] = false;
	compound_db3.s4.s4arr[6] = false;
	compound_db3.s4.s4arr[7] = false;
	compound_db3.s4.s4arr[8] = false;
	compound_db3.s4.s4arr[9] = false;
	compound_db3.s4.s4arr[10] = false;
	compound_db3.s5.c1 = 0;
	compound_db3.s5.c2 = false;
	compound_db1_inlined_1.in1 = false;
	compound_db1_inlined_1.in2 = 0;
	compound_db1_inlined_1.arr1[1] = false;
	compound_db1_inlined_1.arr1[2] = false;
	compound_db1_inlined_1.arr1[3] = false;
	compound_db1_inlined_1.arr2[1][5] = 0;
	compound_db1_inlined_1.arr2[1][6] = 0;
	compound_db1_inlined_1.arr2[1][7] = 0;
	compound_db1_inlined_1.arr2[2][5] = 0;
	compound_db1_inlined_1.arr2[2][6] = 0;
	compound_db1_inlined_1.arr2[2][7] = 0;
	compound_db1_inlined_1.arr2[3][5] = 0;
	compound_db1_inlined_1.arr2[3][6] = 0;
	compound_db1_inlined_1.arr2[3][7] = 0;
	compound_db1_inlined_1.s1.a = false;
	compound_db1_inlined_1.s2.b = false;
	compound_db1_inlined_1.s2.c = false;
	compound_db1_inlined_1.s3.s3a.d = false;
	compound_db1_inlined_1.s3.s3a.e = false;
	compound_db1_inlined_1.s4.s4arr[1] = false;
	compound_db1_inlined_1.s4.s4arr[2] = false;
	compound_db1_inlined_1.s4.s4arr[3] = false;
	compound_db1_inlined_1.s4.s4arr[4] = false;
	compound_db1_inlined_1.s4.s4arr[5] = false;
	compound_db1_inlined_1.s4.s4arr[6] = false;
	compound_db1_inlined_1.s4.s4arr[7] = false;
	compound_db1_inlined_1.s4.s4arr[8] = false;
	compound_db1_inlined_1.s4.s4arr[9] = false;
	compound_db1_inlined_1.s4.s4arr[10] = false;
	compound_db1_inlined_1.s5.c1 = 0;
	compound_db1_inlined_1.s5.c2 = false;
	compound_db3_inlined_2.in2 = 987;
	compound_db3_inlined_2.in1 = false;
	compound_db3_inlined_2.arr1[1] = false;
	compound_db3_inlined_2.arr1[2] = false;
	compound_db3_inlined_2.arr1[3] = false;
	compound_db3_inlined_2.arr2[1][5] = 0;
	compound_db3_inlined_2.arr2[1][6] = 0;
	compound_db3_inlined_2.arr2[1][7] = 0;
	compound_db3_inlined_2.arr2[2][5] = 0;
	compound_db3_inlined_2.arr2[2][6] = 0;
	compound_db3_inlined_2.arr2[2][7] = 0;
	compound_db3_inlined_2.arr2[3][5] = 0;
	compound_db3_inlined_2.arr2[3][6] = 0;
	compound_db3_inlined_2.arr2[3][7] = 0;
	compound_db3_inlined_2.s1.a = false;
	compound_db3_inlined_2.s2.b = false;
	compound_db3_inlined_2.s2.c = false;
	compound_db3_inlined_2.s3.s3a.d = false;
	compound_db3_inlined_2.s3.s3a.e = false;
	compound_db3_inlined_2.s4.s4arr[1] = false;
	compound_db3_inlined_2.s4.s4arr[2] = false;
	compound_db3_inlined_2.s4.s4arr[3] = false;
	compound_db3_inlined_2.s4.s4arr[4] = false;
	compound_db3_inlined_2.s4.s4arr[5] = false;
	compound_db3_inlined_2.s4.s4arr[6] = false;
	compound_db3_inlined_2.s4.s4arr[7] = false;
	compound_db3_inlined_2.s4.s4arr[8] = false;
	compound_db3_inlined_2.s4.s4arr[9] = false;
	compound_db3_inlined_2.s4.s4arr[10] = false;
	compound_db3_inlined_2.s5.c1 = 0;
	compound_db3_inlined_2.s5.c2 = false;

	// Custom entry logic

	return 0;
}