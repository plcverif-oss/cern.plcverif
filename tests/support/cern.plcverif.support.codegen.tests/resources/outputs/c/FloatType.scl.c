#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>
 
// Struct definitions
typedef struct {
	float real1;
	float real2;
	int16_t int1;
} __float_fc1;

// Global variables
__float_fc1 float_fc11;

// Forward declarations of the generated functions
void float_fc1(__float_fc1 *__context);

// Declare nondet assignment functions
bool nondet_bool(void);
uint8_t nondet_uint8_t(void);
uint16_t nondet_uint16_t(void);
uint32_t nondet_uint32_t(void);
uint64_t nondet_uint64_t(void);
int8_t nondet_int8_t(void);
int16_t nondet_int16_t(void);
int32_t nondet_int32_t(void);
int64_t nondet_int64_t(void);
double nondet_float(void);
double nondet_double(void);

// Translated functions
void float_fc1(__float_fc1 *__context) {
	// Temporary variables

	{
		__context->real1 = 12.3;
		__context->real1 = 23.0;
		__context->real1 = 34.0;
		__context->real1 = (45.6 + 7.0);
		__context->real1 = (__context->real1 + 8.0);
		__context->real1 = (91.2 + 92.3);
		__context->real1 = (__context->real1 + 92.4);
		__context->real1 = (__context->real1 + __context->real2);
		__context->real1 = (51.1 * 3.0);
		__context->real1 = (__context->real1 * 4.0);
		__context->real1 = (52.1 * 53.1);
		__context->real1 = (__context->real1 * 54.1);
		__context->real1 = (__context->real1 * __context->real2);
		__context->real1 = (60.1 / 20.3);
		__context->real1 = (__context->real1 / 20.4);
		__context->real1 = (__context->real1 / __context->real2);
		__context->real1 = (12.6 / 2.0);
		__context->real1 = (__context->real2 / 3.0);
		__context->real1 = (30.0 / 2.5);
		__context->real1 = (31.0 / __context->real2);
		__context->int1 = ((int16_t) __context->real1);
		__context->int1 = (((int16_t) __context->real1) + 1);
		__context->int1 = (((int16_t) __context->real1) * 2);
		__context->int1 = (((int16_t) __context->real1) * ((int16_t) __context->real2));
		__context->real1 = (((float) ((int16_t) __context->real2)) * 0.1);
		return;
	}
}

// Entry point
int main(void) {
	// Initial values
	float_fc11.real1 = 0.0;
	float_fc11.real2 = 0.0;
	float_fc11.int1 = 0;

	// Custom entry logic

	return 0;
}