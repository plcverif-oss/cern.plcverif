#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>
 
// Struct definitions
typedef struct {
	bool b;
	bool w;
} __call_FB5;

typedef struct {
	bool a;
	bool q;
	__call_FB5 c;
} __call_FB4;

typedef struct {
} __call_FC1;

typedef struct {
	bool in1;
} __call_FC2;

typedef struct {
	bool in1;
	int16_t in2;
	int16_t RET_VAL;
} __call_FC3;

typedef struct {
} __call_OB1;

// Global variables
bool IX0_0;
bool MX0_0;
bool MX1_0;
bool MX2_0;
bool MX3_0;
bool MX3_1;
bool MX3_2;
bool MX4_0;
__call_OB1 call_OB11;
__call_FC1 call_FC11;
__call_FC2 call_FC21;
__call_FC3 call_FC31;
__call_FB4 call_DB4;

// Forward declarations of the generated functions
void call_OB1(__call_OB1 *__context);
void call_FC1();
void call_FC2(__call_FC2 *__context);
void call_FC3(__call_FC3 *__context);
void call_FB4(__call_FB4 *__context);
void call_FB5(__call_FB5 *__context);

// Declare nondet assignment functions
bool nondet_bool(void);
uint8_t nondet_uint8_t(void);
uint16_t nondet_uint16_t(void);
uint32_t nondet_uint32_t(void);
uint64_t nondet_uint64_t(void);
int8_t nondet_int8_t(void);
int16_t nondet_int16_t(void);
int32_t nondet_int32_t(void);
int64_t nondet_int64_t(void);
double nondet_float(void);
double nondet_double(void);

// Translated functions
void call_OB1(__call_OB1 *__context) {
	// Temporary variables
	int16_t r;

	{
		r = 0;
		call_FC1();
		// Assign inputs
		call_FC21.in1 = true;
		call_FC2(&call_FC21);
		// Assign outputs
		// Assign inputs
		call_FC31.in1 = IX0_0;
		call_FC31.in2 = 100;
		call_FC3(&call_FC31);
		// Assign outputs
		r = call_FC31.RET_VAL;
		call_DB4.a = true;
		// Assign inputs
		call_FB4(&call_DB4);
		// Assign outputs
		MX0_0 = call_DB4.q;
		return;
	}
}

void call_FC1() {
	// Temporary variables

	{
		MX1_0 = true;
		return;
	}
}

void call_FC2(__call_FC2 *__context) {
	// Temporary variables

	{
		MX2_0 = (! __context->in1);
		return;
	}
}

void call_FC3(__call_FC3 *__context) {
	// Temporary variables

	{
		if ((__context->in2 > 100)) {
			MX3_0 = true;
		}
		else {
			MX3_1 = false;
		}
		MX3_2 = true;
		__context->RET_VAL = __context->in2;
		return;
	}
}

void call_FB4(__call_FB4 *__context) {
	// Temporary variables

	{
		__context->q = (! __context->a);
		__context->c.b = __context->a;
		// Assign inputs
		call_FB5(&__context->c);
		// Assign outputs
		MX4_0 = __context->c.w;
		return;
	}
}

void call_FB5(__call_FB5 *__context) {
	// Temporary variables

	{
		__context->w = __context->b;
		return;
	}
}

// Entry point
int main(void) {
	// Initial values
	IX0_0 = false;
	MX0_0 = false;
	MX1_0 = false;
	MX2_0 = false;
	MX3_0 = false;
	MX3_1 = false;
	MX3_2 = false;
	MX4_0 = false;
	call_FC21.in1 = false;
	call_FC31.in1 = false;
	call_FC31.in2 = 0;
	call_FC31.RET_VAL = 0;
	call_DB4.a = false;
	call_DB4.q = false;
	call_DB4.c.b = false;
	call_DB4.c.w = false;

	// Custom entry logic

	return 0;
}