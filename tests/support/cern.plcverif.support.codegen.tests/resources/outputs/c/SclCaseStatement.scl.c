#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>
 
// Struct definitions
typedef struct {
	int16_t I;
} __caseFunction1;

typedef struct {
	int16_t I;
} __caseFunction2;

typedef struct {
	int16_t I;
} __caseFunction3;

typedef struct {
	int16_t I;
} __caseFunction4;

typedef struct {
	int16_t I;
} __caseFunction5;

typedef struct {
} __caseFunction_OB1;

// Global variables
bool MX1_0;
bool MX1_1;
bool MX1_2;
bool MX1_3;
bool MX1_4;
bool MX1_5;
bool MX2_0;
bool MX2_1;
bool MX2_2;
bool MX2_3;
bool MX2_4;
bool MX2_5;
bool MX3_0;
bool MX3_1;
bool MX3_2;
bool MX3_3;
bool MX4_0;
bool MX4_1;
bool MX4_2;
bool MX4_3;
bool MX5_0;
bool MX5_1;
bool MX5_2;
bool MX5_3;
__caseFunction1 caseFunction11;
__caseFunction2 caseFunction21;
__caseFunction3 caseFunction31;
__caseFunction4 caseFunction41;
__caseFunction5 caseFunction51;
__caseFunction_OB1 caseFunction_OB11;

// Forward declarations of the generated functions
void caseFunction1(__caseFunction1 *__context);
void caseFunction2(__caseFunction2 *__context);
void caseFunction3(__caseFunction3 *__context);
void caseFunction4(__caseFunction4 *__context);
void caseFunction5(__caseFunction5 *__context);
void caseFunction_OB1();

// Declare nondet assignment functions
bool nondet_bool(void);
uint8_t nondet_uint8_t(void);
uint16_t nondet_uint16_t(void);
uint32_t nondet_uint32_t(void);
uint64_t nondet_uint64_t(void);
int8_t nondet_int8_t(void);
int16_t nondet_int16_t(void);
int32_t nondet_int32_t(void);
int64_t nondet_int64_t(void);
double nondet_float(void);
double nondet_double(void);

// Translated functions
void caseFunction1(__caseFunction1 *__context) {
	// Temporary variables

	{
		MX1_0 = true;
		if ((__context->I == 1)) {
			MX1_1 = false;
		}
		else if ((__context->I == 2)) {
			MX1_2 = false;
		}
		else if ((__context->I == 3)) {
			MX1_3 = false;
		}
		else {
			MX1_4 = false;
		}
		MX1_5 = true;
		return;
	}
}

void caseFunction2(__caseFunction2 *__context) {
	// Temporary variables

	{
		MX2_0 = true;
		if ((((__context->I * 2) >= 1) && ((__context->I * 2) <= 3))) {
			MX2_1 = false;
		}
		else if ((((__context->I * 2) >= 2) && ((__context->I * 2) <= 4))) {
			MX2_2 = false;
		}
		else if ((((__context->I * 2) == 6) || (((__context->I * 2) == 8) || ((__context->I * 2) == 10)))) {
			MX2_3 = false;
		}
		else if ((((__context->I * 2) >= 11) && ((__context->I * 2) <= 13))) {
			MX2_4 = false;
		}
		else if ((((__context->I * 2) >= 12) && ((__context->I * 2) <= 14))) {
			MX2_5 = false;
		}
		else {
			MX2_4 = false;
		}
		MX2_5 = true;
		return;
	}
}

void caseFunction3(__caseFunction3 *__context) {
	// Temporary variables

	{
		MX3_0 = true;
		if ((__context->I == 1)) {
			MX3_1 = false;
		}
		else if (((__context->I == 2) || ((__context->I == 3) || ((__context->I == 5) || (__context->I == 7))))) {
			MX3_2 = false;
		}
		MX3_3 = true;
		return;
	}
}

void caseFunction4(__caseFunction4 *__context) {
	// Temporary variables

	{
		MX4_0 = true;
		if (((((int32_t) __context->I) * 2) == 1)) {
			MX4_1 = false;
		}
		else if ((((((int32_t) __context->I) * 2) == 2) || (((((int32_t) __context->I) * 2) == 3) || (((((int32_t) __context->I) * 2) == 5) || ((((int32_t) __context->I) * 2) == 7))))) {
			MX4_2 = false;
		}
		MX4_3 = true;
		return;
	}
}

void caseFunction5(__caseFunction5 *__context) {
	// Temporary variables

	{
		MX5_0 = true;
		if ((((__context->I * 2) >= 1) && ((__context->I * 2) <= 5))) {
			MX5_1 = false;
		}
		else if ((((__context->I * 2) == 6) || (((__context->I * 2) >= 8) && ((__context->I * 2) <= 10)))) {
			MX5_2 = false;
		}
		MX5_3 = true;
		return;
	}
}

void caseFunction_OB1() {
	// Temporary variables

	{
		// Assign inputs
		caseFunction11.I = 1;
		caseFunction1(&caseFunction11);
		// Assign outputs
		// Assign inputs
		caseFunction21.I = 1;
		caseFunction2(&caseFunction21);
		// Assign outputs
		// Assign inputs
		caseFunction31.I = 1;
		caseFunction3(&caseFunction31);
		// Assign outputs
		// Assign inputs
		caseFunction41.I = 1;
		caseFunction4(&caseFunction41);
		// Assign outputs
		return;
	}
}

// Entry point
int main(void) {
	// Initial values
	MX1_0 = false;
	MX1_1 = false;
	MX1_2 = false;
	MX1_3 = false;
	MX1_4 = false;
	MX1_5 = false;
	MX2_0 = false;
	MX2_1 = false;
	MX2_2 = false;
	MX2_3 = false;
	MX2_4 = false;
	MX2_5 = false;
	MX3_0 = false;
	MX3_1 = false;
	MX3_2 = false;
	MX3_3 = false;
	MX4_0 = false;
	MX4_1 = false;
	MX4_2 = false;
	MX4_3 = false;
	MX5_0 = false;
	MX5_1 = false;
	MX5_2 = false;
	MX5_3 = false;
	caseFunction11.I = 0;
	caseFunction21.I = 0;
	caseFunction31.I = 0;
	caseFunction41.I = 0;
	caseFunction51.I = 0;

	// Custom entry logic

	return 0;
}