#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>
 
// Struct definitions
typedef struct {
	int16_t in_int;
	int32_t in_dint;
	float in_real;
	int16_t out_int;
	int32_t out_dint;
	float out_real;
} __typeconv_fb1;

// Global variables
__typeconv_fb1 typeconv_db1;

// Forward declarations of the generated functions
void typeconv_fb1(__typeconv_fb1 *__context);

// Declare nondet assignment functions
bool nondet_bool(void);
uint8_t nondet_uint8_t(void);
uint16_t nondet_uint16_t(void);
uint32_t nondet_uint32_t(void);
uint64_t nondet_uint64_t(void);
int8_t nondet_int8_t(void);
int16_t nondet_int16_t(void);
int32_t nondet_int32_t(void);
int64_t nondet_int64_t(void);
double nondet_float(void);
double nondet_double(void);

// Translated functions
void typeconv_fb1(__typeconv_fb1 *__context) {
	// Temporary variables

	{
		__context->out_int = __context->in_int;
		__context->out_dint = __context->in_dint;
		__context->out_real = __context->in_real;
		__context->out_real = ((float) __context->in_int);
		__context->out_dint = ((int32_t) __context->in_int);
		__context->out_int = ((int16_t) __context->in_dint);
		__context->out_int = ((int16_t) __context->in_real);
		__context->out_dint = ((int32_t) __context->in_real);
		__context->out_dint = 56;
		__context->out_dint = 567;
		__context->out_dint = 567;
		__context->out_dint = 567;
		__context->out_dint = (((int32_t) __context->in_int) * 10);
		return;
	}
}

// Entry point
int main(void) {
	// Initial values
	typeconv_db1.in_int = 0;
	typeconv_db1.in_dint = 0;
	typeconv_db1.in_real = 0.0;
	typeconv_db1.out_int = 0;
	typeconv_db1.out_dint = 0;
	typeconv_db1.out_real = 0.0;

	// Custom entry logic

	return 0;
}