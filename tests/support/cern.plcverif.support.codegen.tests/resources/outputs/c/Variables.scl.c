#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>
 
// Struct definitions
typedef struct {
	bool s1;
	int32_t s2;
} __anonymous_type;

typedef struct {
} __variables_OB1;

typedef struct {
	bool a1;
	bool a2;
	bool a3;
	int16_t b1;
	int16_t b2;
	int16_t b3;
	int16_t b4;
} __variables_fb1;

typedef struct {
	bool arr1[8];
	bool[4] arr2[8];
	int16_t arr3[8];
	__anonymous_type arr4[5];
} __variables_fb2;

// Global variables
__variables_fb1 variables_db1;
__variables_fb2 variables_db2;
__variables_OB1 variables_OB11;

// Forward declarations of the generated functions
void variables_fb1(__variables_fb1 *__context);
void variables_fb2(__variables_fb2 *__context);
void variables_OB1();

// Declare nondet assignment functions
bool nondet_bool(void);
uint8_t nondet_uint8_t(void);
uint16_t nondet_uint16_t(void);
uint32_t nondet_uint32_t(void);
uint64_t nondet_uint64_t(void);
int8_t nondet_int8_t(void);
int16_t nondet_int16_t(void);
int32_t nondet_int32_t(void);
int64_t nondet_int64_t(void);
double nondet_float(void);
double nondet_double(void);

// Translated functions
void variables_fb1(__variables_fb1 *__context) {
	// Temporary variables

	{
		return;
	}
}

void variables_fb2(__variables_fb2 *__context) {
	// Temporary variables

	{
		return;
	}
}

void variables_OB1() {
	// Temporary variables

	{
		// Assign inputs
		variables_fb1(&variables_db1);
		// Assign outputs
		// Assign inputs
		variables_fb2(&variables_db2);
		// Assign outputs
		return;
	}
}

// Entry point
int main(void) {
	// Initial values
	variables_db1.a1 = false;
	variables_db1.a2 = false;
	variables_db1.a3 = true;
	variables_db1.b1 = 0;
	variables_db1.b2 = 0;
	variables_db1.b3 = 123;
	variables_db1.b4 = -234;
	variables_db2.arr1[0] = false;
	variables_db2.arr1[1] = false;
	variables_db2.arr1[2] = false;
	variables_db2.arr1[3] = false;
	variables_db2.arr1[4] = false;
	variables_db2.arr1[5] = false;
	variables_db2.arr1[6] = false;
	variables_db2.arr1[7] = false;
	variables_db2.arr2[0][1] = false;
	variables_db2.arr2[0][2] = false;
	variables_db2.arr2[0][3] = false;
	variables_db2.arr2[1][1] = false;
	variables_db2.arr2[1][2] = false;
	variables_db2.arr2[1][3] = false;
	variables_db2.arr2[2][1] = false;
	variables_db2.arr2[2][2] = false;
	variables_db2.arr2[2][3] = false;
	variables_db2.arr2[3][1] = false;
	variables_db2.arr2[3][2] = false;
	variables_db2.arr2[3][3] = false;
	variables_db2.arr2[4][1] = false;
	variables_db2.arr2[4][2] = false;
	variables_db2.arr2[4][3] = false;
	variables_db2.arr2[5][1] = false;
	variables_db2.arr2[5][2] = false;
	variables_db2.arr2[5][3] = false;
	variables_db2.arr2[6][1] = false;
	variables_db2.arr2[6][2] = false;
	variables_db2.arr2[6][3] = false;
	variables_db2.arr2[7][1] = false;
	variables_db2.arr2[7][2] = false;
	variables_db2.arr2[7][3] = false;
	variables_db2.arr3[0] = 0;
	variables_db2.arr3[1] = 0;
	variables_db2.arr3[2] = 0;
	variables_db2.arr3[3] = 0;
	variables_db2.arr3[4] = 0;
	variables_db2.arr3[5] = 0;
	variables_db2.arr3[6] = 0;
	variables_db2.arr3[7] = 0;

	// Custom entry logic

	return 0;
}