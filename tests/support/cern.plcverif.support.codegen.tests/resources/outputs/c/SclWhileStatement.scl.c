#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>
 
// Struct definitions
typedef struct {
} __whileFunction1;

typedef struct {
} __whileFunction1_continue;

typedef struct {
} __whileFunction1_exit;

typedef struct {
} __whileFunction_OB1;

// Global variables
bool MX0_1;
bool MX0_2;
bool MX0_3;
bool MX0_4;
bool MX0_5;
bool MX1_0;
bool MX1_1;
bool MX1_2;
bool MX1_3;
bool MX1_4;
bool MX1_5;
bool MX1_6;
bool MX2_0;
bool MX2_1;
bool MX2_2;
bool MX2_3;
bool MX2_4;
bool MX2_5;
bool MX2_6;
__whileFunction1 whileFunction11;
__whileFunction1_exit whileFunction1_exit1;
__whileFunction1_continue whileFunction1_continue1;
__whileFunction_OB1 whileFunction_OB11;

// Forward declarations of the generated functions
void whileFunction1();
void whileFunction1_exit();
void whileFunction1_continue();
void whileFunction_OB1();

// Declare nondet assignment functions
bool nondet_bool(void);
uint8_t nondet_uint8_t(void);
uint16_t nondet_uint16_t(void);
uint32_t nondet_uint32_t(void);
uint64_t nondet_uint64_t(void);
int8_t nondet_int8_t(void);
int16_t nondet_int16_t(void);
int32_t nondet_int32_t(void);
int64_t nondet_int64_t(void);
double nondet_float(void);
double nondet_double(void);

// Translated functions
void whileFunction1() {
	// Temporary variables

	{
		MX0_1 = true;
		while (MX0_2) {
			MX0_3 = true;
			MX0_4 = true;
		}
		MX0_5 = true;
		return;
	}
}

void whileFunction1_exit() {
	// Temporary variables

	{
		MX1_0 = true;
		while (MX1_1) {
			MX1_2 = true;
			if (MX1_3) {
				MX1_4 = true;
				break;
			}
			MX1_5 = true;
		}
		MX1_6 = true;
		return;
	}
}

void whileFunction1_continue() {
	// Temporary variables

	{
		MX2_0 = true;
		while (MX2_1) {
			MX2_2 = true;
			if (MX2_3) {
				MX2_4 = true;
				continue;
			}
			MX2_5 = true;
		}
		MX2_6 = true;
		return;
	}
}

void whileFunction_OB1() {
	// Temporary variables

	{
		whileFunction1();
		whileFunction1_exit();
		whileFunction1_continue();
		return;
	}
}

// Entry point
int main(void) {
	// Initial values
	MX0_1 = false;
	MX0_2 = false;
	MX0_3 = false;
	MX0_4 = false;
	MX0_5 = false;
	MX1_0 = false;
	MX1_1 = false;
	MX1_2 = false;
	MX1_3 = false;
	MX1_4 = false;
	MX1_5 = false;
	MX1_6 = false;
	MX2_0 = false;
	MX2_1 = false;
	MX2_2 = false;
	MX2_3 = false;
	MX2_4 = false;
	MX2_5 = false;
	MX2_6 = false;

	// Custom entry logic

	return 0;
}