#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>
 
// Struct definitions
typedef struct {
} __assertFunction1;

typedef struct {
} __assertFunction2;

typedef struct {
} __assertFunction3;

typedef struct {
} __assertFunction4;

typedef struct {
} __assertFunction_OB1;

// Global variables
bool MX1_1;
bool IX1_2;
bool MX1_3;
bool MX2_1;
bool IX2_2;
bool IX2_3;
bool MX2_4;
bool MX3_1;
bool IX3_2;
bool IX3_3;
bool IX3_4;
bool MX3_4;
bool MX4_1;
bool IX4_2;
bool IX4_3;
bool IX4_4;
bool MX4_5;
__assertFunction1 assertFunction11;
__assertFunction2 assertFunction21;
__assertFunction3 assertFunction31;
__assertFunction4 assertFunction41;
__assertFunction_OB1 assertFunction_OB11;

// Forward declarations of the generated functions
void assertFunction1();
void assertFunction2();
void assertFunction3();
void assertFunction4();
void assertFunction_OB1();

// Declare nondet assignment functions
bool nondet_bool(void);
uint8_t nondet_uint8_t(void);
uint16_t nondet_uint16_t(void);
uint32_t nondet_uint32_t(void);
uint64_t nondet_uint64_t(void);
int8_t nondet_int8_t(void);
int16_t nondet_int16_t(void);
int32_t nondet_int32_t(void);
int64_t nondet_int64_t(void);
double nondet_float(void);
double nondet_double(void);

// Translated functions
void assertFunction1() {
	// Temporary variables

	{
		MX1_1 = true;
		assert(IX1_2);
		MX1_3 = true;
		return;
	}
}

void assertFunction2() {
	// Temporary variables

	{
		MX2_1 = true;
		assert((IX2_2 || IX2_3));
		MX2_4 = true;
		return;
	}
}

void assertFunction3() {
	// Temporary variables

	{
		MX3_1 = true;
		assert(((IX3_2 || IX3_3) && IX3_4));
		MX3_4 = true;
		return;
	}
}

void assertFunction4() {
	// Temporary variables

	{
		MX4_1 = true;
		assert((!(IX4_2) || (!(IX4_3) || IX4_4)));
		MX4_5 = true;
		return;
	}
}

void assertFunction_OB1() {
	// Temporary variables

	{
		assertFunction1();
		assertFunction2();
		assertFunction3();
		assertFunction4();
		return;
	}
}

// Entry point
int main(void) {
	// Initial values
	MX1_1 = false;
	IX1_2 = false;
	MX1_3 = false;
	MX2_1 = false;
	IX2_2 = false;
	IX2_3 = false;
	MX2_4 = false;
	MX3_1 = false;
	IX3_2 = false;
	IX3_3 = false;
	IX3_4 = false;
	MX3_4 = false;
	MX4_1 = false;
	IX4_2 = false;
	IX4_3 = false;
	IX4_4 = false;
	MX4_5 = false;

	// Custom entry logic

	return 0;
}