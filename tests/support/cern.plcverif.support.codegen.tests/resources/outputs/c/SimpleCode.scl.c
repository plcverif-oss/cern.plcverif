#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>
 
// Struct definitions
typedef struct {
	int16_t out1;
} __function1;

typedef struct {
	bool in1;
	int16_t in2;
	int16_t RET_VAL;
} __function2;

typedef struct {
} __simplecode_OB1;

// Global variables
bool MX123_7;
__simplecode_OB1 simplecode_OB11;
__function1 function11;
__function2 function21;

// Forward declarations of the generated functions
void simplecode_OB1();
void function1(__function1 *__context);
void function2(__function2 *__context);

// Declare nondet assignment functions
bool nondet_bool(void);
uint8_t nondet_uint8_t(void);
uint16_t nondet_uint16_t(void);
uint32_t nondet_uint32_t(void);
uint64_t nondet_uint64_t(void);
int8_t nondet_int8_t(void);
int16_t nondet_int16_t(void);
int32_t nondet_int32_t(void);
int64_t nondet_int64_t(void);
double nondet_float(void);
double nondet_double(void);

// Translated functions
void simplecode_OB1() {
	// Temporary variables

	{
		// Assign inputs
		function1(&function11);
		// Assign outputs
		return;
	}
}

void function1(__function1 *__context) {
	// Temporary variables

	{
		// Assign inputs
		function21.in1 = MX123_7;
		function21.in2 = 123;
		function2(&function21);
		// Assign outputs
		__context->out1 = function21.RET_VAL;
		return;
	}
}

void function2(__function2 *__context) {
	// Temporary variables

	{
		if (__context->in1) {
			__context->RET_VAL = __context->in2;
		}
		else {
			__context->RET_VAL = (- __context->in2);
		}
		return;
	}
}

// Entry point
int main(void) {
	// Initial values
	MX123_7 = false;
	function11.out1 = 0;
	function21.in1 = false;
	function21.in2 = 0;
	function21.RET_VAL = 0;

	// Custom entry logic

	return 0;
}