#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>
 
// Struct definitions
typedef struct {
	bool a;
	bool b;
} __implInstFb1;

// Global variables
__implInstFb1 instance;

// Forward declarations of the generated functions
void implInstFb1(__implInstFb1 *__context);

// Declare nondet assignment functions
bool nondet_bool(void);
uint8_t nondet_uint8_t(void);
uint16_t nondet_uint16_t(void);
uint32_t nondet_uint32_t(void);
uint64_t nondet_uint64_t(void);
int8_t nondet_int8_t(void);
int16_t nondet_int16_t(void);
int32_t nondet_int32_t(void);
int64_t nondet_int64_t(void);
double nondet_float(void);
double nondet_double(void);

// Translated functions
void implInstFb1(__implInstFb1 *__context) {
	// Temporary variables

	{
		__context->b = __context->a;
		return;
	}
}

// Entry point
int main(void) {
	// Initial values
	instance.a = false;
	instance.b = false;

	// Custom entry logic

	return 0;
}