#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>
 
// Struct definitions
typedef struct {
} __gotoFunction1;

// Global variables
bool MX0_0;
bool MX0_1;
bool MX0_2;
bool IX0_3;
bool IX0_4;
__gotoFunction1 gotoFunction11;

// Forward declarations of the generated functions
void gotoFunction1();

// Declare nondet assignment functions
bool nondet_bool(void);
uint8_t nondet_uint8_t(void);
uint16_t nondet_uint16_t(void);
uint32_t nondet_uint32_t(void);
uint64_t nondet_uint64_t(void);
int8_t nondet_int8_t(void);
int16_t nondet_int16_t(void);
int32_t nondet_int32_t(void);
int64_t nondet_int64_t(void);
double nondet_float(void);
double nondet_double(void);

// Translated functions
void gotoFunction1() {
	// Temporary variables

	{
		l1: ;
		MX0_0 = true;
		goto gotoFunction1_l1;
		gotoFunction1_l1: ;
		l2: ;
		MX0_1 = true;
		MX0_2 = true;
		if (IX0_3) {
			goto l2;
		}
		else if (IX0_4) {
			goto l1;
		}
		return;
	}
}

// Entry point
int main(void) {
	// Initial values
	MX0_0 = false;
	MX0_1 = false;
	MX0_2 = false;
	IX0_3 = false;
	IX0_4 = false;

	// Custom entry logic

	return 0;
}