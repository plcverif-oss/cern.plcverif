#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>
 
// Struct definitions
typedef struct {
} __expr_OB1;

typedef struct {
	bool boolVar1;
	bool boolVar2;
	int16_t intVar1;
	int16_t intVar2;
	uint8_t byteVar1;
	uint8_t byteVar2;
	uint16_t wordVar1;
	uint16_t wordVar2;
	uint32_t dwordVar1;
	uint32_t dwordVar2;
} __expr_fc1;

// Global variables
bool IX0_0;
__expr_OB1 expr_OB11;
__expr_fc1 expr_fc11;

// Forward declarations of the generated functions
void expr_OB1();
void expr_fc1(__expr_fc1 *__context);

// Declare nondet assignment functions
bool nondet_bool(void);
uint8_t nondet_uint8_t(void);
uint16_t nondet_uint16_t(void);
uint32_t nondet_uint32_t(void);
uint64_t nondet_uint64_t(void);
int8_t nondet_int8_t(void);
int16_t nondet_int16_t(void);
int32_t nondet_int32_t(void);
int64_t nondet_int64_t(void);
double nondet_float(void);
double nondet_double(void);

// Translated functions
void expr_OB1() {
	// Temporary variables

	{
		// Assign inputs
		expr_fc1(&expr_fc11);
		// Assign outputs
		return;
	}
}

void expr_fc1(__expr_fc1 *__context) {
	// Temporary variables

	{
		__context->boolVar1 = (__context->boolVar1 && __context->boolVar2);
		__context->boolVar1 = (__context->boolVar1 && false);
		__context->boolVar1 = (__context->boolVar1 && true);
		__context->boolVar1 = (IX0_0 && true);
		__context->boolVar1 = (true && true);
		__context->boolVar1 = (false && true);
		__context->boolVar1 = (__context->boolVar1 || __context->boolVar2);
		__context->boolVar1 = (__context->boolVar1 || false);
		__context->boolVar1 = (__context->boolVar1 || true);
		__context->boolVar1 = (IX0_0 || true);
		__context->boolVar1 = (true || true);
		__context->boolVar1 = (false || true);
		__context->boolVar1 = (__context->boolVar1 != __context->boolVar2);
		__context->boolVar1 = (__context->boolVar1 != false);
		__context->boolVar1 = (__context->boolVar1 != true);
		__context->boolVar1 = (IX0_0 != true);
		__context->boolVar1 = (true != true);
		__context->boolVar1 = (false != true);
		__context->byteVar1 = (__context->byteVar1 & __context->byteVar2);
		__context->byteVar1 = (__context->byteVar1 & 123);
		__context->byteVar1 = (__context->byteVar1 & 123);
		__context->byteVar1 = (1 & 3);
		__context->wordVar1 = (__context->wordVar1 & __context->wordVar2);
		__context->wordVar1 = (__context->wordVar1 & 123);
		__context->wordVar1 = (__context->wordVar1 & 123);
		__context->wordVar1 = (__context->wordVar1 & 123);
		__context->dwordVar1 = (__context->dwordVar1 & __context->dwordVar2);
		__context->dwordVar1 = (__context->dwordVar1 & 123);
		__context->dwordVar1 = (__context->dwordVar1 & 123);
		__context->dwordVar1 = (__context->dwordVar1 & 123);
		__context->dwordVar1 = (__context->dwordVar1 & 123);
		__context->byteVar1 = (__context->byteVar1 | __context->byteVar2);
		__context->byteVar1 = (__context->byteVar1 | 123);
		__context->byteVar1 = (__context->byteVar1 | 123);
		__context->byteVar1 = (1 | 3);
		__context->wordVar1 = (__context->wordVar1 | __context->wordVar2);
		__context->wordVar1 = (__context->wordVar1 | 123);
		__context->wordVar1 = (__context->wordVar1 | 123);
		__context->wordVar1 = (__context->wordVar1 | 123);
		__context->dwordVar1 = (__context->dwordVar1 | __context->dwordVar2);
		__context->dwordVar1 = (__context->dwordVar1 | 123);
		__context->dwordVar1 = (__context->dwordVar1 | 123);
		__context->dwordVar1 = (__context->dwordVar1 | 123);
		__context->dwordVar1 = (__context->dwordVar1 | 123);
		__context->byteVar1 = (__context->byteVar1 ^ __context->byteVar2);
		__context->byteVar1 = (__context->byteVar1 ^ 123);
		__context->byteVar1 = (__context->byteVar1 ^ 123);
		__context->byteVar1 = (1 ^ 3);
		__context->wordVar1 = (__context->wordVar1 ^ __context->wordVar2);
		__context->wordVar1 = (__context->wordVar1 ^ 123);
		__context->wordVar1 = (__context->wordVar1 ^ 123);
		__context->wordVar1 = (__context->wordVar1 ^ 123);
		__context->dwordVar1 = (__context->dwordVar1 ^ __context->dwordVar2);
		__context->dwordVar1 = (__context->dwordVar1 ^ 123);
		__context->dwordVar1 = (__context->dwordVar1 ^ 123);
		__context->dwordVar1 = (__context->dwordVar1 ^ 123);
		__context->dwordVar1 = (__context->dwordVar1 ^ 123);
		return;
	}
}

// Entry point
int main(void) {
	// Initial values
	IX0_0 = false;
	expr_fc11.boolVar1 = false;
	expr_fc11.boolVar2 = false;
	expr_fc11.intVar1 = 0;
	expr_fc11.intVar2 = 0;
	expr_fc11.byteVar1 = 0;
	expr_fc11.byteVar2 = 0;
	expr_fc11.wordVar1 = 0;
	expr_fc11.wordVar2 = 0;
	expr_fc11.dwordVar1 = 0;
	expr_fc11.dwordVar2 = 0;

	// Custom entry logic

	return 0;
}