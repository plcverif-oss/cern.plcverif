#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>
 
// Struct definitions
typedef struct {
	int32_t PT;
	bool IN;
	bool Q;
	int32_t ET;
	bool running;
	int32_t start;
} __TON;

typedef struct {
} __timeh_OB1;

typedef struct {
	bool enabled;
	bool q;
	__TON ton;
} __timeh_fb1;

// Global variables
bool MX0_0;
int32_t __GLOBAL_TIME;
__timeh_OB1 timeh_OB11;
__timeh_fb1 timeh_db1;
int32_t T_CYCLE;

// Forward declarations of the generated functions
void timeh_OB1();
void timeh_fb1(__timeh_fb1 *__context);
void TON(__TON *__context);

// Declare nondet assignment functions
bool nondet_bool(void);
uint8_t nondet_uint8_t(void);
uint16_t nondet_uint16_t(void);
uint32_t nondet_uint32_t(void);
uint64_t nondet_uint64_t(void);
int8_t nondet_int8_t(void);
int16_t nondet_int16_t(void);
int32_t nondet_int32_t(void);
int64_t nondet_int64_t(void);
double nondet_float(void);
double nondet_double(void);

// Translated functions
void timeh_OB1() {
	// Temporary variables

	{
		// Assign inputs
		timeh_db1.enabled = true;
		timeh_fb1(&timeh_db1);
		// Assign outputs
		MX0_0 = timeh_db1.q;
		return;
	}
}

void timeh_fb1(__timeh_fb1 *__context) {
	// Temporary variables

	{
		if (__context->enabled) {
			// Assign inputs
			__context->ton.IN = true;
			__context->ton.PT = 1500;
			TON(&__context->ton);
			// Assign outputs
		}
		__context->q = __context->ton.Q;
		return;
	}
}

void TON(__TON *__context) {
	// Temporary variables

	{
		if ((__context->IN == false)) {
			__context->Q = false;
			__context->ET = 0;
			__context->running = false;
		}
		else {
			if ((__context->running == false)) {
				__context->start = __GLOBAL_TIME;
				__context->running = true;
				__context->ET = 0;
				if ((__context->PT == 0)) {
					__context->Q = true;
				}
			}
			else {
				if ((! ((__GLOBAL_TIME - (__context->start + __context->PT)) >= 0))) {
					if ((! __context->Q)) {
						__context->ET = (__GLOBAL_TIME - __context->start);
					}
				}
				else {
					__context->Q = true;
					__context->ET = __context->PT;
				}
			}
		}
		return;
	}
}

// Entry point
int main(void) {
	// Initial values
	MX0_0 = false;
	__GLOBAL_TIME = 0;
	timeh_db1.enabled = false;
	timeh_db1.q = false;
	timeh_db1.ton.PT = 0;
	timeh_db1.ton.IN = false;
	timeh_db1.ton.Q = false;
	timeh_db1.ton.ET = 0;
	timeh_db1.ton.running = false;
	timeh_db1.ton.start = 0;
	T_CYCLE = 0;

	// Custom entry logic

	return 0;
}