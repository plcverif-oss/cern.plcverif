#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>
 
// Struct definitions
typedef struct {
} __basic_OB1;

typedef struct {
	bool in;
	int16_t in2;
	bool RET_VAL;
} __basic_foo;

// Global variables
bool IX0_0;
__basic_OB1 basic_OB11;
__basic_foo basic_foo1;

// Forward declarations of the generated functions
void basic_OB1(__basic_OB1 *__context);
void basic_foo(__basic_foo *__context);

// Declare nondet assignment functions
bool nondet_bool(void);
uint8_t nondet_uint8_t(void);
uint16_t nondet_uint16_t(void);
uint32_t nondet_uint32_t(void);
uint64_t nondet_uint64_t(void);
int8_t nondet_int8_t(void);
int16_t nondet_int16_t(void);
int32_t nondet_int32_t(void);
int64_t nondet_int64_t(void);
double nondet_float(void);
double nondet_double(void);

// Translated functions
void basic_OB1(__basic_OB1 *__context) {
	// Temporary variables
	bool result;

	{
		result = false;
		// Assign inputs
		basic_foo1.in = IX0_0;
		basic_foo1.in2 = 5;
		basic_foo(&basic_foo1);
		// Assign outputs
		result = basic_foo1.RET_VAL;
		assert((!(result) || (! IX0_0)));
		return;
	}
}

void basic_foo(__basic_foo *__context) {
	// Temporary variables
	bool temp;

	{
		temp = false;
		if ((__context->in2 > 0)) {
			temp = (! __context->in);
		}
		else {
			temp = __context->in;
		}
		__context->RET_VAL = temp;
		return;
	}
}

// Entry point
int main(void) {
	// Initial values
	IX0_0 = false;
	basic_foo1.in = false;
	basic_foo1.in2 = 0;
	basic_foo1.RET_VAL = false;

	// Custom entry logic

	return 0;
}