#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>
 
// Struct definitions
typedef struct {
	int16_t in1;
	int16_t in2;
	int16_t RET_VAL;
} __MAX0;

typedef struct {
	int16_t j;
} __extractNested1;

typedef struct {
	int16_t j;
} __extractNested2;

typedef struct {
	int16_t j;
} __extractNested3;

typedef struct {
	int16_t j;
} __extractNested4;

typedef struct {
} __extractNested_OB1;

// Global variables
__extractNested1 extractNested11;
__extractNested2 extractNested21;
__extractNested3 extractNested31;
__extractNested4 extractNested41;
__MAX0 MAX01;
__extractNested_OB1 extractNested_OB11;
__MAX0 MAX01_inlined_1;
__MAX0 MAX01_inlined_2;
__MAX0 MAX01_inlined_3;
__MAX0 MAX01_inlined_4;
__MAX0 MAX01_inlined_5;
__MAX0 MAX01_inlined_6;
__MAX0 MAX01_inlined_7;
__MAX0 MAX01_inlined_8;
__MAX0 MAX01_inlined_9;

// Forward declarations of the generated functions
void extractNested1(__extractNested1 *__context);
void extractNested2(__extractNested2 *__context);
void extractNested3(__extractNested3 *__context);
void extractNested4(__extractNested4 *__context);
void MAX0(__MAX0 *__context);
void extractNested_OB1();

// Declare nondet assignment functions
bool nondet_bool(void);
uint8_t nondet_uint8_t(void);
uint16_t nondet_uint16_t(void);
uint32_t nondet_uint32_t(void);
uint64_t nondet_uint64_t(void);
int8_t nondet_int8_t(void);
int16_t nondet_int16_t(void);
int32_t nondet_int32_t(void);
int64_t nondet_int64_t(void);
double nondet_float(void);
double nondet_double(void);

// Translated functions
void extractNested1(__extractNested1 *__context) {
	// Temporary variables
	int16_t ___nested_ret_val1;

	{
		___nested_ret_val1 = 0;
		// Assign inputs
		MAX01_inlined_1.in1 = 2;
		MAX01_inlined_1.in2 = 3;
		MAX0(&MAX01_inlined_1);
		// Assign outputs
		___nested_ret_val1 = MAX01_inlined_1.RET_VAL;
		// Assign inputs
		MAX01_inlined_2.in1 = 1;
		MAX01_inlined_2.in2 = ___nested_ret_val1;
		MAX0(&MAX01_inlined_2);
		// Assign outputs
		__context->j = MAX01_inlined_2.RET_VAL;
		return;
	}
}

void extractNested2(__extractNested2 *__context) {
	// Temporary variables
	int16_t ___nested_ret_val2;
	int16_t ___nested_ret_val3;

	{
		___nested_ret_val2 = 0;
		___nested_ret_val3 = 0;
		// Assign inputs
		MAX01_inlined_3.in1 = 3;
		MAX01_inlined_3.in2 = 4;
		MAX0(&MAX01_inlined_3);
		// Assign outputs
		___nested_ret_val3 = MAX01_inlined_3.RET_VAL;
		// Assign inputs
		MAX01_inlined_4.in1 = 2;
		MAX01_inlined_4.in2 = ((2 * ___nested_ret_val3) + 6);
		MAX0(&MAX01_inlined_4);
		// Assign outputs
		___nested_ret_val2 = MAX01_inlined_4.RET_VAL;
		// Assign inputs
		MAX01_inlined_5.in1 = 1;
		MAX01_inlined_5.in2 = (___nested_ret_val2 + 5);
		MAX0(&MAX01_inlined_5);
		// Assign outputs
		__context->j = MAX01_inlined_5.RET_VAL;
		return;
	}
}

void extractNested3(__extractNested3 *__context) {
	// Temporary variables
	int16_t ___nested_ret_val4;
	int16_t ___nested_ret_val5;

	{
		___nested_ret_val4 = 0;
		___nested_ret_val5 = 0;
		// Assign inputs
		MAX01_inlined_6.in1 = 11;
		MAX01_inlined_6.in2 = 22;
		MAX0(&MAX01_inlined_6);
		// Assign outputs
		___nested_ret_val4 = MAX01_inlined_6.RET_VAL;
		// Assign inputs
		MAX01_inlined_7.in1 = 33;
		MAX01_inlined_7.in2 = 44;
		MAX0(&MAX01_inlined_7);
		// Assign outputs
		___nested_ret_val5 = MAX01_inlined_7.RET_VAL;
		// Assign inputs
		MAX01_inlined_8.in1 = ___nested_ret_val4;
		MAX01_inlined_8.in2 = ___nested_ret_val5;
		MAX0(&MAX01_inlined_8);
		// Assign outputs
		__context->j = MAX01_inlined_8.RET_VAL;
		return;
	}
}

void extractNested4(__extractNested4 *__context) {
	// Temporary variables
	int16_t ___nested_ret_val6;

	{
		___nested_ret_val6 = 0;
		// Assign inputs
		MAX01_inlined_9.in1 = 0;
		MAX01_inlined_9.in2 = 1;
		MAX0(&MAX01_inlined_9);
		// Assign outputs
		___nested_ret_val6 = MAX01_inlined_9.RET_VAL;
		__context->j = (___nested_ret_val6 + 10);
		return;
	}
}

void MAX0(__MAX0 *__context) {
	// Temporary variables

	{
		if ((__context->in1 > __context->in2)) {
			__context->RET_VAL = __context->in1;
		}
		else {
			__context->RET_VAL = __context->in2;
		}
		return;
	}
}

void extractNested_OB1() {
	// Temporary variables

	{
		// Assign inputs
		extractNested1(&extractNested11);
		// Assign outputs
		// Assign inputs
		extractNested2(&extractNested21);
		// Assign outputs
		// Assign inputs
		extractNested3(&extractNested31);
		// Assign outputs
		// Assign inputs
		extractNested4(&extractNested41);
		// Assign outputs
		return;
	}
}

// Entry point
int main(void) {
	// Initial values
	extractNested11.j = 0;
	extractNested21.j = 0;
	extractNested31.j = 0;
	extractNested41.j = 0;
	MAX01.in1 = 0;
	MAX01.in2 = 0;
	MAX01.RET_VAL = 0;
	MAX01_inlined_1.in1 = 0;
	MAX01_inlined_1.in2 = 0;
	MAX01_inlined_1.RET_VAL = 0;
	MAX01_inlined_2.in1 = 0;
	MAX01_inlined_2.in2 = 0;
	MAX01_inlined_2.RET_VAL = 0;
	MAX01_inlined_3.in1 = 0;
	MAX01_inlined_3.in2 = 0;
	MAX01_inlined_3.RET_VAL = 0;
	MAX01_inlined_4.in1 = 0;
	MAX01_inlined_4.in2 = 0;
	MAX01_inlined_4.RET_VAL = 0;
	MAX01_inlined_5.in1 = 0;
	MAX01_inlined_5.in2 = 0;
	MAX01_inlined_5.RET_VAL = 0;
	MAX01_inlined_6.in1 = 0;
	MAX01_inlined_6.in2 = 0;
	MAX01_inlined_6.RET_VAL = 0;
	MAX01_inlined_7.in1 = 0;
	MAX01_inlined_7.in2 = 0;
	MAX01_inlined_7.RET_VAL = 0;
	MAX01_inlined_8.in1 = 0;
	MAX01_inlined_8.in2 = 0;
	MAX01_inlined_8.RET_VAL = 0;
	MAX01_inlined_9.in1 = 0;
	MAX01_inlined_9.in2 = 0;
	MAX01_inlined_9.RET_VAL = 0;

	// Custom entry logic

	return 0;
}