#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>
 
// Struct definitions
typedef struct {
} __callParamsFunc1;

typedef struct {
	bool RET_VAL;
} __callParamsFunc2;

typedef struct {
	bool IN;
	bool RET_VAL;
} __callParamsFunc3;

typedef struct {
	bool i1;
	bool i2;
	bool RET_VAL;
} __callParamsFunc4;

typedef struct {
	bool i1;
	bool i2;
	bool out1;
	bool RET_VAL;
} __callParamsFunc5;

typedef struct {
	bool i1;
	bool i2;
	bool inout1;
	bool RET_VAL;
} __callParamsFunc6;

typedef struct {
} __callParams_OB1;

// Global variables
bool MX2_0;
bool MX0_0;
bool MX3_0;
bool MX3_1;
bool MX1_0;
bool MX1_1;
bool MX4_0;
bool MX4_1;
bool MX5_4;
bool MX5_0;
bool MX5_5;
bool MX5_1;
bool MX5_6;
bool MX5_2;
bool MX6_4;
bool MX6_0;
bool MX6_5;
bool MX6_1;
bool MX6_6;
bool MX6_2;
__callParams_OB1 callParams_OB11;
__callParamsFunc1 callParamsFunc11;
__callParamsFunc2 callParamsFunc21;
__callParamsFunc3 callParamsFunc31;
__callParamsFunc4 callParamsFunc41;
__callParamsFunc5 callParamsFunc51;
__callParamsFunc6 callParamsFunc61;
__callParamsFunc3 callParamsFunc31_inlined_1;
__callParamsFunc3 callParamsFunc31_inlined_2;
__callParamsFunc4 callParamsFunc41_inlined_3;
__callParamsFunc4 callParamsFunc41_inlined_4;
__callParamsFunc5 callParamsFunc51_inlined_5;
__callParamsFunc5 callParamsFunc51_inlined_6;
__callParamsFunc5 callParamsFunc51_inlined_7;
__callParamsFunc6 callParamsFunc61_inlined_8;
__callParamsFunc6 callParamsFunc61_inlined_9;
__callParamsFunc6 callParamsFunc61_inlined_10;

// Forward declarations of the generated functions
void callParams_OB1();
void callParamsFunc1();
void callParamsFunc2(__callParamsFunc2 *__context);
void callParamsFunc3(__callParamsFunc3 *__context);
void callParamsFunc4(__callParamsFunc4 *__context);
void callParamsFunc5(__callParamsFunc5 *__context);
void callParamsFunc6(__callParamsFunc6 *__context);

// Declare nondet assignment functions
bool nondet_bool(void);
uint8_t nondet_uint8_t(void);
uint16_t nondet_uint16_t(void);
uint32_t nondet_uint32_t(void);
uint64_t nondet_uint64_t(void);
int8_t nondet_int8_t(void);
int16_t nondet_int16_t(void);
int32_t nondet_int32_t(void);
int64_t nondet_int64_t(void);
double nondet_float(void);
double nondet_double(void);

// Translated functions
void callParams_OB1() {
	// Temporary variables

	{
		callParamsFunc1();
		return;
	}
}

void callParamsFunc1() {
	// Temporary variables

	{
		// Assign inputs
		callParamsFunc2(&callParamsFunc21);
		// Assign outputs
		MX2_0 = callParamsFunc21.RET_VAL;
		// Assign inputs
		callParamsFunc31_inlined_1.IN = MX0_0;
		callParamsFunc3(&callParamsFunc31_inlined_1);
		// Assign outputs
		MX3_0 = callParamsFunc31_inlined_1.RET_VAL;
		// Assign inputs
		callParamsFunc31_inlined_2.IN = MX0_0;
		callParamsFunc3(&callParamsFunc31_inlined_2);
		// Assign outputs
		MX3_1 = callParamsFunc31_inlined_2.RET_VAL;
		// Assign inputs
		callParamsFunc41_inlined_3.i1 = MX1_0;
		callParamsFunc41_inlined_3.i2 = MX1_1;
		callParamsFunc4(&callParamsFunc41_inlined_3);
		// Assign outputs
		MX4_0 = callParamsFunc41_inlined_3.RET_VAL;
		// Assign inputs
		callParamsFunc41_inlined_4.i2 = MX1_0;
		callParamsFunc41_inlined_4.i1 = MX1_1;
		callParamsFunc4(&callParamsFunc41_inlined_4);
		// Assign outputs
		MX4_1 = callParamsFunc41_inlined_4.RET_VAL;
		// Assign inputs
		callParamsFunc51_inlined_5.i1 = MX1_0;
		callParamsFunc51_inlined_5.i2 = MX1_1;
		callParamsFunc5(&callParamsFunc51_inlined_5);
		// Assign outputs
		MX5_4 = callParamsFunc51_inlined_5.out1;
		MX5_0 = callParamsFunc51_inlined_5.RET_VAL;
		// Assign inputs
		callParamsFunc51_inlined_6.i1 = MX1_0;
		callParamsFunc51_inlined_6.i2 = MX1_1;
		callParamsFunc5(&callParamsFunc51_inlined_6);
		// Assign outputs
		MX5_5 = callParamsFunc51_inlined_6.out1;
		MX5_1 = callParamsFunc51_inlined_6.RET_VAL;
		// Assign inputs
		callParamsFunc51_inlined_7.i1 = MX1_0;
		callParamsFunc51_inlined_7.i2 = MX1_1;
		callParamsFunc5(&callParamsFunc51_inlined_7);
		// Assign outputs
		MX5_6 = callParamsFunc51_inlined_7.out1;
		MX5_2 = callParamsFunc51_inlined_7.RET_VAL;
		// Assign inputs
		callParamsFunc61_inlined_8.i1 = MX1_0;
		callParamsFunc61_inlined_8.i2 = MX1_1;
		callParamsFunc61_inlined_8.inout1 = MX6_4;
		callParamsFunc6(&callParamsFunc61_inlined_8);
		// Assign outputs
		MX6_4 = callParamsFunc61_inlined_8.inout1;
		MX6_0 = callParamsFunc61_inlined_8.RET_VAL;
		// Assign inputs
		callParamsFunc61_inlined_9.inout1 = MX6_5;
		callParamsFunc61_inlined_9.i1 = MX1_0;
		callParamsFunc61_inlined_9.i2 = MX1_1;
		callParamsFunc6(&callParamsFunc61_inlined_9);
		// Assign outputs
		MX6_5 = callParamsFunc61_inlined_9.inout1;
		MX6_1 = callParamsFunc61_inlined_9.RET_VAL;
		// Assign inputs
		callParamsFunc61_inlined_10.i1 = MX1_0;
		callParamsFunc61_inlined_10.i2 = MX1_1;
		callParamsFunc61_inlined_10.inout1 = MX6_6;
		callParamsFunc6(&callParamsFunc61_inlined_10);
		// Assign outputs
		MX6_6 = callParamsFunc61_inlined_10.inout1;
		MX6_2 = callParamsFunc61_inlined_10.RET_VAL;
		return;
	}
}

void callParamsFunc2(__callParamsFunc2 *__context) {
	// Temporary variables

	{
		__context->RET_VAL = false;
		return;
	}
}

void callParamsFunc3(__callParamsFunc3 *__context) {
	// Temporary variables

	{
		__context->RET_VAL = (! __context->IN);
		return;
	}
}

void callParamsFunc4(__callParamsFunc4 *__context) {
	// Temporary variables

	{
		__context->RET_VAL = (__context->i1 && __context->i2);
		return;
	}
}

void callParamsFunc5(__callParamsFunc5 *__context) {
	// Temporary variables

	{
		__context->out1 = (__context->i1 || __context->i2);
		__context->RET_VAL = (__context->i1 && __context->i2);
		return;
	}
}

void callParamsFunc6(__callParamsFunc6 *__context) {
	// Temporary variables

	{
		__context->inout1 = (__context->inout1 || __context->i2);
		__context->RET_VAL = (__context->i1 && __context->i2);
		return;
	}
}

// Entry point
int main(void) {
	// Initial values
	MX2_0 = false;
	MX0_0 = false;
	MX3_0 = false;
	MX3_1 = false;
	MX1_0 = false;
	MX1_1 = false;
	MX4_0 = false;
	MX4_1 = false;
	MX5_4 = false;
	MX5_0 = false;
	MX5_5 = false;
	MX5_1 = false;
	MX5_6 = false;
	MX5_2 = false;
	MX6_4 = false;
	MX6_0 = false;
	MX6_5 = false;
	MX6_1 = false;
	MX6_6 = false;
	MX6_2 = false;
	callParamsFunc21.RET_VAL = false;
	callParamsFunc31.IN = false;
	callParamsFunc31.RET_VAL = false;
	callParamsFunc41.i1 = false;
	callParamsFunc41.i2 = false;
	callParamsFunc41.RET_VAL = false;
	callParamsFunc51.i1 = false;
	callParamsFunc51.i2 = false;
	callParamsFunc51.out1 = false;
	callParamsFunc51.RET_VAL = false;
	callParamsFunc61.i1 = false;
	callParamsFunc61.i2 = false;
	callParamsFunc61.inout1 = false;
	callParamsFunc61.RET_VAL = false;
	callParamsFunc31_inlined_1.IN = false;
	callParamsFunc31_inlined_1.RET_VAL = false;
	callParamsFunc31_inlined_2.IN = false;
	callParamsFunc31_inlined_2.RET_VAL = false;
	callParamsFunc41_inlined_3.i1 = false;
	callParamsFunc41_inlined_3.i2 = false;
	callParamsFunc41_inlined_3.RET_VAL = false;
	callParamsFunc41_inlined_4.i1 = false;
	callParamsFunc41_inlined_4.i2 = false;
	callParamsFunc41_inlined_4.RET_VAL = false;
	callParamsFunc51_inlined_5.i1 = false;
	callParamsFunc51_inlined_5.i2 = false;
	callParamsFunc51_inlined_5.out1 = false;
	callParamsFunc51_inlined_5.RET_VAL = false;
	callParamsFunc51_inlined_6.i1 = false;
	callParamsFunc51_inlined_6.i2 = false;
	callParamsFunc51_inlined_6.out1 = false;
	callParamsFunc51_inlined_6.RET_VAL = false;
	callParamsFunc51_inlined_7.i1 = false;
	callParamsFunc51_inlined_7.i2 = false;
	callParamsFunc51_inlined_7.out1 = false;
	callParamsFunc51_inlined_7.RET_VAL = false;
	callParamsFunc61_inlined_8.i1 = false;
	callParamsFunc61_inlined_8.i2 = false;
	callParamsFunc61_inlined_8.inout1 = false;
	callParamsFunc61_inlined_8.RET_VAL = false;
	callParamsFunc61_inlined_9.i1 = false;
	callParamsFunc61_inlined_9.i2 = false;
	callParamsFunc61_inlined_9.inout1 = false;
	callParamsFunc61_inlined_9.RET_VAL = false;
	callParamsFunc61_inlined_10.i1 = false;
	callParamsFunc61_inlined_10.i2 = false;
	callParamsFunc61_inlined_10.inout1 = false;
	callParamsFunc61_inlined_10.RET_VAL = false;

	// Custom entry logic

	return 0;
}