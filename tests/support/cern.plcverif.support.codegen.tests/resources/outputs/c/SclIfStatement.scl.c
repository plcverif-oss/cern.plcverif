#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>
 
// Struct definitions
typedef struct {
} __ifFunction1;

typedef struct {
} __ifFunction2;

typedef struct {
} __ifFunction3;

typedef struct {
} __ifFunction4;

typedef struct {
} __ifFunction5;

typedef struct {
} __ifFunction_OB1;

// Global variables
bool MX1_1;
bool MX1_2;
bool MX1_3;
bool MX1_4;
bool MX2_1;
bool MX2_2;
bool MX2_3;
bool MX2_4;
bool MX2_5;
bool MX3_1;
bool MX3_2;
bool MX3_3;
bool MX3_4;
bool MX3_5;
bool MX3_6;
bool MX3_7;
bool MX4_1;
bool MX4_2;
bool MX4_3;
bool MX4_4;
bool MX4_5;
bool MX4_6;
bool MX4_7;
bool MX4_0;
bool MX5_1;
bool MX5_2;
bool MX5_3;
bool MX5_4;
bool MX5_5;
bool MX5_6;
bool MX5_7;
__ifFunction1 ifFunction11;
__ifFunction2 ifFunction21;
__ifFunction3 ifFunction31;
__ifFunction4 ifFunction41;
__ifFunction5 ifFunction51;
__ifFunction_OB1 ifFunction_OB11;

// Forward declarations of the generated functions
void ifFunction1();
void ifFunction2();
void ifFunction3();
void ifFunction4();
void ifFunction5();
void ifFunction_OB1();

// Declare nondet assignment functions
bool nondet_bool(void);
uint8_t nondet_uint8_t(void);
uint16_t nondet_uint16_t(void);
uint32_t nondet_uint32_t(void);
uint64_t nondet_uint64_t(void);
int8_t nondet_int8_t(void);
int16_t nondet_int16_t(void);
int32_t nondet_int32_t(void);
int64_t nondet_int64_t(void);
double nondet_float(void);
double nondet_double(void);

// Translated functions
void ifFunction1() {
	// Temporary variables

	{
		MX1_1 = true;
		if (MX1_2) {
			MX1_3 = true;
		}
		MX1_4 = true;
		return;
	}
}

void ifFunction2() {
	// Temporary variables

	{
		MX2_1 = true;
		if (MX2_2) {
			MX2_3 = true;
		}
		else {
			MX2_4 = true;
		}
		MX2_5 = true;
		return;
	}
}

void ifFunction3() {
	// Temporary variables

	{
		MX3_1 = true;
		if (MX3_2) {
			MX3_3 = true;
		}
		else if (MX3_4) {
			MX3_5 = true;
		}
		else {
			MX3_6 = true;
		}
		MX3_7 = true;
		return;
	}
}

void ifFunction4() {
	// Temporary variables

	{
		MX4_1 = true;
		if (MX4_2) {
			MX4_3 = true;
		}
		else if (MX4_4) {
			MX4_5 = true;
		}
		else if (MX4_6) {
			MX4_7 = true;
		}
		else {
			MX4_0 = true;
		}
		MX4_1 = true;
		return;
	}
}

void ifFunction5() {
	// Temporary variables

	{
		MX5_1 = true;
		if (MX5_2) {
			MX5_3 = true;
		}
		else {
			MX5_4 = true;
			if (MX5_5) {
				MX5_6 = true;
			}
		}
		MX5_7 = true;
		return;
	}
}

void ifFunction_OB1() {
	// Temporary variables

	{
		ifFunction1();
		ifFunction2();
		ifFunction3();
		ifFunction4();
		ifFunction5();
		return;
	}
}

// Entry point
int main(void) {
	// Initial values
	MX1_1 = false;
	MX1_2 = false;
	MX1_3 = false;
	MX1_4 = false;
	MX2_1 = false;
	MX2_2 = false;
	MX2_3 = false;
	MX2_4 = false;
	MX2_5 = false;
	MX3_1 = false;
	MX3_2 = false;
	MX3_3 = false;
	MX3_4 = false;
	MX3_5 = false;
	MX3_6 = false;
	MX3_7 = false;
	MX4_1 = false;
	MX4_2 = false;
	MX4_3 = false;
	MX4_4 = false;
	MX4_5 = false;
	MX4_6 = false;
	MX4_7 = false;
	MX4_0 = false;
	MX5_1 = false;
	MX5_2 = false;
	MX5_3 = false;
	MX5_4 = false;
	MX5_5 = false;
	MX5_6 = false;
	MX5_7 = false;

	// Custom entry logic

	return 0;
}