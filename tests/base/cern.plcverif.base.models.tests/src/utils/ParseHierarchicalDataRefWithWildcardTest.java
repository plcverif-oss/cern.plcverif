/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package utils;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef;
import cern.plcverif.base.models.cfa.utils.CfaDeclarationUtils;
import cern.plcverif.library.testmodels.TestModelCollection;

public class ParseHierarchicalDataRefWithWildcardTest {
	private static CfaNetworkDeclaration cfa;
	
	@BeforeClass
	public static void setup() throws IOException {
		cfa = TestModelCollection.getInstance().getModel("CompoundDataStructures.scl").getCfd();
	}
	
	@Test
	public void test1() {
		List<DataRef> refs = CfaDeclarationUtils.parseHierarchicalElementaryDataRefsWithWildcard("compound_db1.in1", cfa, it -> it.getDisplayName());
		Assert.assertEquals(1, refs.size());
	}
	
	@Test
	public void test2() {
		List<DataRef> refs = CfaDeclarationUtils.parseHierarchicalElementaryDataRefsWithWildcard("compound_db1.in*", cfa, it -> it.getDisplayName());
		Assert.assertEquals(2, refs.size());
	}
	
	@Test
	public void test3() {
		List<DataRef> refs = CfaDeclarationUtils.parseHierarchicalElementaryDataRefsWithWildcard("compound_db1.*", cfa, it -> it.getDisplayName());
		Assert.assertEquals(2, refs.size());
	}
	
	@Test
	public void test4() {
		List<DataRef> refs = CfaDeclarationUtils.parseHierarchicalElementaryDataRefsWithWildcard("compound_db1.arr1[*]", cfa, it -> it.getDisplayName());
		// match: compound_db1.arr1[1], compound_db1.arr1[2], compound_db1.arr1[3]
		Assert.assertEquals(3, refs.size());
	}
	
	@Test
	public void test5() {
		List<DataRef> refs = CfaDeclarationUtils.parseHierarchicalElementaryDataRefsWithWildcard("compound_db1.arr4[*].c1", cfa, it -> it.getDisplayName());
		// match: compound_db1.arr4[8].c1, compound_db1.arr4[9].c1, compound_db1.arr4[10].c1
		Assert.assertEquals(3, refs.size());
	}
	
	@Test
	public void test6() {
		List<DataRef> refs = CfaDeclarationUtils.parseHierarchicalElementaryDataRefsWithWildcard("compound_db1.arr4[*]", cfa, it -> it.getDisplayName());
		// match: no match, as compound_db1.arr4[8] is a compound
		Assert.assertEquals(0, refs.size());
	}
	
	@Test
	public void test7() {
		List<DataRef> refs = CfaDeclarationUtils.parseHierarchicalElementaryDataRefsWithWildcard("compound_db2.*1", cfa, it -> it.getDisplayName());
		// match: compound_db2.c1
		Assert.assertEquals(1, refs.size());
	}	
	
	@Test
	public void test8() {
		List<DataRef> refs = CfaDeclarationUtils.parseHierarchicalElementaryDataRefsWithWildcard("compound_db2.?1", cfa, it -> it.getDisplayName());
		// match: compound_db2.c1
		Assert.assertEquals(1, refs.size());
	}
}
