/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.models.cfa.trace;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory;
import cern.plcverif.base.models.cfa.CfaInstanceSafeFactory;
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef;
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructure;
import cern.plcverif.base.models.cfa.cfadeclaration.Field;
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariable;
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariableRef;
import cern.plcverif.base.models.cfa.cfainstance.ArrayVariable;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.expr.ExpressionSafeFactory;

@SuppressWarnings("PMD.VariableNamingConventions")
public class ArrayCfaVariableTraceTest {
	// struct ref_root {
	//     struct ds1 {
	//         int a[0..2];
	//     }
	//     bool b;
	//     ds1 c[0..1];
	// }
	private DataStructure rootDS;
	private DataStructure ds1;
	private Field a;
	private Field b;
	private Field c;

	@Before
	public void init() {
		rootDS = CfaDeclarationSafeFactory.INSTANCE.createCfaNetworkDeclaration("").getRootDataStructure();
		ds1 = CfaDeclarationSafeFactory.INSTANCE.createDataStructure("ds1", rootDS);
		a = CfaDeclarationSafeFactory.INSTANCE.createField("a", ds1, CfaDeclarationSafeFactory.INSTANCE.createArrayType(CfaDeclarationSafeFactory.INSTANCE.createIntType(true, 16), 0, 2));
		b = CfaDeclarationSafeFactory.INSTANCE.createField("b", rootDS, CfaDeclarationSafeFactory.INSTANCE.createBoolType());
		c = CfaDeclarationSafeFactory.INSTANCE.createField("c", rootDS, CfaDeclarationSafeFactory.INSTANCE.createArrayType(CfaDeclarationSafeFactory.INSTANCE.createDataStructureRef(ds1), 0, 1));
	}

	@Test
	public void variableInstantiatorTest() {
		// Arrange
		VariableTraceModel varTrace = new VariableTraceModel();
		CfaNetworkInstance instance = CfaInstanceSafeFactory.INSTANCE.createCfaNetworkInstance("test");

		// Act
		varTrace.build(rootDS);
		varTrace.createSimpleVariables(instance);

		// Assert
		assertEquals(2, instance.getVariables().size());
		for (AbstractVariable var : instance.getVariables()) {
			if (var.getFqn().getIdentifiers().size() == 1) {
				assertEquals("b", var.getFqn().getIdentifiers().get(0));
				assertTrue(var.getType().dataTypeEquals(b.getType()));
			} else {
				assertEquals(4, var.getFqn().getIdentifiers().size());
				assertTrue(var instanceof ArrayVariable);
				assertEquals("c", var.getFqn().getIdentifiers().get(0));
				assertEquals("*", var.getFqn().getIdentifiers().get(1));
				assertEquals("a", var.getFqn().getIdentifiers().get(2));
				assertEquals("*", var.getFqn().getIdentifiers().get(3));
				assertTrue(var.getType().dataTypeEquals(ExpressionSafeFactory.INSTANCE.createIntType(true, 16)));
				ArrayVariable array = (ArrayVariable) var;
				assertEquals(2, array.getDimensions().size());
				assertEquals(2, array.getDimensions().get(0).getSize());
				assertEquals(3, array.getDimensions().get(1).getSize());
			}
		}
	}

	@Test
	public void enumeratedVariableInstantiatorTest() {
		// Arrange
		VariableTraceModel varTrace = new VariableTraceModel();
		CfaNetworkInstance instance = CfaInstanceSafeFactory.INSTANCE.createCfaNetworkInstance("test");

		// Act
		varTrace.build(rootDS);
		varTrace.enumerateArrays();
		varTrace.createSimpleVariables(instance);

		// Assert
		boolean[][] encounteredElements = new boolean[2][3];

		assertEquals(7, instance.getVariables().size());
		for (AbstractVariable var : instance.getVariables()) {
			if (var.getFqn().getIdentifiers().size() == 1) {
				assertEquals("b", var.getFqn().getIdentifiers().get(0));
				assertTrue(var.getType().dataTypeEquals(b.getType()));
			} else {
				assertEquals(4, var.getFqn().getIdentifiers().size());
				assertEquals("c", var.getFqn().getIdentifiers().get(0));
				int firstIndex = Integer.parseInt(var.getFqn().getIdentifiers().get(1));
				assertEquals("a", var.getFqn().getIdentifiers().get(2));
				int secondIndex = Integer.parseInt(var.getFqn().getIdentifiers().get(3));
				assertTrue(var.getType().dataTypeEquals(ExpressionSafeFactory.INSTANCE.createIntType(true, 16)));
				encounteredElements[firstIndex][secondIndex] = true;
			}
		}

		for (int i = 0; i < 2; ++i) {
			for (int j = 0; j < 3; ++j) {
				assertTrue(encounteredElements[i][j]);
			}
		}
	}

	@Test
	public void refTracingTest1() {
		// Arrange
		VariableTraceModel varTrace = new VariableTraceModel();
		CfaNetworkInstance instance = CfaInstanceSafeFactory.INSTANCE.createCfaNetworkInstance("test");

		varTrace.build(rootDS);
		varTrace.createSimpleVariables(instance);

		DataRef ref_b = CfaDeclarationSafeFactory.INSTANCE.createFieldRef(b);
		DataRef ref_c_1_a_1 =
				CfaDeclarationSafeFactory.INSTANCE
						.appendIndexing(
								CfaDeclarationSafeFactory.INSTANCE
										.appendFieldRef(
												CfaDeclarationSafeFactory.INSTANCE.appendIndexing(
														CfaDeclarationSafeFactory.INSTANCE.createFieldRef(c),
														1),
								a),
								1
						);

		// Act
		AbstractVariableRef ref_bVR = varTrace.transformHierarchicalReference(ref_b);
		AbstractVariableRef ref_c_aVR = varTrace.transformHierarchicalReference(ref_c_1_a_1);

		// Assert
		assertEquals(2, instance.getVariables().size());
		for (AbstractVariable var : instance.getVariables()) {
			if (var.getFqn().getIdentifiers().size() == 1) {
				assertEquals("b", var.getFqn().getIdentifiers().get(0));
				assertTrue(var.getType().dataTypeEquals(b.getType()));

				assertEquals(var, ref_bVR.getVariable());
			} else {
				assertEquals(4, var.getFqn().getIdentifiers().size());
				assertTrue(var instanceof ArrayVariable);
				assertEquals("c", var.getFqn().getIdentifiers().get(0));
				assertEquals("*", var.getFqn().getIdentifiers().get(1));
				assertEquals("a", var.getFqn().getIdentifiers().get(2));
				assertEquals("*", var.getFqn().getIdentifiers().get(3));
				assertTrue(var.getType().dataTypeEquals(ExpressionSafeFactory.INSTANCE.createIntType(true, 16)));
				ArrayVariable array = (ArrayVariable) var;
				assertEquals(2, array.getDimensions().size());
				assertEquals(2, array.getDimensions().get(0).getSize());
				assertEquals(3, array.getDimensions().get(1).getSize());

				assertEquals(var, ref_c_aVR.getVariable());
			}
		}
	}

	@Test
	public void enumeratedRefTracingTest1() {
		// Arrange
		VariableTraceModel varTrace = new VariableTraceModel();
		CfaNetworkInstance instance = CfaInstanceSafeFactory.INSTANCE.createCfaNetworkInstance("test");

		varTrace.build(rootDS);
		varTrace.enumerateArrays();
		varTrace.createSimpleVariables(instance);

		DataRef ref_b = CfaDeclarationSafeFactory.INSTANCE.createFieldRef(b);
		DataRef ref_c_1_a_0 =
				CfaDeclarationSafeFactory.INSTANCE.appendIndexing(
						CfaDeclarationSafeFactory.INSTANCE.appendFieldRef(
								CfaDeclarationSafeFactory.INSTANCE.appendIndexing(
										CfaDeclarationSafeFactory.INSTANCE.createFieldRef(c),
										1),
								a),
						0);

		// Act
		AbstractVariableRef ref_bVR = varTrace.transformHierarchicalReference(ref_b);
		AbstractVariableRef ref_c_aVR = varTrace.transformHierarchicalReference(ref_c_1_a_0);

		// Assert
		assertEquals(7, instance.getVariables().size());
		boolean foundArrayRef = false;
		for (AbstractVariable var : instance.getVariables()) {
			if (var.getFqn().getIdentifiers().size() == 1) {
				assertEquals("b", var.getFqn().getIdentifiers().get(0));
				assertTrue(var.getType().dataTypeEquals(b.getType()));

				assertEquals(var, ref_bVR.getVariable());
			} else if (var.getFqn().getIdentifiers().get(1).equals("1")
					&& var.getFqn().getIdentifiers().get(3).equals("0")) {
				foundArrayRef = true;
				assertEquals(4, var.getFqn().getIdentifiers().size());
				assertEquals("c", var.getFqn().getIdentifiers().get(0));
				assertEquals("a", var.getFqn().getIdentifiers().get(2));
				assertTrue(var.getType().dataTypeEquals(ExpressionSafeFactory.INSTANCE.createIntType(true, 16)));

				assertEquals(var, ref_c_aVR.getVariable());

			}
		}
		assertTrue(foundArrayRef);
	}

}
