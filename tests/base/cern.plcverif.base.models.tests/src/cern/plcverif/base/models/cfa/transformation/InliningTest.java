/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.models.cfa.transformation;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import cern.plcverif.base.common.emf.XmiToEObject;
import cern.plcverif.base.common.emf.textual.EmfModelPrinter;
import cern.plcverif.base.common.emf.textual.SimpleEObjectToText;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.base.models.cfa.cfabase.CfabaseFactory;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.cfa.trace.CfaInstantiationTrace;
import cern.plcverif.base.models.cfa.validation.CfaValidation;
import cern.plcverif.library.testmodels.TestModel;
import cern.plcverif.library.testmodels.TestModel.TestModelTag;
import cern.plcverif.library.testmodels.TestModelCollection;

public class InliningTest {
	@Rule
	public TemporaryFolder folder = new TemporaryFolder();
	
	/**
	 * Checks whether inlining (with different options) results in valid models.
	 */
	@Test
	public void inliningSmokeTest() throws IOException {
		for (TestModel testModel : TestModelCollection.getInstance().getAllModelsWithTag(TestModelTag.VALID)) {
			smokeTest(testModel);
		}
	}

	@Test
	public void inliningRegressionTest() throws IOException {
		CfabaseFactory.eINSTANCE.createArrayType();

		for (int i = 1; i <= 7; i++) {
			String modelName = String.format("FuncInlTest%s.scl", i);
			regressionTest(modelName);
		}
	}

	private void regressionTest(String modelName) throws IOException {
		// Load input
		String inputCfdPath = String.format("/cern/plcverif/base/cfa/serialization/transformation/%s.cfd", modelName);
		CfaNetworkDeclaration inputCfd = XmiToEObject.deserializeFromString(
				IoUtils.readResourceFile(inputCfdPath, this.getClass().getClassLoader()), CfaNetworkDeclaration.class);

		// Instantiate
		CfaInstantiationTrace traceModel = CfaInstantiator.transformCfaNetwork(inputCfd, false);
		CallInliner.transform(traceModel.getInstance());
		CfaNetworkInstance actualCfi = traceModel.getInstance();
		//CfaToXmi.serializeToFile(actualCfi, folder.newFile(modelName + ".cfi.inlined"));

		// Load expected
		String expectedCfiPath = String.format("/cern/plcverif/base/cfa/serialization/transformation/%s.cfi.inlined",
				modelName);
		CfaNetworkInstance expectedCfi = XmiToEObject.deserializeFromString(
				IoUtils.readResourceFile(expectedCfiPath, this.getClass().getClassLoader()), CfaNetworkInstance.class);

		// Compare
		CharSequence actualCfiText = EmfModelPrinter.print(actualCfi, SimpleEObjectToText.INSTANCE);
		CharSequence expectedCfiText = EmfModelPrinter.print(expectedCfi, SimpleEObjectToText.INSTANCE);
		Assert.assertEquals(expectedCfiText, actualCfiText);
	}

	private void smokeTest(TestModel testModel) throws IOException {
		smokeTest(testModel.getCfd(), testModel.getModelId());
	}

	private void smokeTest(CfaNetworkDeclaration declaration, String modelName) {
		// Act
		CfaInstantiationTrace traceModel = CfaInstantiator.transformCfaNetwork(declaration, false);
		CallInliner.transform(traceModel.getInstance());
		CfaInstantiationTrace traceModelWithoutArrays = CfaInstantiator.transformCfaNetwork(declaration, true);
		CallInliner.transform(traceModelWithoutArrays.getInstance());

		// Assert
		// System.out.println(modelName + " INLINING");

		// System.out.print("Validating CFA declaration... ");
		CfaValidation.validate(traceModel.getDeclaration());
		// System.out.println("OK.");

		// System.out.print("Validating CFA instance with arrays... ");
		CfaValidation.validate(traceModel.getInstance());
		// System.out.println("OK.");

		// System.out.print("Validating CFA instance without arrays... ");
		CfaValidation.validate(traceModelWithoutArrays.getInstance());
		// System.out.println("OK.");
	}

}
