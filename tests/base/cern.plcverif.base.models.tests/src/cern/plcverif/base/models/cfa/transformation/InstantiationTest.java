/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.models.cfa.transformation;

import java.io.IOException;

import org.junit.Test;

import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.trace.CfaInstantiationTrace;
import cern.plcverif.base.models.cfa.validation.CfaValidation;
import cern.plcverif.library.testmodels.TestModel;
import cern.plcverif.library.testmodels.TestModel.TestModelTag;
import cern.plcverif.library.testmodels.TestModelCollection;

public class InstantiationTest {
	/**
	 * Checks whether instantiation (with different options) results in valid
	 * models.
	 */
	@Test
	public void instantiationSmokeTest() throws IOException {
		for (TestModel testModel : TestModelCollection.getInstance().getAllModelsWithTag(TestModelTag.VALID)) {
			test(testModel);
		}
	}

	private void test(TestModel testModel) throws IOException {
		test(testModel.getCfd(), testModel.getModelId());
	}

	private void test(CfaNetworkDeclaration declaration, String modelName) throws IOException {
		// Act
		CfaInstantiationTrace traceModel = CfaInstantiator.transformCfaNetwork(declaration, false);
		CfaInstantiationTrace traceModelWithoutArrays = CfaInstantiator.transformCfaNetwork(declaration, true);

		// Assert
		// System.out.println(modelName + " INLINING");

		// System.out.print("Validating CFA declaration... ");
		CfaValidation.validate(traceModel.getDeclaration());
		// System.out.println("OK.");

		// System.out.print("Validating CFA instance with arrays... ");
		CfaValidation.validate(traceModel.getInstance());
		// System.out.println("OK.");

		// System.out.print("Validating CFA instance without arrays... ");
		CfaValidation.validate(traceModelWithoutArrays.getInstance());
		// System.out.println("OK.");
	}
}
