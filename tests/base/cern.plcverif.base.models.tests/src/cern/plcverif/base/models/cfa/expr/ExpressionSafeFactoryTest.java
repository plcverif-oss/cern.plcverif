/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.models.cfa.expr;

import org.junit.Assert;
import org.junit.Test;

import cern.plcverif.base.models.expr.ExpressionSafeFactory;
import cern.plcverif.base.models.expr.IntLiteral;
import cern.plcverif.base.models.expr.IntType;

public class ExpressionSafeFactoryTest {
	private static final ExpressionSafeFactory FACTORY = ExpressionSafeFactory.INSTANCE;
	
	@Test
	public void createIntLiteralTest1() {
		IntLiteral literal = FACTORY.createIntLiteral(0, true, 16);
		
		Assert.assertNotNull(literal);
		Assert.assertTrue(literal.getType() instanceof IntType);
		Assert.assertEquals(true, ((IntType)literal.getType()).isSigned());
		Assert.assertEquals(16, ((IntType)literal.getType()).getBits());
	}
	
	@Test
	public void createIntLiteralTest2() {
		IntLiteral literal = FACTORY.createIntLiteral(0, false, 32);
		
		Assert.assertNotNull(literal);
		Assert.assertTrue(literal.getType() instanceof IntType);
		Assert.assertEquals(false, ((IntType)literal.getType()).isSigned());
		Assert.assertEquals(32, ((IntType)literal.getType()).getBits());
	}
	
	@Test(expected = Exception.class)
	public void createIntLiteralTest3() {
		// exception expected: -1 cannot be unsigned
		FACTORY.createIntLiteral(-1, false, 16);
		Assert.fail();
	}
	
	@Test
	public void createIntLiteralTest4() {
		// max of INT (signed 16-bit)
		IntLiteral literal = FACTORY.createIntLiteral(32_767, true, 16);
		
		Assert.assertNotNull(literal);
		Assert.assertEquals(32767, literal.getValue());
	}
	
	@Test
	public void createIntLiteralTest5() {
		// min of INT (signed 16-bit)
		IntLiteral literal = FACTORY.createIntLiteral(-32_768, true, 16);
		
		Assert.assertNotNull(literal);
		Assert.assertEquals(-32768, literal.getValue());
	}
	
	@Test
	public void createIntLiteralTest6() {
		// max of DINT (signed 32-bit)
		IntLiteral literal = FACTORY.createIntLiteral(0b0111_1111__1111_1111___1111_1111__1111_1111L, true, 32);
		
		Assert.assertNotNull(literal);
		Assert.assertEquals((long)(Math.pow(2, 31) - 1), literal.getValue()); // 2**31 - 1
	}

	@Test
	public void createIntLiteralTest7() {
		// min of DINT (signed 32-bit)
		IntLiteral literal = FACTORY.createIntLiteral(-0b0111_1111__1111_1111___1111_1111__1111_1111L, true, 32);
		
		Assert.assertNotNull(literal);
		Assert.assertEquals((long)(-Math.pow(2, 31) + 1), literal.getValue()); // - (2**31 - 1)
	}
	
	@Test
	public void createIntLiteralTest8() {
		// max of UDINT (unsigned 32-bit)
		IntLiteral literal = FACTORY.createIntLiteral(0b1111_1111__1111_1111___1111_1111__1111_1111L, false, 32);
		
		Assert.assertNotNull(literal);
		Assert.assertEquals((long)(Math.pow(2, 32) - 1), literal.getValue());
	}
}
