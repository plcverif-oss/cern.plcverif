/*******************************************************************************
 * (C) Copyright CERN 2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/

package cern.plcverif.base.models.expr.utils

import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef
import cern.plcverif.base.models.expr.FloatWidth
import org.junit.Assert
import org.junit.Before
import org.junit.Test

import static org.eclipse.emf.ecore.util.EcoreUtil.copy
import cern.plcverif.base.models.cfa.utils.CfaUtils

/**
 * Tests for {@link ExprUtils#conflictingCfdReferences}.
 */
class ConflictingCfdRefsTest {
	extension CfaDeclarationSafeFactory factory = CfaDeclarationSafeFactory.INSTANCE;

	FieldRef aRef;
	FieldRef bRef;
	FieldRef cRef;
	FieldRef dRef;
	FieldRef ceRef;
	FieldRef deRef;

	DataRef fRef;
	DataRef f1Ref;
	DataRef f2Ref;
	DataRef faRef;
	DataRef fbRef;

	DataRef gRef;
	DataRef g1Ref;
	DataRef g2Ref;
	DataRef g18Ref;
	DataRef g19Ref;

	@Before
	def void setup() {
		// a : INT 
		// b : INT
		// c, d : STRUCT e : BOOL END_STRUCT
		// f : ARRAY [1..5] OF REAL
		// g : ARRAY [1..5, 8..9] OF REAL
		val network = createCfaNetworkDeclaration("network");
		val aField = createField("a", network.rootDataStructure, createIntType(true, 16));
		val bField = createField("b", network.rootDataStructure, createIntType(true, 16));
		val innerStruct = createDataStructure("inner", network.rootDataStructure);
		val eField = createField("e", innerStruct, createBoolType);
		val cField = createField("c", network.rootDataStructure, createDataStructureRef(innerStruct));
		val dField = createField("c", network.rootDataStructure, createDataStructureRef(innerStruct));

		val fType = createArrayType(createFloatType(FloatWidth.B16), 1, 5);
		val fField = createField("f", network.rootDataStructure, fType);

		val gType = createArrayType(createArrayType(createFloatType(FloatWidth.B16), 8, 9), 1, 5);
		val gField = createField("g", network.rootDataStructure, gType);

		this.aRef = createFieldRef(aField);
		this.bRef = createFieldRef(bField);
		this.cRef = createFieldRef(cField);
		this.dRef = createFieldRef(dField);
		this.ceRef = appendFieldRef(copy(cRef), eField);
		this.deRef = appendFieldRef(copy(dRef), eField);
		this.fRef = createFieldRef(fField);
		this.f1Ref = appendIndexing(copy(fRef), 1);
		this.f2Ref = appendIndexing(copy(fRef), 2);
		this.faRef = appendIndexing(copy(fRef), copy(aRef));
		this.fbRef = appendIndexing(copy(fRef), copy(bRef));

		this.gRef = createFieldRef(gField);
		this.g1Ref = appendIndexing(copy(gRef), 1);
		this.g2Ref = appendIndexing(copy(gRef), 2);
		this.g18Ref = appendIndexing(appendIndexing(copy(gRef), 1), 8);
		this.g19Ref = appendIndexing(appendIndexing(copy(gRef), 1), 9);
	}

	@Test
	def void distinctVariablesTest() {
		// Distinct atomic variables are not in conflict.
		assertNoConflict(aRef, bRef);

		// Distinct non-atomic variables are not in conflict either.
		assertNoConflict(aRef, cRef);
		assertNoConflict(aRef, dRef);
		assertNoConflict(aRef, ceRef);
		assertNoConflict(aRef, deRef);

		// Distinct array is not in conflict with any other variable
		assertNoConflict(aRef, fRef);
		assertNoConflict(aRef, f1Ref);
		assertNoConflict(cRef, fRef);
		assertNoConflict(cRef, f1Ref);
		assertNoConflict(ceRef, fRef);
		assertNoConflict(ceRef, f1Ref);

		// 'a' and 'f[a]' are not in conflict
		assertNoConflict(aRef, faRef);
	}

	@Test
	def void selfConflictTest() {
		// Every reference is in conflict with itself
		assertConflict(aRef, aRef);
		assertConflict(bRef, bRef);
		assertConflict(cRef, cRef);
		assertConflict(dRef, dRef);
		assertConflict(ceRef, ceRef);
		assertConflict(deRef, deRef);

		assertConflict(fRef, fRef);
		assertConflict(f1Ref, f1Ref);
		assertConflict(faRef, faRef);
	}

	@Test
	def void differentInstancesOfSameStructure() {
		// The different fields with the same data structure are not in conflict
		assertNoConflict(cRef, dRef);
		assertNoConflict(ceRef, dRef);
		assertNoConflict(cRef, deRef);
		assertNoConflict(ceRef, deRef);
	}

	@Test
	def void onedimArrayTest() {
		// Distinct array elements are not in conflict
		assertNoConflict(f1Ref, f2Ref);

		// An array element is always in conflict with the full array
		assertConflict(fRef, f1Ref);
		assertConflict(fRef, f2Ref);
		assertConflict(fRef, faRef);
		assertConflict(fRef, fbRef);

		// If an index is not known in compile time, it is in conflict with every other array element
		assertConflict(faRef, f1Ref);
		assertConflict(faRef, f2Ref);
		assertConflict(faRef, fbRef);
	}

	@Test
	def void twodimArrayTest() {
		// Distinct array elements are not in conflict
		assertNoConflict(g1Ref, g2Ref);
		assertNoConflict(g18Ref, g19Ref);

		// An array element is always in conflict with the full array
		assertConflict(gRef, g1Ref);
		assertConflict(gRef, g18Ref);
		assertConflict(gRef, g19Ref);
		assertConflict(gRef, g2Ref);

		// References to a part of the full array
		assertConflict(g1Ref, g18Ref);
		assertConflict(g1Ref, g19Ref);
		assertNoConflict(g2Ref, g18Ref);
		assertNoConflict(g2Ref, g19Ref);
	}

	private def void assertNoConflict(DataRef ref1, DataRef ref2) {
		Assert.assertFalse(CfaUtils.conflictingCfdReferences(ref1, ref2));
		Assert.assertFalse(CfaUtils.conflictingCfdReferences(ref2, ref1));
	}

	private def void assertConflict(DataRef ref1, DataRef ref2) {
		Assert.assertTrue(CfaUtils.conflictingCfdReferences(ref1, ref2));
		Assert.assertTrue(CfaUtils.conflictingCfdReferences(ref2, ref1));
	}
}
