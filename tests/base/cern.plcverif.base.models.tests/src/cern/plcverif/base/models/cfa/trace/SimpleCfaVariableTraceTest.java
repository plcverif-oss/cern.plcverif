/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.models.cfa.trace;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory;
import cern.plcverif.base.models.cfa.CfaInstanceSafeFactory;
import cern.plcverif.base.models.cfa.cfadeclaration.DataRef;
import cern.plcverif.base.models.cfa.cfadeclaration.DataStructure;
import cern.plcverif.base.models.cfa.cfadeclaration.Field;
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariable;
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariableRef;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;

@SuppressWarnings("PMD.VariableNamingConventions")
public class SimpleCfaVariableTraceTest {
	// struct _root {
	//     struct ds1 {
	//         int a;
	//     }
	//     bool b;
	//     ds1 c;
	// }
	private DataStructure rootDS;
	private DataStructure ds1;
	private Field a;
	private Field b;
	private Field c;

	@Before
	public void init() {
		rootDS = CfaDeclarationSafeFactory.INSTANCE.createCfaNetworkDeclaration("").getRootDataStructure();
		ds1 = CfaDeclarationSafeFactory.INSTANCE.createDataStructure("ds1", rootDS);
		a = CfaDeclarationSafeFactory.INSTANCE.createField("a", ds1,
				CfaDeclarationSafeFactory.INSTANCE.createIntType(true, 16));
		b = CfaDeclarationSafeFactory.INSTANCE.createField("b", rootDS,
				CfaDeclarationSafeFactory.INSTANCE.createBoolType());
		c = CfaDeclarationSafeFactory.INSTANCE.createField("c", rootDS,
				CfaDeclarationSafeFactory.INSTANCE.createDataStructureRef(ds1));
	}

	@Test
	public void variableInstantiatorTest() {
		// Arrange
		VariableTraceModel varTrace = new VariableTraceModel();
		CfaNetworkInstance instance = CfaInstanceSafeFactory.INSTANCE.createCfaNetworkInstance("test");

		// Act
		varTrace.build(rootDS);
		varTrace.createSimpleVariables(instance);

		// Assert
		assertEquals(2, instance.getVariables().size());
		for (AbstractVariable var : instance.getVariables()) {
			if (var.getFqn().getIdentifiers().size() == 1) {
				assertEquals("b", var.getFqn().getIdentifiers().get(0));
				assertTrue(var.getType().dataTypeEquals(b.getType()));
			}
			else {
				assertEquals(2, var.getFqn().getIdentifiers().size());
				assertEquals("c", var.getFqn().getIdentifiers().get(0));
				assertEquals("a", var.getFqn().getIdentifiers().get(1));
				assertTrue(var.getType().dataTypeEquals(a.getType()));
			}
		}
	}

	@Test
	public void refTracingTest1() {
		// Arrange
		VariableTraceModel varTrace = new VariableTraceModel();
		CfaNetworkInstance instance = CfaInstanceSafeFactory.INSTANCE.createCfaNetworkInstance("test");

		varTrace.build(rootDS);
		varTrace.createSimpleVariables(instance);

		DataRef ref_b = CfaDeclarationSafeFactory.INSTANCE.createFieldRef(b);
		DataRef ref_c_a = CfaDeclarationSafeFactory.INSTANCE
				.appendFieldRef(CfaDeclarationSafeFactory.INSTANCE.createFieldRef(c), a);

		// Act
		AbstractVariableRef _bVR = varTrace.transformHierarchicalReference(ref_b);
		AbstractVariableRef _c_aVR = varTrace.transformHierarchicalReference(ref_c_a);

		// Assert
		assertEquals(2, instance.getVariables().size());
		for (AbstractVariable var : instance.getVariables()) {
			if (var.getFqn().getIdentifiers().size() == 1) {
				assertEquals("b", var.getFqn().getIdentifiers().get(0));
				assertTrue(var.getType().dataTypeEquals(b.getType()));

				assertEquals(var, _bVR.getVariable());
			} else {
				assertEquals(2, var.getFqn().getIdentifiers().size());
				assertEquals("c", var.getFqn().getIdentifiers().get(0));
				assertEquals("a", var.getFqn().getIdentifiers().get(1));
				assertTrue(var.getType().dataTypeEquals(a.getType()));

				assertEquals(var, _c_aVR.getVariable());
			}
		}
	}

}
