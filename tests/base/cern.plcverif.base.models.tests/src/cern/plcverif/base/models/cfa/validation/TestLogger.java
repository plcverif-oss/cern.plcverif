package cern.plcverif.base.models.cfa.validation;

import cern.plcverif.base.common.logging.IPlcverifLogger;
import cern.plcverif.base.common.logging.PlcverifSeverity;

/**
 * Test logger for mocking real {@link IPlcverifLogger} implementations. It can
 * return the number of error messages logged.
 */
class TestLogger implements IPlcverifLogger {
	int errorLogItems = 0;

	public void log(PlcverifSeverity severity, String messageFormat, Object... args) {
		if (severity == PlcverifSeverity.Error) {
			errorLogItems++;
		}
	}

	public void log(PlcverifSeverity severity, String message, Throwable throwable) {
		if (severity == PlcverifSeverity.Error) {
			errorLogItems++;
		}
	}

	/**
	 * Number of log items logged with {@link PlcverifSeverity#Error} severity.
	 */
	public int getErrorCount() {
		return errorLogItems;
	}
}