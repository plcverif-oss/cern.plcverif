package cern.plcverif.base.models.cfa.validation

import cern.plcverif.base.models.cfa.cfabase.CfabaseFactory
import cern.plcverif.base.models.cfa.cfainstance.ArrayVariable
import cern.plcverif.base.models.cfa.cfainstance.CfainstanceFactory
import cern.plcverif.base.models.expr.ExprFactory
import cern.plcverif.base.models.expr.IntLiteral
import org.eclipse.emf.ecore.EObject
import org.junit.Assert
import org.junit.Test

class CfiValidationTest {
	extension CfainstanceFactory factory = CfainstanceFactory.eINSTANCE;
	extension ExprFactory exprFactory = ExprFactory.eINSTANCE;
	extension CfabaseFactory baseFactory = CfabaseFactory.eINSTANCE;

	/**
	 * Returns {@code var : ARRAY [1..5] OF BOOL}.
	 */
	private def ArrayVariable createTestArrayVariable() {
		return createArrayVariable() => [
			name = "var"
			fqn = createFullyQualifiedName() => [identifiers.add("var")]
			type = createBoolType

			dimensions.add(createArrayDimension() => [
				defined = true
				lowerIndex = 1
				upperIndex = 5
			]);
		];
	}

	@Test
	def void arrayIndexingTest1() {
		// var : ARRAY [1..5] OF BOOL
		val arrayVar = createTestArrayVariable();

		// var[0]
		val arrayRef = createArrayElementRef() => [
			variable = arrayVar
			indices.add(createSignedInt16Literal(0))
		];

		val testLogger = validateAndReturnTestLogger(arrayRef);

		// 0 is out of range --> expect 1 error
		Assert.assertEquals(1, testLogger.errorCount);
	}

	@Test
	def void arrayIndexingTest2() {
		// var : ARRAY [1..5] OF BOOL
		val arrayVar = createTestArrayVariable();

		// var[1,3]
		val arrayRef = createArrayElementRef() => [
			variable = arrayVar
			indices.add(createSignedInt16Literal(1))
			indices.add(createSignedInt16Literal(3))
		];

		val testLogger = validateAndReturnTestLogger(arrayRef);

		// Too many indices --> expect 1 error
		Assert.assertEquals(1, testLogger.errorCount);
	}

	@Test
	def void arrayIndexingTest3() {
		// var : ARRAY [1..5] OF BOOL
		val arrayVar = createTestArrayVariable();

		// var[2] with unsigned index
		val arrayRef = createArrayElementRef() => [
			variable = arrayVar
			indices.add(createIntLiteral() => [
				value = 2
				type = createIntType() => [
					bits = 16
					signed = false
				]
			])
		];

		val testLogger = validateAndReturnTestLogger(arrayRef);

		// unsigned index --> expect 1 error
		Assert.assertEquals(1, testLogger.errorCount);
	}

	// Helper methods
	// --------------
	private def IntLiteral createSignedInt16Literal(long intval) {
		return createIntLiteral() => [
			value = intval
			type = createIntType() => [
				bits = 16
				signed = true
			]
		];
	}

	private def validateAndReturnTestLogger(EObject e) {
		val testLogger = new TestLogger();
		val validator = new CfaInstanceValidation(null, testLogger);
		validator.check(e);
		return testLogger;
	}
}
