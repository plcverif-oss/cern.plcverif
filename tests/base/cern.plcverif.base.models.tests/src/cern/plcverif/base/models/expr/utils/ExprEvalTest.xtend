/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.expr.utils

import cern.plcverif.base.common.emf.EObjectMap
import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory
import cern.plcverif.base.models.expr.AtomicExpression
import cern.plcverif.base.models.expr.BoolLiteral
import cern.plcverif.base.models.expr.Literal
import cern.plcverif.base.models.expr.Type
import org.junit.Assert
import org.junit.Test
import cern.plcverif.base.models.expr.IntLiteral
import cern.plcverif.base.models.expr.IntType

class ExprEvalTest {
	@Extension
	static final CfaDeclarationSafeFactory FACTORY = CfaDeclarationSafeFactory.INSTANCE;
	
	static final ExprEval commonEvaluator = new ExprEval();
	
	@Test
	def test1() {
		val a = sym("a", createBoolType);
		val expr = neg(a);
		
		val EObjectMap<AtomicExpression, Literal> valuations = new EObjectMap<AtomicExpression, Literal>();
		valuations.put(a, falseLiteral);
		
		val evaluator = new ExprEval(valuations);
		
		Assert.assertFalse(commonEvaluator.isComputableConstant(expr));
		Assert.assertTrue(evaluator.isComputableConstant(expr));
		
		// NOT a == true, assuming a=false
		val evalResult = evaluator.compute(expr);
		Assert.assertTrue(evalResult instanceof BoolLiteral);
		Assert.assertTrue((evalResult as BoolLiteral).value);
	}
	
	@Test
	def test2() {
		val a = sym("a", createBoolType);
		val b = sym("b", createBoolType);
		val expr = or(neg(a), b);
		
		val EObjectMap<AtomicExpression, Literal> valuations = new EObjectMap<AtomicExpression, Literal>();
		valuations.put(a, trueLiteral);
		valuations.put(b, falseLiteral);
		
		val evaluator = new ExprEval(valuations);
		
		Assert.assertFalse(commonEvaluator.isComputableConstant(expr));
		Assert.assertTrue(evaluator.isComputableConstant(expr));
		
		// ((NOT a) OR b) == false, assuming a=true, b=false
		val evalResult = evaluator.compute(expr);
		Assert.assertTrue(evalResult instanceof BoolLiteral);
		Assert.assertFalse((evalResult as BoolLiteral).value);
	}
	
	@Test
	def test3() {
		val a = sym("a", createIntType(true, 16));
		val b = sym("b", createBoolType);
		val intConst = createIntLiteral(5, true, 16);
		
		val expr = and(gt(a, intConst), b);
		
		val EObjectMap<AtomicExpression, Literal> valuations = new EObjectMap<AtomicExpression, Literal>();
		valuations.put(a, createIntLiteral(9, true, 16));
		valuations.put(b, trueLiteral);
		
		val evaluator = new ExprEval(valuations);
		
		Assert.assertFalse(commonEvaluator.isComputableConstant(expr));
		Assert.assertTrue(evaluator.isComputableConstant(expr));
		
		// ((a > 5) AND b) == true, assuming a=9, b=true
		val evalResult = evaluator.compute(expr);
		Assert.assertTrue(evalResult instanceof BoolLiteral);
		Assert.assertTrue((evalResult as BoolLiteral).value);
	}
	
	@Test
	def test4() {
		val a = sym("a", createIntType(true, 16));
		val intConst = createIntLiteral(1, true, 16);
		val expr = neg(eq(a,intConst));
		
		val EObjectMap<AtomicExpression, Literal> valuations = new EObjectMap<AtomicExpression, Literal>();
		valuations.put(a, createIntLiteral(2, true, 16));
		
		val evaluator = new ExprEval(valuations);
		
		Assert.assertFalse(commonEvaluator.isComputableConstant(expr));
		Assert.assertTrue(evaluator.isComputableConstant(expr));
		
		// NOT (a == 1) == true, assuming a=2
		val evalResult = evaluator.compute(expr);
		Assert.assertTrue(evalResult instanceof BoolLiteral);
		Assert.assertTrue((evalResult as BoolLiteral).value);
	}
	
	@Test
	def test5() {
		val a = sym("a", createIntType(true, 16));
		val b = sym("b", createIntType(true, 16));
		val intConst = createIntLiteral(1, true, 16);
		val expr = neg(and(eq(a,intConst),eq(b,intConst)));
		
		val EObjectMap<AtomicExpression, Literal> valuations = new EObjectMap<AtomicExpression, Literal>();
		valuations.put(a, createIntLiteral(2, true, 16));
		valuations.put(b, createIntLiteral(3, true, 16));
		
		val evaluator = new ExprEval(valuations);
		
		Assert.assertFalse(commonEvaluator.isComputableConstant(expr));
		Assert.assertTrue(evaluator.isComputableConstant(expr));
		
		// NOT (a == 1 and b==1) == true, assuming a=2,b=3
		val evalResult = evaluator.compute(expr);
		Assert.assertTrue(evalResult instanceof BoolLiteral);
		Assert.assertTrue((evalResult as BoolLiteral).value);
	}
	
	@Test
	def overflowTest1() {
		// (int16) int32#32768 = -32768	
		intDownCastTest(32768, createIntType(true, 32),createIntType(true, 16), -32768);
		
		// (int16) int32#32769 = -32767	
		intDownCastTest(32769, createIntType(true, 32),createIntType(true, 16), -32767);
		
		// (int16) int32#-32768 = -32768		
		intDownCastTest(-32768, createIntType(true, 32),createIntType(true, 16), -32768);
		
		// (int16) int32#-32769 = 32767		
		intDownCastTest(-32769, createIntType(true, 32),createIntType(true, 16), 32767);
	}
	
	@Test
	def overflowTest2() {
		// (uint16) int32#32768 = 32768		
		intDownCastTest(32768, createIntType(true, 32),createIntType(false, 16), 32768);
		
		// (uint16) int32#65535 = 65535		
		intDownCastTest(65535, createIntType(true, 32),createIntType(false, 16), 65535);
		
		// (uint16) int32#65536 = 0		
		intDownCastTest(65536, createIntType(true, 32),createIntType(false, 16), 0);
		
		// (uint16) int32#-1 = 65535		
		intDownCastTest(-1, createIntType(true, 32),createIntType(false, 16), 65535);
	}
	
	@Test
	def overflowTest3() {
		// (int8) int32#128 = -128
		intDownCastTest(128, createIntType(true, 32),createIntType(true, 8), -128);
		
		// (int8) int32#129 = -127
		intDownCastTest(129, createIntType(true, 32),createIntType(true, 8), -127);
		
		// (int8) int32#127 = 127	
		intDownCastTest(127, createIntType(true, 32),createIntType(true, 8), 127);
		
		// (int8) int32#-128 = -128		
		intDownCastTest(-128, createIntType(true, 32),createIntType(true, 8), -128);
		
		// (int8) int32#-129 = 127		
		intDownCastTest(-129, createIntType(true, 32),createIntType(true, 8), 127);
	}
	
	private def intDownCastTest(long value, IntType originalType, IntType castedType, long expectedValue) {
		val expr = createTypeConversion(createIntLiteral(value, originalType), castedType);
		
		Assert.assertTrue(commonEvaluator.isComputableConstant(expr));
		
		val evalResult = commonEvaluator.compute(expr);
		Assert.assertTrue(evalResult instanceof IntLiteral);
		Assert.assertEquals(expectedValue, (evalResult as IntLiteral).value);
	}
	
	private static def AtomicExpression sym(String name, Type type) {
		val ret = createUninterpretedSymbol(name);
		ret.type = type;
		return ret;
	}
}