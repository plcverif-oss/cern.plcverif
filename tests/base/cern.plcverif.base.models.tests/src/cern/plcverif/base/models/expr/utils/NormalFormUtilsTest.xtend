/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.models.expr.utils

import org.junit.Test
import cern.plcverif.base.models.expr.ExpressionSafeFactory
import cern.plcverif.base.models.expr.Expression
import org.junit.Assert
import cern.plcverif.base.models.expr.string.ExprToString

class NormalFormUtilsTest {
	@Extension
	static final ExpressionSafeFactory FACTORY = ExpressionSafeFactory.INSTANCE;
	
	@Test
	def test1() {
		// (a AND b) OR (c AND d)
		val input = or(and(sym("a"), sym("b")), and(sym("c"), sym("d")));
		val cnf = NormalFormUtils.toConjunctiveNf(input);
		val expected = '(((?c? || ?a?) && (?c? || ?b?)) && ((?d? || ?a?) && (?d? || ?b?)))';
		
		Assert.assertEquals(expected, ExprToString.toDiagString(cnf));
		Assert.assertEquals(4, NormalFormUtils.getCnfClauses(input).size);
	}
	
	@Test
	def test2() {
		// NOT ((a AND b) OR (c AND d))
		val input = neg(or(and(sym("a"), sym("b")), and(sym("c"), sym("d"))));
		val cnf = NormalFormUtils.toConjunctiveNf(input);
		val expected = '(((! ?a?) || (! ?b?)) && ((! ?c?) || (! ?d?)))';
		
		Assert.assertEquals(expected, ExprToString.toDiagString(cnf));
		Assert.assertEquals(2, NormalFormUtils.getCnfClauses(input).size);
	}
	
	@Test
	def test3() {
		// D AND (A OR ((B1 OR B2) AND (C1 OR C2)) 
		val input =  and(sym("d"), or(sym("a"), and(or(sym("b1"), sym("b2")), or(sym("c1"), sym("c2")))));
		val cnf = NormalFormUtils.toConjunctiveNf(input);
		val expected = '(?d? && ((?a? || (?b1? || ?b2?)) && (?a? || (?c1? || ?c2?))))';
		
		Assert.assertEquals(expected, ExprToString.toDiagString(cnf));
		Assert.assertEquals(3, NormalFormUtils.getCnfClauses(input).size);
	}
	
	@Test
	def test4() {
		// A OR (B AND (C AND D))
		val input =  or(sym("a"), and(sym("b"), and(sym("c"), sym("d"))));
		val cnf = NormalFormUtils.toConjunctiveNf(input);
		val expected = '((?a? || ?b?) && ((?a? || ?c?) && (?a? || ?d?)))';
		
		Assert.assertEquals(expected, ExprToString.toDiagString(cnf));
		Assert.assertEquals(3, NormalFormUtils.getCnfClauses(input).size);
	}
	
	
	private static def Expression sym(String name) {
		return createUninterpretedSymbol(name);
	}
}