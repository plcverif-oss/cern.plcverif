package cern.plcverif.base.models.cfa.transformation

import cern.plcverif.base.common.emf.XmiToEObject
import cern.plcverif.base.common.utils.IoUtils
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration
import cern.plcverif.base.models.cfa.textual.CfaToString
import cern.plcverif.base.models.cfa.validation.CfaValidation
import org.junit.Assert
import org.junit.Test

class CfdInitialAssignmentsToTransitionTest {
	@Test
	def test1() {
		val modelName = "CfdInitAmtTransformationTest1.scl";
		val expected = "[instance/u1/a := true, instance/u1/b := 13, instance/u2/a := true, instance/u2/b := 13]";

		runTest(modelName, expected);
	}

	@Test
	def test2() {
		val modelName = "CfdInitAmtTransformationTest2.scl";
		val expected = "[test2_idb/u1/a := true, test2_idb/u1/b := 13, test2_idb/u2/a := true, test2_idb/u2/b := 13, test2_sdb/b := 55, test2_sdb/a := true, instance/u1/a := true, instance/u1/b := 13, instance/u2/a := true, instance/u2/b := 13]";

		runTest(modelName, expected);
	}
	
	@Test
	def test3() {
		val modelName = "CfdInitAmtTransformationTest3.scl";
		val expected = "[test3d_sdb/c[1] := 61, test3d_sdb/c[2] := 52]";

		runTest(modelName, expected);
	}

	private def runTest(String modelName, String expectedInitAmts) {
		val inputCfdPath = String.format("/cern/plcverif/base/cfa/serialization/transformation/%s.cfd", modelName);
		val CfaNetworkDeclaration inputCfd = XmiToEObject.deserializeFromString(
			IoUtils.readResourceFile(inputCfdPath, this.getClass().getClassLoader()), CfaNetworkDeclaration);

		val initAmts = CfdInitialAssignmentsToTransition.getInstantiatedInitAmts(inputCfd);
		CfaValidation.validate(inputCfd);

		val initAmtsString = CfaToString.INSTANCE.toString(initAmts).toString;
		Assert.assertEquals(expectedInitAmts, initAmtsString);
	}
}
