/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.common.settings

import java.util.List
import org.junit.Test
import org.junit.Assert
import cern.plcverif.base.common.settings.exceptions.SettingsParserException

class SettingsSerializerTest {
	@Test
	def elementSerializationTest1() {
		val root = SettingsElement.empty();
		root.setSimpleAttribute("key1", "val1");
		root.setSimpleAttribute("key2", "val2");

		Assert.assertEquals(
			'''
				-key1 = val1
				-key2 = val2
			'''.toString.trim,
			SettingsSerializer.serialize(root).trim
		);

		Assert.assertEquals(
			'''-key1=val1 -key2=val2'''.toString.trim,
			SettingsSerializer.serialize(root, true).trim
		);
	}

	@Test
	def elementSerializationTest2() {
		val root = SettingsElement.empty();
		root.setSimpleAttribute("key1", "val1");
		root.setSimpleAttribute("key2", "value with space");

		Assert.assertEquals(
			'''
				-key1 = val1
				-key2 = "value with space"
			'''.toString.trim,
			SettingsSerializer.serialize(root).trim
		);

		Assert.assertEquals(
			'''-key1=val1 -key2="value with space"'''.toString.trim,
			SettingsSerializer.serialize(root, true).trim
		);
	}

	@Test
	def elementSerializationTest3() {
		val root = SettingsElement.empty();
		root.setSimpleAttribute("key1", "val1");
		root.setSimpleAttribute("key2", "value with \"quote\" inside");

		Assert.assertEquals(
			'''
				-key1 = val1
				-key2 = "value with ''quote'' inside"
			'''.toString.trim,
			SettingsSerializer.serialize(root).trim
		);

		Assert.assertEquals(
			'''-key1=val1 -key2="value with ''quote'' inside"'''.toString.trim,
			SettingsSerializer.serialize(root, true).trim
		);
	}
	
	@Test
	def elementSerializationTest4() {
		val root = SettingsElement.empty();
		root.setSimpleAttribute("key1", "");
		root.setSimpleAttribute("key2", " ");

		Assert.assertEquals(
			'''
				-key1 = ""
				-key2 = " "
			'''.toString.trim,
			SettingsSerializer.serialize(root).trim
		);

		Assert.assertEquals(
			'''-key1="" -key2=" "'''.toString.trim,
			SettingsSerializer.serialize(root, true).trim
		);
	}

	@Test
	def elementParsingTest1() {
		// All settings in one line, no lists
		assertCorrectlyParsed("key1:val1; key2:val2", "-key1=val1  -key2=val2");
		assertCorrectlyParsed("key1:val1; key2.sub:val3; key2:val2", "-key1=val1  -key2=val2   -key2.sub=val3");
		assertCorrectlyParsed("key1:val1; key2.sub:val3; key2:null", "-key1=val1 -key2.sub=val3");
		assertCorrectlyParsed("key1:val1; key2:val2", "-key1   =   val1  -key2=val2");
		assertCorrectlyParsed("key1:val1; key2:val2", '''-key1   =   "val1"  -key2=val2''');
		assertCorrectlyParsed("key1:val1; key2:val2", '''-key1="val1"  -key2=val2''');
		assertCorrectlyParsed("key1:v a l 1; key2:val2", '''-key1="v a l 1"  -key2=val2''');
		assertCorrectlyParsed("key1:v-a-l-1; key2:val2", '''-key1="v-a-l-1"  -key2=val2''');
		assertCorrectlyParsed("key1:v a l 1; key2:val2", '''-key1  =  "v a l 1"  -key2=val2''');
		assertCorrectlyParsed("key1:v-a-l-1; key2:val2", '''-key1 = "v-a-l-1"  -key2=val2''');
		assertCorrectlyParsed("key1:; key2:", '''-key1=""  -key2=""''');
	}
	
	@Test
	def elementParsingTest2() {
		// One setting per line, no lists
		//@formatter:off
		assertCorrectlyParsed("key1:val1; key2:val2", 
			'''
			-key1=val1
			-key2=val2''');
		assertCorrectlyParsed("key1:val1; key2.sub:val3; key2:val2",
			'''
			-key1=val1
			-key2=val2
			-key2.sub=val3''');
		assertCorrectlyParsed("key1:val1; key2.sub:val3; key2:null", 
			'''
			-key1=val1
			-key2.sub=val3''');
		assertCorrectlyParsed("key1:val1; key2:val2", 
			'''
			-key1   =   val1
			-key2=val2''');
		assertCorrectlyParsed("key1:val1; key2:val2", 
			'''
			-key1   =   "val1"
			-key2=val2''');
		assertCorrectlyParsed("key1:val1; key2:val2", 
			'''
			-key1="val1"
			-key2=val2''');
		assertCorrectlyParsed("key1:v a l 1; key2:val2", 
			'''
			-key1="v a l 1"
			-key2=val2''');
		assertCorrectlyParsed("key1:v-a-l-1; key2:val2", 
			'''
			-key1="v-a-l-1"
			-key2=val2''');
		assertCorrectlyParsed("key1:v a l 1; key2:val2", 
			'''
			-key1  =  "v a l 1"
			-key2=val2''');
		assertCorrectlyParsed("key1:v-a-l-1; key2:val2", 
			'''
			-key1 = "v-a-l-1"
			-key2=val2''');
		//@formatter:on
	}
	
	@Test
	def commentParsingTest1() {
		//@formatter:off
		assertCorrectlyParsed("key1:val1", 
			'''
			-key1=val1
			//-key2=val2''');
		assertCorrectlyParsed("key1:val1; key2:val2", 
			'''
			-key1=val1
			// Comment between two lines
			-key2=val2''');
		assertCorrectlyParsed("key1:val1; key2:val2", 
			'''
			-key1=val1 // Comment after a value
			-key2=val2''');
		assertCorrectlyParsed("key1:val1; key2:val2", 
			'''
			-key1=val1
			-key2=val2     // Comment after a value''');
		//@formatter:on
	}
	
	@Test
	def listParsingTest1() {
		// All settings in one line, lists
		assertCorrectlyParsed("key1(L); key1.0:val1; key1.1:val2", "-key1={val1,val2}");
		assertCorrectlyParsed("key1(L); key1.0:val1; key1.1:val2", "-key1=val1,val2");
		assertCorrectlyParsed("key1(L); key1.0:val1; key1.1:val2", '''-key1="val1","val2"''');
		assertCorrectlyParsed("key1(L); key1.0:val1; key1.1:val2", "-key1={val1, val2}");
		assertCorrectlyParsed("key1(L); key1.0:val1; key1.1:val2", '''-key1={"val1", "val2"}''');
		assertCorrectlyParsed("key1(L); key1.0:val1,val2", '''-key1={"val1,val2"}''');
		assertCorrectlyParsed("key1(L); key1.0:val1,  val2", '''-key1={"val1,  val2"}''');
		
		assertCorrectlyParsed("key1(L); key1.0:val1; key1.1:val2", "-key1.0=val1  -key1.1=val2");
		assertCorrectlyParsed("key1(L); key1.0:val1; key1.2:val2", "-key1.0=val1  -key1.2=val2");
		assertCorrectlyParsed("key1(L); key1.0:val1; key1.2:val2", "-key1.0 = val1  -key1.2 = val2");
		assertCorrectlyParsed("key1(L); key1.0:val1; key1.1:val2", '''-key1.0=val1  -key1.1="val2"''');
		assertCorrectlyParsed("key1(L); key1.0:val1; key1.2:val2", '''-key1.0=val1  -key1.2="val2"''');
		assertCorrectlyParsed("key1(L); key1.0:val1; key1.2:val2", '''-key1.0 = val1  -key1.2 = "val2"''');
	}

	@Test
	def listParsingTest2() {
		// One setting per line, lists
		//@formatter:off
		assertCorrectlyParsed("key1(L); key1.0:val1; key1.1:val2", 
			'''
			-key1.0=val1
			-key1.1=val2
			''');
		assertCorrectlyParsed("key1(L); key1.0:val1; key1.2:val2", 
			'''
			-key1.0=val1
			-key1.2=val2''');
		assertCorrectlyParsed("key1(L); key1.0:val1; key1.2:val2", 
			'''
			-key1.0 = val1
			-key1.2 = val2''');
		assertCorrectlyParsed("key1(L); key1.0:val1; key1.1:val2", 
			'''
			-key1.0=val1
			-key1.1="val2"''');
		assertCorrectlyParsed("key1(L); key1.0:val1; key1.2:val2", 
			'''
			-key1.0=val1
			-key1.2="val2"''');
		assertCorrectlyParsed("key1(L); key1.0:val1; key1.2:val2", 
			'''
			-key1.0 = val1
			-key1.2 = "val2"''');
		//@formatter:on
	}
	
	@Test
	def void nestedQuotationMarkParsingTest1() {
		assertCorrectlyParsed('''key1:This is a long description including some "quote".''', 
			'''-key1="This is a long description including some ''quote''."''');
			assertCorrectlyParsed('''key1(L); key1.0:This is a long description including some "quote".''', 
			'''-key1={"This is a long description including some ''quote''."}''');
	}
	
	@Test
	def void specificParsingCasesTest1() {
		assertCorrectlyParsed("a.b.c:val; a.b:null; a:null", 
			'''-a.b.c = val''');
			
		assertCorrectlyParsed("key1:val1; key2:null", 
		'''-key1=val1  -key2''');
		
		assertCorrectlyParsed("key1:val1; key2:null", 
		'''-key2 -key1=val1''');
	}
	
	@Test(expected = SettingsParserException)
	def void duplicatedElementParserTest1() {
		SettingsSerializer.parseSettingsFromString(
			'''
			-key1 = val1
			-key2 = val2
			-key1 = val3
			'''
		);
	}
	
	@Test(expected = SettingsParserException)
	def void duplicatedElementParserTest2() {
		SettingsSerializer.parseSettingsFromString(
			'''-key1 = val1     -key2 = val2      -key1 = val3
			'''
		);
	}
	
	@Test(expected = SettingsParserException)
	def void listCannotHaveAttributesTest() {
		SettingsSerializer.parseSettingsFromString(
			'''
			-key1 = value
			-key1.0 = item0
			-key1.2 = item2
			'''
		);
	}
	
	
	
	
//----	
	private def void assertCorrectlyParsed(String expectedTextual, String settingsString) {
		Assert.assertEquals(
			expectedTextual,
			SettingsSerializer.parseSettingsFromString(settingsString).allFqnValuePairs
		);
	}

	private static def String allFqnValuePairs(Settings root) {
		return allFqnValuePairList(root).join("; ");
	}

	private static def List<String> allFqnValuePairList(Settings root) {
		val List<String> accu = newArrayList();
		collectAllFqnValuePairs(root, accu);
		return accu.sort;
	} 
	
	private static def dispatch void collectAllFqnValuePairs(Settings root, List<String> fqnAccu) {
		throw new UnsupportedOperationException("Unsupported case.");
	}
	
	private static def dispatch void collectAllFqnValuePairs(Void root, List<String> fqnAccu) {
		// nothing to do
	}
	
	private static def dispatch void collectAllFqnValuePairs(SettingsElement root, List<String> fqnAccu) {
		if (!root.fqn.equals("")) {
			fqnAccu.add(root.fqn + ":" + root.value);
		}
		
		for (attrName : root.attributeNames) {
			collectAllFqnValuePairs(root.getAttribute(attrName).get, fqnAccu);
		}
	} 
	
	private static def dispatch void collectAllFqnValuePairs(SettingsList root, List<String> fqnAccu) {
		fqnAccu.add(root.fqn + "(L)");
		for (item : root.elements) {
			collectAllFqnValuePairs(item, fqnAccu);
		}
	}  
	
}