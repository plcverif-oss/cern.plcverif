/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.common.settings

import java.util.Optional
import org.junit.Assert
import org.junit.Test
import cern.plcverif.base.common.settings.specific.AbstractSpecificSettings
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsElement

class EffectiveSettingsTest {
	static class Specific extends AbstractSpecificSettings {
		@PlcverifSettingsElement(name="key1")
		public String key1 = "Xvalue1";
		
		@PlcverifSettingsElement(name="key2", mandatory = OPTIONAL)
		public String key2 = null;
	}
	
	@Test
	def test1() {
		val settings1 = SettingsSerializer.parseSettingsFromString('''-key1="value1" -key2="value2" -key3="value3"''');
		val settings2 = SettingsSerializer.parseSettingsFromString('''-key1="value1" -key2="Xvalue2" -key3="Xvalue3"''');
		
		val effective = EffectiveSettings.base(Optional.of(settings1)).overrideWith(settings2).effectiveSettingsOrThrow;
		val effectiveStr = SettingsSerializer.serialize(effective.toSingle, true);
		
		assertSettingsTextEquals('''-key1=value1   -key2=Xvalue2   -key3=Xvalue3''', effectiveStr);
	}
	
	@Test
	def test2() {
		val settings1 = SettingsSerializer.parseSettingsFromString('''-key1="value1" -key2="value2" -key3="value3"''');
		val settings2 = SettingsSerializer.parseSettingsFromString('''-key2="Xvalue2" -key3="Xvalue3"''');
		
		val effective = EffectiveSettings.base(Optional.of(settings1)).overrideWith(settings2).effectiveSettingsOrThrow;
		val effectiveStr = SettingsSerializer.serialize(effective.toSingle, true);
		
		assertSettingsTextEquals('''-key1=value1   -key2=Xvalue2   -key3=Xvalue3''', effectiveStr);
	}
	
	@Test
	def test3() {
		val settings1 = SettingsSerializer.parseSettingsFromString('''-key1="value1" -key3="value3"''');
		val settings2 = SettingsSerializer.parseSettingsFromString('''-key2="Xvalue2" -key3="Xvalue3"''');
		
		val effective = EffectiveSettings.base(Optional.of(settings1)).overrideWith(settings2).overrideWith(Optional.empty).effectiveSettingsOrThrow;
		val effectiveStr = SettingsSerializer.serialize(effective.toSingle, true);
		
		assertSettingsTextEquals('''-key1=value1   -key2=Xvalue2   -key3=Xvalue3''', effectiveStr);
	}
	
	@Test
	def test4() {
		val settings1 = SettingsSerializer.parseSettingsFromString('''-key1="value1" -key2="value2" -key3="value3"''');
		val settings2 = new Specific();
		
		val effective = EffectiveSettings.base(Optional.of(settings1)).overrideWith(settings2).effectiveSettingsOrThrow;
		val effectiveStr = SettingsSerializer.serialize(effective.toSingle, true);
		
		assertSettingsTextEquals('''-key1="Xvalue1" -key2="value2" -key3="value3"''', effectiveStr);
	}
	
	@Test
	def test5() {
		val settings1 = SettingsSerializer.parseSettingsFromString('''-key2="value2" -key3="value3"''');
		val settings2 = new Specific();
		
		val effective = EffectiveSettings.base(Optional.of(settings1)).overrideWith(settings2).effectiveSettingsOrThrow;
		val effectiveStr = SettingsSerializer.serialize(effective.toSingle, true);
		
		assertSettingsTextEquals('''-key1="Xvalue1" -key2="value2" -key3="value3"''', effectiveStr);
	}
	
	@Test
	def test6() {
		// With attributes
		val settings1 = SettingsSerializer.parseSettingsFromString('''-key1="value1" -key1.subkey1=subval1''');
		val settings2 = SettingsSerializer.parseSettingsFromString('''-key1="Xvalue1"''');
		
		val effective = EffectiveSettings.base(Optional.of(settings1)).overrideWith(settings2).effectiveSettingsOrThrow;
		val effectiveStr = SettingsSerializer.serialize(effective.toSingle, true);
		
		assertSettingsTextEquals('''-key1=Xvalue1 -key1.subkey1=subval1''', effectiveStr);
	}
	
	@Test
	def test7() {
		// With simple list
		val settings1 = SettingsSerializer.parseSettingsFromString('''-list.0=A -list.1=B''');
		val settings2 = SettingsSerializer.parseSettingsFromString('''-list.0=X -list.2=Y''');
		
		val effective = EffectiveSettings.base(Optional.of(settings1)).overrideWith(settings2).effectiveSettingsOrThrow;
		val effectiveStr = SettingsSerializer.serialize(effective.toSingle, true);
		
		assertSettingsTextEquals('''-list.0=X -list.1=B -list.2=Y''', effectiveStr);
	}
	
	def static assertSettingsTextEquals(String str1, String str2) {
		Assert.assertEquals(
			str1.trim.replaceAll("\"", "").replaceAll("\\s+"," "),
			str2.trim.replaceAll("\"", "").replaceAll("\\s+"," ")
		);
	}
	
}