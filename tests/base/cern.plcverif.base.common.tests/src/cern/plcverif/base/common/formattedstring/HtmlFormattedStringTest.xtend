/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.common.formattedstring

import cern.plcverif.base.common.formattedstring.Formats.BoldFormat
import cern.plcverif.base.common.formattedstring.Formats.ColorFormat
import cern.plcverif.base.common.formattedstring.Formats.ItalicFormat
import org.junit.Assert
import org.junit.Test

class HtmlFormattedStringTest {
	Formatter formatter = new HtmlFormatter();
	
	@Test
	def test1() {
		val expectedHtml = "<p><b>apple</b></p>";
		val BasicFormattedString formattedString = new BasicFormattedString();
		formattedString.append("apple", BoldFormat.INSTANCE);

		Assert.assertEquals(expectedHtml, formattedString.format(formatter));
	}

	@Test
	def test2() {
		val expectedHtml = "<p>pear <b>apple</b></p>";
		val BasicFormattedString formattedString = new BasicFormattedString();
		formattedString.append("pear ");
		formattedString.append("apple", BoldFormat.INSTANCE);

		Assert.assertEquals(expectedHtml, formattedString.format(formatter));
	}

	@Test
	def test3() {
		val expectedHtml = "<p><i>pear</i> <b>apple</b></p>";
		val BasicFormattedString formattedString = new BasicFormattedString();
		formattedString.append("pear", ItalicFormat.INSTANCE);
		formattedString.append(" ");
		formattedString.append("apple", BoldFormat.INSTANCE);

		Assert.assertEquals(expectedHtml, formattedString.format(formatter));
	}

	@Test
	def test4() {
		val expectedHtml = '''<p><font color="#FF00FF">apple</font></p>''';
		val BasicFormattedString formattedString = new BasicFormattedString();
		formattedString.append("apple", new ColorFormat("FF00FF"));

		Assert.assertEquals(expectedHtml, formattedString.format(formatter));
	}
}
