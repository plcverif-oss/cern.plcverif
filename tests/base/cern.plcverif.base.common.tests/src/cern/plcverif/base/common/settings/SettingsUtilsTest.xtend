/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.common.settings

import cern.plcverif.base.common.settings.specific.AbstractSpecificSettings
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsElement
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory
import org.junit.Assert
import org.junit.Test
import static cern.plcverif.base.common.settings.SettingsUtils.tryGetNode;

class SettingsUtilsTest {
	@Test
	def nonConsumedTest1() {
		val settings = SettingsSerializer.parseSettingsFromString("-keyA=valA\n-keyB=valB\n");
		settings.getAttribute("keyA").get.toSingle.value();
		
		Assert.assertTrue(settings.getAttribute("keyA").get.toSingle.isConsumed);
		Assert.assertFalse(settings.getAttribute("keyB").get.toSingle.isConsumed);
		Assert.assertEquals(1, SettingsUtils.nonConsumedSettingsElements(settings).size);
	}
	
	@Test
	def nonConsumedTest2() {
		val settings = SettingsSerializer.parseSettingsFromString('''
		-keyA=valA
		-subkey.keyB=valB
		-subkey.keyC=valC
		''');
		
		settings.getAttribute("keyA").get.toSingle.value();
		
		val result = SettingsUtils.nonConsumedSettingsElements(settings);
		Assert.assertEquals(2, result.size);
		Assert.assertEquals("subkey.keyB", result.get(0).fqn);
		Assert.assertEquals("subkey.keyC", result.get(1).fqn);
		Assert.assertTrue(settings.getAttribute("keyA").get.toSingle.isConsumed);
	}
	
	@Test
	def nonConsumedTest3() {
		val settings = SettingsSerializer.parseSettingsFromString('''
		-keyA=valA
		-subkey.0=valB
		-subkey.1=valC
		-subkey.2=valD
		''');
		
		settings.getAttribute("keyA").get.toSingle.value();
		settings.getAttribute("subkey").get.toList.elements.get(1).toSingle.value();
		
		val result = SettingsUtils.nonConsumedSettingsElements(settings);
		Assert.assertEquals(2, result.size);
		Assert.assertEquals("subkey.0", result.get(0).fqn);
		Assert.assertEquals("subkey.2", result.get(1).fqn);
		Assert.assertTrue(settings.getAttribute("keyA").get.toSingle.isConsumed);
	}
	
	static class TestSpecificSettings extends AbstractSpecificSettings {
		@PlcverifSettingsElement(name = "key1")
		boolean key1;
	
		@PlcverifSettingsElement(name = "key2")
		int key2;
	
		@PlcverifSettingsElement(name = "key3", mandatory = PlcverifSettingsMandatory.OPTIONAL)
		String key3 = "";
	}

	@Test
	def nonConsumedTest4() {
		val settings = SettingsSerializer.parseSettingsFromString('''
			-key1=true
			-key2=5
			-key3=ABCD
			-key3.aaa=ZZZ
			-key4=XXXX
			-key5=123
		''');
		val specSettings = SpecificSettingsSerializer.parse(settings, TestSpecificSettings);

		Assert.assertNotNull(specSettings);
		Assert.assertEquals(true, specSettings.key1);
		Assert.assertEquals(5, specSettings.key2);
		Assert.assertEquals("ABCD", specSettings.key3);
		
		val result = SettingsUtils.nonConsumedSettingsElements(settings);
		Assert.assertEquals(3, result.size);
		Assert.assertEquals("aaa,key4,key5", result.map[it | it.name].sort.join(",")); 
		Assert.assertTrue(settings.getAttribute("key1").get.toSingle.isConsumed);
		Assert.assertTrue(settings.getAttribute("key2").get.toSingle.isConsumed);
		Assert.assertTrue(settings.getAttribute("key3").get.toSingle.isConsumed);
	}
	
	
	@Test 
	def tryGetNodeTest1() {
		// createIfNotExists = false
		
		val settings = SettingsSerializer.parseSettingsFromString('''
			-kAlpha.kBeta = B
			-kAlpha.kGamma = C
			-kAlpha = A
			-kDelta = D
		''');
		
		val beta = tryGetNode(settings, "kAlpha.kBeta", false);
		Assert.assertTrue(beta.isPresent);
		Assert.assertTrue(beta.get instanceof SettingsElement);
		Assert.assertEquals("B", (beta.get as SettingsElement).value);
		
		val alpha = tryGetNode(settings, "kAlpha", false);
		Assert.assertTrue(alpha.isPresent);
		Assert.assertTrue(alpha.get instanceof SettingsElement);
		Assert.assertEquals("A", (alpha.get as SettingsElement).value);
		
		val delta = tryGetNode(settings, "kDelta", false);
		Assert.assertTrue(delta.isPresent);
		Assert.assertTrue(delta.get instanceof SettingsElement);
		Assert.assertEquals("D", (delta.get as SettingsElement).value);
		
		val epsilon = tryGetNode(settings, "kEpsilon", false);
		Assert.assertFalse(epsilon.isPresent);
	}
	
	@Test 
	def tryGetNodeTest2() {
		// createIfNotExists = true
		
		val settings = SettingsSerializer.parseSettingsFromString('''
			-kAlpha.kBeta = B
			-kAlpha.kGamma = C
			-kAlpha = A
			-kDelta = D
		''');
		
		val beta = tryGetNode(settings, "kAlpha.kBeta", true);
		Assert.assertTrue(beta.isPresent);
		Assert.assertTrue(beta.get instanceof SettingsElement);
		Assert.assertEquals("B", (beta.get as SettingsElement).value);
		
		val epsilon = tryGetNode(settings, "kEpsilon", true);
		Assert.assertTrue(epsilon.isPresent);
		Assert.assertTrue(epsilon.get instanceof SettingsElement);
		Assert.assertEquals(null, (epsilon.get as SettingsElement).value);
	}
	
	@Test 
	def tryGetNodeTest3() {
		// createIfNotExists = false
		
		val settings = SettingsSerializer.parseSettingsFromString('''
			-kAlpha.0 = zero
			-kAlpha.1 = one
			-kAlpha.0.kBeta = B
		''');
		
		val zero = tryGetNode(settings, "kAlpha.0", false);
		Assert.assertTrue(zero.isPresent);
		Assert.assertTrue(zero.get instanceof SettingsElement);
		Assert.assertEquals("zero", (zero.get as SettingsElement).value);
		
		val one = tryGetNode(settings, "kAlpha.1", false);
		Assert.assertTrue(one.isPresent);
		Assert.assertTrue(one.get instanceof SettingsElement);
		Assert.assertEquals("one", (one.get as SettingsElement).value);
		
		val two = tryGetNode(settings, "kAlpha.2", false);
		Assert.assertFalse(two.isPresent);
		
		val beta = tryGetNode(settings, "kAlpha.0.kBeta", false);
		Assert.assertTrue(beta.isPresent);
		Assert.assertTrue(beta.get instanceof SettingsElement);
		Assert.assertEquals("B", (beta.get as SettingsElement).value);
		
		val gamma = tryGetNode(settings, "kAlpha.0.kGamma", false);
		Assert.assertFalse(gamma.isPresent);
	}
	
	@Test 
	def tryGetNodeTest4() {
		// createIfNotExists = true
		
		val settings = SettingsSerializer.parseSettingsFromString('''
			-kAlpha.0 = zero
			-kAlpha.1 = one
			-kAlpha.0.kBeta = B
		''');
		
		val zero = tryGetNode(settings, "kAlpha.0", true);
		Assert.assertTrue(zero.isPresent);
		Assert.assertTrue(zero.get instanceof SettingsElement);
		Assert.assertEquals("zero", (zero.get as SettingsElement).value);
		
		val one = tryGetNode(settings, "kAlpha.1", true);
		Assert.assertTrue(one.isPresent);
		Assert.assertTrue(one.get instanceof SettingsElement);
		Assert.assertEquals("one", (one.get as SettingsElement).value);
		
		val two = tryGetNode(settings, "kAlpha.2", true);
		Assert.assertTrue(two.isPresent);
		Assert.assertTrue(two.get instanceof SettingsElement);
		Assert.assertEquals(null, (two.get as SettingsElement).value);
		
		val beta = tryGetNode(settings, "kAlpha.0.kBeta", true);
		Assert.assertTrue(beta.isPresent);
		Assert.assertTrue(beta.get instanceof SettingsElement);
		Assert.assertEquals("B", (beta.get as SettingsElement).value);
		
		val gamma = tryGetNode(settings, "kAlpha.0.kGamma", true);
		Assert.assertTrue(gamma.isPresent);
		Assert.assertTrue(gamma.get instanceof SettingsElement);
		Assert.assertEquals(null, (gamma.get as SettingsElement).value);
	}
}