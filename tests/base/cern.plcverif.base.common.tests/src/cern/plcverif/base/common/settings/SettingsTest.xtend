/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 * 
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.common.settings

import org.junit.Assert
import org.junit.Test
import java.util.Arrays

class SettingsTest {
	@Test
	def parseElementTest1() {
		val settingsText = '''
			-key1 = value1
			-key2 = value2
			-key3 = value3
		'''
		val settings = SettingsSerializer.parseSettingsFromString(settingsText);

		Assert.assertTrue(settings instanceof SettingsElement);
		Assert.assertTrue(settings.getAttribute("key1").get() instanceof SettingsElement);
		Assert.assertTrue((settings.getAttribute("key1").get() as SettingsElement).value == "value1");
		Assert.assertTrue(settings.getAttribute("key2").get() instanceof SettingsElement);
		Assert.assertTrue((settings.getAttribute("key2").get() as SettingsElement).value == "value2");
		Assert.assertTrue(settings.getAttribute("key3").get() instanceof SettingsElement);
		Assert.assertTrue((settings.getAttribute("key3").get() as SettingsElement).value == "value3");
	}

	@Test
	def parseElementTest2() {
		val settingsText = '''
			-group.key1 = value1
			-group.key2 = value2
			-group.key3 = value3
		'''
		val settings = SettingsSerializer.parseSettingsFromString(settingsText);

		Assert.assertTrue(settings instanceof SettingsElement);
		Assert.assertTrue(settings.getAttribute("group").get() instanceof SettingsElement);
		val groupSettings = settings.getAttribute("group").get() as SettingsElement;

		Assert.assertTrue(groupSettings.getAttribute("key1").get() instanceof SettingsElement);
		Assert.assertTrue((groupSettings.getAttribute("key1").get() as SettingsElement).value == "value1");
		Assert.assertTrue(groupSettings.getAttribute("key2").get() instanceof SettingsElement);
		Assert.assertTrue((groupSettings.getAttribute("key2").get() as SettingsElement).value == "value2");
		Assert.assertTrue(groupSettings.getAttribute("key3").get() instanceof SettingsElement);
		Assert.assertTrue((groupSettings.getAttribute("key3").get() as SettingsElement).value == "value3");
	}

	@Test
	def parseListTest1() {
		val settingsText = '''
			-listKey = {"val0", "val1", "val2"}
			-key2 = value2
		'''
		val settings = SettingsSerializer.parseSettingsFromString(settingsText);

		Assert.assertTrue(settings instanceof SettingsElement);
		Assert.assertTrue(settings.getAttribute("listKey").get() instanceof SettingsList);
		val list = settings.getAttribute("listKey").get() as SettingsList;

		for (i : 0 .. 2) {
			Assert.assertTrue(list.elements.get(i) instanceof SettingsElement);
			Assert.assertTrue((list.elements.get(i) as SettingsElement).value == ("val" + i));
		}
	}

	@Test
	def parseListTest2() {
		val settingsText = '''
			-listKey = {val0, val1, val2}
			-key2 = value2
		'''
		val settings = SettingsSerializer.parseSettingsFromString(settingsText);

		Assert.assertTrue(settings instanceof SettingsElement);
		Assert.assertTrue(settings.getAttribute("listKey").get() instanceof SettingsList);
		val list = settings.getAttribute("listKey").get() as SettingsList;

		for (i : 0 .. 2) {
			Assert.assertTrue(list.elements.get(i) instanceof SettingsElement);
			Assert.assertTrue((list.elements.get(i) as SettingsElement).value == ("val" + i));
		}
	}

	@Test
	def parseListTest3() {
		val settingsText = '''
			-listKey.0 = val0
			-listKey.1 = val1
			-listKey.2 = val2
			-key2 = value2
		'''
		val settings = SettingsSerializer.parseSettingsFromString(settingsText);

		Assert.assertTrue(settings instanceof SettingsElement);
		Assert.assertTrue(settings.getAttribute("listKey").get() instanceof SettingsList);
		val list = settings.getAttribute("listKey").get() as SettingsList;

		for (i : 0 .. 2) {
			Assert.assertTrue(list.elements.get(i) instanceof SettingsElement);
			Assert.assertTrue((list.elements.get(i) as SettingsElement).value == ("val" + i));
		}
	}

	@Test
	def overwriteElementTest1() {
		val settingsText1 = '''
			-a1 = val0
			-a2 = val1
		'''
		val settings1 = SettingsSerializer.parseSettingsFromString(settingsText1);
		val settingsText2 = '''
			-b1 = val2
			-b2 = val3
			-b3 = val4
		'''
		val settings2 = SettingsSerializer.parseSettingsFromString(settingsText2);

		Assert.assertTrue(settings1 instanceof SettingsElement);
		Assert.assertTrue(settings2 instanceof SettingsElement);

		settings1.overrideWith(settings2);

		Assert.assertTrue(settings1.attributeNames.containsAll(Arrays.asList("a1", "a2", "b1", "b2", "b3")));
	}

	@Test
	def overwriteElementTest2() {
		val settingsText1 = '''
			-a1 = val0
			-a2 = val1
		'''
		val settings1 = SettingsSerializer.parseSettingsFromString(settingsText1);
		val settingsText2 = '''
			-a1 = val2
			-b2 = val3
			-b3 = val4
		'''
		val settings2 = SettingsSerializer.parseSettingsFromString(settingsText2);

		Assert.assertTrue(settings1 instanceof SettingsElement);
		Assert.assertTrue(settings2 instanceof SettingsElement);

		settings1.overrideWith(settings2);

		Assert.assertTrue(settings1.attributeNames.containsAll(Arrays.asList("a1", "a2", "b2", "b3")));
		Assert.assertEquals(4, settings1.attributeNames.size);
		Assert.assertEquals("val2", (settings1.getAttribute("a1").get() as SettingsElement).value);
	}

	@Test
	def overwriteElementTest3() {
		val settingsText1 = '''
			-a.a1 = val0
			-a.a2 = val1
		'''
		val settings1 = SettingsSerializer.parseSettingsFromString(settingsText1);
		val settingsText2 = '''
			-a.a1 = val2
			-a.a3 = val3
			-a.a1.aa1 = val4
		'''
		val settings2 = SettingsSerializer.parseSettingsFromString(settingsText2);

		Assert.assertTrue(settings1 instanceof SettingsElement);
		Assert.assertTrue(settings2 instanceof SettingsElement);

		settings1.overrideWith(settings2);

		Assert.assertEquals(
			"-a.a1 = val2 -a.a1.aa1 = val4 -a.a2 = val1 -a.a3 = val3",
			SettingsSerializer.serialize(settings1).replaceAll("\\s+", " ").trim()
		);
	}

	@Test
	def overwriteListTest1() {
		val settingsText1 = '''
			-list.0 = "val0"
			-list.1 = "val1"
		'''
		val settings1 = SettingsSerializer.parseSettingsFromString(settingsText1);
		val settingsText2 = '''
			-list.2 = val2
		'''
		val settings2 = SettingsSerializer.parseSettingsFromString(settingsText2);

		Assert.assertTrue(settings1 instanceof SettingsElement);
		Assert.assertTrue(settings2 instanceof SettingsElement);

		settings1.overrideWith(settings2);
		checkList(settings1, "list", "val", 0, 2);
	}

	@Test
	def overwriteListTest2() {
		val settingsText1 = '''
			-list.0 = "val0"
			-list.1 = "val99"
		'''
		val settings1 = SettingsSerializer.parseSettingsFromString(settingsText1);
		val settingsText2 = '''
			-list.1 = val1
			-list.2 = val2
		'''
		val settings2 = SettingsSerializer.parseSettingsFromString(settingsText2);

		Assert.assertTrue(settings1 instanceof SettingsElement);
		Assert.assertTrue(settings2 instanceof SettingsElement);

		settings1.overrideWith(settings2);
		println(SettingsSerializer.serialize(settings1));
		checkList(settings1, "list", "val", 0, 2);
	}

	@Test
	def overwriteListTest3() {
		val settingsText1 = '''
			-list = {"val0","val99"}
		'''
		val settings1 = SettingsSerializer.parseSettingsFromString(settingsText1);
		val settingsText2 = '''
			-list.1 = val1
			-list.2 = val2
		'''
		val settings2 = SettingsSerializer.parseSettingsFromString(settingsText2);

		Assert.assertTrue(settings1 instanceof SettingsElement);
		Assert.assertTrue(settings2 instanceof SettingsElement);

		settings1.overrideWith(settings2);
		checkList(settings1, "list", "val", 0, 2);
	}

	@Test
	def void overwriteListTest4() {
		// merging two lists
		val settingsText1 = '''
			-list.0 = "val0"
			-list.2 = "val2"
			-list.4 = "val4"
		'''
		val settings1 = SettingsSerializer.parseSettingsFromString(settingsText1);
		val settingsText2 = '''
			-list.1 = val1
			-list.3 = val3
		'''
		val settings2 = SettingsSerializer.parseSettingsFromString(settingsText2);

		Assert.assertTrue(settings1 instanceof SettingsElement);
		Assert.assertTrue(settings2 instanceof SettingsElement);

		settings1.overrideWith(settings2);
		println(SettingsSerializer.serialize(settings1));

		checkList(settings1, "list", "val", 0, 4);
	}

	@Test
	def overwriteListTest5() {
		// overriding empty list
		val settingsText1 = '''
			-dummy = foo
		'''
		val settings1 = SettingsSerializer.parseSettingsFromString(settingsText1);
		val settingsText2 = '''
			-list.0 = val0
			-list.1 = val1
			-list.2 = val2
		'''
		val settings2 = SettingsSerializer.parseSettingsFromString(settingsText2);

		Assert.assertTrue(settings1 instanceof SettingsElement);
		Assert.assertTrue(settings2 instanceof SettingsElement);

		settings1.overrideWith(settings2);
		checkList(settings1, "list", "val", 0, 2);
	}

	@Test
	def overwriteListTest6() {
		// overriding list with 1 element
		val settingsText1 = '''
			-list.0 = val99
		'''
		val settings1 = SettingsSerializer.parseSettingsFromString(settingsText1);
		val settingsText2 = '''
			-list.0 = val0
			-list.1 = val1
			-list.2 = val2
		'''
		val settings2 = SettingsSerializer.parseSettingsFromString(settingsText2);

		Assert.assertTrue(settings1 instanceof SettingsElement);
		Assert.assertTrue(settings2 instanceof SettingsElement);

		settings1.overrideWith(settings2);
		checkList(settings1, "list", "val", 0, 2);
	}

	private def checkList(SettingsElement settings, String listAttributeName, String valuePrefix, int minItem,
		int maxItem) {
		Assert.assertTrue(settings.attributeNames.contains(listAttributeName));
		Assert.assertTrue(settings.getAttribute(listAttributeName).get() instanceof SettingsList);
		val list = settings.getAttribute(listAttributeName).get() as SettingsList;

		Assert.assertEquals((maxItem - minItem + 1), list.elements.size);
		for (i : minItem .. maxItem) {
			Assert.assertTrue(list.elements.get(i) instanceof SettingsElement);
			Assert.assertTrue((list.elements.get(i) as SettingsElement).value == (valuePrefix + i));
		}
	}

	@Test
	def void serializationTest1() {
		val toSerialize = SettingsElement.createRootElement("_root");
		toSerialize.setSimpleAttribute("key1", "val1");
		toSerialize.setSimpleAttribute("key2", "val2");

		val expected = '''
			-key1 = val1
			-key2 = val2
		'''
		val actual = SettingsSerializer.serialize(toSerialize);

		Assert.assertNotNull(actual);
		assertEqualsWithoutWhitespaceDiff(expected, actual);
	}

	@Test
	def void serializationTest2() {
		val toSerialize = SettingsElement.createRootElement("_root");
		val elem1 = SettingsElement.ofValue("val1");
		val elem2 = SettingsElement.ofValue("val2");
		toSerialize.setAttribute("key1", elem1);
		toSerialize.setAttribute("key2", elem2);

		val expected = '''
			-key1 = val1
			-key2 = val2
		'''
		val actual = SettingsSerializer.serialize(toSerialize);

		Assert.assertNotNull(actual);
		assertEqualsWithoutWhitespaceDiff(expected, actual);
	}

	@Test
	def void serializationTest3() {
		val toSerialize = SettingsElement.createRootElement("_root");
		val elem1 = SettingsElement.ofValue("val1");
		val elem2 = SettingsElement.ofValue("val2");
		toSerialize.setAttribute("key1", elem1);
		toSerialize.setAttribute("key2", elem2);

		val expected = '''
			-key1 = val1
			-key2 = val2
		'''
		val actual = SettingsSerializer.serialize(toSerialize);

		Assert.assertNotNull(actual);
		assertEqualsWithoutWhitespaceDiff(expected, actual);
	}

	private static def void assertEqualsWithoutWhitespaceDiff(String expected, String actual) {
		Assert.assertEquals(expected.replaceAll("\\s", ""), actual.replaceAll("\\s", ""));
	}

	@Test
	def copyTest1() {
		val settingsText = '''
			-key1 = value1
			-key2 = value2
			-key3 = value3
		'''
		val settings = SettingsSerializer.parseSettingsFromString(settingsText);

		Assert.assertTrue(settings.getAttribute("key1").get() instanceof SettingsElement);
		val node1 = settings.getAttribute("key1").get().toSingle;
		Assert.assertTrue(settings.getAttribute("key2").get() instanceof SettingsElement);
		val node2 = settings.getAttribute("key2").get().toSingle;

		val copy = settings.copy();

		Assert.assertTrue(copy.getAttribute("key1").get() instanceof SettingsElement);
		val copyNode1 = copy.getAttribute("key1").get().toSingle;
		Assert.assertTrue(copy.getAttribute("key2").get() instanceof SettingsElement);
		val copyNode2 = copy.getAttribute("key2").get().toSingle;

		Assert.assertTrue(node1 !== copyNode1);
		Assert.assertTrue(node2 !== copyNode2);
		Assert.assertTrue(copyNode1 !== copyNode2);

		Assert.assertTrue(copyNode1.parent === copy);
		Assert.assertTrue(copyNode2.parent === copy);
	}

	@Test
	def modifyParentTest1() {
		val root1 = SettingsElement.createRootElement("_root1");
		val root2 = SettingsElement.createRootElement("_root2");

		val child = SettingsElement.empty();

		root1.setAttribute("attrib1", child);
		Assert.assertEquals(root1, child.parent);
		Assert.assertEquals(1, root1.attributeNames.size);
		Assert.assertEquals(0, root2.attributeNames.size);

		root1.setAttribute("attrib2", child);
		Assert.assertEquals(root1, child.parent);
		// 'attrib1' was removed
		Assert.assertFalse(root1.attributeNames.contains("attrib1"));
		Assert.assertTrue(root1.attributeNames.contains("attrib2"));
		Assert.assertEquals(1, root1.attributeNames.size);
		Assert.assertEquals(0, root2.attributeNames.size);

		root2.setAttribute("attrib3", child);
		Assert.assertEquals(root2, child.parent);
		Assert.assertTrue(root2.attributeNames.contains("attrib3"));
		Assert.assertEquals(0, root1.attributeNames.size);
		Assert.assertEquals(1, root2.attributeNames.size);
	}

	@Test
	def modifyParentTest2() {
		val root1 = SettingsList.empty();
		val root2 = SettingsList.empty();

		val child = SettingsElement.empty();

		root1.set(1, child);
		Assert.assertEquals(root1, child.parent);
		Assert.assertEquals(1, root1.elements.filter[it !== null].size);
		Assert.assertEquals(0, root2.elements.filter[it !== null].size);

		root1.set(2, child);
		Assert.assertEquals(root1, child.parent);
		// 'attrib1' was removed
		Assert.assertTrue(root1.elements.get(1) === null);
		Assert.assertTrue(root1.elements.get(2) === child);
		Assert.assertEquals(1, root1.elements.filter[it !== null].size);
		Assert.assertEquals(0, root2.elements.filter[it !== null].size);

		root2.set(3, child);
		Assert.assertEquals(root2, child.parent);
		Assert.assertTrue(root1.elements.get(1) === null);
		Assert.assertTrue(root1.elements.size <= 2 || root1.elements.get(2) === null);
		Assert.assertTrue(root2.elements.get(3) === child);
		Assert.assertEquals(0, root1.elements.filter[it !== null].size);
		Assert.assertEquals(1, root2.elements.filter[it !== null].size);
	}
}
