package cern.plcverif.base.common.utils;

import org.junit.Assert;
import org.junit.Test;

public class UiUtilsTest {
	@Test
	public void genericSeparatorTest() {
		Assert.assertEquals("", UiUtils.separatedString("", 3, '_'));
		Assert.assertEquals("0", UiUtils.separatedString("0", 3, '_'));
		Assert.assertEquals("123", UiUtils.separatedString("123", 3, '_'));
		Assert.assertEquals("1_234", UiUtils.separatedString("1234", 3, '_'));
		Assert.assertEquals("123_456_789", UiUtils.separatedString("123456789", 3, '_'));
		Assert.assertEquals("10101010_10101010", UiUtils.separatedString("1010101010101010", 8, '_'));
	}
}
