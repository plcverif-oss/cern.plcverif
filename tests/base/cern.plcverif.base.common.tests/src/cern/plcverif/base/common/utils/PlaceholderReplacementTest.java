/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class PlaceholderReplacementTest {
	@Test
	public void simpleReplacement() {
		String text = "lorem=${ipsum},sit";
		String expected = "lorem=1,sit";

		String actual = PlaceholderReplacement.text(text).replace("ipsum", "1").toText();
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void multipleReplacementsOfSameKey() {
		String text = "lorem=${ipsum},dolor=${ipsum}";
		String expected = "lorem=1,dolor=1";

		String actual = PlaceholderReplacement.text(text).replace("ipsum", "1").toText();
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void multipleKeys() {
		String text = "lorem=${ipsum},dolor=${sit}";
		String expected = "lorem=1,dolor=2";

		String actual = PlaceholderReplacement.text(text).replace("ipsum", "1").replace("sit", "2").toText();
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void multipleKeysWithMap() {
		String text = "lorem=${ipsum},dolor=${sit}";
		String expected = "lorem=3,dolor=4";

		Map<String, String> valueMapping = new HashMap<>();
		valueMapping.put("ipsum", "3");
		valueMapping.put("sit", "4");
		
		String actual = PlaceholderReplacement.text(text).replace(valueMapping).toText();
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void replacementInList() {
		List<String> toBeModified = Arrays.asList("lorem=${ipsum}", "dolor=${sit}");
		List<String> expected = Arrays.asList("lorem=1", "dolor=2");

		List<String> actual = PlaceholderReplacement.list(toBeModified).replace("ipsum", "1").replace("sit", "2")
				.toList();
		Assert.assertEquals(expected, actual);
	}

	@Test(expected = PlaceholderReplacement.NonReplacedPlaceholderException.class)
	public void exceptionIfNonreplacedPlaceholder1() {
		String toBeModified = "lorem=${ipsum} dolor=${sit}";

		PlaceholderReplacement.text(toBeModified).replace("ipsum", "1").toText();
	}

	@Test(expected = PlaceholderReplacement.NonReplacedPlaceholderException.class)
	public void exceptionIfNonreplacedPlaceholder2() {
		List<String> toBeModified = Arrays.asList("lorem=${ipsum}", "dolor=${sit}");

		PlaceholderReplacement.list(toBeModified).replace("ipsum", "1").toList();
	}
}
