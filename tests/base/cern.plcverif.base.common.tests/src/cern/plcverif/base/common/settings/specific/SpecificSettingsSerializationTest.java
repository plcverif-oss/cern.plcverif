/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.base.common.settings.specific;

import static cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer.parse;
import static cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer.serialize;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;

import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsSerializer;
import cern.plcverif.base.common.settings.exceptions.SettingsAnnotationException;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.exceptions.SettingsSerializerException;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsComposite;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsElement;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsList;
import cern.plcverif.base.common.settings.specific.annotations.PlcverifSettingsMandatory;

public class SpecificSettingsSerializationTest {
	public static class BoolSpecificSettings extends AbstractSpecificSettings {
		@PlcverifSettingsElement(name = "bool1")
		private boolean boolean1;

		@PlcverifSettingsElement(name = "bool2")
		private boolean boolean2 = true;

		@PlcverifSettingsElement(name = "bool3", mandatory = PlcverifSettingsMandatory.OPTIONAL)
		private boolean boolean3 = true;
	}

	@Test
	public void parseBoolTest() throws Exception {
		String config = "-bool1\n-bool2 = false";

		SettingsElement settings = SettingsSerializer.parseSettingsFromString(config);
		BoolSpecificSettings specSettings = parse(settings, BoolSpecificSettings.class);

		Assert.assertNotNull(specSettings);
		Assert.assertEquals(true, specSettings.boolean1);
		Assert.assertEquals(false, specSettings.boolean2);
		Assert.assertEquals(true, specSettings.boolean3);
		Assert.assertEquals(settings, specSettings.getRepresentedSettingsElement());
	}

	@Test(expected = SettingsParserException.class)
	public void parseInvalidBoolTest() throws Exception {
		String config = "-bool1\n-bool2 = INVALID";

		SettingsElement settings = SettingsSerializer.parseSettingsFromString(config);
		parse(settings, BoolSpecificSettings.class);
	}

	@Test
	public void serializeBoolTest() throws Exception {
		BoolSpecificSettings specSettings = new BoolSpecificSettings();
		specSettings.boolean1 = true;
		specSettings.boolean2 = false;
		specSettings.boolean3 = true;

		//@formatter:off
		String expected1 = (
				"-bool1 = `true`\n" + 
				"-bool2 = `false`\n" + 
				"-bool3 = `true`\n").replace('`', '"');
		String expected2 = expected1.replace("-", "-prefix.");
		//@formatter:on

		assertEqualsWithoutWhitespaceDiff(expected1, serialize(specSettings, ""));
		assertEqualsWithoutWhitespaceDiff(expected2, serialize(specSettings, "prefix"));
	}

	public static class MinmaxSpecificSettings extends AbstractSpecificSettings {
		@PlcverifSettingsElement(name = "min")
		private int minimum;

		@PlcverifSettingsElement(name = "max")
		private int maximum;
	}

	@Test
	public void parseIntTest() throws Exception {
		String config = "-min = 0\n-max = 20";

		SettingsElement settings = SettingsSerializer.parseSettingsFromString(config);
		MinmaxSpecificSettings specSettings = parse(settings, MinmaxSpecificSettings.class);

		Assert.assertNotNull(specSettings);
		Assert.assertEquals(0, specSettings.minimum);
		Assert.assertEquals(20, specSettings.maximum);
		Assert.assertEquals(settings, specSettings.getRepresentedSettingsElement());
	}

	@Test(expected = SettingsParserException.class)
	public void parseInvalidIntTest() throws Exception {
		String config = "-min = 0\n-max = INVALID";

		SettingsElement settings = SettingsSerializer.parseSettingsFromString(config);
		parse(settings, MinmaxSpecificSettings.class);
	}

	@Test
	public void serializeIntTest() throws Exception {
		MinmaxSpecificSettings specSettings = new MinmaxSpecificSettings();
		specSettings.minimum = 10;
		specSettings.maximum = 50;

		//@formatter:off
		String expected1 = (
				"-min = `10`\n" + 
				"-max = `50`\n").replace('`', '"');
		String expected2 = expected1.replace("-", "-prefix.");
		//@formatter:on

		assertEqualsWithoutWhitespaceDiff(expected1, serialize(specSettings, ""));
		assertEqualsWithoutWhitespaceDiff(expected2, serialize(specSettings, "prefix"));
	}

	public static class StringSpecificSettings extends AbstractSpecificSettings {
		@PlcverifSettingsElement(name = "str1")
		private String string1;

		@PlcverifSettingsElement(name = "str2")
		private String string2 = "default";

		@PlcverifSettingsElement(name = "str3", mandatory = PlcverifSettingsMandatory.OPTIONAL)
		private String string3 = "str3_default";
	}

	@Test
	public void parseStringTest() throws Exception {
		String config = "-str1 = abc\n" + "-str2 = \"abc def\"";

		SettingsElement settings = SettingsSerializer.parseSettingsFromString(config);
		StringSpecificSettings specSettings = parse(settings, StringSpecificSettings.class);

		Assert.assertNotNull(specSettings);
		Assert.assertEquals("abc", specSettings.string1);
		Assert.assertEquals("abc def", specSettings.string2);
		Assert.assertEquals("str3_default", specSettings.string3);
		Assert.assertEquals(settings, specSettings.getRepresentedSettingsElement());
	}

	@Test
	public void serializeStringTest() throws Exception {
		StringSpecificSettings specSettings = new StringSpecificSettings();
		specSettings.string1 = "foo1";
		specSettings.string2 = "foo2";
		specSettings.string3 = "foo3";

		//@formatter:off
		String expected1 = (
				"-str1 = `foo1`\n" + 
				"-str2 = `foo2`\n" + 
				"-str3 = `foo3`\n").replace('`', '"');
		String expected2 = expected1.replace("-", "-prefix.");
		//@formatter:on

		assertEqualsWithoutWhitespaceDiff(expected1, serialize(specSettings, ""));
		assertEqualsWithoutWhitespaceDiff(expected2, serialize(specSettings, "prefix"));
	}

	@Test
	public void serializeStringWithoutDefaultsTest() throws Exception {
		StringSpecificSettings specSettings = new StringSpecificSettings();
		specSettings.string1 = "foo1";
		specSettings.string2 = "foo2";
		specSettings.string3 = "foo3";

		//@formatter:off
		String expected1 = (
				"-str1 = `foo1`\n" + 
				"-str2 = `foo2`\n" + 
				"-str3 = `foo3`\n").replace('`', '"');
		String expected2 = expected1.replace("-", "-prefix.");
		//@formatter:on

		assertEqualsWithoutWhitespaceDiff(expected1, serialize(specSettings, ""));
		assertEqualsWithoutWhitespaceDiff(expected2, serialize(specSettings, "prefix"));
	}

	@Test
	public void serializeStringNonStrict() throws Exception {
		StringSpecificSettings specSettings = new StringSpecificSettings();
		// The element string1 is mandatory, but not filled. In non-strict mode
		// the output should not contain this element and no exception should be
		// thrown.
		specSettings.string2 = "foo2";
		specSettings.string3 = "foo3";

		//@formatter:off
		String expected1 = (
				"-str2 = `foo2`\n" + 
				"-str3 = `foo3`\n").replace('`', '"');
		String expected2 = expected1.replace("-", "-prefix.");
		//@formatter:on

		assertEqualsWithoutWhitespaceDiff(expected1, serialize(specSettings, "", Optional.empty(), false));
		assertEqualsWithoutWhitespaceDiff(expected2, serialize(specSettings, "prefix", Optional.empty(), false));
	}

	@Test(expected = SettingsSerializerException.class)
	public void serializeStringStrictWithMissingValue() throws Exception {
		StringSpecificSettings specSettings = new StringSpecificSettings();
		// The element string1 is mandatory, but not filled. In strict mode
		// this should throw an exception
		specSettings.string2 = "foo2";
		specSettings.string3 = "foo3";

		serialize(specSettings, "", Optional.empty(), true); // exception
																// expected
	}

	public static class PathSpecificSettings extends AbstractSpecificSettings {
		@PlcverifSettingsElement(name = "path1")
		private Path path1;

		@PlcverifSettingsElement(name = "path2")
		private Path path2 = Paths.get("dummy");

		@PlcverifSettingsElement(name = "path3", mandatory = PlcverifSettingsMandatory.OPTIONAL)
		private Path path3 = Paths.get("path3");
	}

	@Test
	public void parsePathTest() throws Exception {
		String config = "-path1 = file.ext\n" + "-path2 = \"./dir/file.ext\"";

		SettingsElement settings = SettingsSerializer.parseSettingsFromString(config);
		PathSpecificSettings specSettings = parse(settings, PathSpecificSettings.class);

		Assert.assertNotNull(specSettings);
		Assert.assertEquals("file.ext", specSettings.path1.toString());
		Assert.assertEquals(".|dir|file.ext".replace("|", File.separator), specSettings.path2.toString());
		Assert.assertEquals("path3", specSettings.path3.toString());
		Assert.assertEquals(settings, specSettings.getRepresentedSettingsElement());
	}

	@Test
	public void serializePathTest() throws Exception {
		PathSpecificSettings specSettings = new PathSpecificSettings();
		specSettings.path1 = Paths.get("dir1", "file1");
		specSettings.path2 = Paths.get("dir2", "file2");
		specSettings.path3 = Paths.get("dir3", "file3");

		//@formatter:off
		String expected1 = (
				"-path1 = `dir1|file1`\n" + 
				"-path2 = `dir2|file2`\n" + 
				"-path3 = `dir3|file3`\n").replace('`', '"').replace('|', File.separatorChar);
		String expected2 = expected1.replace("-", "-prefix.");
		//@formatter:on

		assertEqualsWithoutWhitespaceDiff(expected1, serialize(specSettings, ""));
		assertEqualsWithoutWhitespaceDiff(expected2, serialize(specSettings, "prefix"));
	}

	public static class StringListSpecificSettings extends AbstractSpecificSettings {
		@PlcverifSettingsList(name = "list1")
		private List<String> list1;

		@PlcverifSettingsList(name = "list2")
		private List<String> list2 = Arrays.asList("a");

		@PlcverifSettingsList(name = "list3", mandatory = PlcverifSettingsMandatory.OPTIONAL)
		private List<String> list3 = Arrays.asList("x", "y");
	}

	@Test
	public void parseStringListTest() throws Exception {
		String config = "-list1 = {\"a\",\"b\",\"c\"}\n" + "-list2 = \"abc\",\"def\"";

		SettingsElement settings = SettingsSerializer.parseSettingsFromString(config);
		StringListSpecificSettings specSettings = parse(settings, StringListSpecificSettings.class);

		Assert.assertNotNull(specSettings);

		Assert.assertEquals(3, specSettings.list1.size());
		Assert.assertEquals("a:b:c", String.join(":", specSettings.list1));

		Assert.assertEquals(2, specSettings.list2.size());
		Assert.assertEquals("abc:def", String.join(":", specSettings.list2));

		Assert.assertEquals(2, specSettings.list3.size());
		Assert.assertEquals("x:y", String.join(":", specSettings.list3));

		Assert.assertEquals(settings, specSettings.getRepresentedSettingsElement());
	}

	@Test
	public void serializeStringListTest() throws Exception {
		StringListSpecificSettings specSettings = new StringListSpecificSettings();
		specSettings.list1 = Arrays.asList("elem1a", "elem1b");
		specSettings.list2 = Arrays.asList("elem2a");
		specSettings.list3 = Arrays.asList("elem3a");

		//@formatter:off
		String expected1 = (
				"-list1.0 = `elem1a`\n" +
				"-list1.1 = `elem1b`\n" +
				"-list2.0 = `elem2a`\n" +
				"-list3.0 = `elem3a`\n").replace('`', '"');
		String expected2 = expected1.replace("-", "-prefix.");
		//@formatter:on

		assertEqualsWithoutWhitespaceDiff(expected1, serialize(specSettings, ""));
		assertEqualsWithoutWhitespaceDiff(expected2, serialize(specSettings, "prefix"));
	}

	@Test
	public void serializeStringListWithoutDefaultsTest1() throws Exception {
		StringListSpecificSettings specSettings = new StringListSpecificSettings();
		specSettings.list1 = Arrays.asList("elem1a", "elem1b");
		specSettings.list3 = Arrays.asList("x", "z"); // 'x' is as in default,
														// but serialize the
														// whole string

		//@formatter:off
		String expected1 = (
				"-list1.0 = `elem1a`\n" +
				"-list1.1 = `elem1b`\n" +
				"-list3.0 = `x`\n"+ 
				"-list3.1 = `z`\n").replace('`', '"');
		String expected2 = expected1.replace("-", "-prefix.");
		//@formatter:on

		assertEqualsWithoutWhitespaceDiff(expected1,
				serialize(specSettings, "", Optional.of(new StringListSpecificSettings()), true));
		assertEqualsWithoutWhitespaceDiff(expected2,
				serialize(specSettings, "prefix", Optional.of(new StringListSpecificSettings()), true));
	}

	@Test
	public void serializeStringListWithoutDefaultsTest2() throws Exception {
		StringListSpecificSettings specSettings = new StringListSpecificSettings();
		specSettings.list1 = Arrays.asList("elem1a", "elem1b");

		//@formatter:off
		String expected1 = (
				"-list1.0 = `elem1a`\n" +
				"-list1.1 = `elem1b`\n").replace('`', '"');
		String expected2 = expected1.replace("-", "-prefix.");
		//@formatter:on

		assertEqualsWithoutWhitespaceDiff(expected1,
				serialize(specSettings, "", Optional.of(new StringListSpecificSettings()), true));
		assertEqualsWithoutWhitespaceDiff(expected2,
				serialize(specSettings, "prefix", Optional.of(new StringListSpecificSettings()), true));
	}

	public static class CompositeSpecificSettings extends AbstractSpecificSettings {
		@PlcverifSettingsElement(name = "id")
		private String id;

		@PlcverifSettingsElement(name = "nested")
		private String nested;

		@PlcverifSettingsComposite(name = "nested")
		private MinmaxSpecificSettings minmax;
	}

	public static class CompositeSpecificSettings2 extends AbstractSpecificSettings {
		@PlcverifSettingsElement(name = "id")
		private String id;

		@PlcverifSettingsComposite(name = "nested")
		private MinmaxSpecificSettings minmax;
	}

	@Test
	public void parseCompositeTest() throws Exception {
		//@formatter:off
		String config = 
				("-id = `abc`\n" +
				"-nested = `PRESENT`\n" +
				"-nested.min = 1\n" +
				"-nested.max = 10\n").replace('`', '"');
		//@formatter:on

		SettingsElement settings = SettingsSerializer.parseSettingsFromString(config);
		CompositeSpecificSettings specSettings = parse(settings, CompositeSpecificSettings.class);

		Assert.assertNotNull(specSettings);

		Assert.assertEquals("abc", specSettings.id);

		Assert.assertEquals("PRESENT", specSettings.nested);
		Assert.assertEquals("PRESENT", specSettings.minmax.getValue());
		Assert.assertNotNull(specSettings.minmax);
		Assert.assertEquals(1, specSettings.minmax.minimum);
		Assert.assertEquals(10, specSettings.minmax.maximum);

		Assert.assertEquals(settings, specSettings.getRepresentedSettingsElement());
		Assert.assertEquals(settings.getAttribute("nested").get(), specSettings.minmax.getRepresentedSettingsElement());
	}

	@Test
	public void serializeCompositeTest() throws Exception {
		CompositeSpecificSettings specSettings = new CompositeSpecificSettings();
		specSettings.id = "id123";
		specSettings.nested = "present";
		specSettings.minmax = new MinmaxSpecificSettings();
		specSettings.minmax.minimum = 10;
		specSettings.minmax.maximum = 100;

		//@formatter:off
		String expected1 = (
				"-id = `id123`\n" +
				"-nested = `present`\n" +
				"-nested.min = `10`\n" +
				"-nested.max = `100`\n").replace('`', '"');
		String expected2 = expected1.replace("-", "-prefix.");
		//@formatter:on

		assertEqualsWithoutWhitespaceDiff(expected1, serialize(specSettings, ""));
		assertEqualsWithoutWhitespaceDiff(expected2, serialize(specSettings, "prefix"));
	}

	@Test
	public void serializeCompositeTest2() throws Exception {
		CompositeSpecificSettings2 specSettings = new CompositeSpecificSettings2();
		specSettings.id = "id123";
		specSettings.minmax = new MinmaxSpecificSettings();
		specSettings.minmax.setValue("present2");
		specSettings.minmax.minimum = 10;
		specSettings.minmax.maximum = 100;

		//@formatter:off
		String expected1 = (
				"-id = `id123`\n" +
				"-nested = `present2`\n" +
				"-nested.min = `10`\n" +
				"-nested.max = `100`\n").replace('`', '"');
		String expected2 = expected1.replace("-", "-prefix.");
		//@formatter:on

		assertEqualsWithoutWhitespaceDiff(expected1, serialize(specSettings, ""));
		assertEqualsWithoutWhitespaceDiff(expected2, serialize(specSettings, "prefix"));
	}

	@Test
	public void serializeCompositeWithoutDefaultsTest() throws Exception {
		CompositeSpecificSettings2 specSettings = new CompositeSpecificSettings2();
		specSettings.id = "id123";
		specSettings.minmax = new MinmaxSpecificSettings();
		specSettings.minmax.minimum = 10;
		specSettings.minmax.maximum = 100;

		MinmaxSpecificSettings defMinMax = new MinmaxSpecificSettings();
		defMinMax.minimum = 10;
		defMinMax.maximum = 100;
		CompositeSpecificSettings2 def = new CompositeSpecificSettings2();
		def.minmax = defMinMax;

		//@formatter:off
		String expected1 = (
				"-id = `id123`\n").replace('`', '"');
		String expected2 = expected1.replace("-", "-prefix.");
		//@formatter:on

		assertEqualsWithoutWhitespaceDiff(expected1, serialize(specSettings, "", Optional.of(def), true));
		assertEqualsWithoutWhitespaceDiff(expected2, serialize(specSettings, "prefix", Optional.of(def), true));
	}

	public enum Vowel {
		A, E, I, O, U
	}

	public static class EnumSpecificSettings extends AbstractSpecificSettings {
		@PlcverifSettingsElement(name = "vowel1")
		private Vowel vowel1;

		@PlcverifSettingsElement(name = "vowel2")
		private Vowel vowel2 = Vowel.E;

		@PlcverifSettingsElement(name = "vowel3", mandatory = PlcverifSettingsMandatory.OPTIONAL)
		private Vowel vowel3 = Vowel.I;
	}

	@Test
	public void parseEnumTest() throws Exception {
		//@formatter:off
		String config = 
				"-vowel1 = E\n" + 
				"-vowel2 = \"U\"";
		//@formatter:on

		SettingsElement settings = SettingsSerializer.parseSettingsFromString(config);
		EnumSpecificSettings specSettings = parse(settings, EnumSpecificSettings.class);

		Assert.assertNotNull(specSettings);
		Assert.assertEquals(Vowel.E, specSettings.vowel1);
		Assert.assertEquals(Vowel.U, specSettings.vowel2);
		Assert.assertEquals(Vowel.I, specSettings.vowel3);
		Assert.assertEquals(settings, specSettings.getRepresentedSettingsElement());
	}

	@Test
	public void serializeEnumTest() throws Exception {
		EnumSpecificSettings specSettings = new EnumSpecificSettings();
		specSettings.vowel1 = Vowel.A;
		specSettings.vowel2 = Vowel.E;
		specSettings.vowel3 = Vowel.O;

		//@formatter:off
		String expected1 = (
				"-vowel1 = `A`\n" + 
				"-vowel2 = `E`\n" + 
				"-vowel3 = `O`\n").replace('`', '"');
		String expected2 = expected1.replace("-", "-prefix.");
		//@formatter:on

		assertEqualsWithoutWhitespaceDiff(expected1, serialize(specSettings, ""));
		assertEqualsWithoutWhitespaceDiff(expected2, serialize(specSettings, "prefix"));
	}

	@Test
	public void serializeEnumWithoutDefaultsTest() throws Exception {
		EnumSpecificSettings specSettings = new EnumSpecificSettings();
		specSettings.vowel1 = Vowel.A;
		specSettings.vowel2 = Vowel.E; // this is the default value
		specSettings.vowel3 = Vowel.O;

		//@formatter:off
		String expected1 = (
				"-vowel1 = `A`\n" + 
				"-vowel3 = `O`\n").replace('`', '"');
		String expected2 = expected1.replace("-", "-prefix.");
		//@formatter:on

		assertEqualsWithoutWhitespaceDiff(expected1,
				serialize(specSettings, "", Optional.of(new EnumSpecificSettings()), true));
		assertEqualsWithoutWhitespaceDiff(expected2,
				serialize(specSettings, "prefix", Optional.of(new EnumSpecificSettings()), true));
	}

	public static class InheritedSpecificSettings extends MinmaxSpecificSettings {
		@PlcverifSettingsElement(name = "avg")
		private int avg;
	}

	@Test
	public void parseInheritedTest() throws Exception {
		String config = "-min = 5\n-max = 20\n-avg=11";

		SettingsElement settings = SettingsSerializer.parseSettingsFromString(config);
		InheritedSpecificSettings specSettings = parse(settings, InheritedSpecificSettings.class);

		Assert.assertNotNull(specSettings);
		Assert.assertEquals(5, ((MinmaxSpecificSettings) specSettings).minimum);
		Assert.assertEquals(20, ((MinmaxSpecificSettings) specSettings).maximum);
		Assert.assertEquals(11, specSettings.avg);
		Assert.assertEquals(settings, specSettings.getRepresentedSettingsElement());
	}

	@Test
	public void serializeInheritedTest() throws Exception {
		InheritedSpecificSettings specSettings = new InheritedSpecificSettings();
		((MinmaxSpecificSettings) specSettings).minimum = 10; // kept private on
																// purpose
		((MinmaxSpecificSettings) specSettings).maximum = 50;
		specSettings.avg = 30;

		//@formatter:off
		String expected1 = (
				"-min = `10`\n" + 
				"-max = `50`\n" +
				"-avg = `30`\n").replace('`', '"');
		String expected2 = expected1.replace("-", "-prefix.");
		//@formatter:on

		assertEqualsWithoutWhitespaceDiff(expected1, serialize(specSettings, ""));
		assertEqualsWithoutWhitespaceDiff(expected2, serialize(specSettings, "prefix"));
	}

	public static class InvalidAnnotationSpecificSettings1 extends AbstractSpecificSettings {
		@PlcverifSettingsElement(name = "id")
		private List<String> id;
	}

	@Test(expected = SettingsAnnotationException.class)
	public void parseInvalidAnnotationSpecificSettings1Test() throws Exception {
		//@formatter:off
		String config = 
				("-id = `abc`\n").replace('`', '"');
		//@formatter:on

		SettingsElement settings = SettingsSerializer.parseSettingsFromString(config);
		parse(settings, InvalidAnnotationSpecificSettings1.class);
	}

	@Test(expected = SettingsSerializerException.class)
	public void serializeInvalidAnnotationSpecificSettings1Test() throws Exception {
		InvalidAnnotationSpecificSettings1 specSettings = new InvalidAnnotationSpecificSettings1();
		serialize(specSettings, "prefix");
	}

	public static class InvalidAnnotationSpecificSettings2 extends AbstractSpecificSettings {
		@PlcverifSettingsList(name = "id")
		private String id;
	}

	@Test(expected = SettingsAnnotationException.class)
	public void parseInvalidAnnotationSpecificSettings2Test() throws Exception {
		//@formatter:off
		String config = 
				("-id = `abc`\n").replace('`', '"');
		//@formatter:on

		SettingsElement settings = SettingsSerializer.parseSettingsFromString(config);
		parse(settings, InvalidAnnotationSpecificSettings1.class);
	}

	private static void assertEqualsWithoutWhitespaceDiff(String expected, String actual) {
		Assert.assertEquals(expected.replaceAll("\\s", ""), actual.replaceAll("\\s", ""));
	}
}
