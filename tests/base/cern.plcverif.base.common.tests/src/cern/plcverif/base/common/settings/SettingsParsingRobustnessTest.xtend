/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.common.settings

import org.junit.Assert
import org.junit.Test

class SettingsParsingRobustnessTest {
	@Test
	def test1() {
		val input = '''
			-key1.a = "A"
			-key1.b = "B"
		'''
		
		// plcverif-cli.exe -key1.a="A" -key1.b="B"
		val cmdLineInput = #['-key1.a=A', '-key1.b=B'];

		assertRoundtrip(input);
		assertRoundtripCmdline(cmdLineInput, input);
	}
	
	@Test
	def test2() {
		val input = '''
			-key1.a = "A = B"
			-key1.b = "A = B"
		'''
		
		// plcverif-cli.exe -key1.a="A = B" -key1.b="A = B"
		val cmdLineInput = #['-key1.a=A = B', '-key1.b=A = B'];

		assertRoundtrip(input);
		assertRoundtripCmdline(cmdLineInput, input);
	}
	
	@Test
	def fileTest3() {
		val input = '''
			-description="This is a ''nice'' example."
			-other_descr="This is a ''nicer'' example."
		'''
		
		assertRoundtrip(input);
	}
	
//	@Test
//	def fileTest3b() {
//		// not supported yet
//		val input = '''
//			-description=This is a "nice" example.
//			-other_descr=This is a ''nicer'' example.
//		'''
//		
//		assertRoundtrip(input);
//	}
	
	@Test
	def cmdlineTest3() {
		// plcverif-cli.exe -description="This is a \"nice\" example." -other_descr="This is a ''nicer'' example."
		val cmdLineInput = #['-description=This is a "nice" example.', "-other_descr=This is a ''nicer'' example."];
		
		val expected = '''
			-description="This is a ''nice'' example."
			-other_descr="This is a ''nicer'' example."
		'''

		assertRoundtripCmdline(cmdLineInput, expected);
	}

	@Test
	def test4() {
		val input = '''
			-listkey = {a,b,c,"d e f",g}
		'''
		
		val expected = '''
			-listkey.0=a
			-listkey.1=b
			-listkey.2=c
			-listkey.3=d e f
			-listkey.4=g
		'''
		
		// plcverif-cli.exe -listkey={a,b,c,"d e f",g}
		val cmdLineInput = #['-listkey={a,b,c,d e f,g}'];

		assertRoundtrip(input, expected);
		assertRoundtripCmdline(cmdLineInput, expected);
	}

	def assertRoundtrip(String settingsInFileFormat) {
		assertRoundtrip(settingsInFileFormat, settingsInFileFormat);
	}
	
	def assertRoundtrip(String settingsInFileFormat, String expected) {
		// First parse it
		val Settings parsedSettings = SettingsSerializer.parseSettingsFromFile(settingsInFileFormat);
		Assert.assertNotNull(parsedSettings);
		Assert.assertTrue(parsedSettings.kind == SettingsKind.ELEMENT);

		// Serialize back		
		val serializedSettings = SettingsSerializer.serialize(parsedSettings.toSingle, false);
		Assert.assertEquals(expected.replaceAll("[ \\\"]", ""), serializedSettings.replaceAll("[ \\\"]", ""));
	}
	
	def assertRoundtripCmdline(String[] settingsInCmdlineFormat, String expected) {
		// First parse it
		val Settings parsedSettings = SettingsSerializer.parseSettingsFromCommandArgs(settingsInCmdlineFormat);
		Assert.assertNotNull(parsedSettings);
		Assert.assertTrue(parsedSettings.kind == SettingsKind.ELEMENT);

		// Serialize back		
		val serializedSettings = SettingsSerializer.serialize(parsedSettings.toSingle, false);
		Assert.assertEquals(expected.replaceAll("[ \\\"]", ""), serializedSettings.replaceAll("[ \"]", ""));
	}

}
