/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.common.utils

import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.util.List
import org.junit.Assert
import org.junit.BeforeClass
import org.junit.Test
import java.util.regex.Pattern

class IoUtilsPathWildcardTest {
	static Path basePath = null;
	
	@BeforeClass
	def static void setup() {
		// Build folder structure:
		
		// base
		// +-- dir1
		// ....+-- file11.csv
	    // +-- dir2
		// ....+-- file21.csv
		// +-- file1.txt
		// +-- file2.txt
		// +-- file3.csv
		// +-- file4.csv
		
		// Directories
		basePath = Files.createTempDirectory("plcverif-test");
		// println('''Base directory: «basePath.toAbsolutePath»''');
		val dir1 = Files.createDirectory(basePath.resolve("dir1"));
		val dir2 = Files.createDirectory(basePath.resolve("dir2"));
		
		// Files
		Files.createFile(dir1.resolve("file11.csv"));
		Files.createFile(dir2.resolve("file21.csv"));
		Files.createFile(basePath.resolve("file1.txt"));
		Files.createFile(basePath.resolve("file2.txt"));
		Files.createFile(basePath.resolve("file3.csv"));
		Files.createFile(basePath.resolve("file4.csv"));
	}
	
	@Test
	def patternMatchTest() {
		Assert.assertTrue("a-c\\".matches(Pattern.quote("a-c\\")));
		
		val r2 = Pattern.quote("a-c\\")+ "e";
		Assert.assertTrue("a-c\\e".matches(r2));
	}
	
	@Test
	def resolveAbsoluteTest1() {
		val result = IoUtils.resolvePathsMatchingPattern('''«basePath»/dir1/file11.csv'''.replaceSeparators);
		
		Assert.assertEquals(1, result.size);
		assertContainsFileName(result, "file11.csv");
	}
	
	@Test
	def resolveWildcardTest1() {
		val result = IoUtils.resolvePathsMatchingPattern('''«basePath»/dir1/*.csv'''.replaceSeparators);
		
		Assert.assertEquals(1, result.size);
		assertContainsFileName(result, "file11.csv");
	}
	
	@Test
	def resolveWildcardTest2() {
		val result = IoUtils.resolvePathsMatchingPattern('''«basePath»/*/*.csv'''.replaceSeparators);
		
		Assert.assertEquals(2, result.size);
		assertContainsFileName(result, "file11.csv");
		assertContainsFileName(result, "file21.csv");
	}
	
	@Test
	def resolveWildcardTest3() {
		val result = IoUtils.resolvePathsMatchingPattern('''«basePath»/*.csv'''.replaceSeparators);
		
		Assert.assertEquals(2, result.size);
		assertContainsFileName(result, "file3.csv");
		assertContainsFileName(result, "file4.csv");
	}
	
	@Test
	def resolveWildcardTest4() {
		val result = IoUtils.resolvePathsMatchingPattern('''«basePath»/file*.csv'''.replaceSeparators);
		
		Assert.assertEquals(2, result.size);
		assertContainsFileName(result, "file3.csv");
		assertContainsFileName(result, "file4.csv");
	}
	
	@Test
	def resolveWildcardTest5() {
		val result = IoUtils.resolvePathsMatchingPattern('''«basePath»/file*.*'''.replaceSeparators);
		
		Assert.assertEquals(4, result.size);
		assertContainsFileName(result, "file1.txt");
		assertContainsFileName(result, "file2.txt");
		assertContainsFileName(result, "file3.csv");
		assertContainsFileName(result, "file4.csv");
	}
	
//	@Test
//	def resolveWildcardTest6() {
//		// This may lead to different results on Windows and *nix
//		val result = IoUtils.resolvePathsMatchingPattern('''«basePath»/**/*.csv'''.replaceSeparators);
//		
//		Assert.assertEquals(2, result.size);
//		assertContainsFileName(result, "file11.csv");
//		assertContainsFileName(result, "file21.csv");
//		assertContainsFileName(result, "file3.csv");
//		assertContainsFileName(result, "file4.csv");
//	}
	@Test
	def resolveWildcardTest7() {
		val result = IoUtils.resolvePathsMatchingPattern('''«basePath»/file*.{txt,csv}'''.replaceSeparators);
		Assert.assertEquals(4, result.size);
		assertContainsFileName(result, "file1.txt");
		assertContainsFileName(result, "file2.txt");
		assertContainsFileName(result, "file3.csv");
		assertContainsFileName(result, "file4.csv");
	}
	
	@Test
	def resolveWildcardTest8() {
		// Relative path
		val result = IoUtils.resolvePathsMatchingPattern('''*.this_extension_will_not_exist'''.replaceSeparators);
		
		// We don't expect any match (but it should not throw exception either)
		Assert.assertEquals(0, result.size);
	}
	
	@Test
	def resolveWildcardTest9() {
		// Relative path
		val result = IoUtils.resolvePathsMatchingPattern('''nonexistent_directory/*.this_extension_will_not_exist'''.replaceSeparators);
		
		// We don't expect any match (but it should not throw exception either)
		Assert.assertEquals(0, result.size);
	}
	
	private def String replaceSeparators(CharSequence path) {
		return path.toString.replace('/', File.separatorChar);
	} 
	
	private def void assertContainsFileName(List<Path> paths, String fileName) {
		Assert.assertFalse(paths.filter[it | it.fileName.toString == fileName].empty);
	}
}