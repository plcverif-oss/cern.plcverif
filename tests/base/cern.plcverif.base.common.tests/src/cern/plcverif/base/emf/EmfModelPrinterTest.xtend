/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Vince Molnar - update after changed printer format 
 *****************************************************************************/
package cern.plcverif.base.emf

import static org.junit.Assert.*;

import org.junit.Test;
import org.eclipse.emf.ecore.EcoreFactory
import cern.plcverif.base.common.emf.textual.EmfModelPrinter
import cern.plcverif.base.common.emf.textual.SimpleEObjectToText

class EmfModelPrinterTest {
	@Test
	def void smokeTest() {
		val expected = 
		'''{EClass name=class1 instanceClassName= instanceClass= defaultValue= instanceTypeName= abstract=true interface=false ePackage:<null> eSuperTypes:[] eAllAttributes:[attrib1] eAllReferences:[] eReferences:[] eAttributes:[attrib1] eAllContainments:[] eAllOperations:[] eAllStructuralFeatures:[attrib1] eAllSuperTypes:[] eIDAttribute:<null> eAllGenericSuperTypes:[]}
		 ├──eAnnotations: <empty>
		 ├──eTypeParameters: <empty>
		 ├──eOperations: <empty>
		 ├──eStructuralFeatures: 
		 │  {EAttribute name=attrib1 ordered=true unique=true lowerBound=0 upperBound=1 many=false required=false changeable=true volatile=false transient=false defaultValueLiteral= defaultValue= unsettable=false derived=false iD=false eType:<null> eContainingClass:class1 eAttributeType:<null>}
		 │   ├──eAnnotations: <empty>
		 │   └──eGenericType: <null>
		 └──eGenericSuperTypes: <empty>'''

		// Building the model
		val factory = EcoreFactory.eINSTANCE;
		val class1 = factory.createEClass();
		class1.name = "class1";
		class1.abstract = true;
		
		val attrib1 = factory.createEAttribute();
		attrib1.name = "attrib1";
		class1.getEStructuralFeatures().add(attrib1);
		
		// Act		
		val actual = EmfModelPrinter.print(class1, SimpleEObjectToText.INSTANCE);
		
		// Assert
		assertEquals(expected.withoutWhitespaces(), actual.withoutWhitespaces());
	}
	
	def String withoutWhitespaces(CharSequence str) {
		return str.toString.replaceAll("\\s", "");
	}
	
}