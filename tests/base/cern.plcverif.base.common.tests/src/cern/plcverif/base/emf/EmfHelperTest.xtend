/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.emf

import cern.plcverif.base.common.emf.EmfHelper
import cern.plcverif.base.models.cfa.CfaInstanceSafeFactory
import cern.plcverif.base.models.cfa.cfabase.Location
import cern.plcverif.base.models.cfa.cfabase.Transition
import cern.plcverif.base.models.cfa.cfainstance.ArrayVariable
import cern.plcverif.base.models.cfa.cfainstance.AutomatonInstance
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance
import cern.plcverif.base.models.cfa.cfainstance.Variable
import cern.plcverif.base.models.cfa.cfainstance.VariableAssignment
import cern.plcverif.base.models.expr.BinaryArithmeticExpression
import cern.plcverif.base.models.expr.BinaryArithmeticOperator
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.IntLiteral
import java.util.Arrays
import org.eclipse.emf.ecore.EObject
import org.junit.Assert
import org.junit.BeforeClass
import org.junit.Test
import java.util.stream.Collectors

class EmfHelperTest {
	static final CfaInstanceSafeFactory FACTORY = CfaInstanceSafeFactory.INSTANCE;

	static CfaNetworkInstance cfa;
	static AutomatonInstance automaton;
	static Variable varV;
	static IntLiteral oneLiteral;
	static IntLiteral twoLiteral;
	static IntLiteral threeLiteral;
	static VariableAssignment assignment;
	
    @BeforeClass
	def static void buildExampleModel() {
		// The model represents: `v : INT :=  (1 + 2) * 3`
		cfa = FACTORY.createCfaNetworkInstance("network");
		automaton = FACTORY.createAutomatonInstance("automaton", cfa);
		varV = FACTORY.createVariable(Arrays.asList("v"), cfa, FACTORY.createIntType(true, 16));

		val initLoc = FACTORY.createInitialLocation("init", automaton);
		val endLoc = FACTORY.createEndLocation("end", automaton);
		val transition = FACTORY.createAssignmentTransition("transition", automaton, initLoc, endLoc, FACTORY.trueLiteral);

		oneLiteral = FACTORY.createIntLiteral(1, true, 16);
		twoLiteral = FACTORY.createIntLiteral(2, true, 16);
		threeLiteral = FACTORY.createIntLiteral(3, true, 16);
		val expr = FACTORY.createBinaryArithmeticExpression(
			FACTORY.createBinaryArithmeticExpression(
				oneLiteral, twoLiteral, BinaryArithmeticOperator.PLUS
			), 
			threeLiteral, 
			BinaryArithmeticOperator.MULTIPLICATION
		);
		assignment = FACTORY.createVariableAssignment(transition, 
			FACTORY.createVariableRef(varV),
			expr);
	}
	
	@Test
	def checkNotContainedTest1() {
		EmfHelper.checkNotContained(FACTORY.createIntType(false, 8));
		// It should not throw an exception.
	}
	
	@Test(expected = typeof(UnsupportedOperationException))
	def checkNotContainedTest2() {
		Assert.assertNotNull(oneLiteral.eContainer);
		EmfHelper.checkNotContained(oneLiteral);
		Assert.fail();
		// It should throw an exception.
	}  
	
	@Test(expected = typeof(UnsupportedOperationException))
	def checkNotContainedTest3() {
		EmfHelper.checkNotContained(oneLiteral, "oneLiteral is already contained.");
		Assert.fail();
		// It should throw an exception.
	}  
	
	@Test
	def countContainersOfTypeRecursivelyTest1() {
		// Expected 2: PLUS, MULTIPLICATION
		Assert.assertEquals(2, EmfHelper.countContainersOfTypeRecursively(oneLiteral, typeof(BinaryArithmeticExpression)));
		
		// Expected 1: MULTIPLICATION
		Assert.assertEquals(1, EmfHelper.countContainersOfTypeRecursively(threeLiteral, typeof(BinaryArithmeticExpression)));
		
		// Expected 0: no such container
		Assert.assertEquals(0, EmfHelper.countContainersOfTypeRecursively(threeLiteral, typeof(Location)));
	}
	
	@Test
	def countContainersOfTypeRecursivelyTest2() {
		// Expected 1: MULTIPLICATION
		Assert.assertEquals(1, 
			EmfHelper.countContainersOfTypeRecursively(
				oneLiteral, typeof(BinaryArithmeticExpression),
				[it | it.operator == BinaryArithmeticOperator.MULTIPLICATION]
			)
		);
		
		// Expected 1: MULTIPLICATION
		Assert.assertEquals(1, 
			EmfHelper.countContainersOfTypeRecursively(
				threeLiteral, typeof(BinaryArithmeticExpression),
				[it | it.operator == BinaryArithmeticOperator.MULTIPLICATION]
			)
		);
		
		// Expected 0: no such container
		Assert.assertEquals(0, 
			EmfHelper.countContainersOfTypeRecursively(
				threeLiteral, typeof(Location),
				[it | it.name !== null]
			)
		);
	}
	
	@Test
	def countContainersOfTypeRecursivelyTest3() {
		// Expected 2: PLUS, MULTIPLICATION
		Assert.assertEquals(2, 
			EmfHelper.countContainersOfTypeRecursively(oneLiteral, 
				[it | it instanceof BinaryArithmeticExpression]
			)
		);
		
		// Expected 1: MULTIPLICATION
		Assert.assertEquals(1, 
			EmfHelper.countContainersOfTypeRecursively(threeLiteral, 
				[it | it instanceof BinaryArithmeticExpression]
		));
		
		// Expected 0: no such container
		Assert.assertEquals(0, 
			EmfHelper.countContainersOfTypeRecursively(threeLiteral, 
				[it | it instanceof Location])
		);
	}
	
	@Test
	def getAllContentsOfTypeTest1() {
		val intLiteralContents = EmfHelper.getAllContentsOfType(assignment, IntLiteral, false);
		Assert.assertEquals(3, intLiteralContents.size);
		Assert.assertTrue(intLiteralContents.contains(oneLiteral));
		Assert.assertTrue(intLiteralContents.contains(twoLiteral));
		Assert.assertTrue(intLiteralContents.contains(threeLiteral));
	}
	
	@Test
	def getAllContentsOfTypeTest2() {
		val contents = EmfHelper.getAllContentsOfType(assignment.rightValue, BinaryArithmeticExpression, false);
		Assert.assertEquals(1, contents.size);
		Assert.assertEquals(oneLiteral.eContainer, contents.get(0));
	}
	
	@Test
	def getAllContentsOfTypeTest3() {
		val contents = EmfHelper.getAllContentsOfType(assignment.rightValue, BinaryArithmeticExpression, true);
		Assert.assertEquals(2, contents.size);
		Assert.assertTrue(contents.contains(oneLiteral.eContainer));
		Assert.assertTrue(contents.contains(threeLiteral.eContainer));
	}
	
	@Test
	def getAllContentsOfTypeTest4() {
		val contents = EmfHelper.getAllContentsOfType(
			assignment.rightValue, BinaryArithmeticExpression, true,
			[it | it.operator == BinaryArithmeticOperator.MULTIPLICATION]
		);
		Assert.assertEquals(1, contents.size);
		Assert.assertTrue(contents.contains(threeLiteral.eContainer));
	}
	
	@Test
	def getAllContentsOfTypeStreamTest1() {
		val intLiteralContents = EmfHelper.getAllContentsOfTypeStream(assignment, IntLiteral, false).collect(Collectors.toList);
		Assert.assertEquals(3, intLiteralContents.size);
		Assert.assertTrue(intLiteralContents.contains(oneLiteral));
		Assert.assertTrue(intLiteralContents.contains(twoLiteral));
		Assert.assertTrue(intLiteralContents.contains(threeLiteral));
	}
	
	@Test
	def getAllContentsOfTypeStreamTest2() {
		val contents = EmfHelper.getAllContentsOfTypeStream(assignment.rightValue, BinaryArithmeticExpression, false).collect(Collectors.toList);
		Assert.assertEquals(1, contents.size);
		Assert.assertEquals(oneLiteral.eContainer, contents.get(0));
	}
	
	@Test
	def getAllContentsOfTypeStreamTest3() {
		val contents = EmfHelper.getAllContentsOfTypeStream(assignment.rightValue, BinaryArithmeticExpression, true).collect(Collectors.toList);
		Assert.assertEquals(2, contents.size);
		Assert.assertTrue(contents.contains(oneLiteral.eContainer));
		Assert.assertTrue(contents.contains(threeLiteral.eContainer));
	}
	
	@Test
	def getAllContentsOfTypeStreamTest4() {
		val contents = EmfHelper.getAllContentsOfTypeStream(
			assignment.rightValue, BinaryArithmeticExpression, true,
			[it | it.operator == BinaryArithmeticOperator.MULTIPLICATION]
		).collect(Collectors.toList);
		Assert.assertEquals(1, contents.size);
		Assert.assertTrue(contents.contains(threeLiteral.eContainer));
	}
	
	@Test
	def getContainerOfTypeTest1() {
		Assert.assertEquals(oneLiteral.eContainer,
			EmfHelper.getContainerOfType(oneLiteral, EObject) 
		);
		
		Assert.assertEquals(oneLiteral.eContainer,
			EmfHelper.getContainerOfType(oneLiteral, BinaryArithmeticExpression) 
		);
		
		Assert.assertEquals(assignment,
			EmfHelper.getContainerOfType(oneLiteral, VariableAssignment) 
		);
		
		Assert.assertEquals(automaton,
			EmfHelper.getContainerOfType(oneLiteral, AutomatonInstance) 
		);
		
		Assert.assertEquals(null,
			EmfHelper.getContainerOfType(oneLiteral, Location) 
		);
		
		Assert.assertEquals(null,
			EmfHelper.getContainerOfType(oneLiteral, IntLiteral) 
		);
	}
	
	@Test
	def getFarthestContinuousAncestorOfTypeTest1() {
		Assert.assertEquals(oneLiteral,
			EmfHelper.getFarthestContinousAncestorOfType(oneLiteral, IntLiteral) 
		);
		
		Assert.assertEquals(assignment.rightValue,
			EmfHelper.getFarthestContinousAncestorOfType(oneLiteral, Expression) 
		);
	}
	
	@Test
	def getLastAncestorBeforeContainerOfTypeTest1() {
		Assert.assertEquals(assignment,
			EmfHelper.getLastAncestorBeforeContainerOfType(oneLiteral, Transition) 
		);
		
		Assert.assertEquals(null,
			EmfHelper.getLastAncestorBeforeContainerOfType(oneLiteral, Location) 
		);
	}
	
	@Test
	def isContainedInAnyTest1() {
		Assert.assertEquals(false, 
			EmfHelper.isContainedInAny(oneLiteral)	
		);
		
		Assert.assertEquals(false, 
			EmfHelper.isContainedInAny(oneLiteral, Location, ArrayVariable)	
		);
		
		Assert.assertEquals(false, 
			EmfHelper.isContainedInAny(oneLiteral, IntLiteral)	
		);
		
		Assert.assertEquals(true, 
			EmfHelper.isContainedInAny(oneLiteral, VariableAssignment)	
		);
		
		Assert.assertEquals(true, 
			EmfHelper.isContainedInAny(oneLiteral, Location, ArrayVariable, VariableAssignment)	
		);
	}
}
