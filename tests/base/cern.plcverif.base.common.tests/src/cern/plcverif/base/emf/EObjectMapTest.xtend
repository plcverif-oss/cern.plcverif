/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.emf

import cern.plcverif.base.common.emf.EObjectMap
import cern.plcverif.base.common.emf.EObjectMap.IMapFactory
import com.google.common.collect.ImmutableMap
import java.util.HashMap
import org.eclipse.emf.ecore.EDataType
import org.eclipse.emf.ecore.EcoreFactory
import org.eclipse.emf.ecore.util.EcoreUtil
import org.junit.Test

import static org.junit.Assert.*
import cern.plcverif.base.common.emf.EquatableEObject

class EObjectMapTest {
	@Test
	def void smokeTest1() {
		val o1a = EcoreFactory.eINSTANCE.createEDataType() => [name = "O1"];
		val o1b = EcoreFactory.eINSTANCE.createEDataType() => [name = "O1"];
		val o2 = EcoreFactory.eINSTANCE.createEDataType() => [name = "O2"];
		val x = EcoreFactory.eINSTANCE.createEDataType() => [name = "X"];

		assertTrue(EcoreUtil.equals(o1a, o1a));
		assertTrue(EcoreUtil.equals(o1a, o1b));
		assertFalse(EcoreUtil.equals(o1a, o2));

		val map = new EObjectMap<EDataType, Integer>(new IMapFactory<Integer>() {
			override createMap() {
				return new HashMap<EquatableEObject, Integer>();
			}
		});
		
		map.put(o1a, 1);
		map.put(o1b, 2);
		map.put(o2, 3);
		
		assertEquals(2, map.get(o1a));
		assertEquals(2, map.get(o1b));
		assertEquals(3, map.get(o2));
		
		assertFalse(map.isEmpty);
		assertEquals(2, map.size);
		assertTrue(map.containsKey(o1a));
		assertTrue(map.containsKey(o1b));
		assertTrue(map.containsKey(o2));
		assertFalse(map.containsKey(x));
		assertTrue(map.containsKey(o1a as Object));
		assertTrue(map.containsKey(o1b as Object));
		assertTrue(map.containsKey(o2 as Object));
		assertFalse(map.containsKey(x as Object));
		assertFalse(map.containsValue(1));
		assertTrue(map.containsValue(2));
		assertTrue(map.containsValue(3));
		assertFalse(map.containsValue(1 as Object));
		assertTrue(map.containsValue(2 as Object));
		assertTrue(map.containsValue(3 as Object));
		
		assertEquals(2, map.remove(o1a));
		assertEquals(1, map.size);
		assertEquals(1, map.entrySet.size);
		assertEquals(1, map.keySet.size);
		assertEquals(1, map.values.size);
		
		map.put(o1a, 100);
		assertEquals(100, map.get(o1a as Object));
		assertEquals(100, map.remove(o1a as Object));
		
		map.clear();
		assertEquals(0, map.size);
		assertTrue(map.isEmpty);
		
		map.putAll(ImmutableMap.of(o1a, 5, o1b, 6));
		assertEquals(1, map.size);
		assertTrue(map.containsKey(o1a));
		assertTrue(map.containsKey(o1b));
		assertTrue(map.containsValue(5) || map.containsValue(6));
	}
	
	@Test
	def void smokeTest2() {
		val map = new EObjectMap<EDataType, Integer>();
		val x = EcoreFactory.eINSTANCE.createEDataType() => [name = "X"];
		
		assertTrue(map.isEmpty);
		assertFalse(map.containsKey(123));
		assertFalse(map.containsValue(123));
		
		assertNull(map.get(123));
		assertNull(map.get(null));
		assertNull(map.get(x));
		
		assertNull(map.remove(123));
		assertNull(map.remove(null));
		assertNull(map.remove(x));
	}
}

