/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.emf

import org.eclipse.emf.ecore.EcoreFactory
import org.junit.Assert
import org.eclipse.emf.ecore.util.EcoreUtil
import cern.plcverif.base.common.emf.EquatableEObject
import org.junit.Test

class EquatableEObjectTest {
	@Test
	def equalityTest1() {
		val c1a = EcoreFactory.eINSTANCE.createEClass() => [name = "C1"];
		val c1b = EcoreFactory.eINSTANCE.createEClass() => [name = "C1"];	
		c1a.getEStructuralFeatures.add(EcoreFactory.eINSTANCE.createEAttribute() => [name = "A"]);	
		c1b.getEStructuralFeatures.add(EcoreFactory.eINSTANCE.createEAttribute() => [name = "A"]);
		
		Assert.assertNotEquals(c1a, c1b);
		Assert.assertTrue(EcoreUtil.equals(c1a, c1b));
		
		val w1a = new EquatableEObject(c1a);
		val w1aa = new EquatableEObject(c1a);
		val w1b = new EquatableEObject(c1b);
		
		Assert.assertEquals(w1a, w1aa);	
		Assert.assertEquals(w1a, w1b);
		Assert.assertEquals(w1a.hashCode, w1b.hashCode);
	}
	
	@Test
	def equalityTest2() {
		val c1a = EcoreFactory.eINSTANCE.createEClass() => [name = "C1"];
		val c1b = EcoreFactory.eINSTANCE.createEClass() => [name = "C1"];	
		c1a.getEStructuralFeatures.add(EcoreFactory.eINSTANCE.createEAttribute() => [name = "A"]);	
		c1b.getEStructuralFeatures.add(EcoreFactory.eINSTANCE.createEAttribute() => [name = "B"]);
		
		Assert.assertNotEquals(c1a, c1b);
		Assert.assertFalse(EcoreUtil.equals(c1a, c1b));
		
		val w1a = new EquatableEObject(c1a);
		val w1b = new EquatableEObject(c1b);
		
		Assert.assertNotEquals(w1a, w1b);
	}
}