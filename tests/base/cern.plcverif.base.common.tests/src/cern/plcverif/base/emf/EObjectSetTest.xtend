/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.emf

import cern.plcverif.base.common.emf.EObjectSet
import java.util.Arrays
import org.eclipse.emf.ecore.EDataType
import org.eclipse.emf.ecore.EcoreFactory
import org.eclipse.emf.ecore.util.EcoreUtil
import org.junit.Test

import static org.junit.Assert.*
import org.eclipse.emf.ecore.EClass

class EObjectSetTest {
	@Test
	def void smokeTest() {
		// Arrange
		val o1a = EcoreFactory.eINSTANCE.createEDataType() => [name = "O1"];
		val o1b = EcoreFactory.eINSTANCE.createEDataType() => [name = "O1"];
		val o2 = EcoreFactory.eINSTANCE.createEDataType() => [name = "O2"];

		// Check arrangement assumptions
		assertTrue(EcoreUtil.equals(o1a, o1a));
		assertTrue(EcoreUtil.equals(o1a, o1b));
		assertFalse(EcoreUtil.equals(o1a, o2));
		
		// Act & Assert
		val set = new EObjectSet<EDataType>();
		
		set.add(o1a);
		set.add(o1b);
		set.add(o2);
		
		assertEquals(2, set.size);
		assertTrue(set.contains(o1a));
		assertTrue(set.contains(o1b));
		assertTrue(set.contains(o2));
		assertTrue(set.containsAll(Arrays.asList(o1a, o2)));
		assertTrue(set.containsAll(Arrays.asList(o1a, o1b, o2)));
		
		set.remove(o1a);
		assertEquals(1, set.size);
		
		set.remove(o2);
		assertTrue(set.isEmpty);
	}
	
	@Test
	def void retainAllTest() {
		// Arrange
		val o1 = EcoreFactory.eINSTANCE.createEDataType() => [name = "O1"];
		val o2 = EcoreFactory.eINSTANCE.createEDataType() => [name = "O2"];
		val o3 = EcoreFactory.eINSTANCE.createEDataType() => [name = "O3"];
		val o3b = EcoreFactory.eINSTANCE.createEDataType() => [name = "O3"];
		val o4 = EcoreFactory.eINSTANCE.createEDataType() => [name = "O4"];

		// Act & Assert
		val set = new EObjectSet<EDataType>();
		
		set.addAll(Arrays.asList(o1, o2, o3));
		assertEquals(3, set.size);
		
		set.retainAll(Arrays.asList(o1, o2, o3));
		assertEquals(3, set.size);
		
		set.retainAll(Arrays.asList(o4, o1, o2, o3b)); // o4 is not in the set!
		assertEquals(3, set.size);
		
		set.retainAll(Arrays.asList(o4, o1, o2)); // o4 is not in the set!
		assertEquals(2, set.size);
		assertTrue(set.contains(o1));
		assertTrue(set.contains(o2));
		assertFalse(set.contains(o3));
		assertFalse(set.contains(o4));
	}
	
	@Test
	def void removeAllTest() {
		// Arrange
		val o1 = EcoreFactory.eINSTANCE.createEDataType() => [name = "O1"];
		val o2 = EcoreFactory.eINSTANCE.createEDataType() => [name = "O2"];
		val o3a = EcoreFactory.eINSTANCE.createEDataType() => [name = "O3"];
		val o3b = EcoreFactory.eINSTANCE.createEDataType() => [name = "O3"];

		// Act & Assert
		val set = new EObjectSet<EDataType>();
		
		set.addAll(Arrays.asList(o1, o2, o3a));
		assertEquals(3, set.size);
		
		set.removeAll(Arrays.asList());
		assertEquals(3, set.size);
		
		set.removeAll(Arrays.asList(o1, o3b));
		assertEquals(1, set.size);
		
		set.removeAll(Arrays.asList(o1, o3a, o3b));
		assertEquals(1, set.size);
		assertFalse(set.contains(o1));
		assertTrue(set.contains(o2));
		assertFalse(set.contains(o3a));
		assertFalse(set.contains(o3b));
	}
	
	@Test
	def void nestedTest() {
		// Arrange
		val c1a = EcoreFactory.eINSTANCE.createEClass() => [name = "C1"];
		val c1aa = EcoreFactory.eINSTANCE.createEClass() => [name = "C1"];
		val c1b = EcoreFactory.eINSTANCE.createEClass() => [name = "C1"];	
		c1a.getEStructuralFeatures.add(EcoreFactory.eINSTANCE.createEAttribute() => [name = "A"]);	
		c1aa.getEStructuralFeatures.add(EcoreFactory.eINSTANCE.createEAttribute() => [name = "A"]);	
		c1b.getEStructuralFeatures.add(EcoreFactory.eINSTANCE.createEAttribute() => [name = "B"]);	
		
		// Act & Assert
		val set = new EObjectSet<EClass>();
		
		set.add(c1a);
		assertEquals(1, set.size);
		
		set.add(c1a);
		assertEquals(1, set.size);
		
		set.add(c1aa);
		assertEquals(1, set.size);
		
		set.remove(c1b);
		assertEquals(1, set.size);
		
		set.add(c1b);
		assertEquals(2, set.size);	
	}
}