/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Vince Molnar - initial API and implementation
 *   Daniel Darvas - further improvements and additions
 *****************************************************************************/
package cern.plcverif.base.platform.tests;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsSerializer;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.utils.IoUtils;

public class CmdArgumentTest {

	@Test
	public void parseAndSerializeTest() throws SettingsParserException {
		// Arrange
		String[] cmdArgs = { "-id=Demo001", "-name=", "If A is false ...", "-description", "=",
				"This a description that is not too long.", "-sourcefiles", "=",
				"C:\\workspaces\\runtime-EclipseApplication\\SclToCfaTestSuite\\Gyula-test2.scl", "-lf=step7",
				"-lf.entry=testfb", "-lf.compiler=TIA1500", "-inline", "-service=verif", "-job.req=assertion",
				"-job.backend=cern.plcverif.backend.nuxmv", "-job.backend.dynamic=false",
				"-job.reporters={cern.plcverif.verifreporting.html}",
				"-job.reporters.0.targetfile=hello.html" };

		// Act
		SettingsSerializer.parseSettingsFromCommandArgs(Arrays.asList(cmdArgs));
		SettingsSerializer.parseSettingsFromCommandArgs(cmdArgs);
		// IJob job = Platform.getPlatform().parseJob(settings);

		// Smoke test, no assert
		Assert.assertTrue(true);
	}

	// @Test
	// public void ParseAndInterpretTest() throws SettingsParserException {
	// // Arrange
	// String[] cmdArgs = { "-id=Demo001", "-name=", "If A is false ...",
	// "-description", "=",
	// "This a description that is not too long.", "-sourcefiles", "=",
	// "C:\\workspaces\\runtime-EclipseApplication\\SclToCfaTestSuite\\Gyula-test2.scl",
	// "-lf=step7",
	// "-lf.entry=testfb", "-lf.compiler=TIA1500", "-inline", "-service=verif",
	// "-job.req=assertion",
	// "-job.backend=cern.plcverif.backend.nuxmv",
	// "-job.backend.dynamic=false",
	// "-job.reporters={cern.plcverif.verifreporting.html}",
	// "-job.reporters.0.targetfile=hello.html" };
	//
	// // Act
	// SettingsElement settings =
	// SettingsSerializer.parseConfigFromCommandArgs(cmdArgs);
	// Platform.getPlatform().parseJob(settings);
	//
	// // Assert
	// System.out.println(SettingsSerializer.serializeConfig(settings));
	// }

	@Test
	public void settingsDemoTest() throws SettingsParserException {
		String modelFileContent = IoUtils.readResourceFile("/cern/plcverif/base/platform/settings/sampleSettings.txt",
				this.getClass().getClassLoader());
//		System.out.println(modelFileContent);

		@SuppressWarnings("unused")
		SettingsElement settings = SettingsSerializer.parseSettingsFromString(modelFileContent);

//		String plantuml = SettingsToPlantUml.generate(settings);
//		System.out.println();
//		System.out.println("===== PlantUML =====");
//		System.out.println();
//		System.out.println(plantuml);
	}
}
