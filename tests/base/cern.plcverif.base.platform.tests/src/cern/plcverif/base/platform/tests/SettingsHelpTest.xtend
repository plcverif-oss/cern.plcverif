/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.base.platform.tests

import cern.plcverif.base.common.settings.help.SettingsHelp
import cern.plcverif.base.platform.Platform
import java.io.PrintWriter
import java.io.StringWriter
import org.junit.Assert
import org.junit.Test

import cern.plcverif.base.interfaces.data.PlatformConfig
import static cern.plcverif.base.interfaces.data.CfaJobSettings.*
import cern.plcverif.base.common.settings.help.ConsoleHelpWriter

class SettingsHelpTest {
	@Test
	def void smokeTest() {
		Platform.createPlatform(new PlatformConfig()).printSettingsHelp();
	} 

	@Test
	def void lightweightContentTest1() {
		val stringWriter = new StringWriter();
		val help = new SettingsHelp(new ConsoleHelpWriter(new PrintWriter(stringWriter)));
		Platform.createPlatform(new PlatformConfig()).fillSettingsHelp(help);
		
		val result = stringWriter.toString();
		Assert.assertTrue(result.containsAll(ID, NAME, DESCRIPTION, JOB, LANGUAGE_FRONTEND, REDUCTIONS, INLINE, SOURCE_FILES, ENCODING, OUTPUT_DIRECTORY));
		// Assert.assertTrue(result.contains("service.req.pattern")); // Doesn't work in Tycho, plug-ins are not loaded
	}
	
	def boolean containsAll(String container, String... strToBeContained) {
		for (str : strToBeContained) {
			if (container.contains(str) == false) {
				return false;
			}
		}
		return true;
	}
	

}