/*******************************************************************************
 * (C) Copyright CERN 2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.requirement.pattern.configuration;

import org.junit.Test;

import cern.plcverif.library.requirement.pattern.configuration.PatternUtils.PatternValidationException;
import cern.plcverif.library.requirement.pattern.configuration.jaxb.Parameter;
import cern.plcverif.library.requirement.pattern.configuration.jaxb.Pattern;
import cern.plcverif.library.requirement.pattern.configuration.jaxb.Pattern.Tlexpr;
import cern.plcverif.library.requirement.pattern.configuration.jaxb.Tltype;

/**
 * Tests for {@link PatternUtils#validate(Pattern)}.
 */
public class PatternUtilsValidationTest {

	private Pattern createPattern() {
		Pattern pattern = new Pattern();
		pattern.setId("foo");
		pattern.setDescription("Foo description");
		pattern.setTlexpr(new Tlexpr());
		pattern.getTlexpr().setType(Tltype.CTL);
		pattern.getTlexpr().setValue("AG(({PLC_END} AND ({1})) --> ({2}))");
		pattern.setHumanreadable("Foo {1} bar {2}.");

		Parameter param1 = new Parameter();
		param1.setKey("1");
		param1.setDescription("Condition");
		pattern.getParameter().add(param1);

		Parameter param2 = new Parameter();
		param2.setKey("2");
		param2.setDescription("Implication");
		pattern.getParameter().add(param2);

		return pattern;
	}

	@Test
	public void correctPatternValidation() throws PatternValidationException {
		Pattern pattern = createPattern();
		
		PatternUtils.validate(pattern);
		// No exception expected.
	}
	
	@Test
	public void correctLtlPatternValidation() throws PatternValidationException {
		Pattern pattern = createPattern();
		pattern.getTlexpr().setValue("G(({PLC_END} AND ({1})) --> ({2}))");
		pattern.getTlexpr().setType(Tltype.LTL);
		
		PatternUtils.validate(pattern);
		// No exception expected.
	}
	
	@Test(expected = PatternValidationException.class)
	public void unknownParamInTlExpr() throws PatternValidationException {
		Pattern pattern = createPattern();
		pattern.getTlexpr().setValue("AG(({PLC_END} AND ({1})) --> ({999}))");
		
		PatternUtils.validate(pattern);
	}
	
	@Test(expected = PatternValidationException.class)
	public void ltlExprInsteadOfCtl() throws PatternValidationException {
		Pattern pattern = createPattern();
		pattern.getTlexpr().setValue("G(({PLC_END} AND ({1})) --> ({2}))");
		
		PatternUtils.validate(pattern);
	}
	
	@Test(expected = PatternValidationException.class)
	public void ctlExprInsteadOfLtl() throws PatternValidationException {
		Pattern pattern = createPattern();
		pattern.getTlexpr().setType(Tltype.LTL);
		
		PatternUtils.validate(pattern);
	}
	
	@Test(expected = PatternValidationException.class)
	public void invalidTlExpression1() throws PatternValidationException {
		Pattern pattern = createPattern();
		pattern.getTlexpr().setValue("impossible to parse {1} {2}");
		
		PatternUtils.validate(pattern);
	}
	
	@Test(expected = PatternValidationException.class)
	public void invalidTlExpression2() throws PatternValidationException {
		Pattern pattern = createPattern();
		pattern.getTlexpr().setValue("AG({1} --> {2}");
		
		PatternUtils.validate(pattern);
	}
	
	@Test(expected = PatternValidationException.class)
	public void unusedParamInTlExpr() throws PatternValidationException {
		Pattern pattern = createPattern();
		pattern.getTlexpr().setValue("AG({1})");
		
		PatternUtils.validate(pattern);
	}
	
	@Test(expected = PatternValidationException.class)
	public void unusedParamInHumanReadableExpr() throws PatternValidationException {
		Pattern pattern = createPattern();
		pattern.setHumanreadable("Foo {1}");
		
		PatternUtils.validate(pattern);
	}
	
}
