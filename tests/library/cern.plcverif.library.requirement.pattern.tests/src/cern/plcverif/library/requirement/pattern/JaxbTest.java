/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.requirement.pattern;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.junit.Assert;
import org.junit.Test;

import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.exprparser.ExpressionParser;
import cern.plcverif.base.models.exprparser.exceptions.ExpressionParsingException;
import cern.plcverif.library.requirement.pattern.configuration.PatternRegistry;
import cern.plcverif.library.requirement.pattern.configuration.PatternSerialization.PatternLoadingException;
import cern.plcverif.library.requirement.pattern.configuration.jaxb.Pattern;
import cern.plcverif.library.requirement.pattern.configuration.jaxb.Patterns;

public class JaxbTest {
	@Test
	public void smokeTest() {
		try {
			String xml = "<patterns><pattern id='id123'><description>Implication</description><tlexpr type='ctl'>AG(({1}) -> ({2}))</tlexpr><humanreadable>aaa</humanreadable><parameter key='1'><description>Condition.</description></parameter><parameter key='2'><description>Implication.</description></parameter></pattern></patterns>";
			StringReader stringReader = new StringReader(xml);
			
			JAXBContext jc = JAXBContext.newInstance(cern.plcverif.library.requirement.pattern.configuration.jaxb.Patterns.class.getPackageName(),
					cern.plcverif.library.requirement.pattern.configuration.jaxb.Patterns.class.getClassLoader());
			Unmarshaller u = jc.createUnmarshaller();
			System.out.println("Parsing...");
			Patterns fhElement = (Patterns) u.unmarshal(stringReader);
			System.out.println(fhElement.getPattern());
		} catch (JAXBException je) {
			je.printStackTrace();
		}
	}

	@Test
	public void parseDefaultPatternsTest() throws ExpressionParsingException, PatternLoadingException {
		System.out.println("Creating expression parser...");
		ExpressionParser exprParser = new ExpressionParser();
		System.out.println("Done.");
		String content = IoUtils.readResourceFile("default-patterns.xml", this.getClass().getClassLoader());
		PatternRegistry registry = PatternRegistry.create(content);
		for (Pattern p : registry.getAllPatterns()) {
			String patternExpr = p.getTlexpr().getValue();
			System.out.printf("Parsing %s...%n", patternExpr);
			Expression parsedExpr = exprParser.parseExpression(patternExpr);
			Assert.assertNotNull(parsedExpr);
		}
		System.out.println("All OK in parseDefaultPatternsTest.");
	}
}
