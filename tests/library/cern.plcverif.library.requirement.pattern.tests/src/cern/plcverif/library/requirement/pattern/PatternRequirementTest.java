/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.requirement.pattern;

import java.io.IOException;
import java.util.Set;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.interfaces.IAstReference;
import cern.plcverif.base.interfaces.IParserLazyResult;
import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.interfaces.exceptions.AtomParsingException;
import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.Field;
import cern.plcverif.base.models.cfa.cfadeclaration.FieldRef;
import cern.plcverif.base.models.cfa.cfadeclaration.OriginalDataTypeFieldAnnotation;
import cern.plcverif.base.models.cfa.textual.CfaToString;
import cern.plcverif.base.models.expr.AtomicExpression;
import cern.plcverif.library.testing.TestingUtil;
import cern.plcverif.library.testmodels.TestModel;
import cern.plcverif.library.testmodels.TestModelCollection;
import cern.plcverif.verif.interfaces.data.RequirementRepresentationStrategy;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.VerificationResult;

public class PatternRequirementTest {
	private static final CfaDeclarationSafeFactory FACTORY = CfaDeclarationSafeFactory.INSTANCE;
	PatternRequirementExtension reqExtension = new PatternRequirementExtension();

	private CfaNetworkDeclaration cfd;
	private IParserLazyResult parserResultMock;
	private VerificationProblem verifProblem;
	private VerificationResult verifResult;

	@Before
	public void loadTestModel() throws IOException, AtomParsingException {
		TestModel testModel = TestModelCollection.getInstance().getModel("BasicCode.scl");
		this.cfd = testModel.getCfd();

		Field resultField = (EmfHelper.getAllContentsOfType(cfd, Field.class, false,
				(it -> it.getName().equalsIgnoreCase("result")))).iterator().next();
		FieldRef resultFieldRef = FACTORY.appendFieldRef(
				FACTORY.createFieldRef(cfd.getRootDataStructure().getFields().stream()
						.filter(it -> it.getDisplayName().equalsIgnoreCase("basic_OB1")).findFirst().get()),
				resultField);

		// LOWPRI use mocking framework (but Mockito didn't work as Eclipse
		// dependency)
		parserResultMock = new IParserLazyResult() {

			@Override
			public String serializeAtom(AtomicExpression atom) {
				return null;
			}
			
			@Override
			public String serializeAtom(AtomicExpression atom, OriginalDataTypeFieldAnnotation hint) {
				return null;
			}

			@Override
			public AtomicExpression parseAtom(String atom) throws AtomParsingException {
				if (atom.equalsIgnoreCase("basic_OB1.result")) {
					return EcoreUtil.copy(resultFieldRef);
				}
				if (atom.equalsIgnoreCase("TRUE")) {
					return FACTORY.trueLiteral();
				}
				throw new AtomParsingException("The parserResultMock was not able to parse: " + atom);
			}

			@Override
			public Set<String> getFileNames() {
				return null;
			}

			@Override
			public IAstReference<?> getAst() {
				return null;
			}

			@Override
			public CfaNetworkDeclaration generateCfa(JobResult result) {
				return null;
			}
		};

		verifProblem = new VerificationProblem();
		verifProblem.setModel(cfd);

		verifResult = new VerificationResult();
		verifResult.switchToStage("Requirement representation");
	}

	@Test
	public void smokeTest1() {
		PatternRequirement req = reqExtension.createRequirement();
		req.getSettings().setPatternId("pattern-forbidden");
		req.getSettings().setPatternParams(ImmutableMap.of("1", "(basic_OB1.result AND FALSE) = TRUE"));

		req.fillVerificationProblem(verifProblem, parserResultMock, verifResult,
				RequirementRepresentationStrategy.EXPLICIT_WITH_LOCATIONREF);

		Assert.assertNotNull(verifProblem.getRequirement());
		Assert.assertEquals("(AG (<EoC> --> (! ((basic_OB1/result && false) = true))))",
				CfaToString.toDiagString(verifProblem.getRequirement()));
		Assert.assertEquals(verifProblem.getRequirementRepresentation(),
				RequirementRepresentationStrategy.EXPLICIT_WITH_LOCATIONREF);
		TestingUtil.assertNoErrorInLog(verifResult);
	}

	@Test
	public void smokeTest2() {
		PatternRequirement req = reqExtension.createRequirement();
		req.getSettings().setPatternId("pattern-forbidden");
		req.getSettings().setPatternParams(ImmutableMap.of("1", "(basic_OB1.result AND FALSE) = TRUE"));
		Assert.assertTrue(this.cfd.getRootDataStructure().getFields().stream()
				.filter(it -> it.getDisplayName().equals("EoC")).count() == 0);
		
		
		req.fillVerificationProblem(verifProblem, parserResultMock, verifResult,
				RequirementRepresentationStrategy.EXPLICIT_WITH_VARIABLE);

		Assert.assertNotNull(verifProblem.getRequirement());
		Assert.assertEquals("(AG (EoC --> (! ((basic_OB1/result && false) = true))))",
				CfaToString.toDiagString(verifProblem.getRequirement()));
		Assert.assertTrue(this.cfd.getRootDataStructure().getFields().stream()
				.filter(it -> it.getDisplayName().equals("EoC")).count() == 1);
		Assert.assertEquals(verifProblem.getRequirementRepresentation(),
				RequirementRepresentationStrategy.EXPLICIT_WITH_VARIABLE);
		TestingUtil.assertNoErrorInLog(verifResult);
	}

	@Test(expected = PlcverifPlatformException.class)
	public void missingParameterTest() {
		PatternRequirement req = reqExtension.createRequirement();
		req.getSettings().setPatternId("pattern-implication");
		req.getSettings().setPatternParams(ImmutableMap.of("1", "(basic_OB1.result AND FALSE) = TRUE"));

		req.fillVerificationProblem(verifProblem, parserResultMock, verifResult,
				RequirementRepresentationStrategy.EXPLICIT_WITH_LOCATIONREF);

		// Expected to fail: 'pattern1' needs two parameters.
	}
	
	@Test(expected = PlcverifPlatformException.class)
	public void unknownPatternTest() {
		PatternRequirement req = reqExtension.createRequirement();
		req.getSettings().setPatternId("pattern-that-does-not-exist");
		req.getSettings().setPatternParams(ImmutableMap.of("1", "(basic_OB1.result AND FALSE) = TRUE"));

		req.fillVerificationProblem(verifProblem, parserResultMock, verifResult,
				RequirementRepresentationStrategy.EXPLICIT_WITH_LOCATIONREF);

		// Expected to fail: this pattern does not exist.
	}
}
