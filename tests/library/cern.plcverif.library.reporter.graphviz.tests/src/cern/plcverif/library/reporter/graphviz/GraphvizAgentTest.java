/*******************************************************************************
 * (C) Copyright CERN 2024. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Xaver Fink - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reporter.graphviz;

import org.junit.Assert;
import org.junit.Test;

import cern.plcverif.base.common.settings.SettingsElement;

public class GraphvizAgentTest {
	
	@Test
	public void testRetrieveSettings() {
	    // Create a GraphvizSettings instance with sample data
	    GraphvizSettings graphvizSettings = new GraphvizSettings();
	    // Set necessary properties in graphvizSettings
	    graphvizSettings.setGraphvizPath("dummy_path");

	    // Create GraphvizAgent instance with the settings
	    GraphvizAgent graphvizAgent = new GraphvizAgent(graphvizSettings);

	    // Call retrieveSettings() method
	    SettingsElement retrievedSettings = graphvizAgent.retrieveSettings();

	    // Assert that the retrieved settings match the expected values
	    // Add assertions based on the expected content in the retrieved settings
	    Assert.assertNotNull(retrievedSettings);
	    Assert.assertEquals(GraphvizExtension.CMD_ID, retrievedSettings.value());
	    Assert.assertEquals("dummy_path", ((SettingsElement)retrievedSettings.getAttribute(GraphvizSettings.GRAPHVIZ_PATH).get()).value());
	}
	
}
