/*******************************************************************************
 * (C) Copyright CERN 2024. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Xaver Fink - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reporter.graphviz;


import cern.plcverif.base.common.exception.PlcverifPlatformException;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.settings.help.SettingsHelp;

import org.junit.Assert;
import org.junit.Test;

public class GraphvizExtensionTest {
	
	@Test
	public void testCreateDefaultReporter() {
	    GraphvizExtension graphvizExtension = new GraphvizExtension();
	    GraphvizAgent graphvizAgent = (GraphvizAgent) graphvizExtension.createReporter();

	    // Assert that the agent is not null and has been created successfully
	    Assert.assertNotNull(graphvizAgent);
	}
	
	@Test
	public void testCreateReporterWithCustomSettings() throws SettingsParserException {
	    // Create custom settings
	    SettingsElement customSettings = SettingsElement.ofValue("graphviz");
	    customSettings.setSimpleAttribute("graphviz_path","dummy_path");

	    GraphvizExtension graphvizExtension = new GraphvizExtension();
	    GraphvizAgent graphvizAgent = (GraphvizAgent) graphvizExtension.createReporter(customSettings);

	    // Assert that the agent is not null and has been created successfully with custom settings
	    Assert.assertNotNull(graphvizAgent);
	}
	
	@Test(expected = PlcverifPlatformException.class)
	public void testExceptionHandlingOnCreateReporter() throws SettingsParserException {
	    GraphvizExtension graphvizExtension = new GraphvizExtension() {
	        @Override
	        public GraphvizAgent createReporter(SettingsElement settings) throws SettingsParserException {
	            throw new SettingsParserException("Simulating failure to load default settings");
	        }
	    };

	    graphvizExtension.createReporter();
	}
	
	@Test
	public void testCreateReporterWithNullSettings() throws SettingsParserException {
	    GraphvizExtension graphvizExtension = new GraphvizExtension();
	    GraphvizAgent graphvizAgent = (GraphvizAgent) graphvizExtension.createReporter(null);

	    // Assert that the agent is not null and has been created successfully
	    Assert.assertNotNull(graphvizAgent);
	}
	
	@Test
	public void testFillSettingsHelp() {
	    GraphvizExtension graphvizExtension = new GraphvizExtension();
	    SettingsHelp help = new SettingsHelp();

	    graphvizExtension.fillSettingsHelp(help);
	}

}
