/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.library.reporter.summary

import cern.plcverif.base.common.settings.SettingsSerializer
import cern.plcverif.base.common.settings.specific.SpecificSettingsSerializer
import cern.plcverif.library.reporter.summary.impl.SummaryMementoGenerator
import cern.plcverif.verif.interfaces.data.ResultEnum
import cern.plcverif.verif.interfaces.data.VerificationResult
import cern.plcverif.verif.interfaces.data.VerificationStageTags
import cern.plcverif.verif.summary.extensions.parsernodes.VerificationMementoNode
import org.junit.Assert
import org.junit.Test

class IntegrationTest {
	
	@Test
	def void test1() {
		val inputSettings = '''
			-id="model_PlcPerf01-01_algo_M1-assert"
			-name="PlcPerf01-01"
			-description="PlcPerf01-1"
			-sourcefiles={"PlcPerf01.scl", "PlcPerf-common.scl"}    
			-lf=step7
			-lf.entry=PlcPerf01
			-job=verif
			-job.req=assertion
			-job.req.assertion_to_check=assertion1
			-job.req.params={"instance.params[2]", "instance.params[9]", "instance.params[10]", "instance.params[14]"}  
			-job.req.bindings.variable.0 = "instance.boundVar"
			-job.req.bindings.value.0 = "123"
			-job.backend=nusmv
			-job.backend.timeout=600
			-job.backend.dynamic=TRUE
			-job.backend.df=TRUE
			-job.backend.algorithm=Classic
		''';
		
		val expectedResult = '''
			-description = "PlcPerf01-1"
			-id = "model_PlcPerf01-01_algo_M1-assert"
			-info.backend_algorithm = "BACKEND_ALGO_ID"
			-info.backend_name = "BACKEND_NAME"
			-info.plugin_store_keys.0 = "PLUGIN$$KEY"
			-info.plugin_store_values.0 = "VALUE"
			-inline = "false"
			-job = "verif"
			-job.req = "assertion"
			-job.req.bindings.value.0 = "123"
			-job.req.bindings.variable.0 = "instance.boundVar"
			-job.req.inputs = {}
			-job.req.params.0 = "instance.params[2]"
			-job.req.params.1 = "instance.params[9]"
			-job.req.params.2 = "instance.params[10]"
			-job.req.params.3 = "instance.params[14]"
			-lf = "step7"
			-name = "PlcPerf01-01"
			-reductions = {}
			-result = "Satisfied"
			-result.backend_exectime_ms = "10"
			-result.cex_exists = "false"
			-result.exec_date = "..."
			-result.runtime_ms = "15"
			-sourcefiles.0 = "PlcPerf01.scl"
			-sourcefiles.1 = "PlcPerf-common.scl"
		'''
		
		// Create mock verification result
		val verifResult = new VerificationResult();
		verifResult.settings = SettingsSerializer.parseSettingsFromString(inputSettings.toString); 
		verifResult.result = ResultEnum.Satisfied;
		verifResult.backendName = "BACKEND_NAME";
		verifResult.backendAlgorithmId = "BACKEND_ALGO_ID";
		
		// stage
		val execStage = verifResult.switchToStage("test", VerificationStageTags.BACKEND_EXECUTION);
		val stage2 = verifResult.switchToStage("test2");
		execStage.lengthNs = 10_000_000;
		stage2.lengthNs = 5_000_000;
		
		// plugin key-value store
		verifResult.valueStore.addValue("PLUGIN", "KEY", "VALUE");
		
		val reporterSettings = new SummaryReporterSettings(); 
		
		// Generate memento
		var result = SummaryMementoGenerator.represent(verifResult, reporterSettings);
		
		// Remove current time
		result = result.replaceAll("-result.exec_date = \"(.*?)\"", "-result.exec_date = \"...\"");
		
		// Assert
		Assert.assertEquals(expectedResult.toString.trim, result.sort.trim);
		
		// Check if generated memento can be parsed back
		val retrievedSettings = SettingsSerializer.parseSettingsFromString(result);
		SpecificSettingsSerializer.parse(retrievedSettings, VerificationMementoNode);
	}
	
	private def String sort(CharSequence settingsContent) {
		return settingsContent.toString().split(System.lineSeparator).sort.join(System.lineSeparator);
	}
}