/*******************************************************************************
 * (C) Copyright CERN 2017-2023. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Xaver Fink	- modifications for ESBMC
 *******************************************************************************/
package cern.plcverif.library.backend.esbmc.parser

import org.junit.Test
import org.junit.Assert
import cern.plcverif.verif.interfaces.data.ResultEnum
import java.util.Arrays
import cern.plcverif.library.backend.esbmc.EsbmcSettings
import cern.plcverif.base.common.logging.IPlcverifLogger

class EsbmcOutputParserTest {
	@Test
	def multiCycleTest() {
		val toBeParsed = '''
		********************************************************************** 
					** Visual Studio 2022 Developer Command Prompt v17.6.4 
					** Copyright (c) 2022 Microsoft Corporation 
					********************************************************************** 
					[vcvarsall.bat] Environment initialized for: 'x64' 
					ESBMC version 7.2.0 64-bit x86_64 windows 
					Target: 64-bit little-endian x86_64-unknown-windows with esbmclibc 
					Parsing 
					Converting 
					Generating GOTO Program 
					GOTO program creation time: 0.646s 
					GOTO program processing time: 0.124s 
					Checking base case, k = 1 
					Starting Bounded Model Checking 
					Not unwinding 
					Not unwinding 
					Symex completed in: 0.001s (27 assignments) 
					Slicing time: 0.000s (removed 20 assignments) 
					Generated 0 VCC(s), 0 remaining after simplification (7 assignments) 
					BMC program time: 0.001s 
					No bug has been found in the base case 
					Checking forward condition, k = 1 
					Starting Bounded Model Checking 
					Not unwinding 
					Not unwinding 
					Symex completed in: 0.000s (27 assignments) 
					Slicing time: 0.000s (removed 20 assignments) 
					Generated 2 VCC(s), 2 remaining after simplification (7 assignments) 
					No solver specified; defaulting to z3 
					Encoding remaining VCC(s) using bit-vector/floating-point arithmetic 
					Encoding to solver time: 0.000s 
					Solving with solver Z3 v4.11.2 
					Runtime decision procedure: 0.002s 
					The forward condition is unable to prove the property 
					Checking base case, k = 3 
					Starting Bounded Model Checking 
					Unwinding loop 4  iteration  1   file C:\Users\xfink\AppData\Local\Temp\PLCverif-output14869676632573066623\case1.c line 57 function main 
					Unwinding loop 3  iteration  1   file C:\Users\xfink\AppData\Local\Temp\PLCverif-output14869676632573066623\case1.c line 39 function main 
					Unwinding loop 4  iteration  2   file C:\Users\xfink\AppData\Local\Temp\PLCverif-output14869676632573066623\case1.c line 57 function main 
					Unwinding loop 3  iteration  1   file C:\Users\xfink\AppData\Local\Temp\PLCverif-output14869676632573066623\case1.c line 39 function main 
					Not unwinding 
					Unwinding loop 5  iteration  1   file C:\Users\xfink\AppData\Local\Temp\PLCverif-output14869676632573066623\case1.c line 62 function main 
					Unwinding loop 4  iteration  1   file C:\Users\xfink\AppData\Local\Temp\PLCverif-output14869676632573066623\case1.c line 57 function main 
					Unwinding loop 3  iteration  1   file C:\Users\xfink\AppData\Local\Temp\PLCverif-output14869676632573066623\case1.c line 39 function main 
					Unwinding loop 4  iteration  2   file C:\Users\xfink\AppData\Local\Temp\PLCverif-output14869676632573066623\case1.c line 57 function main 
					Unwinding loop 3  iteration  1   file C:\Users\xfink\AppData\Local\Temp\PLCverif-output14869676632573066623\case1.c line 39 function main 
					Not unwinding 
					Unwinding loop 5  iteration  2   file C:\Users\xfink\AppData\Local\Temp\PLCverif-output14869676632573066623\case1.c line 62 function main 
					Unwinding loop 4  iteration  1   file C:\Users\xfink\AppData\Local\Temp\PLCverif-output14869676632573066623\case1.c line 57 function main 
					Unwinding loop 3  iteration  1   file C:\Users\xfink\AppData\Local\Temp\PLCverif-output14869676632573066623\case1.c line 39 function main 
					Unwinding loop 4  iteration  2   file C:\Users\xfink\AppData\Local\Temp\PLCverif-output14869676632573066623\case1.c line 57 function main 
					Unwinding loop 3  iteration  1   file C:\Users\xfink\AppData\Local\Temp\PLCverif-output14869676632573066623\case1.c line 39 function main 
					Not unwinding 
					Not unwinding 
					Symex completed in: 0.004s (148 assignments) 
					Slicing time: 0.001s (removed 96 assignments) 
					Generated 6 VCC(s), 6 remaining after simplification (52 assignments) 
					No solver specified; defaulting to z3 
					Encoding remaining VCC(s) using bit-vector/floating-point arithmetic 
					Encoding to solver time: 0.000s 
					Solving with solver Z3 v4.11.2 
					Runtime decision procedure: 0.003s 
					Building error trace 
					
					[Counterexample] 
					
					
					State 1 file C:\Users\xfink\AppData\Local\Temp\PLCverif-output14869676632573066623\case1.c line 25 function main thread 0 
					---------------------------------------------------- 
					  FuncAbs_in = 16 (00000000 00010000) 
					
					State 2 file C:\Users\xfink\AppData\Local\Temp\PLCverif-output14869676632573066623\case1.c line 44 function main thread 0 
					---------------------------------------------------- 
					  FuncAbs_RET_VAL = 16 (00000000 00010000) 
					
					State 4 file C:\Users\xfink\AppData\Local\Temp\PLCverif-output14869676632573066623\case1.c line 25 function main thread 0 
					---------------------------------------------------- 
					  FuncAbs_in = 512 (00000010 00000000) 
					
					State 5 file C:\Users\xfink\AppData\Local\Temp\PLCverif-output14869676632573066623\case1.c line 44 function main thread 0 
					---------------------------------------------------- 
					  FuncAbs_RET_VAL = 512 (00000010 00000000) 
					
					State 7 file C:\Users\xfink\AppData\Local\Temp\PLCverif-output14869676632573066623\case1.c line 25 function main thread 0 
					---------------------------------------------------- 
					  FuncAbs_in = -32768 (10000000 00000000) 
					
					State 8 file C:\Users\xfink\AppData\Local\Temp\PLCverif-output14869676632573066623\case1.c line 61 function main thread 0 
					---------------------------------------------------- 
					  FuncAbs_RET_VAL = -32768 (10000000 00000000) 
					
					State 9 file C:\Users\xfink\AppData\Local\Temp\PLCverif-output14869676632573066623\case1.c line 35 function main thread 0 
					---------------------------------------------------- 
					Violated property: 
					  file C:\Users\xfink\AppData\Local\Temp\PLCverif-output14869676632573066623\case1.c line 35 function main 
					  assertion error 
					  (_Bool)(!(_Bool)EoC || (signed int)FuncAbs_RET_VAL >= 0) 
					
					
					VERIFICATION FAILED 
					
					Bug found (k = 3)
		''';

		val parser = new EsbmcOutputParser(toBeParsed, new EsbmcSettings(), IPlcverifLogger.NullPlcverifLogger.NULL_LOGGER);
		Assert.assertEquals(ResultEnum.Violated, parser.parseResult());
	
		val vvmaps = parser.parseCounterexampleToMaps();
		Assert.assertNotNull(vvmaps);
		Assert.assertEquals(3, vvmaps.size);
		
		val in1_expectedValues = Arrays.asList("16", "512", "-32768");
		val out_expectedValues = Arrays.asList("16", "512", "-32768");
		
		for (i : 0..2) {
			val vvmap = vvmaps.get(i);
			Assert.assertEquals(in1_expectedValues.get(i), vvmap.get("FuncAbs_in"));
			Assert.assertEquals(out_expectedValues.get(i), vvmap.get("FuncAbs_RET_VAL"));
		}
	}
}
