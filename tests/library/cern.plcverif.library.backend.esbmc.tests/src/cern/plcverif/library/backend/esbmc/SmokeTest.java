/*******************************************************************************
 * (C) Copyright CERN 2017-2023. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Xaver Fink	- modifications for ESBMC
 *******************************************************************************/
package cern.plcverif.library.backend.esbmc;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.cfa.transformation.CallInliner;
import cern.plcverif.base.models.cfa.transformation.ElseEliminator;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.ExpressionSafeFactory;
import cern.plcverif.base.models.expr.UnaryCtlOperator;
import cern.plcverif.library.backend.esbmc.EsbmcSettings.EsbmcModelVariant;
import cern.plcverif.library.backend.esbmc.model.EsbmcModelBuilder;
import cern.plcverif.library.reduction.basic.BasicReductionsExtension;
import cern.plcverif.library.testing.MockJobResult;
import cern.plcverif.library.testmodels.TestModel;
import cern.plcverif.library.testmodels.TestModelCollection;
import cern.plcverif.verif.interfaces.data.RequirementRepresentationStrategy;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.VerificationResult;

public class SmokeTest {
	@Test
	public void batchSmokeTest() {
		for (TestModel testModel : TestModelCollection.getInstance().getAllModels()) {
			smokeTestCfd(testModel);
			smokeTestCfi(testModel);
		}
	}
	
	public void smokeTestCfd(TestModel testModel) {
		System.out.println(String.format("=== %s (ESBMC CFD backend smoke test) ===", testModel.getModelId()));
		
		try {
			// Load and get declaration model; perform needed transformations
			CfaNetworkDeclaration cfd = testModel.getCfd();
			ElseEliminator.transform(cfd);

			// dummy requirement
			Expression req = ExpressionSafeFactory.INSTANCE.createUnaryCtlExpression(
					ExpressionSafeFactory.INSTANCE.trueLiteral()
					, UnaryCtlOperator.AG);
			
			new BasicReductionsExtension().createReduction().reduce(cfd, new MockJobResult(), null);
			
			// Generate ESBMC representation
			VerificationProblem problem = new VerificationProblem();
			problem.setModel(cfd);
			problem.setRequirement(req, RequirementRepresentationStrategy.IMPLICIT);
			CharSequence result = EsbmcModelBuilder.buildModel(problem, new VerificationResult(), new EsbmcSettings(EsbmcModelVariant.CFD));
			Assert.assertNotNull(result);
		} catch (IOException e) {
			e.printStackTrace();
			Assert.fail();
		}
	}
	
	public void smokeTestCfi(TestModel testModel) {
		System.out.println(String.format("=== %s (ESBMC CFI backend smoke test) ===", testModel.getModelId()));
		
		try {
			// Load and get instance model; perform needed transformations
			CfaNetworkInstance cfi = testModel.getCfi(false);
			ElseEliminator.transform(cfi);
			CallInliner.transform(cfi);

			// dummy requirement
			Expression req = ExpressionSafeFactory.INSTANCE.createUnaryCtlExpression(
					ExpressionSafeFactory.INSTANCE.trueLiteral()
					, UnaryCtlOperator.AG);
			
			new BasicReductionsExtension().createReduction().reduce(cfi, new MockJobResult(), null);
			
			// Generate ESBMC representation
			VerificationProblem problem = new VerificationProblem();
			problem.setModel(cfi);
			problem.setRequirement(req, RequirementRepresentationStrategy.IMPLICIT);
			CharSequence result = EsbmcModelBuilder.buildModel(problem, new VerificationResult(), new EsbmcSettings(EsbmcModelVariant.CFI));
			Assert.assertNotNull(result);
		} catch (IOException e) {
			e.printStackTrace();
			Assert.fail();
		}
	}
}
