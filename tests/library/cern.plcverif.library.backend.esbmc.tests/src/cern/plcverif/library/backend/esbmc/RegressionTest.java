/*******************************************************************************
 * (C) Copyright CERN 2017-2023. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Xaver Fink	- modifications for ESBMC
 *******************************************************************************/
package cern.plcverif.library.backend.esbmc;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.transformation.ElseEliminator;
import cern.plcverif.base.models.cfa.validation.CfaValidation;
import cern.plcverif.library.backend.esbmc.EsbmcSettings.EsbmcModelVariant;
import cern.plcverif.library.backend.esbmc.model.EsbmcModelBuilderCfi;
import cern.plcverif.library.requirement.assertion.AssertionRequirementExtension;
import cern.plcverif.library.testmodels.TestModel;
import cern.plcverif.library.testmodels.TestModelCollection;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.VerificationResult;
import cern.plcverif.verif.jobmock.MockVerificationJob;
import cern.plcverif.verif.jobmock.MockVerificationJob.MockVerificationJobConfig;

/**
 * Regression tests relying on the MockVerificationJob, using the whole
 * Verification job except for backend execution.
 */
public class RegressionTest {
	public static final boolean CONTINUE_ON_ASSERT_VIOLATION = false; // NOPMD
	public static boolean writeActualOutputs = CONTINUE_ON_ASSERT_VIOLATION;
	
	private static String fileOutputDir;
	private static Path outputDir;

	@BeforeClass
	public static void setup() throws IOException {
		outputDir = Files.createTempDirectory("esbmc-regression");
		fileOutputDir = Files.createDirectories(Path.of("esbmc-regression-output")).toString();
	}
	
	@Test
	public void modelGenerationRegressionTests() throws IOException, SettingsParserException {
		for (TestModel testModel : TestModelCollection.getInstance().getAllModels()) {
			esbmcModelGenerationRegressionTest(testModel, String.format("/regression-tests/%s.cfi.c", testModel.getModelId()),
					true, EsbmcModelVariant.CFI);
			esbmcModelGenerationRegressionTest(testModel,
					String.format("/regression-tests/%s-noreduction.cfi.c", testModel.getModelId()), false, EsbmcModelVariant.CFI);
			
			esbmcModelGenerationRegressionTest(testModel, String.format("/regression-tests/%s.cfd.c", testModel.getModelId()),
					true, EsbmcModelVariant.CFD);
			esbmcModelGenerationRegressionTest(testModel,
					String.format("/regression-tests/%s-noreduction.cfd.c", testModel.getModelId()), false, EsbmcModelVariant.CFD);
		}
	}

	private void esbmcModelGenerationRegressionTest(TestModel testModel, String expectedSmvPath, boolean reductions, EsbmcModelVariant modelVariant)
			throws IOException, SettingsParserException {
		String modelId = reductions ? testModel.getModelId() : testModel.getModelId() + "-noreduction";
		System.out.println(
				String.format("=== %s (ESBMC-%s model generation regression test with verification loop) ===", modelId, modelVariant));
		

		EsbmcSettings settings = new EsbmcSettings();
		settings.setModelVariant(modelVariant);
		
		// Generate the CFI using the VerificationJob workflow
		MockVerificationJobConfig config = reductions ? new MockVerificationJobConfig()
				: MockVerificationJobConfig.noReduction();
		config.setBackendExtension(new EsbmcBackendExtension());
		config.setRequirementExtension(new AssertionRequirementExtension());
		config.setReporterExtension(null);
		config.setSettings("-job.strict=true -job.backend." + EsbmcSettings.MODEL_VARIANT + "=" + modelVariant.toString());
		
		CfaNetworkDeclaration cfd = testModel.getCfd();
		CfaValidation.validate(cfd);
		VerificationResult result = MockVerificationJob.run(cfd, modelId, outputDir, config, false);
		VerificationProblem verifProblem = result.getVerifProblem().get();
		
		// The ESBMC-specific transformations need to be done here as this
		// part is skipped from the mock job
		ElseEliminator.transform(verifProblem.getModel());
		
		CharSequence actualEsbmcModel = EsbmcModelBuilderCfi.buildModel(verifProblem, result, settings);
		Assert.assertNotNull(actualEsbmcModel);

		if (writeActualOutputs) {
			File directory = new File("esbmc-regression-output-perm");
		    if (! directory.exists()){
		        directory.mkdir();
		    }
			File modelFile = new File( directory, String.format("%s.%s.c" , modelId, modelVariant.toString().toLowerCase()));
			IoUtils.writeAllContent(modelFile, actualEsbmcModel);
		}

		// ASSERT
		if (!CONTINUE_ON_ASSERT_VIOLATION) {
			// Load expected
			String expectedEsbmcModel = IoUtils.readResourceFile(expectedSmvPath, this.getClass().getClassLoader());
			Assert.assertEquals(expectedEsbmcModel.trim(), actualEsbmcModel.toString().trim());
		}
	}
	
	@Test
	public void sentinel() {
		Assert.assertFalse(CONTINUE_ON_ASSERT_VIOLATION);
	}
}
