#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Variables
float float_fc1_real1 = 0.0;
float float_fc1_real2 = 0.0;
int16_t float_fc1_int1 = 0;
uint16_t __assertion_error = 0;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			float_fc1_real1 = 12.3;
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			float_fc1_real1 = 23.0;
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
			float_fc1_real1 = 34.0;
			goto verificationLoop_VerificationLoop_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l3: {
			float_fc1_real1 = (45.6 + 7.0);
			goto verificationLoop_VerificationLoop_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l4: {
			float_fc1_real1 = (float_fc1_real1 + 8.0);
			goto verificationLoop_VerificationLoop_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l5: {
			float_fc1_real1 = (91.2 + 92.3);
			goto verificationLoop_VerificationLoop_l6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l6: {
			float_fc1_real1 = (float_fc1_real1 + 92.4);
			goto verificationLoop_VerificationLoop_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l7: {
			float_fc1_real1 = (float_fc1_real1 + float_fc1_real2);
			goto verificationLoop_VerificationLoop_l8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l8: {
			float_fc1_real1 = (51.1 * 3.0);
			goto verificationLoop_VerificationLoop_l9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l9: {
			float_fc1_real1 = (float_fc1_real1 * 4.0);
			goto verificationLoop_VerificationLoop_l10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l10: {
			float_fc1_real1 = (52.1 * 53.1);
			goto verificationLoop_VerificationLoop_l11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l11: {
			float_fc1_real1 = (float_fc1_real1 * 54.1);
			goto verificationLoop_VerificationLoop_l12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l12: {
			float_fc1_real1 = (float_fc1_real1 * float_fc1_real2);
			goto verificationLoop_VerificationLoop_l13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l13: {
			float_fc1_real1 = (60.1 / 20.3);
			goto verificationLoop_VerificationLoop_l14;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l14: {
			float_fc1_real1 = (float_fc1_real1 / 20.4);
			goto verificationLoop_VerificationLoop_l15;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l15: {
			float_fc1_real1 = (float_fc1_real1 / float_fc1_real2);
			goto verificationLoop_VerificationLoop_l16;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l16: {
			float_fc1_real1 = (12.6 / 2.0);
			goto verificationLoop_VerificationLoop_l17;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l17: {
			float_fc1_real1 = (float_fc1_real2 / 3.0);
			goto verificationLoop_VerificationLoop_l18;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l18: {
			float_fc1_real1 = (30.0 / 2.5);
			goto verificationLoop_VerificationLoop_l19;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l19: {
			float_fc1_real1 = (31.0 / float_fc1_real2);
			goto verificationLoop_VerificationLoop_l20;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l20: {
			float_fc1_int1 = ((int16_t) float_fc1_real1);
			goto verificationLoop_VerificationLoop_l21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l21: {
			float_fc1_int1 = (((int16_t) float_fc1_real1) + 1);
			goto verificationLoop_VerificationLoop_l22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l22: {
			float_fc1_int1 = (((int16_t) float_fc1_real1) * 2);
			goto verificationLoop_VerificationLoop_l23;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l23: {
			float_fc1_int1 = (((int16_t) float_fc1_real1) * ((int16_t) float_fc1_real2));
			goto verificationLoop_VerificationLoop_l24;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l24: {
			float_fc1_real1 = (((float) ((int16_t) float_fc1_real2)) * 0.1);
			goto verificationLoop_VerificationLoop_l25;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l25: {
			goto callEnd;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
