#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Root data structure

// Global variables
bool MX0_0;
bool MX0_1;
bool MX0_2;
bool IX0_3;
bool IX0_4;
uint16_t __assertion_error;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Automata declarations
void gotoFunction1();
void VerificationLoop();

// Automata
void gotoFunction1() {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			MX0_0 = true;
			goto l1;
		//assert(false);
		return;  			}
	l1: {
			MX0_1 = true;
			MX0_2 = true;
			goto l3;
		//assert(false);
		return;  			}
	l3: {
		if (IX0_3) {
			goto l1;
		}
		if (((! IX0_3) && IX0_4)) {
			goto init;
		}
		if (((! IX0_3) && (! ((! IX0_3) && IX0_4)))) {
			goto l9;
		}
		//assert(false);
		return;  			}
	l9: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			IX0_3 = nondet_bool();
			IX0_4 = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			gotoFunction1();
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
int main() {
	// Initial values
	MX0_0 = false;
	MX0_1 = false;
	MX0_2 = false;
	IX0_3 = false;
	IX0_4 = false;
	__assertion_error = 0;
	
	VerificationLoop();
}
