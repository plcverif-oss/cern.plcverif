#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Variables
bool MX0_1 = false;
bool MX0_2 = false;
bool MX0_3 = false;
bool MX0_4 = false;
bool MX0_5 = false;
bool MX1_0 = false;
bool MX1_1 = false;
bool MX1_2 = false;
bool MX1_3 = false;
bool MX1_4 = false;
bool MX1_5 = false;
bool MX1_6 = false;
bool MX2_0 = false;
bool MX2_1 = false;
bool MX2_2 = false;
bool MX2_3 = false;
bool MX2_4 = false;
bool MX2_5 = false;
bool MX2_6 = false;
uint16_t __assertion_error = 0;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_init1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_init2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l3: {
			goto callEnd;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_init: {
			MX0_1 = true;
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l1: {
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l2: {
			MX0_2 = true;
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l3: {
			MX0_3 = true;
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l4: {
		if (MX0_4) {
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l5;
		}
		if ((! MX0_4)) {
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l1;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l5: {
			MX0_5 = true;
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l6: {
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_init1: {
			MX1_0 = true;
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l11: {
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l21: {
			MX1_1 = true;
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l31;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l31: {
		if (MX1_2) {
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l41;
		}
		if ((! MX1_2)) {
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l7;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l41: {
			MX1_3 = true;
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l51;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l51: {
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l61: {
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l7: {
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l8: {
			MX1_4 = true;
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l9: {
		if (MX1_5) {
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l10;
		}
		if ((! MX1_5)) {
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l11;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l10: {
			MX1_6 = true;
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l111;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l111: {
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_init2: {
			MX2_0 = true;
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l12: {
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l22: {
			MX2_1 = true;
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l32;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l32: {
		if (MX2_2) {
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l42;
		}
		if ((! MX2_2)) {
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l71;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l42: {
			MX2_3 = true;
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l52: {
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l62: {
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l81;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l71: {
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l81;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l81: {
			MX2_4 = true;
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l91;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l91: {
		if (MX2_5) {
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l101;
		}
		if ((! MX2_5)) {
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l12;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l101: {
			MX2_6 = true;
			goto verificationLoop_VerificationLoop_repeatFunction_OB1_l112;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_repeatFunction_OB1_l112: {
			goto verificationLoop_VerificationLoop_l3;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
