#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Variables
bool instance_in1 = false;
bool instance_in2 = false;
bool instance_in3 = false;
bool instance_out = false;
bool instance___RLO = false;
bool instance___NFC = false;
bool instance___BR = false;
bool instance___STA = false;
bool instance___OR = false;
bool instance___CC0 = false;
bool instance___CC1 = false;
bool instance___RLO_1 = false;
bool instance___RLO_2 = false;
bool instance___RLO_3 = false;
bool instance___RLO_4 = false;
bool instance___RLO_5 = false;
bool instance___RLO_6 = false;
bool instance___RLO_7 = false;
bool instance___BR_1 = false;
bool instance___BR_2 = false;
bool instance___BR_3 = false;
bool instance___BR_4 = false;
bool instance___BR_5 = false;
bool instance___BR_6 = false;
bool instance___BR_7 = false;
bool instance___OR_1 = false;
bool instance___OR_2 = false;
bool instance___OR_3 = false;
bool instance___OR_4 = false;
bool instance___OR_5 = false;
bool instance___OR_6 = false;
bool instance___OR_7 = false;
bool instance___FC0_1 = false;
bool instance___FC0_2 = false;
bool instance___FC0_3 = false;
bool instance___FC0_4 = false;
bool instance___FC0_5 = false;
bool instance___FC0_6 = false;
bool instance___FC0_7 = false;
bool instance___FC1_1 = false;
bool instance___FC1_2 = false;
bool instance___FC1_3 = false;
bool instance___FC1_4 = false;
bool instance___FC1_5 = false;
bool instance___FC1_6 = false;
bool instance___FC1_7 = false;
bool instance___FC2_1 = false;
bool instance___FC2_2 = false;
bool instance___FC2_3 = false;
bool instance___FC2_4 = false;
bool instance___FC2_5 = false;
bool instance___FC2_6 = false;
bool instance___FC2_7 = false;
uint16_t __assertion_error = 0;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			instance_in1 = nondet_bool();
			instance_in2 = nondet_bool();
			instance_in3 = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			instance___OR = false;
			instance___STA = true;
			instance___RLO = true;
			instance___CC0 = false;
			instance___CC1 = false;
			instance___BR = false;
			instance___NFC = false;
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			instance___RLO_7 = instance___RLO_6;
			instance___BR_7 = instance___BR_6;
			instance___OR_7 = instance___OR_6;
			instance___FC0_7 = instance___FC0_6;
			instance___FC1_7 = instance___FC1_6;
			instance___FC2_7 = instance___FC2_6;
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
			instance___RLO_6 = instance___RLO_5;
			instance___BR_6 = instance___BR_5;
			instance___OR_6 = instance___OR_5;
			instance___FC0_6 = instance___FC0_5;
			instance___FC1_6 = instance___FC1_5;
			instance___FC2_6 = instance___FC2_5;
			goto verificationLoop_VerificationLoop_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l3: {
			instance___RLO_5 = instance___RLO_4;
			instance___BR_5 = instance___BR_4;
			instance___OR_5 = instance___OR_4;
			instance___FC0_5 = instance___FC0_4;
			instance___FC1_5 = instance___FC1_4;
			instance___FC2_5 = instance___FC2_4;
			goto verificationLoop_VerificationLoop_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l4: {
			instance___RLO_4 = instance___RLO_3;
			instance___BR_4 = instance___BR_3;
			instance___OR_4 = instance___OR_3;
			instance___FC0_4 = instance___FC0_3;
			instance___FC1_4 = instance___FC1_3;
			instance___FC2_4 = instance___FC2_3;
			goto verificationLoop_VerificationLoop_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l5: {
			instance___RLO_3 = instance___RLO_2;
			instance___BR_3 = instance___BR_2;
			instance___OR_3 = instance___OR_2;
			instance___FC0_3 = instance___FC0_2;
			instance___FC1_3 = instance___FC1_2;
			instance___FC2_3 = instance___FC2_2;
			goto verificationLoop_VerificationLoop_l6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l6: {
			instance___RLO_2 = instance___RLO_1;
			instance___BR_2 = instance___BR_1;
			instance___OR_2 = instance___OR_1;
			instance___FC0_2 = instance___FC0_1;
			instance___FC1_2 = instance___FC1_1;
			instance___FC2_2 = instance___FC2_1;
			goto verificationLoop_VerificationLoop_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l7: {
			instance___RLO_1 = (instance___RLO && instance___NFC);
			instance___OR_1 = false;
			instance___BR_1 = instance___BR;
			instance___FC0_1 = false;
			instance___FC1_1 = true;
			instance___FC2_1 = false;
			goto verificationLoop_VerificationLoop_l8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l8: {
			instance___OR = false;
			goto verificationLoop_VerificationLoop_l9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l9: {
			instance___STA = instance___RLO;
			goto verificationLoop_VerificationLoop_l10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l10: {
			instance___NFC = false;
			goto verificationLoop_VerificationLoop_l11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l11: {
			instance___RLO = (((! instance___NFC) || instance___RLO) && (instance_in1 || instance___OR));
			goto verificationLoop_VerificationLoop_l12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l12: {
			instance___STA = instance_in1;
			goto verificationLoop_VerificationLoop_l13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l13: {
			instance___NFC = true;
			goto verificationLoop_VerificationLoop_l14;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l14: {
			instance___OR = instance___OR_1;
			goto verificationLoop_VerificationLoop_l15;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l15: {
			instance___NFC = true;
			goto verificationLoop_VerificationLoop_l16;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l16: {
			instance___STA = true;
			goto verificationLoop_VerificationLoop_l17;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l17: {
			instance___BR = instance___BR_1;
			goto verificationLoop_VerificationLoop_l18;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l18: {
		if (((instance___FC0_1 == false) && ((instance___FC1_1 == false) && (instance___FC2_1 == false)))) {
			goto verificationLoop_VerificationLoop_l19;
		}
		if (((instance___FC0_1 == true) && ((instance___FC1_1 == false) && (instance___FC2_1 == false)))) {
			goto verificationLoop_VerificationLoop_l19;
		}
		if (((instance___FC0_1 == false) && ((instance___FC1_1 == true) && (instance___FC2_1 == false)))) {
			goto verificationLoop_VerificationLoop_l19;
		}
		if (((instance___FC0_1 == true) && ((instance___FC1_1 == true) && (instance___FC2_1 == false)))) {
			goto verificationLoop_VerificationLoop_l19;
		}
		if (((instance___FC0_1 == false) && ((instance___FC1_1 == false) && (instance___FC2_1 == true)))) {
			goto verificationLoop_VerificationLoop_l19;
		}
		if (((instance___FC0_1 == false) && ((instance___FC1_1 == false) && (instance___FC2_1 == true)))) {
			goto verificationLoop_VerificationLoop_l19;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l19: {
			instance___RLO_1 = instance___RLO_2;
			instance___BR_1 = instance___BR_2;
			instance___OR_1 = instance___OR_2;
			instance___FC0_1 = instance___FC0_2;
			instance___FC1_1 = instance___FC1_2;
			instance___FC2_1 = instance___FC2_2;
			goto verificationLoop_VerificationLoop_l20;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l20: {
			instance___RLO_2 = instance___RLO_3;
			instance___BR_2 = instance___BR_3;
			instance___OR_2 = instance___OR_3;
			instance___FC0_2 = instance___FC0_3;
			instance___FC1_2 = instance___FC1_3;
			instance___FC2_2 = instance___FC2_3;
			goto verificationLoop_VerificationLoop_l21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l21: {
			instance___RLO_3 = instance___RLO_4;
			instance___BR_3 = instance___BR_4;
			instance___OR_3 = instance___OR_4;
			instance___FC0_3 = instance___FC0_4;
			instance___FC1_3 = instance___FC1_4;
			instance___FC2_3 = instance___FC2_4;
			goto verificationLoop_VerificationLoop_l22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l22: {
			instance___RLO_4 = instance___RLO_5;
			instance___BR_4 = instance___BR_5;
			instance___OR_4 = instance___OR_5;
			instance___FC0_4 = instance___FC0_5;
			instance___FC1_4 = instance___FC1_5;
			instance___FC2_4 = instance___FC2_5;
			goto verificationLoop_VerificationLoop_l23;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l23: {
			instance___RLO_5 = instance___RLO_6;
			instance___BR_5 = instance___BR_6;
			instance___OR_5 = instance___OR_6;
			instance___FC0_5 = instance___FC0_6;
			instance___FC1_5 = instance___FC1_6;
			instance___FC2_5 = instance___FC2_6;
			goto verificationLoop_VerificationLoop_l24;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l24: {
			instance___RLO_6 = instance___RLO_7;
			instance___BR_6 = instance___BR_7;
			instance___OR_6 = instance___OR_7;
			instance___FC0_6 = instance___FC0_7;
			instance___FC1_6 = instance___FC1_7;
			instance___FC2_6 = instance___FC2_7;
			goto verificationLoop_VerificationLoop_l25;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l25: {
			instance___RLO_7 = false;
			instance___BR_7 = false;
			instance___OR_7 = false;
			instance___FC0_7 = false;
			instance___FC1_7 = false;
			instance___FC2_7 = false;
			goto verificationLoop_VerificationLoop_l26;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l26: {
			instance___RLO_7 = instance___RLO_6;
			instance___BR_7 = instance___BR_6;
			instance___OR_7 = instance___OR_6;
			instance___FC0_7 = instance___FC0_6;
			instance___FC1_7 = instance___FC1_6;
			instance___FC2_7 = instance___FC2_6;
			goto verificationLoop_VerificationLoop_l27;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l27: {
			instance___RLO_6 = instance___RLO_5;
			instance___BR_6 = instance___BR_5;
			instance___OR_6 = instance___OR_5;
			instance___FC0_6 = instance___FC0_5;
			instance___FC1_6 = instance___FC1_5;
			instance___FC2_6 = instance___FC2_5;
			goto verificationLoop_VerificationLoop_l28;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l28: {
			instance___RLO_5 = instance___RLO_4;
			instance___BR_5 = instance___BR_4;
			instance___OR_5 = instance___OR_4;
			instance___FC0_5 = instance___FC0_4;
			instance___FC1_5 = instance___FC1_4;
			instance___FC2_5 = instance___FC2_4;
			goto verificationLoop_VerificationLoop_l29;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l29: {
			instance___RLO_4 = instance___RLO_3;
			instance___BR_4 = instance___BR_3;
			instance___OR_4 = instance___OR_3;
			instance___FC0_4 = instance___FC0_3;
			instance___FC1_4 = instance___FC1_3;
			instance___FC2_4 = instance___FC2_3;
			goto verificationLoop_VerificationLoop_l30;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l30: {
			instance___RLO_3 = instance___RLO_2;
			instance___BR_3 = instance___BR_2;
			instance___OR_3 = instance___OR_2;
			instance___FC0_3 = instance___FC0_2;
			instance___FC1_3 = instance___FC1_2;
			instance___FC2_3 = instance___FC2_2;
			goto verificationLoop_VerificationLoop_l31;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l31: {
			instance___RLO_2 = instance___RLO_1;
			instance___BR_2 = instance___BR_1;
			instance___OR_2 = instance___OR_1;
			instance___FC0_2 = instance___FC0_1;
			instance___FC1_2 = instance___FC1_1;
			instance___FC2_2 = instance___FC2_1;
			goto verificationLoop_VerificationLoop_l32;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l32: {
			instance___RLO_1 = (instance___RLO && instance___NFC);
			instance___OR_1 = false;
			instance___BR_1 = instance___BR;
			instance___FC0_1 = false;
			instance___FC1_1 = true;
			instance___FC2_1 = false;
			goto verificationLoop_VerificationLoop_l33;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l33: {
			instance___OR = false;
			goto verificationLoop_VerificationLoop_l34;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l34: {
			instance___STA = instance___RLO;
			goto verificationLoop_VerificationLoop_l35;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l35: {
			instance___NFC = false;
			goto verificationLoop_VerificationLoop_l36;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l36: {
			instance___RLO = (((! instance___NFC) || instance___RLO) && (instance_in2 || instance___OR));
			goto verificationLoop_VerificationLoop_l37;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l37: {
			instance___STA = instance_in2;
			goto verificationLoop_VerificationLoop_l38;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l38: {
			instance___NFC = true;
			goto verificationLoop_VerificationLoop_l39;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l39: {
			instance___OR = instance___OR_1;
			goto verificationLoop_VerificationLoop_l40;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l40: {
			instance___NFC = true;
			goto verificationLoop_VerificationLoop_l41;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l41: {
			instance___STA = true;
			goto verificationLoop_VerificationLoop_l42;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l42: {
			instance___BR = instance___BR_1;
			goto verificationLoop_VerificationLoop_l43;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l43: {
		if (((instance___FC0_1 == false) && ((instance___FC1_1 == false) && (instance___FC2_1 == false)))) {
			goto verificationLoop_VerificationLoop_l44;
		}
		if (((instance___FC0_1 == true) && ((instance___FC1_1 == false) && (instance___FC2_1 == false)))) {
			goto verificationLoop_VerificationLoop_l44;
		}
		if (((instance___FC0_1 == false) && ((instance___FC1_1 == true) && (instance___FC2_1 == false)))) {
			goto verificationLoop_VerificationLoop_l44;
		}
		if (((instance___FC0_1 == true) && ((instance___FC1_1 == true) && (instance___FC2_1 == false)))) {
			goto verificationLoop_VerificationLoop_l44;
		}
		if (((instance___FC0_1 == false) && ((instance___FC1_1 == false) && (instance___FC2_1 == true)))) {
			goto verificationLoop_VerificationLoop_l44;
		}
		if (((instance___FC0_1 == false) && ((instance___FC1_1 == false) && (instance___FC2_1 == true)))) {
			goto verificationLoop_VerificationLoop_l44;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l44: {
			instance___RLO_1 = instance___RLO_2;
			instance___BR_1 = instance___BR_2;
			instance___OR_1 = instance___OR_2;
			instance___FC0_1 = instance___FC0_2;
			instance___FC1_1 = instance___FC1_2;
			instance___FC2_1 = instance___FC2_2;
			goto verificationLoop_VerificationLoop_l45;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l45: {
			instance___RLO_2 = instance___RLO_3;
			instance___BR_2 = instance___BR_3;
			instance___OR_2 = instance___OR_3;
			instance___FC0_2 = instance___FC0_3;
			instance___FC1_2 = instance___FC1_3;
			instance___FC2_2 = instance___FC2_3;
			goto verificationLoop_VerificationLoop_l46;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l46: {
			instance___RLO_3 = instance___RLO_4;
			instance___BR_3 = instance___BR_4;
			instance___OR_3 = instance___OR_4;
			instance___FC0_3 = instance___FC0_4;
			instance___FC1_3 = instance___FC1_4;
			instance___FC2_3 = instance___FC2_4;
			goto verificationLoop_VerificationLoop_l47;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l47: {
			instance___RLO_4 = instance___RLO_5;
			instance___BR_4 = instance___BR_5;
			instance___OR_4 = instance___OR_5;
			instance___FC0_4 = instance___FC0_5;
			instance___FC1_4 = instance___FC1_5;
			instance___FC2_4 = instance___FC2_5;
			goto verificationLoop_VerificationLoop_l48;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l48: {
			instance___RLO_5 = instance___RLO_6;
			instance___BR_5 = instance___BR_6;
			instance___OR_5 = instance___OR_6;
			instance___FC0_5 = instance___FC0_6;
			instance___FC1_5 = instance___FC1_6;
			instance___FC2_5 = instance___FC2_6;
			goto verificationLoop_VerificationLoop_l49;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l49: {
			instance___RLO_6 = instance___RLO_7;
			instance___BR_6 = instance___BR_7;
			instance___OR_6 = instance___OR_7;
			instance___FC0_6 = instance___FC0_7;
			instance___FC1_6 = instance___FC1_7;
			instance___FC2_6 = instance___FC2_7;
			goto verificationLoop_VerificationLoop_l50;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l50: {
			instance___RLO_7 = false;
			instance___BR_7 = false;
			instance___OR_7 = false;
			instance___FC0_7 = false;
			instance___FC1_7 = false;
			instance___FC2_7 = false;
			goto verificationLoop_VerificationLoop_l51;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l51: {
			instance___RLO_7 = instance___RLO_6;
			instance___BR_7 = instance___BR_6;
			instance___OR_7 = instance___OR_6;
			instance___FC0_7 = instance___FC0_6;
			instance___FC1_7 = instance___FC1_6;
			instance___FC2_7 = instance___FC2_6;
			goto verificationLoop_VerificationLoop_l52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l52: {
			instance___RLO_6 = instance___RLO_5;
			instance___BR_6 = instance___BR_5;
			instance___OR_6 = instance___OR_5;
			instance___FC0_6 = instance___FC0_5;
			instance___FC1_6 = instance___FC1_5;
			instance___FC2_6 = instance___FC2_5;
			goto verificationLoop_VerificationLoop_l53;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l53: {
			instance___RLO_5 = instance___RLO_4;
			instance___BR_5 = instance___BR_4;
			instance___OR_5 = instance___OR_4;
			instance___FC0_5 = instance___FC0_4;
			instance___FC1_5 = instance___FC1_4;
			instance___FC2_5 = instance___FC2_4;
			goto verificationLoop_VerificationLoop_l54;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l54: {
			instance___RLO_4 = instance___RLO_3;
			instance___BR_4 = instance___BR_3;
			instance___OR_4 = instance___OR_3;
			instance___FC0_4 = instance___FC0_3;
			instance___FC1_4 = instance___FC1_3;
			instance___FC2_4 = instance___FC2_3;
			goto verificationLoop_VerificationLoop_l55;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l55: {
			instance___RLO_3 = instance___RLO_2;
			instance___BR_3 = instance___BR_2;
			instance___OR_3 = instance___OR_2;
			instance___FC0_3 = instance___FC0_2;
			instance___FC1_3 = instance___FC1_2;
			instance___FC2_3 = instance___FC2_2;
			goto verificationLoop_VerificationLoop_l56;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l56: {
			instance___RLO_2 = instance___RLO_1;
			instance___BR_2 = instance___BR_1;
			instance___OR_2 = instance___OR_1;
			instance___FC0_2 = instance___FC0_1;
			instance___FC1_2 = instance___FC1_1;
			instance___FC2_2 = instance___FC2_1;
			goto verificationLoop_VerificationLoop_l57;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l57: {
			instance___RLO_1 = (instance___RLO && instance___NFC);
			instance___OR_1 = false;
			instance___BR_1 = instance___BR;
			instance___FC0_1 = false;
			instance___FC1_1 = true;
			instance___FC2_1 = false;
			goto verificationLoop_VerificationLoop_l58;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l58: {
			instance___OR = false;
			goto verificationLoop_VerificationLoop_l59;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l59: {
			instance___STA = instance___RLO;
			goto verificationLoop_VerificationLoop_l60;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l60: {
			instance___NFC = false;
			goto verificationLoop_VerificationLoop_l61;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l61: {
			instance___RLO = (((! instance___NFC) || instance___RLO) && (instance_in3 || instance___OR));
			goto verificationLoop_VerificationLoop_l62;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l62: {
			instance___STA = instance_in3;
			goto verificationLoop_VerificationLoop_l63;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l63: {
			instance___NFC = true;
			goto verificationLoop_VerificationLoop_l64;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l64: {
			instance___OR = instance___OR_1;
			goto verificationLoop_VerificationLoop_l65;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l65: {
			instance___NFC = true;
			goto verificationLoop_VerificationLoop_l66;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l66: {
			instance___STA = true;
			goto verificationLoop_VerificationLoop_l67;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l67: {
			instance___BR = instance___BR_1;
			goto verificationLoop_VerificationLoop_l68;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l68: {
		if (((instance___FC0_1 == false) && ((instance___FC1_1 == false) && (instance___FC2_1 == false)))) {
			goto verificationLoop_VerificationLoop_l69;
		}
		if (((instance___FC0_1 == true) && ((instance___FC1_1 == false) && (instance___FC2_1 == false)))) {
			goto verificationLoop_VerificationLoop_l69;
		}
		if (((instance___FC0_1 == false) && ((instance___FC1_1 == true) && (instance___FC2_1 == false)))) {
			goto verificationLoop_VerificationLoop_l69;
		}
		if (((instance___FC0_1 == true) && ((instance___FC1_1 == true) && (instance___FC2_1 == false)))) {
			goto verificationLoop_VerificationLoop_l69;
		}
		if (((instance___FC0_1 == false) && ((instance___FC1_1 == false) && (instance___FC2_1 == true)))) {
			goto verificationLoop_VerificationLoop_l69;
		}
		if (((instance___FC0_1 == false) && ((instance___FC1_1 == false) && (instance___FC2_1 == true)))) {
			goto verificationLoop_VerificationLoop_l69;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l69: {
			instance___RLO_1 = instance___RLO_2;
			instance___BR_1 = instance___BR_2;
			instance___OR_1 = instance___OR_2;
			instance___FC0_1 = instance___FC0_2;
			instance___FC1_1 = instance___FC1_2;
			instance___FC2_1 = instance___FC2_2;
			goto verificationLoop_VerificationLoop_l70;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l70: {
			instance___RLO_2 = instance___RLO_3;
			instance___BR_2 = instance___BR_3;
			instance___OR_2 = instance___OR_3;
			instance___FC0_2 = instance___FC0_3;
			instance___FC1_2 = instance___FC1_3;
			instance___FC2_2 = instance___FC2_3;
			goto verificationLoop_VerificationLoop_l71;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l71: {
			instance___RLO_3 = instance___RLO_4;
			instance___BR_3 = instance___BR_4;
			instance___OR_3 = instance___OR_4;
			instance___FC0_3 = instance___FC0_4;
			instance___FC1_3 = instance___FC1_4;
			instance___FC2_3 = instance___FC2_4;
			goto verificationLoop_VerificationLoop_l72;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l72: {
			instance___RLO_4 = instance___RLO_5;
			instance___BR_4 = instance___BR_5;
			instance___OR_4 = instance___OR_5;
			instance___FC0_4 = instance___FC0_5;
			instance___FC1_4 = instance___FC1_5;
			instance___FC2_4 = instance___FC2_5;
			goto verificationLoop_VerificationLoop_l73;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l73: {
			instance___RLO_5 = instance___RLO_6;
			instance___BR_5 = instance___BR_6;
			instance___OR_5 = instance___OR_6;
			instance___FC0_5 = instance___FC0_6;
			instance___FC1_5 = instance___FC1_6;
			instance___FC2_5 = instance___FC2_6;
			goto verificationLoop_VerificationLoop_l74;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l74: {
			instance___RLO_6 = instance___RLO_7;
			instance___BR_6 = instance___BR_7;
			instance___OR_6 = instance___OR_7;
			instance___FC0_6 = instance___FC0_7;
			instance___FC1_6 = instance___FC1_7;
			instance___FC2_6 = instance___FC2_7;
			goto verificationLoop_VerificationLoop_l75;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l75: {
			instance___RLO_7 = false;
			instance___BR_7 = false;
			instance___OR_7 = false;
			instance___FC0_7 = false;
			instance___FC1_7 = false;
			instance___FC2_7 = false;
			goto verificationLoop_VerificationLoop_l76;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l76: {
			instance_out = instance___RLO;
			goto verificationLoop_VerificationLoop_l77;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l77: {
			instance___OR = false;
			goto verificationLoop_VerificationLoop_l78;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l78: {
			instance___STA = instance___RLO;
			goto verificationLoop_VerificationLoop_l79;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l79: {
			instance___NFC = false;
			goto verificationLoop_VerificationLoop_l80;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l80: {
			goto callEnd;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
