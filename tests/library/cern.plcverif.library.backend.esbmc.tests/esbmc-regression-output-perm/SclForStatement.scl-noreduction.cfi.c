#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Variables
bool MX0_1 = false;
bool MX0_2 = false;
bool MX0_3 = false;
bool MX1_1 = false;
bool MX1_2 = false;
bool MX1_3 = false;
bool MX2_1 = false;
bool MX2_2 = false;
bool MX2_3 = false;
bool MX3_1 = false;
bool MX3_2 = false;
bool MX3_3 = false;
bool MX3_4 = false;
bool MX4_1 = false;
bool MX4_2 = false;
bool MX4_3 = false;
bool MX4_4 = false;
int16_t forFunction1_I = 0;
int16_t forFunction2_I = 0;
int16_t forFunction3_I = 0;
int16_t forFunction4_I = 0;
int16_t forFunction5_I = 0;
uint16_t __assertion_error = 0;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			goto verificationLoop_VerificationLoop_forFunction_OB1_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			goto verificationLoop_VerificationLoop_forFunction_OB1_init1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
			goto verificationLoop_VerificationLoop_forFunction_OB1_init2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l3: {
			goto verificationLoop_VerificationLoop_forFunction_OB1_init3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l4: {
			goto verificationLoop_VerificationLoop_forFunction_OB1_init4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l5: {
			goto callEnd;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_init: {
			MX0_1 = true;
			goto verificationLoop_VerificationLoop_forFunction_OB1_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l1: {
			forFunction1_I = 1;
			goto verificationLoop_VerificationLoop_forFunction_OB1_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l2: {
			MX0_2 = true;
			goto verificationLoop_VerificationLoop_forFunction_OB1_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l3: {
			forFunction1_I = (forFunction1_I + 1);
			goto verificationLoop_VerificationLoop_forFunction_OB1_l6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l4: {
			MX0_3 = true;
			goto verificationLoop_VerificationLoop_forFunction_OB1_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l5: {
		if ((forFunction1_I <= 27)) {
			goto verificationLoop_VerificationLoop_forFunction_OB1_l2;
		}
		if ((! (forFunction1_I <= 27))) {
			goto verificationLoop_VerificationLoop_forFunction_OB1_l4;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l6: {
			goto verificationLoop_VerificationLoop_forFunction_OB1_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l7: {
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_init1: {
			MX1_1 = true;
			goto verificationLoop_VerificationLoop_forFunction_OB1_l11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l11: {
			forFunction2_I = 1;
			goto verificationLoop_VerificationLoop_forFunction_OB1_l51;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l21: {
			MX1_2 = true;
			goto verificationLoop_VerificationLoop_forFunction_OB1_l31;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l31: {
			forFunction2_I = (forFunction2_I + 3);
			goto verificationLoop_VerificationLoop_forFunction_OB1_l61;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l41: {
			MX1_3 = true;
			goto verificationLoop_VerificationLoop_forFunction_OB1_l71;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l51: {
		if ((forFunction2_I <= 27)) {
			goto verificationLoop_VerificationLoop_forFunction_OB1_l21;
		}
		if ((! (forFunction2_I <= 27))) {
			goto verificationLoop_VerificationLoop_forFunction_OB1_l41;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l61: {
			goto verificationLoop_VerificationLoop_forFunction_OB1_l51;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l71: {
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_init2: {
			MX2_1 = true;
			goto verificationLoop_VerificationLoop_forFunction_OB1_l12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l12: {
			forFunction3_I = 27;
			goto verificationLoop_VerificationLoop_forFunction_OB1_l52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l22: {
			MX2_2 = true;
			goto verificationLoop_VerificationLoop_forFunction_OB1_l32;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l32: {
			forFunction3_I = (forFunction3_I + (- 2));
			goto verificationLoop_VerificationLoop_forFunction_OB1_l62;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l42: {
			MX2_3 = true;
			goto verificationLoop_VerificationLoop_forFunction_OB1_l72;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l52: {
		if ((forFunction3_I >= 1)) {
			goto verificationLoop_VerificationLoop_forFunction_OB1_l22;
		}
		if ((! (forFunction3_I >= 1))) {
			goto verificationLoop_VerificationLoop_forFunction_OB1_l42;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l62: {
			goto verificationLoop_VerificationLoop_forFunction_OB1_l52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l72: {
			goto verificationLoop_VerificationLoop_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_init3: {
			MX3_1 = true;
			goto verificationLoop_VerificationLoop_forFunction_OB1_l13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l13: {
			forFunction4_I = 1;
			goto verificationLoop_VerificationLoop_forFunction_OB1_l10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l23: {
			MX3_2 = true;
			goto verificationLoop_VerificationLoop_forFunction_OB1_l33;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l33: {
		if (((forFunction4_I / 2) == 0)) {
			goto verificationLoop_VerificationLoop_forFunction_OB1_l43;
		}
		if ((! ((forFunction4_I / 2) == 0))) {
			goto verificationLoop_VerificationLoop_forFunction_OB1_l63;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l43: {
			goto verificationLoop_VerificationLoop_forFunction_OB1_l8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l53: {
			goto verificationLoop_VerificationLoop_forFunction_OB1_l73;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l63: {
			goto verificationLoop_VerificationLoop_forFunction_OB1_l73;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l73: {
			MX3_3 = true;
			goto verificationLoop_VerificationLoop_forFunction_OB1_l8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l8: {
			forFunction4_I = (forFunction4_I + 1);
			goto verificationLoop_VerificationLoop_forFunction_OB1_l111;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l9: {
			MX3_4 = true;
			goto verificationLoop_VerificationLoop_forFunction_OB1_l121;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l10: {
		if ((forFunction4_I <= 10)) {
			goto verificationLoop_VerificationLoop_forFunction_OB1_l23;
		}
		if ((! (forFunction4_I <= 10))) {
			goto verificationLoop_VerificationLoop_forFunction_OB1_l9;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l111: {
			goto verificationLoop_VerificationLoop_forFunction_OB1_l10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l121: {
			goto verificationLoop_VerificationLoop_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_init4: {
			MX4_1 = true;
			goto verificationLoop_VerificationLoop_forFunction_OB1_l14;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l14: {
			forFunction5_I = 1;
			goto verificationLoop_VerificationLoop_forFunction_OB1_l101;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l24: {
			MX4_2 = true;
			goto verificationLoop_VerificationLoop_forFunction_OB1_l34;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l34: {
		if (((forFunction5_I / 2) == 0)) {
			goto verificationLoop_VerificationLoop_forFunction_OB1_l44;
		}
		if ((! ((forFunction5_I / 2) == 0))) {
			goto verificationLoop_VerificationLoop_forFunction_OB1_l64;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l44: {
			goto verificationLoop_VerificationLoop_forFunction_OB1_l91;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l54: {
			goto verificationLoop_VerificationLoop_forFunction_OB1_l74;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l64: {
			goto verificationLoop_VerificationLoop_forFunction_OB1_l74;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l74: {
			MX4_3 = true;
			goto verificationLoop_VerificationLoop_forFunction_OB1_l81;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l81: {
			forFunction5_I = (forFunction5_I + 1);
			goto verificationLoop_VerificationLoop_forFunction_OB1_l112;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l91: {
			MX4_4 = true;
			goto verificationLoop_VerificationLoop_forFunction_OB1_l122;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l101: {
		if ((forFunction5_I <= 10)) {
			goto verificationLoop_VerificationLoop_forFunction_OB1_l24;
		}
		if ((! (forFunction5_I <= 10))) {
			goto verificationLoop_VerificationLoop_forFunction_OB1_l91;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l112: {
			goto verificationLoop_VerificationLoop_forFunction_OB1_l101;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_forFunction_OB1_l122: {
			goto verificationLoop_VerificationLoop_l5;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
