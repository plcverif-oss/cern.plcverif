#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Variables
bool IX0_0 = false;
bool expr_fc1_boolVar1 = false;
bool expr_fc1_boolVar2 = false;
int16_t expr_fc1_intVar1 = 0;
int16_t expr_fc1_intVar2 = 0;
uint8_t expr_fc1_byteVar1 = 0;
uint8_t expr_fc1_byteVar2 = 0;
uint16_t expr_fc1_wordVar1 = 0;
uint16_t expr_fc1_wordVar2 = 0;
uint32_t expr_fc1_dwordVar1 = 0;
uint32_t expr_fc1_dwordVar2 = 0;
uint16_t __assertion_error = 0;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			IX0_0 = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			goto verificationLoop_VerificationLoop_expr_OB1_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			goto callEnd;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_init: {
			expr_fc1_boolVar1 = (expr_fc1_boolVar1 && expr_fc1_boolVar2);
			goto verificationLoop_VerificationLoop_expr_OB1_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l1: {
			expr_fc1_boolVar1 = (expr_fc1_boolVar1 && false);
			goto verificationLoop_VerificationLoop_expr_OB1_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l2: {
			expr_fc1_boolVar1 = (expr_fc1_boolVar1 && true);
			goto verificationLoop_VerificationLoop_expr_OB1_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l3: {
			expr_fc1_boolVar1 = (IX0_0 && true);
			goto verificationLoop_VerificationLoop_expr_OB1_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l4: {
			expr_fc1_boolVar1 = (true && true);
			goto verificationLoop_VerificationLoop_expr_OB1_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l5: {
			expr_fc1_boolVar1 = (false && true);
			goto verificationLoop_VerificationLoop_expr_OB1_l6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l6: {
			expr_fc1_boolVar1 = (expr_fc1_boolVar1 || expr_fc1_boolVar2);
			goto verificationLoop_VerificationLoop_expr_OB1_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l7: {
			expr_fc1_boolVar1 = (expr_fc1_boolVar1 || false);
			goto verificationLoop_VerificationLoop_expr_OB1_l8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l8: {
			expr_fc1_boolVar1 = (expr_fc1_boolVar1 || true);
			goto verificationLoop_VerificationLoop_expr_OB1_l9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l9: {
			expr_fc1_boolVar1 = (IX0_0 || true);
			goto verificationLoop_VerificationLoop_expr_OB1_l10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l10: {
			expr_fc1_boolVar1 = (true || true);
			goto verificationLoop_VerificationLoop_expr_OB1_l11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l11: {
			expr_fc1_boolVar1 = (false || true);
			goto verificationLoop_VerificationLoop_expr_OB1_l12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l12: {
			expr_fc1_boolVar1 = (expr_fc1_boolVar1 != expr_fc1_boolVar2);
			goto verificationLoop_VerificationLoop_expr_OB1_l13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l13: {
			expr_fc1_boolVar1 = (expr_fc1_boolVar1 != false);
			goto verificationLoop_VerificationLoop_expr_OB1_l14;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l14: {
			expr_fc1_boolVar1 = (expr_fc1_boolVar1 != true);
			goto verificationLoop_VerificationLoop_expr_OB1_l15;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l15: {
			expr_fc1_boolVar1 = (IX0_0 != true);
			goto verificationLoop_VerificationLoop_expr_OB1_l16;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l16: {
			expr_fc1_boolVar1 = (true != true);
			goto verificationLoop_VerificationLoop_expr_OB1_l17;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l17: {
			expr_fc1_boolVar1 = (false != true);
			goto verificationLoop_VerificationLoop_expr_OB1_l18;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l18: {
			expr_fc1_byteVar1 = (expr_fc1_byteVar1 & expr_fc1_byteVar2);
			goto verificationLoop_VerificationLoop_expr_OB1_l19;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l19: {
			expr_fc1_byteVar1 = (expr_fc1_byteVar1 & 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l20;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l20: {
			expr_fc1_byteVar1 = (expr_fc1_byteVar1 & 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l21: {
			expr_fc1_byteVar1 = (1 & 3);
			goto verificationLoop_VerificationLoop_expr_OB1_l22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l22: {
			expr_fc1_wordVar1 = (expr_fc1_wordVar1 & expr_fc1_wordVar2);
			goto verificationLoop_VerificationLoop_expr_OB1_l23;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l23: {
			expr_fc1_wordVar1 = (expr_fc1_wordVar1 & 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l24;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l24: {
			expr_fc1_wordVar1 = (expr_fc1_wordVar1 & 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l25;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l25: {
			expr_fc1_wordVar1 = (expr_fc1_wordVar1 & 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l26;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l26: {
			expr_fc1_dwordVar1 = (expr_fc1_dwordVar1 & expr_fc1_dwordVar2);
			goto verificationLoop_VerificationLoop_expr_OB1_l27;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l27: {
			expr_fc1_dwordVar1 = (expr_fc1_dwordVar1 & 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l28;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l28: {
			expr_fc1_dwordVar1 = (expr_fc1_dwordVar1 & 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l29;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l29: {
			expr_fc1_dwordVar1 = (expr_fc1_dwordVar1 & 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l30;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l30: {
			expr_fc1_dwordVar1 = (expr_fc1_dwordVar1 & 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l31;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l31: {
			expr_fc1_byteVar1 = (expr_fc1_byteVar1 | expr_fc1_byteVar2);
			goto verificationLoop_VerificationLoop_expr_OB1_l32;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l32: {
			expr_fc1_byteVar1 = (expr_fc1_byteVar1 | 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l33;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l33: {
			expr_fc1_byteVar1 = (expr_fc1_byteVar1 | 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l34;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l34: {
			expr_fc1_byteVar1 = (1 | 3);
			goto verificationLoop_VerificationLoop_expr_OB1_l35;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l35: {
			expr_fc1_wordVar1 = (expr_fc1_wordVar1 | expr_fc1_wordVar2);
			goto verificationLoop_VerificationLoop_expr_OB1_l36;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l36: {
			expr_fc1_wordVar1 = (expr_fc1_wordVar1 | 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l37;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l37: {
			expr_fc1_wordVar1 = (expr_fc1_wordVar1 | 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l38;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l38: {
			expr_fc1_wordVar1 = (expr_fc1_wordVar1 | 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l39;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l39: {
			expr_fc1_dwordVar1 = (expr_fc1_dwordVar1 | expr_fc1_dwordVar2);
			goto verificationLoop_VerificationLoop_expr_OB1_l40;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l40: {
			expr_fc1_dwordVar1 = (expr_fc1_dwordVar1 | 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l41;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l41: {
			expr_fc1_dwordVar1 = (expr_fc1_dwordVar1 | 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l42;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l42: {
			expr_fc1_dwordVar1 = (expr_fc1_dwordVar1 | 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l43;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l43: {
			expr_fc1_dwordVar1 = (expr_fc1_dwordVar1 | 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l44;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l44: {
			expr_fc1_byteVar1 = (expr_fc1_byteVar1 ^ expr_fc1_byteVar2);
			goto verificationLoop_VerificationLoop_expr_OB1_l45;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l45: {
			expr_fc1_byteVar1 = (expr_fc1_byteVar1 ^ 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l46;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l46: {
			expr_fc1_byteVar1 = (expr_fc1_byteVar1 ^ 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l47;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l47: {
			expr_fc1_byteVar1 = (1 ^ 3);
			goto verificationLoop_VerificationLoop_expr_OB1_l48;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l48: {
			expr_fc1_wordVar1 = (expr_fc1_wordVar1 ^ expr_fc1_wordVar2);
			goto verificationLoop_VerificationLoop_expr_OB1_l49;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l49: {
			expr_fc1_wordVar1 = (expr_fc1_wordVar1 ^ 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l50;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l50: {
			expr_fc1_wordVar1 = (expr_fc1_wordVar1 ^ 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l51;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l51: {
			expr_fc1_wordVar1 = (expr_fc1_wordVar1 ^ 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l52: {
			expr_fc1_dwordVar1 = (expr_fc1_dwordVar1 ^ expr_fc1_dwordVar2);
			goto verificationLoop_VerificationLoop_expr_OB1_l53;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l53: {
			expr_fc1_dwordVar1 = (expr_fc1_dwordVar1 ^ 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l54;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l54: {
			expr_fc1_dwordVar1 = (expr_fc1_dwordVar1 ^ 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l55;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l55: {
			expr_fc1_dwordVar1 = (expr_fc1_dwordVar1 ^ 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l56;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l56: {
			expr_fc1_dwordVar1 = (expr_fc1_dwordVar1 ^ 123);
			goto verificationLoop_VerificationLoop_expr_OB1_l57;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_expr_OB1_l57: {
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
