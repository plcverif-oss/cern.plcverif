#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Variables
int16_t typeconv_db1_in_int = 0;
int32_t typeconv_db1_in_dint = 0;
float typeconv_db1_in_real = 0.0;
int16_t typeconv_db1_out_int = 0;
int32_t typeconv_db1_out_dint = 0;
float typeconv_db1_out_real = 0.0;
uint16_t __assertion_error = 0;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			typeconv_db1_in_dint = nondet_int32_t();
			typeconv_db1_in_int = nondet_int16_t();
			typeconv_db1_in_real = nondet_float();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			typeconv_db1_out_int = typeconv_db1_in_int;
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			typeconv_db1_out_dint = typeconv_db1_in_dint;
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
			typeconv_db1_out_real = typeconv_db1_in_real;
			goto verificationLoop_VerificationLoop_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l3: {
			typeconv_db1_out_real = ((float) typeconv_db1_in_int);
			goto verificationLoop_VerificationLoop_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l4: {
			typeconv_db1_out_dint = ((int32_t) typeconv_db1_in_int);
			goto verificationLoop_VerificationLoop_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l5: {
			typeconv_db1_out_int = ((int16_t) typeconv_db1_in_dint);
			goto verificationLoop_VerificationLoop_l6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l6: {
			typeconv_db1_out_int = ((int16_t) typeconv_db1_in_real);
			goto verificationLoop_VerificationLoop_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l7: {
			typeconv_db1_out_dint = ((int32_t) typeconv_db1_in_real);
			goto verificationLoop_VerificationLoop_l8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l8: {
			typeconv_db1_out_dint = 56;
			goto verificationLoop_VerificationLoop_l9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l9: {
			typeconv_db1_out_dint = 567;
			goto verificationLoop_VerificationLoop_l10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l10: {
			typeconv_db1_out_dint = 567;
			goto verificationLoop_VerificationLoop_l11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l11: {
			typeconv_db1_out_dint = 567;
			goto verificationLoop_VerificationLoop_l12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l12: {
			typeconv_db1_out_dint = (((int32_t) typeconv_db1_in_int) * 10);
			goto verificationLoop_VerificationLoop_l13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l13: {
			goto callEnd;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
