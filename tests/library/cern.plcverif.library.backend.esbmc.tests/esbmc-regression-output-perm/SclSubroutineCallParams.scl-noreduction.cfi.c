#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Variables
bool MX2_0 = false;
bool MX0_0 = false;
bool MX3_0 = false;
bool MX3_1 = false;
bool MX1_0 = false;
bool MX1_1 = false;
bool MX4_0 = false;
bool MX4_1 = false;
bool MX5_4 = false;
bool MX5_0 = false;
bool MX5_5 = false;
bool MX5_1 = false;
bool MX5_6 = false;
bool MX5_2 = false;
bool MX6_4 = false;
bool MX6_0 = false;
bool MX6_5 = false;
bool MX6_1 = false;
bool MX6_6 = false;
bool MX6_2 = false;
bool callParamsFunc2_RET_VAL = false;
bool callParamsFunc3_IN = false;
bool callParamsFunc3_RET_VAL = false;
bool callParamsFunc4_i1 = false;
bool callParamsFunc4_i2 = false;
bool callParamsFunc4_RET_VAL = false;
bool callParamsFunc5_i1 = false;
bool callParamsFunc5_i2 = false;
bool callParamsFunc5_out1 = false;
bool callParamsFunc5_RET_VAL = false;
bool callParamsFunc6_i1 = false;
bool callParamsFunc6_i2 = false;
bool callParamsFunc6_inout1 = false;
bool callParamsFunc6_RET_VAL = false;
bool callParamsFunc3_inlined_1_IN = false;
bool callParamsFunc3_inlined_1_RET_VAL = false;
bool callParamsFunc3_inlined_2_IN = false;
bool callParamsFunc3_inlined_2_RET_VAL = false;
bool callParamsFunc4_inlined_3_i1 = false;
bool callParamsFunc4_inlined_3_i2 = false;
bool callParamsFunc4_inlined_3_RET_VAL = false;
bool callParamsFunc4_inlined_4_i1 = false;
bool callParamsFunc4_inlined_4_i2 = false;
bool callParamsFunc4_inlined_4_RET_VAL = false;
bool callParamsFunc5_inlined_5_i1 = false;
bool callParamsFunc5_inlined_5_i2 = false;
bool callParamsFunc5_inlined_5_out1 = false;
bool callParamsFunc5_inlined_5_RET_VAL = false;
bool callParamsFunc5_inlined_6_i1 = false;
bool callParamsFunc5_inlined_6_i2 = false;
bool callParamsFunc5_inlined_6_out1 = false;
bool callParamsFunc5_inlined_6_RET_VAL = false;
bool callParamsFunc5_inlined_7_i1 = false;
bool callParamsFunc5_inlined_7_i2 = false;
bool callParamsFunc5_inlined_7_out1 = false;
bool callParamsFunc5_inlined_7_RET_VAL = false;
bool callParamsFunc6_inlined_8_i1 = false;
bool callParamsFunc6_inlined_8_i2 = false;
bool callParamsFunc6_inlined_8_inout1 = false;
bool callParamsFunc6_inlined_8_RET_VAL = false;
bool callParamsFunc6_inlined_9_i1 = false;
bool callParamsFunc6_inlined_9_i2 = false;
bool callParamsFunc6_inlined_9_inout1 = false;
bool callParamsFunc6_inlined_9_RET_VAL = false;
bool callParamsFunc6_inlined_10_i1 = false;
bool callParamsFunc6_inlined_10_i2 = false;
bool callParamsFunc6_inlined_10_inout1 = false;
bool callParamsFunc6_inlined_10_RET_VAL = false;
uint16_t __assertion_error = 0;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			goto verificationLoop_VerificationLoop_callParams_OB1_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			goto callEnd;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_init: {
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_l1: {
			callParamsFunc3_inlined_1_IN = MX0_0;
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_init1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_l2: {
			callParamsFunc3_inlined_2_IN = MX0_0;
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_init2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_l3: {
			callParamsFunc4_inlined_3_i1 = MX1_0;
			callParamsFunc4_inlined_3_i2 = MX1_1;
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_init3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_l4: {
			callParamsFunc4_inlined_4_i2 = MX1_0;
			callParamsFunc4_inlined_4_i1 = MX1_1;
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_init4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_l5: {
			callParamsFunc5_inlined_5_i1 = MX1_0;
			callParamsFunc5_inlined_5_i2 = MX1_1;
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_init5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_l6: {
			callParamsFunc5_inlined_6_i1 = MX1_0;
			callParamsFunc5_inlined_6_i2 = MX1_1;
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_init6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_l7: {
			callParamsFunc5_inlined_7_i1 = MX1_0;
			callParamsFunc5_inlined_7_i2 = MX1_1;
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_init7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_l8: {
			callParamsFunc6_inlined_8_i1 = MX1_0;
			callParamsFunc6_inlined_8_i2 = MX1_1;
			callParamsFunc6_inlined_8_inout1 = MX6_4;
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_init8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_l9: {
			callParamsFunc6_inlined_9_inout1 = MX6_5;
			callParamsFunc6_inlined_9_i1 = MX1_0;
			callParamsFunc6_inlined_9_i2 = MX1_1;
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_init9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_l10: {
			callParamsFunc6_inlined_10_i1 = MX1_0;
			callParamsFunc6_inlined_10_i2 = MX1_1;
			callParamsFunc6_inlined_10_inout1 = MX6_6;
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_init10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_l11: {
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_init: {
			callParamsFunc2_RET_VAL = false;
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l1: {
			MX2_0 = callParamsFunc2_RET_VAL;
			goto verificationLoop_VerificationLoop_callParams_OB1_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_init1: {
			callParamsFunc3_inlined_1_RET_VAL = (! callParamsFunc3_inlined_1_IN);
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l11: {
			MX3_0 = callParamsFunc3_inlined_1_RET_VAL;
			goto verificationLoop_VerificationLoop_callParams_OB1_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_init2: {
			callParamsFunc3_inlined_2_RET_VAL = (! callParamsFunc3_inlined_2_IN);
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l12: {
			MX3_1 = callParamsFunc3_inlined_2_RET_VAL;
			goto verificationLoop_VerificationLoop_callParams_OB1_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_init3: {
			callParamsFunc4_inlined_3_RET_VAL = (callParamsFunc4_inlined_3_i1 && callParamsFunc4_inlined_3_i2);
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l13: {
			MX4_0 = callParamsFunc4_inlined_3_RET_VAL;
			goto verificationLoop_VerificationLoop_callParams_OB1_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_init4: {
			callParamsFunc4_inlined_4_RET_VAL = (callParamsFunc4_inlined_4_i1 && callParamsFunc4_inlined_4_i2);
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l14;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l14: {
			MX4_1 = callParamsFunc4_inlined_4_RET_VAL;
			goto verificationLoop_VerificationLoop_callParams_OB1_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_init5: {
			callParamsFunc5_inlined_5_out1 = (callParamsFunc5_inlined_5_i1 || callParamsFunc5_inlined_5_i2);
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l15;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l15: {
			callParamsFunc5_inlined_5_RET_VAL = (callParamsFunc5_inlined_5_i1 && callParamsFunc5_inlined_5_i2);
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l2: {
			MX5_4 = callParamsFunc5_inlined_5_out1;
			MX5_0 = callParamsFunc5_inlined_5_RET_VAL;
			goto verificationLoop_VerificationLoop_callParams_OB1_l6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_init6: {
			callParamsFunc5_inlined_6_out1 = (callParamsFunc5_inlined_6_i1 || callParamsFunc5_inlined_6_i2);
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l16;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l16: {
			callParamsFunc5_inlined_6_RET_VAL = (callParamsFunc5_inlined_6_i1 && callParamsFunc5_inlined_6_i2);
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l21: {
			MX5_5 = callParamsFunc5_inlined_6_out1;
			MX5_1 = callParamsFunc5_inlined_6_RET_VAL;
			goto verificationLoop_VerificationLoop_callParams_OB1_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_init7: {
			callParamsFunc5_inlined_7_out1 = (callParamsFunc5_inlined_7_i1 || callParamsFunc5_inlined_7_i2);
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l17;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l17: {
			callParamsFunc5_inlined_7_RET_VAL = (callParamsFunc5_inlined_7_i1 && callParamsFunc5_inlined_7_i2);
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l22: {
			MX5_6 = callParamsFunc5_inlined_7_out1;
			MX5_2 = callParamsFunc5_inlined_7_RET_VAL;
			goto verificationLoop_VerificationLoop_callParams_OB1_l8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_init8: {
			callParamsFunc6_inlined_8_inout1 = (callParamsFunc6_inlined_8_inout1 || callParamsFunc6_inlined_8_i2);
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l18;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l18: {
			callParamsFunc6_inlined_8_RET_VAL = (callParamsFunc6_inlined_8_i1 && callParamsFunc6_inlined_8_i2);
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l23;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l23: {
			MX6_4 = callParamsFunc6_inlined_8_inout1;
			MX6_0 = callParamsFunc6_inlined_8_RET_VAL;
			goto verificationLoop_VerificationLoop_callParams_OB1_l9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_init9: {
			callParamsFunc6_inlined_9_inout1 = (callParamsFunc6_inlined_9_inout1 || callParamsFunc6_inlined_9_i2);
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l19;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l19: {
			callParamsFunc6_inlined_9_RET_VAL = (callParamsFunc6_inlined_9_i1 && callParamsFunc6_inlined_9_i2);
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l24;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l24: {
			MX6_5 = callParamsFunc6_inlined_9_inout1;
			MX6_1 = callParamsFunc6_inlined_9_RET_VAL;
			goto verificationLoop_VerificationLoop_callParams_OB1_l10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_init10: {
			callParamsFunc6_inlined_10_inout1 = (callParamsFunc6_inlined_10_inout1 || callParamsFunc6_inlined_10_i2);
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l110;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l110: {
			callParamsFunc6_inlined_10_RET_VAL = (callParamsFunc6_inlined_10_i1 && callParamsFunc6_inlined_10_i2);
			goto verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l25;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callParams_OB1_callParamsFunc1_l25: {
			MX6_6 = callParamsFunc6_inlined_10_inout1;
			MX6_2 = callParamsFunc6_inlined_10_RET_VAL;
			goto verificationLoop_VerificationLoop_callParams_OB1_l11;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
