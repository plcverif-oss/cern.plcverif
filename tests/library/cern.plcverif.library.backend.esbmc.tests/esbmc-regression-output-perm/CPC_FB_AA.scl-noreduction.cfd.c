#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Root data structure
typedef struct {
	uint16_t ParReg;
} __CPC_ANALOGALARM_PARAM;
typedef struct {
	float I;
	float PosSt;
	float HHSt;
	float HSt;
	float LSt;
	float LLSt;
	float HH;
	float H;
	float L;
	float LL;
	int16_t PAlDt;
	__CPC_ANALOGALARM_PARAM PAA;
	bool AuAlAck;
	bool ISt;
	bool WSt;
	bool AlUnAck;
	bool MAlBRSt;
	bool AuIhMB;
	bool IOErrorW;
	bool IOSimuW;
	bool IOError;
	bool IOSimu;
	bool AuEHH;
	bool AuEH;
	bool AuEL;
	bool AuELL;
	bool HHAlSt;
	bool HWSt;
	bool LWSt;
	bool LLAlSt;
	bool ConfigW;
	bool EHHSt;
	bool EHSt;
	bool ELSt;
	bool ELLSt;
	bool ArmRcpSt;
	bool PAuAckAl;
	bool MAlBSetRst_old;
	bool MAlAck_old;
	bool AuAlAck_old;
	bool Alarm_Cond_old;
	bool MNewHHR_old;
	bool MNewHR_old;
	bool MNewLR_old;
	bool MNewLLR_old;
	bool ArmRcp_old;
	bool ActRcp_old;
	bool AlUnAck_old;
	bool HH_AlarmPh1;
	bool HH_AlarmPh2;
	bool H_AlarmPh1;
	bool H_AlarmPh2;
	bool L_AlarmPh1;
	bool L_AlarmPh2;
	bool LL_AlarmPh1;
	bool LL_AlarmPh2;
	int32_t HH_TimeAlarm;
	int32_t H_TimeAlarm;
	int32_t L_TimeAlarm;
	int32_t LL_TimeAlarm;
	int32_t TimeAlarm;
	int16_t Iinc;
	int16_t Winc;
	int16_t HHinc;
	int16_t Hinc;
	int16_t Linc;
	int16_t LLinc;
	bool WISt;
	bool WWSt;
	bool WHHAlSt;
	bool WHWSt;
	bool WLWSt;
	bool WLLAlSt;
} __CPC_DB_AA;
typedef struct {
	bool new;
	bool old;
	bool re;
	bool fe;
} __DETECT_EDGE;
typedef struct {
	bool new;
	bool old;
	bool RET_VAL;
} __R_EDGE;
typedef struct {
	bool ParRegb[16];
} __anonymous_type;
typedef struct {
	uint16_t Manreg01;
	bool Manreg01b[16];
	float HH;
	float H;
	float L;
	float LL;
	uint16_t StsReg01;
	uint16_t StsReg02;
	__CPC_DB_AA Perst;
} __CPC_FB_AA;

// Global variables
bool First_Cycle;
int32_t UNICOS_LiveCounter;
int32_t T_CYCLE;
__R_EDGE R_EDGE1;
__DETECT_EDGE DETECT_EDGE1;
__CPC_FB_AA instance;
__R_EDGE R_EDGE1_inlined_1;
__R_EDGE R_EDGE1_inlined_2;
__R_EDGE R_EDGE1_inlined_3;
__R_EDGE R_EDGE1_inlined_4;
__R_EDGE R_EDGE1_inlined_5;
__R_EDGE R_EDGE1_inlined_6;
__R_EDGE R_EDGE1_inlined_7;
__R_EDGE R_EDGE1_inlined_8;
__R_EDGE R_EDGE1_inlined_9;
__R_EDGE R_EDGE1_inlined_10;
uint16_t __assertion_error;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Automata declarations
void CPC_FB_AA(__CPC_FB_AA *__context);
void R_EDGE(__R_EDGE *__context);
void DETECT_EDGE(__DETECT_EDGE *__context);
void VerificationLoop();

// Automata
void CPC_FB_AA(__CPC_FB_AA *__context) {
	// Temporary variables
	bool E_MAlBSetRst;
	bool E_MAlAck;
	bool E_AuAlAck;
	bool E_Alarm_Cond;
	bool E_MNewHHR;
	bool E_MNewHR;
	bool E_MNewLR;
	bool E_MNewLLR;
	bool E_ArmRcp;
	bool E_ActRcp;
	bool RE_AlUnAck;
	bool FE_AlUnAck;
	bool PosHHW;
	bool PosHW;
	bool PosLW;
	bool PosLLW;
	bool ConfigW;
	bool IhMHHSt;
	bool IhMHSt;
	bool IhMLSt;
	bool IhMLLSt;
	bool PAuAckAl;
	uint16_t TempStsReg01;
	bool StsReg01b[16];
	uint16_t TempStsReg02;
	bool StsReg02b[16];
	__CPC_ANALOGALARM_PARAM TempPAA;
	__anonymous_type PAAb;
	float PulseWidth;
	
	// Start with initial location
	goto init;
	init: {
			__context->Manreg01b[0] = ((__context->Manreg01 & 256) != 0);
			goto varview_refresh1;
		//assert(false);
		return;  			}
	l1: {
			TempStsReg01 = __context->StsReg01;
			goto x2;
		//assert(false);
		return;  			}
	l2: {
			TempStsReg02 = __context->StsReg02;
			goto x3;
		//assert(false);
		return;  			}
	l3: {
			TempPAA = __context->Perst.PAA;
			goto x4;
		//assert(false);
		return;  			}
	l4: {
			// Assign inputs
			R_EDGE1_inlined_1.new = __context->Manreg01b[10];
			R_EDGE1_inlined_1.old = __context->Perst.ArmRcp_old;
			R_EDGE(&R_EDGE1_inlined_1);
			// Assign outputs
			__context->Perst.ArmRcp_old = R_EDGE1_inlined_1.old;
			E_ArmRcp = R_EDGE1_inlined_1.RET_VAL;
			goto l5;
		//assert(false);
		return;  			}
	l5: {
			// Assign inputs
			R_EDGE1_inlined_2.new = __context->Manreg01b[11];
			R_EDGE1_inlined_2.old = __context->Perst.ActRcp_old;
			R_EDGE(&R_EDGE1_inlined_2);
			// Assign outputs
			__context->Perst.ActRcp_old = R_EDGE1_inlined_2.old;
			E_ActRcp = R_EDGE1_inlined_2.RET_VAL;
			goto l6;
		//assert(false);
		return;  			}
	l6: {
			// Assign inputs
			R_EDGE1_inlined_3.new = __context->Manreg01b[0];
			R_EDGE1_inlined_3.old = __context->Perst.MNewHHR_old;
			R_EDGE(&R_EDGE1_inlined_3);
			// Assign outputs
			__context->Perst.MNewHHR_old = R_EDGE1_inlined_3.old;
			E_MNewHHR = R_EDGE1_inlined_3.RET_VAL;
			goto l7;
		//assert(false);
		return;  			}
	l7: {
			// Assign inputs
			R_EDGE1_inlined_4.new = __context->Manreg01b[1];
			R_EDGE1_inlined_4.old = __context->Perst.MNewHR_old;
			R_EDGE(&R_EDGE1_inlined_4);
			// Assign outputs
			__context->Perst.MNewHR_old = R_EDGE1_inlined_4.old;
			E_MNewHR = R_EDGE1_inlined_4.RET_VAL;
			goto l8;
		//assert(false);
		return;  			}
	l8: {
			// Assign inputs
			R_EDGE1_inlined_5.new = __context->Manreg01b[2];
			R_EDGE1_inlined_5.old = __context->Perst.MAlBSetRst_old;
			R_EDGE(&R_EDGE1_inlined_5);
			// Assign outputs
			__context->Perst.MAlBSetRst_old = R_EDGE1_inlined_5.old;
			E_MAlBSetRst = R_EDGE1_inlined_5.RET_VAL;
			goto l9;
		//assert(false);
		return;  			}
	l9: {
			// Assign inputs
			R_EDGE1_inlined_6.new = __context->Manreg01b[4];
			R_EDGE1_inlined_6.old = __context->Perst.MNewLR_old;
			R_EDGE(&R_EDGE1_inlined_6);
			// Assign outputs
			__context->Perst.MNewLR_old = R_EDGE1_inlined_6.old;
			E_MNewLR = R_EDGE1_inlined_6.RET_VAL;
			goto l10;
		//assert(false);
		return;  			}
	l10: {
			// Assign inputs
			R_EDGE1_inlined_7.new = __context->Manreg01b[5];
			R_EDGE1_inlined_7.old = __context->Perst.MNewLLR_old;
			R_EDGE(&R_EDGE1_inlined_7);
			// Assign outputs
			__context->Perst.MNewLLR_old = R_EDGE1_inlined_7.old;
			E_MNewLLR = R_EDGE1_inlined_7.RET_VAL;
			goto l11;
		//assert(false);
		return;  			}
	l11: {
			// Assign inputs
			R_EDGE1_inlined_8.new = __context->Manreg01b[7];
			R_EDGE1_inlined_8.old = __context->Perst.MAlAck_old;
			R_EDGE(&R_EDGE1_inlined_8);
			// Assign outputs
			__context->Perst.MAlAck_old = R_EDGE1_inlined_8.old;
			E_MAlAck = R_EDGE1_inlined_8.RET_VAL;
			goto l12;
		//assert(false);
		return;  			}
	l12: {
			// Assign inputs
			R_EDGE1_inlined_9.new = __context->Perst.AuAlAck;
			R_EDGE1_inlined_9.old = __context->Perst.AuAlAck_old;
			R_EDGE(&R_EDGE1_inlined_9);
			// Assign outputs
			__context->Perst.AuAlAck_old = R_EDGE1_inlined_9.old;
			E_AuAlAck = R_EDGE1_inlined_9.RET_VAL;
			goto l13;
		//assert(false);
		return;  			}
	l13: {
			IhMHHSt = PAAb.ParRegb[8];
			goto l14;
		//assert(false);
		return;  			}
	l14: {
			IhMHSt = PAAb.ParRegb[9];
			goto l15;
		//assert(false);
		return;  			}
	l15: {
			IhMLSt = PAAb.ParRegb[10];
			goto l16;
		//assert(false);
		return;  			}
	l16: {
			IhMLLSt = PAAb.ParRegb[11];
			goto l17;
		//assert(false);
		return;  			}
	l17: {
			PAuAckAl = PAAb.ParRegb[12];
			goto l18;
		//assert(false);
		return;  			}
	l18: {
		if (E_MAlBSetRst) {
			goto l19;
		}
		if ((! E_MAlBSetRst)) {
			goto l21;
		}
		//assert(false);
		return;  			}
	l19: {
			__context->Perst.MAlBRSt = (! __context->Perst.MAlBRSt);
			goto l20;
		//assert(false);
		return;  			}
	l20: {
			goto l22;
		//assert(false);
		return;  			}
	l21: {
			goto l22;
		//assert(false);
		return;  			}
	l22: {
		if (__context->Perst.AuIhMB) {
			goto l23;
		}
		if ((! __context->Perst.AuIhMB)) {
			goto l25;
		}
		//assert(false);
		return;  			}
	l23: {
			__context->Perst.MAlBRSt = false;
			goto l24;
		//assert(false);
		return;  			}
	l24: {
			goto l26;
		//assert(false);
		return;  			}
	l25: {
			goto l26;
		//assert(false);
		return;  			}
	l26: {
			__context->Perst.EHHSt = __context->Perst.AuEHH;
			goto l27;
		//assert(false);
		return;  			}
	l27: {
			__context->Perst.EHSt = __context->Perst.AuEH;
			goto l28;
		//assert(false);
		return;  			}
	l28: {
			__context->Perst.ELSt = __context->Perst.AuEL;
			goto l29;
		//assert(false);
		return;  			}
	l29: {
			__context->Perst.ELLSt = __context->Perst.AuELL;
			goto l30;
		//assert(false);
		return;  			}
	l30: {
		if ((E_ArmRcp && (! ((((E_MNewHHR && IhMHHSt) || (E_MNewHR && IhMHSt)) || (E_MNewLR && IhMLSt)) || (E_MNewLLR && IhMLLSt))))) {
			goto l31;
		}
		if ((! (E_ArmRcp && (! ((((E_MNewHHR && IhMHHSt) || (E_MNewHR && IhMHSt)) || (E_MNewLR && IhMLSt)) || (E_MNewLLR && IhMLLSt)))))) {
			goto l33;
		}
		//assert(false);
		return;  			}
	l31: {
			__context->Perst.ArmRcpSt = true;
			goto l32;
		//assert(false);
		return;  			}
	l32: {
			goto l34;
		//assert(false);
		return;  			}
	l33: {
			goto l34;
		//assert(false);
		return;  			}
	l34: {
		if ((E_ArmRcp && E_ActRcp)) {
			goto l35;
		}
		if ((! (E_ArmRcp && E_ActRcp))) {
			goto l37;
		}
		//assert(false);
		return;  			}
	l35: {
			__context->Perst.ArmRcpSt = false;
			goto l36;
		//assert(false);
		return;  			}
	l36: {
			goto l38;
		//assert(false);
		return;  			}
	l37: {
			goto l38;
		//assert(false);
		return;  			}
	l38: {
		if (First_Cycle) {
			goto l39;
		}
		if ((! First_Cycle)) {
			goto l44;
		}
		//assert(false);
		return;  			}
	l39: {
			__context->Perst.HHSt = __context->HH;
			goto l40;
		//assert(false);
		return;  			}
	l40: {
			__context->Perst.HSt = __context->H;
			goto l41;
		//assert(false);
		return;  			}
	l41: {
			__context->Perst.LSt = __context->L;
			goto l42;
		//assert(false);
		return;  			}
	l42: {
			__context->Perst.LLSt = __context->LL;
			goto l43;
		//assert(false);
		return;  			}
	l43: {
			goto l45;
		//assert(false);
		return;  			}
	l44: {
			goto l45;
		//assert(false);
		return;  			}
	l45: {
		if (IhMHHSt) {
			goto l46;
		}
		if (((! IhMHHSt) && ((E_MNewHHR && (! __context->Perst.ArmRcpSt)) || ((E_MNewHHR && __context->Perst.ArmRcpSt) && E_ActRcp)))) {
			goto l48;
		}
		if (((! IhMHHSt) && (! ((! IhMHHSt) && ((E_MNewHHR && (! __context->Perst.ArmRcpSt)) || ((E_MNewHHR && __context->Perst.ArmRcpSt) && E_ActRcp)))))) {
			goto l50;
		}
		//assert(false);
		return;  			}
	l46: {
			__context->Perst.HHSt = __context->Perst.HH;
			goto l47;
		//assert(false);
		return;  			}
	l47: {
			goto l51;
		//assert(false);
		return;  			}
	l48: {
			__context->Perst.HHSt = __context->HH;
			goto l49;
		//assert(false);
		return;  			}
	l49: {
			goto l51;
		//assert(false);
		return;  			}
	l50: {
			goto l51;
		//assert(false);
		return;  			}
	l51: {
		if (IhMHSt) {
			goto l52;
		}
		if (((! IhMHSt) && ((E_MNewHR && (! __context->Perst.ArmRcpSt)) || ((E_MNewHR && __context->Perst.ArmRcpSt) && E_ActRcp)))) {
			goto l54;
		}
		if (((! IhMHSt) && (! ((! IhMHSt) && ((E_MNewHR && (! __context->Perst.ArmRcpSt)) || ((E_MNewHR && __context->Perst.ArmRcpSt) && E_ActRcp)))))) {
			goto l56;
		}
		//assert(false);
		return;  			}
	l52: {
			__context->Perst.HSt = __context->Perst.H;
			goto l53;
		//assert(false);
		return;  			}
	l53: {
			goto l57;
		//assert(false);
		return;  			}
	l54: {
			__context->Perst.HSt = __context->H;
			goto l55;
		//assert(false);
		return;  			}
	l55: {
			goto l57;
		//assert(false);
		return;  			}
	l56: {
			goto l57;
		//assert(false);
		return;  			}
	l57: {
		if (IhMLSt) {
			goto l58;
		}
		if (((! IhMLSt) && ((E_MNewLR && (! __context->Perst.ArmRcpSt)) || ((E_MNewLR && __context->Perst.ArmRcpSt) && E_ActRcp)))) {
			goto l60;
		}
		if (((! IhMLSt) && (! ((! IhMLSt) && ((E_MNewLR && (! __context->Perst.ArmRcpSt)) || ((E_MNewLR && __context->Perst.ArmRcpSt) && E_ActRcp)))))) {
			goto l62;
		}
		//assert(false);
		return;  			}
	l58: {
			__context->Perst.LSt = __context->Perst.L;
			goto l59;
		//assert(false);
		return;  			}
	l59: {
			goto l63;
		//assert(false);
		return;  			}
	l60: {
			__context->Perst.LSt = __context->L;
			goto l61;
		//assert(false);
		return;  			}
	l61: {
			goto l63;
		//assert(false);
		return;  			}
	l62: {
			goto l63;
		//assert(false);
		return;  			}
	l63: {
		if (IhMLLSt) {
			goto l64;
		}
		if (((! IhMLLSt) && ((E_MNewLLR && (! __context->Perst.ArmRcpSt)) || ((E_MNewLLR && __context->Perst.ArmRcpSt) && E_ActRcp)))) {
			goto l66;
		}
		if (((! IhMLLSt) && (! ((! IhMLLSt) && ((E_MNewLLR && (! __context->Perst.ArmRcpSt)) || ((E_MNewLLR && __context->Perst.ArmRcpSt) && E_ActRcp)))))) {
			goto l68;
		}
		//assert(false);
		return;  			}
	l64: {
			__context->Perst.LLSt = __context->Perst.LL;
			goto l65;
		//assert(false);
		return;  			}
	l65: {
			goto l69;
		//assert(false);
		return;  			}
	l66: {
			__context->Perst.LLSt = __context->LL;
			goto l67;
		//assert(false);
		return;  			}
	l67: {
			goto l69;
		//assert(false);
		return;  			}
	l68: {
			goto l69;
		//assert(false);
		return;  			}
	l69: {
		if ((__context->Perst.EHHSt && (__context->Perst.I > __context->Perst.HHSt))) {
			goto l70;
		}
		if ((! (__context->Perst.EHHSt && (__context->Perst.I > __context->Perst.HHSt)))) {
			goto l72;
		}
		//assert(false);
		return;  			}
	l70: {
			__context->Perst.HH_AlarmPh1 = true;
			goto l71;
		//assert(false);
		return;  			}
	l71: {
			goto l75;
		//assert(false);
		return;  			}
	l72: {
			__context->Perst.HH_AlarmPh1 = false;
			goto l73;
		//assert(false);
		return;  			}
	l73: {
			__context->Perst.HH_AlarmPh2 = false;
			goto l74;
		//assert(false);
		return;  			}
	l74: {
			goto l75;
		//assert(false);
		return;  			}
	l75: {
		if (__context->Perst.HH_AlarmPh1) {
			goto l76;
		}
		if ((! __context->Perst.HH_AlarmPh1)) {
			goto l104;
		}
		//assert(false);
		return;  			}
	l76: {
		if ((! __context->Perst.HH_AlarmPh2)) {
			goto l77;
		}
		if ((! (! __context->Perst.HH_AlarmPh2))) {
			goto l80;
		}
		//assert(false);
		return;  			}
	l77: {
			__context->Perst.HH_TimeAlarm = (UNICOS_LiveCounter + ((int32_t) __context->Perst.PAlDt));
			goto l78;
		//assert(false);
		return;  			}
	l78: {
			__context->Perst.HH_AlarmPh2 = true;
			goto l79;
		//assert(false);
		return;  			}
	l79: {
			goto l81;
		//assert(false);
		return;  			}
	l80: {
			goto l81;
		//assert(false);
		return;  			}
	l81: {
		if (__context->Perst.HH_AlarmPh2) {
			goto l82;
		}
		if ((! __context->Perst.HH_AlarmPh2)) {
			goto l98;
		}
		//assert(false);
		return;  			}
	l82: {
		if ((UNICOS_LiveCounter >= __context->Perst.HH_TimeAlarm)) {
			goto l83;
		}
		if (((! (UNICOS_LiveCounter >= __context->Perst.HH_TimeAlarm)) && __context->Perst.MAlBRSt)) {
			goto l91;
		}
		if (((! (UNICOS_LiveCounter >= __context->Perst.HH_TimeAlarm)) && (! ((! (UNICOS_LiveCounter >= __context->Perst.HH_TimeAlarm)) && __context->Perst.MAlBRSt)))) {
			goto l94;
		}
		//assert(false);
		return;  			}
	l83: {
		if (__context->Perst.MAlBRSt) {
			goto l84;
		}
		if ((! __context->Perst.MAlBRSt)) {
			goto l87;
		}
		//assert(false);
		return;  			}
	l84: {
			__context->Perst.HHAlSt = false;
			goto l85;
		//assert(false);
		return;  			}
	l85: {
			PosHHW = true;
			goto l86;
		//assert(false);
		return;  			}
	l86: {
			goto l90;
		//assert(false);
		return;  			}
	l87: {
			__context->Perst.HHAlSt = true;
			goto l88;
		//assert(false);
		return;  			}
	l88: {
			PosHHW = false;
			goto l89;
		//assert(false);
		return;  			}
	l89: {
			goto l90;
		//assert(false);
		return;  			}
	l90: {
			goto l97;
		//assert(false);
		return;  			}
	l91: {
			__context->Perst.HHAlSt = false;
			goto l92;
		//assert(false);
		return;  			}
	l92: {
			PosHHW = false;
			goto l93;
		//assert(false);
		return;  			}
	l93: {
			goto l97;
		//assert(false);
		return;  			}
	l94: {
			__context->Perst.HHAlSt = false;
			goto l95;
		//assert(false);
		return;  			}
	l95: {
			PosHHW = true;
			goto l96;
		//assert(false);
		return;  			}
	l96: {
			goto l97;
		//assert(false);
		return;  			}
	l97: {
			goto l99;
		//assert(false);
		return;  			}
	l98: {
			goto l99;
		//assert(false);
		return;  			}
	l99: {
		if (((__context->Perst.HH_TimeAlarm - ((int32_t) __context->Perst.PAlDt)) > UNICOS_LiveCounter)) {
			goto l100;
		}
		if ((! ((__context->Perst.HH_TimeAlarm - ((int32_t) __context->Perst.PAlDt)) > UNICOS_LiveCounter))) {
			goto l102;
		}
		//assert(false);
		return;  			}
	l100: {
			__context->Perst.HH_TimeAlarm = (UNICOS_LiveCounter + ((int32_t) __context->Perst.PAlDt));
			goto l101;
		//assert(false);
		return;  			}
	l101: {
			goto l103;
		//assert(false);
		return;  			}
	l102: {
			goto l103;
		//assert(false);
		return;  			}
	l103: {
			goto l108;
		//assert(false);
		return;  			}
	l104: {
			__context->Perst.HH_TimeAlarm = 0;
			goto l105;
		//assert(false);
		return;  			}
	l105: {
			__context->Perst.HHAlSt = false;
			goto l106;
		//assert(false);
		return;  			}
	l106: {
			PosHHW = false;
			goto l107;
		//assert(false);
		return;  			}
	l107: {
			goto l108;
		//assert(false);
		return;  			}
	l108: {
		if ((__context->Perst.ELLSt && (__context->Perst.I < __context->Perst.LLSt))) {
			goto l109;
		}
		if ((! (__context->Perst.ELLSt && (__context->Perst.I < __context->Perst.LLSt)))) {
			goto l111;
		}
		//assert(false);
		return;  			}
	l109: {
			__context->Perst.LL_AlarmPh1 = true;
			goto l110;
		//assert(false);
		return;  			}
	l110: {
			goto l114;
		//assert(false);
		return;  			}
	l111: {
			__context->Perst.LL_AlarmPh1 = false;
			goto l112;
		//assert(false);
		return;  			}
	l112: {
			__context->Perst.LL_AlarmPh2 = false;
			goto l113;
		//assert(false);
		return;  			}
	l113: {
			goto l114;
		//assert(false);
		return;  			}
	l114: {
		if (__context->Perst.LL_AlarmPh1) {
			goto l115;
		}
		if ((! __context->Perst.LL_AlarmPh1)) {
			goto l143;
		}
		//assert(false);
		return;  			}
	l115: {
		if ((! __context->Perst.LL_AlarmPh2)) {
			goto l116;
		}
		if ((! (! __context->Perst.LL_AlarmPh2))) {
			goto l119;
		}
		//assert(false);
		return;  			}
	l116: {
			__context->Perst.LL_TimeAlarm = (UNICOS_LiveCounter + ((int32_t) __context->Perst.PAlDt));
			goto l117;
		//assert(false);
		return;  			}
	l117: {
			__context->Perst.LL_AlarmPh2 = true;
			goto l118;
		//assert(false);
		return;  			}
	l118: {
			goto l120;
		//assert(false);
		return;  			}
	l119: {
			goto l120;
		//assert(false);
		return;  			}
	l120: {
		if (__context->Perst.LL_AlarmPh2) {
			goto l121;
		}
		if ((! __context->Perst.LL_AlarmPh2)) {
			goto l137;
		}
		//assert(false);
		return;  			}
	l121: {
		if ((UNICOS_LiveCounter >= __context->Perst.LL_TimeAlarm)) {
			goto l122;
		}
		if (((! (UNICOS_LiveCounter >= __context->Perst.LL_TimeAlarm)) && __context->Perst.MAlBRSt)) {
			goto l130;
		}
		if (((! (UNICOS_LiveCounter >= __context->Perst.LL_TimeAlarm)) && (! ((! (UNICOS_LiveCounter >= __context->Perst.LL_TimeAlarm)) && __context->Perst.MAlBRSt)))) {
			goto l133;
		}
		//assert(false);
		return;  			}
	l122: {
		if (__context->Perst.MAlBRSt) {
			goto l123;
		}
		if ((! __context->Perst.MAlBRSt)) {
			goto l126;
		}
		//assert(false);
		return;  			}
	l123: {
			__context->Perst.LLAlSt = false;
			goto l124;
		//assert(false);
		return;  			}
	l124: {
			PosLLW = true;
			goto l125;
		//assert(false);
		return;  			}
	l125: {
			goto l129;
		//assert(false);
		return;  			}
	l126: {
			__context->Perst.LLAlSt = true;
			goto l127;
		//assert(false);
		return;  			}
	l127: {
			PosLLW = false;
			goto l128;
		//assert(false);
		return;  			}
	l128: {
			goto l129;
		//assert(false);
		return;  			}
	l129: {
			goto l136;
		//assert(false);
		return;  			}
	l130: {
			__context->Perst.LLAlSt = false;
			goto l131;
		//assert(false);
		return;  			}
	l131: {
			PosLLW = false;
			goto l132;
		//assert(false);
		return;  			}
	l132: {
			goto l136;
		//assert(false);
		return;  			}
	l133: {
			__context->Perst.LLAlSt = false;
			goto l134;
		//assert(false);
		return;  			}
	l134: {
			PosLLW = true;
			goto l135;
		//assert(false);
		return;  			}
	l135: {
			goto l136;
		//assert(false);
		return;  			}
	l136: {
			goto l138;
		//assert(false);
		return;  			}
	l137: {
			goto l138;
		//assert(false);
		return;  			}
	l138: {
		if (((__context->Perst.LL_TimeAlarm - ((int32_t) __context->Perst.PAlDt)) > UNICOS_LiveCounter)) {
			goto l139;
		}
		if ((! ((__context->Perst.LL_TimeAlarm - ((int32_t) __context->Perst.PAlDt)) > UNICOS_LiveCounter))) {
			goto l141;
		}
		//assert(false);
		return;  			}
	l139: {
			__context->Perst.LL_TimeAlarm = (UNICOS_LiveCounter + ((int32_t) __context->Perst.PAlDt));
			goto l140;
		//assert(false);
		return;  			}
	l140: {
			goto l142;
		//assert(false);
		return;  			}
	l141: {
			goto l142;
		//assert(false);
		return;  			}
	l142: {
			goto l147;
		//assert(false);
		return;  			}
	l143: {
			__context->Perst.LL_TimeAlarm = 0;
			goto l144;
		//assert(false);
		return;  			}
	l144: {
			__context->Perst.LLAlSt = false;
			goto l145;
		//assert(false);
		return;  			}
	l145: {
			PosLLW = false;
			goto l146;
		//assert(false);
		return;  			}
	l146: {
			goto l147;
		//assert(false);
		return;  			}
	l147: {
		if ((__context->Perst.EHSt && (__context->Perst.I > __context->Perst.HSt))) {
			goto l148;
		}
		if ((! (__context->Perst.EHSt && (__context->Perst.I > __context->Perst.HSt)))) {
			goto l150;
		}
		//assert(false);
		return;  			}
	l148: {
			__context->Perst.H_AlarmPh1 = true;
			goto l149;
		//assert(false);
		return;  			}
	l149: {
			goto l153;
		//assert(false);
		return;  			}
	l150: {
			__context->Perst.H_AlarmPh1 = false;
			goto l151;
		//assert(false);
		return;  			}
	l151: {
			__context->Perst.H_AlarmPh2 = false;
			goto l152;
		//assert(false);
		return;  			}
	l152: {
			goto l153;
		//assert(false);
		return;  			}
	l153: {
		if (__context->Perst.H_AlarmPh1) {
			goto l154;
		}
		if ((! __context->Perst.H_AlarmPh1)) {
			goto l182;
		}
		//assert(false);
		return;  			}
	l154: {
		if ((! __context->Perst.H_AlarmPh2)) {
			goto l155;
		}
		if ((! (! __context->Perst.H_AlarmPh2))) {
			goto l158;
		}
		//assert(false);
		return;  			}
	l155: {
			__context->Perst.H_TimeAlarm = (UNICOS_LiveCounter + ((int32_t) __context->Perst.PAlDt));
			goto l156;
		//assert(false);
		return;  			}
	l156: {
			__context->Perst.H_AlarmPh2 = true;
			goto l157;
		//assert(false);
		return;  			}
	l157: {
			goto l159;
		//assert(false);
		return;  			}
	l158: {
			goto l159;
		//assert(false);
		return;  			}
	l159: {
		if (__context->Perst.H_AlarmPh2) {
			goto l160;
		}
		if ((! __context->Perst.H_AlarmPh2)) {
			goto l176;
		}
		//assert(false);
		return;  			}
	l160: {
		if ((UNICOS_LiveCounter >= __context->Perst.H_TimeAlarm)) {
			goto l161;
		}
		if (((! (UNICOS_LiveCounter >= __context->Perst.H_TimeAlarm)) && __context->Perst.MAlBRSt)) {
			goto l169;
		}
		if (((! (UNICOS_LiveCounter >= __context->Perst.H_TimeAlarm)) && (! ((! (UNICOS_LiveCounter >= __context->Perst.H_TimeAlarm)) && __context->Perst.MAlBRSt)))) {
			goto l172;
		}
		//assert(false);
		return;  			}
	l161: {
		if (__context->Perst.MAlBRSt) {
			goto l162;
		}
		if ((! __context->Perst.MAlBRSt)) {
			goto l165;
		}
		//assert(false);
		return;  			}
	l162: {
			__context->Perst.HWSt = false;
			goto l163;
		//assert(false);
		return;  			}
	l163: {
			PosHW = true;
			goto l164;
		//assert(false);
		return;  			}
	l164: {
			goto l168;
		//assert(false);
		return;  			}
	l165: {
			__context->Perst.HWSt = true;
			goto l166;
		//assert(false);
		return;  			}
	l166: {
			PosHW = false;
			goto l167;
		//assert(false);
		return;  			}
	l167: {
			goto l168;
		//assert(false);
		return;  			}
	l168: {
			goto l175;
		//assert(false);
		return;  			}
	l169: {
			__context->Perst.HWSt = false;
			goto l170;
		//assert(false);
		return;  			}
	l170: {
			PosHW = false;
			goto l171;
		//assert(false);
		return;  			}
	l171: {
			goto l175;
		//assert(false);
		return;  			}
	l172: {
			__context->Perst.HWSt = false;
			goto l173;
		//assert(false);
		return;  			}
	l173: {
			PosHW = true;
			goto l174;
		//assert(false);
		return;  			}
	l174: {
			goto l175;
		//assert(false);
		return;  			}
	l175: {
			goto l177;
		//assert(false);
		return;  			}
	l176: {
			goto l177;
		//assert(false);
		return;  			}
	l177: {
		if (((__context->Perst.H_TimeAlarm - ((int32_t) __context->Perst.PAlDt)) > UNICOS_LiveCounter)) {
			goto l178;
		}
		if ((! ((__context->Perst.H_TimeAlarm - ((int32_t) __context->Perst.PAlDt)) > UNICOS_LiveCounter))) {
			goto l180;
		}
		//assert(false);
		return;  			}
	l178: {
			__context->Perst.H_TimeAlarm = (UNICOS_LiveCounter + ((int32_t) __context->Perst.PAlDt));
			goto l179;
		//assert(false);
		return;  			}
	l179: {
			goto l181;
		//assert(false);
		return;  			}
	l180: {
			goto l181;
		//assert(false);
		return;  			}
	l181: {
			goto l186;
		//assert(false);
		return;  			}
	l182: {
			__context->Perst.H_TimeAlarm = 0;
			goto l183;
		//assert(false);
		return;  			}
	l183: {
			__context->Perst.HWSt = false;
			goto l184;
		//assert(false);
		return;  			}
	l184: {
			PosHW = false;
			goto l185;
		//assert(false);
		return;  			}
	l185: {
			goto l186;
		//assert(false);
		return;  			}
	l186: {
		if ((__context->Perst.ELSt && (__context->Perst.I < __context->Perst.LSt))) {
			goto l187;
		}
		if ((! (__context->Perst.ELSt && (__context->Perst.I < __context->Perst.LSt)))) {
			goto l189;
		}
		//assert(false);
		return;  			}
	l187: {
			__context->Perst.L_AlarmPh1 = true;
			goto l188;
		//assert(false);
		return;  			}
	l188: {
			goto l192;
		//assert(false);
		return;  			}
	l189: {
			__context->Perst.L_AlarmPh1 = false;
			goto l190;
		//assert(false);
		return;  			}
	l190: {
			__context->Perst.L_AlarmPh2 = false;
			goto l191;
		//assert(false);
		return;  			}
	l191: {
			goto l192;
		//assert(false);
		return;  			}
	l192: {
		if (__context->Perst.L_AlarmPh1) {
			goto l193;
		}
		if ((! __context->Perst.L_AlarmPh1)) {
			goto l221;
		}
		//assert(false);
		return;  			}
	l193: {
		if ((! __context->Perst.L_AlarmPh2)) {
			goto l194;
		}
		if ((! (! __context->Perst.L_AlarmPh2))) {
			goto l197;
		}
		//assert(false);
		return;  			}
	l194: {
			__context->Perst.L_TimeAlarm = (UNICOS_LiveCounter + ((int32_t) __context->Perst.PAlDt));
			goto l195;
		//assert(false);
		return;  			}
	l195: {
			__context->Perst.L_AlarmPh2 = true;
			goto l196;
		//assert(false);
		return;  			}
	l196: {
			goto l198;
		//assert(false);
		return;  			}
	l197: {
			goto l198;
		//assert(false);
		return;  			}
	l198: {
		if (__context->Perst.L_AlarmPh2) {
			goto l199;
		}
		if ((! __context->Perst.L_AlarmPh2)) {
			goto l215;
		}
		//assert(false);
		return;  			}
	l199: {
		if ((UNICOS_LiveCounter >= __context->Perst.L_TimeAlarm)) {
			goto l200;
		}
		if (((! (UNICOS_LiveCounter >= __context->Perst.L_TimeAlarm)) && __context->Perst.MAlBRSt)) {
			goto l208;
		}
		if (((! (UNICOS_LiveCounter >= __context->Perst.L_TimeAlarm)) && (! ((! (UNICOS_LiveCounter >= __context->Perst.L_TimeAlarm)) && __context->Perst.MAlBRSt)))) {
			goto l211;
		}
		//assert(false);
		return;  			}
	l200: {
		if (__context->Perst.MAlBRSt) {
			goto l201;
		}
		if ((! __context->Perst.MAlBRSt)) {
			goto l204;
		}
		//assert(false);
		return;  			}
	l201: {
			__context->Perst.LWSt = false;
			goto l202;
		//assert(false);
		return;  			}
	l202: {
			PosLW = true;
			goto l203;
		//assert(false);
		return;  			}
	l203: {
			goto l207;
		//assert(false);
		return;  			}
	l204: {
			__context->Perst.LWSt = true;
			goto l205;
		//assert(false);
		return;  			}
	l205: {
			PosLW = false;
			goto l206;
		//assert(false);
		return;  			}
	l206: {
			goto l207;
		//assert(false);
		return;  			}
	l207: {
			goto l214;
		//assert(false);
		return;  			}
	l208: {
			__context->Perst.LWSt = false;
			goto l209;
		//assert(false);
		return;  			}
	l209: {
			PosLW = false;
			goto l210;
		//assert(false);
		return;  			}
	l210: {
			goto l214;
		//assert(false);
		return;  			}
	l211: {
			__context->Perst.LWSt = false;
			goto l212;
		//assert(false);
		return;  			}
	l212: {
			PosLW = true;
			goto l213;
		//assert(false);
		return;  			}
	l213: {
			goto l214;
		//assert(false);
		return;  			}
	l214: {
			goto l216;
		//assert(false);
		return;  			}
	l215: {
			goto l216;
		//assert(false);
		return;  			}
	l216: {
		if (((__context->Perst.L_TimeAlarm - ((int32_t) __context->Perst.PAlDt)) > UNICOS_LiveCounter)) {
			goto l217;
		}
		if ((! ((__context->Perst.L_TimeAlarm - ((int32_t) __context->Perst.PAlDt)) > UNICOS_LiveCounter))) {
			goto l219;
		}
		//assert(false);
		return;  			}
	l217: {
			__context->Perst.L_TimeAlarm = (UNICOS_LiveCounter + ((int32_t) __context->Perst.PAlDt));
			goto l218;
		//assert(false);
		return;  			}
	l218: {
			goto l220;
		//assert(false);
		return;  			}
	l219: {
			goto l220;
		//assert(false);
		return;  			}
	l220: {
			goto l225;
		//assert(false);
		return;  			}
	l221: {
			__context->Perst.L_TimeAlarm = 0;
			goto l222;
		//assert(false);
		return;  			}
	l222: {
			__context->Perst.LWSt = false;
			goto l223;
		//assert(false);
		return;  			}
	l223: {
			PosLW = false;
			goto l224;
		//assert(false);
		return;  			}
	l224: {
			goto l225;
		//assert(false);
		return;  			}
	l225: {
			__context->Perst.ISt = (__context->Perst.HHAlSt || __context->Perst.LLAlSt);
			goto l226;
		//assert(false);
		return;  			}
	l226: {
			__context->Perst.WSt = (__context->Perst.HWSt || __context->Perst.LWSt);
			goto l227;
		//assert(false);
		return;  			}
	l227: {
			// Assign inputs
			R_EDGE1_inlined_10.new = __context->Perst.ISt;
			R_EDGE1_inlined_10.old = __context->Perst.Alarm_Cond_old;
			R_EDGE(&R_EDGE1_inlined_10);
			// Assign outputs
			__context->Perst.Alarm_Cond_old = R_EDGE1_inlined_10.old;
			E_Alarm_Cond = R_EDGE1_inlined_10.RET_VAL;
			goto l228;
		//assert(false);
		return;  			}
	l228: {
		if ((((E_MAlAck || E_AuAlAck) || __context->Perst.MAlBRSt) || PAuAckAl)) {
			goto l229;
		}
		if (((! (((E_MAlAck || E_AuAlAck) || __context->Perst.MAlBRSt) || PAuAckAl)) && E_Alarm_Cond)) {
			goto l231;
		}
		if (((! (((E_MAlAck || E_AuAlAck) || __context->Perst.MAlBRSt) || PAuAckAl)) && (! ((! (((E_MAlAck || E_AuAlAck) || __context->Perst.MAlBRSt) || PAuAckAl)) && E_Alarm_Cond)))) {
			goto l233;
		}
		//assert(false);
		return;  			}
	l229: {
			__context->Perst.AlUnAck = false;
			goto l230;
		//assert(false);
		return;  			}
	l230: {
			goto l234;
		//assert(false);
		return;  			}
	l231: {
			__context->Perst.AlUnAck = true;
			goto l232;
		//assert(false);
		return;  			}
	l232: {
			goto l234;
		//assert(false);
		return;  			}
	l233: {
			goto l234;
		//assert(false);
		return;  			}
	l234: {
			__context->Perst.IOErrorW = __context->Perst.IOError;
			goto l235;
		//assert(false);
		return;  			}
	l235: {
			__context->Perst.IOSimuW = __context->Perst.IOSimu;
			goto l236;
		//assert(false);
		return;  			}
	l236: {
			ConfigW = ((((((((__context->Perst.ELLSt && __context->Perst.ELSt) && (__context->Perst.LSt < __context->Perst.LLSt)) || ((__context->Perst.EHSt && __context->Perst.ELSt) && (__context->Perst.HSt < __context->Perst.LSt))) || ((__context->Perst.EHHSt && __context->Perst.EHSt) && (__context->Perst.HHSt < __context->Perst.HSt))) || ((__context->Perst.ELLSt && __context->Perst.EHSt) && (__context->Perst.LLSt > __context->Perst.HSt))) || ((__context->Perst.ELLSt && __context->Perst.EHHSt) && (__context->Perst.LLSt > __context->Perst.HHSt))) || ((__context->Perst.ELSt && __context->Perst.EHHSt) && (__context->Perst.LSt > __context->Perst.HHSt))) || ((((! __context->Perst.EHHSt) && (! __context->Perst.EHSt)) && (! __context->Perst.ELSt)) && (! __context->Perst.ELLSt)));
			goto l237;
		//assert(false);
		return;  			}
	l237: {
			__context->Perst.PosSt = __context->Perst.I;
			goto l238;
		//assert(false);
		return;  			}
	l238: {
		if ((__context->Perst.ArmRcpSt && E_ActRcp)) {
			goto l239;
		}
		if ((! (__context->Perst.ArmRcpSt && E_ActRcp))) {
			goto l241;
		}
		//assert(false);
		return;  			}
	l239: {
			__context->Perst.ArmRcpSt = false;
			goto l240;
		//assert(false);
		return;  			}
	l240: {
			goto l242;
		//assert(false);
		return;  			}
	l241: {
			goto l242;
		//assert(false);
		return;  			}
	l242: {
			PulseWidth = (1500.0 / ((float) ((int32_t) T_CYCLE)));
			goto l243;
		//assert(false);
		return;  			}
	l243: {
		if ((__context->Perst.ISt || (__context->Perst.Iinc > 0))) {
			goto l244;
		}
		if ((! (__context->Perst.ISt || (__context->Perst.Iinc > 0)))) {
			goto l247;
		}
		//assert(false);
		return;  			}
	l244: {
			__context->Perst.Iinc = (__context->Perst.Iinc + 1);
			goto l245;
		//assert(false);
		return;  			}
	l245: {
			__context->Perst.WISt = true;
			goto l246;
		//assert(false);
		return;  			}
	l246: {
			goto l248;
		//assert(false);
		return;  			}
	l247: {
			goto l248;
		//assert(false);
		return;  			}
	l248: {
		if (((((float) __context->Perst.Iinc) > PulseWidth) || ((! __context->Perst.ISt) && (__context->Perst.Iinc == 0)))) {
			goto l249;
		}
		if ((! ((((float) __context->Perst.Iinc) > PulseWidth) || ((! __context->Perst.ISt) && (__context->Perst.Iinc == 0))))) {
			goto l252;
		}
		//assert(false);
		return;  			}
	l249: {
			__context->Perst.Iinc = 0;
			goto l250;
		//assert(false);
		return;  			}
	l250: {
			__context->Perst.WISt = __context->Perst.ISt;
			goto l251;
		//assert(false);
		return;  			}
	l251: {
			goto l253;
		//assert(false);
		return;  			}
	l252: {
			goto l253;
		//assert(false);
		return;  			}
	l253: {
		if ((__context->Perst.WSt || (__context->Perst.Winc > 0))) {
			goto l254;
		}
		if ((! (__context->Perst.WSt || (__context->Perst.Winc > 0)))) {
			goto l257;
		}
		//assert(false);
		return;  			}
	l254: {
			__context->Perst.Winc = (__context->Perst.Winc + 1);
			goto l255;
		//assert(false);
		return;  			}
	l255: {
			__context->Perst.WWSt = true;
			goto l256;
		//assert(false);
		return;  			}
	l256: {
			goto l258;
		//assert(false);
		return;  			}
	l257: {
			goto l258;
		//assert(false);
		return;  			}
	l258: {
		if (((((float) __context->Perst.Winc) > PulseWidth) || ((! __context->Perst.WSt) && (__context->Perst.Winc == 0)))) {
			goto l259;
		}
		if ((! ((((float) __context->Perst.Winc) > PulseWidth) || ((! __context->Perst.WSt) && (__context->Perst.Winc == 0))))) {
			goto l262;
		}
		//assert(false);
		return;  			}
	l259: {
			__context->Perst.Winc = 0;
			goto l260;
		//assert(false);
		return;  			}
	l260: {
			__context->Perst.WWSt = __context->Perst.WSt;
			goto l261;
		//assert(false);
		return;  			}
	l261: {
			goto l263;
		//assert(false);
		return;  			}
	l262: {
			goto l263;
		//assert(false);
		return;  			}
	l263: {
		if ((__context->Perst.HHAlSt || (__context->Perst.HHinc > 0))) {
			goto l264;
		}
		if ((! (__context->Perst.HHAlSt || (__context->Perst.HHinc > 0)))) {
			goto l267;
		}
		//assert(false);
		return;  			}
	l264: {
			__context->Perst.HHinc = (__context->Perst.HHinc + 1);
			goto l265;
		//assert(false);
		return;  			}
	l265: {
			__context->Perst.WHHAlSt = true;
			goto l266;
		//assert(false);
		return;  			}
	l266: {
			goto l268;
		//assert(false);
		return;  			}
	l267: {
			goto l268;
		//assert(false);
		return;  			}
	l268: {
		if (((((float) __context->Perst.HHinc) > PulseWidth) || ((! __context->Perst.HHAlSt) && (__context->Perst.HHinc == 0)))) {
			goto l269;
		}
		if ((! ((((float) __context->Perst.HHinc) > PulseWidth) || ((! __context->Perst.HHAlSt) && (__context->Perst.HHinc == 0))))) {
			goto l272;
		}
		//assert(false);
		return;  			}
	l269: {
			__context->Perst.HHinc = 0;
			goto l270;
		//assert(false);
		return;  			}
	l270: {
			__context->Perst.WHHAlSt = __context->Perst.HHAlSt;
			goto l271;
		//assert(false);
		return;  			}
	l271: {
			goto l273;
		//assert(false);
		return;  			}
	l272: {
			goto l273;
		//assert(false);
		return;  			}
	l273: {
		if ((__context->Perst.HWSt || (__context->Perst.Hinc > 0))) {
			goto l274;
		}
		if ((! (__context->Perst.HWSt || (__context->Perst.Hinc > 0)))) {
			goto l277;
		}
		//assert(false);
		return;  			}
	l274: {
			__context->Perst.Hinc = (__context->Perst.Hinc + 1);
			goto l275;
		//assert(false);
		return;  			}
	l275: {
			__context->Perst.WHWSt = true;
			goto l276;
		//assert(false);
		return;  			}
	l276: {
			goto l278;
		//assert(false);
		return;  			}
	l277: {
			goto l278;
		//assert(false);
		return;  			}
	l278: {
		if (((((float) __context->Perst.Hinc) > PulseWidth) || ((! __context->Perst.HWSt) && (__context->Perst.Hinc == 0)))) {
			goto l279;
		}
		if ((! ((((float) __context->Perst.Hinc) > PulseWidth) || ((! __context->Perst.HWSt) && (__context->Perst.Hinc == 0))))) {
			goto l282;
		}
		//assert(false);
		return;  			}
	l279: {
			__context->Perst.Hinc = 0;
			goto l280;
		//assert(false);
		return;  			}
	l280: {
			__context->Perst.WHWSt = __context->Perst.HWSt;
			goto l281;
		//assert(false);
		return;  			}
	l281: {
			goto l283;
		//assert(false);
		return;  			}
	l282: {
			goto l283;
		//assert(false);
		return;  			}
	l283: {
		if ((__context->Perst.LWSt || (__context->Perst.Linc > 0))) {
			goto l284;
		}
		if ((! (__context->Perst.LWSt || (__context->Perst.Linc > 0)))) {
			goto l287;
		}
		//assert(false);
		return;  			}
	l284: {
			__context->Perst.Linc = (__context->Perst.Linc + 1);
			goto l285;
		//assert(false);
		return;  			}
	l285: {
			__context->Perst.WLWSt = true;
			goto l286;
		//assert(false);
		return;  			}
	l286: {
			goto l288;
		//assert(false);
		return;  			}
	l287: {
			goto l288;
		//assert(false);
		return;  			}
	l288: {
		if (((((float) __context->Perst.Linc) > PulseWidth) || ((! __context->Perst.LWSt) && (__context->Perst.Linc == 0)))) {
			goto l289;
		}
		if ((! ((((float) __context->Perst.Linc) > PulseWidth) || ((! __context->Perst.LWSt) && (__context->Perst.Linc == 0))))) {
			goto l292;
		}
		//assert(false);
		return;  			}
	l289: {
			__context->Perst.Linc = 0;
			goto l290;
		//assert(false);
		return;  			}
	l290: {
			__context->Perst.WLWSt = __context->Perst.LWSt;
			goto l291;
		//assert(false);
		return;  			}
	l291: {
			goto l293;
		//assert(false);
		return;  			}
	l292: {
			goto l293;
		//assert(false);
		return;  			}
	l293: {
		if ((__context->Perst.LLAlSt || (__context->Perst.LLinc > 0))) {
			goto l294;
		}
		if ((! (__context->Perst.LLAlSt || (__context->Perst.LLinc > 0)))) {
			goto l297;
		}
		//assert(false);
		return;  			}
	l294: {
			__context->Perst.LLinc = (__context->Perst.LLinc + 1);
			goto l295;
		//assert(false);
		return;  			}
	l295: {
			__context->Perst.WLLAlSt = true;
			goto l296;
		//assert(false);
		return;  			}
	l296: {
			goto l298;
		//assert(false);
		return;  			}
	l297: {
			goto l298;
		//assert(false);
		return;  			}
	l298: {
		if (((((float) __context->Perst.LLinc) > PulseWidth) || ((! __context->Perst.LLAlSt) && (__context->Perst.LLinc == 0)))) {
			goto l299;
		}
		if ((! ((((float) __context->Perst.LLinc) > PulseWidth) || ((! __context->Perst.LLAlSt) && (__context->Perst.LLinc == 0))))) {
			goto l302;
		}
		//assert(false);
		return;  			}
	l299: {
			__context->Perst.LLinc = 0;
			goto l300;
		//assert(false);
		return;  			}
	l300: {
			__context->Perst.WLLAlSt = __context->Perst.LLAlSt;
			goto l301;
		//assert(false);
		return;  			}
	l301: {
			goto l303;
		//assert(false);
		return;  			}
	l302: {
			goto l303;
		//assert(false);
		return;  			}
	l303: {
			StsReg01b[8] = __context->Perst.WISt;
			goto x5;
		//assert(false);
		return;  			}
	l304: {
			StsReg01b[9] = __context->Perst.WWSt;
			goto x6;
		//assert(false);
		return;  			}
	l305: {
			StsReg01b[10] = false;
			goto x7;
		//assert(false);
		return;  			}
	l306: {
			StsReg01b[11] = __context->Perst.ArmRcpSt;
			goto x8;
		//assert(false);
		return;  			}
	l307: {
			StsReg01b[12] = false;
			goto x9;
		//assert(false);
		return;  			}
	l308: {
			StsReg01b[13] = ConfigW;
			goto x10;
		//assert(false);
		return;  			}
	l309: {
			StsReg01b[14] = __context->Perst.IOErrorW;
			goto x11;
		//assert(false);
		return;  			}
	l310: {
			StsReg01b[15] = __context->Perst.IOSimuW;
			goto x12;
		//assert(false);
		return;  			}
	l311: {
			StsReg01b[0] = PosHHW;
			goto x13;
		//assert(false);
		return;  			}
	l312: {
			StsReg01b[1] = PosHW;
			goto x14;
		//assert(false);
		return;  			}
	l313: {
			StsReg01b[2] = PosLW;
			goto x15;
		//assert(false);
		return;  			}
	l314: {
			StsReg01b[3] = PosLLW;
			goto x16;
		//assert(false);
		return;  			}
	l315: {
			StsReg01b[4] = __context->Perst.AlUnAck;
			goto x17;
		//assert(false);
		return;  			}
	l316: {
			StsReg01b[5] = false;
			goto x18;
		//assert(false);
		return;  			}
	l317: {
			StsReg01b[6] = __context->Perst.MAlBRSt;
			goto x19;
		//assert(false);
		return;  			}
	l318: {
			StsReg01b[7] = __context->Perst.AuIhMB;
			goto x20;
		//assert(false);
		return;  			}
	l319: {
			StsReg02b[8] = __context->Perst.EHHSt;
			goto x21;
		//assert(false);
		return;  			}
	l320: {
			StsReg02b[9] = __context->Perst.EHSt;
			goto x22;
		//assert(false);
		return;  			}
	l321: {
			StsReg02b[10] = __context->Perst.ELSt;
			goto x23;
		//assert(false);
		return;  			}
	l322: {
			StsReg02b[11] = __context->Perst.ELLSt;
			goto x24;
		//assert(false);
		return;  			}
	l323: {
			StsReg02b[12] = __context->Perst.WHHAlSt;
			goto x25;
		//assert(false);
		return;  			}
	l324: {
			StsReg02b[13] = __context->Perst.WHWSt;
			goto x26;
		//assert(false);
		return;  			}
	l325: {
			StsReg02b[14] = __context->Perst.WLWSt;
			goto x27;
		//assert(false);
		return;  			}
	l326: {
			StsReg02b[15] = __context->Perst.WLLAlSt;
			goto x28;
		//assert(false);
		return;  			}
	l327: {
			StsReg02b[0] = IhMHHSt;
			goto x29;
		//assert(false);
		return;  			}
	l328: {
			StsReg02b[1] = IhMHSt;
			goto x30;
		//assert(false);
		return;  			}
	l329: {
			StsReg02b[2] = IhMLSt;
			goto x31;
		//assert(false);
		return;  			}
	l330: {
			StsReg02b[3] = IhMLLSt;
			goto x32;
		//assert(false);
		return;  			}
	l331: {
			StsReg02b[4] = false;
			goto x33;
		//assert(false);
		return;  			}
	l332: {
			StsReg02b[5] = false;
			goto x34;
		//assert(false);
		return;  			}
	l333: {
			StsReg02b[6] = false;
			goto x35;
		//assert(false);
		return;  			}
	l334: {
			StsReg02b[7] = false;
			goto x36;
		//assert(false);
		return;  			}
	l335: {
			// Assign inputs
			DETECT_EDGE1.new = __context->Perst.AlUnAck;
			DETECT_EDGE1.old = __context->Perst.AlUnAck_old;
			DETECT_EDGE(&DETECT_EDGE1);
			// Assign outputs
			__context->Perst.AlUnAck_old = DETECT_EDGE1.old;
			RE_AlUnAck = DETECT_EDGE1.re;
			FE_AlUnAck = DETECT_EDGE1.fe;
			goto l336;
		//assert(false);
		return;  			}
	l336: {
			__context->StsReg01 = TempStsReg01;
			goto l337;
		//assert(false);
		return;  			}
	l337: {
			__context->StsReg02 = TempStsReg02;
			goto l338;
		//assert(false);
		return;  			}
	l338: {
			__context->Perst.PAA = TempPAA;
			goto l339;
		//assert(false);
		return;  			}
	l339: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	x: {
			ConfigW = false;
			E_ActRcp = false;
			E_Alarm_Cond = false;
			E_ArmRcp = false;
			E_AuAlAck = false;
			E_MAlAck = false;
			E_MAlBSetRst = false;
			E_MNewHHR = false;
			E_MNewHR = false;
			E_MNewLLR = false;
			E_MNewLR = false;
			FE_AlUnAck = false;
			IhMHHSt = false;
			IhMHSt = false;
			IhMLLSt = false;
			IhMLSt = false;
			PAuAckAl = false;
			PosHHW = false;
			PosHW = false;
			PosLLW = false;
			PosLW = false;
			PulseWidth = 0.0;
			RE_AlUnAck = false;
			TempPAA.ParReg = 0;
			TempStsReg01 = 0;
			TempStsReg02 = 0;
			goto x1;
		//assert(false);
		return;  			}
	varview_refresh: {
			StsReg01b[0] = ((TempStsReg01 & 256) != 0);
			goto varview_refresh33;
		//assert(false);
		return;  			}
	varview_refresh1: {
			__context->Manreg01b[1] = ((__context->Manreg01 & 512) != 0);
			goto varview_refresh3;
		//assert(false);
		return;  			}
	varview_refresh3: {
			__context->Manreg01b[2] = ((__context->Manreg01 & 1024) != 0);
			goto varview_refresh5;
		//assert(false);
		return;  			}
	varview_refresh5: {
			__context->Manreg01b[3] = ((__context->Manreg01 & 2048) != 0);
			goto varview_refresh7;
		//assert(false);
		return;  			}
	varview_refresh7: {
			__context->Manreg01b[4] = ((__context->Manreg01 & 4096) != 0);
			goto varview_refresh9;
		//assert(false);
		return;  			}
	varview_refresh9: {
			__context->Manreg01b[5] = ((__context->Manreg01 & 8192) != 0);
			goto varview_refresh11;
		//assert(false);
		return;  			}
	varview_refresh11: {
			__context->Manreg01b[6] = ((__context->Manreg01 & 16384) != 0);
			goto varview_refresh13;
		//assert(false);
		return;  			}
	varview_refresh13: {
			__context->Manreg01b[7] = ((__context->Manreg01 & 32768) != 0);
			goto varview_refresh15;
		//assert(false);
		return;  			}
	varview_refresh15: {
			__context->Manreg01b[8] = ((__context->Manreg01 & 1) != 0);
			goto varview_refresh17;
		//assert(false);
		return;  			}
	varview_refresh17: {
			__context->Manreg01b[9] = ((__context->Manreg01 & 2) != 0);
			goto varview_refresh19;
		//assert(false);
		return;  			}
	varview_refresh19: {
			__context->Manreg01b[10] = ((__context->Manreg01 & 4) != 0);
			goto varview_refresh21;
		//assert(false);
		return;  			}
	varview_refresh21: {
			__context->Manreg01b[11] = ((__context->Manreg01 & 8) != 0);
			goto varview_refresh23;
		//assert(false);
		return;  			}
	varview_refresh23: {
			__context->Manreg01b[12] = ((__context->Manreg01 & 16) != 0);
			goto varview_refresh25;
		//assert(false);
		return;  			}
	varview_refresh25: {
			__context->Manreg01b[13] = ((__context->Manreg01 & 32) != 0);
			goto varview_refresh27;
		//assert(false);
		return;  			}
	varview_refresh27: {
			__context->Manreg01b[14] = ((__context->Manreg01 & 64) != 0);
			goto varview_refresh29;
		//assert(false);
		return;  			}
	varview_refresh29: {
			__context->Manreg01b[15] = ((__context->Manreg01 & 128) != 0);
			goto varview_refresh;
		//assert(false);
		return;  			}
	varview_refresh32: {
			StsReg02b[0] = ((TempStsReg02 & 256) != 0);
			goto varview_refresh65;
		//assert(false);
		return;  			}
	varview_refresh33: {
			StsReg01b[1] = ((TempStsReg01 & 512) != 0);
			goto varview_refresh35;
		//assert(false);
		return;  			}
	varview_refresh35: {
			StsReg01b[2] = ((TempStsReg01 & 1024) != 0);
			goto varview_refresh37;
		//assert(false);
		return;  			}
	varview_refresh37: {
			StsReg01b[3] = ((TempStsReg01 & 2048) != 0);
			goto varview_refresh39;
		//assert(false);
		return;  			}
	varview_refresh39: {
			StsReg01b[4] = ((TempStsReg01 & 4096) != 0);
			goto varview_refresh41;
		//assert(false);
		return;  			}
	varview_refresh41: {
			StsReg01b[5] = ((TempStsReg01 & 8192) != 0);
			goto varview_refresh43;
		//assert(false);
		return;  			}
	varview_refresh43: {
			StsReg01b[6] = ((TempStsReg01 & 16384) != 0);
			goto varview_refresh45;
		//assert(false);
		return;  			}
	varview_refresh45: {
			StsReg01b[7] = ((TempStsReg01 & 32768) != 0);
			goto varview_refresh47;
		//assert(false);
		return;  			}
	varview_refresh47: {
			StsReg01b[8] = ((TempStsReg01 & 1) != 0);
			goto varview_refresh49;
		//assert(false);
		return;  			}
	varview_refresh49: {
			StsReg01b[9] = ((TempStsReg01 & 2) != 0);
			goto varview_refresh51;
		//assert(false);
		return;  			}
	varview_refresh51: {
			StsReg01b[10] = ((TempStsReg01 & 4) != 0);
			goto varview_refresh53;
		//assert(false);
		return;  			}
	varview_refresh53: {
			StsReg01b[11] = ((TempStsReg01 & 8) != 0);
			goto varview_refresh55;
		//assert(false);
		return;  			}
	varview_refresh55: {
			StsReg01b[12] = ((TempStsReg01 & 16) != 0);
			goto varview_refresh57;
		//assert(false);
		return;  			}
	varview_refresh57: {
			StsReg01b[13] = ((TempStsReg01 & 32) != 0);
			goto varview_refresh59;
		//assert(false);
		return;  			}
	varview_refresh59: {
			StsReg01b[14] = ((TempStsReg01 & 64) != 0);
			goto varview_refresh61;
		//assert(false);
		return;  			}
	varview_refresh61: {
			StsReg01b[15] = ((TempStsReg01 & 128) != 0);
			goto varview_refresh32;
		//assert(false);
		return;  			}
	varview_refresh64: {
			PAAb.ParRegb[0] = ((TempPAA.ParReg & 256) != 0);
			goto varview_refresh96;
		//assert(false);
		return;  			}
	varview_refresh65: {
			StsReg02b[1] = ((TempStsReg02 & 512) != 0);
			goto varview_refresh67;
		//assert(false);
		return;  			}
	varview_refresh67: {
			StsReg02b[2] = ((TempStsReg02 & 1024) != 0);
			goto varview_refresh69;
		//assert(false);
		return;  			}
	varview_refresh69: {
			StsReg02b[3] = ((TempStsReg02 & 2048) != 0);
			goto varview_refresh71;
		//assert(false);
		return;  			}
	varview_refresh71: {
			StsReg02b[4] = ((TempStsReg02 & 4096) != 0);
			goto varview_refresh73;
		//assert(false);
		return;  			}
	varview_refresh73: {
			StsReg02b[5] = ((TempStsReg02 & 8192) != 0);
			goto varview_refresh75;
		//assert(false);
		return;  			}
	varview_refresh75: {
			StsReg02b[6] = ((TempStsReg02 & 16384) != 0);
			goto varview_refresh77;
		//assert(false);
		return;  			}
	varview_refresh77: {
			StsReg02b[7] = ((TempStsReg02 & 32768) != 0);
			goto varview_refresh79;
		//assert(false);
		return;  			}
	varview_refresh79: {
			StsReg02b[8] = ((TempStsReg02 & 1) != 0);
			goto varview_refresh81;
		//assert(false);
		return;  			}
	varview_refresh81: {
			StsReg02b[9] = ((TempStsReg02 & 2) != 0);
			goto varview_refresh83;
		//assert(false);
		return;  			}
	varview_refresh83: {
			StsReg02b[10] = ((TempStsReg02 & 4) != 0);
			goto varview_refresh85;
		//assert(false);
		return;  			}
	varview_refresh85: {
			StsReg02b[11] = ((TempStsReg02 & 8) != 0);
			goto varview_refresh87;
		//assert(false);
		return;  			}
	varview_refresh87: {
			StsReg02b[12] = ((TempStsReg02 & 16) != 0);
			goto varview_refresh89;
		//assert(false);
		return;  			}
	varview_refresh89: {
			StsReg02b[13] = ((TempStsReg02 & 32) != 0);
			goto varview_refresh91;
		//assert(false);
		return;  			}
	varview_refresh91: {
			StsReg02b[14] = ((TempStsReg02 & 64) != 0);
			goto varview_refresh93;
		//assert(false);
		return;  			}
	varview_refresh93: {
			StsReg02b[15] = ((TempStsReg02 & 128) != 0);
			goto varview_refresh64;
		//assert(false);
		return;  			}
	varview_refresh96: {
			PAAb.ParRegb[1] = ((TempPAA.ParReg & 512) != 0);
			goto varview_refresh98;
		//assert(false);
		return;  			}
	varview_refresh98: {
			PAAb.ParRegb[2] = ((TempPAA.ParReg & 1024) != 0);
			goto varview_refresh100;
		//assert(false);
		return;  			}
	varview_refresh100: {
			PAAb.ParRegb[3] = ((TempPAA.ParReg & 2048) != 0);
			goto varview_refresh102;
		//assert(false);
		return;  			}
	varview_refresh102: {
			PAAb.ParRegb[4] = ((TempPAA.ParReg & 4096) != 0);
			goto varview_refresh104;
		//assert(false);
		return;  			}
	varview_refresh104: {
			PAAb.ParRegb[5] = ((TempPAA.ParReg & 8192) != 0);
			goto varview_refresh106;
		//assert(false);
		return;  			}
	varview_refresh106: {
			PAAb.ParRegb[6] = ((TempPAA.ParReg & 16384) != 0);
			goto varview_refresh108;
		//assert(false);
		return;  			}
	varview_refresh108: {
			PAAb.ParRegb[7] = ((TempPAA.ParReg & 32768) != 0);
			goto varview_refresh110;
		//assert(false);
		return;  			}
	varview_refresh110: {
			PAAb.ParRegb[8] = ((TempPAA.ParReg & 1) != 0);
			goto varview_refresh112;
		//assert(false);
		return;  			}
	varview_refresh112: {
			PAAb.ParRegb[9] = ((TempPAA.ParReg & 2) != 0);
			goto varview_refresh114;
		//assert(false);
		return;  			}
	varview_refresh114: {
			PAAb.ParRegb[10] = ((TempPAA.ParReg & 4) != 0);
			goto varview_refresh116;
		//assert(false);
		return;  			}
	varview_refresh116: {
			PAAb.ParRegb[11] = ((TempPAA.ParReg & 8) != 0);
			goto varview_refresh118;
		//assert(false);
		return;  			}
	varview_refresh118: {
			PAAb.ParRegb[12] = ((TempPAA.ParReg & 16) != 0);
			goto varview_refresh120;
		//assert(false);
		return;  			}
	varview_refresh120: {
			PAAb.ParRegb[13] = ((TempPAA.ParReg & 32) != 0);
			goto varview_refresh122;
		//assert(false);
		return;  			}
	varview_refresh122: {
			PAAb.ParRegb[14] = ((TempPAA.ParReg & 64) != 0);
			goto varview_refresh124;
		//assert(false);
		return;  			}
	varview_refresh124: {
			PAAb.ParRegb[15] = ((TempPAA.ParReg & 128) != 0);
			goto x;
		//assert(false);
		return;  			}
	x1: {
			StsReg01b[0] = ((TempStsReg01 & 256) != 0);
			goto varview_refresh128;
		//assert(false);
		return;  			}
	varview_refresh127: {
			StsReg02b[0] = ((TempStsReg02 & 256) != 0);
			goto varview_refresh160;
		//assert(false);
		return;  			}
	varview_refresh128: {
			StsReg01b[1] = ((TempStsReg01 & 512) != 0);
			goto varview_refresh130;
		//assert(false);
		return;  			}
	varview_refresh130: {
			StsReg01b[2] = ((TempStsReg01 & 1024) != 0);
			goto varview_refresh132;
		//assert(false);
		return;  			}
	varview_refresh132: {
			StsReg01b[3] = ((TempStsReg01 & 2048) != 0);
			goto varview_refresh134;
		//assert(false);
		return;  			}
	varview_refresh134: {
			StsReg01b[4] = ((TempStsReg01 & 4096) != 0);
			goto varview_refresh136;
		//assert(false);
		return;  			}
	varview_refresh136: {
			StsReg01b[5] = ((TempStsReg01 & 8192) != 0);
			goto varview_refresh138;
		//assert(false);
		return;  			}
	varview_refresh138: {
			StsReg01b[6] = ((TempStsReg01 & 16384) != 0);
			goto varview_refresh140;
		//assert(false);
		return;  			}
	varview_refresh140: {
			StsReg01b[7] = ((TempStsReg01 & 32768) != 0);
			goto varview_refresh142;
		//assert(false);
		return;  			}
	varview_refresh142: {
			StsReg01b[8] = ((TempStsReg01 & 1) != 0);
			goto varview_refresh144;
		//assert(false);
		return;  			}
	varview_refresh144: {
			StsReg01b[9] = ((TempStsReg01 & 2) != 0);
			goto varview_refresh146;
		//assert(false);
		return;  			}
	varview_refresh146: {
			StsReg01b[10] = ((TempStsReg01 & 4) != 0);
			goto varview_refresh148;
		//assert(false);
		return;  			}
	varview_refresh148: {
			StsReg01b[11] = ((TempStsReg01 & 8) != 0);
			goto varview_refresh150;
		//assert(false);
		return;  			}
	varview_refresh150: {
			StsReg01b[12] = ((TempStsReg01 & 16) != 0);
			goto varview_refresh152;
		//assert(false);
		return;  			}
	varview_refresh152: {
			StsReg01b[13] = ((TempStsReg01 & 32) != 0);
			goto varview_refresh154;
		//assert(false);
		return;  			}
	varview_refresh154: {
			StsReg01b[14] = ((TempStsReg01 & 64) != 0);
			goto varview_refresh156;
		//assert(false);
		return;  			}
	varview_refresh156: {
			StsReg01b[15] = ((TempStsReg01 & 128) != 0);
			goto varview_refresh127;
		//assert(false);
		return;  			}
	varview_refresh159: {
			PAAb.ParRegb[0] = ((TempPAA.ParReg & 256) != 0);
			goto varview_refresh191;
		//assert(false);
		return;  			}
	varview_refresh160: {
			StsReg02b[1] = ((TempStsReg02 & 512) != 0);
			goto varview_refresh162;
		//assert(false);
		return;  			}
	varview_refresh162: {
			StsReg02b[2] = ((TempStsReg02 & 1024) != 0);
			goto varview_refresh164;
		//assert(false);
		return;  			}
	varview_refresh164: {
			StsReg02b[3] = ((TempStsReg02 & 2048) != 0);
			goto varview_refresh166;
		//assert(false);
		return;  			}
	varview_refresh166: {
			StsReg02b[4] = ((TempStsReg02 & 4096) != 0);
			goto varview_refresh168;
		//assert(false);
		return;  			}
	varview_refresh168: {
			StsReg02b[5] = ((TempStsReg02 & 8192) != 0);
			goto varview_refresh170;
		//assert(false);
		return;  			}
	varview_refresh170: {
			StsReg02b[6] = ((TempStsReg02 & 16384) != 0);
			goto varview_refresh172;
		//assert(false);
		return;  			}
	varview_refresh172: {
			StsReg02b[7] = ((TempStsReg02 & 32768) != 0);
			goto varview_refresh174;
		//assert(false);
		return;  			}
	varview_refresh174: {
			StsReg02b[8] = ((TempStsReg02 & 1) != 0);
			goto varview_refresh176;
		//assert(false);
		return;  			}
	varview_refresh176: {
			StsReg02b[9] = ((TempStsReg02 & 2) != 0);
			goto varview_refresh178;
		//assert(false);
		return;  			}
	varview_refresh178: {
			StsReg02b[10] = ((TempStsReg02 & 4) != 0);
			goto varview_refresh180;
		//assert(false);
		return;  			}
	varview_refresh180: {
			StsReg02b[11] = ((TempStsReg02 & 8) != 0);
			goto varview_refresh182;
		//assert(false);
		return;  			}
	varview_refresh182: {
			StsReg02b[12] = ((TempStsReg02 & 16) != 0);
			goto varview_refresh184;
		//assert(false);
		return;  			}
	varview_refresh184: {
			StsReg02b[13] = ((TempStsReg02 & 32) != 0);
			goto varview_refresh186;
		//assert(false);
		return;  			}
	varview_refresh186: {
			StsReg02b[14] = ((TempStsReg02 & 64) != 0);
			goto varview_refresh188;
		//assert(false);
		return;  			}
	varview_refresh188: {
			StsReg02b[15] = ((TempStsReg02 & 128) != 0);
			goto varview_refresh159;
		//assert(false);
		return;  			}
	varview_refresh191: {
			PAAb.ParRegb[1] = ((TempPAA.ParReg & 512) != 0);
			goto varview_refresh193;
		//assert(false);
		return;  			}
	varview_refresh193: {
			PAAb.ParRegb[2] = ((TempPAA.ParReg & 1024) != 0);
			goto varview_refresh195;
		//assert(false);
		return;  			}
	varview_refresh195: {
			PAAb.ParRegb[3] = ((TempPAA.ParReg & 2048) != 0);
			goto varview_refresh197;
		//assert(false);
		return;  			}
	varview_refresh197: {
			PAAb.ParRegb[4] = ((TempPAA.ParReg & 4096) != 0);
			goto varview_refresh199;
		//assert(false);
		return;  			}
	varview_refresh199: {
			PAAb.ParRegb[5] = ((TempPAA.ParReg & 8192) != 0);
			goto varview_refresh201;
		//assert(false);
		return;  			}
	varview_refresh201: {
			PAAb.ParRegb[6] = ((TempPAA.ParReg & 16384) != 0);
			goto varview_refresh203;
		//assert(false);
		return;  			}
	varview_refresh203: {
			PAAb.ParRegb[7] = ((TempPAA.ParReg & 32768) != 0);
			goto varview_refresh205;
		//assert(false);
		return;  			}
	varview_refresh205: {
			PAAb.ParRegb[8] = ((TempPAA.ParReg & 1) != 0);
			goto varview_refresh207;
		//assert(false);
		return;  			}
	varview_refresh207: {
			PAAb.ParRegb[9] = ((TempPAA.ParReg & 2) != 0);
			goto varview_refresh209;
		//assert(false);
		return;  			}
	varview_refresh209: {
			PAAb.ParRegb[10] = ((TempPAA.ParReg & 4) != 0);
			goto varview_refresh211;
		//assert(false);
		return;  			}
	varview_refresh211: {
			PAAb.ParRegb[11] = ((TempPAA.ParReg & 8) != 0);
			goto varview_refresh213;
		//assert(false);
		return;  			}
	varview_refresh213: {
			PAAb.ParRegb[12] = ((TempPAA.ParReg & 16) != 0);
			goto varview_refresh215;
		//assert(false);
		return;  			}
	varview_refresh215: {
			PAAb.ParRegb[13] = ((TempPAA.ParReg & 32) != 0);
			goto varview_refresh217;
		//assert(false);
		return;  			}
	varview_refresh217: {
			PAAb.ParRegb[14] = ((TempPAA.ParReg & 64) != 0);
			goto varview_refresh219;
		//assert(false);
		return;  			}
	varview_refresh219: {
			PAAb.ParRegb[15] = ((TempPAA.ParReg & 128) != 0);
			goto l1;
		//assert(false);
		return;  			}
	x2: {
			StsReg01b[0] = ((TempStsReg01 & 256) != 0);
			goto varview_refresh222;
		//assert(false);
		return;  			}
	varview_refresh222: {
			StsReg01b[1] = ((TempStsReg01 & 512) != 0);
			goto varview_refresh224;
		//assert(false);
		return;  			}
	varview_refresh224: {
			StsReg01b[2] = ((TempStsReg01 & 1024) != 0);
			goto varview_refresh226;
		//assert(false);
		return;  			}
	varview_refresh226: {
			StsReg01b[3] = ((TempStsReg01 & 2048) != 0);
			goto varview_refresh228;
		//assert(false);
		return;  			}
	varview_refresh228: {
			StsReg01b[4] = ((TempStsReg01 & 4096) != 0);
			goto varview_refresh230;
		//assert(false);
		return;  			}
	varview_refresh230: {
			StsReg01b[5] = ((TempStsReg01 & 8192) != 0);
			goto varview_refresh232;
		//assert(false);
		return;  			}
	varview_refresh232: {
			StsReg01b[6] = ((TempStsReg01 & 16384) != 0);
			goto varview_refresh234;
		//assert(false);
		return;  			}
	varview_refresh234: {
			StsReg01b[7] = ((TempStsReg01 & 32768) != 0);
			goto varview_refresh236;
		//assert(false);
		return;  			}
	varview_refresh236: {
			StsReg01b[8] = ((TempStsReg01 & 1) != 0);
			goto varview_refresh238;
		//assert(false);
		return;  			}
	varview_refresh238: {
			StsReg01b[9] = ((TempStsReg01 & 2) != 0);
			goto varview_refresh240;
		//assert(false);
		return;  			}
	varview_refresh240: {
			StsReg01b[10] = ((TempStsReg01 & 4) != 0);
			goto varview_refresh242;
		//assert(false);
		return;  			}
	varview_refresh242: {
			StsReg01b[11] = ((TempStsReg01 & 8) != 0);
			goto varview_refresh244;
		//assert(false);
		return;  			}
	varview_refresh244: {
			StsReg01b[12] = ((TempStsReg01 & 16) != 0);
			goto varview_refresh246;
		//assert(false);
		return;  			}
	varview_refresh246: {
			StsReg01b[13] = ((TempStsReg01 & 32) != 0);
			goto varview_refresh248;
		//assert(false);
		return;  			}
	varview_refresh248: {
			StsReg01b[14] = ((TempStsReg01 & 64) != 0);
			goto varview_refresh250;
		//assert(false);
		return;  			}
	varview_refresh250: {
			StsReg01b[15] = ((TempStsReg01 & 128) != 0);
			goto l2;
		//assert(false);
		return;  			}
	x3: {
			StsReg02b[0] = ((TempStsReg02 & 256) != 0);
			goto varview_refresh253;
		//assert(false);
		return;  			}
	varview_refresh253: {
			StsReg02b[1] = ((TempStsReg02 & 512) != 0);
			goto varview_refresh255;
		//assert(false);
		return;  			}
	varview_refresh255: {
			StsReg02b[2] = ((TempStsReg02 & 1024) != 0);
			goto varview_refresh257;
		//assert(false);
		return;  			}
	varview_refresh257: {
			StsReg02b[3] = ((TempStsReg02 & 2048) != 0);
			goto varview_refresh259;
		//assert(false);
		return;  			}
	varview_refresh259: {
			StsReg02b[4] = ((TempStsReg02 & 4096) != 0);
			goto varview_refresh261;
		//assert(false);
		return;  			}
	varview_refresh261: {
			StsReg02b[5] = ((TempStsReg02 & 8192) != 0);
			goto varview_refresh263;
		//assert(false);
		return;  			}
	varview_refresh263: {
			StsReg02b[6] = ((TempStsReg02 & 16384) != 0);
			goto varview_refresh265;
		//assert(false);
		return;  			}
	varview_refresh265: {
			StsReg02b[7] = ((TempStsReg02 & 32768) != 0);
			goto varview_refresh267;
		//assert(false);
		return;  			}
	varview_refresh267: {
			StsReg02b[8] = ((TempStsReg02 & 1) != 0);
			goto varview_refresh269;
		//assert(false);
		return;  			}
	varview_refresh269: {
			StsReg02b[9] = ((TempStsReg02 & 2) != 0);
			goto varview_refresh271;
		//assert(false);
		return;  			}
	varview_refresh271: {
			StsReg02b[10] = ((TempStsReg02 & 4) != 0);
			goto varview_refresh273;
		//assert(false);
		return;  			}
	varview_refresh273: {
			StsReg02b[11] = ((TempStsReg02 & 8) != 0);
			goto varview_refresh275;
		//assert(false);
		return;  			}
	varview_refresh275: {
			StsReg02b[12] = ((TempStsReg02 & 16) != 0);
			goto varview_refresh277;
		//assert(false);
		return;  			}
	varview_refresh277: {
			StsReg02b[13] = ((TempStsReg02 & 32) != 0);
			goto varview_refresh279;
		//assert(false);
		return;  			}
	varview_refresh279: {
			StsReg02b[14] = ((TempStsReg02 & 64) != 0);
			goto varview_refresh281;
		//assert(false);
		return;  			}
	varview_refresh281: {
			StsReg02b[15] = ((TempStsReg02 & 128) != 0);
			goto l3;
		//assert(false);
		return;  			}
	x4: {
			PAAb.ParRegb[0] = ((TempPAA.ParReg & 256) != 0);
			goto varview_refresh284;
		//assert(false);
		return;  			}
	varview_refresh284: {
			PAAb.ParRegb[1] = ((TempPAA.ParReg & 512) != 0);
			goto varview_refresh286;
		//assert(false);
		return;  			}
	varview_refresh286: {
			PAAb.ParRegb[2] = ((TempPAA.ParReg & 1024) != 0);
			goto varview_refresh288;
		//assert(false);
		return;  			}
	varview_refresh288: {
			PAAb.ParRegb[3] = ((TempPAA.ParReg & 2048) != 0);
			goto varview_refresh290;
		//assert(false);
		return;  			}
	varview_refresh290: {
			PAAb.ParRegb[4] = ((TempPAA.ParReg & 4096) != 0);
			goto varview_refresh292;
		//assert(false);
		return;  			}
	varview_refresh292: {
			PAAb.ParRegb[5] = ((TempPAA.ParReg & 8192) != 0);
			goto varview_refresh294;
		//assert(false);
		return;  			}
	varview_refresh294: {
			PAAb.ParRegb[6] = ((TempPAA.ParReg & 16384) != 0);
			goto varview_refresh296;
		//assert(false);
		return;  			}
	varview_refresh296: {
			PAAb.ParRegb[7] = ((TempPAA.ParReg & 32768) != 0);
			goto varview_refresh298;
		//assert(false);
		return;  			}
	varview_refresh298: {
			PAAb.ParRegb[8] = ((TempPAA.ParReg & 1) != 0);
			goto varview_refresh300;
		//assert(false);
		return;  			}
	varview_refresh300: {
			PAAb.ParRegb[9] = ((TempPAA.ParReg & 2) != 0);
			goto varview_refresh302;
		//assert(false);
		return;  			}
	varview_refresh302: {
			PAAb.ParRegb[10] = ((TempPAA.ParReg & 4) != 0);
			goto varview_refresh304;
		//assert(false);
		return;  			}
	varview_refresh304: {
			PAAb.ParRegb[11] = ((TempPAA.ParReg & 8) != 0);
			goto varview_refresh306;
		//assert(false);
		return;  			}
	varview_refresh306: {
			PAAb.ParRegb[12] = ((TempPAA.ParReg & 16) != 0);
			goto varview_refresh308;
		//assert(false);
		return;  			}
	varview_refresh308: {
			PAAb.ParRegb[13] = ((TempPAA.ParReg & 32) != 0);
			goto varview_refresh310;
		//assert(false);
		return;  			}
	varview_refresh310: {
			PAAb.ParRegb[14] = ((TempPAA.ParReg & 64) != 0);
			goto varview_refresh312;
		//assert(false);
		return;  			}
	varview_refresh312: {
			PAAb.ParRegb[15] = ((TempPAA.ParReg & 128) != 0);
			goto l4;
		//assert(false);
		return;  			}
	x5: {
		if (StsReg01b[8]) {
			TempStsReg01 = (TempStsReg01 | 1);
			goto l304;
		}
		if ((! StsReg01b[8])) {
			TempStsReg01 = (TempStsReg01 & 65534);
			goto l304;
		}
		//assert(false);
		return;  			}
	x6: {
		if (StsReg01b[9]) {
			TempStsReg01 = (TempStsReg01 | 2);
			goto l305;
		}
		if ((! StsReg01b[9])) {
			TempStsReg01 = (TempStsReg01 & 65533);
			goto l305;
		}
		//assert(false);
		return;  			}
	x7: {
		if (StsReg01b[10]) {
			TempStsReg01 = (TempStsReg01 | 4);
			goto l306;
		}
		if ((! StsReg01b[10])) {
			TempStsReg01 = (TempStsReg01 & 65531);
			goto l306;
		}
		//assert(false);
		return;  			}
	x8: {
		if (StsReg01b[11]) {
			TempStsReg01 = (TempStsReg01 | 8);
			goto l307;
		}
		if ((! StsReg01b[11])) {
			TempStsReg01 = (TempStsReg01 & 65527);
			goto l307;
		}
		//assert(false);
		return;  			}
	x9: {
		if (StsReg01b[12]) {
			TempStsReg01 = (TempStsReg01 | 16);
			goto l308;
		}
		if ((! StsReg01b[12])) {
			TempStsReg01 = (TempStsReg01 & 65519);
			goto l308;
		}
		//assert(false);
		return;  			}
	x10: {
		if (StsReg01b[13]) {
			TempStsReg01 = (TempStsReg01 | 32);
			goto l309;
		}
		if ((! StsReg01b[13])) {
			TempStsReg01 = (TempStsReg01 & 65503);
			goto l309;
		}
		//assert(false);
		return;  			}
	x11: {
		if (StsReg01b[14]) {
			TempStsReg01 = (TempStsReg01 | 64);
			goto l310;
		}
		if ((! StsReg01b[14])) {
			TempStsReg01 = (TempStsReg01 & 65471);
			goto l310;
		}
		//assert(false);
		return;  			}
	x12: {
		if (StsReg01b[15]) {
			TempStsReg01 = (TempStsReg01 | 128);
			goto l311;
		}
		if ((! StsReg01b[15])) {
			TempStsReg01 = (TempStsReg01 & 65407);
			goto l311;
		}
		//assert(false);
		return;  			}
	x13: {
		if (StsReg01b[0]) {
			TempStsReg01 = (TempStsReg01 | 256);
			goto l312;
		}
		if ((! StsReg01b[0])) {
			TempStsReg01 = (TempStsReg01 & 65279);
			goto l312;
		}
		//assert(false);
		return;  			}
	x14: {
		if (StsReg01b[1]) {
			TempStsReg01 = (TempStsReg01 | 512);
			goto l313;
		}
		if ((! StsReg01b[1])) {
			TempStsReg01 = (TempStsReg01 & 65023);
			goto l313;
		}
		//assert(false);
		return;  			}
	x15: {
		if (StsReg01b[2]) {
			TempStsReg01 = (TempStsReg01 | 1024);
			goto l314;
		}
		if ((! StsReg01b[2])) {
			TempStsReg01 = (TempStsReg01 & 64511);
			goto l314;
		}
		//assert(false);
		return;  			}
	x16: {
		if (StsReg01b[3]) {
			TempStsReg01 = (TempStsReg01 | 2048);
			goto l315;
		}
		if ((! StsReg01b[3])) {
			TempStsReg01 = (TempStsReg01 & 63487);
			goto l315;
		}
		//assert(false);
		return;  			}
	x17: {
		if (StsReg01b[4]) {
			TempStsReg01 = (TempStsReg01 | 4096);
			goto l316;
		}
		if ((! StsReg01b[4])) {
			TempStsReg01 = (TempStsReg01 & 61439);
			goto l316;
		}
		//assert(false);
		return;  			}
	x18: {
		if (StsReg01b[5]) {
			TempStsReg01 = (TempStsReg01 | 8192);
			goto l317;
		}
		if ((! StsReg01b[5])) {
			TempStsReg01 = (TempStsReg01 & 57343);
			goto l317;
		}
		//assert(false);
		return;  			}
	x19: {
		if (StsReg01b[6]) {
			TempStsReg01 = (TempStsReg01 | 16384);
			goto l318;
		}
		if ((! StsReg01b[6])) {
			TempStsReg01 = (TempStsReg01 & 49151);
			goto l318;
		}
		//assert(false);
		return;  			}
	x20: {
		if (StsReg01b[7]) {
			TempStsReg01 = (TempStsReg01 | 32768);
			goto l319;
		}
		if ((! StsReg01b[7])) {
			TempStsReg01 = (TempStsReg01 & 32767);
			goto l319;
		}
		//assert(false);
		return;  			}
	x21: {
		if (StsReg02b[8]) {
			TempStsReg02 = (TempStsReg02 | 1);
			goto l320;
		}
		if ((! StsReg02b[8])) {
			TempStsReg02 = (TempStsReg02 & 65534);
			goto l320;
		}
		//assert(false);
		return;  			}
	x22: {
		if (StsReg02b[9]) {
			TempStsReg02 = (TempStsReg02 | 2);
			goto l321;
		}
		if ((! StsReg02b[9])) {
			TempStsReg02 = (TempStsReg02 & 65533);
			goto l321;
		}
		//assert(false);
		return;  			}
	x23: {
		if (StsReg02b[10]) {
			TempStsReg02 = (TempStsReg02 | 4);
			goto l322;
		}
		if ((! StsReg02b[10])) {
			TempStsReg02 = (TempStsReg02 & 65531);
			goto l322;
		}
		//assert(false);
		return;  			}
	x24: {
		if (StsReg02b[11]) {
			TempStsReg02 = (TempStsReg02 | 8);
			goto l323;
		}
		if ((! StsReg02b[11])) {
			TempStsReg02 = (TempStsReg02 & 65527);
			goto l323;
		}
		//assert(false);
		return;  			}
	x25: {
		if (StsReg02b[12]) {
			TempStsReg02 = (TempStsReg02 | 16);
			goto l324;
		}
		if ((! StsReg02b[12])) {
			TempStsReg02 = (TempStsReg02 & 65519);
			goto l324;
		}
		//assert(false);
		return;  			}
	x26: {
		if (StsReg02b[13]) {
			TempStsReg02 = (TempStsReg02 | 32);
			goto l325;
		}
		if ((! StsReg02b[13])) {
			TempStsReg02 = (TempStsReg02 & 65503);
			goto l325;
		}
		//assert(false);
		return;  			}
	x27: {
		if (StsReg02b[14]) {
			TempStsReg02 = (TempStsReg02 | 64);
			goto l326;
		}
		if ((! StsReg02b[14])) {
			TempStsReg02 = (TempStsReg02 & 65471);
			goto l326;
		}
		//assert(false);
		return;  			}
	x28: {
		if (StsReg02b[15]) {
			TempStsReg02 = (TempStsReg02 | 128);
			goto l327;
		}
		if ((! StsReg02b[15])) {
			TempStsReg02 = (TempStsReg02 & 65407);
			goto l327;
		}
		//assert(false);
		return;  			}
	x29: {
		if (StsReg02b[0]) {
			TempStsReg02 = (TempStsReg02 | 256);
			goto l328;
		}
		if ((! StsReg02b[0])) {
			TempStsReg02 = (TempStsReg02 & 65279);
			goto l328;
		}
		//assert(false);
		return;  			}
	x30: {
		if (StsReg02b[1]) {
			TempStsReg02 = (TempStsReg02 | 512);
			goto l329;
		}
		if ((! StsReg02b[1])) {
			TempStsReg02 = (TempStsReg02 & 65023);
			goto l329;
		}
		//assert(false);
		return;  			}
	x31: {
		if (StsReg02b[2]) {
			TempStsReg02 = (TempStsReg02 | 1024);
			goto l330;
		}
		if ((! StsReg02b[2])) {
			TempStsReg02 = (TempStsReg02 & 64511);
			goto l330;
		}
		//assert(false);
		return;  			}
	x32: {
		if (StsReg02b[3]) {
			TempStsReg02 = (TempStsReg02 | 2048);
			goto l331;
		}
		if ((! StsReg02b[3])) {
			TempStsReg02 = (TempStsReg02 & 63487);
			goto l331;
		}
		//assert(false);
		return;  			}
	x33: {
		if (StsReg02b[4]) {
			TempStsReg02 = (TempStsReg02 | 4096);
			goto l332;
		}
		if ((! StsReg02b[4])) {
			TempStsReg02 = (TempStsReg02 & 61439);
			goto l332;
		}
		//assert(false);
		return;  			}
	x34: {
		if (StsReg02b[5]) {
			TempStsReg02 = (TempStsReg02 | 8192);
			goto l333;
		}
		if ((! StsReg02b[5])) {
			TempStsReg02 = (TempStsReg02 & 57343);
			goto l333;
		}
		//assert(false);
		return;  			}
	x35: {
		if (StsReg02b[6]) {
			TempStsReg02 = (TempStsReg02 | 16384);
			goto l334;
		}
		if ((! StsReg02b[6])) {
			TempStsReg02 = (TempStsReg02 & 49151);
			goto l334;
		}
		//assert(false);
		return;  			}
	x36: {
		if (StsReg02b[7]) {
			TempStsReg02 = (TempStsReg02 | 32768);
			goto l335;
		}
		if ((! StsReg02b[7])) {
			TempStsReg02 = (TempStsReg02 & 32767);
			goto l335;
		}
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void R_EDGE(__R_EDGE *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
		if (((__context->new == true) && (__context->old == false))) {
			goto l1100;
		}
		if ((! ((__context->new == true) && (__context->old == false)))) {
			goto l410;
		}
		//assert(false);
		return;  			}
	l1100: {
			__context->RET_VAL = true;
			goto l2100;
		//assert(false);
		return;  			}
	l2100: {
			__context->old = true;
			goto l340;
		//assert(false);
		return;  			}
	l340: {
			goto l710;
		//assert(false);
		return;  			}
	l410: {
			__context->RET_VAL = false;
			goto l510;
		//assert(false);
		return;  			}
	l510: {
			__context->old = __context->new;
			goto l610;
		//assert(false);
		return;  			}
	l610: {
			goto l710;
		//assert(false);
		return;  			}
	l710: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void DETECT_EDGE(__DETECT_EDGE *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init2;
	init2: {
		if ((__context->new != __context->old)) {
			goto l1101;
		}
		if ((! (__context->new != __context->old))) {
			goto l1010;
		}
		//assert(false);
		return;  			}
	l1101: {
		if ((__context->new == true)) {
			goto l2101;
		}
		if ((! (__context->new == true))) {
			goto l511;
		}
		//assert(false);
		return;  			}
	l2101: {
			__context->re = true;
			goto l341;
		//assert(false);
		return;  			}
	l341: {
			__context->fe = false;
			goto l411;
		//assert(false);
		return;  			}
	l411: {
			goto l810;
		//assert(false);
		return;  			}
	l511: {
			__context->re = false;
			goto l611;
		//assert(false);
		return;  			}
	l611: {
			__context->fe = true;
			goto l711;
		//assert(false);
		return;  			}
	l711: {
			goto l810;
		//assert(false);
		return;  			}
	l810: {
			__context->old = __context->new;
			goto l910;
		//assert(false);
		return;  			}
	l910: {
			goto l1310;
		//assert(false);
		return;  			}
	l1010: {
			__context->re = false;
			goto l1110;
		//assert(false);
		return;  			}
	l1110: {
			__context->fe = false;
			goto l1210;
		//assert(false);
		return;  			}
	l1210: {
			goto l1310;
		//assert(false);
		return;  			}
	l1310: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init3;
	init3: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			instance.H = nondet_float();
			instance.HH = nondet_float();
			instance.L = nondet_float();
			instance.LL = nondet_float();
			instance.Manreg01 = nondet_uint16_t();
			instance.Manreg01b[0] = nondet_bool();
			instance.Manreg01b[10] = nondet_bool();
			instance.Manreg01b[11] = nondet_bool();
			instance.Manreg01b[12] = nondet_bool();
			instance.Manreg01b[13] = nondet_bool();
			instance.Manreg01b[14] = nondet_bool();
			instance.Manreg01b[15] = nondet_bool();
			instance.Manreg01b[1] = nondet_bool();
			instance.Manreg01b[2] = nondet_bool();
			instance.Manreg01b[3] = nondet_bool();
			instance.Manreg01b[4] = nondet_bool();
			instance.Manreg01b[5] = nondet_bool();
			instance.Manreg01b[6] = nondet_bool();
			instance.Manreg01b[7] = nondet_bool();
			instance.Manreg01b[8] = nondet_bool();
			instance.Manreg01b[9] = nondet_bool();
			instance.Perst.ActRcp_old = nondet_bool();
			instance.Perst.AlUnAck = nondet_bool();
			instance.Perst.AlUnAck_old = nondet_bool();
			instance.Perst.Alarm_Cond_old = nondet_bool();
			instance.Perst.ArmRcpSt = nondet_bool();
			instance.Perst.ArmRcp_old = nondet_bool();
			instance.Perst.AuAlAck = nondet_bool();
			instance.Perst.AuAlAck_old = nondet_bool();
			instance.Perst.AuEH = nondet_bool();
			instance.Perst.AuEHH = nondet_bool();
			instance.Perst.AuEL = nondet_bool();
			instance.Perst.AuELL = nondet_bool();
			instance.Perst.AuIhMB = nondet_bool();
			instance.Perst.ConfigW = nondet_bool();
			instance.Perst.EHHSt = nondet_bool();
			instance.Perst.EHSt = nondet_bool();
			instance.Perst.ELLSt = nondet_bool();
			instance.Perst.ELSt = nondet_bool();
			instance.Perst.H = nondet_float();
			instance.Perst.HH = nondet_float();
			instance.Perst.HHAlSt = nondet_bool();
			instance.Perst.HHSt = nondet_float();
			instance.Perst.HH_AlarmPh1 = nondet_bool();
			instance.Perst.HH_AlarmPh2 = nondet_bool();
			instance.Perst.HH_TimeAlarm = nondet_int32_t();
			instance.Perst.HHinc = nondet_int16_t();
			instance.Perst.HSt = nondet_float();
			instance.Perst.HWSt = nondet_bool();
			instance.Perst.H_AlarmPh1 = nondet_bool();
			instance.Perst.H_AlarmPh2 = nondet_bool();
			instance.Perst.H_TimeAlarm = nondet_int32_t();
			instance.Perst.Hinc = nondet_int16_t();
			instance.Perst.I = nondet_float();
			instance.Perst.IOError = nondet_bool();
			instance.Perst.IOErrorW = nondet_bool();
			instance.Perst.IOSimu = nondet_bool();
			instance.Perst.IOSimuW = nondet_bool();
			instance.Perst.ISt = nondet_bool();
			instance.Perst.Iinc = nondet_int16_t();
			instance.Perst.L = nondet_float();
			instance.Perst.LL = nondet_float();
			instance.Perst.LLAlSt = nondet_bool();
			instance.Perst.LLSt = nondet_float();
			instance.Perst.LL_AlarmPh1 = nondet_bool();
			instance.Perst.LL_AlarmPh2 = nondet_bool();
			instance.Perst.LL_TimeAlarm = nondet_int32_t();
			instance.Perst.LLinc = nondet_int16_t();
			instance.Perst.LSt = nondet_float();
			instance.Perst.LWSt = nondet_bool();
			instance.Perst.L_AlarmPh1 = nondet_bool();
			instance.Perst.L_AlarmPh2 = nondet_bool();
			instance.Perst.L_TimeAlarm = nondet_int32_t();
			instance.Perst.Linc = nondet_int16_t();
			instance.Perst.MAlAck_old = nondet_bool();
			instance.Perst.MAlBRSt = nondet_bool();
			instance.Perst.MAlBSetRst_old = nondet_bool();
			instance.Perst.MNewHHR_old = nondet_bool();
			instance.Perst.MNewHR_old = nondet_bool();
			instance.Perst.MNewLLR_old = nondet_bool();
			instance.Perst.MNewLR_old = nondet_bool();
			instance.Perst.PAA.ParReg = nondet_uint16_t();
			instance.Perst.PAlDt = nondet_int16_t();
			instance.Perst.PAuAckAl = nondet_bool();
			instance.Perst.PosSt = nondet_float();
			instance.Perst.TimeAlarm = nondet_int32_t();
			instance.Perst.WHHAlSt = nondet_bool();
			instance.Perst.WHWSt = nondet_bool();
			instance.Perst.WISt = nondet_bool();
			instance.Perst.WLLAlSt = nondet_bool();
			instance.Perst.WLWSt = nondet_bool();
			instance.Perst.WSt = nondet_bool();
			instance.Perst.WWSt = nondet_bool();
			instance.Perst.Winc = nondet_int16_t();
			instance.StsReg01 = nondet_uint16_t();
			instance.StsReg02 = nondet_uint16_t();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			CPC_FB_AA(&instance);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
int main() {
	// Initial values
	First_Cycle = false;
	UNICOS_LiveCounter = 0;
	T_CYCLE = 0;
	R_EDGE1.new = false;
	R_EDGE1.old = false;
	R_EDGE1.RET_VAL = false;
	DETECT_EDGE1.new = false;
	DETECT_EDGE1.old = false;
	DETECT_EDGE1.re = false;
	DETECT_EDGE1.fe = false;
	instance.Manreg01 = 0;
	instance.Manreg01b[0] = false;
	instance.Manreg01b[1] = false;
	instance.Manreg01b[2] = false;
	instance.Manreg01b[3] = false;
	instance.Manreg01b[4] = false;
	instance.Manreg01b[5] = false;
	instance.Manreg01b[6] = false;
	instance.Manreg01b[7] = false;
	instance.Manreg01b[8] = false;
	instance.Manreg01b[9] = false;
	instance.Manreg01b[10] = false;
	instance.Manreg01b[11] = false;
	instance.Manreg01b[12] = false;
	instance.Manreg01b[13] = false;
	instance.Manreg01b[14] = false;
	instance.Manreg01b[15] = false;
	instance.HH = 0.0;
	instance.H = 0.0;
	instance.L = 0.0;
	instance.LL = 0.0;
	instance.StsReg01 = 0;
	instance.StsReg02 = 0;
	instance.Perst.I = 0.0;
	instance.Perst.PosSt = 0.0;
	instance.Perst.HHSt = 0.0;
	instance.Perst.HSt = 0.0;
	instance.Perst.LSt = 0.0;
	instance.Perst.LLSt = 0.0;
	instance.Perst.HH = 0.0;
	instance.Perst.H = 0.0;
	instance.Perst.L = 0.0;
	instance.Perst.LL = 0.0;
	instance.Perst.PAlDt = 0;
	instance.Perst.PAA.ParReg = 0;
	instance.Perst.AuAlAck = false;
	instance.Perst.ISt = false;
	instance.Perst.WSt = false;
	instance.Perst.AlUnAck = false;
	instance.Perst.MAlBRSt = false;
	instance.Perst.AuIhMB = false;
	instance.Perst.IOErrorW = false;
	instance.Perst.IOSimuW = false;
	instance.Perst.IOError = false;
	instance.Perst.IOSimu = false;
	instance.Perst.AuEHH = false;
	instance.Perst.AuEH = false;
	instance.Perst.AuEL = false;
	instance.Perst.AuELL = false;
	instance.Perst.HHAlSt = false;
	instance.Perst.HWSt = false;
	instance.Perst.LWSt = false;
	instance.Perst.LLAlSt = false;
	instance.Perst.ConfigW = false;
	instance.Perst.EHHSt = false;
	instance.Perst.EHSt = false;
	instance.Perst.ELSt = false;
	instance.Perst.ELLSt = false;
	instance.Perst.ArmRcpSt = false;
	instance.Perst.PAuAckAl = false;
	instance.Perst.MAlBSetRst_old = false;
	instance.Perst.MAlAck_old = false;
	instance.Perst.AuAlAck_old = false;
	instance.Perst.Alarm_Cond_old = false;
	instance.Perst.MNewHHR_old = false;
	instance.Perst.MNewHR_old = false;
	instance.Perst.MNewLR_old = false;
	instance.Perst.MNewLLR_old = false;
	instance.Perst.ArmRcp_old = false;
	instance.Perst.ActRcp_old = false;
	instance.Perst.AlUnAck_old = false;
	instance.Perst.HH_AlarmPh1 = false;
	instance.Perst.HH_AlarmPh2 = false;
	instance.Perst.H_AlarmPh1 = false;
	instance.Perst.H_AlarmPh2 = false;
	instance.Perst.L_AlarmPh1 = false;
	instance.Perst.L_AlarmPh2 = false;
	instance.Perst.LL_AlarmPh1 = false;
	instance.Perst.LL_AlarmPh2 = false;
	instance.Perst.HH_TimeAlarm = 0;
	instance.Perst.H_TimeAlarm = 0;
	instance.Perst.L_TimeAlarm = 0;
	instance.Perst.LL_TimeAlarm = 0;
	instance.Perst.TimeAlarm = 0;
	instance.Perst.Iinc = 0;
	instance.Perst.Winc = 0;
	instance.Perst.HHinc = 0;
	instance.Perst.Hinc = 0;
	instance.Perst.Linc = 0;
	instance.Perst.LLinc = 0;
	instance.Perst.WISt = false;
	instance.Perst.WWSt = false;
	instance.Perst.WHHAlSt = false;
	instance.Perst.WHWSt = false;
	instance.Perst.WLWSt = false;
	instance.Perst.WLLAlSt = false;
	R_EDGE1_inlined_1.new = false;
	R_EDGE1_inlined_1.old = false;
	R_EDGE1_inlined_1.RET_VAL = false;
	R_EDGE1_inlined_2.new = false;
	R_EDGE1_inlined_2.old = false;
	R_EDGE1_inlined_2.RET_VAL = false;
	R_EDGE1_inlined_3.new = false;
	R_EDGE1_inlined_3.old = false;
	R_EDGE1_inlined_3.RET_VAL = false;
	R_EDGE1_inlined_4.new = false;
	R_EDGE1_inlined_4.old = false;
	R_EDGE1_inlined_4.RET_VAL = false;
	R_EDGE1_inlined_5.new = false;
	R_EDGE1_inlined_5.old = false;
	R_EDGE1_inlined_5.RET_VAL = false;
	R_EDGE1_inlined_6.new = false;
	R_EDGE1_inlined_6.old = false;
	R_EDGE1_inlined_6.RET_VAL = false;
	R_EDGE1_inlined_7.new = false;
	R_EDGE1_inlined_7.old = false;
	R_EDGE1_inlined_7.RET_VAL = false;
	R_EDGE1_inlined_8.new = false;
	R_EDGE1_inlined_8.old = false;
	R_EDGE1_inlined_8.RET_VAL = false;
	R_EDGE1_inlined_9.new = false;
	R_EDGE1_inlined_9.old = false;
	R_EDGE1_inlined_9.RET_VAL = false;
	R_EDGE1_inlined_10.new = false;
	R_EDGE1_inlined_10.old = false;
	R_EDGE1_inlined_10.RET_VAL = false;
	__assertion_error = 0;
	
	VerificationLoop();
}
