#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Variables
int32_t UNICOS_LiveCounter = 0;
int32_t T_CYCLE = 0;
bool R_EDGE_new = false;
bool R_EDGE_old = false;
bool R_EDGE_RET_VAL = false;
bool DETECT_EDGE_new = false;
bool DETECT_EDGE_old = false;
bool DETECT_EDGE_re = false;
bool DETECT_EDGE_fe = false;
uint16_t instance_Manreg01 = 0;
bool instance_PAuAckAl = false;
uint16_t instance_StsReg01 = 0;
int16_t instance_Perst_PAlDt = 0;
bool instance_Perst_PAuAckAl = false;
bool instance_Perst_I = false;
bool instance_Perst_AuAlAck = false;
bool instance_Perst_ISt = false;
bool instance_Perst_AlUnAck = false;
bool instance_Perst_MAlBRSt = false;
bool instance_Perst_AuIhMB = false;
bool instance_Perst_IOErrorW = false;
bool instance_Perst_IOSimuW = false;
bool instance_Perst_IOError = false;
bool instance_Perst_IOSimu = false;
bool instance_Perst_AuEAl = true;
bool instance_Perst_MAlBSetRst_old = false;
bool instance_Perst_MAlAck_old = false;
bool instance_Perst_AuAlAck_old = false;
bool instance_Perst_Alarm_Cond_old = false;
bool instance_Perst_AlUnAck_old = false;
bool instance_Perst_AlarmPh1 = false;
bool instance_Perst_AlarmPh2 = false;
bool instance_Perst_WISt = false;
int32_t instance_Perst_TimeAlarm = 0;
int16_t instance_Perst_Iinc = 0;
bool instance_E_MAlBSetRst = false;
bool instance_E_MAlAck = false;
bool instance_E_AuAlAck = false;
bool instance_E_Alarm_Cond = false;
bool instance_RE_AlUnAck = false;
bool instance_FE_AlUnAck = false;
bool instance_PosW = false;
bool instance_ConfigW = false;
uint16_t instance_TempStsReg01 = 0;
float instance_PulseWidth = 0.0;
bool R_EDGE_inlined_1_new = false;
bool R_EDGE_inlined_1_old = false;
bool R_EDGE_inlined_1_RET_VAL = false;
bool R_EDGE_inlined_2_new = false;
bool R_EDGE_inlined_2_old = false;
bool R_EDGE_inlined_2_RET_VAL = false;
bool R_EDGE_inlined_3_new = false;
bool R_EDGE_inlined_3_old = false;
bool R_EDGE_inlined_3_RET_VAL = false;
bool R_EDGE_inlined_4_new = false;
bool R_EDGE_inlined_4_old = false;
bool R_EDGE_inlined_4_RET_VAL = false;
uint16_t __assertion_error = 0;
bool instance_Manreg01b_0 = false;
bool instance_Manreg01b_1 = false;
bool instance_Manreg01b_2 = false;
bool instance_Manreg01b_3 = false;
bool instance_Manreg01b_4 = false;
bool instance_Manreg01b_5 = false;
bool instance_Manreg01b_6 = false;
bool instance_Manreg01b_7 = false;
bool instance_Manreg01b_8 = false;
bool instance_Manreg01b_9 = false;
bool instance_Manreg01b_10 = false;
bool instance_Manreg01b_11 = false;
bool instance_Manreg01b_12 = false;
bool instance_Manreg01b_13 = false;
bool instance_Manreg01b_14 = false;
bool instance_Manreg01b_15 = false;
bool instance_StsReg01b_0 = false;
bool instance_StsReg01b_1 = false;
bool instance_StsReg01b_2 = false;
bool instance_StsReg01b_3 = false;
bool instance_StsReg01b_4 = false;
bool instance_StsReg01b_5 = false;
bool instance_StsReg01b_6 = false;
bool instance_StsReg01b_7 = false;
bool instance_StsReg01b_8 = false;
bool instance_StsReg01b_9 = false;
bool instance_StsReg01b_10 = false;
bool instance_StsReg01b_11 = false;
bool instance_StsReg01b_12 = false;
bool instance_StsReg01b_13 = false;
bool instance_StsReg01b_14 = false;
bool instance_StsReg01b_15 = false;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			instance_Manreg01 = nondet_uint16_t();
			instance_Manreg01b_0 = nondet_bool();
			instance_Manreg01b_10 = nondet_bool();
			instance_Manreg01b_11 = nondet_bool();
			instance_Manreg01b_12 = nondet_bool();
			instance_Manreg01b_13 = nondet_bool();
			instance_Manreg01b_14 = nondet_bool();
			instance_Manreg01b_15 = nondet_bool();
			instance_Manreg01b_1 = nondet_bool();
			instance_Manreg01b_2 = nondet_bool();
			instance_Manreg01b_3 = nondet_bool();
			instance_Manreg01b_4 = nondet_bool();
			instance_Manreg01b_5 = nondet_bool();
			instance_Manreg01b_6 = nondet_bool();
			instance_Manreg01b_7 = nondet_bool();
			instance_Manreg01b_8 = nondet_bool();
			instance_Manreg01b_9 = nondet_bool();
			instance_PAuAckAl = nondet_bool();
			instance_Perst_AlUnAck = nondet_bool();
			instance_Perst_AlUnAck_old = nondet_bool();
			instance_Perst_AlarmPh1 = nondet_bool();
			instance_Perst_AlarmPh2 = nondet_bool();
			instance_Perst_Alarm_Cond_old = nondet_bool();
			instance_Perst_AuAlAck = nondet_bool();
			instance_Perst_AuAlAck_old = nondet_bool();
			instance_Perst_AuEAl = nondet_bool();
			instance_Perst_AuIhMB = nondet_bool();
			instance_Perst_I = nondet_bool();
			instance_Perst_IOError = nondet_bool();
			instance_Perst_IOErrorW = nondet_bool();
			instance_Perst_IOSimu = nondet_bool();
			instance_Perst_IOSimuW = nondet_bool();
			instance_Perst_ISt = nondet_bool();
			instance_Perst_Iinc = nondet_int16_t();
			instance_Perst_MAlAck_old = nondet_bool();
			instance_Perst_MAlBRSt = nondet_bool();
			instance_Perst_MAlBSetRst_old = nondet_bool();
			instance_Perst_PAlDt = nondet_int16_t();
			instance_Perst_PAuAckAl = nondet_bool();
			instance_Perst_TimeAlarm = nondet_int32_t();
			instance_Perst_WISt = nondet_bool();
			instance_StsReg01 = nondet_uint16_t();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			instance_Manreg01b_0 = ((instance_Manreg01 & 256) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			instance_TempStsReg01 = instance_StsReg01;
			goto verificationLoop_VerificationLoop_x2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
			R_EDGE_inlined_1_new = instance_Manreg01b_2;
			R_EDGE_inlined_1_old = instance_Perst_MAlBSetRst_old;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l3: {
			R_EDGE_inlined_2_new = instance_Manreg01b_7;
			R_EDGE_inlined_2_old = instance_Perst_MAlAck_old;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_init1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l4: {
			R_EDGE_inlined_3_new = instance_Perst_AuAlAck;
			R_EDGE_inlined_3_old = instance_Perst_AuAlAck_old;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_init2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l5: {
		if (instance_E_MAlBSetRst) {
			goto verificationLoop_VerificationLoop_l6;
		}
		if ((! instance_E_MAlBSetRst)) {
			goto verificationLoop_VerificationLoop_l8;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l6: {
			instance_Perst_MAlBRSt = (! instance_Perst_MAlBRSt);
			goto verificationLoop_VerificationLoop_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l7: {
			goto verificationLoop_VerificationLoop_l9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l8: {
			goto verificationLoop_VerificationLoop_l9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l9: {
		if (instance_Perst_AuIhMB) {
			goto verificationLoop_VerificationLoop_l10;
		}
		if ((! instance_Perst_AuIhMB)) {
			goto verificationLoop_VerificationLoop_l12;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l10: {
			instance_Perst_MAlBRSt = false;
			goto verificationLoop_VerificationLoop_l11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l11: {
			goto verificationLoop_VerificationLoop_l13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l12: {
			goto verificationLoop_VerificationLoop_l13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l13: {
		if ((instance_Perst_I && instance_Perst_AuEAl)) {
			goto verificationLoop_VerificationLoop_l14;
		}
		if ((! (instance_Perst_I && instance_Perst_AuEAl))) {
			goto verificationLoop_VerificationLoop_l16;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l14: {
			instance_Perst_AlarmPh1 = true;
			goto verificationLoop_VerificationLoop_l15;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l15: {
			goto verificationLoop_VerificationLoop_l19;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l16: {
			instance_Perst_AlarmPh1 = false;
			goto verificationLoop_VerificationLoop_l17;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l17: {
			instance_Perst_AlarmPh2 = false;
			goto verificationLoop_VerificationLoop_l18;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l18: {
			goto verificationLoop_VerificationLoop_l19;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l19: {
			instance_PosW = false;
			goto verificationLoop_VerificationLoop_l20;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l20: {
		if (instance_Perst_AlarmPh1) {
			goto verificationLoop_VerificationLoop_l21;
		}
		if ((! instance_Perst_AlarmPh1)) {
			goto verificationLoop_VerificationLoop_l49;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l21: {
		if ((! instance_Perst_AlarmPh2)) {
			goto verificationLoop_VerificationLoop_l22;
		}
		if ((! (! instance_Perst_AlarmPh2))) {
			goto verificationLoop_VerificationLoop_l25;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l22: {
			instance_Perst_TimeAlarm = (UNICOS_LiveCounter + ((int32_t) instance_Perst_PAlDt));
			goto verificationLoop_VerificationLoop_l23;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l23: {
			instance_Perst_AlarmPh2 = true;
			goto verificationLoop_VerificationLoop_l24;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l24: {
			goto verificationLoop_VerificationLoop_l26;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l25: {
			goto verificationLoop_VerificationLoop_l26;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l26: {
		if (instance_Perst_AlarmPh2) {
			goto verificationLoop_VerificationLoop_l27;
		}
		if ((! instance_Perst_AlarmPh2)) {
			goto verificationLoop_VerificationLoop_l43;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l27: {
		if ((UNICOS_LiveCounter >= instance_Perst_TimeAlarm)) {
			goto verificationLoop_VerificationLoop_l28;
		}
		if (((! (UNICOS_LiveCounter >= instance_Perst_TimeAlarm)) && instance_Perst_MAlBRSt)) {
			goto verificationLoop_VerificationLoop_l36;
		}
		if (((! (UNICOS_LiveCounter >= instance_Perst_TimeAlarm)) && (! ((! (UNICOS_LiveCounter >= instance_Perst_TimeAlarm)) && instance_Perst_MAlBRSt)))) {
			goto verificationLoop_VerificationLoop_l39;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l28: {
		if (instance_Perst_MAlBRSt) {
			goto verificationLoop_VerificationLoop_l29;
		}
		if ((! instance_Perst_MAlBRSt)) {
			goto verificationLoop_VerificationLoop_l32;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l29: {
			instance_Perst_ISt = false;
			goto verificationLoop_VerificationLoop_l30;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l30: {
			instance_PosW = true;
			goto verificationLoop_VerificationLoop_l31;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l31: {
			goto verificationLoop_VerificationLoop_l35;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l32: {
			instance_Perst_ISt = true;
			goto verificationLoop_VerificationLoop_l33;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l33: {
			instance_PosW = false;
			goto verificationLoop_VerificationLoop_l34;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l34: {
			goto verificationLoop_VerificationLoop_l35;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l35: {
			goto verificationLoop_VerificationLoop_l42;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l36: {
			instance_Perst_ISt = false;
			goto verificationLoop_VerificationLoop_l37;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l37: {
			instance_PosW = false;
			goto verificationLoop_VerificationLoop_l38;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l38: {
			goto verificationLoop_VerificationLoop_l42;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l39: {
			instance_Perst_ISt = false;
			goto verificationLoop_VerificationLoop_l40;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l40: {
			instance_PosW = true;
			goto verificationLoop_VerificationLoop_l41;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l41: {
			goto verificationLoop_VerificationLoop_l42;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l42: {
			goto verificationLoop_VerificationLoop_l44;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l43: {
			goto verificationLoop_VerificationLoop_l44;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l44: {
		if (((instance_Perst_TimeAlarm - ((int32_t) instance_Perst_PAlDt)) > UNICOS_LiveCounter)) {
			goto verificationLoop_VerificationLoop_l45;
		}
		if ((! ((instance_Perst_TimeAlarm - ((int32_t) instance_Perst_PAlDt)) > UNICOS_LiveCounter))) {
			goto verificationLoop_VerificationLoop_l47;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l45: {
			instance_Perst_TimeAlarm = (UNICOS_LiveCounter + ((int32_t) instance_Perst_PAlDt));
			goto verificationLoop_VerificationLoop_l46;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l46: {
			goto verificationLoop_VerificationLoop_l48;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l47: {
			goto verificationLoop_VerificationLoop_l48;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l48: {
			goto verificationLoop_VerificationLoop_l52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l49: {
			instance_Perst_TimeAlarm = 0;
			goto verificationLoop_VerificationLoop_l50;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l50: {
			instance_Perst_ISt = false;
			goto verificationLoop_VerificationLoop_l51;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l51: {
			goto verificationLoop_VerificationLoop_l52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l52: {
			R_EDGE_inlined_4_new = instance_Perst_ISt;
			R_EDGE_inlined_4_old = instance_Perst_Alarm_Cond_old;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_init3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l53: {
		if ((((instance_E_MAlAck || instance_E_AuAlAck) || instance_Perst_MAlBRSt) || instance_Perst_PAuAckAl)) {
			goto verificationLoop_VerificationLoop_l54;
		}
		if (((! (((instance_E_MAlAck || instance_E_AuAlAck) || instance_Perst_MAlBRSt) || instance_Perst_PAuAckAl)) && instance_E_Alarm_Cond)) {
			goto verificationLoop_VerificationLoop_l56;
		}
		if (((! (((instance_E_MAlAck || instance_E_AuAlAck) || instance_Perst_MAlBRSt) || instance_Perst_PAuAckAl)) && (! ((! (((instance_E_MAlAck || instance_E_AuAlAck) || instance_Perst_MAlBRSt) || instance_Perst_PAuAckAl)) && instance_E_Alarm_Cond)))) {
			goto verificationLoop_VerificationLoop_l58;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l54: {
			instance_Perst_AlUnAck = false;
			goto verificationLoop_VerificationLoop_l55;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l55: {
			goto verificationLoop_VerificationLoop_l59;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l56: {
			instance_Perst_AlUnAck = true;
			goto verificationLoop_VerificationLoop_l57;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l57: {
			goto verificationLoop_VerificationLoop_l59;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l58: {
			goto verificationLoop_VerificationLoop_l59;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l59: {
			instance_ConfigW = (! instance_Perst_AuEAl);
			goto verificationLoop_VerificationLoop_l60;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l60: {
			instance_Perst_IOErrorW = instance_Perst_IOError;
			goto verificationLoop_VerificationLoop_l61;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l61: {
			instance_Perst_IOSimuW = instance_Perst_IOSimu;
			goto verificationLoop_VerificationLoop_l62;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l62: {
			instance_PulseWidth = (1500.0 / ((float) ((int32_t) T_CYCLE)));
			goto verificationLoop_VerificationLoop_l63;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l63: {
		if ((instance_Perst_ISt || (instance_Perst_Iinc > 0))) {
			goto verificationLoop_VerificationLoop_l64;
		}
		if ((! (instance_Perst_ISt || (instance_Perst_Iinc > 0)))) {
			goto verificationLoop_VerificationLoop_l67;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l64: {
			instance_Perst_Iinc = (instance_Perst_Iinc + 1);
			goto verificationLoop_VerificationLoop_l65;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l65: {
			instance_Perst_WISt = true;
			goto verificationLoop_VerificationLoop_l66;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l66: {
			goto verificationLoop_VerificationLoop_l68;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l67: {
			goto verificationLoop_VerificationLoop_l68;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l68: {
		if (((((float) instance_Perst_Iinc) > instance_PulseWidth) || ((! instance_Perst_ISt) && (instance_Perst_Iinc == 0)))) {
			goto verificationLoop_VerificationLoop_l69;
		}
		if ((! ((((float) instance_Perst_Iinc) > instance_PulseWidth) || ((! instance_Perst_ISt) && (instance_Perst_Iinc == 0))))) {
			goto verificationLoop_VerificationLoop_l72;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l69: {
			instance_Perst_Iinc = 0;
			goto verificationLoop_VerificationLoop_l70;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l70: {
			instance_Perst_WISt = instance_Perst_ISt;
			goto verificationLoop_VerificationLoop_l71;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l71: {
			goto verificationLoop_VerificationLoop_l73;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l72: {
			goto verificationLoop_VerificationLoop_l73;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l73: {
			instance_StsReg01b_8 = instance_Perst_WISt;
			goto verificationLoop_VerificationLoop_x3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l74: {
			instance_StsReg01b_9 = false;
			goto verificationLoop_VerificationLoop_x4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l75: {
			instance_StsReg01b_10 = false;
			goto verificationLoop_VerificationLoop_x5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l76: {
			instance_StsReg01b_11 = false;
			goto verificationLoop_VerificationLoop_x6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l77: {
			instance_StsReg01b_12 = false;
			goto verificationLoop_VerificationLoop_x7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l78: {
			instance_StsReg01b_13 = instance_ConfigW;
			goto verificationLoop_VerificationLoop_x8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l79: {
			instance_StsReg01b_14 = instance_Perst_IOErrorW;
			goto verificationLoop_VerificationLoop_x9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l80: {
			instance_StsReg01b_15 = instance_Perst_IOSimuW;
			goto verificationLoop_VerificationLoop_x10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l81: {
			instance_StsReg01b_0 = instance_Perst_AuEAl;
			goto verificationLoop_VerificationLoop_x11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l82: {
			instance_StsReg01b_1 = instance_PosW;
			goto verificationLoop_VerificationLoop_x12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l83: {
			instance_StsReg01b_2 = false;
			goto verificationLoop_VerificationLoop_x13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l84: {
			instance_StsReg01b_3 = false;
			goto verificationLoop_VerificationLoop_x14;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l85: {
			instance_StsReg01b_4 = instance_Perst_AlUnAck;
			goto verificationLoop_VerificationLoop_x15;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l86: {
			instance_StsReg01b_5 = false;
			goto verificationLoop_VerificationLoop_x16;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l87: {
			instance_StsReg01b_6 = instance_Perst_MAlBRSt;
			goto verificationLoop_VerificationLoop_x17;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l88: {
			instance_StsReg01b_7 = instance_Perst_AuIhMB;
			goto verificationLoop_VerificationLoop_x18;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l89: {
			DETECT_EDGE_new = instance_Perst_AlUnAck;
			DETECT_EDGE_old = instance_Perst_AlUnAck_old;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_init4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l90: {
			instance_StsReg01 = instance_TempStsReg01;
			goto verificationLoop_VerificationLoop_l91;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l91: {
			goto callEnd;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x: {
			instance_ConfigW = false;
			instance_E_Alarm_Cond = false;
			instance_E_AuAlAck = false;
			instance_E_MAlAck = false;
			instance_E_MAlBSetRst = false;
			instance_FE_AlUnAck = false;
			instance_PosW = false;
			instance_PulseWidth = 0.0;
			instance_RE_AlUnAck = false;
			instance_TempStsReg01 = 0;
			goto verificationLoop_VerificationLoop_x1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh: {
			instance_StsReg01b_0 = ((instance_TempStsReg01 & 256) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh16;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh1: {
			instance_Manreg01b_1 = ((instance_Manreg01 & 512) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh2: {
			instance_Manreg01b_2 = ((instance_Manreg01 & 1024) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh3: {
			instance_Manreg01b_3 = ((instance_Manreg01 & 2048) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh4: {
			instance_Manreg01b_4 = ((instance_Manreg01 & 4096) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh5: {
			instance_Manreg01b_5 = ((instance_Manreg01 & 8192) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh6: {
			instance_Manreg01b_6 = ((instance_Manreg01 & 16384) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh7: {
			instance_Manreg01b_7 = ((instance_Manreg01 & 32768) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh8: {
			instance_Manreg01b_8 = ((instance_Manreg01 & 1) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh9: {
			instance_Manreg01b_9 = ((instance_Manreg01 & 2) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh10: {
			instance_Manreg01b_10 = ((instance_Manreg01 & 4) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh11: {
			instance_Manreg01b_11 = ((instance_Manreg01 & 8) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh12: {
			instance_Manreg01b_12 = ((instance_Manreg01 & 16) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh13: {
			instance_Manreg01b_13 = ((instance_Manreg01 & 32) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh14;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh14: {
			instance_Manreg01b_14 = ((instance_Manreg01 & 64) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh15;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh15: {
			instance_Manreg01b_15 = ((instance_Manreg01 & 128) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh16: {
			instance_StsReg01b_1 = ((instance_TempStsReg01 & 512) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh17;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh17: {
			instance_StsReg01b_2 = ((instance_TempStsReg01 & 1024) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh18;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh18: {
			instance_StsReg01b_3 = ((instance_TempStsReg01 & 2048) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh19;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh19: {
			instance_StsReg01b_4 = ((instance_TempStsReg01 & 4096) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh20;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh20: {
			instance_StsReg01b_5 = ((instance_TempStsReg01 & 8192) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh21: {
			instance_StsReg01b_6 = ((instance_TempStsReg01 & 16384) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh22: {
			instance_StsReg01b_7 = ((instance_TempStsReg01 & 32768) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh23;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh23: {
			instance_StsReg01b_8 = ((instance_TempStsReg01 & 1) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh24;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh24: {
			instance_StsReg01b_9 = ((instance_TempStsReg01 & 2) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh25;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh25: {
			instance_StsReg01b_10 = ((instance_TempStsReg01 & 4) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh26;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh26: {
			instance_StsReg01b_11 = ((instance_TempStsReg01 & 8) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh27;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh27: {
			instance_StsReg01b_12 = ((instance_TempStsReg01 & 16) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh28;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh28: {
			instance_StsReg01b_13 = ((instance_TempStsReg01 & 32) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh29;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh29: {
			instance_StsReg01b_14 = ((instance_TempStsReg01 & 64) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh30;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh30: {
			instance_StsReg01b_15 = ((instance_TempStsReg01 & 128) != 0);
			goto verificationLoop_VerificationLoop_x;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x1: {
			instance_StsReg01b_0 = ((instance_TempStsReg01 & 256) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh31;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh31: {
			instance_StsReg01b_1 = ((instance_TempStsReg01 & 512) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh32;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh32: {
			instance_StsReg01b_2 = ((instance_TempStsReg01 & 1024) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh33;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh33: {
			instance_StsReg01b_3 = ((instance_TempStsReg01 & 2048) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh34;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh34: {
			instance_StsReg01b_4 = ((instance_TempStsReg01 & 4096) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh35;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh35: {
			instance_StsReg01b_5 = ((instance_TempStsReg01 & 8192) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh36;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh36: {
			instance_StsReg01b_6 = ((instance_TempStsReg01 & 16384) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh37;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh37: {
			instance_StsReg01b_7 = ((instance_TempStsReg01 & 32768) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh38;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh38: {
			instance_StsReg01b_8 = ((instance_TempStsReg01 & 1) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh39;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh39: {
			instance_StsReg01b_9 = ((instance_TempStsReg01 & 2) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh40;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh40: {
			instance_StsReg01b_10 = ((instance_TempStsReg01 & 4) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh41;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh41: {
			instance_StsReg01b_11 = ((instance_TempStsReg01 & 8) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh42;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh42: {
			instance_StsReg01b_12 = ((instance_TempStsReg01 & 16) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh43;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh43: {
			instance_StsReg01b_13 = ((instance_TempStsReg01 & 32) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh44;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh44: {
			instance_StsReg01b_14 = ((instance_TempStsReg01 & 64) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh45;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh45: {
			instance_StsReg01b_15 = ((instance_TempStsReg01 & 128) != 0);
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x2: {
			instance_StsReg01b_0 = ((instance_TempStsReg01 & 256) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh46;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh46: {
			instance_StsReg01b_1 = ((instance_TempStsReg01 & 512) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh47;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh47: {
			instance_StsReg01b_2 = ((instance_TempStsReg01 & 1024) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh48;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh48: {
			instance_StsReg01b_3 = ((instance_TempStsReg01 & 2048) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh49;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh49: {
			instance_StsReg01b_4 = ((instance_TempStsReg01 & 4096) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh50;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh50: {
			instance_StsReg01b_5 = ((instance_TempStsReg01 & 8192) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh51;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh51: {
			instance_StsReg01b_6 = ((instance_TempStsReg01 & 16384) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh52: {
			instance_StsReg01b_7 = ((instance_TempStsReg01 & 32768) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh53;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh53: {
			instance_StsReg01b_8 = ((instance_TempStsReg01 & 1) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh54;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh54: {
			instance_StsReg01b_9 = ((instance_TempStsReg01 & 2) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh55;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh55: {
			instance_StsReg01b_10 = ((instance_TempStsReg01 & 4) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh56;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh56: {
			instance_StsReg01b_11 = ((instance_TempStsReg01 & 8) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh57;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh57: {
			instance_StsReg01b_12 = ((instance_TempStsReg01 & 16) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh58;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh58: {
			instance_StsReg01b_13 = ((instance_TempStsReg01 & 32) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh59;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh59: {
			instance_StsReg01b_14 = ((instance_TempStsReg01 & 64) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh60;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh60: {
			instance_StsReg01b_15 = ((instance_TempStsReg01 & 128) != 0);
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x3: {
		if (instance_StsReg01b_8) {
			instance_TempStsReg01 = (instance_TempStsReg01 | 1);
			goto verificationLoop_VerificationLoop_l74;
		}
		if ((! instance_StsReg01b_8)) {
			instance_TempStsReg01 = (instance_TempStsReg01 & 65534);
			goto verificationLoop_VerificationLoop_l74;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x4: {
		if (instance_StsReg01b_9) {
			instance_TempStsReg01 = (instance_TempStsReg01 | 2);
			goto verificationLoop_VerificationLoop_l75;
		}
		if ((! instance_StsReg01b_9)) {
			instance_TempStsReg01 = (instance_TempStsReg01 & 65533);
			goto verificationLoop_VerificationLoop_l75;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x5: {
		if (instance_StsReg01b_10) {
			instance_TempStsReg01 = (instance_TempStsReg01 | 4);
			goto verificationLoop_VerificationLoop_l76;
		}
		if ((! instance_StsReg01b_10)) {
			instance_TempStsReg01 = (instance_TempStsReg01 & 65531);
			goto verificationLoop_VerificationLoop_l76;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x6: {
		if (instance_StsReg01b_11) {
			instance_TempStsReg01 = (instance_TempStsReg01 | 8);
			goto verificationLoop_VerificationLoop_l77;
		}
		if ((! instance_StsReg01b_11)) {
			instance_TempStsReg01 = (instance_TempStsReg01 & 65527);
			goto verificationLoop_VerificationLoop_l77;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x7: {
		if (instance_StsReg01b_12) {
			instance_TempStsReg01 = (instance_TempStsReg01 | 16);
			goto verificationLoop_VerificationLoop_l78;
		}
		if ((! instance_StsReg01b_12)) {
			instance_TempStsReg01 = (instance_TempStsReg01 & 65519);
			goto verificationLoop_VerificationLoop_l78;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x8: {
		if (instance_StsReg01b_13) {
			instance_TempStsReg01 = (instance_TempStsReg01 | 32);
			goto verificationLoop_VerificationLoop_l79;
		}
		if ((! instance_StsReg01b_13)) {
			instance_TempStsReg01 = (instance_TempStsReg01 & 65503);
			goto verificationLoop_VerificationLoop_l79;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x9: {
		if (instance_StsReg01b_14) {
			instance_TempStsReg01 = (instance_TempStsReg01 | 64);
			goto verificationLoop_VerificationLoop_l80;
		}
		if ((! instance_StsReg01b_14)) {
			instance_TempStsReg01 = (instance_TempStsReg01 & 65471);
			goto verificationLoop_VerificationLoop_l80;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x10: {
		if (instance_StsReg01b_15) {
			instance_TempStsReg01 = (instance_TempStsReg01 | 128);
			goto verificationLoop_VerificationLoop_l81;
		}
		if ((! instance_StsReg01b_15)) {
			instance_TempStsReg01 = (instance_TempStsReg01 & 65407);
			goto verificationLoop_VerificationLoop_l81;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x11: {
		if (instance_StsReg01b_0) {
			instance_TempStsReg01 = (instance_TempStsReg01 | 256);
			goto verificationLoop_VerificationLoop_l82;
		}
		if ((! instance_StsReg01b_0)) {
			instance_TempStsReg01 = (instance_TempStsReg01 & 65279);
			goto verificationLoop_VerificationLoop_l82;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x12: {
		if (instance_StsReg01b_1) {
			instance_TempStsReg01 = (instance_TempStsReg01 | 512);
			goto verificationLoop_VerificationLoop_l83;
		}
		if ((! instance_StsReg01b_1)) {
			instance_TempStsReg01 = (instance_TempStsReg01 & 65023);
			goto verificationLoop_VerificationLoop_l83;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x13: {
		if (instance_StsReg01b_2) {
			instance_TempStsReg01 = (instance_TempStsReg01 | 1024);
			goto verificationLoop_VerificationLoop_l84;
		}
		if ((! instance_StsReg01b_2)) {
			instance_TempStsReg01 = (instance_TempStsReg01 & 64511);
			goto verificationLoop_VerificationLoop_l84;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x14: {
		if (instance_StsReg01b_3) {
			instance_TempStsReg01 = (instance_TempStsReg01 | 2048);
			goto verificationLoop_VerificationLoop_l85;
		}
		if ((! instance_StsReg01b_3)) {
			instance_TempStsReg01 = (instance_TempStsReg01 & 63487);
			goto verificationLoop_VerificationLoop_l85;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x15: {
		if (instance_StsReg01b_4) {
			instance_TempStsReg01 = (instance_TempStsReg01 | 4096);
			goto verificationLoop_VerificationLoop_l86;
		}
		if ((! instance_StsReg01b_4)) {
			instance_TempStsReg01 = (instance_TempStsReg01 & 61439);
			goto verificationLoop_VerificationLoop_l86;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x16: {
		if (instance_StsReg01b_5) {
			instance_TempStsReg01 = (instance_TempStsReg01 | 8192);
			goto verificationLoop_VerificationLoop_l87;
		}
		if ((! instance_StsReg01b_5)) {
			instance_TempStsReg01 = (instance_TempStsReg01 & 57343);
			goto verificationLoop_VerificationLoop_l87;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x17: {
		if (instance_StsReg01b_6) {
			instance_TempStsReg01 = (instance_TempStsReg01 | 16384);
			goto verificationLoop_VerificationLoop_l88;
		}
		if ((! instance_StsReg01b_6)) {
			instance_TempStsReg01 = (instance_TempStsReg01 & 49151);
			goto verificationLoop_VerificationLoop_l88;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x18: {
		if (instance_StsReg01b_7) {
			instance_TempStsReg01 = (instance_TempStsReg01 | 32768);
			goto verificationLoop_VerificationLoop_l89;
		}
		if ((! instance_StsReg01b_7)) {
			instance_TempStsReg01 = (instance_TempStsReg01 & 32767);
			goto verificationLoop_VerificationLoop_l89;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_init: {
		if (((R_EDGE_inlined_1_new == true) && (R_EDGE_inlined_1_old == false))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l1;
		}
		if ((! ((R_EDGE_inlined_1_new == true) && (R_EDGE_inlined_1_old == false)))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l4;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l1: {
			R_EDGE_inlined_1_RET_VAL = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l2: {
			R_EDGE_inlined_1_old = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l3: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l4: {
			R_EDGE_inlined_1_RET_VAL = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l5: {
			R_EDGE_inlined_1_old = R_EDGE_inlined_1_new;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l6: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l7: {
			instance_Perst_MAlBSetRst_old = R_EDGE_inlined_1_old;
			instance_E_MAlBSetRst = R_EDGE_inlined_1_RET_VAL;
			goto verificationLoop_VerificationLoop_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_init1: {
		if (((R_EDGE_inlined_2_new == true) && (R_EDGE_inlined_2_old == false))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l11;
		}
		if ((! ((R_EDGE_inlined_2_new == true) && (R_EDGE_inlined_2_old == false)))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l41;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l11: {
			R_EDGE_inlined_2_RET_VAL = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l21: {
			R_EDGE_inlined_2_old = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l31;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l31: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l71;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l41: {
			R_EDGE_inlined_2_RET_VAL = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l51;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l51: {
			R_EDGE_inlined_2_old = R_EDGE_inlined_2_new;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l61;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l61: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l71;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l71: {
			instance_Perst_MAlAck_old = R_EDGE_inlined_2_old;
			instance_E_MAlAck = R_EDGE_inlined_2_RET_VAL;
			goto verificationLoop_VerificationLoop_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_init2: {
		if (((R_EDGE_inlined_3_new == true) && (R_EDGE_inlined_3_old == false))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l12;
		}
		if ((! ((R_EDGE_inlined_3_new == true) && (R_EDGE_inlined_3_old == false)))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l42;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l12: {
			R_EDGE_inlined_3_RET_VAL = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l22: {
			R_EDGE_inlined_3_old = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l32;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l32: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l72;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l42: {
			R_EDGE_inlined_3_RET_VAL = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l52: {
			R_EDGE_inlined_3_old = R_EDGE_inlined_3_new;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l62;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l62: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l72;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l72: {
			instance_Perst_AuAlAck_old = R_EDGE_inlined_3_old;
			instance_E_AuAlAck = R_EDGE_inlined_3_RET_VAL;
			goto verificationLoop_VerificationLoop_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_init3: {
		if (((R_EDGE_inlined_4_new == true) && (R_EDGE_inlined_4_old == false))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l13;
		}
		if ((! ((R_EDGE_inlined_4_new == true) && (R_EDGE_inlined_4_old == false)))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l43;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l13: {
			R_EDGE_inlined_4_RET_VAL = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l23;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l23: {
			R_EDGE_inlined_4_old = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l33;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l33: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l73;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l43: {
			R_EDGE_inlined_4_RET_VAL = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l53;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l53: {
			R_EDGE_inlined_4_old = R_EDGE_inlined_4_new;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l63;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l63: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l73;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l73: {
			instance_Perst_Alarm_Cond_old = R_EDGE_inlined_4_old;
			instance_E_Alarm_Cond = R_EDGE_inlined_4_RET_VAL;
			goto verificationLoop_VerificationLoop_l53;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_init4: {
		if ((DETECT_EDGE_new != DETECT_EDGE_old)) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l14;
		}
		if ((! (DETECT_EDGE_new != DETECT_EDGE_old))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l10;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l14: {
		if ((DETECT_EDGE_new == true)) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l24;
		}
		if ((! (DETECT_EDGE_new == true))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l54;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l24: {
			DETECT_EDGE_re = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l34;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l34: {
			DETECT_EDGE_fe = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l44;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l44: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l54: {
			DETECT_EDGE_re = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l64;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l64: {
			DETECT_EDGE_fe = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l74;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l74: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l8: {
			DETECT_EDGE_old = DETECT_EDGE_new;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l9: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l131;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l10: {
			DETECT_EDGE_re = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l111;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l111: {
			DETECT_EDGE_fe = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l121;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l121: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DA_l131;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DA_l131: {
			instance_Perst_AlUnAck_old = DETECT_EDGE_old;
			instance_RE_AlUnAck = DETECT_EDGE_re;
			instance_FE_AlUnAck = DETECT_EDGE_fe;
			goto verificationLoop_VerificationLoop_l90;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
