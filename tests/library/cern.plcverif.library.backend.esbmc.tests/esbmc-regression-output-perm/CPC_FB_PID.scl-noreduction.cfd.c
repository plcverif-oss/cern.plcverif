#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Root data structure
typedef struct {
	float Kc;
	float Ti;
	float Td;
	float Tds;
	float SPH;
	float SPL;
	float OutH;
	float OutL;
	bool EKc;
	bool ETi;
	bool ETd;
	bool ETds;
	bool ESPH;
	bool ESPL;
	bool EOutH;
	bool EOutL;
} __CPC_PID_LIB_PARAM;
typedef struct {
	float PMinRan;
	float PMaxRan;
	float POutMinRan;
	float POutMaxRan;
	int32_t MVFiltTime;
	int32_t PIDCycle;
	int16_t ScaMethod;
	bool RA;
} __CPC_PID_PARAM;
typedef struct {
	float InSpd;
	float DeSpd;
} __CPC_RAMP_PARAM;
typedef struct {
	float INV;
	int32_t Td;
	int32_t Tds;
	int32_t CYCLE;
	int32_t TM_LAG;
	float OUTV;
	float FiltINV;
	float LastFiltINV;
} __DIF;
typedef struct {
	float INV;
	int32_t Ti;
	int32_t CYCLE;
	float OUTV;
} __INTEG;
typedef struct {
	float INV;
	int32_t TM_LAG;
	float DF_OUTV;
	bool TRACK;
	bool DFOUT_ON;
	bool COM_RST;
	int32_t CYCLE;
	float OUTV;
} __LAG1ST;
typedef struct {
	float INV;
	float UPRLM_P;
	float DNRLM_P;
	float UPRLM_N;
	float DNRLM_N;
	float H_LM;
	float L_LM;
	float PV;
	float DF_OUTV;
	bool DFOUT_ON;
	bool TRACK;
	bool MAN_ON;
	bool COM_RST;
	int32_t CYCLE;
	float OUTV;
	bool QUPRLM_P;
	bool QDNRLM_P;
	bool QUPRLM_N;
	bool QDNRLM_N;
	bool QH_LM;
	bool QL_LM;
} __ROC_LIM;
typedef struct {
	float HMV;
	float HOutO;
	bool IOError;
	bool IOSimu;
	bool AuActR;
	bool AuOutPR;
	bool AuRegR;
	bool AuTrR;
	bool AuAuMoR;
	bool AuIhSR;
	float AuPosR;
	float AuSPR;
	bool AuESP;
	bool AuIhMMo;
	bool AuIhFoMo;
	__CPC_RAMP_PARAM AuSPSpd;
	bool AuPRest;
	__CPC_PID_LIB_PARAM AuPPID;
	uint16_t Manreg01;
	uint16_t Manreg02;
	bool Manreg01b[16];
	bool Manreg02b[16];
	float MPosR;
	float MSP;
	float MSPH;
	float MSPL;
	float MOutH;
	float MOutL;
	float MKc;
	float MTd;
	float MTi;
	float MTds;
	__CPC_PID_PARAM PControl;
	float OutOV;
	bool AuRegSt;
	uint16_t Stsreg01;
	bool Stsreg01b[16];
	uint16_t Stsreg02;
	bool Stsreg02b[16];
	bool AuMoSt;
	bool MMoSt;
	bool FoMoSt;
	bool SoftLDSt;
	bool RegSt;
	bool OutPSt;
	bool TrSt;
	bool IOErrorW;
	bool IOSimuW;
	float ActSP;
	float MSPSt;
	float AuSPSt;
	float MPosRSt;
	float AuPosRSt;
	float ActKc;
	float ActTi;
	float ActTd;
	float ActTds;
	float ActSPH;
	float ActSPL;
	float ActOutH;
	float ActOutL;
	float MV;
	float DefKc;
	float DefTd;
	float DefTi;
	float DefTds;
	float DefSPH;
	float DefSPL;
	float DefOutH;
	float DefOutL;
	float DefSP;
	bool E_MAuMoR;
	bool E_MMMoR;
	bool E_MFoMoR;
	bool E_MPRest;
	bool E_MPSav;
	bool E_MNewPosR;
	bool E_MNewSPR;
	bool E_MNewSPHR;
	bool E_MNewSPLR;
	bool E_MNewOutHR;
	bool E_MNewOutLR;
	bool E_MNewKcR;
	bool E_MNewTdR;
	bool E_MNewTiR;
	bool E_MNewTdsR;
	bool E_MRegR;
	bool E_MOutPR;
	bool E_AuAuMoR;
	bool E_ArmRcp;
	bool E_ActRcp;
	bool E_MSoftLDR;
	bool MAuMoR_old;
	bool MMMoR_old;
	bool MFoMoR_old;
	bool MPRest_old;
	bool MPDefold;
	bool MNewPosR_old;
	bool MNewSPR_old;
	bool MNewSPHR_old;
	bool MNewSPLR_old;
	bool MNewOutHR_old;
	bool MNewOutLR_old;
	bool MNewKcR_old;
	bool MNewTdR_old;
	bool MNewTiR_old;
	bool MNewTdsR_old;
	bool MRegR_old;
	bool MOutPR_old;
	bool AuAuMoR_old;
	bool ArmRcp_old;
	bool ActRcp_old;
	bool MSoftLDR_old;
	bool KcDiDef;
	bool TiDiDef;
	bool TdDiDef;
	bool TdsDiDef;
	bool SPHDiDef;
	bool SPLDiDef;
	bool OutHDiDef;
	bool OutLDiDef;
	bool ArmRcpSt;
	bool last_RegSt;
	float SPScaled;
	float HMVScaled;
	float SPHScaled;
	float SPLScaled;
	float OutHScaled;
	float OutLScaled;
	bool tracking_control;
	float tracking_value;
	float tracking_value_Scaled;
	float ActSPR;
	float dev;
	float PID_Out;
	bool PID_activation;
	__ROC_LIM ROC_LIM;
	__LAG1ST MVFILTER;
	__INTEG INTEG;
	__DIF DIF;
	float PID_FF;
	float PID_calc;
} __CPC_FB_PID;
typedef struct {
	bool new;
	bool old;
	bool RET_VAL;
} __R_EDGE;

// Global variables
int32_t PID_EXEC_CYCLE;
__R_EDGE R_EDGE1;
__CPC_FB_PID instance;
__R_EDGE R_EDGE1_inlined_1;
__R_EDGE R_EDGE1_inlined_2;
__R_EDGE R_EDGE1_inlined_3;
__R_EDGE R_EDGE1_inlined_4;
__R_EDGE R_EDGE1_inlined_5;
__R_EDGE R_EDGE1_inlined_6;
__R_EDGE R_EDGE1_inlined_7;
__R_EDGE R_EDGE1_inlined_8;
__R_EDGE R_EDGE1_inlined_9;
__R_EDGE R_EDGE1_inlined_10;
__R_EDGE R_EDGE1_inlined_11;
__R_EDGE R_EDGE1_inlined_12;
__R_EDGE R_EDGE1_inlined_13;
__R_EDGE R_EDGE1_inlined_14;
__R_EDGE R_EDGE1_inlined_15;
__R_EDGE R_EDGE1_inlined_16;
__R_EDGE R_EDGE1_inlined_17;
__R_EDGE R_EDGE1_inlined_18;
__R_EDGE R_EDGE1_inlined_19;
__R_EDGE R_EDGE1_inlined_20;
__R_EDGE R_EDGE1_inlined_21;
uint16_t __assertion_error;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Automata declarations
void CPC_FB_PID(__CPC_FB_PID *__context);
void ROC_LIM(__ROC_LIM *__context);
void LAG1ST(__LAG1ST *__context);
void INTEG(__INTEG *__context);
void DIF(__DIF *__context);
void R_EDGE(__R_EDGE *__context);
void VerificationLoop();

// Automata
void CPC_FB_PID(__CPC_FB_PID *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			__context->Manreg01b[0] = ((__context->Manreg01 & 256) != 0);
			goto varview_refresh1;
		//assert(false);
		return;  			}
	l1: {
			// Assign inputs
			R_EDGE1_inlined_2.new = __context->Manreg01b[9];
			R_EDGE1_inlined_2.old = __context->MMMoR_old;
			R_EDGE(&R_EDGE1_inlined_2);
			// Assign outputs
			__context->MMMoR_old = R_EDGE1_inlined_2.old;
			__context->E_MMMoR = R_EDGE1_inlined_2.RET_VAL;
			goto l2;
		//assert(false);
		return;  			}
	l2: {
			// Assign inputs
			R_EDGE1_inlined_3.new = __context->Manreg01b[10];
			R_EDGE1_inlined_3.old = __context->MFoMoR_old;
			R_EDGE(&R_EDGE1_inlined_3);
			// Assign outputs
			__context->MFoMoR_old = R_EDGE1_inlined_3.old;
			__context->E_MFoMoR = R_EDGE1_inlined_3.RET_VAL;
			goto l3;
		//assert(false);
		return;  			}
	l3: {
			// Assign inputs
			R_EDGE1_inlined_4.new = __context->Manreg01b[11];
			R_EDGE1_inlined_4.old = __context->ActRcp_old;
			R_EDGE(&R_EDGE1_inlined_4);
			// Assign outputs
			__context->ActRcp_old = R_EDGE1_inlined_4.old;
			__context->E_ActRcp = R_EDGE1_inlined_4.RET_VAL;
			goto l4;
		//assert(false);
		return;  			}
	l4: {
			// Assign inputs
			R_EDGE1_inlined_5.new = __context->Manreg01b[12];
			R_EDGE1_inlined_5.old = __context->ArmRcp_old;
			R_EDGE(&R_EDGE1_inlined_5);
			// Assign outputs
			__context->ArmRcp_old = R_EDGE1_inlined_5.old;
			__context->E_ArmRcp = R_EDGE1_inlined_5.RET_VAL;
			goto l5;
		//assert(false);
		return;  			}
	l5: {
			// Assign inputs
			R_EDGE1_inlined_6.new = __context->Manreg01b[13];
			R_EDGE1_inlined_6.old = __context->MPDefold;
			R_EDGE(&R_EDGE1_inlined_6);
			// Assign outputs
			__context->MPDefold = R_EDGE1_inlined_6.old;
			__context->E_MPSav = R_EDGE1_inlined_6.RET_VAL;
			goto l6;
		//assert(false);
		return;  			}
	l6: {
			// Assign inputs
			R_EDGE1_inlined_7.new = __context->Manreg01b[14];
			R_EDGE1_inlined_7.old = __context->MPRest_old;
			R_EDGE(&R_EDGE1_inlined_7);
			// Assign outputs
			__context->MPRest_old = R_EDGE1_inlined_7.old;
			__context->E_MPRest = R_EDGE1_inlined_7.RET_VAL;
			goto l7;
		//assert(false);
		return;  			}
	l7: {
			// Assign inputs
			R_EDGE1_inlined_8.new = __context->Manreg01b[15];
			R_EDGE1_inlined_8.old = __context->MNewSPR_old;
			R_EDGE(&R_EDGE1_inlined_8);
			// Assign outputs
			__context->MNewSPR_old = R_EDGE1_inlined_8.old;
			__context->E_MNewSPR = R_EDGE1_inlined_8.RET_VAL;
			goto l8;
		//assert(false);
		return;  			}
	l8: {
			// Assign inputs
			R_EDGE1_inlined_9.new = __context->Manreg01b[0];
			R_EDGE1_inlined_9.old = __context->MNewSPHR_old;
			R_EDGE(&R_EDGE1_inlined_9);
			// Assign outputs
			__context->MNewSPHR_old = R_EDGE1_inlined_9.old;
			__context->E_MNewSPHR = R_EDGE1_inlined_9.RET_VAL;
			goto l9;
		//assert(false);
		return;  			}
	l9: {
			// Assign inputs
			R_EDGE1_inlined_10.new = __context->Manreg01b[1];
			R_EDGE1_inlined_10.old = __context->MNewSPLR_old;
			R_EDGE(&R_EDGE1_inlined_10);
			// Assign outputs
			__context->MNewSPLR_old = R_EDGE1_inlined_10.old;
			__context->E_MNewSPLR = R_EDGE1_inlined_10.RET_VAL;
			goto l10;
		//assert(false);
		return;  			}
	l10: {
			// Assign inputs
			R_EDGE1_inlined_11.new = __context->Manreg01b[2];
			R_EDGE1_inlined_11.old = __context->MNewOutHR_old;
			R_EDGE(&R_EDGE1_inlined_11);
			// Assign outputs
			__context->MNewOutHR_old = R_EDGE1_inlined_11.old;
			__context->E_MNewOutHR = R_EDGE1_inlined_11.RET_VAL;
			goto l11;
		//assert(false);
		return;  			}
	l11: {
			// Assign inputs
			R_EDGE1_inlined_12.new = __context->Manreg01b[3];
			R_EDGE1_inlined_12.old = __context->MNewOutLR_old;
			R_EDGE(&R_EDGE1_inlined_12);
			// Assign outputs
			__context->MNewOutLR_old = R_EDGE1_inlined_12.old;
			__context->E_MNewOutLR = R_EDGE1_inlined_12.RET_VAL;
			goto l12;
		//assert(false);
		return;  			}
	l12: {
			// Assign inputs
			R_EDGE1_inlined_13.new = __context->Manreg01b[4];
			R_EDGE1_inlined_13.old = __context->MNewKcR_old;
			R_EDGE(&R_EDGE1_inlined_13);
			// Assign outputs
			__context->MNewKcR_old = R_EDGE1_inlined_13.old;
			__context->E_MNewKcR = R_EDGE1_inlined_13.RET_VAL;
			goto l13;
		//assert(false);
		return;  			}
	l13: {
			// Assign inputs
			R_EDGE1_inlined_14.new = __context->Manreg01b[5];
			R_EDGE1_inlined_14.old = __context->MNewTdR_old;
			R_EDGE(&R_EDGE1_inlined_14);
			// Assign outputs
			__context->MNewTdR_old = R_EDGE1_inlined_14.old;
			__context->E_MNewTdR = R_EDGE1_inlined_14.RET_VAL;
			goto l14;
		//assert(false);
		return;  			}
	l14: {
			// Assign inputs
			R_EDGE1_inlined_15.new = __context->Manreg01b[6];
			R_EDGE1_inlined_15.old = __context->MNewTiR_old;
			R_EDGE(&R_EDGE1_inlined_15);
			// Assign outputs
			__context->MNewTiR_old = R_EDGE1_inlined_15.old;
			__context->E_MNewTiR = R_EDGE1_inlined_15.RET_VAL;
			goto l15;
		//assert(false);
		return;  			}
	l15: {
			// Assign inputs
			R_EDGE1_inlined_16.new = __context->Manreg01b[7];
			R_EDGE1_inlined_16.old = __context->MNewTdsR_old;
			R_EDGE(&R_EDGE1_inlined_16);
			// Assign outputs
			__context->MNewTdsR_old = R_EDGE1_inlined_16.old;
			__context->E_MNewTdsR = R_EDGE1_inlined_16.RET_VAL;
			goto l16;
		//assert(false);
		return;  			}
	l16: {
			// Assign inputs
			R_EDGE1_inlined_17.new = __context->Manreg02b[8];
			R_EDGE1_inlined_17.old = __context->MRegR_old;
			R_EDGE(&R_EDGE1_inlined_17);
			// Assign outputs
			__context->MRegR_old = R_EDGE1_inlined_17.old;
			__context->E_MRegR = R_EDGE1_inlined_17.RET_VAL;
			goto l17;
		//assert(false);
		return;  			}
	l17: {
			// Assign inputs
			R_EDGE1_inlined_18.new = __context->Manreg02b[9];
			R_EDGE1_inlined_18.old = __context->MOutPR_old;
			R_EDGE(&R_EDGE1_inlined_18);
			// Assign outputs
			__context->MOutPR_old = R_EDGE1_inlined_18.old;
			__context->E_MOutPR = R_EDGE1_inlined_18.RET_VAL;
			goto l18;
		//assert(false);
		return;  			}
	l18: {
			// Assign inputs
			R_EDGE1_inlined_19.new = __context->Manreg02b[10];
			R_EDGE1_inlined_19.old = __context->MSoftLDR_old;
			R_EDGE(&R_EDGE1_inlined_19);
			// Assign outputs
			__context->MSoftLDR_old = R_EDGE1_inlined_19.old;
			__context->E_MSoftLDR = R_EDGE1_inlined_19.RET_VAL;
			goto l19;
		//assert(false);
		return;  			}
	l19: {
			// Assign inputs
			R_EDGE1_inlined_20.new = __context->Manreg02b[11];
			R_EDGE1_inlined_20.old = __context->MNewPosR_old;
			R_EDGE(&R_EDGE1_inlined_20);
			// Assign outputs
			__context->MNewPosR_old = R_EDGE1_inlined_20.old;
			__context->E_MNewPosR = R_EDGE1_inlined_20.RET_VAL;
			goto l20;
		//assert(false);
		return;  			}
	l20: {
			// Assign inputs
			R_EDGE1_inlined_21.new = __context->AuAuMoR;
			R_EDGE1_inlined_21.old = __context->AuAuMoR_old;
			R_EDGE(&R_EDGE1_inlined_21);
			// Assign outputs
			__context->AuAuMoR_old = R_EDGE1_inlined_21.old;
			__context->E_AuAuMoR = R_EDGE1_inlined_21.RET_VAL;
			goto l21;
		//assert(false);
		return;  			}
	l21: {
		if ((((((((((__context->ActSP == 0.0) && (__context->ActSPL == 0.0)) && (__context->ActSPH == 0.0)) && (__context->ActKc == 0.0)) && (__context->ActTd == 0.0)) && (__context->ActTd == 0.0)) && (__context->ActTds == 0.0)) && (__context->ActOutH == 0.0)) && (__context->ActOutL == 0.0))) {
			goto l22;
		}
		if ((! (((((((((__context->ActSP == 0.0) && (__context->ActSPL == 0.0)) && (__context->ActSPH == 0.0)) && (__context->ActKc == 0.0)) && (__context->ActTd == 0.0)) && (__context->ActTd == 0.0)) && (__context->ActTds == 0.0)) && (__context->ActOutH == 0.0)) && (__context->ActOutL == 0.0)))) {
			goto l32;
		}
		//assert(false);
		return;  			}
	l22: {
			__context->ActSP = __context->DefSP;
			goto l23;
		//assert(false);
		return;  			}
	l23: {
			__context->ActSPL = __context->DefSPL;
			goto l24;
		//assert(false);
		return;  			}
	l24: {
			__context->ActSPH = __context->DefSPH;
			goto l25;
		//assert(false);
		return;  			}
	l25: {
			__context->ActKc = __context->DefKc;
			goto l26;
		//assert(false);
		return;  			}
	l26: {
			__context->ActTd = __context->DefTd;
			goto l27;
		//assert(false);
		return;  			}
	l27: {
			__context->ActTi = __context->DefTi;
			goto l28;
		//assert(false);
		return;  			}
	l28: {
			__context->ActTds = __context->DefTds;
			goto l29;
		//assert(false);
		return;  			}
	l29: {
			__context->ActOutH = __context->DefOutH;
			goto l30;
		//assert(false);
		return;  			}
	l30: {
			__context->ActOutL = __context->DefOutL;
			goto l31;
		//assert(false);
		return;  			}
	l31: {
			goto l33;
		//assert(false);
		return;  			}
	l32: {
			goto l33;
		//assert(false);
		return;  			}
	l33: {
		if (((((__context->AuMoSt || __context->MMoSt) || __context->SoftLDSt) && __context->E_MFoMoR) && (! __context->AuIhFoMo))) {
			goto l34;
		}
		if ((! ((((__context->AuMoSt || __context->MMoSt) || __context->SoftLDSt) && __context->E_MFoMoR) && (! __context->AuIhFoMo)))) {
			goto l39;
		}
		//assert(false);
		return;  			}
	l34: {
			__context->AuMoSt = false;
			goto l35;
		//assert(false);
		return;  			}
	l35: {
			__context->MMoSt = false;
			goto l36;
		//assert(false);
		return;  			}
	l36: {
			__context->FoMoSt = true;
			goto l37;
		//assert(false);
		return;  			}
	l37: {
			__context->SoftLDSt = false;
			goto l38;
		//assert(false);
		return;  			}
	l38: {
			goto l40;
		//assert(false);
		return;  			}
	l39: {
			goto l40;
		//assert(false);
		return;  			}
	l40: {
		if (((((__context->AuMoSt || __context->FoMoSt) || __context->SoftLDSt) && __context->E_MMMoR) && (! __context->AuIhMMo))) {
			goto l41;
		}
		if ((! ((((__context->AuMoSt || __context->FoMoSt) || __context->SoftLDSt) && __context->E_MMMoR) && (! __context->AuIhMMo)))) {
			goto l46;
		}
		//assert(false);
		return;  			}
	l41: {
			__context->AuMoSt = false;
			goto l42;
		//assert(false);
		return;  			}
	l42: {
			__context->MMoSt = true;
			goto l43;
		//assert(false);
		return;  			}
	l43: {
			__context->FoMoSt = false;
			goto l44;
		//assert(false);
		return;  			}
	l44: {
			__context->SoftLDSt = false;
			goto l45;
		//assert(false);
		return;  			}
	l45: {
			goto l47;
		//assert(false);
		return;  			}
	l46: {
			goto l47;
		//assert(false);
		return;  			}
	l47: {
		if ((((((((__context->MMoSt && (__context->E_MAuMoR || __context->E_AuAuMoR)) || (__context->FoMoSt && __context->E_MAuMoR)) || (__context->SoftLDSt && __context->E_MAuMoR)) || (__context->MMoSt && __context->AuIhMMo)) || (__context->FoMoSt && __context->AuIhFoMo)) || (__context->SoftLDSt && __context->AuIhFoMo)) || (! (((__context->AuMoSt || __context->MMoSt) || __context->FoMoSt) || __context->SoftLDSt)))) {
			goto l48;
		}
		if ((! (((((((__context->MMoSt && (__context->E_MAuMoR || __context->E_AuAuMoR)) || (__context->FoMoSt && __context->E_MAuMoR)) || (__context->SoftLDSt && __context->E_MAuMoR)) || (__context->MMoSt && __context->AuIhMMo)) || (__context->FoMoSt && __context->AuIhFoMo)) || (__context->SoftLDSt && __context->AuIhFoMo)) || (! (((__context->AuMoSt || __context->MMoSt) || __context->FoMoSt) || __context->SoftLDSt))))) {
			goto l53;
		}
		//assert(false);
		return;  			}
	l48: {
			__context->AuMoSt = true;
			goto l49;
		//assert(false);
		return;  			}
	l49: {
			__context->MMoSt = false;
			goto l50;
		//assert(false);
		return;  			}
	l50: {
			__context->FoMoSt = false;
			goto l51;
		//assert(false);
		return;  			}
	l51: {
			__context->SoftLDSt = false;
			goto l52;
		//assert(false);
		return;  			}
	l52: {
			goto l54;
		//assert(false);
		return;  			}
	l53: {
			goto l54;
		//assert(false);
		return;  			}
	l54: {
		if (((((__context->AuMoSt || __context->MMoSt) && __context->E_MSoftLDR) && (! (__context->E_MAuMoR || __context->E_MMMoR))) && (! __context->AuIhFoMo))) {
			goto l55;
		}
		if ((! ((((__context->AuMoSt || __context->MMoSt) && __context->E_MSoftLDR) && (! (__context->E_MAuMoR || __context->E_MMMoR))) && (! __context->AuIhFoMo)))) {
			goto l60;
		}
		//assert(false);
		return;  			}
	l55: {
			__context->AuMoSt = false;
			goto l56;
		//assert(false);
		return;  			}
	l56: {
			__context->MMoSt = false;
			goto l57;
		//assert(false);
		return;  			}
	l57: {
			__context->FoMoSt = false;
			goto l58;
		//assert(false);
		return;  			}
	l58: {
			__context->SoftLDSt = true;
			goto l59;
		//assert(false);
		return;  			}
	l59: {
			goto l61;
		//assert(false);
		return;  			}
	l60: {
			goto l61;
		//assert(false);
		return;  			}
	l61: {
		if (((((__context->AuMoSt && __context->AuRegR) && (! __context->TrSt)) || ((((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt) && __context->E_MRegR) && (! __context->TrSt))) || (((__context->TrSt == true) && (__context->AuTrR == false)) && (__context->last_RegSt == true)))) {
			goto l62;
		}
		if ((! ((((__context->AuMoSt && __context->AuRegR) && (! __context->TrSt)) || ((((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt) && __context->E_MRegR) && (! __context->TrSt))) || (((__context->TrSt == true) && (__context->AuTrR == false)) && (__context->last_RegSt == true))))) {
			goto l68;
		}
		//assert(false);
		return;  			}
	l62: {
			__context->RegSt = true;
			goto l63;
		//assert(false);
		return;  			}
	l63: {
			__context->OutPSt = false;
			goto l64;
		//assert(false);
		return;  			}
	l64: {
			__context->TrSt = false;
			goto l65;
		//assert(false);
		return;  			}
	l65: {
			__context->last_RegSt = true;
			goto l66;
		//assert(false);
		return;  			}
	l66: {
			__context->tracking_control = false;
			goto l67;
		//assert(false);
		return;  			}
	l67: {
			goto l69;
		//assert(false);
		return;  			}
	l68: {
			goto l69;
		//assert(false);
		return;  			}
	l69: {
		if (((((__context->AuMoSt && __context->AuOutPR) && (! __context->TrSt)) || ((((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt) && __context->E_MOutPR) && (! __context->TrSt))) || ((__context->TrSt && (! __context->AuTrR)) && (! __context->last_RegSt)))) {
			goto l70;
		}
		if ((! ((((__context->AuMoSt && __context->AuOutPR) && (! __context->TrSt)) || ((((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt) && __context->E_MOutPR) && (! __context->TrSt))) || ((__context->TrSt && (! __context->AuTrR)) && (! __context->last_RegSt))))) {
			goto l76;
		}
		//assert(false);
		return;  			}
	l70: {
			__context->RegSt = false;
			goto l71;
		//assert(false);
		return;  			}
	l71: {
			__context->OutPSt = true;
			goto l72;
		//assert(false);
		return;  			}
	l72: {
			__context->TrSt = false;
			goto l73;
		//assert(false);
		return;  			}
	l73: {
			__context->last_RegSt = false;
			goto l74;
		//assert(false);
		return;  			}
	l74: {
			__context->tracking_control = true;
			goto l75;
		//assert(false);
		return;  			}
	l75: {
			goto l77;
		//assert(false);
		return;  			}
	l76: {
			goto l77;
		//assert(false);
		return;  			}
	l77: {
		if (__context->AuTrR) {
			goto l78;
		}
		if ((! __context->AuTrR)) {
			goto l83;
		}
		//assert(false);
		return;  			}
	l78: {
			__context->RegSt = false;
			goto l79;
		//assert(false);
		return;  			}
	l79: {
			__context->OutPSt = false;
			goto l80;
		//assert(false);
		return;  			}
	l80: {
			__context->TrSt = true;
			goto l81;
		//assert(false);
		return;  			}
	l81: {
			__context->tracking_control = true;
			goto l82;
		//assert(false);
		return;  			}
	l82: {
			goto l84;
		//assert(false);
		return;  			}
	l83: {
			goto l84;
		//assert(false);
		return;  			}
	l84: {
			__context->IOErrorW = __context->IOError;
			goto l85;
		//assert(false);
		return;  			}
	l85: {
			__context->IOSimuW = __context->IOSimu;
			goto l86;
		//assert(false);
		return;  			}
	l86: {
		if ((__context->E_ArmRcp && (! (__context->FoMoSt || __context->SoftLDSt)))) {
			goto l87;
		}
		if ((! (__context->E_ArmRcp && (! (__context->FoMoSt || __context->SoftLDSt))))) {
			goto l89;
		}
		//assert(false);
		return;  			}
	l87: {
			__context->ArmRcpSt = true;
			goto l88;
		//assert(false);
		return;  			}
	l88: {
			goto l90;
		//assert(false);
		return;  			}
	l89: {
			goto l90;
		//assert(false);
		return;  			}
	l90: {
		if ((__context->E_ArmRcp && __context->E_ActRcp)) {
			goto l91;
		}
		if ((! (__context->E_ArmRcp && __context->E_ActRcp))) {
			goto l93;
		}
		//assert(false);
		return;  			}
	l91: {
			__context->ArmRcpSt = false;
			goto l92;
		//assert(false);
		return;  			}
	l92: {
			goto l94;
		//assert(false);
		return;  			}
	l93: {
			goto l94;
		//assert(false);
		return;  			}
	l94: {
		if (__context->AuESP) {
			goto l95;
		}
		if (((! __context->AuESP) && __context->AuPRest)) {
			goto l97;
		}
		if ((((! __context->AuESP) && (! __context->AuPRest)) && ((__context->E_MNewSPR && (! __context->ArmRcpSt)) || ((__context->E_MNewSPR && __context->ArmRcpSt) && __context->E_ActRcp)))) {
			goto l99;
		}
		if (((! __context->AuESP) && ((! ((! __context->AuESP) && __context->AuPRest)) && (! (((! __context->AuESP) && (! __context->AuPRest)) && ((__context->E_MNewSPR && (! __context->ArmRcpSt)) || ((__context->E_MNewSPR && __context->ArmRcpSt) && __context->E_ActRcp))))))) {
			goto l107;
		}
		//assert(false);
		return;  			}
	l95: {
			__context->ActSPR = __context->AuSPR;
			goto l96;
		//assert(false);
		return;  			}
	l96: {
			goto l108;
		//assert(false);
		return;  			}
	l97: {
			__context->ActSPR = __context->DefSP;
			goto l98;
		//assert(false);
		return;  			}
	l98: {
			goto l108;
		//assert(false);
		return;  			}
	l99: {
		if (__context->E_MPSav) {
			goto l100;
		}
		if (((! __context->E_MPSav) && __context->E_MPRest)) {
			goto l102;
		}
		if (((! __context->E_MPSav) && (! ((! __context->E_MPSav) && __context->E_MPRest)))) {
			goto l104;
		}
		//assert(false);
		return;  			}
	l100: {
			__context->DefSP = __context->MSP;
			goto l101;
		//assert(false);
		return;  			}
	l101: {
			goto l106;
		//assert(false);
		return;  			}
	l102: {
			__context->ActSPR = __context->DefSP;
			goto l103;
		//assert(false);
		return;  			}
	l103: {
			goto l106;
		//assert(false);
		return;  			}
	l104: {
			__context->ActSPR = __context->MSP;
			goto l105;
		//assert(false);
		return;  			}
	l105: {
			goto l106;
		//assert(false);
		return;  			}
	l106: {
			goto l108;
		//assert(false);
		return;  			}
	l107: {
			goto l108;
		//assert(false);
		return;  			}
	l108: {
		if (__context->AuPPID.ESPH) {
			goto l109;
		}
		if (((! __context->AuPPID.ESPH) && __context->AuPRest)) {
			goto l111;
		}
		if ((((! __context->AuPPID.ESPH) && (! __context->AuPRest)) && ((__context->E_MNewSPHR && (! __context->ArmRcpSt)) || ((__context->E_MNewSPHR && __context->ArmRcpSt) && __context->E_ActRcp)))) {
			goto l113;
		}
		if (((! __context->AuPPID.ESPH) && ((! ((! __context->AuPPID.ESPH) && __context->AuPRest)) && (! (((! __context->AuPPID.ESPH) && (! __context->AuPRest)) && ((__context->E_MNewSPHR && (! __context->ArmRcpSt)) || ((__context->E_MNewSPHR && __context->ArmRcpSt) && __context->E_ActRcp))))))) {
			goto l121;
		}
		//assert(false);
		return;  			}
	l109: {
			__context->ActSPH = __context->AuPPID.SPH;
			goto l110;
		//assert(false);
		return;  			}
	l110: {
			goto l122;
		//assert(false);
		return;  			}
	l111: {
			__context->ActSPH = __context->DefSPH;
			goto l112;
		//assert(false);
		return;  			}
	l112: {
			goto l122;
		//assert(false);
		return;  			}
	l113: {
		if (__context->E_MPSav) {
			goto l114;
		}
		if (((! __context->E_MPSav) && __context->E_MPRest)) {
			goto l116;
		}
		if (((! __context->E_MPSav) && (! ((! __context->E_MPSav) && __context->E_MPRest)))) {
			goto l118;
		}
		//assert(false);
		return;  			}
	l114: {
			__context->DefSPH = __context->MSPH;
			goto l115;
		//assert(false);
		return;  			}
	l115: {
			goto l120;
		//assert(false);
		return;  			}
	l116: {
			__context->ActSPH = __context->DefSPH;
			goto l117;
		//assert(false);
		return;  			}
	l117: {
			goto l120;
		//assert(false);
		return;  			}
	l118: {
			__context->ActSPH = __context->MSPH;
			goto l119;
		//assert(false);
		return;  			}
	l119: {
			goto l120;
		//assert(false);
		return;  			}
	l120: {
			goto l122;
		//assert(false);
		return;  			}
	l121: {
			goto l122;
		//assert(false);
		return;  			}
	l122: {
		if (((__context->ActSPH > __context->PControl.PMaxRan) || (__context->ActSPH < __context->PControl.PMinRan))) {
			goto l123;
		}
		if ((! ((__context->ActSPH > __context->PControl.PMaxRan) || (__context->ActSPH < __context->PControl.PMinRan)))) {
			goto l125;
		}
		//assert(false);
		return;  			}
	l123: {
			__context->ActSPH = __context->PControl.PMaxRan;
			goto l124;
		//assert(false);
		return;  			}
	l124: {
			goto l126;
		//assert(false);
		return;  			}
	l125: {
			goto l126;
		//assert(false);
		return;  			}
	l126: {
		if (__context->AuPPID.ESPL) {
			goto l127;
		}
		if (((! __context->AuPPID.ESPL) && __context->AuPRest)) {
			goto l129;
		}
		if ((((! __context->AuPPID.ESPL) && (! __context->AuPRest)) && ((__context->E_MNewSPLR && (! __context->ArmRcpSt)) || ((__context->E_MNewSPLR && __context->ArmRcpSt) && __context->E_ActRcp)))) {
			goto l131;
		}
		if (((! __context->AuPPID.ESPL) && ((! ((! __context->AuPPID.ESPL) && __context->AuPRest)) && (! (((! __context->AuPPID.ESPL) && (! __context->AuPRest)) && ((__context->E_MNewSPLR && (! __context->ArmRcpSt)) || ((__context->E_MNewSPLR && __context->ArmRcpSt) && __context->E_ActRcp))))))) {
			goto l139;
		}
		//assert(false);
		return;  			}
	l127: {
			__context->ActSPL = __context->AuPPID.SPL;
			goto l128;
		//assert(false);
		return;  			}
	l128: {
			goto l140;
		//assert(false);
		return;  			}
	l129: {
			__context->ActSPL = __context->DefSPL;
			goto l130;
		//assert(false);
		return;  			}
	l130: {
			goto l140;
		//assert(false);
		return;  			}
	l131: {
		if (__context->E_MPSav) {
			goto l132;
		}
		if (((! __context->E_MPSav) && __context->E_MPRest)) {
			goto l134;
		}
		if (((! __context->E_MPSav) && (! ((! __context->E_MPSav) && __context->E_MPRest)))) {
			goto l136;
		}
		//assert(false);
		return;  			}
	l132: {
			__context->DefSPL = __context->MSPL;
			goto l133;
		//assert(false);
		return;  			}
	l133: {
			goto l138;
		//assert(false);
		return;  			}
	l134: {
			__context->ActSPL = __context->DefSPL;
			goto l135;
		//assert(false);
		return;  			}
	l135: {
			goto l138;
		//assert(false);
		return;  			}
	l136: {
			__context->ActSPL = __context->MSPL;
			goto l137;
		//assert(false);
		return;  			}
	l137: {
			goto l138;
		//assert(false);
		return;  			}
	l138: {
			goto l140;
		//assert(false);
		return;  			}
	l139: {
			goto l140;
		//assert(false);
		return;  			}
	l140: {
		if (((__context->ActSPL < __context->PControl.PMinRan) || (__context->ActSPL > __context->PControl.PMaxRan))) {
			goto l141;
		}
		if ((! ((__context->ActSPL < __context->PControl.PMinRan) || (__context->ActSPL > __context->PControl.PMaxRan)))) {
			goto l143;
		}
		//assert(false);
		return;  			}
	l141: {
			__context->ActSPL = __context->PControl.PMinRan;
			goto l142;
		//assert(false);
		return;  			}
	l142: {
			goto l144;
		//assert(false);
		return;  			}
	l143: {
			goto l144;
		//assert(false);
		return;  			}
	l144: {
		if ((__context->ActSPR > __context->ActSPH)) {
			goto l145;
		}
		if (((! (__context->ActSPR > __context->ActSPH)) && (__context->ActSPR < __context->ActSPL))) {
			goto l147;
		}
		if (((! (__context->ActSPR > __context->ActSPH)) && (! ((! (__context->ActSPR > __context->ActSPH)) && (__context->ActSPR < __context->ActSPL))))) {
			goto l149;
		}
		//assert(false);
		return;  			}
	l145: {
			__context->ActSPR = __context->ActSPH;
			goto l146;
		//assert(false);
		return;  			}
	l146: {
			goto l150;
		//assert(false);
		return;  			}
	l147: {
			__context->ActSPR = __context->ActSPL;
			goto l148;
		//assert(false);
		return;  			}
	l148: {
			goto l150;
		//assert(false);
		return;  			}
	l149: {
			goto l150;
		//assert(false);
		return;  			}
	l150: {
		if (__context->AuPPID.EKc) {
			goto l151;
		}
		if (((! __context->AuPPID.EKc) && __context->AuPRest)) {
			goto l153;
		}
		if ((((! __context->AuPPID.EKc) && (! __context->AuPRest)) && ((__context->E_MNewKcR && (! __context->ArmRcpSt)) || ((__context->E_MNewKcR && __context->ArmRcpSt) && __context->E_ActRcp)))) {
			goto l155;
		}
		if (((! __context->AuPPID.EKc) && ((! ((! __context->AuPPID.EKc) && __context->AuPRest)) && (! (((! __context->AuPPID.EKc) && (! __context->AuPRest)) && ((__context->E_MNewKcR && (! __context->ArmRcpSt)) || ((__context->E_MNewKcR && __context->ArmRcpSt) && __context->E_ActRcp))))))) {
			goto l163;
		}
		//assert(false);
		return;  			}
	l151: {
			__context->ActKc = __context->AuPPID.Kc;
			goto l152;
		//assert(false);
		return;  			}
	l152: {
			goto l164;
		//assert(false);
		return;  			}
	l153: {
			__context->ActKc = __context->DefKc;
			goto l154;
		//assert(false);
		return;  			}
	l154: {
			goto l164;
		//assert(false);
		return;  			}
	l155: {
		if (__context->E_MPSav) {
			goto l156;
		}
		if (((! __context->E_MPSav) && __context->E_MPRest)) {
			goto l158;
		}
		if (((! __context->E_MPSav) && (! ((! __context->E_MPSav) && __context->E_MPRest)))) {
			goto l160;
		}
		//assert(false);
		return;  			}
	l156: {
			__context->DefKc = __context->MKc;
			goto l157;
		//assert(false);
		return;  			}
	l157: {
			goto l162;
		//assert(false);
		return;  			}
	l158: {
			__context->ActKc = __context->DefKc;
			goto l159;
		//assert(false);
		return;  			}
	l159: {
			goto l162;
		//assert(false);
		return;  			}
	l160: {
			__context->ActKc = __context->MKc;
			goto l161;
		//assert(false);
		return;  			}
	l161: {
			goto l162;
		//assert(false);
		return;  			}
	l162: {
			goto l164;
		//assert(false);
		return;  			}
	l163: {
			goto l164;
		//assert(false);
		return;  			}
	l164: {
		if (__context->AuPPID.ETi) {
			goto l165;
		}
		if (((! __context->AuPPID.ETi) && __context->AuPRest)) {
			goto l167;
		}
		if ((((! __context->AuPPID.ETi) && (! __context->AuPRest)) && ((__context->E_MNewTiR && (! __context->ArmRcpSt)) || ((__context->E_MNewTiR && __context->ArmRcpSt) && __context->E_ActRcp)))) {
			goto l169;
		}
		if (((! __context->AuPPID.ETi) && ((! ((! __context->AuPPID.ETi) && __context->AuPRest)) && (! (((! __context->AuPPID.ETi) && (! __context->AuPRest)) && ((__context->E_MNewTiR && (! __context->ArmRcpSt)) || ((__context->E_MNewTiR && __context->ArmRcpSt) && __context->E_ActRcp))))))) {
			goto l177;
		}
		//assert(false);
		return;  			}
	l165: {
			__context->ActTi = __context->AuPPID.Ti;
			goto l166;
		//assert(false);
		return;  			}
	l166: {
			goto l178;
		//assert(false);
		return;  			}
	l167: {
			__context->ActTi = __context->DefTi;
			goto l168;
		//assert(false);
		return;  			}
	l168: {
			goto l178;
		//assert(false);
		return;  			}
	l169: {
		if (__context->E_MPSav) {
			goto l170;
		}
		if (((! __context->E_MPSav) && __context->E_MPRest)) {
			goto l172;
		}
		if (((! __context->E_MPSav) && (! ((! __context->E_MPSav) && __context->E_MPRest)))) {
			goto l174;
		}
		//assert(false);
		return;  			}
	l170: {
			__context->DefTi = __context->MTi;
			goto l171;
		//assert(false);
		return;  			}
	l171: {
			goto l176;
		//assert(false);
		return;  			}
	l172: {
			__context->ActTi = __context->DefTi;
			goto l173;
		//assert(false);
		return;  			}
	l173: {
			goto l176;
		//assert(false);
		return;  			}
	l174: {
			__context->ActTi = __context->MTi;
			goto l175;
		//assert(false);
		return;  			}
	l175: {
			goto l176;
		//assert(false);
		return;  			}
	l176: {
			goto l178;
		//assert(false);
		return;  			}
	l177: {
			goto l178;
		//assert(false);
		return;  			}
	l178: {
		if (__context->AuPPID.ETd) {
			goto l179;
		}
		if (((! __context->AuPPID.ETd) && __context->AuPRest)) {
			goto l181;
		}
		if ((((! __context->AuPPID.ETd) && (! __context->AuPRest)) && ((__context->E_MNewTdR && (! __context->ArmRcpSt)) || ((__context->E_MNewTdR && __context->ArmRcpSt) && __context->E_ActRcp)))) {
			goto l183;
		}
		if (((! __context->AuPPID.ETd) && ((! ((! __context->AuPPID.ETd) && __context->AuPRest)) && (! (((! __context->AuPPID.ETd) && (! __context->AuPRest)) && ((__context->E_MNewTdR && (! __context->ArmRcpSt)) || ((__context->E_MNewTdR && __context->ArmRcpSt) && __context->E_ActRcp))))))) {
			goto l191;
		}
		//assert(false);
		return;  			}
	l179: {
			__context->ActTd = __context->AuPPID.Td;
			goto l180;
		//assert(false);
		return;  			}
	l180: {
			goto l192;
		//assert(false);
		return;  			}
	l181: {
			__context->ActTd = __context->DefTd;
			goto l182;
		//assert(false);
		return;  			}
	l182: {
			goto l192;
		//assert(false);
		return;  			}
	l183: {
		if (__context->E_MPSav) {
			goto l184;
		}
		if (((! __context->E_MPSav) && __context->E_MPRest)) {
			goto l186;
		}
		if (((! __context->E_MPSav) && (! ((! __context->E_MPSav) && __context->E_MPRest)))) {
			goto l188;
		}
		//assert(false);
		return;  			}
	l184: {
			__context->DefTd = __context->MTd;
			goto l185;
		//assert(false);
		return;  			}
	l185: {
			goto l190;
		//assert(false);
		return;  			}
	l186: {
			__context->ActTd = __context->DefTd;
			goto l187;
		//assert(false);
		return;  			}
	l187: {
			goto l190;
		//assert(false);
		return;  			}
	l188: {
			__context->ActTd = __context->MTd;
			goto l189;
		//assert(false);
		return;  			}
	l189: {
			goto l190;
		//assert(false);
		return;  			}
	l190: {
			goto l192;
		//assert(false);
		return;  			}
	l191: {
			goto l192;
		//assert(false);
		return;  			}
	l192: {
		if (__context->AuPPID.ETds) {
			goto l193;
		}
		if (((! __context->AuPPID.ETds) && __context->AuPRest)) {
			goto l195;
		}
		if ((((! __context->AuPPID.ETds) && (! __context->AuPRest)) && ((__context->E_MNewTdsR && (! __context->ArmRcpSt)) || ((__context->E_MNewTdsR && __context->ArmRcpSt) && __context->E_ActRcp)))) {
			goto l197;
		}
		if (((! __context->AuPPID.ETds) && ((! ((! __context->AuPPID.ETds) && __context->AuPRest)) && (! (((! __context->AuPPID.ETds) && (! __context->AuPRest)) && ((__context->E_MNewTdsR && (! __context->ArmRcpSt)) || ((__context->E_MNewTdsR && __context->ArmRcpSt) && __context->E_ActRcp))))))) {
			goto l205;
		}
		//assert(false);
		return;  			}
	l193: {
			__context->ActTds = __context->AuPPID.Tds;
			goto l194;
		//assert(false);
		return;  			}
	l194: {
			goto l206;
		//assert(false);
		return;  			}
	l195: {
			__context->ActTds = __context->DefTds;
			goto l196;
		//assert(false);
		return;  			}
	l196: {
			goto l206;
		//assert(false);
		return;  			}
	l197: {
		if (__context->E_MPSav) {
			goto l198;
		}
		if (((! __context->E_MPSav) && __context->E_MPRest)) {
			goto l200;
		}
		if (((! __context->E_MPSav) && (! ((! __context->E_MPSav) && __context->E_MPRest)))) {
			goto l202;
		}
		//assert(false);
		return;  			}
	l198: {
			__context->DefTds = __context->MTds;
			goto l199;
		//assert(false);
		return;  			}
	l199: {
			goto l204;
		//assert(false);
		return;  			}
	l200: {
			__context->ActTds = __context->DefTds;
			goto l201;
		//assert(false);
		return;  			}
	l201: {
			goto l204;
		//assert(false);
		return;  			}
	l202: {
			__context->ActTds = __context->MTds;
			goto l203;
		//assert(false);
		return;  			}
	l203: {
			goto l204;
		//assert(false);
		return;  			}
	l204: {
			goto l206;
		//assert(false);
		return;  			}
	l205: {
			goto l206;
		//assert(false);
		return;  			}
	l206: {
		if ((__context->HMV >= __context->PControl.PMaxRan)) {
			goto l207;
		}
		if ((! (__context->HMV >= __context->PControl.PMaxRan))) {
			goto l209;
		}
		//assert(false);
		return;  			}
	l207: {
			__context->HMV = __context->PControl.PMaxRan;
			goto l208;
		//assert(false);
		return;  			}
	l208: {
			goto l210;
		//assert(false);
		return;  			}
	l209: {
			goto l210;
		//assert(false);
		return;  			}
	l210: {
		if ((__context->HMV <= __context->PControl.PMinRan)) {
			goto l211;
		}
		if ((! (__context->HMV <= __context->PControl.PMinRan))) {
			goto l213;
		}
		//assert(false);
		return;  			}
	l211: {
			__context->HMV = __context->PControl.PMinRan;
			goto l212;
		//assert(false);
		return;  			}
	l212: {
			goto l214;
		//assert(false);
		return;  			}
	l213: {
			goto l214;
		//assert(false);
		return;  			}
	l214: {
			// Assign inputs
			__context->MVFILTER.INV = __context->HMV;
			__context->MVFILTER.TM_LAG = __context->PControl.MVFiltTime;
			__context->MVFILTER.CYCLE = PID_EXEC_CYCLE;
			LAG1ST(&__context->MVFILTER);
			// Assign outputs
			goto l215;
		//assert(false);
		return;  			}
	l215: {
		if ((__context->ArmRcpSt && __context->E_ActRcp)) {
			goto l216;
		}
		if ((! (__context->ArmRcpSt && __context->E_ActRcp))) {
			goto l218;
		}
		//assert(false);
		return;  			}
	l216: {
			__context->ArmRcpSt = false;
			goto l217;
		//assert(false);
		return;  			}
	l217: {
			goto l219;
		//assert(false);
		return;  			}
	l218: {
			goto l219;
		//assert(false);
		return;  			}
	l219: {
		if (__context->AuPPID.EOutH) {
			goto l220;
		}
		if (((! __context->AuPPID.EOutH) && __context->AuPRest)) {
			goto l222;
		}
		if ((((! __context->AuPPID.EOutH) && (! __context->AuPRest)) && __context->E_MNewOutHR)) {
			goto l224;
		}
		if (((! __context->AuPPID.EOutH) && ((! ((! __context->AuPPID.EOutH) && __context->AuPRest)) && (! (((! __context->AuPPID.EOutH) && (! __context->AuPRest)) && __context->E_MNewOutHR))))) {
			goto l237;
		}
		//assert(false);
		return;  			}
	l220: {
			__context->ActOutH = __context->AuPPID.OutH;
			goto l221;
		//assert(false);
		return;  			}
	l221: {
			goto l238;
		//assert(false);
		return;  			}
	l222: {
			__context->ActOutH = __context->DefOutH;
			goto l223;
		//assert(false);
		return;  			}
	l223: {
			goto l238;
		//assert(false);
		return;  			}
	l224: {
		if ((! __context->ArmRcpSt)) {
			goto l225;
		}
		if (((! (! __context->ArmRcpSt)) && __context->E_ActRcp)) {
			goto l233;
		}
		if (((! (! __context->ArmRcpSt)) && (! ((! (! __context->ArmRcpSt)) && __context->E_ActRcp)))) {
			goto l235;
		}
		//assert(false);
		return;  			}
	l225: {
		if (__context->E_MPSav) {
			goto l226;
		}
		if (((! __context->E_MPSav) && __context->E_MPRest)) {
			goto l228;
		}
		if (((! __context->E_MPSav) && (! ((! __context->E_MPSav) && __context->E_MPRest)))) {
			goto l230;
		}
		//assert(false);
		return;  			}
	l226: {
			__context->DefOutH = __context->MOutH;
			goto l227;
		//assert(false);
		return;  			}
	l227: {
			goto l232;
		//assert(false);
		return;  			}
	l228: {
			__context->ActOutH = __context->DefOutH;
			goto l229;
		//assert(false);
		return;  			}
	l229: {
			goto l232;
		//assert(false);
		return;  			}
	l230: {
			__context->ActOutH = __context->MOutH;
			goto l231;
		//assert(false);
		return;  			}
	l231: {
			goto l232;
		//assert(false);
		return;  			}
	l232: {
			goto l236;
		//assert(false);
		return;  			}
	l233: {
			__context->ActOutH = __context->MOutH;
			goto l234;
		//assert(false);
		return;  			}
	l234: {
			goto l236;
		//assert(false);
		return;  			}
	l235: {
			goto l236;
		//assert(false);
		return;  			}
	l236: {
			goto l238;
		//assert(false);
		return;  			}
	l237: {
			goto l238;
		//assert(false);
		return;  			}
	l238: {
		if (((__context->PControl.ScaMethod == 1) && ((__context->ActOutH > 100.0) || (__context->ActOutH < 0.0)))) {
			goto l239;
		}
		if ((! ((__context->PControl.ScaMethod == 1) && ((__context->ActOutH > 100.0) || (__context->ActOutH < 0.0))))) {
			goto l241;
		}
		//assert(false);
		return;  			}
	l239: {
			__context->ActOutH = 100.0;
			goto l240;
		//assert(false);
		return;  			}
	l240: {
			goto l242;
		//assert(false);
		return;  			}
	l241: {
			goto l242;
		//assert(false);
		return;  			}
	l242: {
		if (((__context->PControl.ScaMethod == 2) && ((__context->ActOutH > __context->PControl.POutMaxRan) || (__context->ActOutH < __context->PControl.POutMinRan)))) {
			goto l243;
		}
		if ((! ((__context->PControl.ScaMethod == 2) && ((__context->ActOutH > __context->PControl.POutMaxRan) || (__context->ActOutH < __context->PControl.POutMinRan))))) {
			goto l245;
		}
		//assert(false);
		return;  			}
	l243: {
			__context->ActOutH = __context->PControl.POutMaxRan;
			goto l244;
		//assert(false);
		return;  			}
	l244: {
			goto l246;
		//assert(false);
		return;  			}
	l245: {
			goto l246;
		//assert(false);
		return;  			}
	l246: {
		if (((__context->PControl.ScaMethod == 3) && ((__context->ActOutH > __context->PControl.POutMaxRan) || (__context->ActOutH < __context->PControl.POutMinRan)))) {
			goto l247;
		}
		if ((! ((__context->PControl.ScaMethod == 3) && ((__context->ActOutH > __context->PControl.POutMaxRan) || (__context->ActOutH < __context->PControl.POutMinRan))))) {
			goto l249;
		}
		//assert(false);
		return;  			}
	l247: {
			__context->ActOutH = __context->PControl.POutMaxRan;
			goto l248;
		//assert(false);
		return;  			}
	l248: {
			goto l250;
		//assert(false);
		return;  			}
	l249: {
			goto l250;
		//assert(false);
		return;  			}
	l250: {
		if (__context->AuPPID.EOutL) {
			goto l251;
		}
		if (((! __context->AuPPID.EOutL) && __context->AuPRest)) {
			goto l253;
		}
		if ((((! __context->AuPPID.EOutL) && (! __context->AuPRest)) && __context->E_MNewOutLR)) {
			goto l255;
		}
		if (((! __context->AuPPID.EOutL) && ((! ((! __context->AuPPID.EOutL) && __context->AuPRest)) && (! (((! __context->AuPPID.EOutL) && (! __context->AuPRest)) && __context->E_MNewOutLR))))) {
			goto l268;
		}
		//assert(false);
		return;  			}
	l251: {
			__context->ActOutL = __context->AuPPID.OutL;
			goto l252;
		//assert(false);
		return;  			}
	l252: {
			goto l269;
		//assert(false);
		return;  			}
	l253: {
			__context->ActOutL = __context->DefOutL;
			goto l254;
		//assert(false);
		return;  			}
	l254: {
			goto l269;
		//assert(false);
		return;  			}
	l255: {
		if ((! __context->ArmRcpSt)) {
			goto l256;
		}
		if (((! (! __context->ArmRcpSt)) && __context->E_ActRcp)) {
			goto l264;
		}
		if (((! (! __context->ArmRcpSt)) && (! ((! (! __context->ArmRcpSt)) && __context->E_ActRcp)))) {
			goto l266;
		}
		//assert(false);
		return;  			}
	l256: {
		if (__context->E_MPSav) {
			goto l257;
		}
		if (((! __context->E_MPSav) && __context->E_MPRest)) {
			goto l259;
		}
		if (((! __context->E_MPSav) && (! ((! __context->E_MPSav) && __context->E_MPRest)))) {
			goto l261;
		}
		//assert(false);
		return;  			}
	l257: {
			__context->DefOutL = __context->MOutL;
			goto l258;
		//assert(false);
		return;  			}
	l258: {
			goto l263;
		//assert(false);
		return;  			}
	l259: {
			__context->ActOutL = __context->DefOutL;
			goto l260;
		//assert(false);
		return;  			}
	l260: {
			goto l263;
		//assert(false);
		return;  			}
	l261: {
			__context->ActOutL = __context->MOutL;
			goto l262;
		//assert(false);
		return;  			}
	l262: {
			goto l263;
		//assert(false);
		return;  			}
	l263: {
			goto l267;
		//assert(false);
		return;  			}
	l264: {
			__context->ActOutL = __context->MOutL;
			goto l265;
		//assert(false);
		return;  			}
	l265: {
			goto l267;
		//assert(false);
		return;  			}
	l266: {
			goto l267;
		//assert(false);
		return;  			}
	l267: {
			goto l269;
		//assert(false);
		return;  			}
	l268: {
			goto l269;
		//assert(false);
		return;  			}
	l269: {
		if (((__context->PControl.ScaMethod == 1) && ((__context->ActOutL > 100.0) || (__context->ActOutL < 0.0)))) {
			goto l270;
		}
		if ((! ((__context->PControl.ScaMethod == 1) && ((__context->ActOutL > 100.0) || (__context->ActOutL < 0.0))))) {
			goto l272;
		}
		//assert(false);
		return;  			}
	l270: {
			__context->ActOutL = 0.0;
			goto l271;
		//assert(false);
		return;  			}
	l271: {
			goto l273;
		//assert(false);
		return;  			}
	l272: {
			goto l273;
		//assert(false);
		return;  			}
	l273: {
		if (((__context->PControl.ScaMethod == 2) && ((__context->ActOutL > __context->PControl.POutMaxRan) || (__context->ActOutL < __context->PControl.POutMinRan)))) {
			goto l274;
		}
		if ((! ((__context->PControl.ScaMethod == 2) && ((__context->ActOutL > __context->PControl.POutMaxRan) || (__context->ActOutL < __context->PControl.POutMinRan))))) {
			goto l276;
		}
		//assert(false);
		return;  			}
	l274: {
			__context->ActOutL = __context->PControl.POutMinRan;
			goto l275;
		//assert(false);
		return;  			}
	l275: {
			goto l277;
		//assert(false);
		return;  			}
	l276: {
			goto l277;
		//assert(false);
		return;  			}
	l277: {
		if (((__context->PControl.ScaMethod == 3) && ((__context->ActOutL > __context->PControl.POutMaxRan) || (__context->ActOutL < __context->PControl.POutMinRan)))) {
			goto l278;
		}
		if ((! ((__context->PControl.ScaMethod == 3) && ((__context->ActOutL > __context->PControl.POutMaxRan) || (__context->ActOutL < __context->PControl.POutMinRan))))) {
			goto l280;
		}
		//assert(false);
		return;  			}
	l278: {
			__context->ActOutL = __context->PControl.POutMinRan;
			goto l279;
		//assert(false);
		return;  			}
	l279: {
			goto l281;
		//assert(false);
		return;  			}
	l280: {
			goto l281;
		//assert(false);
		return;  			}
	l281: {
			__context->AuPosRSt = __context->AuPosR;
			goto l282;
		//assert(false);
		return;  			}
	l282: {
		if (__context->OutPSt) {
			goto l283;
		}
		if ((! __context->OutPSt)) {
			goto l296;
		}
		//assert(false);
		return;  			}
	l283: {
			__context->ActSP = __context->HMV;
			goto l284;
		//assert(false);
		return;  			}
	l284: {
		if (((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt)) {
			goto l285;
		}
		if ((! ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt))) {
			goto l291;
		}
		//assert(false);
		return;  			}
	l285: {
		if (__context->E_MNewPosR) {
			goto l286;
		}
		if ((! __context->E_MNewPosR)) {
			goto l288;
		}
		//assert(false);
		return;  			}
	l286: {
			__context->MPosRSt = __context->MPosR;
			goto l287;
		//assert(false);
		return;  			}
	l287: {
			goto l289;
		//assert(false);
		return;  			}
	l288: {
			goto l289;
		//assert(false);
		return;  			}
	l289: {
			__context->tracking_value = __context->MPosRSt;
			goto l290;
		//assert(false);
		return;  			}
	l290: {
			goto l295;
		//assert(false);
		return;  			}
	l291: {
			__context->AuPosRSt = __context->AuPosR;
			goto l292;
		//assert(false);
		return;  			}
	l292: {
			__context->MPosRSt = __context->AuPosR;
			goto l293;
		//assert(false);
		return;  			}
	l293: {
			__context->tracking_value = __context->AuPosRSt;
			goto l294;
		//assert(false);
		return;  			}
	l294: {
			goto l295;
		//assert(false);
		return;  			}
	l295: {
			goto l297;
		//assert(false);
		return;  			}
	l296: {
			goto l297;
		//assert(false);
		return;  			}
	l297: {
		if (__context->RegSt) {
			goto l298;
		}
		if ((! __context->RegSt)) {
			goto l304;
		}
		//assert(false);
		return;  			}
	l298: {
		if (((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt)) {
			goto l299;
		}
		if ((! ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt))) {
			goto l301;
		}
		//assert(false);
		return;  			}
	l299: {
			__context->MPosRSt = __context->OutOV;
			goto l300;
		//assert(false);
		return;  			}
	l300: {
			goto l303;
		//assert(false);
		return;  			}
	l301: {
			__context->MPosRSt = __context->AuPosR;
			goto l302;
		//assert(false);
		return;  			}
	l302: {
			goto l303;
		//assert(false);
		return;  			}
	l303: {
			goto l305;
		//assert(false);
		return;  			}
	l304: {
			goto l305;
		//assert(false);
		return;  			}
	l305: {
		if (__context->TrSt) {
			goto l306;
		}
		if ((! __context->TrSt)) {
			goto l309;
		}
		//assert(false);
		return;  			}
	l306: {
			__context->ActSP = __context->HMV;
			goto l307;
		//assert(false);
		return;  			}
	l307: {
			__context->tracking_value = __context->HOutO;
			goto l308;
		//assert(false);
		return;  			}
	l308: {
			goto l310;
		//assert(false);
		return;  			}
	l309: {
			goto l310;
		//assert(false);
		return;  			}
	l310: {
		if ((__context->PControl.ScaMethod == 1)) {
			goto l311;
		}
		if ((! (__context->PControl.ScaMethod == 1))) {
			goto l319;
		}
		//assert(false);
		return;  			}
	l311: {
			__context->SPScaled = ((100.0 * (__context->ActSP - __context->PControl.PMinRan)) / (__context->PControl.PMaxRan - __context->PControl.PMinRan));
			goto l312;
		//assert(false);
		return;  			}
	l312: {
			__context->HMVScaled = ((100.0 * (__context->MVFILTER.OUTV - __context->PControl.PMinRan)) / (__context->PControl.PMaxRan - __context->PControl.PMinRan));
			goto l313;
		//assert(false);
		return;  			}
	l313: {
			__context->SPHScaled = ((100.0 * (__context->ActSPH - __context->PControl.PMinRan)) / (__context->PControl.PMaxRan - __context->PControl.PMinRan));
			goto l314;
		//assert(false);
		return;  			}
	l314: {
			__context->SPLScaled = ((100.0 * (__context->ActSPL - __context->PControl.PMinRan)) / (__context->PControl.PMaxRan - __context->PControl.PMinRan));
			goto l315;
		//assert(false);
		return;  			}
	l315: {
			__context->OutHScaled = __context->ActOutH;
			goto l316;
		//assert(false);
		return;  			}
	l316: {
			__context->OutLScaled = __context->ActOutL;
			goto l317;
		//assert(false);
		return;  			}
	l317: {
			__context->tracking_value_Scaled = __context->tracking_value;
			goto l318;
		//assert(false);
		return;  			}
	l318: {
			goto l320;
		//assert(false);
		return;  			}
	l319: {
			goto l320;
		//assert(false);
		return;  			}
	l320: {
		if ((__context->PControl.ScaMethod == 2)) {
			goto l321;
		}
		if ((! (__context->PControl.ScaMethod == 2))) {
			goto l329;
		}
		//assert(false);
		return;  			}
	l321: {
			__context->SPScaled = ((100.0 * (__context->ActSP - __context->PControl.PMinRan)) / (__context->PControl.PMaxRan - __context->PControl.PMinRan));
			goto l322;
		//assert(false);
		return;  			}
	l322: {
			__context->HMVScaled = ((100.0 * (__context->MVFILTER.OUTV - __context->PControl.PMinRan)) / (__context->PControl.PMaxRan - __context->PControl.PMinRan));
			goto l323;
		//assert(false);
		return;  			}
	l323: {
			__context->SPHScaled = ((100.0 * (__context->ActSPH - __context->PControl.PMinRan)) / (__context->PControl.PMaxRan - __context->PControl.PMinRan));
			goto l324;
		//assert(false);
		return;  			}
	l324: {
			__context->SPLScaled = ((100.0 * (__context->ActSPL - __context->PControl.PMinRan)) / (__context->PControl.PMaxRan - __context->PControl.PMinRan));
			goto l325;
		//assert(false);
		return;  			}
	l325: {
			__context->OutHScaled = ((100.0 * (__context->ActOutH - __context->PControl.POutMinRan)) / (__context->PControl.POutMaxRan - __context->PControl.POutMinRan));
			goto l326;
		//assert(false);
		return;  			}
	l326: {
			__context->OutLScaled = ((100.0 * (__context->ActOutL - __context->PControl.POutMinRan)) / (__context->PControl.POutMaxRan - __context->PControl.POutMinRan));
			goto l327;
		//assert(false);
		return;  			}
	l327: {
			__context->tracking_value_Scaled = ((100.0 * (__context->tracking_value - __context->PControl.POutMinRan)) / (__context->PControl.POutMaxRan - __context->PControl.POutMinRan));
			goto l328;
		//assert(false);
		return;  			}
	l328: {
			goto l330;
		//assert(false);
		return;  			}
	l329: {
			goto l330;
		//assert(false);
		return;  			}
	l330: {
		if ((__context->PControl.ScaMethod == 3)) {
			goto l331;
		}
		if ((! (__context->PControl.ScaMethod == 3))) {
			goto l339;
		}
		//assert(false);
		return;  			}
	l331: {
			__context->SPScaled = __context->ActSP;
			goto l332;
		//assert(false);
		return;  			}
	l332: {
			__context->HMVScaled = __context->MVFILTER.OUTV;
			goto l333;
		//assert(false);
		return;  			}
	l333: {
			__context->SPHScaled = __context->ActSPH;
			goto l334;
		//assert(false);
		return;  			}
	l334: {
			__context->SPLScaled = __context->ActSPL;
			goto l335;
		//assert(false);
		return;  			}
	l335: {
			__context->OutHScaled = __context->ActOutH;
			goto l336;
		//assert(false);
		return;  			}
	l336: {
			__context->OutLScaled = __context->ActOutL;
			goto l337;
		//assert(false);
		return;  			}
	l337: {
			__context->tracking_value_Scaled = __context->tracking_value;
			goto l338;
		//assert(false);
		return;  			}
	l338: {
			goto l340;
		//assert(false);
		return;  			}
	l339: {
			goto l340;
		//assert(false);
		return;  			}
	l340: {
		if (__context->PControl.RA) {
			goto l341;
		}
		if ((! __context->PControl.RA)) {
			goto l343;
		}
		//assert(false);
		return;  			}
	l341: {
			__context->dev = (__context->HMVScaled - __context->SPScaled);
			goto l342;
		//assert(false);
		return;  			}
	l342: {
			goto l345;
		//assert(false);
		return;  			}
	l343: {
			__context->dev = (__context->SPScaled - __context->HMVScaled);
			goto l344;
		//assert(false);
		return;  			}
	l344: {
			goto l345;
		//assert(false);
		return;  			}
	l345: {
		if (__context->PID_activation) {
			goto l346;
		}
		if ((! __context->PID_activation)) {
			goto l370;
		}
		//assert(false);
		return;  			}
	l346: {
			__context->PID_activation = false;
			goto l347;
		//assert(false);
		return;  			}
	l347: {
			// Assign inputs
			__context->ROC_LIM.INV = __context->ActSPR;
			__context->ROC_LIM.UPRLM_P = __context->AuSPSpd.InSpd;
			__context->ROC_LIM.DNRLM_P = __context->AuSPSpd.DeSpd;
			__context->ROC_LIM.UPRLM_N = __context->AuSPSpd.InSpd;
			__context->ROC_LIM.DNRLM_N = __context->AuSPSpd.DeSpd;
			__context->ROC_LIM.H_LM = __context->ActSPH;
			__context->ROC_LIM.L_LM = __context->ActSPL;
			__context->ROC_LIM.PV = __context->HMV;
			__context->ROC_LIM.DF_OUTV = 0.0;
			__context->ROC_LIM.DFOUT_ON = false;
			__context->ROC_LIM.TRACK = false;
			__context->ROC_LIM.MAN_ON = __context->tracking_control;
			__context->ROC_LIM.CYCLE = __context->PControl.PIDCycle;
			ROC_LIM(&__context->ROC_LIM);
			// Assign outputs
			goto l348;
		//assert(false);
		return;  			}
	l348: {
			__context->ActSP = __context->ROC_LIM.OUTV;
			goto l349;
		//assert(false);
		return;  			}
	l349: {
			// Assign inputs
			__context->INTEG.INV = ((__context->ActKc * __context->dev) + (__context->ActTi * (__context->PID_Out - __context->PID_calc)));
			__context->INTEG.Ti = ((int32_t) ((int32_t) (__context->ActTi * 1000.0)));
			__context->INTEG.CYCLE = __context->PControl.PIDCycle;
			INTEG(&__context->INTEG);
			// Assign outputs
			goto l350;
		//assert(false);
		return;  			}
	l350: {
			// Assign inputs
			__context->DIF.INV = (__context->ActKc * __context->dev);
			__context->DIF.Td = ((int32_t) ((int32_t) (__context->ActTd * 1000.0)));
			__context->DIF.TM_LAG = ((int32_t) ((int32_t) (__context->ActTds * 1000.0)));
			__context->DIF.CYCLE = __context->PControl.PIDCycle;
			DIF(&__context->DIF);
			// Assign outputs
			goto l351;
		//assert(false);
		return;  			}
	l351: {
			__context->PID_calc = ((((__context->ActKc * __context->dev) + __context->INTEG.OUTV) + __context->DIF.OUTV) + __context->PID_FF);
			goto l352;
		//assert(false);
		return;  			}
	l352: {
		if (__context->tracking_control) {
			goto l353;
		}
		if ((! __context->tracking_control)) {
			goto l361;
		}
		//assert(false);
		return;  			}
	l353: {
		if ((__context->tracking_value_Scaled > __context->OutHScaled)) {
			goto l354;
		}
		if (((! (__context->tracking_value_Scaled > __context->OutHScaled)) && (__context->tracking_value_Scaled < __context->OutLScaled))) {
			goto l356;
		}
		if (((! (__context->tracking_value_Scaled > __context->OutHScaled)) && (! ((! (__context->tracking_value_Scaled > __context->OutHScaled)) && (__context->tracking_value_Scaled < __context->OutLScaled))))) {
			goto l358;
		}
		//assert(false);
		return;  			}
	l354: {
			__context->PID_Out = __context->OutHScaled;
			goto l355;
		//assert(false);
		return;  			}
	l355: {
			goto l360;
		//assert(false);
		return;  			}
	l356: {
			__context->PID_Out = __context->OutLScaled;
			goto l357;
		//assert(false);
		return;  			}
	l357: {
			goto l360;
		//assert(false);
		return;  			}
	l358: {
			__context->PID_Out = __context->tracking_value_Scaled;
			goto l359;
		//assert(false);
		return;  			}
	l359: {
			goto l360;
		//assert(false);
		return;  			}
	l360: {
			goto l369;
		//assert(false);
		return;  			}
	l361: {
		if ((__context->PID_calc > __context->OutHScaled)) {
			goto l362;
		}
		if (((! (__context->PID_calc > __context->OutHScaled)) && (__context->PID_calc < __context->OutLScaled))) {
			goto l364;
		}
		if (((! (__context->PID_calc > __context->OutHScaled)) && (! ((! (__context->PID_calc > __context->OutHScaled)) && (__context->PID_calc < __context->OutLScaled))))) {
			goto l366;
		}
		//assert(false);
		return;  			}
	l362: {
			__context->PID_Out = __context->OutHScaled;
			goto l363;
		//assert(false);
		return;  			}
	l363: {
			goto l368;
		//assert(false);
		return;  			}
	l364: {
			__context->PID_Out = __context->OutLScaled;
			goto l365;
		//assert(false);
		return;  			}
	l365: {
			goto l368;
		//assert(false);
		return;  			}
	l366: {
			__context->PID_Out = __context->PID_calc;
			goto l367;
		//assert(false);
		return;  			}
	l367: {
			goto l368;
		//assert(false);
		return;  			}
	l368: {
			goto l369;
		//assert(false);
		return;  			}
	l369: {
			goto l371;
		//assert(false);
		return;  			}
	l370: {
			goto l371;
		//assert(false);
		return;  			}
	l371: {
		if ((__context->PControl.ScaMethod == 2)) {
			goto l372;
		}
		if ((! (__context->PControl.ScaMethod == 2))) {
			goto l374;
		}
		//assert(false);
		return;  			}
	l372: {
			__context->OutOV = (__context->PControl.POutMinRan + ((__context->PID_Out * (__context->PControl.POutMaxRan - __context->PControl.POutMinRan)) / 100.0));
			goto l373;
		//assert(false);
		return;  			}
	l373: {
			goto l376;
		//assert(false);
		return;  			}
	l374: {
			__context->OutOV = __context->PID_Out;
			goto l375;
		//assert(false);
		return;  			}
	l375: {
			goto l376;
		//assert(false);
		return;  			}
	l376: {
		if (((__context->ActKc != __context->DefKc) && (! __context->AuPPID.EKc))) {
			goto l377;
		}
		if ((! ((__context->ActKc != __context->DefKc) && (! __context->AuPPID.EKc)))) {
			goto l379;
		}
		//assert(false);
		return;  			}
	l377: {
			__context->KcDiDef = true;
			goto l378;
		//assert(false);
		return;  			}
	l378: {
			goto l381;
		//assert(false);
		return;  			}
	l379: {
			__context->KcDiDef = false;
			goto l380;
		//assert(false);
		return;  			}
	l380: {
			goto l381;
		//assert(false);
		return;  			}
	l381: {
		if (((__context->ActTi != __context->DefTi) && (! __context->AuPPID.ETi))) {
			goto l382;
		}
		if ((! ((__context->ActTi != __context->DefTi) && (! __context->AuPPID.ETi)))) {
			goto l384;
		}
		//assert(false);
		return;  			}
	l382: {
			__context->TiDiDef = true;
			goto l383;
		//assert(false);
		return;  			}
	l383: {
			goto l386;
		//assert(false);
		return;  			}
	l384: {
			__context->TiDiDef = false;
			goto l385;
		//assert(false);
		return;  			}
	l385: {
			goto l386;
		//assert(false);
		return;  			}
	l386: {
		if (((__context->ActTd != __context->DefTd) && (! __context->AuPPID.ETd))) {
			goto l387;
		}
		if ((! ((__context->ActTd != __context->DefTd) && (! __context->AuPPID.ETd)))) {
			goto l389;
		}
		//assert(false);
		return;  			}
	l387: {
			__context->TdDiDef = true;
			goto l388;
		//assert(false);
		return;  			}
	l388: {
			goto l391;
		//assert(false);
		return;  			}
	l389: {
			__context->TdDiDef = false;
			goto l390;
		//assert(false);
		return;  			}
	l390: {
			goto l391;
		//assert(false);
		return;  			}
	l391: {
		if (((__context->ActTds != __context->DefTds) && (! __context->AuPPID.ETds))) {
			goto l392;
		}
		if ((! ((__context->ActTds != __context->DefTds) && (! __context->AuPPID.ETds)))) {
			goto l394;
		}
		//assert(false);
		return;  			}
	l392: {
			__context->TdsDiDef = true;
			goto l393;
		//assert(false);
		return;  			}
	l393: {
			goto l396;
		//assert(false);
		return;  			}
	l394: {
			__context->TdsDiDef = false;
			goto l395;
		//assert(false);
		return;  			}
	l395: {
			goto l396;
		//assert(false);
		return;  			}
	l396: {
		if (((__context->ActOutH != __context->DefOutH) && (! __context->AuPPID.EOutH))) {
			goto l397;
		}
		if ((! ((__context->ActOutH != __context->DefOutH) && (! __context->AuPPID.EOutH)))) {
			goto l399;
		}
		//assert(false);
		return;  			}
	l397: {
			__context->OutHDiDef = true;
			goto l398;
		//assert(false);
		return;  			}
	l398: {
			goto l401;
		//assert(false);
		return;  			}
	l399: {
			__context->OutHDiDef = false;
			goto l400;
		//assert(false);
		return;  			}
	l400: {
			goto l401;
		//assert(false);
		return;  			}
	l401: {
		if (((__context->ActOutL != __context->DefOutL) && (! __context->AuPPID.EOutL))) {
			goto l402;
		}
		if ((! ((__context->ActOutL != __context->DefOutL) && (! __context->AuPPID.EOutL)))) {
			goto l404;
		}
		//assert(false);
		return;  			}
	l402: {
			__context->OutLDiDef = true;
			goto l403;
		//assert(false);
		return;  			}
	l403: {
			goto l406;
		//assert(false);
		return;  			}
	l404: {
			__context->OutLDiDef = false;
			goto l405;
		//assert(false);
		return;  			}
	l405: {
			goto l406;
		//assert(false);
		return;  			}
	l406: {
		if (((__context->ActSPH != __context->DefSPH) && (! __context->AuPPID.ESPH))) {
			goto l407;
		}
		if ((! ((__context->ActSPH != __context->DefSPH) && (! __context->AuPPID.ESPH)))) {
			goto l409;
		}
		//assert(false);
		return;  			}
	l407: {
			__context->SPHDiDef = true;
			goto l408;
		//assert(false);
		return;  			}
	l408: {
			goto l411;
		//assert(false);
		return;  			}
	l409: {
			__context->SPHDiDef = false;
			goto l410;
		//assert(false);
		return;  			}
	l410: {
			goto l411;
		//assert(false);
		return;  			}
	l411: {
		if (((__context->ActSPL != __context->DefSPL) && (! __context->AuPPID.ESPL))) {
			goto l412;
		}
		if ((! ((__context->ActSPL != __context->DefSPL) && (! __context->AuPPID.ESPL)))) {
			goto l414;
		}
		//assert(false);
		return;  			}
	l412: {
			__context->SPLDiDef = true;
			goto l413;
		//assert(false);
		return;  			}
	l413: {
			goto l416;
		//assert(false);
		return;  			}
	l414: {
			__context->SPLDiDef = false;
			goto l415;
		//assert(false);
		return;  			}
	l415: {
			goto l416;
		//assert(false);
		return;  			}
	l416: {
			__context->MV = __context->MVFILTER.OUTV;
			goto l417;
		//assert(false);
		return;  			}
	l417: {
			__context->MSPSt = __context->MSP;
			goto l418;
		//assert(false);
		return;  			}
	l418: {
			__context->AuSPSt = __context->AuSPR;
			goto l419;
		//assert(false);
		return;  			}
	l419: {
			__context->AuRegSt = __context->AuRegR;
			goto l420;
		//assert(false);
		return;  			}
	l420: {
			__context->MAuMoR_old = false;
			goto l421;
		//assert(false);
		return;  			}
	l421: {
			__context->MMMoR_old = false;
			goto l422;
		//assert(false);
		return;  			}
	l422: {
			__context->MFoMoR_old = false;
			goto l423;
		//assert(false);
		return;  			}
	l423: {
			__context->MRegR_old = false;
			goto l424;
		//assert(false);
		return;  			}
	l424: {
			__context->MOutPR_old = false;
			goto l425;
		//assert(false);
		return;  			}
	l425: {
			__context->MPRest_old = false;
			goto l426;
		//assert(false);
		return;  			}
	l426: {
			__context->MPDefold = false;
			goto l427;
		//assert(false);
		return;  			}
	l427: {
			__context->MNewPosR_old = false;
			goto l428;
		//assert(false);
		return;  			}
	l428: {
			__context->MNewSPR_old = false;
			goto l429;
		//assert(false);
		return;  			}
	l429: {
			__context->MNewSPHR_old = false;
			goto l430;
		//assert(false);
		return;  			}
	l430: {
			__context->MNewSPLR_old = false;
			goto l431;
		//assert(false);
		return;  			}
	l431: {
			__context->MNewOutHR_old = false;
			goto l432;
		//assert(false);
		return;  			}
	l432: {
			__context->MNewOutLR_old = false;
			goto l433;
		//assert(false);
		return;  			}
	l433: {
			__context->MNewKcR_old = false;
			goto l434;
		//assert(false);
		return;  			}
	l434: {
			__context->MNewTdR_old = false;
			goto l435;
		//assert(false);
		return;  			}
	l435: {
			__context->MNewTiR_old = false;
			goto l436;
		//assert(false);
		return;  			}
	l436: {
			__context->MNewTdsR_old = false;
			goto l437;
		//assert(false);
		return;  			}
	l437: {
			__context->Stsreg01b[8] = __context->SoftLDSt;
			goto x1;
		//assert(false);
		return;  			}
	l438: {
			__context->Stsreg01b[9] = __context->AuActR;
			goto x2;
		//assert(false);
		return;  			}
	l439: {
			__context->Stsreg01b[10] = __context->AuMoSt;
			goto x3;
		//assert(false);
		return;  			}
	l440: {
			__context->Stsreg01b[11] = __context->MMoSt;
			goto x4;
		//assert(false);
		return;  			}
	l441: {
			__context->Stsreg01b[12] = __context->FoMoSt;
			goto x5;
		//assert(false);
		return;  			}
	l442: {
			__context->Stsreg01b[13] = __context->ArmRcpSt;
			goto x6;
		//assert(false);
		return;  			}
	l443: {
			__context->Stsreg01b[14] = __context->IOErrorW;
			goto x7;
		//assert(false);
		return;  			}
	l444: {
			__context->Stsreg01b[15] = __context->IOSimuW;
			goto x8;
		//assert(false);
		return;  			}
	l445: {
			__context->Stsreg01b[0] = __context->AuRegR;
			goto x9;
		//assert(false);
		return;  			}
	l446: {
			__context->Stsreg01b[1] = __context->RegSt;
			goto x10;
		//assert(false);
		return;  			}
	l447: {
			__context->Stsreg01b[2] = __context->TrSt;
			goto x11;
		//assert(false);
		return;  			}
	l448: {
			__context->Stsreg01b[3] = __context->OutPSt;
			goto x12;
		//assert(false);
		return;  			}
	l449: {
			__context->Stsreg01b[4] = __context->AuIhSR;
			goto x13;
		//assert(false);
		return;  			}
	l450: {
			__context->Stsreg01b[5] = __context->AuIhFoMo;
			goto x14;
		//assert(false);
		return;  			}
	l451: {
			__context->Stsreg01b[6] = __context->AuESP;
			goto x15;
		//assert(false);
		return;  			}
	l452: {
			__context->Stsreg01b[7] = __context->AuIhMMo;
			goto x16;
		//assert(false);
		return;  			}
	l453: {
			__context->Stsreg02b[8] = __context->AuPPID.EKc;
			goto x17;
		//assert(false);
		return;  			}
	l454: {
			__context->Stsreg02b[9] = __context->AuPPID.ETi;
			goto x18;
		//assert(false);
		return;  			}
	l455: {
			__context->Stsreg02b[10] = __context->AuPPID.ETd;
			goto x19;
		//assert(false);
		return;  			}
	l456: {
			__context->Stsreg02b[11] = __context->AuPPID.ETds;
			goto x20;
		//assert(false);
		return;  			}
	l457: {
			__context->Stsreg02b[12] = __context->KcDiDef;
			goto x21;
		//assert(false);
		return;  			}
	l458: {
			__context->Stsreg02b[13] = __context->TiDiDef;
			goto x22;
		//assert(false);
		return;  			}
	l459: {
			__context->Stsreg02b[14] = __context->TdDiDef;
			goto x23;
		//assert(false);
		return;  			}
	l460: {
			__context->Stsreg02b[15] = __context->TdsDiDef;
			goto x24;
		//assert(false);
		return;  			}
	l461: {
			__context->Stsreg02b[0] = __context->SPHDiDef;
			goto x25;
		//assert(false);
		return;  			}
	l462: {
			__context->Stsreg02b[1] = __context->SPLDiDef;
			goto x26;
		//assert(false);
		return;  			}
	l463: {
			__context->Stsreg02b[2] = __context->OutHDiDef;
			goto x27;
		//assert(false);
		return;  			}
	l464: {
			__context->Stsreg02b[3] = __context->OutLDiDef;
			goto x28;
		//assert(false);
		return;  			}
	l465: {
			__context->Stsreg02b[4] = __context->AuPPID.ESPH;
			goto x29;
		//assert(false);
		return;  			}
	l466: {
			__context->Stsreg02b[5] = __context->AuPPID.ESPL;
			goto x30;
		//assert(false);
		return;  			}
	l467: {
			__context->Stsreg02b[6] = __context->AuPPID.EOutH;
			goto x31;
		//assert(false);
		return;  			}
	l468: {
			__context->Stsreg02b[7] = __context->AuPPID.EOutL;
			goto x32;
		//assert(false);
		return;  			}
	l469: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	x: {
			// Assign inputs
			R_EDGE1_inlined_1.new = __context->Manreg01b[8];
			R_EDGE1_inlined_1.old = __context->MAuMoR_old;
			R_EDGE(&R_EDGE1_inlined_1);
			// Assign outputs
			__context->MAuMoR_old = R_EDGE1_inlined_1.old;
			__context->E_MAuMoR = R_EDGE1_inlined_1.RET_VAL;
			goto l1;
		//assert(false);
		return;  			}
	varview_refresh: {
			__context->Manreg02b[0] = ((__context->Manreg02 & 256) != 0);
			goto varview_refresh33;
		//assert(false);
		return;  			}
	varview_refresh1: {
			__context->Manreg01b[1] = ((__context->Manreg01 & 512) != 0);
			goto varview_refresh3;
		//assert(false);
		return;  			}
	varview_refresh3: {
			__context->Manreg01b[2] = ((__context->Manreg01 & 1024) != 0);
			goto varview_refresh5;
		//assert(false);
		return;  			}
	varview_refresh5: {
			__context->Manreg01b[3] = ((__context->Manreg01 & 2048) != 0);
			goto varview_refresh7;
		//assert(false);
		return;  			}
	varview_refresh7: {
			__context->Manreg01b[4] = ((__context->Manreg01 & 4096) != 0);
			goto varview_refresh9;
		//assert(false);
		return;  			}
	varview_refresh9: {
			__context->Manreg01b[5] = ((__context->Manreg01 & 8192) != 0);
			goto varview_refresh11;
		//assert(false);
		return;  			}
	varview_refresh11: {
			__context->Manreg01b[6] = ((__context->Manreg01 & 16384) != 0);
			goto varview_refresh13;
		//assert(false);
		return;  			}
	varview_refresh13: {
			__context->Manreg01b[7] = ((__context->Manreg01 & 32768) != 0);
			goto varview_refresh15;
		//assert(false);
		return;  			}
	varview_refresh15: {
			__context->Manreg01b[8] = ((__context->Manreg01 & 1) != 0);
			goto varview_refresh17;
		//assert(false);
		return;  			}
	varview_refresh17: {
			__context->Manreg01b[9] = ((__context->Manreg01 & 2) != 0);
			goto varview_refresh19;
		//assert(false);
		return;  			}
	varview_refresh19: {
			__context->Manreg01b[10] = ((__context->Manreg01 & 4) != 0);
			goto varview_refresh21;
		//assert(false);
		return;  			}
	varview_refresh21: {
			__context->Manreg01b[11] = ((__context->Manreg01 & 8) != 0);
			goto varview_refresh23;
		//assert(false);
		return;  			}
	varview_refresh23: {
			__context->Manreg01b[12] = ((__context->Manreg01 & 16) != 0);
			goto varview_refresh25;
		//assert(false);
		return;  			}
	varview_refresh25: {
			__context->Manreg01b[13] = ((__context->Manreg01 & 32) != 0);
			goto varview_refresh27;
		//assert(false);
		return;  			}
	varview_refresh27: {
			__context->Manreg01b[14] = ((__context->Manreg01 & 64) != 0);
			goto varview_refresh29;
		//assert(false);
		return;  			}
	varview_refresh29: {
			__context->Manreg01b[15] = ((__context->Manreg01 & 128) != 0);
			goto varview_refresh;
		//assert(false);
		return;  			}
	varview_refresh32: {
			__context->Stsreg01b[0] = ((__context->Stsreg01 & 256) != 0);
			goto varview_refresh65;
		//assert(false);
		return;  			}
	varview_refresh33: {
			__context->Manreg02b[1] = ((__context->Manreg02 & 512) != 0);
			goto varview_refresh35;
		//assert(false);
		return;  			}
	varview_refresh35: {
			__context->Manreg02b[2] = ((__context->Manreg02 & 1024) != 0);
			goto varview_refresh37;
		//assert(false);
		return;  			}
	varview_refresh37: {
			__context->Manreg02b[3] = ((__context->Manreg02 & 2048) != 0);
			goto varview_refresh39;
		//assert(false);
		return;  			}
	varview_refresh39: {
			__context->Manreg02b[4] = ((__context->Manreg02 & 4096) != 0);
			goto varview_refresh41;
		//assert(false);
		return;  			}
	varview_refresh41: {
			__context->Manreg02b[5] = ((__context->Manreg02 & 8192) != 0);
			goto varview_refresh43;
		//assert(false);
		return;  			}
	varview_refresh43: {
			__context->Manreg02b[6] = ((__context->Manreg02 & 16384) != 0);
			goto varview_refresh45;
		//assert(false);
		return;  			}
	varview_refresh45: {
			__context->Manreg02b[7] = ((__context->Manreg02 & 32768) != 0);
			goto varview_refresh47;
		//assert(false);
		return;  			}
	varview_refresh47: {
			__context->Manreg02b[8] = ((__context->Manreg02 & 1) != 0);
			goto varview_refresh49;
		//assert(false);
		return;  			}
	varview_refresh49: {
			__context->Manreg02b[9] = ((__context->Manreg02 & 2) != 0);
			goto varview_refresh51;
		//assert(false);
		return;  			}
	varview_refresh51: {
			__context->Manreg02b[10] = ((__context->Manreg02 & 4) != 0);
			goto varview_refresh53;
		//assert(false);
		return;  			}
	varview_refresh53: {
			__context->Manreg02b[11] = ((__context->Manreg02 & 8) != 0);
			goto varview_refresh55;
		//assert(false);
		return;  			}
	varview_refresh55: {
			__context->Manreg02b[12] = ((__context->Manreg02 & 16) != 0);
			goto varview_refresh57;
		//assert(false);
		return;  			}
	varview_refresh57: {
			__context->Manreg02b[13] = ((__context->Manreg02 & 32) != 0);
			goto varview_refresh59;
		//assert(false);
		return;  			}
	varview_refresh59: {
			__context->Manreg02b[14] = ((__context->Manreg02 & 64) != 0);
			goto varview_refresh61;
		//assert(false);
		return;  			}
	varview_refresh61: {
			__context->Manreg02b[15] = ((__context->Manreg02 & 128) != 0);
			goto varview_refresh32;
		//assert(false);
		return;  			}
	varview_refresh64: {
			__context->Stsreg02b[0] = ((__context->Stsreg02 & 256) != 0);
			goto varview_refresh96;
		//assert(false);
		return;  			}
	varview_refresh65: {
			__context->Stsreg01b[1] = ((__context->Stsreg01 & 512) != 0);
			goto varview_refresh67;
		//assert(false);
		return;  			}
	varview_refresh67: {
			__context->Stsreg01b[2] = ((__context->Stsreg01 & 1024) != 0);
			goto varview_refresh69;
		//assert(false);
		return;  			}
	varview_refresh69: {
			__context->Stsreg01b[3] = ((__context->Stsreg01 & 2048) != 0);
			goto varview_refresh71;
		//assert(false);
		return;  			}
	varview_refresh71: {
			__context->Stsreg01b[4] = ((__context->Stsreg01 & 4096) != 0);
			goto varview_refresh73;
		//assert(false);
		return;  			}
	varview_refresh73: {
			__context->Stsreg01b[5] = ((__context->Stsreg01 & 8192) != 0);
			goto varview_refresh75;
		//assert(false);
		return;  			}
	varview_refresh75: {
			__context->Stsreg01b[6] = ((__context->Stsreg01 & 16384) != 0);
			goto varview_refresh77;
		//assert(false);
		return;  			}
	varview_refresh77: {
			__context->Stsreg01b[7] = ((__context->Stsreg01 & 32768) != 0);
			goto varview_refresh79;
		//assert(false);
		return;  			}
	varview_refresh79: {
			__context->Stsreg01b[8] = ((__context->Stsreg01 & 1) != 0);
			goto varview_refresh81;
		//assert(false);
		return;  			}
	varview_refresh81: {
			__context->Stsreg01b[9] = ((__context->Stsreg01 & 2) != 0);
			goto varview_refresh83;
		//assert(false);
		return;  			}
	varview_refresh83: {
			__context->Stsreg01b[10] = ((__context->Stsreg01 & 4) != 0);
			goto varview_refresh85;
		//assert(false);
		return;  			}
	varview_refresh85: {
			__context->Stsreg01b[11] = ((__context->Stsreg01 & 8) != 0);
			goto varview_refresh87;
		//assert(false);
		return;  			}
	varview_refresh87: {
			__context->Stsreg01b[12] = ((__context->Stsreg01 & 16) != 0);
			goto varview_refresh89;
		//assert(false);
		return;  			}
	varview_refresh89: {
			__context->Stsreg01b[13] = ((__context->Stsreg01 & 32) != 0);
			goto varview_refresh91;
		//assert(false);
		return;  			}
	varview_refresh91: {
			__context->Stsreg01b[14] = ((__context->Stsreg01 & 64) != 0);
			goto varview_refresh93;
		//assert(false);
		return;  			}
	varview_refresh93: {
			__context->Stsreg01b[15] = ((__context->Stsreg01 & 128) != 0);
			goto varview_refresh64;
		//assert(false);
		return;  			}
	varview_refresh96: {
			__context->Stsreg02b[1] = ((__context->Stsreg02 & 512) != 0);
			goto varview_refresh98;
		//assert(false);
		return;  			}
	varview_refresh98: {
			__context->Stsreg02b[2] = ((__context->Stsreg02 & 1024) != 0);
			goto varview_refresh100;
		//assert(false);
		return;  			}
	varview_refresh100: {
			__context->Stsreg02b[3] = ((__context->Stsreg02 & 2048) != 0);
			goto varview_refresh102;
		//assert(false);
		return;  			}
	varview_refresh102: {
			__context->Stsreg02b[4] = ((__context->Stsreg02 & 4096) != 0);
			goto varview_refresh104;
		//assert(false);
		return;  			}
	varview_refresh104: {
			__context->Stsreg02b[5] = ((__context->Stsreg02 & 8192) != 0);
			goto varview_refresh106;
		//assert(false);
		return;  			}
	varview_refresh106: {
			__context->Stsreg02b[6] = ((__context->Stsreg02 & 16384) != 0);
			goto varview_refresh108;
		//assert(false);
		return;  			}
	varview_refresh108: {
			__context->Stsreg02b[7] = ((__context->Stsreg02 & 32768) != 0);
			goto varview_refresh110;
		//assert(false);
		return;  			}
	varview_refresh110: {
			__context->Stsreg02b[8] = ((__context->Stsreg02 & 1) != 0);
			goto varview_refresh112;
		//assert(false);
		return;  			}
	varview_refresh112: {
			__context->Stsreg02b[9] = ((__context->Stsreg02 & 2) != 0);
			goto varview_refresh114;
		//assert(false);
		return;  			}
	varview_refresh114: {
			__context->Stsreg02b[10] = ((__context->Stsreg02 & 4) != 0);
			goto varview_refresh116;
		//assert(false);
		return;  			}
	varview_refresh116: {
			__context->Stsreg02b[11] = ((__context->Stsreg02 & 8) != 0);
			goto varview_refresh118;
		//assert(false);
		return;  			}
	varview_refresh118: {
			__context->Stsreg02b[12] = ((__context->Stsreg02 & 16) != 0);
			goto varview_refresh120;
		//assert(false);
		return;  			}
	varview_refresh120: {
			__context->Stsreg02b[13] = ((__context->Stsreg02 & 32) != 0);
			goto varview_refresh122;
		//assert(false);
		return;  			}
	varview_refresh122: {
			__context->Stsreg02b[14] = ((__context->Stsreg02 & 64) != 0);
			goto varview_refresh124;
		//assert(false);
		return;  			}
	varview_refresh124: {
			__context->Stsreg02b[15] = ((__context->Stsreg02 & 128) != 0);
			goto x;
		//assert(false);
		return;  			}
	x1: {
		if (__context->Stsreg01b[8]) {
			__context->Stsreg01 = (__context->Stsreg01 | 1);
			goto l438;
		}
		if ((! __context->Stsreg01b[8])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65534);
			goto l438;
		}
		//assert(false);
		return;  			}
	x2: {
		if (__context->Stsreg01b[9]) {
			__context->Stsreg01 = (__context->Stsreg01 | 2);
			goto l439;
		}
		if ((! __context->Stsreg01b[9])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65533);
			goto l439;
		}
		//assert(false);
		return;  			}
	x3: {
		if (__context->Stsreg01b[10]) {
			__context->Stsreg01 = (__context->Stsreg01 | 4);
			goto l440;
		}
		if ((! __context->Stsreg01b[10])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65531);
			goto l440;
		}
		//assert(false);
		return;  			}
	x4: {
		if (__context->Stsreg01b[11]) {
			__context->Stsreg01 = (__context->Stsreg01 | 8);
			goto l441;
		}
		if ((! __context->Stsreg01b[11])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65527);
			goto l441;
		}
		//assert(false);
		return;  			}
	x5: {
		if (__context->Stsreg01b[12]) {
			__context->Stsreg01 = (__context->Stsreg01 | 16);
			goto l442;
		}
		if ((! __context->Stsreg01b[12])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65519);
			goto l442;
		}
		//assert(false);
		return;  			}
	x6: {
		if (__context->Stsreg01b[13]) {
			__context->Stsreg01 = (__context->Stsreg01 | 32);
			goto l443;
		}
		if ((! __context->Stsreg01b[13])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65503);
			goto l443;
		}
		//assert(false);
		return;  			}
	x7: {
		if (__context->Stsreg01b[14]) {
			__context->Stsreg01 = (__context->Stsreg01 | 64);
			goto l444;
		}
		if ((! __context->Stsreg01b[14])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65471);
			goto l444;
		}
		//assert(false);
		return;  			}
	x8: {
		if (__context->Stsreg01b[15]) {
			__context->Stsreg01 = (__context->Stsreg01 | 128);
			goto l445;
		}
		if ((! __context->Stsreg01b[15])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65407);
			goto l445;
		}
		//assert(false);
		return;  			}
	x9: {
		if (__context->Stsreg01b[0]) {
			__context->Stsreg01 = (__context->Stsreg01 | 256);
			goto l446;
		}
		if ((! __context->Stsreg01b[0])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65279);
			goto l446;
		}
		//assert(false);
		return;  			}
	x10: {
		if (__context->Stsreg01b[1]) {
			__context->Stsreg01 = (__context->Stsreg01 | 512);
			goto l447;
		}
		if ((! __context->Stsreg01b[1])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65023);
			goto l447;
		}
		//assert(false);
		return;  			}
	x11: {
		if (__context->Stsreg01b[2]) {
			__context->Stsreg01 = (__context->Stsreg01 | 1024);
			goto l448;
		}
		if ((! __context->Stsreg01b[2])) {
			__context->Stsreg01 = (__context->Stsreg01 & 64511);
			goto l448;
		}
		//assert(false);
		return;  			}
	x12: {
		if (__context->Stsreg01b[3]) {
			__context->Stsreg01 = (__context->Stsreg01 | 2048);
			goto l449;
		}
		if ((! __context->Stsreg01b[3])) {
			__context->Stsreg01 = (__context->Stsreg01 & 63487);
			goto l449;
		}
		//assert(false);
		return;  			}
	x13: {
		if (__context->Stsreg01b[4]) {
			__context->Stsreg01 = (__context->Stsreg01 | 4096);
			goto l450;
		}
		if ((! __context->Stsreg01b[4])) {
			__context->Stsreg01 = (__context->Stsreg01 & 61439);
			goto l450;
		}
		//assert(false);
		return;  			}
	x14: {
		if (__context->Stsreg01b[5]) {
			__context->Stsreg01 = (__context->Stsreg01 | 8192);
			goto l451;
		}
		if ((! __context->Stsreg01b[5])) {
			__context->Stsreg01 = (__context->Stsreg01 & 57343);
			goto l451;
		}
		//assert(false);
		return;  			}
	x15: {
		if (__context->Stsreg01b[6]) {
			__context->Stsreg01 = (__context->Stsreg01 | 16384);
			goto l452;
		}
		if ((! __context->Stsreg01b[6])) {
			__context->Stsreg01 = (__context->Stsreg01 & 49151);
			goto l452;
		}
		//assert(false);
		return;  			}
	x16: {
		if (__context->Stsreg01b[7]) {
			__context->Stsreg01 = (__context->Stsreg01 | 32768);
			goto l453;
		}
		if ((! __context->Stsreg01b[7])) {
			__context->Stsreg01 = (__context->Stsreg01 & 32767);
			goto l453;
		}
		//assert(false);
		return;  			}
	x17: {
		if (__context->Stsreg02b[8]) {
			__context->Stsreg02 = (__context->Stsreg02 | 1);
			goto l454;
		}
		if ((! __context->Stsreg02b[8])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65534);
			goto l454;
		}
		//assert(false);
		return;  			}
	x18: {
		if (__context->Stsreg02b[9]) {
			__context->Stsreg02 = (__context->Stsreg02 | 2);
			goto l455;
		}
		if ((! __context->Stsreg02b[9])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65533);
			goto l455;
		}
		//assert(false);
		return;  			}
	x19: {
		if (__context->Stsreg02b[10]) {
			__context->Stsreg02 = (__context->Stsreg02 | 4);
			goto l456;
		}
		if ((! __context->Stsreg02b[10])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65531);
			goto l456;
		}
		//assert(false);
		return;  			}
	x20: {
		if (__context->Stsreg02b[11]) {
			__context->Stsreg02 = (__context->Stsreg02 | 8);
			goto l457;
		}
		if ((! __context->Stsreg02b[11])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65527);
			goto l457;
		}
		//assert(false);
		return;  			}
	x21: {
		if (__context->Stsreg02b[12]) {
			__context->Stsreg02 = (__context->Stsreg02 | 16);
			goto l458;
		}
		if ((! __context->Stsreg02b[12])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65519);
			goto l458;
		}
		//assert(false);
		return;  			}
	x22: {
		if (__context->Stsreg02b[13]) {
			__context->Stsreg02 = (__context->Stsreg02 | 32);
			goto l459;
		}
		if ((! __context->Stsreg02b[13])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65503);
			goto l459;
		}
		//assert(false);
		return;  			}
	x23: {
		if (__context->Stsreg02b[14]) {
			__context->Stsreg02 = (__context->Stsreg02 | 64);
			goto l460;
		}
		if ((! __context->Stsreg02b[14])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65471);
			goto l460;
		}
		//assert(false);
		return;  			}
	x24: {
		if (__context->Stsreg02b[15]) {
			__context->Stsreg02 = (__context->Stsreg02 | 128);
			goto l461;
		}
		if ((! __context->Stsreg02b[15])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65407);
			goto l461;
		}
		//assert(false);
		return;  			}
	x25: {
		if (__context->Stsreg02b[0]) {
			__context->Stsreg02 = (__context->Stsreg02 | 256);
			goto l462;
		}
		if ((! __context->Stsreg02b[0])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65279);
			goto l462;
		}
		//assert(false);
		return;  			}
	x26: {
		if (__context->Stsreg02b[1]) {
			__context->Stsreg02 = (__context->Stsreg02 | 512);
			goto l463;
		}
		if ((! __context->Stsreg02b[1])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65023);
			goto l463;
		}
		//assert(false);
		return;  			}
	x27: {
		if (__context->Stsreg02b[2]) {
			__context->Stsreg02 = (__context->Stsreg02 | 1024);
			goto l464;
		}
		if ((! __context->Stsreg02b[2])) {
			__context->Stsreg02 = (__context->Stsreg02 & 64511);
			goto l464;
		}
		//assert(false);
		return;  			}
	x28: {
		if (__context->Stsreg02b[3]) {
			__context->Stsreg02 = (__context->Stsreg02 | 2048);
			goto l465;
		}
		if ((! __context->Stsreg02b[3])) {
			__context->Stsreg02 = (__context->Stsreg02 & 63487);
			goto l465;
		}
		//assert(false);
		return;  			}
	x29: {
		if (__context->Stsreg02b[4]) {
			__context->Stsreg02 = (__context->Stsreg02 | 4096);
			goto l466;
		}
		if ((! __context->Stsreg02b[4])) {
			__context->Stsreg02 = (__context->Stsreg02 & 61439);
			goto l466;
		}
		//assert(false);
		return;  			}
	x30: {
		if (__context->Stsreg02b[5]) {
			__context->Stsreg02 = (__context->Stsreg02 | 8192);
			goto l467;
		}
		if ((! __context->Stsreg02b[5])) {
			__context->Stsreg02 = (__context->Stsreg02 & 57343);
			goto l467;
		}
		//assert(false);
		return;  			}
	x31: {
		if (__context->Stsreg02b[6]) {
			__context->Stsreg02 = (__context->Stsreg02 | 16384);
			goto l468;
		}
		if ((! __context->Stsreg02b[6])) {
			__context->Stsreg02 = (__context->Stsreg02 & 49151);
			goto l468;
		}
		//assert(false);
		return;  			}
	x32: {
		if (__context->Stsreg02b[7]) {
			__context->Stsreg02 = (__context->Stsreg02 | 32768);
			goto l469;
		}
		if ((! __context->Stsreg02b[7])) {
			__context->Stsreg02 = (__context->Stsreg02 & 32767);
			goto l469;
		}
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void ROC_LIM(__ROC_LIM *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			__context->OUTV = __context->INV;
			goto l1100;
		//assert(false);
		return;  			}
	l1100: {
		if ((__context->OUTV > __context->H_LM)) {
			goto l2100;
		}
		if (((! (__context->OUTV > __context->H_LM)) && (__context->OUTV < __context->L_LM))) {
			goto l470;
		}
		if (((! (__context->OUTV > __context->H_LM)) && (! ((! (__context->OUTV > __context->H_LM)) && (__context->OUTV < __context->L_LM))))) {
			goto l610;
		}
		//assert(false);
		return;  			}
	l2100: {
			__context->OUTV = __context->H_LM;
			goto l3100;
		//assert(false);
		return;  			}
	l3100: {
			goto l710;
		//assert(false);
		return;  			}
	l470: {
			__context->OUTV = __context->L_LM;
			goto l510;
		//assert(false);
		return;  			}
	l510: {
			goto l710;
		//assert(false);
		return;  			}
	l610: {
			goto l710;
		//assert(false);
		return;  			}
	l710: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void LAG1ST(__LAG1ST *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init2;
	init2: {
			__context->OUTV = __context->INV;
			goto l1101;
		//assert(false);
		return;  			}
	l1101: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void INTEG(__INTEG *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init3;
	init3: {
			__context->OUTV = (__context->OUTV + ((__context->INV * ((float) ((int32_t) __context->CYCLE))) / ((float) ((int32_t) __context->Ti))));
			goto l1102;
		//assert(false);
		return;  			}
	l1102: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void DIF(__DIF *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init4;
	init4: {
			__context->FiltINV = (((((float) ((int32_t) __context->CYCLE)) * __context->INV) + (((float) ((int32_t) __context->Tds)) * __context->LastFiltINV)) / (((float) ((int32_t) __context->CYCLE)) + ((float) ((int32_t) __context->Tds))));
			goto l1103;
		//assert(false);
		return;  			}
	l1103: {
			__context->OUTV = ((((float) ((int32_t) __context->Td)) * (__context->FiltINV - __context->LastFiltINV)) / ((float) ((int32_t) __context->CYCLE)));
			goto l2101;
		//assert(false);
		return;  			}
	l2101: {
			__context->LastFiltINV = __context->FiltINV;
			goto l3101;
		//assert(false);
		return;  			}
	l3101: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void R_EDGE(__R_EDGE *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init5;
	init5: {
		if (((__context->new == true) && (__context->old == false))) {
			goto l1104;
		}
		if ((! ((__context->new == true) && (__context->old == false)))) {
			goto l471;
		}
		//assert(false);
		return;  			}
	l1104: {
			__context->RET_VAL = true;
			goto l2102;
		//assert(false);
		return;  			}
	l2102: {
			__context->old = true;
			goto l3102;
		//assert(false);
		return;  			}
	l3102: {
			goto l711;
		//assert(false);
		return;  			}
	l471: {
			__context->RET_VAL = false;
			goto l511;
		//assert(false);
		return;  			}
	l511: {
			__context->old = __context->new;
			goto l611;
		//assert(false);
		return;  			}
	l611: {
			goto l711;
		//assert(false);
		return;  			}
	l711: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init6;
	init6: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			instance.AuActR = nondet_bool();
			instance.AuAuMoR = nondet_bool();
			instance.AuESP = nondet_bool();
			instance.AuIhFoMo = nondet_bool();
			instance.AuIhMMo = nondet_bool();
			instance.AuIhSR = nondet_bool();
			instance.AuOutPR = nondet_bool();
			instance.AuPPID.EKc = nondet_bool();
			instance.AuPPID.EOutH = nondet_bool();
			instance.AuPPID.EOutL = nondet_bool();
			instance.AuPPID.ESPH = nondet_bool();
			instance.AuPPID.ESPL = nondet_bool();
			instance.AuPPID.ETd = nondet_bool();
			instance.AuPPID.ETds = nondet_bool();
			instance.AuPPID.ETi = nondet_bool();
			instance.AuPPID.Kc = nondet_float();
			instance.AuPPID.OutH = nondet_float();
			instance.AuPPID.OutL = nondet_float();
			instance.AuPPID.SPH = nondet_float();
			instance.AuPPID.SPL = nondet_float();
			instance.AuPPID.Td = nondet_float();
			instance.AuPPID.Tds = nondet_float();
			instance.AuPPID.Ti = nondet_float();
			instance.AuPRest = nondet_bool();
			instance.AuPosR = nondet_float();
			instance.AuRegR = nondet_bool();
			instance.AuSPR = nondet_float();
			instance.AuSPSpd.DeSpd = nondet_float();
			instance.AuSPSpd.InSpd = nondet_float();
			instance.AuTrR = nondet_bool();
			instance.HMV = nondet_float();
			instance.HOutO = nondet_float();
			instance.IOError = nondet_bool();
			instance.IOSimu = nondet_bool();
			instance.MKc = nondet_float();
			instance.MOutH = nondet_float();
			instance.MOutL = nondet_float();
			instance.MPosR = nondet_float();
			instance.MSP = nondet_float();
			instance.MSPH = nondet_float();
			instance.MSPL = nondet_float();
			instance.MTd = nondet_float();
			instance.MTds = nondet_float();
			instance.MTi = nondet_float();
			instance.Manreg01 = nondet_uint16_t();
			instance.Manreg01b[0] = nondet_bool();
			instance.Manreg01b[10] = nondet_bool();
			instance.Manreg01b[11] = nondet_bool();
			instance.Manreg01b[12] = nondet_bool();
			instance.Manreg01b[13] = nondet_bool();
			instance.Manreg01b[14] = nondet_bool();
			instance.Manreg01b[15] = nondet_bool();
			instance.Manreg01b[1] = nondet_bool();
			instance.Manreg01b[2] = nondet_bool();
			instance.Manreg01b[3] = nondet_bool();
			instance.Manreg01b[4] = nondet_bool();
			instance.Manreg01b[5] = nondet_bool();
			instance.Manreg01b[6] = nondet_bool();
			instance.Manreg01b[7] = nondet_bool();
			instance.Manreg01b[8] = nondet_bool();
			instance.Manreg01b[9] = nondet_bool();
			instance.Manreg02 = nondet_uint16_t();
			instance.Manreg02b[0] = nondet_bool();
			instance.Manreg02b[10] = nondet_bool();
			instance.Manreg02b[11] = nondet_bool();
			instance.Manreg02b[12] = nondet_bool();
			instance.Manreg02b[13] = nondet_bool();
			instance.Manreg02b[14] = nondet_bool();
			instance.Manreg02b[15] = nondet_bool();
			instance.Manreg02b[1] = nondet_bool();
			instance.Manreg02b[2] = nondet_bool();
			instance.Manreg02b[3] = nondet_bool();
			instance.Manreg02b[4] = nondet_bool();
			instance.Manreg02b[5] = nondet_bool();
			instance.Manreg02b[6] = nondet_bool();
			instance.Manreg02b[7] = nondet_bool();
			instance.Manreg02b[8] = nondet_bool();
			instance.Manreg02b[9] = nondet_bool();
			instance.PControl.MVFiltTime = nondet_int32_t();
			instance.PControl.PIDCycle = nondet_int32_t();
			instance.PControl.PMaxRan = nondet_float();
			instance.PControl.PMinRan = nondet_float();
			instance.PControl.POutMaxRan = nondet_float();
			instance.PControl.POutMinRan = nondet_float();
			instance.PControl.RA = nondet_bool();
			instance.PControl.ScaMethod = nondet_int16_t();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			CPC_FB_PID(&instance);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
int main() {
	// Initial values
	PID_EXEC_CYCLE = 0;
	R_EDGE1.new = false;
	R_EDGE1.old = false;
	R_EDGE1.RET_VAL = false;
	instance.HMV = 0.0;
	instance.HOutO = 0.0;
	instance.IOError = false;
	instance.IOSimu = false;
	instance.AuActR = false;
	instance.AuOutPR = false;
	instance.AuRegR = false;
	instance.AuTrR = false;
	instance.AuAuMoR = false;
	instance.AuIhSR = false;
	instance.AuPosR = 0.0;
	instance.AuSPR = 0.0;
	instance.AuESP = false;
	instance.AuIhMMo = false;
	instance.AuIhFoMo = false;
	instance.AuSPSpd.InSpd = 0.0;
	instance.AuSPSpd.DeSpd = 0.0;
	instance.AuPRest = false;
	instance.AuPPID.Kc = 0.0;
	instance.AuPPID.Ti = 0.0;
	instance.AuPPID.Td = 0.0;
	instance.AuPPID.Tds = 0.0;
	instance.AuPPID.SPH = 0.0;
	instance.AuPPID.SPL = 0.0;
	instance.AuPPID.OutH = 0.0;
	instance.AuPPID.OutL = 0.0;
	instance.AuPPID.EKc = false;
	instance.AuPPID.ETi = false;
	instance.AuPPID.ETd = false;
	instance.AuPPID.ETds = false;
	instance.AuPPID.ESPH = false;
	instance.AuPPID.ESPL = false;
	instance.AuPPID.EOutH = false;
	instance.AuPPID.EOutL = false;
	instance.Manreg01 = 0;
	instance.Manreg02 = 0;
	instance.Manreg01b[0] = false;
	instance.Manreg01b[1] = false;
	instance.Manreg01b[2] = false;
	instance.Manreg01b[3] = false;
	instance.Manreg01b[4] = false;
	instance.Manreg01b[5] = false;
	instance.Manreg01b[6] = false;
	instance.Manreg01b[7] = false;
	instance.Manreg01b[8] = false;
	instance.Manreg01b[9] = false;
	instance.Manreg01b[10] = false;
	instance.Manreg01b[11] = false;
	instance.Manreg01b[12] = false;
	instance.Manreg01b[13] = false;
	instance.Manreg01b[14] = false;
	instance.Manreg01b[15] = false;
	instance.Manreg02b[0] = false;
	instance.Manreg02b[1] = false;
	instance.Manreg02b[2] = false;
	instance.Manreg02b[3] = false;
	instance.Manreg02b[4] = false;
	instance.Manreg02b[5] = false;
	instance.Manreg02b[6] = false;
	instance.Manreg02b[7] = false;
	instance.Manreg02b[8] = false;
	instance.Manreg02b[9] = false;
	instance.Manreg02b[10] = false;
	instance.Manreg02b[11] = false;
	instance.Manreg02b[12] = false;
	instance.Manreg02b[13] = false;
	instance.Manreg02b[14] = false;
	instance.Manreg02b[15] = false;
	instance.MPosR = 0.0;
	instance.MSP = 0.0;
	instance.MSPH = 0.0;
	instance.MSPL = 0.0;
	instance.MOutH = 0.0;
	instance.MOutL = 0.0;
	instance.MKc = 0.0;
	instance.MTd = 0.0;
	instance.MTi = 0.0;
	instance.MTds = 0.0;
	instance.PControl.PMinRan = 0.0;
	instance.PControl.PMaxRan = 0.0;
	instance.PControl.POutMinRan = 0.0;
	instance.PControl.POutMaxRan = 0.0;
	instance.PControl.MVFiltTime = 0;
	instance.PControl.PIDCycle = 0;
	instance.PControl.ScaMethod = 0;
	instance.PControl.RA = false;
	instance.OutOV = 0.0;
	instance.AuRegSt = false;
	instance.Stsreg01 = 0;
	instance.Stsreg01b[0] = false;
	instance.Stsreg01b[1] = false;
	instance.Stsreg01b[2] = false;
	instance.Stsreg01b[3] = false;
	instance.Stsreg01b[4] = false;
	instance.Stsreg01b[5] = false;
	instance.Stsreg01b[6] = false;
	instance.Stsreg01b[7] = false;
	instance.Stsreg01b[8] = false;
	instance.Stsreg01b[9] = false;
	instance.Stsreg01b[10] = false;
	instance.Stsreg01b[11] = false;
	instance.Stsreg01b[12] = false;
	instance.Stsreg01b[13] = false;
	instance.Stsreg01b[14] = false;
	instance.Stsreg01b[15] = false;
	instance.Stsreg02 = 0;
	instance.Stsreg02b[0] = false;
	instance.Stsreg02b[1] = false;
	instance.Stsreg02b[2] = false;
	instance.Stsreg02b[3] = false;
	instance.Stsreg02b[4] = false;
	instance.Stsreg02b[5] = false;
	instance.Stsreg02b[6] = false;
	instance.Stsreg02b[7] = false;
	instance.Stsreg02b[8] = false;
	instance.Stsreg02b[9] = false;
	instance.Stsreg02b[10] = false;
	instance.Stsreg02b[11] = false;
	instance.Stsreg02b[12] = false;
	instance.Stsreg02b[13] = false;
	instance.Stsreg02b[14] = false;
	instance.Stsreg02b[15] = false;
	instance.AuMoSt = false;
	instance.MMoSt = false;
	instance.FoMoSt = false;
	instance.SoftLDSt = false;
	instance.RegSt = false;
	instance.OutPSt = false;
	instance.TrSt = false;
	instance.IOErrorW = false;
	instance.IOSimuW = false;
	instance.ActSP = 0.0;
	instance.MSPSt = 0.0;
	instance.AuSPSt = 0.0;
	instance.MPosRSt = 0.0;
	instance.AuPosRSt = 0.0;
	instance.ActKc = 0.0;
	instance.ActTi = 0.0;
	instance.ActTd = 0.0;
	instance.ActTds = 0.0;
	instance.ActSPH = 0.0;
	instance.ActSPL = 0.0;
	instance.ActOutH = 0.0;
	instance.ActOutL = 0.0;
	instance.MV = 0.0;
	instance.DefKc = 0.0;
	instance.DefTd = 0.0;
	instance.DefTi = 0.0;
	instance.DefTds = 0.0;
	instance.DefSPH = 0.0;
	instance.DefSPL = 0.0;
	instance.DefOutH = 0.0;
	instance.DefOutL = 0.0;
	instance.DefSP = 0.0;
	instance.E_MAuMoR = false;
	instance.E_MMMoR = false;
	instance.E_MFoMoR = false;
	instance.E_MPRest = false;
	instance.E_MPSav = false;
	instance.E_MNewPosR = false;
	instance.E_MNewSPR = false;
	instance.E_MNewSPHR = false;
	instance.E_MNewSPLR = false;
	instance.E_MNewOutHR = false;
	instance.E_MNewOutLR = false;
	instance.E_MNewKcR = false;
	instance.E_MNewTdR = false;
	instance.E_MNewTiR = false;
	instance.E_MNewTdsR = false;
	instance.E_MRegR = false;
	instance.E_MOutPR = false;
	instance.E_AuAuMoR = false;
	instance.E_ArmRcp = false;
	instance.E_ActRcp = false;
	instance.E_MSoftLDR = false;
	instance.MAuMoR_old = false;
	instance.MMMoR_old = false;
	instance.MFoMoR_old = false;
	instance.MPRest_old = false;
	instance.MPDefold = false;
	instance.MNewPosR_old = false;
	instance.MNewSPR_old = false;
	instance.MNewSPHR_old = false;
	instance.MNewSPLR_old = false;
	instance.MNewOutHR_old = false;
	instance.MNewOutLR_old = false;
	instance.MNewKcR_old = false;
	instance.MNewTdR_old = false;
	instance.MNewTiR_old = false;
	instance.MNewTdsR_old = false;
	instance.MRegR_old = false;
	instance.MOutPR_old = false;
	instance.AuAuMoR_old = false;
	instance.ArmRcp_old = false;
	instance.ActRcp_old = false;
	instance.MSoftLDR_old = false;
	instance.KcDiDef = false;
	instance.TiDiDef = false;
	instance.TdDiDef = false;
	instance.TdsDiDef = false;
	instance.SPHDiDef = false;
	instance.SPLDiDef = false;
	instance.OutHDiDef = false;
	instance.OutLDiDef = false;
	instance.ArmRcpSt = false;
	instance.last_RegSt = false;
	instance.SPScaled = 0.0;
	instance.HMVScaled = 0.0;
	instance.SPHScaled = 0.0;
	instance.SPLScaled = 0.0;
	instance.OutHScaled = 0.0;
	instance.OutLScaled = 0.0;
	instance.tracking_control = false;
	instance.tracking_value = 0.0;
	instance.tracking_value_Scaled = 0.0;
	instance.ActSPR = 0.0;
	instance.dev = 0.0;
	instance.PID_Out = 0.0;
	instance.PID_activation = false;
	instance.ROC_LIM.INV = 0.0;
	instance.ROC_LIM.UPRLM_P = 10.0;
	instance.ROC_LIM.DNRLM_P = 10.0;
	instance.ROC_LIM.UPRLM_N = 10.0;
	instance.ROC_LIM.DNRLM_N = 10.0;
	instance.ROC_LIM.H_LM = 100.0;
	instance.ROC_LIM.L_LM = 0.0;
	instance.ROC_LIM.PV = 0.0;
	instance.ROC_LIM.DF_OUTV = 0.0;
	instance.ROC_LIM.DFOUT_ON = false;
	instance.ROC_LIM.TRACK = false;
	instance.ROC_LIM.MAN_ON = false;
	instance.ROC_LIM.COM_RST = false;
	instance.ROC_LIM.CYCLE = 1000;
	instance.ROC_LIM.OUTV = 0.0;
	instance.ROC_LIM.QUPRLM_P = false;
	instance.ROC_LIM.QDNRLM_P = false;
	instance.ROC_LIM.QUPRLM_N = false;
	instance.ROC_LIM.QDNRLM_N = false;
	instance.ROC_LIM.QH_LM = false;
	instance.ROC_LIM.QL_LM = false;
	instance.MVFILTER.INV = 0.0;
	instance.MVFILTER.TM_LAG = 25000;
	instance.MVFILTER.DF_OUTV = 0.0;
	instance.MVFILTER.TRACK = false;
	instance.MVFILTER.DFOUT_ON = false;
	instance.MVFILTER.COM_RST = false;
	instance.MVFILTER.CYCLE = 1000;
	instance.MVFILTER.OUTV = 0.0;
	instance.INTEG.INV = 0.0;
	instance.INTEG.Ti = 0;
	instance.INTEG.CYCLE = 0;
	instance.INTEG.OUTV = 0.0;
	instance.DIF.INV = 0.0;
	instance.DIF.Td = 0;
	instance.DIF.Tds = 0;
	instance.DIF.CYCLE = 0;
	instance.DIF.TM_LAG = 0;
	instance.DIF.OUTV = 0.0;
	instance.DIF.FiltINV = 0.0;
	instance.DIF.LastFiltINV = 0.0;
	instance.PID_FF = 0.0;
	instance.PID_calc = 0.0;
	R_EDGE1_inlined_1.new = false;
	R_EDGE1_inlined_1.old = false;
	R_EDGE1_inlined_1.RET_VAL = false;
	R_EDGE1_inlined_2.new = false;
	R_EDGE1_inlined_2.old = false;
	R_EDGE1_inlined_2.RET_VAL = false;
	R_EDGE1_inlined_3.new = false;
	R_EDGE1_inlined_3.old = false;
	R_EDGE1_inlined_3.RET_VAL = false;
	R_EDGE1_inlined_4.new = false;
	R_EDGE1_inlined_4.old = false;
	R_EDGE1_inlined_4.RET_VAL = false;
	R_EDGE1_inlined_5.new = false;
	R_EDGE1_inlined_5.old = false;
	R_EDGE1_inlined_5.RET_VAL = false;
	R_EDGE1_inlined_6.new = false;
	R_EDGE1_inlined_6.old = false;
	R_EDGE1_inlined_6.RET_VAL = false;
	R_EDGE1_inlined_7.new = false;
	R_EDGE1_inlined_7.old = false;
	R_EDGE1_inlined_7.RET_VAL = false;
	R_EDGE1_inlined_8.new = false;
	R_EDGE1_inlined_8.old = false;
	R_EDGE1_inlined_8.RET_VAL = false;
	R_EDGE1_inlined_9.new = false;
	R_EDGE1_inlined_9.old = false;
	R_EDGE1_inlined_9.RET_VAL = false;
	R_EDGE1_inlined_10.new = false;
	R_EDGE1_inlined_10.old = false;
	R_EDGE1_inlined_10.RET_VAL = false;
	R_EDGE1_inlined_11.new = false;
	R_EDGE1_inlined_11.old = false;
	R_EDGE1_inlined_11.RET_VAL = false;
	R_EDGE1_inlined_12.new = false;
	R_EDGE1_inlined_12.old = false;
	R_EDGE1_inlined_12.RET_VAL = false;
	R_EDGE1_inlined_13.new = false;
	R_EDGE1_inlined_13.old = false;
	R_EDGE1_inlined_13.RET_VAL = false;
	R_EDGE1_inlined_14.new = false;
	R_EDGE1_inlined_14.old = false;
	R_EDGE1_inlined_14.RET_VAL = false;
	R_EDGE1_inlined_15.new = false;
	R_EDGE1_inlined_15.old = false;
	R_EDGE1_inlined_15.RET_VAL = false;
	R_EDGE1_inlined_16.new = false;
	R_EDGE1_inlined_16.old = false;
	R_EDGE1_inlined_16.RET_VAL = false;
	R_EDGE1_inlined_17.new = false;
	R_EDGE1_inlined_17.old = false;
	R_EDGE1_inlined_17.RET_VAL = false;
	R_EDGE1_inlined_18.new = false;
	R_EDGE1_inlined_18.old = false;
	R_EDGE1_inlined_18.RET_VAL = false;
	R_EDGE1_inlined_19.new = false;
	R_EDGE1_inlined_19.old = false;
	R_EDGE1_inlined_19.RET_VAL = false;
	R_EDGE1_inlined_20.new = false;
	R_EDGE1_inlined_20.old = false;
	R_EDGE1_inlined_20.RET_VAL = false;
	R_EDGE1_inlined_21.new = false;
	R_EDGE1_inlined_21.old = false;
	R_EDGE1_inlined_21.RET_VAL = false;
	__assertion_error = 0;
	
	VerificationLoop();
}
