#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Root data structure
typedef struct {
	bool s1;
	int32_t s2;
} __anonymous_type;
typedef struct {
	bool a1;
	bool a2;
	bool a3;
	int16_t b1;
	int16_t b2;
	int16_t b3;
	int16_t b4;
} __variables_fb1;
typedef struct {
	bool arr1[8];
	bool arr2[8][4];
	int16_t arr3[8];
	__anonymous_type arr4[5];
} __variables_fb2;

// Global variables
__variables_fb1 variables_db1;
__variables_fb2 variables_db2;
uint16_t __assertion_error;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Automata declarations
void variables_fb1(__variables_fb1 *__context);
void variables_fb2(__variables_fb2 *__context);
void variables_OB1();
void VerificationLoop();

// Automata
void variables_fb1(__variables_fb1 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			goto end;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void variables_fb2(__variables_fb2 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			goto end1;
		//assert(false);
		return;  			}
	end1: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void variables_OB1() {
	// Temporary variables
	
	// Start with initial location
	goto init2;
	init2: {
			// Assign inputs
			variables_fb1(&variables_db1);
			// Assign outputs
			goto l1;
		//assert(false);
		return;  			}
	l1: {
			// Assign inputs
			variables_fb2(&variables_db2);
			// Assign outputs
			goto l2;
		//assert(false);
		return;  			}
	l2: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init3;
	init3: {
			goto loop_start;
		//assert(false);
		return;  			}
	end2: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end2;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			variables_OB1();
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
int main() {
	// Initial values
	variables_db1.a1 = false;
	variables_db1.a2 = false;
	variables_db1.a3 = true;
	variables_db1.b1 = 0;
	variables_db1.b2 = 0;
	variables_db1.b3 = 123;
	variables_db1.b4 = -234;
	variables_db2.arr1[0] = false;
	variables_db2.arr1[1] = false;
	variables_db2.arr1[2] = false;
	variables_db2.arr1[3] = false;
	variables_db2.arr1[4] = false;
	variables_db2.arr1[5] = false;
	variables_db2.arr1[6] = false;
	variables_db2.arr1[7] = false;
	variables_db2.arr2[0][1] = false;
	variables_db2.arr2[0][2] = false;
	variables_db2.arr2[0][3] = false;
	variables_db2.arr2[1][1] = false;
	variables_db2.arr2[1][2] = false;
	variables_db2.arr2[1][3] = false;
	variables_db2.arr2[2][1] = false;
	variables_db2.arr2[2][2] = false;
	variables_db2.arr2[2][3] = false;
	variables_db2.arr2[3][1] = false;
	variables_db2.arr2[3][2] = false;
	variables_db2.arr2[3][3] = false;
	variables_db2.arr2[4][1] = false;
	variables_db2.arr2[4][2] = false;
	variables_db2.arr2[4][3] = false;
	variables_db2.arr2[5][1] = false;
	variables_db2.arr2[5][2] = false;
	variables_db2.arr2[5][3] = false;
	variables_db2.arr2[6][1] = false;
	variables_db2.arr2[6][2] = false;
	variables_db2.arr2[6][3] = false;
	variables_db2.arr2[7][1] = false;
	variables_db2.arr2[7][2] = false;
	variables_db2.arr2[7][3] = false;
	variables_db2.arr3[0] = 0;
	variables_db2.arr3[1] = 0;
	variables_db2.arr3[2] = 0;
	variables_db2.arr3[3] = 0;
	variables_db2.arr3[4] = 0;
	variables_db2.arr3[5] = 0;
	variables_db2.arr3[6] = 0;
	variables_db2.arr3[7] = 0;
	__assertion_error = 0;
	
	VerificationLoop();
}
