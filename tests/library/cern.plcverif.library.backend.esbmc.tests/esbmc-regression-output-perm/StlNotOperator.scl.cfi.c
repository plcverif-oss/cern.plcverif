#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Variables
uint16_t NotTest_in = 0;
bool NotTest_inbool = false;
uint16_t NotTest_out = 0;
bool NotTest_outbool = false;
uint16_t __assertion_error = 0;
uint16_t __assertion_error1 = 0;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto verificationLoop_VerificationLoop_loop_start;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error1 == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_end: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_loop_start: {
			NotTest_in = nondet_uint16_t();
			NotTest_inbool = nondet_bool();
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_prepare_EoC: {
			goto verificationLoop_VerificationLoop_loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l4: {
			NotTest_out = ((uint16_t) ((65535 ^ (((int32_t) NotTest_in) & 65535)) | 0));
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l7: {
		if (((! (! (((uint16_t)~ NotTest_in) == NotTest_out))) && (! (! (((uint16_t)~ (NotTest_in & NotTest_in)) == NotTest_out))))) {
			NotTest_out = ((uint16_t) ((int32_t) 0));
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l10;
		}
		if ((! (((uint16_t)~ NotTest_in) == NotTest_out))) {
			__assertion_error = 1;
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_end;
		}
		if (((! (! (((uint16_t)~ NotTest_in) == NotTest_out))) && (! (((uint16_t)~ (NotTest_in & NotTest_in)) == NotTest_out)))) {
			__assertion_error = 2;
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_end;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l10: {
		if ((! (! (((uint16_t)~ NotTest_out) == 65535)))) {
			NotTest_outbool = (NotTest_inbool != true);
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l21;
		}
		if ((! (((uint16_t)~ NotTest_out) == 65535))) {
			__assertion_error = 3;
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_end;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l21: {
		if ((! (! ((! NotTest_inbool) == NotTest_outbool)))) {
			NotTest_outbool = false;
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l29;
		}
		if ((! ((! NotTest_inbool) == NotTest_outbool))) {
			__assertion_error = 4;
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_end;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l29: {
		if ((! (! (! NotTest_outbool)))) {
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_end;
		}
		if ((! (! NotTest_outbool))) {
			__assertion_error = 6;
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_end;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_end: {
			goto verificationLoop_VerificationLoop_prepare_EoC;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
