#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Variables
bool MX1_1 = false;
bool IX1_2 = false;
bool MX1_3 = false;
bool MX2_1 = false;
bool IX2_2 = false;
bool IX2_3 = false;
bool MX2_4 = false;
bool MX3_1 = false;
bool IX3_2 = false;
bool IX3_3 = false;
bool IX3_4 = false;
bool MX3_4 = false;
bool MX4_1 = false;
bool IX4_2 = false;
bool IX4_3 = false;
bool IX4_4 = false;
bool MX4_5 = false;
uint16_t __assertion_error = 0;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			IX1_2 = nondet_bool();
			IX2_2 = nondet_bool();
			IX2_3 = nondet_bool();
			IX3_2 = nondet_bool();
			IX3_3 = nondet_bool();
			IX3_4 = nondet_bool();
			IX4_2 = nondet_bool();
			IX4_3 = nondet_bool();
			IX4_4 = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			goto verificationLoop_VerificationLoop_assertFunction_OB1_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			goto verificationLoop_VerificationLoop_assertFunction_OB1_init1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
			goto verificationLoop_VerificationLoop_assertFunction_OB1_init2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l3: {
			goto verificationLoop_VerificationLoop_assertFunction_OB1_init3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l4: {
			goto callEnd;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_assertFunction_OB1_init: {
			MX1_1 = true;
			goto verificationLoop_VerificationLoop_assertFunction_OB1_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_assertFunction_OB1_l1: {
		if ((! (! IX1_2))) {
			MX1_3 = true;
			goto verificationLoop_VerificationLoop_assertFunction_OB1_l2;
		}
		if ((! IX1_2)) {
			__assertion_error = 1;
			goto verificationLoop_VerificationLoop_assertFunction_OB1_l2;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_assertFunction_OB1_l2: {
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_assertFunction_OB1_init1: {
			MX2_1 = true;
			goto verificationLoop_VerificationLoop_assertFunction_OB1_l11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_assertFunction_OB1_l11: {
		if ((! (! (IX2_2 || IX2_3)))) {
			MX2_4 = true;
			goto verificationLoop_VerificationLoop_assertFunction_OB1_l21;
		}
		if ((! (IX2_2 || IX2_3))) {
			__assertion_error = 2;
			goto verificationLoop_VerificationLoop_assertFunction_OB1_l21;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_assertFunction_OB1_l21: {
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_assertFunction_OB1_init2: {
			MX3_1 = true;
			goto verificationLoop_VerificationLoop_assertFunction_OB1_l12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_assertFunction_OB1_l12: {
		if ((! (! ((IX3_2 || IX3_3) && IX3_4)))) {
			MX3_4 = true;
			goto verificationLoop_VerificationLoop_assertFunction_OB1_l22;
		}
		if ((! ((IX3_2 || IX3_3) && IX3_4))) {
			__assertion_error = 3;
			goto verificationLoop_VerificationLoop_assertFunction_OB1_l22;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_assertFunction_OB1_l22: {
			goto verificationLoop_VerificationLoop_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_assertFunction_OB1_init3: {
			MX4_1 = true;
			goto verificationLoop_VerificationLoop_assertFunction_OB1_l13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_assertFunction_OB1_l13: {
		if ((! (! (!(IX4_2) || (!(IX4_3) || IX4_4))))) {
			MX4_5 = true;
			goto verificationLoop_VerificationLoop_assertFunction_OB1_l23;
		}
		if ((! (!(IX4_2) || (!(IX4_3) || IX4_4)))) {
			__assertion_error = 4;
			goto verificationLoop_VerificationLoop_assertFunction_OB1_l23;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_assertFunction_OB1_l23: {
			goto verificationLoop_VerificationLoop_l4;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
