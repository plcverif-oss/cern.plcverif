#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Root data structure
typedef struct {
	uint16_t ParReg;
} __CPC_ANALOGALARM_PARAM;
typedef struct {
	float I;
	float PosSt;
	float HHSt;
	float HSt;
	float LSt;
	float LLSt;
	float HH;
	float H;
	float L;
	float LL;
	int16_t PAlDt;
	__CPC_ANALOGALARM_PARAM PAA;
	bool AuAlAck;
	bool ISt;
	bool WSt;
	bool AlUnAck;
	bool MAlBRSt;
	bool AuIhMB;
	bool IOErrorW;
	bool IOSimuW;
	bool IOError;
	bool IOSimu;
	bool AuEHH;
	bool AuEH;
	bool AuEL;
	bool AuELL;
	bool HHAlSt;
	bool HWSt;
	bool LWSt;
	bool LLAlSt;
	bool ConfigW;
	bool EHHSt;
	bool EHSt;
	bool ELSt;
	bool ELLSt;
	bool ArmRcpSt;
	bool PAuAckAl;
	bool MAlBSetRst_old;
	bool MAlAck_old;
	bool AuAlAck_old;
	bool Alarm_Cond_old;
	bool MNewHHR_old;
	bool MNewHR_old;
	bool MNewLR_old;
	bool MNewLLR_old;
	bool ArmRcp_old;
	bool ActRcp_old;
	bool AlUnAck_old;
	bool HH_AlarmPh1;
	bool HH_AlarmPh2;
	bool H_AlarmPh1;
	bool H_AlarmPh2;
	bool L_AlarmPh1;
	bool L_AlarmPh2;
	bool LL_AlarmPh1;
	bool LL_AlarmPh2;
	int32_t HH_TimeAlarm;
	int32_t H_TimeAlarm;
	int32_t L_TimeAlarm;
	int32_t LL_TimeAlarm;
	int32_t TimeAlarm;
	int16_t Iinc;
	int16_t Winc;
	int16_t HHinc;
	int16_t Hinc;
	int16_t Linc;
	int16_t LLinc;
	bool WISt;
	bool WWSt;
	bool WHHAlSt;
	bool WHWSt;
	bool WLWSt;
	bool WLLAlSt;
} __CPC_DB_AA;
typedef struct {
	bool new;
	bool old;
	bool re;
	bool fe;
} __DETECT_EDGE;
typedef struct {
	bool new;
	bool old;
	bool RET_VAL;
} __R_EDGE;
typedef struct {
	bool ParRegb[16];
} __anonymous_type;
typedef struct {
	uint16_t Manreg01;
	bool Manreg01b[16];
	float HH;
	float H;
	float L;
	float LL;
	uint16_t StsReg01;
	uint16_t StsReg02;
	__CPC_DB_AA Perst;
} __CPC_FB_AA;

// Global variables
bool First_Cycle;
int32_t UNICOS_LiveCounter;
int32_t T_CYCLE;
__R_EDGE R_EDGE1;
__DETECT_EDGE DETECT_EDGE1;
__CPC_FB_AA instance;
__R_EDGE R_EDGE1_inlined_1;
__R_EDGE R_EDGE1_inlined_2;
__R_EDGE R_EDGE1_inlined_3;
__R_EDGE R_EDGE1_inlined_4;
__R_EDGE R_EDGE1_inlined_5;
__R_EDGE R_EDGE1_inlined_6;
__R_EDGE R_EDGE1_inlined_7;
__R_EDGE R_EDGE1_inlined_8;
__R_EDGE R_EDGE1_inlined_9;
__R_EDGE R_EDGE1_inlined_10;
uint16_t __assertion_error;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Automata declarations
void CPC_FB_AA(__CPC_FB_AA *__context);
void R_EDGE(__R_EDGE *__context);
void DETECT_EDGE(__DETECT_EDGE *__context);
void VerificationLoop();

// Automata
void CPC_FB_AA(__CPC_FB_AA *__context) {
	// Temporary variables
	bool E_MAlBSetRst;
	bool E_MAlAck;
	bool E_AuAlAck;
	bool E_Alarm_Cond;
	bool E_MNewHHR;
	bool E_MNewHR;
	bool E_MNewLR;
	bool E_MNewLLR;
	bool E_ArmRcp;
	bool E_ActRcp;
	bool RE_AlUnAck;
	bool FE_AlUnAck;
	bool PosHHW;
	bool PosHW;
	bool PosLW;
	bool PosLLW;
	bool ConfigW;
	bool IhMHHSt;
	bool IhMHSt;
	bool IhMLSt;
	bool IhMLLSt;
	bool PAuAckAl;
	uint16_t TempStsReg01;
	bool StsReg01b[16];
	uint16_t TempStsReg02;
	bool StsReg02b[16];
	__CPC_ANALOGALARM_PARAM TempPAA;
	__anonymous_type PAAb;
	float PulseWidth;
	
	// Start with initial location
	goto init;
	init: {
			__context->Manreg01b[0] = ((__context->Manreg01 & 256) != 0);
			__context->Manreg01b[1] = ((__context->Manreg01 & 512) != 0);
			__context->Manreg01b[2] = ((__context->Manreg01 & 1024) != 0);
			__context->Manreg01b[3] = ((__context->Manreg01 & 2048) != 0);
			__context->Manreg01b[4] = ((__context->Manreg01 & 4096) != 0);
			__context->Manreg01b[5] = ((__context->Manreg01 & 8192) != 0);
			__context->Manreg01b[6] = ((__context->Manreg01 & 16384) != 0);
			__context->Manreg01b[7] = ((__context->Manreg01 & 32768) != 0);
			__context->Manreg01b[8] = ((__context->Manreg01 & 1) != 0);
			__context->Manreg01b[9] = ((__context->Manreg01 & 2) != 0);
			__context->Manreg01b[10] = ((__context->Manreg01 & 4) != 0);
			__context->Manreg01b[11] = ((__context->Manreg01 & 8) != 0);
			__context->Manreg01b[12] = ((__context->Manreg01 & 16) != 0);
			__context->Manreg01b[13] = ((__context->Manreg01 & 32) != 0);
			__context->Manreg01b[14] = ((__context->Manreg01 & 64) != 0);
			__context->Manreg01b[15] = ((__context->Manreg01 & 128) != 0);
			StsReg01b[0] = ((TempStsReg01 & 256) != 0);
			StsReg01b[1] = ((TempStsReg01 & 512) != 0);
			StsReg01b[2] = ((TempStsReg01 & 1024) != 0);
			StsReg01b[3] = ((TempStsReg01 & 2048) != 0);
			StsReg01b[4] = ((TempStsReg01 & 4096) != 0);
			StsReg01b[5] = ((TempStsReg01 & 8192) != 0);
			StsReg01b[6] = ((TempStsReg01 & 16384) != 0);
			StsReg01b[7] = ((TempStsReg01 & 32768) != 0);
			StsReg01b[8] = ((TempStsReg01 & 1) != 0);
			StsReg01b[9] = ((TempStsReg01 & 2) != 0);
			StsReg01b[10] = ((TempStsReg01 & 4) != 0);
			StsReg01b[11] = ((TempStsReg01 & 8) != 0);
			StsReg01b[12] = ((TempStsReg01 & 16) != 0);
			StsReg01b[13] = ((TempStsReg01 & 32) != 0);
			StsReg01b[14] = ((TempStsReg01 & 64) != 0);
			StsReg01b[15] = ((TempStsReg01 & 128) != 0);
			StsReg02b[0] = ((TempStsReg02 & 256) != 0);
			StsReg02b[1] = ((TempStsReg02 & 512) != 0);
			StsReg02b[2] = ((TempStsReg02 & 1024) != 0);
			StsReg02b[3] = ((TempStsReg02 & 2048) != 0);
			StsReg02b[4] = ((TempStsReg02 & 4096) != 0);
			StsReg02b[5] = ((TempStsReg02 & 8192) != 0);
			StsReg02b[6] = ((TempStsReg02 & 16384) != 0);
			StsReg02b[7] = ((TempStsReg02 & 32768) != 0);
			StsReg02b[8] = ((TempStsReg02 & 1) != 0);
			StsReg02b[9] = ((TempStsReg02 & 2) != 0);
			StsReg02b[10] = ((TempStsReg02 & 4) != 0);
			StsReg02b[11] = ((TempStsReg02 & 8) != 0);
			StsReg02b[12] = ((TempStsReg02 & 16) != 0);
			StsReg02b[13] = ((TempStsReg02 & 32) != 0);
			StsReg02b[14] = ((TempStsReg02 & 64) != 0);
			StsReg02b[15] = ((TempStsReg02 & 128) != 0);
			PAAb.ParRegb[0] = ((TempPAA.ParReg & 256) != 0);
			PAAb.ParRegb[1] = ((TempPAA.ParReg & 512) != 0);
			PAAb.ParRegb[2] = ((TempPAA.ParReg & 1024) != 0);
			PAAb.ParRegb[3] = ((TempPAA.ParReg & 2048) != 0);
			PAAb.ParRegb[4] = ((TempPAA.ParReg & 4096) != 0);
			PAAb.ParRegb[5] = ((TempPAA.ParReg & 8192) != 0);
			PAAb.ParRegb[6] = ((TempPAA.ParReg & 16384) != 0);
			PAAb.ParRegb[7] = ((TempPAA.ParReg & 32768) != 0);
			PAAb.ParRegb[8] = ((TempPAA.ParReg & 1) != 0);
			PAAb.ParRegb[9] = ((TempPAA.ParReg & 2) != 0);
			PAAb.ParRegb[10] = ((TempPAA.ParReg & 4) != 0);
			PAAb.ParRegb[11] = ((TempPAA.ParReg & 8) != 0);
			PAAb.ParRegb[12] = ((TempPAA.ParReg & 16) != 0);
			PAAb.ParRegb[13] = ((TempPAA.ParReg & 32) != 0);
			PAAb.ParRegb[14] = ((TempPAA.ParReg & 64) != 0);
			PAAb.ParRegb[15] = ((TempPAA.ParReg & 128) != 0);
			goto x;
		//assert(false);
		return;  			}
	l4: {
			// Assign inputs
			R_EDGE1_inlined_1.new = __context->Manreg01b[10];
			R_EDGE1_inlined_1.old = __context->Perst.ArmRcp_old;
			R_EDGE(&R_EDGE1_inlined_1);
			// Assign outputs
			__context->Perst.ArmRcp_old = R_EDGE1_inlined_1.old;
			E_ArmRcp = R_EDGE1_inlined_1.RET_VAL;
			goto l5;
		//assert(false);
		return;  			}
	l5: {
			// Assign inputs
			R_EDGE1_inlined_2.new = __context->Manreg01b[11];
			R_EDGE1_inlined_2.old = __context->Perst.ActRcp_old;
			R_EDGE(&R_EDGE1_inlined_2);
			// Assign outputs
			__context->Perst.ActRcp_old = R_EDGE1_inlined_2.old;
			E_ActRcp = R_EDGE1_inlined_2.RET_VAL;
			goto l6;
		//assert(false);
		return;  			}
	l6: {
			// Assign inputs
			R_EDGE1_inlined_3.new = __context->Manreg01b[0];
			R_EDGE1_inlined_3.old = __context->Perst.MNewHHR_old;
			R_EDGE(&R_EDGE1_inlined_3);
			// Assign outputs
			__context->Perst.MNewHHR_old = R_EDGE1_inlined_3.old;
			E_MNewHHR = R_EDGE1_inlined_3.RET_VAL;
			goto l7;
		//assert(false);
		return;  			}
	l7: {
			// Assign inputs
			R_EDGE1_inlined_4.new = __context->Manreg01b[1];
			R_EDGE1_inlined_4.old = __context->Perst.MNewHR_old;
			R_EDGE(&R_EDGE1_inlined_4);
			// Assign outputs
			__context->Perst.MNewHR_old = R_EDGE1_inlined_4.old;
			E_MNewHR = R_EDGE1_inlined_4.RET_VAL;
			goto l8;
		//assert(false);
		return;  			}
	l8: {
			// Assign inputs
			R_EDGE1_inlined_5.new = __context->Manreg01b[2];
			R_EDGE1_inlined_5.old = __context->Perst.MAlBSetRst_old;
			R_EDGE(&R_EDGE1_inlined_5);
			// Assign outputs
			__context->Perst.MAlBSetRst_old = R_EDGE1_inlined_5.old;
			E_MAlBSetRst = R_EDGE1_inlined_5.RET_VAL;
			goto l9;
		//assert(false);
		return;  			}
	l9: {
			// Assign inputs
			R_EDGE1_inlined_6.new = __context->Manreg01b[4];
			R_EDGE1_inlined_6.old = __context->Perst.MNewLR_old;
			R_EDGE(&R_EDGE1_inlined_6);
			// Assign outputs
			__context->Perst.MNewLR_old = R_EDGE1_inlined_6.old;
			E_MNewLR = R_EDGE1_inlined_6.RET_VAL;
			goto l10;
		//assert(false);
		return;  			}
	l10: {
			// Assign inputs
			R_EDGE1_inlined_7.new = __context->Manreg01b[5];
			R_EDGE1_inlined_7.old = __context->Perst.MNewLLR_old;
			R_EDGE(&R_EDGE1_inlined_7);
			// Assign outputs
			__context->Perst.MNewLLR_old = R_EDGE1_inlined_7.old;
			E_MNewLLR = R_EDGE1_inlined_7.RET_VAL;
			goto l11;
		//assert(false);
		return;  			}
	l11: {
			// Assign inputs
			R_EDGE1_inlined_8.new = __context->Manreg01b[7];
			R_EDGE1_inlined_8.old = __context->Perst.MAlAck_old;
			R_EDGE(&R_EDGE1_inlined_8);
			// Assign outputs
			__context->Perst.MAlAck_old = R_EDGE1_inlined_8.old;
			E_MAlAck = R_EDGE1_inlined_8.RET_VAL;
			goto l12;
		//assert(false);
		return;  			}
	l12: {
			// Assign inputs
			R_EDGE1_inlined_9.new = __context->Perst.AuAlAck;
			R_EDGE1_inlined_9.old = __context->Perst.AuAlAck_old;
			R_EDGE(&R_EDGE1_inlined_9);
			// Assign outputs
			__context->Perst.AuAlAck_old = R_EDGE1_inlined_9.old;
			E_AuAlAck = R_EDGE1_inlined_9.RET_VAL;
			goto l13;
		//assert(false);
		return;  			}
	l13: {
			IhMHHSt = PAAb.ParRegb[8];
			IhMHSt = PAAb.ParRegb[9];
			IhMLSt = PAAb.ParRegb[10];
			IhMLLSt = PAAb.ParRegb[11];
			PAuAckAl = PAAb.ParRegb[12];
			goto l18;
		//assert(false);
		return;  			}
	l18: {
		if (E_MAlBSetRst) {
			__context->Perst.MAlBRSt = (! __context->Perst.MAlBRSt);
			goto l22;
		}
		if ((! E_MAlBSetRst)) {
			goto l22;
		}
		//assert(false);
		return;  			}
	l22: {
		if (__context->Perst.AuIhMB) {
			__context->Perst.MAlBRSt = false;
			goto l26;
		}
		if ((! __context->Perst.AuIhMB)) {
			goto l26;
		}
		//assert(false);
		return;  			}
	l26: {
			__context->Perst.EHHSt = __context->Perst.AuEHH;
			goto l27;
		//assert(false);
		return;  			}
	l27: {
			__context->Perst.EHSt = __context->Perst.AuEH;
			goto l28;
		//assert(false);
		return;  			}
	l28: {
			__context->Perst.ELSt = __context->Perst.AuEL;
			goto l29;
		//assert(false);
		return;  			}
	l29: {
			__context->Perst.ELLSt = __context->Perst.AuELL;
			goto l30;
		//assert(false);
		return;  			}
	l30: {
		if ((E_ArmRcp && (! ((((E_MNewHHR && IhMHHSt) || (E_MNewHR && IhMHSt)) || (E_MNewLR && IhMLSt)) || (E_MNewLLR && IhMLLSt))))) {
			__context->Perst.ArmRcpSt = true;
			goto l34;
		}
		if ((! (E_ArmRcp && (! ((((E_MNewHHR && IhMHHSt) || (E_MNewHR && IhMHSt)) || (E_MNewLR && IhMLSt)) || (E_MNewLLR && IhMLLSt)))))) {
			goto l34;
		}
		//assert(false);
		return;  			}
	l34: {
		if ((E_ArmRcp && E_ActRcp)) {
			__context->Perst.ArmRcpSt = false;
			goto l38;
		}
		if ((! (E_ArmRcp && E_ActRcp))) {
			goto l38;
		}
		//assert(false);
		return;  			}
	l38: {
		if (First_Cycle) {
			__context->Perst.HHSt = __context->HH;
			goto l40;
		}
		if ((! First_Cycle)) {
			goto l45;
		}
		//assert(false);
		return;  			}
	l40: {
			__context->Perst.HSt = __context->H;
			goto l41;
		//assert(false);
		return;  			}
	l41: {
			__context->Perst.LSt = __context->L;
			goto l42;
		//assert(false);
		return;  			}
	l42: {
			__context->Perst.LLSt = __context->LL;
			goto l45;
		//assert(false);
		return;  			}
	l45: {
		if (IhMHHSt) {
			__context->Perst.HHSt = __context->Perst.HH;
			goto l51;
		}
		if (((! IhMHHSt) && ((E_MNewHHR && (! __context->Perst.ArmRcpSt)) || ((E_MNewHHR && __context->Perst.ArmRcpSt) && E_ActRcp)))) {
			__context->Perst.HHSt = __context->HH;
			goto l51;
		}
		if (((! IhMHHSt) && (! ((! IhMHHSt) && ((E_MNewHHR && (! __context->Perst.ArmRcpSt)) || ((E_MNewHHR && __context->Perst.ArmRcpSt) && E_ActRcp)))))) {
			goto l51;
		}
		//assert(false);
		return;  			}
	l51: {
		if (IhMHSt) {
			__context->Perst.HSt = __context->Perst.H;
			goto l57;
		}
		if (((! IhMHSt) && ((E_MNewHR && (! __context->Perst.ArmRcpSt)) || ((E_MNewHR && __context->Perst.ArmRcpSt) && E_ActRcp)))) {
			__context->Perst.HSt = __context->H;
			goto l57;
		}
		if (((! IhMHSt) && (! ((! IhMHSt) && ((E_MNewHR && (! __context->Perst.ArmRcpSt)) || ((E_MNewHR && __context->Perst.ArmRcpSt) && E_ActRcp)))))) {
			goto l57;
		}
		//assert(false);
		return;  			}
	l57: {
		if (IhMLSt) {
			__context->Perst.LSt = __context->Perst.L;
			goto l63;
		}
		if (((! IhMLSt) && ((E_MNewLR && (! __context->Perst.ArmRcpSt)) || ((E_MNewLR && __context->Perst.ArmRcpSt) && E_ActRcp)))) {
			__context->Perst.LSt = __context->L;
			goto l63;
		}
		if (((! IhMLSt) && (! ((! IhMLSt) && ((E_MNewLR && (! __context->Perst.ArmRcpSt)) || ((E_MNewLR && __context->Perst.ArmRcpSt) && E_ActRcp)))))) {
			goto l63;
		}
		//assert(false);
		return;  			}
	l63: {
		if (IhMLLSt) {
			__context->Perst.LLSt = __context->Perst.LL;
			goto l69;
		}
		if (((! IhMLLSt) && ((E_MNewLLR && (! __context->Perst.ArmRcpSt)) || ((E_MNewLLR && __context->Perst.ArmRcpSt) && E_ActRcp)))) {
			__context->Perst.LLSt = __context->LL;
			goto l69;
		}
		if (((! IhMLLSt) && (! ((! IhMLLSt) && ((E_MNewLLR && (! __context->Perst.ArmRcpSt)) || ((E_MNewLLR && __context->Perst.ArmRcpSt) && E_ActRcp)))))) {
			goto l69;
		}
		//assert(false);
		return;  			}
	l69: {
		if ((__context->Perst.EHHSt && (__context->Perst.I > __context->Perst.HHSt))) {
			__context->Perst.HH_AlarmPh1 = true;
			goto l75;
		}
		if ((! (__context->Perst.EHHSt && (__context->Perst.I > __context->Perst.HHSt)))) {
			__context->Perst.HH_AlarmPh1 = false;
			goto l73;
		}
		//assert(false);
		return;  			}
	l73: {
			__context->Perst.HH_AlarmPh2 = false;
			goto l75;
		//assert(false);
		return;  			}
	l75: {
		if (__context->Perst.HH_AlarmPh1) {
			goto l76;
		}
		if ((! __context->Perst.HH_AlarmPh1)) {
			__context->Perst.HH_TimeAlarm = 0;
			goto l105;
		}
		//assert(false);
		return;  			}
	l76: {
		if ((! __context->Perst.HH_AlarmPh2)) {
			__context->Perst.HH_TimeAlarm = (UNICOS_LiveCounter + ((int32_t) __context->Perst.PAlDt));
			goto l78;
		}
		if ((! (! __context->Perst.HH_AlarmPh2))) {
			goto l81;
		}
		//assert(false);
		return;  			}
	l78: {
			__context->Perst.HH_AlarmPh2 = true;
			goto l81;
		//assert(false);
		return;  			}
	l81: {
		if (__context->Perst.HH_AlarmPh2) {
			goto l82;
		}
		if ((! __context->Perst.HH_AlarmPh2)) {
			goto l99;
		}
		//assert(false);
		return;  			}
	l82: {
		if ((UNICOS_LiveCounter >= __context->Perst.HH_TimeAlarm)) {
			goto l83;
		}
		if (((! (UNICOS_LiveCounter >= __context->Perst.HH_TimeAlarm)) && __context->Perst.MAlBRSt)) {
			__context->Perst.HHAlSt = false;
			PosHHW = false;
			goto l97;
		}
		if (((! (UNICOS_LiveCounter >= __context->Perst.HH_TimeAlarm)) && (! ((! (UNICOS_LiveCounter >= __context->Perst.HH_TimeAlarm)) && __context->Perst.MAlBRSt)))) {
			__context->Perst.HHAlSt = false;
			PosHHW = true;
			goto l97;
		}
		//assert(false);
		return;  			}
	l83: {
		if (__context->Perst.MAlBRSt) {
			__context->Perst.HHAlSt = false;
			PosHHW = true;
			goto l90;
		}
		if ((! __context->Perst.MAlBRSt)) {
			__context->Perst.HHAlSt = true;
			PosHHW = false;
			goto l90;
		}
		//assert(false);
		return;  			}
	l90: {
			goto l97;
		//assert(false);
		return;  			}
	l97: {
			goto l99;
		//assert(false);
		return;  			}
	l99: {
		if (((__context->Perst.HH_TimeAlarm - ((int32_t) __context->Perst.PAlDt)) > UNICOS_LiveCounter)) {
			__context->Perst.HH_TimeAlarm = (UNICOS_LiveCounter + ((int32_t) __context->Perst.PAlDt));
			goto l103;
		}
		if ((! ((__context->Perst.HH_TimeAlarm - ((int32_t) __context->Perst.PAlDt)) > UNICOS_LiveCounter))) {
			goto l103;
		}
		//assert(false);
		return;  			}
	l103: {
			goto l108;
		//assert(false);
		return;  			}
	l105: {
			__context->Perst.HHAlSt = false;
			PosHHW = false;
			goto l108;
		//assert(false);
		return;  			}
	l108: {
		if ((__context->Perst.ELLSt && (__context->Perst.I < __context->Perst.LLSt))) {
			__context->Perst.LL_AlarmPh1 = true;
			goto l114;
		}
		if ((! (__context->Perst.ELLSt && (__context->Perst.I < __context->Perst.LLSt)))) {
			__context->Perst.LL_AlarmPh1 = false;
			goto l112;
		}
		//assert(false);
		return;  			}
	l112: {
			__context->Perst.LL_AlarmPh2 = false;
			goto l114;
		//assert(false);
		return;  			}
	l114: {
		if (__context->Perst.LL_AlarmPh1) {
			goto l115;
		}
		if ((! __context->Perst.LL_AlarmPh1)) {
			__context->Perst.LL_TimeAlarm = 0;
			goto l144;
		}
		//assert(false);
		return;  			}
	l115: {
		if ((! __context->Perst.LL_AlarmPh2)) {
			__context->Perst.LL_TimeAlarm = (UNICOS_LiveCounter + ((int32_t) __context->Perst.PAlDt));
			goto l117;
		}
		if ((! (! __context->Perst.LL_AlarmPh2))) {
			goto l120;
		}
		//assert(false);
		return;  			}
	l117: {
			__context->Perst.LL_AlarmPh2 = true;
			goto l120;
		//assert(false);
		return;  			}
	l120: {
		if (__context->Perst.LL_AlarmPh2) {
			goto l121;
		}
		if ((! __context->Perst.LL_AlarmPh2)) {
			goto l138;
		}
		//assert(false);
		return;  			}
	l121: {
		if ((UNICOS_LiveCounter >= __context->Perst.LL_TimeAlarm)) {
			goto l122;
		}
		if (((! (UNICOS_LiveCounter >= __context->Perst.LL_TimeAlarm)) && __context->Perst.MAlBRSt)) {
			__context->Perst.LLAlSt = false;
			PosLLW = false;
			goto l136;
		}
		if (((! (UNICOS_LiveCounter >= __context->Perst.LL_TimeAlarm)) && (! ((! (UNICOS_LiveCounter >= __context->Perst.LL_TimeAlarm)) && __context->Perst.MAlBRSt)))) {
			__context->Perst.LLAlSt = false;
			PosLLW = true;
			goto l136;
		}
		//assert(false);
		return;  			}
	l122: {
		if (__context->Perst.MAlBRSt) {
			__context->Perst.LLAlSt = false;
			PosLLW = true;
			goto l129;
		}
		if ((! __context->Perst.MAlBRSt)) {
			__context->Perst.LLAlSt = true;
			PosLLW = false;
			goto l129;
		}
		//assert(false);
		return;  			}
	l129: {
			goto l136;
		//assert(false);
		return;  			}
	l136: {
			goto l138;
		//assert(false);
		return;  			}
	l138: {
		if (((__context->Perst.LL_TimeAlarm - ((int32_t) __context->Perst.PAlDt)) > UNICOS_LiveCounter)) {
			__context->Perst.LL_TimeAlarm = (UNICOS_LiveCounter + ((int32_t) __context->Perst.PAlDt));
			goto l142;
		}
		if ((! ((__context->Perst.LL_TimeAlarm - ((int32_t) __context->Perst.PAlDt)) > UNICOS_LiveCounter))) {
			goto l142;
		}
		//assert(false);
		return;  			}
	l142: {
			goto l147;
		//assert(false);
		return;  			}
	l144: {
			__context->Perst.LLAlSt = false;
			PosLLW = false;
			goto l147;
		//assert(false);
		return;  			}
	l147: {
		if ((__context->Perst.EHSt && (__context->Perst.I > __context->Perst.HSt))) {
			__context->Perst.H_AlarmPh1 = true;
			goto l153;
		}
		if ((! (__context->Perst.EHSt && (__context->Perst.I > __context->Perst.HSt)))) {
			__context->Perst.H_AlarmPh1 = false;
			goto l151;
		}
		//assert(false);
		return;  			}
	l151: {
			__context->Perst.H_AlarmPh2 = false;
			goto l153;
		//assert(false);
		return;  			}
	l153: {
		if (__context->Perst.H_AlarmPh1) {
			goto l154;
		}
		if ((! __context->Perst.H_AlarmPh1)) {
			__context->Perst.H_TimeAlarm = 0;
			goto l183;
		}
		//assert(false);
		return;  			}
	l154: {
		if ((! __context->Perst.H_AlarmPh2)) {
			__context->Perst.H_TimeAlarm = (UNICOS_LiveCounter + ((int32_t) __context->Perst.PAlDt));
			goto l156;
		}
		if ((! (! __context->Perst.H_AlarmPh2))) {
			goto l159;
		}
		//assert(false);
		return;  			}
	l156: {
			__context->Perst.H_AlarmPh2 = true;
			goto l159;
		//assert(false);
		return;  			}
	l159: {
		if (__context->Perst.H_AlarmPh2) {
			goto l160;
		}
		if ((! __context->Perst.H_AlarmPh2)) {
			goto l177;
		}
		//assert(false);
		return;  			}
	l160: {
		if ((UNICOS_LiveCounter >= __context->Perst.H_TimeAlarm)) {
			goto l161;
		}
		if (((! (UNICOS_LiveCounter >= __context->Perst.H_TimeAlarm)) && __context->Perst.MAlBRSt)) {
			__context->Perst.HWSt = false;
			PosHW = false;
			goto l175;
		}
		if (((! (UNICOS_LiveCounter >= __context->Perst.H_TimeAlarm)) && (! ((! (UNICOS_LiveCounter >= __context->Perst.H_TimeAlarm)) && __context->Perst.MAlBRSt)))) {
			__context->Perst.HWSt = false;
			PosHW = true;
			goto l175;
		}
		//assert(false);
		return;  			}
	l161: {
		if (__context->Perst.MAlBRSt) {
			__context->Perst.HWSt = false;
			PosHW = true;
			goto l168;
		}
		if ((! __context->Perst.MAlBRSt)) {
			__context->Perst.HWSt = true;
			PosHW = false;
			goto l168;
		}
		//assert(false);
		return;  			}
	l168: {
			goto l175;
		//assert(false);
		return;  			}
	l175: {
			goto l177;
		//assert(false);
		return;  			}
	l177: {
		if (((__context->Perst.H_TimeAlarm - ((int32_t) __context->Perst.PAlDt)) > UNICOS_LiveCounter)) {
			__context->Perst.H_TimeAlarm = (UNICOS_LiveCounter + ((int32_t) __context->Perst.PAlDt));
			goto l181;
		}
		if ((! ((__context->Perst.H_TimeAlarm - ((int32_t) __context->Perst.PAlDt)) > UNICOS_LiveCounter))) {
			goto l181;
		}
		//assert(false);
		return;  			}
	l181: {
			goto l186;
		//assert(false);
		return;  			}
	l183: {
			__context->Perst.HWSt = false;
			PosHW = false;
			goto l186;
		//assert(false);
		return;  			}
	l186: {
		if ((__context->Perst.ELSt && (__context->Perst.I < __context->Perst.LSt))) {
			__context->Perst.L_AlarmPh1 = true;
			goto l192;
		}
		if ((! (__context->Perst.ELSt && (__context->Perst.I < __context->Perst.LSt)))) {
			__context->Perst.L_AlarmPh1 = false;
			goto l190;
		}
		//assert(false);
		return;  			}
	l190: {
			__context->Perst.L_AlarmPh2 = false;
			goto l192;
		//assert(false);
		return;  			}
	l192: {
		if (__context->Perst.L_AlarmPh1) {
			goto l193;
		}
		if ((! __context->Perst.L_AlarmPh1)) {
			__context->Perst.L_TimeAlarm = 0;
			goto l222;
		}
		//assert(false);
		return;  			}
	l193: {
		if ((! __context->Perst.L_AlarmPh2)) {
			__context->Perst.L_TimeAlarm = (UNICOS_LiveCounter + ((int32_t) __context->Perst.PAlDt));
			goto l195;
		}
		if ((! (! __context->Perst.L_AlarmPh2))) {
			goto l198;
		}
		//assert(false);
		return;  			}
	l195: {
			__context->Perst.L_AlarmPh2 = true;
			goto l198;
		//assert(false);
		return;  			}
	l198: {
		if (__context->Perst.L_AlarmPh2) {
			goto l199;
		}
		if ((! __context->Perst.L_AlarmPh2)) {
			goto l216;
		}
		//assert(false);
		return;  			}
	l199: {
		if ((UNICOS_LiveCounter >= __context->Perst.L_TimeAlarm)) {
			goto l200;
		}
		if (((! (UNICOS_LiveCounter >= __context->Perst.L_TimeAlarm)) && __context->Perst.MAlBRSt)) {
			__context->Perst.LWSt = false;
			PosLW = false;
			goto l214;
		}
		if (((! (UNICOS_LiveCounter >= __context->Perst.L_TimeAlarm)) && (! ((! (UNICOS_LiveCounter >= __context->Perst.L_TimeAlarm)) && __context->Perst.MAlBRSt)))) {
			__context->Perst.LWSt = false;
			PosLW = true;
			goto l214;
		}
		//assert(false);
		return;  			}
	l200: {
		if (__context->Perst.MAlBRSt) {
			__context->Perst.LWSt = false;
			PosLW = true;
			goto l207;
		}
		if ((! __context->Perst.MAlBRSt)) {
			__context->Perst.LWSt = true;
			PosLW = false;
			goto l207;
		}
		//assert(false);
		return;  			}
	l207: {
			goto l214;
		//assert(false);
		return;  			}
	l214: {
			goto l216;
		//assert(false);
		return;  			}
	l216: {
		if (((__context->Perst.L_TimeAlarm - ((int32_t) __context->Perst.PAlDt)) > UNICOS_LiveCounter)) {
			__context->Perst.L_TimeAlarm = (UNICOS_LiveCounter + ((int32_t) __context->Perst.PAlDt));
			goto l220;
		}
		if ((! ((__context->Perst.L_TimeAlarm - ((int32_t) __context->Perst.PAlDt)) > UNICOS_LiveCounter))) {
			goto l220;
		}
		//assert(false);
		return;  			}
	l220: {
			goto l225;
		//assert(false);
		return;  			}
	l222: {
			__context->Perst.LWSt = false;
			PosLW = false;
			goto l225;
		//assert(false);
		return;  			}
	l225: {
			__context->Perst.ISt = (__context->Perst.HHAlSt || __context->Perst.LLAlSt);
			goto l226;
		//assert(false);
		return;  			}
	l226: {
			__context->Perst.WSt = (__context->Perst.HWSt || __context->Perst.LWSt);
			goto l227;
		//assert(false);
		return;  			}
	l227: {
			// Assign inputs
			R_EDGE1_inlined_10.new = __context->Perst.ISt;
			R_EDGE1_inlined_10.old = __context->Perst.Alarm_Cond_old;
			R_EDGE(&R_EDGE1_inlined_10);
			// Assign outputs
			__context->Perst.Alarm_Cond_old = R_EDGE1_inlined_10.old;
			E_Alarm_Cond = R_EDGE1_inlined_10.RET_VAL;
			goto l228;
		//assert(false);
		return;  			}
	l228: {
		if ((((E_MAlAck || E_AuAlAck) || __context->Perst.MAlBRSt) || PAuAckAl)) {
			__context->Perst.AlUnAck = false;
			goto l234;
		}
		if (((! (((E_MAlAck || E_AuAlAck) || __context->Perst.MAlBRSt) || PAuAckAl)) && E_Alarm_Cond)) {
			__context->Perst.AlUnAck = true;
			goto l234;
		}
		if (((! (((E_MAlAck || E_AuAlAck) || __context->Perst.MAlBRSt) || PAuAckAl)) && (! ((! (((E_MAlAck || E_AuAlAck) || __context->Perst.MAlBRSt) || PAuAckAl)) && E_Alarm_Cond)))) {
			goto l234;
		}
		//assert(false);
		return;  			}
	l234: {
			__context->Perst.IOErrorW = __context->Perst.IOError;
			goto l235;
		//assert(false);
		return;  			}
	l235: {
			__context->Perst.IOSimuW = __context->Perst.IOSimu;
			goto l236;
		//assert(false);
		return;  			}
	l236: {
			ConfigW = ((((((((__context->Perst.ELLSt && __context->Perst.ELSt) && (__context->Perst.LSt < __context->Perst.LLSt)) || ((__context->Perst.EHSt && __context->Perst.ELSt) && (__context->Perst.HSt < __context->Perst.LSt))) || ((__context->Perst.EHHSt && __context->Perst.EHSt) && (__context->Perst.HHSt < __context->Perst.HSt))) || ((__context->Perst.ELLSt && __context->Perst.EHSt) && (__context->Perst.LLSt > __context->Perst.HSt))) || ((__context->Perst.ELLSt && __context->Perst.EHHSt) && (__context->Perst.LLSt > __context->Perst.HHSt))) || ((__context->Perst.ELSt && __context->Perst.EHHSt) && (__context->Perst.LSt > __context->Perst.HHSt))) || ((((! __context->Perst.EHHSt) && (! __context->Perst.EHSt)) && (! __context->Perst.ELSt)) && (! __context->Perst.ELLSt)));
			goto l237;
		//assert(false);
		return;  			}
	l237: {
			__context->Perst.PosSt = __context->Perst.I;
			goto l238;
		//assert(false);
		return;  			}
	l238: {
		if ((__context->Perst.ArmRcpSt && E_ActRcp)) {
			__context->Perst.ArmRcpSt = false;
			goto l242;
		}
		if ((! (__context->Perst.ArmRcpSt && E_ActRcp))) {
			goto l242;
		}
		//assert(false);
		return;  			}
	l242: {
			PulseWidth = (1500.0 / ((float) ((int32_t) T_CYCLE)));
			goto l243;
		//assert(false);
		return;  			}
	l243: {
		if ((__context->Perst.ISt || (__context->Perst.Iinc > 0))) {
			__context->Perst.Iinc = (__context->Perst.Iinc + 1);
			goto l245;
		}
		if ((! (__context->Perst.ISt || (__context->Perst.Iinc > 0)))) {
			goto l248;
		}
		//assert(false);
		return;  			}
	l245: {
			__context->Perst.WISt = true;
			goto l248;
		//assert(false);
		return;  			}
	l248: {
		if (((((float) __context->Perst.Iinc) > PulseWidth) || ((! __context->Perst.ISt) && (__context->Perst.Iinc == 0)))) {
			__context->Perst.Iinc = 0;
			goto l250;
		}
		if ((! ((((float) __context->Perst.Iinc) > PulseWidth) || ((! __context->Perst.ISt) && (__context->Perst.Iinc == 0))))) {
			goto l253;
		}
		//assert(false);
		return;  			}
	l250: {
			__context->Perst.WISt = __context->Perst.ISt;
			goto l253;
		//assert(false);
		return;  			}
	l253: {
		if ((__context->Perst.WSt || (__context->Perst.Winc > 0))) {
			__context->Perst.Winc = (__context->Perst.Winc + 1);
			goto l255;
		}
		if ((! (__context->Perst.WSt || (__context->Perst.Winc > 0)))) {
			goto l258;
		}
		//assert(false);
		return;  			}
	l255: {
			__context->Perst.WWSt = true;
			goto l258;
		//assert(false);
		return;  			}
	l258: {
		if (((((float) __context->Perst.Winc) > PulseWidth) || ((! __context->Perst.WSt) && (__context->Perst.Winc == 0)))) {
			__context->Perst.Winc = 0;
			goto l260;
		}
		if ((! ((((float) __context->Perst.Winc) > PulseWidth) || ((! __context->Perst.WSt) && (__context->Perst.Winc == 0))))) {
			goto l263;
		}
		//assert(false);
		return;  			}
	l260: {
			__context->Perst.WWSt = __context->Perst.WSt;
			goto l263;
		//assert(false);
		return;  			}
	l263: {
		if ((__context->Perst.HHAlSt || (__context->Perst.HHinc > 0))) {
			__context->Perst.HHinc = (__context->Perst.HHinc + 1);
			goto l265;
		}
		if ((! (__context->Perst.HHAlSt || (__context->Perst.HHinc > 0)))) {
			goto l268;
		}
		//assert(false);
		return;  			}
	l265: {
			__context->Perst.WHHAlSt = true;
			goto l268;
		//assert(false);
		return;  			}
	l268: {
		if (((((float) __context->Perst.HHinc) > PulseWidth) || ((! __context->Perst.HHAlSt) && (__context->Perst.HHinc == 0)))) {
			__context->Perst.HHinc = 0;
			goto l270;
		}
		if ((! ((((float) __context->Perst.HHinc) > PulseWidth) || ((! __context->Perst.HHAlSt) && (__context->Perst.HHinc == 0))))) {
			goto l273;
		}
		//assert(false);
		return;  			}
	l270: {
			__context->Perst.WHHAlSt = __context->Perst.HHAlSt;
			goto l273;
		//assert(false);
		return;  			}
	l273: {
		if ((__context->Perst.HWSt || (__context->Perst.Hinc > 0))) {
			__context->Perst.Hinc = (__context->Perst.Hinc + 1);
			goto l275;
		}
		if ((! (__context->Perst.HWSt || (__context->Perst.Hinc > 0)))) {
			goto l278;
		}
		//assert(false);
		return;  			}
	l275: {
			__context->Perst.WHWSt = true;
			goto l278;
		//assert(false);
		return;  			}
	l278: {
		if (((((float) __context->Perst.Hinc) > PulseWidth) || ((! __context->Perst.HWSt) && (__context->Perst.Hinc == 0)))) {
			__context->Perst.Hinc = 0;
			goto l280;
		}
		if ((! ((((float) __context->Perst.Hinc) > PulseWidth) || ((! __context->Perst.HWSt) && (__context->Perst.Hinc == 0))))) {
			goto l283;
		}
		//assert(false);
		return;  			}
	l280: {
			__context->Perst.WHWSt = __context->Perst.HWSt;
			goto l283;
		//assert(false);
		return;  			}
	l283: {
		if ((__context->Perst.LWSt || (__context->Perst.Linc > 0))) {
			__context->Perst.Linc = (__context->Perst.Linc + 1);
			goto l285;
		}
		if ((! (__context->Perst.LWSt || (__context->Perst.Linc > 0)))) {
			goto l288;
		}
		//assert(false);
		return;  			}
	l285: {
			__context->Perst.WLWSt = true;
			goto l288;
		//assert(false);
		return;  			}
	l288: {
		if (((((float) __context->Perst.Linc) > PulseWidth) || ((! __context->Perst.LWSt) && (__context->Perst.Linc == 0)))) {
			__context->Perst.Linc = 0;
			goto l290;
		}
		if ((! ((((float) __context->Perst.Linc) > PulseWidth) || ((! __context->Perst.LWSt) && (__context->Perst.Linc == 0))))) {
			goto l293;
		}
		//assert(false);
		return;  			}
	l290: {
			__context->Perst.WLWSt = __context->Perst.LWSt;
			goto l293;
		//assert(false);
		return;  			}
	l293: {
		if ((__context->Perst.LLAlSt || (__context->Perst.LLinc > 0))) {
			__context->Perst.LLinc = (__context->Perst.LLinc + 1);
			goto l295;
		}
		if ((! (__context->Perst.LLAlSt || (__context->Perst.LLinc > 0)))) {
			goto l298;
		}
		//assert(false);
		return;  			}
	l295: {
			__context->Perst.WLLAlSt = true;
			goto l298;
		//assert(false);
		return;  			}
	l298: {
		if (((((float) __context->Perst.LLinc) > PulseWidth) || ((! __context->Perst.LLAlSt) && (__context->Perst.LLinc == 0)))) {
			__context->Perst.LLinc = 0;
			goto l300;
		}
		if ((! ((((float) __context->Perst.LLinc) > PulseWidth) || ((! __context->Perst.LLAlSt) && (__context->Perst.LLinc == 0))))) {
			goto l303;
		}
		//assert(false);
		return;  			}
	l300: {
			__context->Perst.WLLAlSt = __context->Perst.LLAlSt;
			goto l303;
		//assert(false);
		return;  			}
	l303: {
			StsReg01b[8] = __context->Perst.WISt;
			goto x5;
		//assert(false);
		return;  			}
	l304: {
			StsReg01b[9] = __context->Perst.WWSt;
			goto x6;
		//assert(false);
		return;  			}
	l305: {
			StsReg01b[10] = false;
			goto x7;
		//assert(false);
		return;  			}
	l306: {
			StsReg01b[11] = __context->Perst.ArmRcpSt;
			goto x8;
		//assert(false);
		return;  			}
	l307: {
			StsReg01b[12] = false;
			goto x9;
		//assert(false);
		return;  			}
	l308: {
			StsReg01b[13] = ConfigW;
			goto x10;
		//assert(false);
		return;  			}
	l309: {
			StsReg01b[14] = __context->Perst.IOErrorW;
			goto x11;
		//assert(false);
		return;  			}
	l310: {
			StsReg01b[15] = __context->Perst.IOSimuW;
			goto x12;
		//assert(false);
		return;  			}
	l311: {
			StsReg01b[0] = PosHHW;
			goto x13;
		//assert(false);
		return;  			}
	l312: {
			StsReg01b[1] = PosHW;
			goto x14;
		//assert(false);
		return;  			}
	l313: {
			StsReg01b[2] = PosLW;
			goto x15;
		//assert(false);
		return;  			}
	l314: {
			StsReg01b[3] = PosLLW;
			goto x16;
		//assert(false);
		return;  			}
	l315: {
			StsReg01b[4] = __context->Perst.AlUnAck;
			goto x17;
		//assert(false);
		return;  			}
	l316: {
			StsReg01b[5] = false;
			goto x18;
		//assert(false);
		return;  			}
	l317: {
			StsReg01b[6] = __context->Perst.MAlBRSt;
			goto x19;
		//assert(false);
		return;  			}
	l318: {
			StsReg01b[7] = __context->Perst.AuIhMB;
			goto x20;
		//assert(false);
		return;  			}
	l319: {
			StsReg02b[8] = __context->Perst.EHHSt;
			goto x21;
		//assert(false);
		return;  			}
	l320: {
			StsReg02b[9] = __context->Perst.EHSt;
			goto x22;
		//assert(false);
		return;  			}
	l321: {
			StsReg02b[10] = __context->Perst.ELSt;
			goto x23;
		//assert(false);
		return;  			}
	l322: {
			StsReg02b[11] = __context->Perst.ELLSt;
			goto x24;
		//assert(false);
		return;  			}
	l323: {
			StsReg02b[12] = __context->Perst.WHHAlSt;
			goto x25;
		//assert(false);
		return;  			}
	l324: {
			StsReg02b[13] = __context->Perst.WHWSt;
			goto x26;
		//assert(false);
		return;  			}
	l325: {
			StsReg02b[14] = __context->Perst.WLWSt;
			goto x27;
		//assert(false);
		return;  			}
	l326: {
			StsReg02b[15] = __context->Perst.WLLAlSt;
			goto x28;
		//assert(false);
		return;  			}
	l327: {
			StsReg02b[0] = IhMHHSt;
			goto x29;
		//assert(false);
		return;  			}
	l328: {
			StsReg02b[1] = IhMHSt;
			goto x30;
		//assert(false);
		return;  			}
	l329: {
			StsReg02b[2] = IhMLSt;
			goto x31;
		//assert(false);
		return;  			}
	l330: {
			StsReg02b[3] = IhMLLSt;
			goto x32;
		//assert(false);
		return;  			}
	l331: {
			StsReg02b[4] = false;
			goto x33;
		//assert(false);
		return;  			}
	l332: {
			StsReg02b[5] = false;
			goto x34;
		//assert(false);
		return;  			}
	l333: {
			StsReg02b[6] = false;
			goto x35;
		//assert(false);
		return;  			}
	l334: {
			StsReg02b[7] = false;
			goto x36;
		//assert(false);
		return;  			}
	l335: {
			// Assign inputs
			DETECT_EDGE1.new = __context->Perst.AlUnAck;
			DETECT_EDGE1.old = __context->Perst.AlUnAck_old;
			DETECT_EDGE(&DETECT_EDGE1);
			// Assign outputs
			__context->Perst.AlUnAck_old = DETECT_EDGE1.old;
			RE_AlUnAck = DETECT_EDGE1.re;
			FE_AlUnAck = DETECT_EDGE1.fe;
			goto l336;
		//assert(false);
		return;  			}
	l336: {
			__context->StsReg01 = TempStsReg01;
			__context->StsReg02 = TempStsReg02;
			__context->Perst.PAA = TempPAA;
			goto l339;
		//assert(false);
		return;  			}
	l339: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	x: {
			ConfigW = false;
			E_ActRcp = false;
			E_Alarm_Cond = false;
			E_ArmRcp = false;
			E_AuAlAck = false;
			E_MAlAck = false;
			E_MAlBSetRst = false;
			E_MNewHHR = false;
			E_MNewHR = false;
			E_MNewLLR = false;
			E_MNewLR = false;
			FE_AlUnAck = false;
			IhMHHSt = false;
			IhMHSt = false;
			IhMLLSt = false;
			IhMLSt = false;
			PAuAckAl = false;
			PosHHW = false;
			PosHW = false;
			PosLLW = false;
			PosLW = false;
			PulseWidth = 0.0;
			RE_AlUnAck = false;
			TempPAA.ParReg = 0;
			TempStsReg01 = 0;
			TempStsReg02 = 0;
			goto x1;
		//assert(false);
		return;  			}
	x1: {
			StsReg01b[0] = ((TempStsReg01 & 256) != 0);
			StsReg01b[1] = ((TempStsReg01 & 512) != 0);
			StsReg01b[2] = ((TempStsReg01 & 1024) != 0);
			StsReg01b[3] = ((TempStsReg01 & 2048) != 0);
			StsReg01b[4] = ((TempStsReg01 & 4096) != 0);
			StsReg01b[5] = ((TempStsReg01 & 8192) != 0);
			StsReg01b[6] = ((TempStsReg01 & 16384) != 0);
			StsReg01b[7] = ((TempStsReg01 & 32768) != 0);
			StsReg01b[8] = ((TempStsReg01 & 1) != 0);
			StsReg01b[9] = ((TempStsReg01 & 2) != 0);
			StsReg01b[10] = ((TempStsReg01 & 4) != 0);
			StsReg01b[11] = ((TempStsReg01 & 8) != 0);
			StsReg01b[12] = ((TempStsReg01 & 16) != 0);
			StsReg01b[13] = ((TempStsReg01 & 32) != 0);
			StsReg01b[14] = ((TempStsReg01 & 64) != 0);
			StsReg01b[15] = ((TempStsReg01 & 128) != 0);
			StsReg02b[0] = ((TempStsReg02 & 256) != 0);
			StsReg02b[1] = ((TempStsReg02 & 512) != 0);
			StsReg02b[2] = ((TempStsReg02 & 1024) != 0);
			StsReg02b[3] = ((TempStsReg02 & 2048) != 0);
			StsReg02b[4] = ((TempStsReg02 & 4096) != 0);
			StsReg02b[5] = ((TempStsReg02 & 8192) != 0);
			StsReg02b[6] = ((TempStsReg02 & 16384) != 0);
			StsReg02b[7] = ((TempStsReg02 & 32768) != 0);
			StsReg02b[8] = ((TempStsReg02 & 1) != 0);
			StsReg02b[9] = ((TempStsReg02 & 2) != 0);
			StsReg02b[10] = ((TempStsReg02 & 4) != 0);
			StsReg02b[11] = ((TempStsReg02 & 8) != 0);
			StsReg02b[12] = ((TempStsReg02 & 16) != 0);
			StsReg02b[13] = ((TempStsReg02 & 32) != 0);
			StsReg02b[14] = ((TempStsReg02 & 64) != 0);
			StsReg02b[15] = ((TempStsReg02 & 128) != 0);
			PAAb.ParRegb[0] = ((TempPAA.ParReg & 256) != 0);
			goto varview_refresh191;
		//assert(false);
		return;  			}
	varview_refresh191: {
			PAAb.ParRegb[1] = ((TempPAA.ParReg & 512) != 0);
			PAAb.ParRegb[2] = ((TempPAA.ParReg & 1024) != 0);
			PAAb.ParRegb[3] = ((TempPAA.ParReg & 2048) != 0);
			PAAb.ParRegb[4] = ((TempPAA.ParReg & 4096) != 0);
			PAAb.ParRegb[5] = ((TempPAA.ParReg & 8192) != 0);
			PAAb.ParRegb[6] = ((TempPAA.ParReg & 16384) != 0);
			PAAb.ParRegb[7] = ((TempPAA.ParReg & 32768) != 0);
			PAAb.ParRegb[8] = ((TempPAA.ParReg & 1) != 0);
			PAAb.ParRegb[9] = ((TempPAA.ParReg & 2) != 0);
			PAAb.ParRegb[10] = ((TempPAA.ParReg & 4) != 0);
			PAAb.ParRegb[11] = ((TempPAA.ParReg & 8) != 0);
			PAAb.ParRegb[12] = ((TempPAA.ParReg & 16) != 0);
			PAAb.ParRegb[13] = ((TempPAA.ParReg & 32) != 0);
			PAAb.ParRegb[14] = ((TempPAA.ParReg & 64) != 0);
			PAAb.ParRegb[15] = ((TempPAA.ParReg & 128) != 0);
			TempStsReg01 = __context->StsReg01;
			goto x2;
		//assert(false);
		return;  			}
	x2: {
			StsReg01b[0] = ((TempStsReg01 & 256) != 0);
			StsReg01b[1] = ((TempStsReg01 & 512) != 0);
			StsReg01b[2] = ((TempStsReg01 & 1024) != 0);
			StsReg01b[3] = ((TempStsReg01 & 2048) != 0);
			StsReg01b[4] = ((TempStsReg01 & 4096) != 0);
			StsReg01b[5] = ((TempStsReg01 & 8192) != 0);
			StsReg01b[6] = ((TempStsReg01 & 16384) != 0);
			StsReg01b[7] = ((TempStsReg01 & 32768) != 0);
			StsReg01b[8] = ((TempStsReg01 & 1) != 0);
			StsReg01b[9] = ((TempStsReg01 & 2) != 0);
			StsReg01b[10] = ((TempStsReg01 & 4) != 0);
			StsReg01b[11] = ((TempStsReg01 & 8) != 0);
			StsReg01b[12] = ((TempStsReg01 & 16) != 0);
			StsReg01b[13] = ((TempStsReg01 & 32) != 0);
			StsReg01b[14] = ((TempStsReg01 & 64) != 0);
			StsReg01b[15] = ((TempStsReg01 & 128) != 0);
			TempStsReg02 = __context->StsReg02;
			goto x3;
		//assert(false);
		return;  			}
	x3: {
			StsReg02b[0] = ((TempStsReg02 & 256) != 0);
			StsReg02b[1] = ((TempStsReg02 & 512) != 0);
			StsReg02b[2] = ((TempStsReg02 & 1024) != 0);
			StsReg02b[3] = ((TempStsReg02 & 2048) != 0);
			StsReg02b[4] = ((TempStsReg02 & 4096) != 0);
			StsReg02b[5] = ((TempStsReg02 & 8192) != 0);
			StsReg02b[6] = ((TempStsReg02 & 16384) != 0);
			StsReg02b[7] = ((TempStsReg02 & 32768) != 0);
			StsReg02b[8] = ((TempStsReg02 & 1) != 0);
			StsReg02b[9] = ((TempStsReg02 & 2) != 0);
			StsReg02b[10] = ((TempStsReg02 & 4) != 0);
			StsReg02b[11] = ((TempStsReg02 & 8) != 0);
			StsReg02b[12] = ((TempStsReg02 & 16) != 0);
			StsReg02b[13] = ((TempStsReg02 & 32) != 0);
			StsReg02b[14] = ((TempStsReg02 & 64) != 0);
			StsReg02b[15] = ((TempStsReg02 & 128) != 0);
			TempPAA = __context->Perst.PAA;
			goto x4;
		//assert(false);
		return;  			}
	x4: {
			PAAb.ParRegb[0] = ((TempPAA.ParReg & 256) != 0);
			PAAb.ParRegb[1] = ((TempPAA.ParReg & 512) != 0);
			PAAb.ParRegb[2] = ((TempPAA.ParReg & 1024) != 0);
			PAAb.ParRegb[3] = ((TempPAA.ParReg & 2048) != 0);
			PAAb.ParRegb[4] = ((TempPAA.ParReg & 4096) != 0);
			PAAb.ParRegb[5] = ((TempPAA.ParReg & 8192) != 0);
			PAAb.ParRegb[6] = ((TempPAA.ParReg & 16384) != 0);
			PAAb.ParRegb[7] = ((TempPAA.ParReg & 32768) != 0);
			PAAb.ParRegb[8] = ((TempPAA.ParReg & 1) != 0);
			PAAb.ParRegb[9] = ((TempPAA.ParReg & 2) != 0);
			PAAb.ParRegb[10] = ((TempPAA.ParReg & 4) != 0);
			PAAb.ParRegb[11] = ((TempPAA.ParReg & 8) != 0);
			PAAb.ParRegb[12] = ((TempPAA.ParReg & 16) != 0);
			PAAb.ParRegb[13] = ((TempPAA.ParReg & 32) != 0);
			PAAb.ParRegb[14] = ((TempPAA.ParReg & 64) != 0);
			PAAb.ParRegb[15] = ((TempPAA.ParReg & 128) != 0);
			goto l4;
		//assert(false);
		return;  			}
	x5: {
		if (StsReg01b[8]) {
			TempStsReg01 = (TempStsReg01 | 1);
			goto l304;
		}
		if ((! StsReg01b[8])) {
			TempStsReg01 = (TempStsReg01 & 65534);
			goto l304;
		}
		//assert(false);
		return;  			}
	x6: {
		if (StsReg01b[9]) {
			TempStsReg01 = (TempStsReg01 | 2);
			goto l305;
		}
		if ((! StsReg01b[9])) {
			TempStsReg01 = (TempStsReg01 & 65533);
			goto l305;
		}
		//assert(false);
		return;  			}
	x7: {
		if (StsReg01b[10]) {
			TempStsReg01 = (TempStsReg01 | 4);
			goto l306;
		}
		if ((! StsReg01b[10])) {
			TempStsReg01 = (TempStsReg01 & 65531);
			goto l306;
		}
		//assert(false);
		return;  			}
	x8: {
		if (StsReg01b[11]) {
			TempStsReg01 = (TempStsReg01 | 8);
			goto l307;
		}
		if ((! StsReg01b[11])) {
			TempStsReg01 = (TempStsReg01 & 65527);
			goto l307;
		}
		//assert(false);
		return;  			}
	x9: {
		if (StsReg01b[12]) {
			TempStsReg01 = (TempStsReg01 | 16);
			goto l308;
		}
		if ((! StsReg01b[12])) {
			TempStsReg01 = (TempStsReg01 & 65519);
			goto l308;
		}
		//assert(false);
		return;  			}
	x10: {
		if (StsReg01b[13]) {
			TempStsReg01 = (TempStsReg01 | 32);
			goto l309;
		}
		if ((! StsReg01b[13])) {
			TempStsReg01 = (TempStsReg01 & 65503);
			goto l309;
		}
		//assert(false);
		return;  			}
	x11: {
		if (StsReg01b[14]) {
			TempStsReg01 = (TempStsReg01 | 64);
			goto l310;
		}
		if ((! StsReg01b[14])) {
			TempStsReg01 = (TempStsReg01 & 65471);
			goto l310;
		}
		//assert(false);
		return;  			}
	x12: {
		if (StsReg01b[15]) {
			TempStsReg01 = (TempStsReg01 | 128);
			goto l311;
		}
		if ((! StsReg01b[15])) {
			TempStsReg01 = (TempStsReg01 & 65407);
			goto l311;
		}
		//assert(false);
		return;  			}
	x13: {
		if (StsReg01b[0]) {
			TempStsReg01 = (TempStsReg01 | 256);
			goto l312;
		}
		if ((! StsReg01b[0])) {
			TempStsReg01 = (TempStsReg01 & 65279);
			goto l312;
		}
		//assert(false);
		return;  			}
	x14: {
		if (StsReg01b[1]) {
			TempStsReg01 = (TempStsReg01 | 512);
			goto l313;
		}
		if ((! StsReg01b[1])) {
			TempStsReg01 = (TempStsReg01 & 65023);
			goto l313;
		}
		//assert(false);
		return;  			}
	x15: {
		if (StsReg01b[2]) {
			TempStsReg01 = (TempStsReg01 | 1024);
			goto l314;
		}
		if ((! StsReg01b[2])) {
			TempStsReg01 = (TempStsReg01 & 64511);
			goto l314;
		}
		//assert(false);
		return;  			}
	x16: {
		if (StsReg01b[3]) {
			TempStsReg01 = (TempStsReg01 | 2048);
			goto l315;
		}
		if ((! StsReg01b[3])) {
			TempStsReg01 = (TempStsReg01 & 63487);
			goto l315;
		}
		//assert(false);
		return;  			}
	x17: {
		if (StsReg01b[4]) {
			TempStsReg01 = (TempStsReg01 | 4096);
			goto l316;
		}
		if ((! StsReg01b[4])) {
			TempStsReg01 = (TempStsReg01 & 61439);
			goto l316;
		}
		//assert(false);
		return;  			}
	x18: {
		if (StsReg01b[5]) {
			TempStsReg01 = (TempStsReg01 | 8192);
			goto l317;
		}
		if ((! StsReg01b[5])) {
			TempStsReg01 = (TempStsReg01 & 57343);
			goto l317;
		}
		//assert(false);
		return;  			}
	x19: {
		if (StsReg01b[6]) {
			TempStsReg01 = (TempStsReg01 | 16384);
			goto l318;
		}
		if ((! StsReg01b[6])) {
			TempStsReg01 = (TempStsReg01 & 49151);
			goto l318;
		}
		//assert(false);
		return;  			}
	x20: {
		if (StsReg01b[7]) {
			TempStsReg01 = (TempStsReg01 | 32768);
			goto l319;
		}
		if ((! StsReg01b[7])) {
			TempStsReg01 = (TempStsReg01 & 32767);
			goto l319;
		}
		//assert(false);
		return;  			}
	x21: {
		if (StsReg02b[8]) {
			TempStsReg02 = (TempStsReg02 | 1);
			goto l320;
		}
		if ((! StsReg02b[8])) {
			TempStsReg02 = (TempStsReg02 & 65534);
			goto l320;
		}
		//assert(false);
		return;  			}
	x22: {
		if (StsReg02b[9]) {
			TempStsReg02 = (TempStsReg02 | 2);
			goto l321;
		}
		if ((! StsReg02b[9])) {
			TempStsReg02 = (TempStsReg02 & 65533);
			goto l321;
		}
		//assert(false);
		return;  			}
	x23: {
		if (StsReg02b[10]) {
			TempStsReg02 = (TempStsReg02 | 4);
			goto l322;
		}
		if ((! StsReg02b[10])) {
			TempStsReg02 = (TempStsReg02 & 65531);
			goto l322;
		}
		//assert(false);
		return;  			}
	x24: {
		if (StsReg02b[11]) {
			TempStsReg02 = (TempStsReg02 | 8);
			goto l323;
		}
		if ((! StsReg02b[11])) {
			TempStsReg02 = (TempStsReg02 & 65527);
			goto l323;
		}
		//assert(false);
		return;  			}
	x25: {
		if (StsReg02b[12]) {
			TempStsReg02 = (TempStsReg02 | 16);
			goto l324;
		}
		if ((! StsReg02b[12])) {
			TempStsReg02 = (TempStsReg02 & 65519);
			goto l324;
		}
		//assert(false);
		return;  			}
	x26: {
		if (StsReg02b[13]) {
			TempStsReg02 = (TempStsReg02 | 32);
			goto l325;
		}
		if ((! StsReg02b[13])) {
			TempStsReg02 = (TempStsReg02 & 65503);
			goto l325;
		}
		//assert(false);
		return;  			}
	x27: {
		if (StsReg02b[14]) {
			TempStsReg02 = (TempStsReg02 | 64);
			goto l326;
		}
		if ((! StsReg02b[14])) {
			TempStsReg02 = (TempStsReg02 & 65471);
			goto l326;
		}
		//assert(false);
		return;  			}
	x28: {
		if (StsReg02b[15]) {
			TempStsReg02 = (TempStsReg02 | 128);
			goto l327;
		}
		if ((! StsReg02b[15])) {
			TempStsReg02 = (TempStsReg02 & 65407);
			goto l327;
		}
		//assert(false);
		return;  			}
	x29: {
		if (StsReg02b[0]) {
			TempStsReg02 = (TempStsReg02 | 256);
			goto l328;
		}
		if ((! StsReg02b[0])) {
			TempStsReg02 = (TempStsReg02 & 65279);
			goto l328;
		}
		//assert(false);
		return;  			}
	x30: {
		if (StsReg02b[1]) {
			TempStsReg02 = (TempStsReg02 | 512);
			goto l329;
		}
		if ((! StsReg02b[1])) {
			TempStsReg02 = (TempStsReg02 & 65023);
			goto l329;
		}
		//assert(false);
		return;  			}
	x31: {
		if (StsReg02b[2]) {
			TempStsReg02 = (TempStsReg02 | 1024);
			goto l330;
		}
		if ((! StsReg02b[2])) {
			TempStsReg02 = (TempStsReg02 & 64511);
			goto l330;
		}
		//assert(false);
		return;  			}
	x32: {
		if (StsReg02b[3]) {
			TempStsReg02 = (TempStsReg02 | 2048);
			goto l331;
		}
		if ((! StsReg02b[3])) {
			TempStsReg02 = (TempStsReg02 & 63487);
			goto l331;
		}
		//assert(false);
		return;  			}
	x33: {
		if (StsReg02b[4]) {
			TempStsReg02 = (TempStsReg02 | 4096);
			goto l332;
		}
		if ((! StsReg02b[4])) {
			TempStsReg02 = (TempStsReg02 & 61439);
			goto l332;
		}
		//assert(false);
		return;  			}
	x34: {
		if (StsReg02b[5]) {
			TempStsReg02 = (TempStsReg02 | 8192);
			goto l333;
		}
		if ((! StsReg02b[5])) {
			TempStsReg02 = (TempStsReg02 & 57343);
			goto l333;
		}
		//assert(false);
		return;  			}
	x35: {
		if (StsReg02b[6]) {
			TempStsReg02 = (TempStsReg02 | 16384);
			goto l334;
		}
		if ((! StsReg02b[6])) {
			TempStsReg02 = (TempStsReg02 & 49151);
			goto l334;
		}
		//assert(false);
		return;  			}
	x36: {
		if (StsReg02b[7]) {
			TempStsReg02 = (TempStsReg02 | 32768);
			goto l335;
		}
		if ((! StsReg02b[7])) {
			TempStsReg02 = (TempStsReg02 & 32767);
			goto l335;
		}
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void R_EDGE(__R_EDGE *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
		if (((__context->new == true) && (__context->old == false))) {
			__context->RET_VAL = true;
			__context->old = true;
			goto l71;
		}
		if ((! ((__context->new == true) && (__context->old == false)))) {
			__context->RET_VAL = false;
			__context->old = __context->new;
			goto l71;
		}
		//assert(false);
		return;  			}
	l71: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void DETECT_EDGE(__DETECT_EDGE *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init2;
	init2: {
		if ((__context->new != __context->old)) {
			goto l1;
		}
		if ((! (__context->new != __context->old))) {
			__context->re = false;
			__context->fe = false;
			goto l131;
		}
		//assert(false);
		return;  			}
	l1: {
		if ((__context->new == true)) {
			__context->re = true;
			__context->fe = false;
			goto l84;
		}
		if ((! (__context->new == true))) {
			__context->re = false;
			__context->fe = true;
			goto l84;
		}
		//assert(false);
		return;  			}
	l84: {
			__context->old = __context->new;
			goto l131;
		//assert(false);
		return;  			}
	l131: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init3;
	init3: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			instance.H = nondet_float();
			instance.HH = nondet_float();
			instance.L = nondet_float();
			instance.LL = nondet_float();
			instance.Manreg01 = nondet_uint16_t();
			instance.Manreg01b[0] = nondet_bool();
			instance.Manreg01b[10] = nondet_bool();
			instance.Manreg01b[11] = nondet_bool();
			instance.Manreg01b[12] = nondet_bool();
			instance.Manreg01b[13] = nondet_bool();
			instance.Manreg01b[14] = nondet_bool();
			instance.Manreg01b[15] = nondet_bool();
			instance.Manreg01b[1] = nondet_bool();
			instance.Manreg01b[2] = nondet_bool();
			instance.Manreg01b[3] = nondet_bool();
			instance.Manreg01b[4] = nondet_bool();
			instance.Manreg01b[5] = nondet_bool();
			instance.Manreg01b[6] = nondet_bool();
			instance.Manreg01b[7] = nondet_bool();
			instance.Manreg01b[8] = nondet_bool();
			instance.Manreg01b[9] = nondet_bool();
			instance.Perst.ActRcp_old = nondet_bool();
			instance.Perst.AlUnAck = nondet_bool();
			instance.Perst.AlUnAck_old = nondet_bool();
			instance.Perst.Alarm_Cond_old = nondet_bool();
			instance.Perst.ArmRcpSt = nondet_bool();
			instance.Perst.ArmRcp_old = nondet_bool();
			instance.Perst.AuAlAck = nondet_bool();
			instance.Perst.AuAlAck_old = nondet_bool();
			instance.Perst.AuEH = nondet_bool();
			instance.Perst.AuEHH = nondet_bool();
			instance.Perst.AuEL = nondet_bool();
			instance.Perst.AuELL = nondet_bool();
			instance.Perst.AuIhMB = nondet_bool();
			instance.Perst.ConfigW = nondet_bool();
			instance.Perst.EHHSt = nondet_bool();
			instance.Perst.EHSt = nondet_bool();
			instance.Perst.ELLSt = nondet_bool();
			instance.Perst.ELSt = nondet_bool();
			instance.Perst.H = nondet_float();
			instance.Perst.HH = nondet_float();
			instance.Perst.HHAlSt = nondet_bool();
			instance.Perst.HHSt = nondet_float();
			instance.Perst.HH_AlarmPh1 = nondet_bool();
			instance.Perst.HH_AlarmPh2 = nondet_bool();
			instance.Perst.HH_TimeAlarm = nondet_int32_t();
			instance.Perst.HHinc = nondet_int16_t();
			instance.Perst.HSt = nondet_float();
			instance.Perst.HWSt = nondet_bool();
			instance.Perst.H_AlarmPh1 = nondet_bool();
			instance.Perst.H_AlarmPh2 = nondet_bool();
			instance.Perst.H_TimeAlarm = nondet_int32_t();
			instance.Perst.Hinc = nondet_int16_t();
			instance.Perst.I = nondet_float();
			instance.Perst.IOError = nondet_bool();
			instance.Perst.IOErrorW = nondet_bool();
			instance.Perst.IOSimu = nondet_bool();
			instance.Perst.IOSimuW = nondet_bool();
			instance.Perst.ISt = nondet_bool();
			instance.Perst.Iinc = nondet_int16_t();
			instance.Perst.L = nondet_float();
			instance.Perst.LL = nondet_float();
			instance.Perst.LLAlSt = nondet_bool();
			instance.Perst.LLSt = nondet_float();
			instance.Perst.LL_AlarmPh1 = nondet_bool();
			instance.Perst.LL_AlarmPh2 = nondet_bool();
			instance.Perst.LL_TimeAlarm = nondet_int32_t();
			instance.Perst.LLinc = nondet_int16_t();
			instance.Perst.LSt = nondet_float();
			instance.Perst.LWSt = nondet_bool();
			instance.Perst.L_AlarmPh1 = nondet_bool();
			instance.Perst.L_AlarmPh2 = nondet_bool();
			instance.Perst.L_TimeAlarm = nondet_int32_t();
			instance.Perst.Linc = nondet_int16_t();
			instance.Perst.MAlAck_old = nondet_bool();
			instance.Perst.MAlBRSt = nondet_bool();
			instance.Perst.MAlBSetRst_old = nondet_bool();
			instance.Perst.MNewHHR_old = nondet_bool();
			instance.Perst.MNewHR_old = nondet_bool();
			instance.Perst.MNewLLR_old = nondet_bool();
			instance.Perst.MNewLR_old = nondet_bool();
			instance.Perst.PAA.ParReg = nondet_uint16_t();
			instance.Perst.PAlDt = nondet_int16_t();
			instance.Perst.PAuAckAl = nondet_bool();
			instance.Perst.PosSt = nondet_float();
			instance.Perst.TimeAlarm = nondet_int32_t();
			instance.Perst.WHHAlSt = nondet_bool();
			instance.Perst.WHWSt = nondet_bool();
			instance.Perst.WISt = nondet_bool();
			instance.Perst.WLLAlSt = nondet_bool();
			instance.Perst.WLWSt = nondet_bool();
			instance.Perst.WSt = nondet_bool();
			instance.Perst.WWSt = nondet_bool();
			instance.Perst.Winc = nondet_int16_t();
			instance.StsReg01 = nondet_uint16_t();
			instance.StsReg02 = nondet_uint16_t();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			CPC_FB_AA(&instance);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
int main() {
	// Initial values
	First_Cycle = false;
	UNICOS_LiveCounter = 0;
	T_CYCLE = 0;
	R_EDGE1.new = false;
	R_EDGE1.old = false;
	R_EDGE1.RET_VAL = false;
	DETECT_EDGE1.new = false;
	DETECT_EDGE1.old = false;
	DETECT_EDGE1.re = false;
	DETECT_EDGE1.fe = false;
	instance.Manreg01 = 0;
	instance.Manreg01b[0] = false;
	instance.Manreg01b[1] = false;
	instance.Manreg01b[2] = false;
	instance.Manreg01b[3] = false;
	instance.Manreg01b[4] = false;
	instance.Manreg01b[5] = false;
	instance.Manreg01b[6] = false;
	instance.Manreg01b[7] = false;
	instance.Manreg01b[8] = false;
	instance.Manreg01b[9] = false;
	instance.Manreg01b[10] = false;
	instance.Manreg01b[11] = false;
	instance.Manreg01b[12] = false;
	instance.Manreg01b[13] = false;
	instance.Manreg01b[14] = false;
	instance.Manreg01b[15] = false;
	instance.HH = 0.0;
	instance.H = 0.0;
	instance.L = 0.0;
	instance.LL = 0.0;
	instance.StsReg01 = 0;
	instance.StsReg02 = 0;
	instance.Perst.I = 0.0;
	instance.Perst.PosSt = 0.0;
	instance.Perst.HHSt = 0.0;
	instance.Perst.HSt = 0.0;
	instance.Perst.LSt = 0.0;
	instance.Perst.LLSt = 0.0;
	instance.Perst.HH = 0.0;
	instance.Perst.H = 0.0;
	instance.Perst.L = 0.0;
	instance.Perst.LL = 0.0;
	instance.Perst.PAlDt = 0;
	instance.Perst.PAA.ParReg = 0;
	instance.Perst.AuAlAck = false;
	instance.Perst.ISt = false;
	instance.Perst.WSt = false;
	instance.Perst.AlUnAck = false;
	instance.Perst.MAlBRSt = false;
	instance.Perst.AuIhMB = false;
	instance.Perst.IOErrorW = false;
	instance.Perst.IOSimuW = false;
	instance.Perst.IOError = false;
	instance.Perst.IOSimu = false;
	instance.Perst.AuEHH = false;
	instance.Perst.AuEH = false;
	instance.Perst.AuEL = false;
	instance.Perst.AuELL = false;
	instance.Perst.HHAlSt = false;
	instance.Perst.HWSt = false;
	instance.Perst.LWSt = false;
	instance.Perst.LLAlSt = false;
	instance.Perst.ConfigW = false;
	instance.Perst.EHHSt = false;
	instance.Perst.EHSt = false;
	instance.Perst.ELSt = false;
	instance.Perst.ELLSt = false;
	instance.Perst.ArmRcpSt = false;
	instance.Perst.PAuAckAl = false;
	instance.Perst.MAlBSetRst_old = false;
	instance.Perst.MAlAck_old = false;
	instance.Perst.AuAlAck_old = false;
	instance.Perst.Alarm_Cond_old = false;
	instance.Perst.MNewHHR_old = false;
	instance.Perst.MNewHR_old = false;
	instance.Perst.MNewLR_old = false;
	instance.Perst.MNewLLR_old = false;
	instance.Perst.ArmRcp_old = false;
	instance.Perst.ActRcp_old = false;
	instance.Perst.AlUnAck_old = false;
	instance.Perst.HH_AlarmPh1 = false;
	instance.Perst.HH_AlarmPh2 = false;
	instance.Perst.H_AlarmPh1 = false;
	instance.Perst.H_AlarmPh2 = false;
	instance.Perst.L_AlarmPh1 = false;
	instance.Perst.L_AlarmPh2 = false;
	instance.Perst.LL_AlarmPh1 = false;
	instance.Perst.LL_AlarmPh2 = false;
	instance.Perst.HH_TimeAlarm = 0;
	instance.Perst.H_TimeAlarm = 0;
	instance.Perst.L_TimeAlarm = 0;
	instance.Perst.LL_TimeAlarm = 0;
	instance.Perst.TimeAlarm = 0;
	instance.Perst.Iinc = 0;
	instance.Perst.Winc = 0;
	instance.Perst.HHinc = 0;
	instance.Perst.Hinc = 0;
	instance.Perst.Linc = 0;
	instance.Perst.LLinc = 0;
	instance.Perst.WISt = false;
	instance.Perst.WWSt = false;
	instance.Perst.WHHAlSt = false;
	instance.Perst.WHWSt = false;
	instance.Perst.WLWSt = false;
	instance.Perst.WLLAlSt = false;
	R_EDGE1_inlined_1.new = false;
	R_EDGE1_inlined_1.old = false;
	R_EDGE1_inlined_1.RET_VAL = false;
	R_EDGE1_inlined_2.new = false;
	R_EDGE1_inlined_2.old = false;
	R_EDGE1_inlined_2.RET_VAL = false;
	R_EDGE1_inlined_3.new = false;
	R_EDGE1_inlined_3.old = false;
	R_EDGE1_inlined_3.RET_VAL = false;
	R_EDGE1_inlined_4.new = false;
	R_EDGE1_inlined_4.old = false;
	R_EDGE1_inlined_4.RET_VAL = false;
	R_EDGE1_inlined_5.new = false;
	R_EDGE1_inlined_5.old = false;
	R_EDGE1_inlined_5.RET_VAL = false;
	R_EDGE1_inlined_6.new = false;
	R_EDGE1_inlined_6.old = false;
	R_EDGE1_inlined_6.RET_VAL = false;
	R_EDGE1_inlined_7.new = false;
	R_EDGE1_inlined_7.old = false;
	R_EDGE1_inlined_7.RET_VAL = false;
	R_EDGE1_inlined_8.new = false;
	R_EDGE1_inlined_8.old = false;
	R_EDGE1_inlined_8.RET_VAL = false;
	R_EDGE1_inlined_9.new = false;
	R_EDGE1_inlined_9.old = false;
	R_EDGE1_inlined_9.RET_VAL = false;
	R_EDGE1_inlined_10.new = false;
	R_EDGE1_inlined_10.old = false;
	R_EDGE1_inlined_10.RET_VAL = false;
	__assertion_error = 0;
	
	VerificationLoop();
}
