#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Root data structure
typedef struct {
	int32_t PT;
	bool IN;
	bool Q;
	int32_t ET;
	bool running;
	int32_t start;
} __TON;
typedef struct {
	bool enabled;
	bool q;
	__TON ton;
} __timeh_fb1;

// Global variables
bool MX0_0;
int32_t __GLOBAL_TIME;
__timeh_fb1 timeh_db1;
int32_t T_CYCLE;
uint16_t __assertion_error;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Automata declarations
void timeh_OB1();
void timeh_fb1(__timeh_fb1 *__context);
void TON(__TON *__context);
void VerificationLoop();

// Automata
void timeh_OB1() {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			// Assign inputs
			timeh_db1.enabled = true;
			timeh_fb1(&timeh_db1);
			// Assign outputs
			goto l1;
		//assert(false);
		return;  			}
	l1: {
			MX0_0 = timeh_db1.q;
			goto l2;
		//assert(false);
		return;  			}
	l2: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void timeh_fb1(__timeh_fb1 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
		if (__context->enabled) {
			goto l11;
		}
		if ((! __context->enabled)) {
			goto l4;
		}
		//assert(false);
		return;  			}
	l11: {
			// Assign inputs
			__context->ton.IN = true;
			__context->ton.PT = 1500;
			TON(&__context->ton);
			// Assign outputs
			goto l21;
		//assert(false);
		return;  			}
	l21: {
			goto l4;
		//assert(false);
		return;  			}
	l4: {
			__context->q = __context->ton.Q;
			goto l5;
		//assert(false);
		return;  			}
	l5: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void TON(__TON *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init2;
	init2: {
		if ((__context->IN == false)) {
			__context->Q = false;
			__context->ET = 0;
			__context->running = false;
			goto l25;
		}
		if ((! (__context->IN == false))) {
			goto l51;
		}
		//assert(false);
		return;  			}
	l51: {
		if ((__context->running == false)) {
			__context->start = __GLOBAL_TIME;
			__context->running = true;
			__context->ET = 0;
			goto l9;
		}
		if ((! (__context->running == false))) {
			goto l14;
		}
		//assert(false);
		return;  			}
	l9: {
		if ((__context->PT == 0)) {
			__context->Q = true;
			goto l13;
		}
		if ((! (__context->PT == 0))) {
			goto l13;
		}
		//assert(false);
		return;  			}
	l13: {
			goto l24;
		//assert(false);
		return;  			}
	l14: {
		if ((! ((__GLOBAL_TIME - (__context->start + __context->PT)) >= 0))) {
			goto l15;
		}
		if ((! (! ((__GLOBAL_TIME - (__context->start + __context->PT)) >= 0)))) {
			__context->Q = true;
			__context->ET = __context->PT;
			goto l23;
		}
		//assert(false);
		return;  			}
	l15: {
		if ((! __context->Q)) {
			__context->ET = (__GLOBAL_TIME - __context->start);
			goto l19;
		}
		if ((! (! __context->Q))) {
			goto l19;
		}
		//assert(false);
		return;  			}
	l19: {
			goto l23;
		//assert(false);
		return;  			}
	l23: {
			goto l24;
		//assert(false);
		return;  			}
	l24: {
			goto l25;
		//assert(false);
		return;  			}
	l25: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init3;
	init3: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			T_CYCLE = nondet_int32_t();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			timeh_OB1();
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			__GLOBAL_TIME = (__GLOBAL_TIME + T_CYCLE);
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
int main() {
	// Initial values
	MX0_0 = false;
	__GLOBAL_TIME = 0;
	timeh_db1.enabled = false;
	timeh_db1.q = false;
	timeh_db1.ton.PT = 0;
	timeh_db1.ton.IN = false;
	timeh_db1.ton.Q = false;
	timeh_db1.ton.ET = 0;
	timeh_db1.ton.running = false;
	timeh_db1.ton.start = 0;
	T_CYCLE = 0;
	__assertion_error = 0;
	
	VerificationLoop();
}
