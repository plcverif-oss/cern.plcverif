#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Variables
bool instance_in1 = false;
bool instance_in2 = false;
int16_t instance_out = 0;
uint16_t __assertion_error = 0;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			instance_in1 = nondet_bool();
			instance_in2 = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
		if ((instance_in1 && (! instance_in2))) {
			goto verificationLoop_VerificationLoop_l1;
		}
		if ((! (instance_in1 && (! instance_in2)))) {
			goto verificationLoop_VerificationLoop_l3;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			instance_out = (instance_out + 1);
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
			goto verificationLoop_VerificationLoop_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l3: {
			goto verificationLoop_VerificationLoop_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l4: {
		if ((! (! (instance_out < 3)))) {
			goto verificationLoop_VerificationLoop_end;
		}
		if ((! (instance_out < 3))) {
			__assertion_error = 1;
			goto verificationLoop_VerificationLoop_end;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_end: {
			goto callEnd;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
