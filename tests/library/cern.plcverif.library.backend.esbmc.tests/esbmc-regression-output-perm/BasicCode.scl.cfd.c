#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Root data structure
typedef struct {
} __basic_OB1;
typedef struct {
	bool in;
	int16_t in2;
	bool RET_VAL;
} __basic_foo;

// Global variables
bool IX0_0;
__basic_OB1 basic_OB11;
__basic_foo basic_foo1;
uint16_t __assertion_error;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Automata declarations
void basic_OB1(__basic_OB1 *__context);
void basic_foo(__basic_foo *__context);
void VerificationLoop();

// Automata
void basic_OB1(__basic_OB1 *__context) {
	// Temporary variables
	bool result;
	
	// Start with initial location
	goto init;
	init: {
			result = false;
			goto l1;
		//assert(false);
		return;  			}
	l1: {
			// Assign inputs
			basic_foo1.in = IX0_0;
			basic_foo1.in2 = 5;
			basic_foo(&basic_foo1);
			// Assign outputs
			result = basic_foo1.RET_VAL;
			goto l2;
		//assert(false);
		return;  			}
	l2: {
		if ((! (!(result) || (! IX0_0)))) {
			__assertion_error = 1;
			goto end;
		}
		if ((! (! (!(result) || (! IX0_0))))) {
			goto end;
		}
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void basic_foo(__basic_foo *__context) {
	// Temporary variables
	bool temp;
	
	// Start with initial location
	goto init1;
	init1: {
			temp = false;
			goto l11;
		//assert(false);
		return;  			}
	l11: {
		if ((__context->in2 > 0)) {
			temp = (! __context->in);
			goto l6;
		}
		if ((! (__context->in2 > 0))) {
			temp = __context->in;
			goto l6;
		}
		//assert(false);
		return;  			}
	l6: {
			__context->RET_VAL = temp;
			goto l7;
		//assert(false);
		return;  			}
	l7: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init2;
	init2: {
			goto loop_start;
		//assert(false);
		return;  			}
	end1: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			IX0_0 = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end1;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			basic_OB1(&basic_OB11);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
int main() {
	// Initial values
	IX0_0 = false;
	basic_foo1.in = false;
	basic_foo1.in2 = 0;
	basic_foo1.RET_VAL = false;
	__assertion_error = 0;
	
	VerificationLoop();
}
