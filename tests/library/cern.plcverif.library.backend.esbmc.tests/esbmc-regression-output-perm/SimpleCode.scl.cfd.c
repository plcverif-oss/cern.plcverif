#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Root data structure
typedef struct {
	int16_t out1;
} __function1;
typedef struct {
	bool in1;
	int16_t in2;
	int16_t RET_VAL;
} __function2;

// Global variables
bool MX123_7;
__function1 function11;
__function2 function21;
uint16_t __assertion_error;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Automata declarations
void simplecode_OB1();
void function1(__function1 *__context);
void function2(__function2 *__context);
void VerificationLoop();

// Automata
void simplecode_OB1() {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			// Assign inputs
			function1(&function11);
			// Assign outputs
			goto l1;
		//assert(false);
		return;  			}
	l1: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void function1(__function1 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			// Assign inputs
			function21.in1 = MX123_7;
			function21.in2 = 123;
			function2(&function21);
			// Assign outputs
			__context->out1 = function21.RET_VAL;
			goto l11;
		//assert(false);
		return;  			}
	l11: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void function2(__function2 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init2;
	init2: {
		if (__context->in1) {
			__context->RET_VAL = __context->in2;
			goto l5;
		}
		if ((! __context->in1)) {
			__context->RET_VAL = (- __context->in2);
			goto l5;
		}
		//assert(false);
		return;  			}
	l5: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init3;
	init3: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			simplecode_OB1();
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
int main() {
	// Initial values
	MX123_7 = false;
	function11.out1 = 0;
	function21.in1 = false;
	function21.in2 = 0;
	function21.RET_VAL = 0;
	__assertion_error = 0;
	
	VerificationLoop();
}
