#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Variables
bool IX0_0 = false;
bool basic_OB1_result = false;
bool basic_foo_in = false;
int16_t basic_foo_in2 = 0;
bool basic_foo_temp = false;
bool basic_foo_RET_VAL = false;
uint16_t __assertion_error = 0;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			IX0_0 = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			basic_OB1_result = false;
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			basic_foo_in = IX0_0;
			basic_foo_in2 = 5;
			goto verificationLoop_VerificationLoop_basic_OB1_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
		if ((! (! (!(basic_OB1_result) || (! IX0_0))))) {
			goto verificationLoop_VerificationLoop_end;
		}
		if ((! (!(basic_OB1_result) || (! IX0_0)))) {
			__assertion_error = 1;
			goto verificationLoop_VerificationLoop_end;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_end: {
			goto callEnd;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_basic_OB1_init: {
			basic_foo_temp = false;
			goto verificationLoop_VerificationLoop_basic_OB1_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_basic_OB1_l1: {
		if ((basic_foo_in2 > 0)) {
			goto verificationLoop_VerificationLoop_basic_OB1_l2;
		}
		if ((! (basic_foo_in2 > 0))) {
			goto verificationLoop_VerificationLoop_basic_OB1_l4;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_basic_OB1_l2: {
			basic_foo_temp = (! basic_foo_in);
			goto verificationLoop_VerificationLoop_basic_OB1_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_basic_OB1_l3: {
			goto verificationLoop_VerificationLoop_basic_OB1_l6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_basic_OB1_l4: {
			basic_foo_temp = basic_foo_in;
			goto verificationLoop_VerificationLoop_basic_OB1_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_basic_OB1_l5: {
			goto verificationLoop_VerificationLoop_basic_OB1_l6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_basic_OB1_l6: {
			basic_foo_RET_VAL = basic_foo_temp;
			goto verificationLoop_VerificationLoop_basic_OB1_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_basic_OB1_l7: {
			basic_OB1_result = basic_foo_RET_VAL;
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
