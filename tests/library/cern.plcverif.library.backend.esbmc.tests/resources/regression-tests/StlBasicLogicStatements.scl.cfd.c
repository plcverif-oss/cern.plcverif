#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Root data structure
typedef struct {
	bool a;
	bool b;
	bool c;
	bool __RLO;
	bool __NFC;
	bool __BR;
	bool __STA;
	bool __OR;
	bool __CC0;
	bool __CC1;
} __simpletest_fb1;

// Global variables
bool QX0_0;
bool QX0_1;
bool QX0_2;
bool QX0_3;
bool QX0_4;
__simpletest_fb1 simpletest_db1;
uint16_t __assertion_error;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Automata declarations
void simpletest_fb1(__simpletest_fb1 *__context);
void VerificationLoop();

// Automata
void simpletest_fb1(__simpletest_fb1 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			__context->__OR = false;
			__context->__STA = true;
			__context->__RLO = true;
			__context->__CC0 = false;
			__context->__CC1 = false;
			__context->__BR = false;
			__context->__NFC = false;
			goto l1;
		//assert(false);
		return;  			}
	l1: {
			__context->__RLO = ((__context->__NFC && __context->__RLO) || __context->a);
			__context->__STA = __context->a;
			goto l3;
		//assert(false);
		return;  			}
	l3: {
			__context->__NFC = true;
			__context->__OR = false;
			goto l5;
		//assert(false);
		return;  			}
	l5: {
			__context->__RLO = ((__context->__NFC && __context->__RLO) || __context->b);
			__context->__STA = __context->b;
			goto l7;
		//assert(false);
		return;  			}
	l7: {
			__context->__NFC = true;
			__context->__OR = false;
			goto l9;
		//assert(false);
		return;  			}
	l9: {
			__context->__RLO = ((__context->__NFC && __context->__RLO) || __context->c);
			__context->__STA = __context->c;
			goto l11;
		//assert(false);
		return;  			}
	l11: {
			__context->__NFC = true;
			__context->__OR = false;
			goto l13;
		//assert(false);
		return;  			}
	l13: {
			QX0_0 = __context->__RLO;
			__context->__OR = false;
			__context->__STA = __context->__RLO;
			__context->__NFC = false;
			goto l17;
		//assert(false);
		return;  			}
	l17: {
		if ((! (QX0_0 == ((__context->a || __context->b) || __context->c)))) {
			__assertion_error = 1;
			goto l70;
		}
		if ((! (! (QX0_0 == ((__context->a || __context->b) || __context->c))))) {
			__context->__RLO = (((! __context->__NFC) || __context->__RLO) && (__context->a || __context->__OR));
			__context->__STA = __context->a;
			goto l19;
		}
		//assert(false);
		return;  			}
	l19: {
			__context->__NFC = true;
			goto l20;
		//assert(false);
		return;  			}
	l20: {
			__context->__RLO = (((! __context->__NFC) || __context->__RLO) && (__context->b || __context->__OR));
			__context->__STA = __context->b;
			goto l22;
		//assert(false);
		return;  			}
	l22: {
			__context->__NFC = true;
			goto l23;
		//assert(false);
		return;  			}
	l23: {
			__context->__RLO = (((! __context->__NFC) || __context->__RLO) && (__context->c || __context->__OR));
			__context->__STA = __context->c;
			goto l25;
		//assert(false);
		return;  			}
	l25: {
			__context->__NFC = true;
			QX0_1 = __context->__RLO;
			__context->__OR = false;
			__context->__STA = __context->__RLO;
			goto l29;
		//assert(false);
		return;  			}
	l29: {
			__context->__NFC = false;
			goto l30;
		//assert(false);
		return;  			}
	l30: {
		if ((! (QX0_1 == ((__context->a && __context->b) && __context->c)))) {
			__assertion_error = 2;
			goto l70;
		}
		if ((! (! (QX0_1 == ((__context->a && __context->b) && __context->c))))) {
			__context->__RLO = true;
			goto l31;
		}
		//assert(false);
		return;  			}
	l31: {
			QX0_2 = __context->__RLO;
			__context->__OR = false;
			__context->__STA = __context->__RLO;
			__context->__NFC = false;
			goto l35;
		//assert(false);
		return;  			}
	l35: {
		if ((! (QX0_2 == true))) {
			__assertion_error = 3;
			goto l70;
		}
		if ((! (! (QX0_2 == true)))) {
			__context->__RLO = false;
			goto l36;
		}
		//assert(false);
		return;  			}
	l36: {
			QX0_3 = __context->__RLO;
			__context->__OR = false;
			__context->__STA = __context->__RLO;
			__context->__NFC = false;
			goto l40;
		//assert(false);
		return;  			}
	l40: {
		if ((! (QX0_3 == false))) {
			__assertion_error = 4;
			goto l70;
		}
		if ((! (! (QX0_3 == false)))) {
			__context->__RLO = (((! __context->__NFC) || __context->__RLO) && (__context->a || __context->__OR));
			__context->__STA = __context->a;
			goto l42;
		}
		//assert(false);
		return;  			}
	l42: {
			__context->__NFC = true;
			goto l43;
		//assert(false);
		return;  			}
	l43: {
			__context->__RLO = (((! __context->__NFC) || __context->__RLO) && (__context->b || __context->__OR));
			__context->__STA = __context->b;
			goto l45;
		//assert(false);
		return;  			}
	l45: {
			__context->__NFC = true;
			goto l46;
		//assert(false);
		return;  			}
	l46: {
			__context->__RLO = ((__context->__NFC && __context->__RLO) || __context->c);
			__context->__STA = __context->c;
			goto l48;
		//assert(false);
		return;  			}
	l48: {
			__context->__NFC = true;
			__context->__OR = false;
			__context->__RLO = (! __context->__RLO);
			goto l51;
		//assert(false);
		return;  			}
	l51: {
			QX0_4 = __context->__RLO;
			__context->__OR = false;
			__context->__STA = __context->__RLO;
			__context->__NFC = false;
			goto l55;
		//assert(false);
		return;  			}
	l55: {
		if ((! (QX0_4 == (! ((__context->a && __context->b) || __context->c))))) {
			__assertion_error = 5;
			goto l70;
		}
		if ((! (! (QX0_4 == (! ((__context->a && __context->b) || __context->c)))))) {
			__context->__RLO = (((! __context->__NFC) || __context->__RLO) && (__context->a || __context->__OR));
			__context->__STA = __context->a;
			goto l57;
		}
		//assert(false);
		return;  			}
	l57: {
			__context->__NFC = true;
			goto l58;
		//assert(false);
		return;  			}
	l58: {
			__context->__RLO = (((! __context->__NFC) || __context->__RLO) && (__context->b || __context->__OR));
			__context->__STA = __context->b;
			goto l60;
		//assert(false);
		return;  			}
	l60: {
			__context->__NFC = true;
			__context->__STA = true;
			goto l62;
		//assert(false);
		return;  			}
	l62: {
			__context->__OR = (__context->__NFC && (__context->__OR || __context->__RLO));
			goto l63;
		//assert(false);
		return;  			}
	l63: {
			__context->__NFC = false;
			goto l64;
		//assert(false);
		return;  			}
	l64: {
			__context->__RLO = (((! __context->__NFC) || __context->__RLO) && (__context->c || __context->__OR));
			__context->__STA = __context->c;
			goto l66;
		//assert(false);
		return;  			}
	l66: {
			__context->__NFC = true;
			__context->__STA = true;
			goto l68;
		//assert(false);
		return;  			}
	l68: {
			__context->__OR = (__context->__NFC && (__context->__OR || __context->__RLO));
			goto l69;
		//assert(false);
		return;  			}
	l69: {
			__context->__NFC = false;
			goto l70;
		//assert(false);
		return;  			}
	l70: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			simpletest_db1.a = nondet_bool();
			simpletest_db1.b = nondet_bool();
			simpletest_db1.c = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			simpletest_fb1(&simpletest_db1);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
int main() {
	// Initial values
	QX0_0 = false;
	QX0_1 = false;
	QX0_2 = false;
	QX0_3 = false;
	QX0_4 = false;
	simpletest_db1.a = false;
	simpletest_db1.b = false;
	simpletest_db1.c = false;
	simpletest_db1.__RLO = false;
	simpletest_db1.__NFC = false;
	simpletest_db1.__BR = false;
	simpletest_db1.__STA = false;
	simpletest_db1.__OR = false;
	simpletest_db1.__CC0 = false;
	simpletest_db1.__CC1 = false;
	__assertion_error = 0;
	
	VerificationLoop();
}
