#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Variables
bool variables_db1_a1 = false;
bool variables_db1_a2 = false;
bool variables_db1_a3 = true;
int16_t variables_db1_b1 = 0;
int16_t variables_db1_b2 = 0;
int16_t variables_db1_b3 = 123;
int16_t variables_db1_b4 = -234;
uint16_t __assertion_error = 0;
bool variables_db2_arr1_0 = false;
bool variables_db2_arr1_1 = false;
bool variables_db2_arr1_2 = false;
bool variables_db2_arr1_3 = false;
bool variables_db2_arr1_4 = false;
bool variables_db2_arr1_5 = false;
bool variables_db2_arr1_6 = false;
bool variables_db2_arr1_7 = false;
bool variables_db2_arr2_0_1 = false;
bool variables_db2_arr2_0_2 = false;
bool variables_db2_arr2_0_3 = false;
bool variables_db2_arr2_1_1 = false;
bool variables_db2_arr2_1_2 = false;
bool variables_db2_arr2_1_3 = false;
bool variables_db2_arr2_2_1 = false;
bool variables_db2_arr2_2_2 = false;
bool variables_db2_arr2_2_3 = false;
bool variables_db2_arr2_3_1 = false;
bool variables_db2_arr2_3_2 = false;
bool variables_db2_arr2_3_3 = false;
bool variables_db2_arr2_4_1 = false;
bool variables_db2_arr2_4_2 = false;
bool variables_db2_arr2_4_3 = false;
bool variables_db2_arr2_5_1 = false;
bool variables_db2_arr2_5_2 = false;
bool variables_db2_arr2_5_3 = false;
bool variables_db2_arr2_6_1 = false;
bool variables_db2_arr2_6_2 = false;
bool variables_db2_arr2_6_3 = false;
bool variables_db2_arr2_7_1 = false;
bool variables_db2_arr2_7_2 = false;
bool variables_db2_arr2_7_3 = false;
int16_t variables_db2_arr3_0 = 0;
int16_t variables_db2_arr3_1 = 0;
int16_t variables_db2_arr3_2 = 0;
int16_t variables_db2_arr3_3 = 0;
int16_t variables_db2_arr3_4 = 0;
int16_t variables_db2_arr3_5 = 0;
int16_t variables_db2_arr3_6 = 0;
int16_t variables_db2_arr3_7 = 0;
bool variables_db2_arr4_1_s1 = false;
bool variables_db2_arr4_2_s1 = false;
bool variables_db2_arr4_3_s1 = false;
bool variables_db2_arr4_4_s1 = false;
int32_t variables_db2_arr4_1_s2 = 0;
int32_t variables_db2_arr4_2_s2 = 0;
int32_t variables_db2_arr4_3_s2 = 0;
int32_t variables_db2_arr4_4_s2 = 0;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			goto verificationLoop_VerificationLoop_variables_OB1_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			goto verificationLoop_VerificationLoop_variables_OB1_init1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
			goto callEnd;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_variables_OB1_init: {
			goto verificationLoop_VerificationLoop_variables_OB1_end;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_variables_OB1_end: {
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_variables_OB1_init1: {
			goto verificationLoop_VerificationLoop_variables_OB1_end1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_variables_OB1_end1: {
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
