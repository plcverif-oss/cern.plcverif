#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Variables
int16_t instance_Perst_index = 0;
int16_t instance_Perst_FEType = 0;
int16_t instance_Perst_InterfaceParam1 = 0;
int16_t instance_Perst_InterfaceParam2 = 0;
float instance_Perst_AuPosR = 0.0;
float instance_Perst_PosSt = 0.0;
bool instance_Perst_PIWDef = false;
bool instance_Perst_FOFEn = false;
uint16_t __assertion_error = 0;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			instance_Perst_AuPosR = nondet_float();
			instance_Perst_FEType = nondet_int16_t();
			instance_Perst_FOFEn = nondet_bool();
			instance_Perst_InterfaceParam1 = nondet_int16_t();
			instance_Perst_InterfaceParam2 = nondet_int16_t();
			instance_Perst_PIWDef = nondet_bool();
			instance_Perst_PosSt = nondet_float();
			instance_Perst_index = nondet_int16_t();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			instance_Perst_PosSt = instance_Perst_AuPosR;
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			goto callEnd;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
