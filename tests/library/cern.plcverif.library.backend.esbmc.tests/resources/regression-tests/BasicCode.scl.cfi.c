#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Variables
bool IX0_0 = false;
bool basic_OB1_result = false;
uint16_t __assertion_error = 0;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			IX0_0 = nondet_bool();
			goto prepare_BoC;
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			basic_OB1_result = (! IX0_0);
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
		if ((! (! (!(basic_OB1_result) || (! IX0_0))))) {
			goto verificationLoop_VerificationLoop_end;
		}
		if ((! (!(basic_OB1_result) || (! IX0_0)))) {
			__assertion_error = 1;
			goto verificationLoop_VerificationLoop_end;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_end: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
