#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Root data structure
typedef struct {
	int16_t I;
} __caseFunction1;
typedef struct {
	int16_t I;
} __caseFunction2;
typedef struct {
	int16_t I;
} __caseFunction3;
typedef struct {
	int16_t I;
} __caseFunction4;
typedef struct {
	int16_t I;
} __caseFunction5;

// Global variables
bool MX1_0;
bool MX1_1;
bool MX1_2;
bool MX1_3;
bool MX1_4;
bool MX1_5;
bool MX2_0;
bool MX2_1;
bool MX2_2;
bool MX2_3;
bool MX2_4;
bool MX2_5;
bool MX3_0;
bool MX3_1;
bool MX3_2;
bool MX3_3;
bool MX4_0;
bool MX4_1;
bool MX4_2;
bool MX4_3;
bool MX5_0;
bool MX5_1;
bool MX5_2;
bool MX5_3;
__caseFunction1 caseFunction11;
__caseFunction2 caseFunction21;
__caseFunction3 caseFunction31;
__caseFunction4 caseFunction41;
__caseFunction5 caseFunction51;
uint16_t __assertion_error;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Automata declarations
void caseFunction1(__caseFunction1 *__context);
void caseFunction2(__caseFunction2 *__context);
void caseFunction3(__caseFunction3 *__context);
void caseFunction4(__caseFunction4 *__context);
void caseFunction5(__caseFunction5 *__context);
void caseFunction_OB1();
void VerificationLoop();

// Automata
void caseFunction1(__caseFunction1 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			MX1_0 = true;
			goto l1;
		//assert(false);
		return;  			}
	l1: {
		if ((__context->I == 1)) {
			MX1_1 = false;
			goto l2;
		}
		if ((! (__context->I == 1))) {
			goto l4;
		}
		//assert(false);
		return;  			}
	l2: {
			MX1_5 = true;
			goto l13;
		//assert(false);
		return;  			}
	l4: {
		if ((__context->I == 2)) {
			MX1_2 = false;
			goto l2;
		}
		if ((! (__context->I == 2))) {
			goto l7;
		}
		//assert(false);
		return;  			}
	l7: {
		if ((__context->I == 3)) {
			MX1_3 = false;
			goto l2;
		}
		if ((! (__context->I == 3))) {
			MX1_4 = false;
			goto l2;
		}
		//assert(false);
		return;  			}
	l13: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void caseFunction2(__caseFunction2 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			MX2_0 = true;
			goto l11;
		//assert(false);
		return;  			}
	l11: {
		if ((((__context->I * 2) >= 1) && ((__context->I * 2) <= 3))) {
			MX2_1 = false;
			goto l21;
		}
		if ((! (((__context->I * 2) >= 1) && ((__context->I * 2) <= 3)))) {
			goto l41;
		}
		//assert(false);
		return;  			}
	l21: {
			MX2_5 = true;
			goto l19;
		//assert(false);
		return;  			}
	l41: {
		if ((((__context->I * 2) >= 2) && ((__context->I * 2) <= 4))) {
			MX2_2 = false;
			goto l21;
		}
		if ((! (((__context->I * 2) >= 2) && ((__context->I * 2) <= 4)))) {
			goto l71;
		}
		//assert(false);
		return;  			}
	l71: {
		if ((((__context->I * 2) == 6) || (((__context->I * 2) == 8) || ((__context->I * 2) == 10)))) {
			MX2_3 = false;
			goto l21;
		}
		if ((! (((__context->I * 2) == 6) || (((__context->I * 2) == 8) || ((__context->I * 2) == 10))))) {
			goto l10;
		}
		//assert(false);
		return;  			}
	l10: {
		if ((((__context->I * 2) >= 11) && ((__context->I * 2) <= 13))) {
			MX2_4 = false;
			goto l21;
		}
		if ((! (((__context->I * 2) >= 11) && ((__context->I * 2) <= 13)))) {
			goto l131;
		}
		//assert(false);
		return;  			}
	l131: {
		if ((((__context->I * 2) >= 12) && ((__context->I * 2) <= 14))) {
			MX2_5 = false;
			goto l21;
		}
		if ((! (((__context->I * 2) >= 12) && ((__context->I * 2) <= 14)))) {
			MX2_4 = false;
			goto l21;
		}
		//assert(false);
		return;  			}
	l19: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void caseFunction3(__caseFunction3 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init2;
	init2: {
			MX3_0 = true;
			goto l12;
		//assert(false);
		return;  			}
	l12: {
		if ((__context->I == 1)) {
			MX3_1 = false;
			goto l22;
		}
		if ((! (__context->I == 1))) {
			goto l42;
		}
		//assert(false);
		return;  			}
	l22: {
			MX3_3 = true;
			goto l9;
		//assert(false);
		return;  			}
	l42: {
		if (((__context->I == 2) || ((__context->I == 3) || ((__context->I == 5) || (__context->I == 7))))) {
			MX3_2 = false;
			goto l22;
		}
		if ((! ((__context->I == 2) || ((__context->I == 3) || ((__context->I == 5) || (__context->I == 7)))))) {
			goto l22;
		}
		//assert(false);
		return;  			}
	l9: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void caseFunction4(__caseFunction4 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init3;
	init3: {
			MX4_0 = true;
			goto l14;
		//assert(false);
		return;  			}
	l14: {
		if (((((int32_t) __context->I) * 2) == 1)) {
			MX4_1 = false;
			goto l23;
		}
		if ((! ((((int32_t) __context->I) * 2) == 1))) {
			goto l43;
		}
		//assert(false);
		return;  			}
	l23: {
			MX4_3 = true;
			goto l91;
		//assert(false);
		return;  			}
	l43: {
		if ((((((int32_t) __context->I) * 2) == 2) || (((((int32_t) __context->I) * 2) == 3) || (((((int32_t) __context->I) * 2) == 5) || ((((int32_t) __context->I) * 2) == 7))))) {
			MX4_2 = false;
			goto l23;
		}
		if ((! (((((int32_t) __context->I) * 2) == 2) || (((((int32_t) __context->I) * 2) == 3) || (((((int32_t) __context->I) * 2) == 5) || ((((int32_t) __context->I) * 2) == 7)))))) {
			goto l23;
		}
		//assert(false);
		return;  			}
	l91: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void caseFunction5(__caseFunction5 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init4;
	init4: {
			MX5_0 = true;
			goto l15;
		//assert(false);
		return;  			}
	l15: {
		if ((((__context->I * 2) >= 1) && ((__context->I * 2) <= 5))) {
			MX5_1 = false;
			goto l24;
		}
		if ((! (((__context->I * 2) >= 1) && ((__context->I * 2) <= 5)))) {
			goto l44;
		}
		//assert(false);
		return;  			}
	l24: {
			MX5_3 = true;
			goto l92;
		//assert(false);
		return;  			}
	l44: {
		if ((((__context->I * 2) == 6) || (((__context->I * 2) >= 8) && ((__context->I * 2) <= 10)))) {
			MX5_2 = false;
			goto l24;
		}
		if ((! (((__context->I * 2) == 6) || (((__context->I * 2) >= 8) && ((__context->I * 2) <= 10))))) {
			goto l24;
		}
		//assert(false);
		return;  			}
	l92: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void caseFunction_OB1() {
	// Temporary variables
	
	// Start with initial location
	goto init5;
	init5: {
			// Assign inputs
			caseFunction11.I = 1;
			caseFunction1(&caseFunction11);
			// Assign outputs
			goto l16;
		//assert(false);
		return;  			}
	l16: {
			// Assign inputs
			caseFunction21.I = 1;
			caseFunction2(&caseFunction21);
			// Assign outputs
			goto l25;
		//assert(false);
		return;  			}
	l25: {
			// Assign inputs
			caseFunction31.I = 1;
			caseFunction3(&caseFunction31);
			// Assign outputs
			goto l3;
		//assert(false);
		return;  			}
	l3: {
			// Assign inputs
			caseFunction41.I = 1;
			caseFunction4(&caseFunction41);
			// Assign outputs
			goto l45;
		//assert(false);
		return;  			}
	l45: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init6;
	init6: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			caseFunction_OB1();
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
int main() {
	// Initial values
	MX1_0 = false;
	MX1_1 = false;
	MX1_2 = false;
	MX1_3 = false;
	MX1_4 = false;
	MX1_5 = false;
	MX2_0 = false;
	MX2_1 = false;
	MX2_2 = false;
	MX2_3 = false;
	MX2_4 = false;
	MX2_5 = false;
	MX3_0 = false;
	MX3_1 = false;
	MX3_2 = false;
	MX3_3 = false;
	MX4_0 = false;
	MX4_1 = false;
	MX4_2 = false;
	MX4_3 = false;
	MX5_0 = false;
	MX5_1 = false;
	MX5_2 = false;
	MX5_3 = false;
	caseFunction11.I = 0;
	caseFunction21.I = 0;
	caseFunction31.I = 0;
	caseFunction41.I = 0;
	caseFunction51.I = 0;
	__assertion_error = 0;
	
	VerificationLoop();
}
