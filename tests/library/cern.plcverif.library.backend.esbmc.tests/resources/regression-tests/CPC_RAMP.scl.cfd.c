#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Root data structure
typedef struct {
	float INV;
	float InSpd;
	float DeSpd;
	float H_LM;
	float L_LM;
	float PV;
	float DF_OUTV;
	bool DFOUT_ON;
	bool TRACK;
	bool MAN_ON;
	bool COM_RST;
	int32_t CYCLE;
	float OUTV;
	bool DONE;
	float INV_sat;
} __RAMP;

// Global variables
__RAMP instance;
uint16_t __assertion_error;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Automata declarations
void RAMP(__RAMP *__context);
void VerificationLoop();

// Automata
void RAMP(__RAMP *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
		if ((__context->INV > __context->H_LM)) {
			__context->INV_sat = __context->H_LM;
			goto l7;
		}
		if (((! (__context->INV > __context->H_LM)) && (__context->INV < __context->L_LM))) {
			__context->INV_sat = __context->L_LM;
			goto l7;
		}
		if (((! (__context->INV > __context->H_LM)) && (! ((! (__context->INV > __context->H_LM)) && (__context->INV < __context->L_LM))))) {
			__context->INV_sat = __context->INV;
			goto l7;
		}
		//assert(false);
		return;  			}
	l7: {
		if (__context->COM_RST) {
			__context->OUTV = 0.0;
			__context->DONE = false;
			goto l53;
		}
		if (((! __context->COM_RST) && __context->TRACK)) {
			goto l11;
		}
		if ((((! __context->COM_RST) && (! __context->TRACK)) && __context->MAN_ON)) {
			goto l20;
		}
		if ((((! __context->COM_RST) && ((! __context->TRACK) && (! __context->MAN_ON))) && __context->DFOUT_ON)) {
			goto l29;
		}
		if (((! __context->COM_RST) && ((! ((! __context->COM_RST) && __context->TRACK)) && ((! (((! __context->COM_RST) && (! __context->TRACK)) && __context->MAN_ON)) && (! (((! __context->COM_RST) && ((! __context->TRACK) && (! __context->MAN_ON))) && __context->DFOUT_ON)))))) {
			goto l38;
		}
		//assert(false);
		return;  			}
	l11: {
		if ((__context->INV > __context->H_LM)) {
			__context->OUTV = __context->H_LM;
			goto l18;
		}
		if (((! (__context->INV > __context->H_LM)) && (__context->INV < __context->L_LM))) {
			__context->OUTV = __context->L_LM;
			goto l18;
		}
		if (((! (__context->INV > __context->H_LM)) && (! ((! (__context->INV > __context->H_LM)) && (__context->INV < __context->L_LM))))) {
			__context->OUTV = __context->INV;
			goto l18;
		}
		//assert(false);
		return;  			}
	l18: {
			__context->DONE = false;
			goto l53;
		//assert(false);
		return;  			}
	l20: {
		if ((__context->PV > __context->H_LM)) {
			__context->OUTV = __context->H_LM;
			goto l27;
		}
		if (((! (__context->PV > __context->H_LM)) && (__context->PV < __context->L_LM))) {
			__context->OUTV = __context->L_LM;
			goto l27;
		}
		if (((! (__context->PV > __context->H_LM)) && (! ((! (__context->PV > __context->H_LM)) && (__context->PV < __context->L_LM))))) {
			__context->OUTV = __context->PV;
			goto l27;
		}
		//assert(false);
		return;  			}
	l27: {
			__context->DONE = false;
			goto l53;
		//assert(false);
		return;  			}
	l29: {
		if ((__context->DF_OUTV > __context->H_LM)) {
			__context->OUTV = __context->H_LM;
			goto l36;
		}
		if (((! (__context->DF_OUTV > __context->H_LM)) && (__context->DF_OUTV < __context->L_LM))) {
			__context->OUTV = __context->L_LM;
			goto l36;
		}
		if (((! (__context->DF_OUTV > __context->H_LM)) && (! ((! (__context->DF_OUTV > __context->H_LM)) && (__context->DF_OUTV < __context->L_LM))))) {
			__context->OUTV = __context->DF_OUTV;
			goto l36;
		}
		//assert(false);
		return;  			}
	l36: {
			__context->DONE = false;
			goto l53;
		//assert(false);
		return;  			}
	l38: {
		if ((__context->OUTV < (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))) {
			__context->OUTV = (__context->OUTV + ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0));
			__context->DONE = false;
			goto l52;
		}
		if (((! (__context->OUTV < (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))) && ((__context->OUTV < __context->INV_sat) && (__context->OUTV > (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))))) {
			__context->OUTV = __context->INV_sat;
			__context->DONE = true;
			goto l52;
		}
		if ((((! (__context->OUTV < (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))) && (! ((__context->OUTV < __context->INV_sat) && (__context->OUTV > (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))))) && (__context->OUTV > (__context->INV_sat + ((__context->DeSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0))))) {
			__context->OUTV = (__context->OUTV - ((__context->DeSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0));
			__context->DONE = false;
			goto l52;
		}
		if ((((! (__context->OUTV < (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))) && ((! ((__context->OUTV < __context->INV_sat) && (__context->OUTV > (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0))))) && (! (__context->OUTV > (__context->INV_sat + ((__context->DeSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))))) && ((__context->OUTV > __context->INV_sat) && (__context->OUTV < (__context->INV_sat + ((__context->DeSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))))) {
			__context->OUTV = __context->INV_sat;
			__context->DONE = true;
			goto l52;
		}
		if (((! (__context->OUTV < (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))) && ((! ((! (__context->OUTV < (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))) && ((__context->OUTV < __context->INV_sat) && (__context->OUTV > (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))))) && ((! (((! (__context->OUTV < (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))) && (! ((__context->OUTV < __context->INV_sat) && (__context->OUTV > (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))))) && (__context->OUTV > (__context->INV_sat + ((__context->DeSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0))))) && (! (((! (__context->OUTV < (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))) && ((! ((__context->OUTV < __context->INV_sat) && (__context->OUTV > (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0))))) && (! (__context->OUTV > (__context->INV_sat + ((__context->DeSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))))) && ((__context->OUTV > __context->INV_sat) && (__context->OUTV < (__context->INV_sat + ((__context->DeSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))))))))) {
			goto l52;
		}
		//assert(false);
		return;  			}
	l52: {
			goto l53;
		//assert(false);
		return;  			}
	l53: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			instance.COM_RST = nondet_bool();
			instance.CYCLE = nondet_int32_t();
			instance.DFOUT_ON = nondet_bool();
			instance.DF_OUTV = nondet_float();
			instance.DeSpd = nondet_float();
			instance.H_LM = nondet_float();
			instance.INV = nondet_float();
			instance.InSpd = nondet_float();
			instance.L_LM = nondet_float();
			instance.MAN_ON = nondet_bool();
			instance.PV = nondet_float();
			instance.TRACK = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			RAMP(&instance);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
int main() {
	// Initial values
	instance.INV = 0.0;
	instance.InSpd = 0.0;
	instance.DeSpd = 0.0;
	instance.H_LM = 0.0;
	instance.L_LM = 0.0;
	instance.PV = 0.0;
	instance.DF_OUTV = 0.0;
	instance.DFOUT_ON = false;
	instance.TRACK = false;
	instance.MAN_ON = false;
	instance.COM_RST = false;
	instance.CYCLE = 0;
	instance.OUTV = 0.0;
	instance.DONE = false;
	instance.INV_sat = 0.0;
	__assertion_error = 0;
	
	VerificationLoop();
}
