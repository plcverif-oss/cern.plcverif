#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Root data structure
typedef struct {
	int16_t in1;
	int16_t in2;
	int16_t RET_VAL;
} __MAX0;
typedef struct {
	int16_t j;
} __extractNested1;
typedef struct {
	int16_t j;
} __extractNested2;
typedef struct {
	int16_t j;
} __extractNested3;
typedef struct {
	int16_t j;
} __extractNested4;

// Global variables
__extractNested1 extractNested11;
__extractNested2 extractNested21;
__extractNested3 extractNested31;
__extractNested4 extractNested41;
__MAX0 MAX01;
__MAX0 MAX01_inlined_1;
__MAX0 MAX01_inlined_2;
__MAX0 MAX01_inlined_3;
__MAX0 MAX01_inlined_4;
__MAX0 MAX01_inlined_5;
__MAX0 MAX01_inlined_6;
__MAX0 MAX01_inlined_7;
__MAX0 MAX01_inlined_8;
__MAX0 MAX01_inlined_9;
uint16_t __assertion_error;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Automata declarations
void extractNested1(__extractNested1 *__context);
void extractNested2(__extractNested2 *__context);
void extractNested3(__extractNested3 *__context);
void extractNested4(__extractNested4 *__context);
void MAX0(__MAX0 *__context);
void extractNested_OB1();
void VerificationLoop();

// Automata
void extractNested1(__extractNested1 *__context) {
	// Temporary variables
	int16_t ___nested_ret_val1;
	
	// Start with initial location
	goto init;
	init: {
			___nested_ret_val1 = 0;
			goto l1;
		//assert(false);
		return;  			}
	l1: {
			// Assign inputs
			MAX01_inlined_1.in1 = 2;
			MAX01_inlined_1.in2 = 3;
			MAX0(&MAX01_inlined_1);
			// Assign outputs
			___nested_ret_val1 = MAX01_inlined_1.RET_VAL;
			goto l2;
		//assert(false);
		return;  			}
	l2: {
			// Assign inputs
			MAX01_inlined_2.in1 = 1;
			MAX01_inlined_2.in2 = ___nested_ret_val1;
			MAX0(&MAX01_inlined_2);
			// Assign outputs
			__context->j = MAX01_inlined_2.RET_VAL;
			goto l3;
		//assert(false);
		return;  			}
	l3: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void extractNested2(__extractNested2 *__context) {
	// Temporary variables
	int16_t ___nested_ret_val2;
	int16_t ___nested_ret_val3;
	
	// Start with initial location
	goto init1;
	init1: {
			___nested_ret_val2 = 0;
			___nested_ret_val3 = 0;
			goto l11;
		//assert(false);
		return;  			}
	l11: {
			// Assign inputs
			MAX01_inlined_3.in1 = 3;
			MAX01_inlined_3.in2 = 4;
			MAX0(&MAX01_inlined_3);
			// Assign outputs
			___nested_ret_val3 = MAX01_inlined_3.RET_VAL;
			goto l21;
		//assert(false);
		return;  			}
	l21: {
			// Assign inputs
			MAX01_inlined_4.in1 = 2;
			MAX01_inlined_4.in2 = ((2 * ___nested_ret_val3) + 6);
			MAX0(&MAX01_inlined_4);
			// Assign outputs
			___nested_ret_val2 = MAX01_inlined_4.RET_VAL;
			goto l31;
		//assert(false);
		return;  			}
	l31: {
			// Assign inputs
			MAX01_inlined_5.in1 = 1;
			MAX01_inlined_5.in2 = (___nested_ret_val2 + 5);
			MAX0(&MAX01_inlined_5);
			// Assign outputs
			__context->j = MAX01_inlined_5.RET_VAL;
			goto l4;
		//assert(false);
		return;  			}
	l4: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void extractNested3(__extractNested3 *__context) {
	// Temporary variables
	int16_t ___nested_ret_val4;
	int16_t ___nested_ret_val5;
	
	// Start with initial location
	goto init2;
	init2: {
			___nested_ret_val4 = 0;
			___nested_ret_val5 = 0;
			goto l12;
		//assert(false);
		return;  			}
	l12: {
			// Assign inputs
			MAX01_inlined_6.in1 = 11;
			MAX01_inlined_6.in2 = 22;
			MAX0(&MAX01_inlined_6);
			// Assign outputs
			___nested_ret_val4 = MAX01_inlined_6.RET_VAL;
			goto l22;
		//assert(false);
		return;  			}
	l22: {
			// Assign inputs
			MAX01_inlined_7.in1 = 33;
			MAX01_inlined_7.in2 = 44;
			MAX0(&MAX01_inlined_7);
			// Assign outputs
			___nested_ret_val5 = MAX01_inlined_7.RET_VAL;
			goto l32;
		//assert(false);
		return;  			}
	l32: {
			// Assign inputs
			MAX01_inlined_8.in1 = ___nested_ret_val4;
			MAX01_inlined_8.in2 = ___nested_ret_val5;
			MAX0(&MAX01_inlined_8);
			// Assign outputs
			__context->j = MAX01_inlined_8.RET_VAL;
			goto l41;
		//assert(false);
		return;  			}
	l41: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void extractNested4(__extractNested4 *__context) {
	// Temporary variables
	int16_t ___nested_ret_val6;
	
	// Start with initial location
	goto init3;
	init3: {
			___nested_ret_val6 = 0;
			goto l13;
		//assert(false);
		return;  			}
	l13: {
			// Assign inputs
			MAX01_inlined_9.in1 = 0;
			MAX01_inlined_9.in2 = 1;
			MAX0(&MAX01_inlined_9);
			// Assign outputs
			___nested_ret_val6 = MAX01_inlined_9.RET_VAL;
			goto l23;
		//assert(false);
		return;  			}
	l23: {
			__context->j = (___nested_ret_val6 + 10);
			goto l33;
		//assert(false);
		return;  			}
	l33: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void MAX0(__MAX0 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init4;
	init4: {
		if ((__context->in1 > __context->in2)) {
			goto l14;
		}
		if ((! (__context->in1 > __context->in2))) {
			goto l34;
		}
		//assert(false);
		return;  			}
	l14: {
			__context->RET_VAL = __context->in1;
			goto l24;
		//assert(false);
		return;  			}
	l24: {
			goto l5;
		//assert(false);
		return;  			}
	l34: {
			__context->RET_VAL = __context->in2;
			goto l42;
		//assert(false);
		return;  			}
	l42: {
			goto l5;
		//assert(false);
		return;  			}
	l5: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void extractNested_OB1() {
	// Temporary variables
	
	// Start with initial location
	goto init5;
	init5: {
			// Assign inputs
			extractNested1(&extractNested11);
			// Assign outputs
			goto l15;
		//assert(false);
		return;  			}
	l15: {
			// Assign inputs
			extractNested2(&extractNested21);
			// Assign outputs
			goto l25;
		//assert(false);
		return;  			}
	l25: {
			// Assign inputs
			extractNested3(&extractNested31);
			// Assign outputs
			goto l35;
		//assert(false);
		return;  			}
	l35: {
			// Assign inputs
			extractNested4(&extractNested41);
			// Assign outputs
			goto l43;
		//assert(false);
		return;  			}
	l43: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init6;
	init6: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			extractNested_OB1();
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
int main() {
	// Initial values
	extractNested11.j = 0;
	extractNested21.j = 0;
	extractNested31.j = 0;
	extractNested41.j = 0;
	MAX01.in1 = 0;
	MAX01.in2 = 0;
	MAX01.RET_VAL = 0;
	MAX01_inlined_1.in1 = 0;
	MAX01_inlined_1.in2 = 0;
	MAX01_inlined_1.RET_VAL = 0;
	MAX01_inlined_2.in1 = 0;
	MAX01_inlined_2.in2 = 0;
	MAX01_inlined_2.RET_VAL = 0;
	MAX01_inlined_3.in1 = 0;
	MAX01_inlined_3.in2 = 0;
	MAX01_inlined_3.RET_VAL = 0;
	MAX01_inlined_4.in1 = 0;
	MAX01_inlined_4.in2 = 0;
	MAX01_inlined_4.RET_VAL = 0;
	MAX01_inlined_5.in1 = 0;
	MAX01_inlined_5.in2 = 0;
	MAX01_inlined_5.RET_VAL = 0;
	MAX01_inlined_6.in1 = 0;
	MAX01_inlined_6.in2 = 0;
	MAX01_inlined_6.RET_VAL = 0;
	MAX01_inlined_7.in1 = 0;
	MAX01_inlined_7.in2 = 0;
	MAX01_inlined_7.RET_VAL = 0;
	MAX01_inlined_8.in1 = 0;
	MAX01_inlined_8.in2 = 0;
	MAX01_inlined_8.RET_VAL = 0;
	MAX01_inlined_9.in1 = 0;
	MAX01_inlined_9.in2 = 0;
	MAX01_inlined_9.RET_VAL = 0;
	__assertion_error = 0;
	
	VerificationLoop();
}
