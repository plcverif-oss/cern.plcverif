#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Root data structure
typedef struct {
	int16_t I;
} __forFunction1;
typedef struct {
	int16_t I;
} __forFunction2;
typedef struct {
	int16_t I;
} __forFunction3;
typedef struct {
	int16_t I;
} __forFunction4;
typedef struct {
	int16_t I;
} __forFunction5;

// Global variables
bool MX0_1;
bool MX0_2;
bool MX0_3;
bool MX1_1;
bool MX1_2;
bool MX1_3;
bool MX2_1;
bool MX2_2;
bool MX2_3;
bool MX3_1;
bool MX3_2;
bool MX3_3;
bool MX3_4;
bool MX4_1;
bool MX4_2;
bool MX4_3;
bool MX4_4;
__forFunction1 forFunction11;
__forFunction2 forFunction21;
__forFunction3 forFunction31;
__forFunction4 forFunction41;
__forFunction5 forFunction51;
uint16_t __assertion_error;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Automata declarations
void forFunction1(__forFunction1 *__context);
void forFunction2(__forFunction2 *__context);
void forFunction3(__forFunction3 *__context);
void forFunction4(__forFunction4 *__context);
void forFunction5(__forFunction5 *__context);
void forFunction_OB1();
void VerificationLoop();

// Automata
void forFunction1(__forFunction1 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			MX0_1 = true;
			goto l1;
		//assert(false);
		return;  			}
	l1: {
			__context->I = 1;
			goto l5;
		//assert(false);
		return;  			}
	l2: {
			MX0_2 = true;
			goto l3;
		//assert(false);
		return;  			}
	l3: {
			__context->I = (__context->I + 1);
			goto l6;
		//assert(false);
		return;  			}
	l4: {
			MX0_3 = true;
			goto l7;
		//assert(false);
		return;  			}
	l5: {
		if ((__context->I <= 27)) {
			goto l2;
		}
		if ((! (__context->I <= 27))) {
			goto l4;
		}
		//assert(false);
		return;  			}
	l6: {
			goto l5;
		//assert(false);
		return;  			}
	l7: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void forFunction2(__forFunction2 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			MX1_1 = true;
			goto l11;
		//assert(false);
		return;  			}
	l11: {
			__context->I = 1;
			goto l51;
		//assert(false);
		return;  			}
	l21: {
			MX1_2 = true;
			goto l31;
		//assert(false);
		return;  			}
	l31: {
			__context->I = (__context->I + 3);
			goto l61;
		//assert(false);
		return;  			}
	l41: {
			MX1_3 = true;
			goto l71;
		//assert(false);
		return;  			}
	l51: {
		if ((__context->I <= 27)) {
			goto l21;
		}
		if ((! (__context->I <= 27))) {
			goto l41;
		}
		//assert(false);
		return;  			}
	l61: {
			goto l51;
		//assert(false);
		return;  			}
	l71: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void forFunction3(__forFunction3 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init2;
	init2: {
			MX2_1 = true;
			goto l12;
		//assert(false);
		return;  			}
	l12: {
			__context->I = 27;
			goto l52;
		//assert(false);
		return;  			}
	l22: {
			MX2_2 = true;
			goto l32;
		//assert(false);
		return;  			}
	l32: {
			__context->I = (__context->I + (- 2));
			goto l62;
		//assert(false);
		return;  			}
	l42: {
			MX2_3 = true;
			goto l72;
		//assert(false);
		return;  			}
	l52: {
		if ((__context->I >= 1)) {
			goto l22;
		}
		if ((! (__context->I >= 1))) {
			goto l42;
		}
		//assert(false);
		return;  			}
	l62: {
			goto l52;
		//assert(false);
		return;  			}
	l72: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void forFunction4(__forFunction4 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init3;
	init3: {
			MX3_1 = true;
			goto l13;
		//assert(false);
		return;  			}
	l13: {
			__context->I = 1;
			goto l10;
		//assert(false);
		return;  			}
	l23: {
			MX3_2 = true;
			goto l33;
		//assert(false);
		return;  			}
	l33: {
		if (((__context->I / 2) == 0)) {
			goto l43;
		}
		if ((! ((__context->I / 2) == 0))) {
			goto l63;
		}
		//assert(false);
		return;  			}
	l43: {
			goto l8;
		//assert(false);
		return;  			}
	l53: {
			goto l73;
		//assert(false);
		return;  			}
	l63: {
			goto l73;
		//assert(false);
		return;  			}
	l73: {
			MX3_3 = true;
			goto l8;
		//assert(false);
		return;  			}
	l8: {
			__context->I = (__context->I + 1);
			goto l111;
		//assert(false);
		return;  			}
	l9: {
			MX3_4 = true;
			goto l121;
		//assert(false);
		return;  			}
	l10: {
		if ((__context->I <= 10)) {
			goto l23;
		}
		if ((! (__context->I <= 10))) {
			goto l9;
		}
		//assert(false);
		return;  			}
	l111: {
			goto l10;
		//assert(false);
		return;  			}
	l121: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void forFunction5(__forFunction5 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init4;
	init4: {
			MX4_1 = true;
			goto l14;
		//assert(false);
		return;  			}
	l14: {
			__context->I = 1;
			goto l101;
		//assert(false);
		return;  			}
	l24: {
			MX4_2 = true;
			goto l34;
		//assert(false);
		return;  			}
	l34: {
		if (((__context->I / 2) == 0)) {
			goto l44;
		}
		if ((! ((__context->I / 2) == 0))) {
			goto l64;
		}
		//assert(false);
		return;  			}
	l44: {
			goto l91;
		//assert(false);
		return;  			}
	l54: {
			goto l74;
		//assert(false);
		return;  			}
	l64: {
			goto l74;
		//assert(false);
		return;  			}
	l74: {
			MX4_3 = true;
			goto l81;
		//assert(false);
		return;  			}
	l81: {
			__context->I = (__context->I + 1);
			goto l112;
		//assert(false);
		return;  			}
	l91: {
			MX4_4 = true;
			goto l122;
		//assert(false);
		return;  			}
	l101: {
		if ((__context->I <= 10)) {
			goto l24;
		}
		if ((! (__context->I <= 10))) {
			goto l91;
		}
		//assert(false);
		return;  			}
	l112: {
			goto l101;
		//assert(false);
		return;  			}
	l122: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void forFunction_OB1() {
	// Temporary variables
	
	// Start with initial location
	goto init5;
	init5: {
			// Assign inputs
			forFunction1(&forFunction11);
			// Assign outputs
			goto l15;
		//assert(false);
		return;  			}
	l15: {
			// Assign inputs
			forFunction2(&forFunction21);
			// Assign outputs
			goto l25;
		//assert(false);
		return;  			}
	l25: {
			// Assign inputs
			forFunction3(&forFunction31);
			// Assign outputs
			goto l35;
		//assert(false);
		return;  			}
	l35: {
			// Assign inputs
			forFunction4(&forFunction41);
			// Assign outputs
			goto l45;
		//assert(false);
		return;  			}
	l45: {
			// Assign inputs
			forFunction5(&forFunction51);
			// Assign outputs
			goto l55;
		//assert(false);
		return;  			}
	l55: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init6;
	init6: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			forFunction_OB1();
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
int main() {
	// Initial values
	MX0_1 = false;
	MX0_2 = false;
	MX0_3 = false;
	MX1_1 = false;
	MX1_2 = false;
	MX1_3 = false;
	MX2_1 = false;
	MX2_2 = false;
	MX2_3 = false;
	MX3_1 = false;
	MX3_2 = false;
	MX3_3 = false;
	MX3_4 = false;
	MX4_1 = false;
	MX4_2 = false;
	MX4_3 = false;
	MX4_4 = false;
	forFunction11.I = 0;
	forFunction21.I = 0;
	forFunction31.I = 0;
	forFunction41.I = 0;
	forFunction51.I = 0;
	__assertion_error = 0;
	
	VerificationLoop();
}
