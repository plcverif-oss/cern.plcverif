#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Variables
bool MX1_0 = false;
bool MX1_1 = false;
bool MX1_2 = false;
bool MX1_3 = false;
bool MX1_4 = false;
bool MX1_5 = false;
bool MX2_0 = false;
bool MX2_1 = false;
bool MX2_2 = false;
bool MX2_3 = false;
bool MX2_4 = false;
bool MX2_5 = false;
bool MX3_0 = false;
bool MX3_1 = false;
bool MX3_2 = false;
bool MX3_3 = false;
bool MX4_0 = false;
bool MX4_1 = false;
bool MX4_2 = false;
bool MX4_3 = false;
bool MX5_0 = false;
bool MX5_1 = false;
bool MX5_2 = false;
bool MX5_3 = false;
int16_t caseFunction1_I = 0;
int16_t caseFunction2_I = 0;
int16_t caseFunction3_I = 0;
int16_t caseFunction4_I = 0;
int16_t caseFunction5_I = 0;
uint16_t __assertion_error = 0;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			caseFunction1_I = 1;
			goto verificationLoop_VerificationLoop_caseFunction_OB1_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			caseFunction2_I = 1;
			goto verificationLoop_VerificationLoop_caseFunction_OB1_init1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
			caseFunction3_I = 1;
			goto verificationLoop_VerificationLoop_caseFunction_OB1_init2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l3: {
			caseFunction4_I = 1;
			goto verificationLoop_VerificationLoop_caseFunction_OB1_init3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l4: {
			goto callEnd;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_init: {
			MX1_0 = true;
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l1: {
		if ((caseFunction1_I == 1)) {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l3;
		}
		if ((! (caseFunction1_I == 1))) {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l4;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l2: {
			MX1_5 = true;
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l3: {
			MX1_1 = false;
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l4: {
		if ((caseFunction1_I == 2)) {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l6;
		}
		if ((! (caseFunction1_I == 2))) {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l7;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l5: {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l6: {
			MX1_2 = false;
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l7: {
		if ((caseFunction1_I == 3)) {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l9;
		}
		if ((! (caseFunction1_I == 3))) {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l10;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l8: {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l9: {
			MX1_3 = false;
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l10: {
			MX1_4 = false;
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l11: {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l12: {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l13: {
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_init1: {
			MX2_0 = true;
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l14;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l14: {
		if ((((caseFunction2_I * 2) >= 1) && ((caseFunction2_I * 2) <= 3))) {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l31;
		}
		if ((! (((caseFunction2_I * 2) >= 1) && ((caseFunction2_I * 2) <= 3)))) {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l41;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l21: {
			MX2_5 = true;
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l19;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l31: {
			MX2_1 = false;
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l51;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l41: {
		if ((((caseFunction2_I * 2) >= 2) && ((caseFunction2_I * 2) <= 4))) {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l61;
		}
		if ((! (((caseFunction2_I * 2) >= 2) && ((caseFunction2_I * 2) <= 4)))) {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l71;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l51: {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l61: {
			MX2_2 = false;
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l81;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l71: {
		if ((((caseFunction2_I * 2) == 6) || (((caseFunction2_I * 2) == 8) || ((caseFunction2_I * 2) == 10)))) {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l91;
		}
		if ((! (((caseFunction2_I * 2) == 6) || (((caseFunction2_I * 2) == 8) || ((caseFunction2_I * 2) == 10))))) {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l101;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l81: {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l91: {
			MX2_3 = false;
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l111;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l101: {
		if ((((caseFunction2_I * 2) >= 11) && ((caseFunction2_I * 2) <= 13))) {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l121;
		}
		if ((! (((caseFunction2_I * 2) >= 11) && ((caseFunction2_I * 2) <= 13)))) {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l131;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l111: {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l121: {
			MX2_4 = false;
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l141;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l131: {
		if ((((caseFunction2_I * 2) >= 12) && ((caseFunction2_I * 2) <= 14))) {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l15;
		}
		if ((! (((caseFunction2_I * 2) >= 12) && ((caseFunction2_I * 2) <= 14)))) {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l16;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l141: {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l15: {
			MX2_5 = false;
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l17;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l16: {
			MX2_4 = false;
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l18;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l17: {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l18: {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l19: {
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_init2: {
			MX3_0 = true;
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l110;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l110: {
		if ((caseFunction3_I == 1)) {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l32;
		}
		if ((! (caseFunction3_I == 1))) {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l42;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l22: {
			MX3_3 = true;
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l92;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l32: {
			MX3_1 = false;
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l42: {
		if (((caseFunction3_I == 2) || ((caseFunction3_I == 3) || ((caseFunction3_I == 5) || (caseFunction3_I == 7))))) {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l62;
		}
		if ((! ((caseFunction3_I == 2) || ((caseFunction3_I == 3) || ((caseFunction3_I == 5) || (caseFunction3_I == 7)))))) {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l72;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l52: {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l62: {
			MX3_2 = false;
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l82;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l72: {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l82: {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l92: {
			goto verificationLoop_VerificationLoop_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_init3: {
			MX4_0 = true;
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l112;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l112: {
		if (((((int32_t) caseFunction4_I) * 2) == 1)) {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l33;
		}
		if ((! ((((int32_t) caseFunction4_I) * 2) == 1))) {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l43;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l23: {
			MX4_3 = true;
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l93;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l33: {
			MX4_1 = false;
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l53;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l43: {
		if ((((((int32_t) caseFunction4_I) * 2) == 2) || (((((int32_t) caseFunction4_I) * 2) == 3) || (((((int32_t) caseFunction4_I) * 2) == 5) || ((((int32_t) caseFunction4_I) * 2) == 7))))) {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l63;
		}
		if ((! (((((int32_t) caseFunction4_I) * 2) == 2) || (((((int32_t) caseFunction4_I) * 2) == 3) || (((((int32_t) caseFunction4_I) * 2) == 5) || ((((int32_t) caseFunction4_I) * 2) == 7)))))) {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l73;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l53: {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l23;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l63: {
			MX4_2 = false;
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l83;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l73: {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l23;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l83: {
			goto verificationLoop_VerificationLoop_caseFunction_OB1_l23;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_caseFunction_OB1_l93: {
			goto verificationLoop_VerificationLoop_l4;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
