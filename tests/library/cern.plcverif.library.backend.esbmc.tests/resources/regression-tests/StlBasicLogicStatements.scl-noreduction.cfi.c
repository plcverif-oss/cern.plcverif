#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Variables
bool QX0_0 = false;
bool QX0_1 = false;
bool QX0_2 = false;
bool QX0_3 = false;
bool QX0_4 = false;
bool simpletest_db1_a = false;
bool simpletest_db1_b = false;
bool simpletest_db1_c = false;
bool simpletest_db1___RLO = false;
bool simpletest_db1___NFC = false;
bool simpletest_db1___BR = false;
bool simpletest_db1___STA = false;
bool simpletest_db1___OR = false;
bool simpletest_db1___CC0 = false;
bool simpletest_db1___CC1 = false;
uint16_t __assertion_error = 0;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			simpletest_db1_a = nondet_bool();
			simpletest_db1_b = nondet_bool();
			simpletest_db1_c = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			simpletest_db1___OR = false;
			simpletest_db1___STA = true;
			simpletest_db1___RLO = true;
			simpletest_db1___CC0 = false;
			simpletest_db1___CC1 = false;
			simpletest_db1___BR = false;
			simpletest_db1___NFC = false;
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			simpletest_db1___RLO = ((simpletest_db1___NFC && simpletest_db1___RLO) || simpletest_db1_a);
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
			simpletest_db1___STA = simpletest_db1_a;
			goto verificationLoop_VerificationLoop_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l3: {
			simpletest_db1___NFC = true;
			goto verificationLoop_VerificationLoop_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l4: {
			simpletest_db1___OR = false;
			goto verificationLoop_VerificationLoop_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l5: {
			simpletest_db1___RLO = ((simpletest_db1___NFC && simpletest_db1___RLO) || simpletest_db1_b);
			goto verificationLoop_VerificationLoop_l6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l6: {
			simpletest_db1___STA = simpletest_db1_b;
			goto verificationLoop_VerificationLoop_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l7: {
			simpletest_db1___NFC = true;
			goto verificationLoop_VerificationLoop_l8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l8: {
			simpletest_db1___OR = false;
			goto verificationLoop_VerificationLoop_l9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l9: {
			simpletest_db1___RLO = ((simpletest_db1___NFC && simpletest_db1___RLO) || simpletest_db1_c);
			goto verificationLoop_VerificationLoop_l10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l10: {
			simpletest_db1___STA = simpletest_db1_c;
			goto verificationLoop_VerificationLoop_l11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l11: {
			simpletest_db1___NFC = true;
			goto verificationLoop_VerificationLoop_l12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l12: {
			simpletest_db1___OR = false;
			goto verificationLoop_VerificationLoop_l13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l13: {
			QX0_0 = simpletest_db1___RLO;
			goto verificationLoop_VerificationLoop_l14;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l14: {
			simpletest_db1___OR = false;
			goto verificationLoop_VerificationLoop_l15;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l15: {
			simpletest_db1___STA = simpletest_db1___RLO;
			goto verificationLoop_VerificationLoop_l16;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l16: {
			simpletest_db1___NFC = false;
			goto verificationLoop_VerificationLoop_l17;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l17: {
		if ((! (! (QX0_0 == ((simpletest_db1_a || simpletest_db1_b) || simpletest_db1_c))))) {
			simpletest_db1___RLO = (((! simpletest_db1___NFC) || simpletest_db1___RLO) && (simpletest_db1_a || simpletest_db1___OR));
			goto verificationLoop_VerificationLoop_l18;
		}
		if ((! (QX0_0 == ((simpletest_db1_a || simpletest_db1_b) || simpletest_db1_c)))) {
			__assertion_error = 1;
			goto verificationLoop_VerificationLoop_l70;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l18: {
			simpletest_db1___STA = simpletest_db1_a;
			goto verificationLoop_VerificationLoop_l19;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l19: {
			simpletest_db1___NFC = true;
			goto verificationLoop_VerificationLoop_l20;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l20: {
			simpletest_db1___RLO = (((! simpletest_db1___NFC) || simpletest_db1___RLO) && (simpletest_db1_b || simpletest_db1___OR));
			goto verificationLoop_VerificationLoop_l21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l21: {
			simpletest_db1___STA = simpletest_db1_b;
			goto verificationLoop_VerificationLoop_l22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l22: {
			simpletest_db1___NFC = true;
			goto verificationLoop_VerificationLoop_l23;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l23: {
			simpletest_db1___RLO = (((! simpletest_db1___NFC) || simpletest_db1___RLO) && (simpletest_db1_c || simpletest_db1___OR));
			goto verificationLoop_VerificationLoop_l24;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l24: {
			simpletest_db1___STA = simpletest_db1_c;
			goto verificationLoop_VerificationLoop_l25;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l25: {
			simpletest_db1___NFC = true;
			goto verificationLoop_VerificationLoop_l26;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l26: {
			QX0_1 = simpletest_db1___RLO;
			goto verificationLoop_VerificationLoop_l27;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l27: {
			simpletest_db1___OR = false;
			goto verificationLoop_VerificationLoop_l28;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l28: {
			simpletest_db1___STA = simpletest_db1___RLO;
			goto verificationLoop_VerificationLoop_l29;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l29: {
			simpletest_db1___NFC = false;
			goto verificationLoop_VerificationLoop_l30;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l30: {
		if ((! (! (QX0_1 == ((simpletest_db1_a && simpletest_db1_b) && simpletest_db1_c))))) {
			simpletest_db1___RLO = true;
			goto verificationLoop_VerificationLoop_l31;
		}
		if ((! (QX0_1 == ((simpletest_db1_a && simpletest_db1_b) && simpletest_db1_c)))) {
			__assertion_error = 2;
			goto verificationLoop_VerificationLoop_l70;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l31: {
			QX0_2 = simpletest_db1___RLO;
			goto verificationLoop_VerificationLoop_l32;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l32: {
			simpletest_db1___OR = false;
			goto verificationLoop_VerificationLoop_l33;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l33: {
			simpletest_db1___STA = simpletest_db1___RLO;
			goto verificationLoop_VerificationLoop_l34;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l34: {
			simpletest_db1___NFC = false;
			goto verificationLoop_VerificationLoop_l35;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l35: {
		if ((! (! (QX0_2 == true)))) {
			simpletest_db1___RLO = false;
			goto verificationLoop_VerificationLoop_l36;
		}
		if ((! (QX0_2 == true))) {
			__assertion_error = 3;
			goto verificationLoop_VerificationLoop_l70;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l36: {
			QX0_3 = simpletest_db1___RLO;
			goto verificationLoop_VerificationLoop_l37;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l37: {
			simpletest_db1___OR = false;
			goto verificationLoop_VerificationLoop_l38;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l38: {
			simpletest_db1___STA = simpletest_db1___RLO;
			goto verificationLoop_VerificationLoop_l39;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l39: {
			simpletest_db1___NFC = false;
			goto verificationLoop_VerificationLoop_l40;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l40: {
		if ((! (! (QX0_3 == false)))) {
			simpletest_db1___RLO = (((! simpletest_db1___NFC) || simpletest_db1___RLO) && (simpletest_db1_a || simpletest_db1___OR));
			goto verificationLoop_VerificationLoop_l41;
		}
		if ((! (QX0_3 == false))) {
			__assertion_error = 4;
			goto verificationLoop_VerificationLoop_l70;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l41: {
			simpletest_db1___STA = simpletest_db1_a;
			goto verificationLoop_VerificationLoop_l42;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l42: {
			simpletest_db1___NFC = true;
			goto verificationLoop_VerificationLoop_l43;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l43: {
			simpletest_db1___RLO = (((! simpletest_db1___NFC) || simpletest_db1___RLO) && (simpletest_db1_b || simpletest_db1___OR));
			goto verificationLoop_VerificationLoop_l44;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l44: {
			simpletest_db1___STA = simpletest_db1_b;
			goto verificationLoop_VerificationLoop_l45;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l45: {
			simpletest_db1___NFC = true;
			goto verificationLoop_VerificationLoop_l46;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l46: {
			simpletest_db1___RLO = ((simpletest_db1___NFC && simpletest_db1___RLO) || simpletest_db1_c);
			goto verificationLoop_VerificationLoop_l47;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l47: {
			simpletest_db1___STA = simpletest_db1_c;
			goto verificationLoop_VerificationLoop_l48;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l48: {
			simpletest_db1___NFC = true;
			goto verificationLoop_VerificationLoop_l49;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l49: {
			simpletest_db1___OR = false;
			goto verificationLoop_VerificationLoop_l50;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l50: {
			simpletest_db1___RLO = (! simpletest_db1___RLO);
			goto verificationLoop_VerificationLoop_l51;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l51: {
			QX0_4 = simpletest_db1___RLO;
			goto verificationLoop_VerificationLoop_l52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l52: {
			simpletest_db1___OR = false;
			goto verificationLoop_VerificationLoop_l53;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l53: {
			simpletest_db1___STA = simpletest_db1___RLO;
			goto verificationLoop_VerificationLoop_l54;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l54: {
			simpletest_db1___NFC = false;
			goto verificationLoop_VerificationLoop_l55;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l55: {
		if ((! (! (QX0_4 == (! ((simpletest_db1_a && simpletest_db1_b) || simpletest_db1_c)))))) {
			simpletest_db1___RLO = (((! simpletest_db1___NFC) || simpletest_db1___RLO) && (simpletest_db1_a || simpletest_db1___OR));
			goto verificationLoop_VerificationLoop_l56;
		}
		if ((! (QX0_4 == (! ((simpletest_db1_a && simpletest_db1_b) || simpletest_db1_c))))) {
			__assertion_error = 5;
			goto verificationLoop_VerificationLoop_l70;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l56: {
			simpletest_db1___STA = simpletest_db1_a;
			goto verificationLoop_VerificationLoop_l57;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l57: {
			simpletest_db1___NFC = true;
			goto verificationLoop_VerificationLoop_l58;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l58: {
			simpletest_db1___RLO = (((! simpletest_db1___NFC) || simpletest_db1___RLO) && (simpletest_db1_b || simpletest_db1___OR));
			goto verificationLoop_VerificationLoop_l59;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l59: {
			simpletest_db1___STA = simpletest_db1_b;
			goto verificationLoop_VerificationLoop_l60;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l60: {
			simpletest_db1___NFC = true;
			goto verificationLoop_VerificationLoop_l61;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l61: {
			simpletest_db1___STA = true;
			goto verificationLoop_VerificationLoop_l62;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l62: {
			simpletest_db1___OR = (simpletest_db1___NFC && (simpletest_db1___OR || simpletest_db1___RLO));
			goto verificationLoop_VerificationLoop_l63;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l63: {
			simpletest_db1___NFC = false;
			goto verificationLoop_VerificationLoop_l64;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l64: {
			simpletest_db1___RLO = (((! simpletest_db1___NFC) || simpletest_db1___RLO) && (simpletest_db1_c || simpletest_db1___OR));
			goto verificationLoop_VerificationLoop_l65;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l65: {
			simpletest_db1___STA = simpletest_db1_c;
			goto verificationLoop_VerificationLoop_l66;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l66: {
			simpletest_db1___NFC = true;
			goto verificationLoop_VerificationLoop_l67;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l67: {
			simpletest_db1___STA = true;
			goto verificationLoop_VerificationLoop_l68;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l68: {
			simpletest_db1___OR = (simpletest_db1___NFC && (simpletest_db1___OR || simpletest_db1___RLO));
			goto verificationLoop_VerificationLoop_l69;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l69: {
			simpletest_db1___NFC = false;
			goto verificationLoop_VerificationLoop_l70;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l70: {
			goto callEnd;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
