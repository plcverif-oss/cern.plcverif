#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Variables
bool MX123_7 = false;
int16_t function1_out1 = 0;
bool function2_in1 = false;
int16_t function2_in2 = 0;
int16_t function2_RET_VAL = 0;
uint16_t __assertion_error = 0;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			goto verificationLoop_VerificationLoop_simplecode_OB1_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			goto callEnd;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_simplecode_OB1_init: {
			function2_in1 = MX123_7;
			function2_in2 = 123;
			goto verificationLoop_VerificationLoop_simplecode_OB1_function1_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_simplecode_OB1_l1: {
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_simplecode_OB1_function1_init: {
		if (function2_in1) {
			goto verificationLoop_VerificationLoop_simplecode_OB1_function1_l1;
		}
		if ((! function2_in1)) {
			goto verificationLoop_VerificationLoop_simplecode_OB1_function1_l3;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_simplecode_OB1_function1_l1: {
			function2_RET_VAL = function2_in2;
			goto verificationLoop_VerificationLoop_simplecode_OB1_function1_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_simplecode_OB1_function1_l2: {
			goto verificationLoop_VerificationLoop_simplecode_OB1_function1_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_simplecode_OB1_function1_l3: {
			function2_RET_VAL = (- function2_in2);
			goto verificationLoop_VerificationLoop_simplecode_OB1_function1_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_simplecode_OB1_function1_l4: {
			goto verificationLoop_VerificationLoop_simplecode_OB1_function1_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_simplecode_OB1_function1_l5: {
			function1_out1 = function2_RET_VAL;
			goto verificationLoop_VerificationLoop_simplecode_OB1_l1;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
