#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Variables
bool MX0_0 = false;
int32_t __GLOBAL_TIME = 0;
bool timeh_db1_enabled = false;
bool timeh_db1_q = false;
int32_t timeh_db1_ton_PT = 0;
bool timeh_db1_ton_IN = false;
bool timeh_db1_ton_Q = false;
int32_t timeh_db1_ton_ET = 0;
bool timeh_db1_ton_running = false;
int32_t timeh_db1_ton_start = 0;
int32_t T_CYCLE = 0;
uint16_t __assertion_error = 0;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			T_CYCLE = nondet_int32_t();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			__GLOBAL_TIME = (__GLOBAL_TIME + T_CYCLE);
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			timeh_db1_enabled = true;
			goto verificationLoop_VerificationLoop_timeh_OB1_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			MX0_0 = timeh_db1_q;
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
			goto callEnd;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_init: {
		if (timeh_db1_enabled) {
			goto verificationLoop_VerificationLoop_timeh_OB1_l1;
		}
		if ((! timeh_db1_enabled)) {
			goto verificationLoop_VerificationLoop_timeh_OB1_l3;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_l1: {
			timeh_db1_ton_IN = true;
			timeh_db1_ton_PT = 1500;
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_l2: {
			goto verificationLoop_VerificationLoop_timeh_OB1_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_l3: {
			goto verificationLoop_VerificationLoop_timeh_OB1_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_l4: {
			timeh_db1_q = timeh_db1_ton_Q;
			goto verificationLoop_VerificationLoop_timeh_OB1_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_l5: {
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_init: {
		if ((timeh_db1_ton_IN == false)) {
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l1;
		}
		if ((! (timeh_db1_ton_IN == false))) {
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l5;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l1: {
			timeh_db1_ton_Q = false;
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l2: {
			timeh_db1_ton_ET = 0;
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l3: {
			timeh_db1_ton_running = false;
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l4: {
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l25;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l5: {
		if ((timeh_db1_ton_running == false)) {
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l6;
		}
		if ((! (timeh_db1_ton_running == false))) {
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l14;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l6: {
			timeh_db1_ton_start = __GLOBAL_TIME;
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l7: {
			timeh_db1_ton_running = true;
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l8: {
			timeh_db1_ton_ET = 0;
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l9: {
		if ((timeh_db1_ton_PT == 0)) {
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l10;
		}
		if ((! (timeh_db1_ton_PT == 0))) {
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l12;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l10: {
			timeh_db1_ton_Q = true;
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l11: {
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l12: {
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l13: {
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l24;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l14: {
		if ((! ((__GLOBAL_TIME - (timeh_db1_ton_start + timeh_db1_ton_PT)) >= 0))) {
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l15;
		}
		if ((! (! ((__GLOBAL_TIME - (timeh_db1_ton_start + timeh_db1_ton_PT)) >= 0)))) {
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l20;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l15: {
		if ((! timeh_db1_ton_Q)) {
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l16;
		}
		if ((! (! timeh_db1_ton_Q))) {
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l18;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l16: {
			timeh_db1_ton_ET = (__GLOBAL_TIME - timeh_db1_ton_start);
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l17;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l17: {
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l19;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l18: {
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l19;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l19: {
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l23;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l20: {
			timeh_db1_ton_Q = true;
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l21: {
			timeh_db1_ton_ET = timeh_db1_ton_PT;
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l22: {
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l23;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l23: {
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l24;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l24: {
			goto verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l25;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_timeh_OB1_timeh_db1_timeh_fb1_l25: {
			goto verificationLoop_VerificationLoop_timeh_OB1_l2;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
