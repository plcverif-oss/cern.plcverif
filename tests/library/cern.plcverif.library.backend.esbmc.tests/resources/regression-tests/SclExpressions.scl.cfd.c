#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Root data structure
typedef struct {
	bool boolVar1;
	bool boolVar2;
	int16_t intVar1;
	int16_t intVar2;
	uint8_t byteVar1;
	uint8_t byteVar2;
	uint16_t wordVar1;
	uint16_t wordVar2;
	uint32_t dwordVar1;
	uint32_t dwordVar2;
} __expr_fc1;

// Global variables
bool IX0_0;
__expr_fc1 expr_fc11;
uint16_t __assertion_error;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Automata declarations
void expr_OB1();
void expr_fc1(__expr_fc1 *__context);
void VerificationLoop();

// Automata
void expr_OB1() {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			// Assign inputs
			expr_fc1(&expr_fc11);
			// Assign outputs
			goto l1;
		//assert(false);
		return;  			}
	l1: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void expr_fc1(__expr_fc1 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			__context->boolVar1 = (__context->boolVar1 && __context->boolVar2);
			goto l11;
		//assert(false);
		return;  			}
	l11: {
			__context->boolVar1 = (__context->boolVar1 && false);
			goto l2;
		//assert(false);
		return;  			}
	l2: {
			__context->boolVar1 = (__context->boolVar1 && true);
			goto l3;
		//assert(false);
		return;  			}
	l3: {
			__context->boolVar1 = (IX0_0 && true);
			goto l4;
		//assert(false);
		return;  			}
	l4: {
			__context->boolVar1 = (true && true);
			goto l5;
		//assert(false);
		return;  			}
	l5: {
			__context->boolVar1 = (false && true);
			goto l6;
		//assert(false);
		return;  			}
	l6: {
			__context->boolVar1 = (__context->boolVar1 || __context->boolVar2);
			goto l7;
		//assert(false);
		return;  			}
	l7: {
			__context->boolVar1 = (__context->boolVar1 || false);
			goto l8;
		//assert(false);
		return;  			}
	l8: {
			__context->boolVar1 = (__context->boolVar1 || true);
			goto l9;
		//assert(false);
		return;  			}
	l9: {
			__context->boolVar1 = (IX0_0 || true);
			goto l10;
		//assert(false);
		return;  			}
	l10: {
			__context->boolVar1 = (true || true);
			goto l111;
		//assert(false);
		return;  			}
	l111: {
			__context->boolVar1 = (false || true);
			goto l12;
		//assert(false);
		return;  			}
	l12: {
			__context->boolVar1 = (__context->boolVar1 != __context->boolVar2);
			goto l13;
		//assert(false);
		return;  			}
	l13: {
			__context->boolVar1 = (__context->boolVar1 != false);
			goto l14;
		//assert(false);
		return;  			}
	l14: {
			__context->boolVar1 = (__context->boolVar1 != true);
			goto l15;
		//assert(false);
		return;  			}
	l15: {
			__context->boolVar1 = (IX0_0 != true);
			goto l16;
		//assert(false);
		return;  			}
	l16: {
			__context->boolVar1 = (true != true);
			goto l17;
		//assert(false);
		return;  			}
	l17: {
			__context->boolVar1 = (false != true);
			__context->byteVar1 = (__context->byteVar1 & __context->byteVar2);
			goto l19;
		//assert(false);
		return;  			}
	l19: {
			__context->byteVar1 = (__context->byteVar1 & 123);
			goto l20;
		//assert(false);
		return;  			}
	l20: {
			__context->byteVar1 = (__context->byteVar1 & 123);
			goto l21;
		//assert(false);
		return;  			}
	l21: {
			__context->byteVar1 = (1 & 3);
			__context->wordVar1 = (__context->wordVar1 & __context->wordVar2);
			goto l23;
		//assert(false);
		return;  			}
	l23: {
			__context->wordVar1 = (__context->wordVar1 & 123);
			goto l24;
		//assert(false);
		return;  			}
	l24: {
			__context->wordVar1 = (__context->wordVar1 & 123);
			goto l25;
		//assert(false);
		return;  			}
	l25: {
			__context->wordVar1 = (__context->wordVar1 & 123);
			__context->dwordVar1 = (__context->dwordVar1 & __context->dwordVar2);
			goto l27;
		//assert(false);
		return;  			}
	l27: {
			__context->dwordVar1 = (__context->dwordVar1 & 123);
			goto l28;
		//assert(false);
		return;  			}
	l28: {
			__context->dwordVar1 = (__context->dwordVar1 & 123);
			goto l29;
		//assert(false);
		return;  			}
	l29: {
			__context->dwordVar1 = (__context->dwordVar1 & 123);
			goto l30;
		//assert(false);
		return;  			}
	l30: {
			__context->dwordVar1 = (__context->dwordVar1 & 123);
			__context->byteVar1 = (__context->byteVar1 | __context->byteVar2);
			goto l32;
		//assert(false);
		return;  			}
	l32: {
			__context->byteVar1 = (__context->byteVar1 | 123);
			goto l33;
		//assert(false);
		return;  			}
	l33: {
			__context->byteVar1 = (__context->byteVar1 | 123);
			goto l34;
		//assert(false);
		return;  			}
	l34: {
			__context->byteVar1 = (1 | 3);
			__context->wordVar1 = (__context->wordVar1 | __context->wordVar2);
			goto l36;
		//assert(false);
		return;  			}
	l36: {
			__context->wordVar1 = (__context->wordVar1 | 123);
			goto l37;
		//assert(false);
		return;  			}
	l37: {
			__context->wordVar1 = (__context->wordVar1 | 123);
			goto l38;
		//assert(false);
		return;  			}
	l38: {
			__context->wordVar1 = (__context->wordVar1 | 123);
			__context->dwordVar1 = (__context->dwordVar1 | __context->dwordVar2);
			goto l40;
		//assert(false);
		return;  			}
	l40: {
			__context->dwordVar1 = (__context->dwordVar1 | 123);
			goto l41;
		//assert(false);
		return;  			}
	l41: {
			__context->dwordVar1 = (__context->dwordVar1 | 123);
			goto l42;
		//assert(false);
		return;  			}
	l42: {
			__context->dwordVar1 = (__context->dwordVar1 | 123);
			goto l43;
		//assert(false);
		return;  			}
	l43: {
			__context->dwordVar1 = (__context->dwordVar1 | 123);
			__context->byteVar1 = (__context->byteVar1 ^ __context->byteVar2);
			goto l45;
		//assert(false);
		return;  			}
	l45: {
			__context->byteVar1 = (__context->byteVar1 ^ 123);
			goto l46;
		//assert(false);
		return;  			}
	l46: {
			__context->byteVar1 = (__context->byteVar1 ^ 123);
			goto l47;
		//assert(false);
		return;  			}
	l47: {
			__context->byteVar1 = (1 ^ 3);
			__context->wordVar1 = (__context->wordVar1 ^ __context->wordVar2);
			goto l49;
		//assert(false);
		return;  			}
	l49: {
			__context->wordVar1 = (__context->wordVar1 ^ 123);
			goto l50;
		//assert(false);
		return;  			}
	l50: {
			__context->wordVar1 = (__context->wordVar1 ^ 123);
			goto l51;
		//assert(false);
		return;  			}
	l51: {
			__context->wordVar1 = (__context->wordVar1 ^ 123);
			__context->dwordVar1 = (__context->dwordVar1 ^ __context->dwordVar2);
			goto l53;
		//assert(false);
		return;  			}
	l53: {
			__context->dwordVar1 = (__context->dwordVar1 ^ 123);
			goto l54;
		//assert(false);
		return;  			}
	l54: {
			__context->dwordVar1 = (__context->dwordVar1 ^ 123);
			goto l55;
		//assert(false);
		return;  			}
	l55: {
			__context->dwordVar1 = (__context->dwordVar1 ^ 123);
			goto l56;
		//assert(false);
		return;  			}
	l56: {
			__context->dwordVar1 = (__context->dwordVar1 ^ 123);
			goto l57;
		//assert(false);
		return;  			}
	l57: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init2;
	init2: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			IX0_0 = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			expr_OB1();
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
int main() {
	// Initial values
	IX0_0 = false;
	expr_fc11.boolVar1 = false;
	expr_fc11.boolVar2 = false;
	expr_fc11.intVar1 = 0;
	expr_fc11.intVar2 = 0;
	expr_fc11.byteVar1 = 0;
	expr_fc11.byteVar2 = 0;
	expr_fc11.wordVar1 = 0;
	expr_fc11.wordVar2 = 0;
	expr_fc11.dwordVar1 = 0;
	expr_fc11.dwordVar2 = 0;
	__assertion_error = 0;
	
	VerificationLoop();
}
