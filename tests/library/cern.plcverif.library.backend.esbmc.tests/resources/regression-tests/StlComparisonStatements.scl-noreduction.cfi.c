#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Variables
bool MX0_0 = false;
int16_t instance_int1 = 0;
int16_t instance_int2 = 0;
int32_t instance_dint1 = 0;
int32_t instance_dint2 = 0;
bool instance___RLO = false;
bool instance___NFC = false;
bool instance___BR = false;
bool instance___STA = false;
bool instance___OR = false;
bool instance___CC0 = false;
bool instance___CC1 = false;
int32_t instance___ACCU1 = 0;
int32_t instance___ACCU2 = 0;
uint16_t __assertion_error = 0;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			instance_dint1 = nondet_int32_t();
			instance_dint2 = nondet_int32_t();
			instance_int1 = nondet_int16_t();
			instance_int2 = nondet_int16_t();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			instance___OR = false;
			instance___STA = true;
			instance___RLO = true;
			instance___CC0 = false;
			instance___CC1 = false;
			instance___BR = false;
			instance___NFC = false;
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			instance___ACCU2 = instance___ACCU1;
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
			instance___ACCU1 = ((int32_t) instance_int1);
			goto verificationLoop_VerificationLoop_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l3: {
			instance___ACCU2 = instance___ACCU1;
			goto verificationLoop_VerificationLoop_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l4: {
			instance___ACCU1 = ((int32_t) instance_int2);
			goto verificationLoop_VerificationLoop_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l5: {
			instance___RLO = (instance___ACCU2 < instance___ACCU1);
			goto verificationLoop_VerificationLoop_l6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l6: {
			instance___CC0 = (instance___ACCU1 > instance___ACCU2);
			instance___CC1 = (instance___ACCU1 < instance___ACCU2);
			instance___OR = false;
			instance___NFC = true;
			instance___STA = instance___RLO;
			goto verificationLoop_VerificationLoop_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l7: {
			instance___RLO = (instance___ACCU2 <= instance___ACCU1);
			goto verificationLoop_VerificationLoop_l8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l8: {
			instance___CC0 = (instance___ACCU1 > instance___ACCU2);
			instance___CC1 = (instance___ACCU1 < instance___ACCU2);
			instance___OR = false;
			instance___NFC = true;
			instance___STA = instance___RLO;
			goto verificationLoop_VerificationLoop_l9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l9: {
			instance___RLO = (instance___ACCU2 >= instance___ACCU1);
			goto verificationLoop_VerificationLoop_l10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l10: {
			instance___CC0 = (instance___ACCU1 > instance___ACCU2);
			instance___CC1 = (instance___ACCU1 < instance___ACCU2);
			instance___OR = false;
			instance___NFC = true;
			instance___STA = instance___RLO;
			goto verificationLoop_VerificationLoop_l11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l11: {
			instance___RLO = (instance___ACCU2 > instance___ACCU1);
			goto verificationLoop_VerificationLoop_l12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l12: {
			instance___CC0 = (instance___ACCU1 > instance___ACCU2);
			instance___CC1 = (instance___ACCU1 < instance___ACCU2);
			instance___OR = false;
			instance___NFC = true;
			instance___STA = instance___RLO;
			goto verificationLoop_VerificationLoop_l13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l13: {
			instance___RLO = (instance___ACCU2 == instance___ACCU1);
			goto verificationLoop_VerificationLoop_l14;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l14: {
			instance___CC0 = (instance___ACCU1 > instance___ACCU2);
			instance___CC1 = (instance___ACCU1 < instance___ACCU2);
			instance___OR = false;
			instance___NFC = true;
			instance___STA = instance___RLO;
			goto verificationLoop_VerificationLoop_l15;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l15: {
			instance___RLO = (instance___ACCU2 != instance___ACCU1);
			goto verificationLoop_VerificationLoop_l16;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l16: {
			instance___CC0 = (instance___ACCU1 > instance___ACCU2);
			instance___CC1 = (instance___ACCU1 < instance___ACCU2);
			instance___OR = false;
			instance___NFC = true;
			instance___STA = instance___RLO;
			goto verificationLoop_VerificationLoop_l17;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l17: {
			instance___ACCU2 = instance___ACCU1;
			goto verificationLoop_VerificationLoop_l18;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l18: {
			instance___ACCU1 = instance_dint1;
			goto verificationLoop_VerificationLoop_l19;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l19: {
			instance___ACCU2 = instance___ACCU1;
			goto verificationLoop_VerificationLoop_l20;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l20: {
			instance___ACCU1 = instance_dint2;
			goto verificationLoop_VerificationLoop_l21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l21: {
			instance___RLO = (instance___ACCU2 < instance___ACCU1);
			goto verificationLoop_VerificationLoop_l22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l22: {
			instance___CC0 = (instance___ACCU1 > instance___ACCU2);
			instance___CC1 = (instance___ACCU1 < instance___ACCU2);
			instance___OR = false;
			instance___NFC = true;
			instance___STA = instance___RLO;
			goto verificationLoop_VerificationLoop_l23;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l23: {
			instance___RLO = (instance___ACCU2 <= instance___ACCU1);
			goto verificationLoop_VerificationLoop_l24;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l24: {
			instance___CC0 = (instance___ACCU1 > instance___ACCU2);
			instance___CC1 = (instance___ACCU1 < instance___ACCU2);
			instance___OR = false;
			instance___NFC = true;
			instance___STA = instance___RLO;
			goto verificationLoop_VerificationLoop_l25;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l25: {
			instance___RLO = (instance___ACCU2 >= instance___ACCU1);
			goto verificationLoop_VerificationLoop_l26;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l26: {
			instance___CC0 = (instance___ACCU1 > instance___ACCU2);
			instance___CC1 = (instance___ACCU1 < instance___ACCU2);
			instance___OR = false;
			instance___NFC = true;
			instance___STA = instance___RLO;
			goto verificationLoop_VerificationLoop_l27;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l27: {
			instance___RLO = (instance___ACCU2 > instance___ACCU1);
			goto verificationLoop_VerificationLoop_l28;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l28: {
			instance___CC0 = (instance___ACCU1 > instance___ACCU2);
			instance___CC1 = (instance___ACCU1 < instance___ACCU2);
			instance___OR = false;
			instance___NFC = true;
			instance___STA = instance___RLO;
			goto verificationLoop_VerificationLoop_l29;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l29: {
			instance___RLO = (instance___ACCU2 == instance___ACCU1);
			goto verificationLoop_VerificationLoop_l30;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l30: {
			instance___CC0 = (instance___ACCU1 > instance___ACCU2);
			instance___CC1 = (instance___ACCU1 < instance___ACCU2);
			instance___OR = false;
			instance___NFC = true;
			instance___STA = instance___RLO;
			goto verificationLoop_VerificationLoop_l31;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l31: {
			instance___RLO = (instance___ACCU2 != instance___ACCU1);
			goto verificationLoop_VerificationLoop_l32;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l32: {
			instance___CC0 = (instance___ACCU1 > instance___ACCU2);
			instance___CC1 = (instance___ACCU1 < instance___ACCU2);
			instance___OR = false;
			instance___NFC = true;
			instance___STA = instance___RLO;
			goto verificationLoop_VerificationLoop_l33;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l33: {
			MX0_0 = instance___RLO;
			goto verificationLoop_VerificationLoop_l34;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l34: {
			instance___OR = false;
			goto verificationLoop_VerificationLoop_l35;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l35: {
			instance___STA = instance___RLO;
			goto verificationLoop_VerificationLoop_l36;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l36: {
			instance___NFC = false;
			goto verificationLoop_VerificationLoop_l37;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l37: {
			goto callEnd;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
