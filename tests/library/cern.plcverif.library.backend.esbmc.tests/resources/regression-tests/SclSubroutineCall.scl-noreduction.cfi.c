#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Variables
bool IX0_0 = false;
bool MX0_0 = false;
bool MX1_0 = false;
bool MX2_0 = false;
bool MX3_0 = false;
bool MX3_1 = false;
bool MX3_2 = false;
bool MX4_0 = false;
int16_t call_OB1_r = 0;
bool call_FC2_in1 = false;
bool call_FC3_in1 = false;
int16_t call_FC3_in2 = 0;
int16_t call_FC3_RET_VAL = 0;
bool call_DB4_a = false;
bool call_DB4_q = false;
bool call_DB4_c_b = false;
bool call_DB4_c_w = false;
uint16_t __assertion_error = 0;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			IX0_0 = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			call_OB1_r = 0;
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			goto verificationLoop_VerificationLoop_call_OB1_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
			call_FC2_in1 = true;
			goto verificationLoop_VerificationLoop_call_OB1_init1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l3: {
			call_FC3_in1 = IX0_0;
			call_FC3_in2 = 100;
			goto verificationLoop_VerificationLoop_call_OB1_init2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l4: {
			call_DB4_a = true;
			goto verificationLoop_VerificationLoop_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l5: {
			goto verificationLoop_VerificationLoop_call_OB1_init3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l6: {
			MX0_0 = call_DB4_q;
			goto verificationLoop_VerificationLoop_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l7: {
			goto callEnd;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_init: {
			MX1_0 = true;
			goto verificationLoop_VerificationLoop_call_OB1_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l1: {
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_init1: {
			MX2_0 = (! call_FC2_in1);
			goto verificationLoop_VerificationLoop_call_OB1_l11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l11: {
			goto verificationLoop_VerificationLoop_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_init2: {
		if ((call_FC3_in2 > 100)) {
			goto verificationLoop_VerificationLoop_call_OB1_l12;
		}
		if ((! (call_FC3_in2 > 100))) {
			goto verificationLoop_VerificationLoop_call_OB1_l3;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l12: {
			MX3_0 = true;
			goto verificationLoop_VerificationLoop_call_OB1_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l2: {
			goto verificationLoop_VerificationLoop_call_OB1_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l3: {
			MX3_1 = false;
			goto verificationLoop_VerificationLoop_call_OB1_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l4: {
			goto verificationLoop_VerificationLoop_call_OB1_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l5: {
			MX3_2 = true;
			goto verificationLoop_VerificationLoop_call_OB1_l6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l6: {
			call_FC3_RET_VAL = call_FC3_in2;
			goto verificationLoop_VerificationLoop_call_OB1_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l7: {
			call_OB1_r = call_FC3_RET_VAL;
			goto verificationLoop_VerificationLoop_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_init3: {
			call_DB4_q = (! call_DB4_a);
			goto verificationLoop_VerificationLoop_call_OB1_l13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l13: {
			call_DB4_c_b = call_DB4_a;
			goto verificationLoop_VerificationLoop_call_OB1_l21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l21: {
			goto verificationLoop_VerificationLoop_call_OB1_call_DB4_call_FB4_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l31: {
			MX4_0 = call_DB4_c_w;
			goto verificationLoop_VerificationLoop_call_OB1_l41;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l41: {
			goto verificationLoop_VerificationLoop_l6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_call_DB4_call_FB4_init: {
			call_DB4_c_w = call_DB4_c_b;
			goto verificationLoop_VerificationLoop_call_OB1_call_DB4_call_FB4_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_call_DB4_call_FB4_l1: {
			goto verificationLoop_VerificationLoop_call_OB1_l31;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
