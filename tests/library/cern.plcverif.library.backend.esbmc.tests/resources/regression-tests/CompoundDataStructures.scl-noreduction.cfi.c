#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Variables
bool compound_db1_in1 = false;
int16_t compound_db1_in2 = 0;
bool compound_db1_s1_a = false;
bool compound_db1_s2_b = false;
bool compound_db1_s2_c = false;
bool compound_db1_s3_s3a_d = false;
bool compound_db1_s3_s3a_e = false;
int16_t compound_db1_s5_c1 = 0;
bool compound_db1_s5_c2 = false;
int16_t compound_db2_c1 = 333;
bool compound_db2_c2 = true;
bool compound_db3_in1 = false;
int16_t compound_db3_in2 = 987;
bool compound_db3_s1_a = false;
bool compound_db3_s2_b = false;
bool compound_db3_s2_c = false;
bool compound_db3_s3_s3a_d = false;
bool compound_db3_s3_s3a_e = false;
int16_t compound_db3_s5_c1 = 0;
bool compound_db3_s5_c2 = false;
bool compound_db1_inlined_1_in1 = false;
int16_t compound_db1_inlined_1_in2 = 0;
bool compound_db1_inlined_1_s1_a = false;
bool compound_db1_inlined_1_s2_b = false;
bool compound_db1_inlined_1_s2_c = false;
bool compound_db1_inlined_1_s3_s3a_d = false;
bool compound_db1_inlined_1_s3_s3a_e = false;
int16_t compound_db1_inlined_1_s5_c1 = 0;
bool compound_db1_inlined_1_s5_c2 = false;
bool compound_db3_inlined_2_in1 = false;
int16_t compound_db3_inlined_2_in2 = 987;
bool compound_db3_inlined_2_s1_a = false;
bool compound_db3_inlined_2_s2_b = false;
bool compound_db3_inlined_2_s2_c = false;
bool compound_db3_inlined_2_s3_s3a_d = false;
bool compound_db3_inlined_2_s3_s3a_e = false;
int16_t compound_db3_inlined_2_s5_c1 = 0;
bool compound_db3_inlined_2_s5_c2 = false;
uint16_t __assertion_error = 0;
bool compound_db1_arr1_1 = false;
bool compound_db1_arr1_2 = false;
bool compound_db1_arr1_3 = false;
int16_t compound_db1_arr2_1_5 = 0;
int16_t compound_db1_arr2_1_6 = 0;
int16_t compound_db1_arr2_1_7 = 0;
int16_t compound_db1_arr2_2_5 = 0;
int16_t compound_db1_arr2_2_6 = 0;
int16_t compound_db1_arr2_2_7 = 0;
int16_t compound_db1_arr2_3_5 = 0;
int16_t compound_db1_arr2_3_6 = 0;
int16_t compound_db1_arr2_3_7 = 0;
bool compound_db1_arr3_1_x = false;
bool compound_db1_arr3_2_x = false;
bool compound_db1_arr3_3_x = false;
bool compound_db1_arr3_4_x = false;
bool compound_db1_arr3_5_x = false;
bool compound_db1_arr3_6_x = false;
bool compound_db1_arr3_7_x = false;
bool compound_db1_arr3_8_x = false;
bool compound_db1_arr3_9_x = false;
bool compound_db1_arr3_10_x = false;
bool compound_db1_arr3_1_y = false;
bool compound_db1_arr3_2_y = false;
bool compound_db1_arr3_3_y = false;
bool compound_db1_arr3_4_y = false;
bool compound_db1_arr3_5_y = false;
bool compound_db1_arr3_6_y = false;
bool compound_db1_arr3_7_y = false;
bool compound_db1_arr3_8_y = false;
bool compound_db1_arr3_9_y = false;
bool compound_db1_arr3_10_y = false;
int16_t compound_db1_arr4_8_c1 = 0;
int16_t compound_db1_arr4_9_c1 = 0;
int16_t compound_db1_arr4_10_c1 = 0;
bool compound_db1_arr4_8_c2 = false;
bool compound_db1_arr4_9_c2 = false;
bool compound_db1_arr4_10_c2 = false;
bool compound_db1_s4_s4arr_1 = false;
bool compound_db1_s4_s4arr_2 = false;
bool compound_db1_s4_s4arr_3 = false;
bool compound_db1_s4_s4arr_4 = false;
bool compound_db1_s4_s4arr_5 = false;
bool compound_db1_s4_s4arr_6 = false;
bool compound_db1_s4_s4arr_7 = false;
bool compound_db1_s4_s4arr_8 = false;
bool compound_db1_s4_s4arr_9 = false;
bool compound_db1_s4_s4arr_10 = false;
bool compound_db3_arr1_1 = false;
bool compound_db3_arr1_2 = false;
bool compound_db3_arr1_3 = false;
int16_t compound_db3_arr2_1_5 = 0;
int16_t compound_db3_arr2_1_6 = 0;
int16_t compound_db3_arr2_1_7 = 0;
int16_t compound_db3_arr2_2_5 = 0;
int16_t compound_db3_arr2_2_6 = 0;
int16_t compound_db3_arr2_2_7 = 0;
int16_t compound_db3_arr2_3_5 = 0;
int16_t compound_db3_arr2_3_6 = 0;
int16_t compound_db3_arr2_3_7 = 0;
bool compound_db3_arr3_1_x = false;
bool compound_db3_arr3_2_x = false;
bool compound_db3_arr3_3_x = false;
bool compound_db3_arr3_4_x = false;
bool compound_db3_arr3_5_x = false;
bool compound_db3_arr3_6_x = false;
bool compound_db3_arr3_7_x = false;
bool compound_db3_arr3_8_x = false;
bool compound_db3_arr3_9_x = false;
bool compound_db3_arr3_10_x = false;
bool compound_db3_arr3_1_y = false;
bool compound_db3_arr3_2_y = false;
bool compound_db3_arr3_3_y = false;
bool compound_db3_arr3_4_y = false;
bool compound_db3_arr3_5_y = false;
bool compound_db3_arr3_6_y = false;
bool compound_db3_arr3_7_y = false;
bool compound_db3_arr3_8_y = false;
bool compound_db3_arr3_9_y = false;
bool compound_db3_arr3_10_y = false;
int16_t compound_db3_arr4_8_c1 = 0;
int16_t compound_db3_arr4_9_c1 = 0;
int16_t compound_db3_arr4_10_c1 = 0;
bool compound_db3_arr4_8_c2 = false;
bool compound_db3_arr4_9_c2 = false;
bool compound_db3_arr4_10_c2 = false;
bool compound_db3_s4_s4arr_1 = false;
bool compound_db3_s4_s4arr_2 = false;
bool compound_db3_s4_s4arr_3 = false;
bool compound_db3_s4_s4arr_4 = false;
bool compound_db3_s4_s4arr_5 = false;
bool compound_db3_s4_s4arr_6 = false;
bool compound_db3_s4_s4arr_7 = false;
bool compound_db3_s4_s4arr_8 = false;
bool compound_db3_s4_s4arr_9 = false;
bool compound_db3_s4_s4arr_10 = false;
bool compound_db1_inlined_1_arr1_1 = false;
bool compound_db1_inlined_1_arr1_2 = false;
bool compound_db1_inlined_1_arr1_3 = false;
int16_t compound_db1_inlined_1_arr2_1_5 = 0;
int16_t compound_db1_inlined_1_arr2_1_6 = 0;
int16_t compound_db1_inlined_1_arr2_1_7 = 0;
int16_t compound_db1_inlined_1_arr2_2_5 = 0;
int16_t compound_db1_inlined_1_arr2_2_6 = 0;
int16_t compound_db1_inlined_1_arr2_2_7 = 0;
int16_t compound_db1_inlined_1_arr2_3_5 = 0;
int16_t compound_db1_inlined_1_arr2_3_6 = 0;
int16_t compound_db1_inlined_1_arr2_3_7 = 0;
bool compound_db1_inlined_1_arr3_1_x = false;
bool compound_db1_inlined_1_arr3_2_x = false;
bool compound_db1_inlined_1_arr3_3_x = false;
bool compound_db1_inlined_1_arr3_4_x = false;
bool compound_db1_inlined_1_arr3_5_x = false;
bool compound_db1_inlined_1_arr3_6_x = false;
bool compound_db1_inlined_1_arr3_7_x = false;
bool compound_db1_inlined_1_arr3_8_x = false;
bool compound_db1_inlined_1_arr3_9_x = false;
bool compound_db1_inlined_1_arr3_10_x = false;
bool compound_db1_inlined_1_arr3_1_y = false;
bool compound_db1_inlined_1_arr3_2_y = false;
bool compound_db1_inlined_1_arr3_3_y = false;
bool compound_db1_inlined_1_arr3_4_y = false;
bool compound_db1_inlined_1_arr3_5_y = false;
bool compound_db1_inlined_1_arr3_6_y = false;
bool compound_db1_inlined_1_arr3_7_y = false;
bool compound_db1_inlined_1_arr3_8_y = false;
bool compound_db1_inlined_1_arr3_9_y = false;
bool compound_db1_inlined_1_arr3_10_y = false;
int16_t compound_db1_inlined_1_arr4_8_c1 = 0;
int16_t compound_db1_inlined_1_arr4_9_c1 = 0;
int16_t compound_db1_inlined_1_arr4_10_c1 = 0;
bool compound_db1_inlined_1_arr4_8_c2 = false;
bool compound_db1_inlined_1_arr4_9_c2 = false;
bool compound_db1_inlined_1_arr4_10_c2 = false;
bool compound_db1_inlined_1_s4_s4arr_1 = false;
bool compound_db1_inlined_1_s4_s4arr_2 = false;
bool compound_db1_inlined_1_s4_s4arr_3 = false;
bool compound_db1_inlined_1_s4_s4arr_4 = false;
bool compound_db1_inlined_1_s4_s4arr_5 = false;
bool compound_db1_inlined_1_s4_s4arr_6 = false;
bool compound_db1_inlined_1_s4_s4arr_7 = false;
bool compound_db1_inlined_1_s4_s4arr_8 = false;
bool compound_db1_inlined_1_s4_s4arr_9 = false;
bool compound_db1_inlined_1_s4_s4arr_10 = false;
bool compound_db3_inlined_2_arr1_1 = false;
bool compound_db3_inlined_2_arr1_2 = false;
bool compound_db3_inlined_2_arr1_3 = false;
int16_t compound_db3_inlined_2_arr2_1_5 = 0;
int16_t compound_db3_inlined_2_arr2_1_6 = 0;
int16_t compound_db3_inlined_2_arr2_1_7 = 0;
int16_t compound_db3_inlined_2_arr2_2_5 = 0;
int16_t compound_db3_inlined_2_arr2_2_6 = 0;
int16_t compound_db3_inlined_2_arr2_2_7 = 0;
int16_t compound_db3_inlined_2_arr2_3_5 = 0;
int16_t compound_db3_inlined_2_arr2_3_6 = 0;
int16_t compound_db3_inlined_2_arr2_3_7 = 0;
bool compound_db3_inlined_2_arr3_1_x = false;
bool compound_db3_inlined_2_arr3_2_x = false;
bool compound_db3_inlined_2_arr3_3_x = false;
bool compound_db3_inlined_2_arr3_4_x = false;
bool compound_db3_inlined_2_arr3_5_x = false;
bool compound_db3_inlined_2_arr3_6_x = false;
bool compound_db3_inlined_2_arr3_7_x = false;
bool compound_db3_inlined_2_arr3_8_x = false;
bool compound_db3_inlined_2_arr3_9_x = false;
bool compound_db3_inlined_2_arr3_10_x = false;
bool compound_db3_inlined_2_arr3_1_y = false;
bool compound_db3_inlined_2_arr3_2_y = false;
bool compound_db3_inlined_2_arr3_3_y = false;
bool compound_db3_inlined_2_arr3_4_y = false;
bool compound_db3_inlined_2_arr3_5_y = false;
bool compound_db3_inlined_2_arr3_6_y = false;
bool compound_db3_inlined_2_arr3_7_y = false;
bool compound_db3_inlined_2_arr3_8_y = false;
bool compound_db3_inlined_2_arr3_9_y = false;
bool compound_db3_inlined_2_arr3_10_y = false;
int16_t compound_db3_inlined_2_arr4_8_c1 = 0;
int16_t compound_db3_inlined_2_arr4_9_c1 = 0;
int16_t compound_db3_inlined_2_arr4_10_c1 = 0;
bool compound_db3_inlined_2_arr4_8_c2 = false;
bool compound_db3_inlined_2_arr4_9_c2 = false;
bool compound_db3_inlined_2_arr4_10_c2 = false;
bool compound_db3_inlined_2_s4_s4arr_1 = false;
bool compound_db3_inlined_2_s4_s4arr_2 = false;
bool compound_db3_inlined_2_s4_s4arr_3 = false;
bool compound_db3_inlined_2_s4_s4arr_4 = false;
bool compound_db3_inlined_2_s4_s4arr_5 = false;
bool compound_db3_inlined_2_s4_s4arr_6 = false;
bool compound_db3_inlined_2_s4_s4arr_7 = false;
bool compound_db3_inlined_2_s4_s4arr_8 = false;
bool compound_db3_inlined_2_s4_s4arr_9 = false;
bool compound_db3_inlined_2_s4_s4arr_10 = false;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			goto verificationLoop_VerificationLoop_compound_OB1_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			goto verificationLoop_VerificationLoop_compound_OB1_init1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
			goto callEnd;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_compound_OB1_init: {
			compound_db1_inlined_1_arr4_8_c1 = compound_db1_inlined_1_in2;
			goto verificationLoop_VerificationLoop_compound_OB1_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_compound_OB1_l1: {
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_compound_OB1_init1: {
			compound_db3_inlined_2_arr4_8_c1 = compound_db3_inlined_2_in2;
			goto verificationLoop_VerificationLoop_compound_OB1_l11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_compound_OB1_l11: {
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
