#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Root data structure
typedef struct {
	float INV;
	int32_t Td;
	int32_t Tds;
	int32_t CYCLE;
	int32_t TM_LAG;
	float OUTV;
	float FiltINV;
	float LastFiltINV;
} __DIF;

// Global variables
__DIF instance;
uint16_t __assertion_error;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Automata declarations
void DIF(__DIF *__context);
void VerificationLoop();

// Automata
void DIF(__DIF *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			__context->FiltINV = (((((float) ((int32_t) __context->CYCLE)) * __context->INV) + (((float) ((int32_t) __context->Tds)) * __context->LastFiltINV)) / (((float) ((int32_t) __context->CYCLE)) + ((float) ((int32_t) __context->Tds))));
			goto l1;
		//assert(false);
		return;  			}
	l1: {
			__context->OUTV = ((((float) ((int32_t) __context->Td)) * (__context->FiltINV - __context->LastFiltINV)) / ((float) ((int32_t) __context->CYCLE)));
			goto l2;
		//assert(false);
		return;  			}
	l2: {
			__context->LastFiltINV = __context->FiltINV;
			goto l3;
		//assert(false);
		return;  			}
	l3: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			instance.CYCLE = nondet_int32_t();
			instance.INV = nondet_float();
			instance.TM_LAG = nondet_int32_t();
			instance.Td = nondet_int32_t();
			instance.Tds = nondet_int32_t();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			DIF(&instance);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
int main() {
	// Initial values
	instance.INV = 0.0;
	instance.Td = 0;
	instance.Tds = 0;
	instance.CYCLE = 0;
	instance.TM_LAG = 0;
	instance.OUTV = 0.0;
	instance.FiltINV = 0.0;
	instance.LastFiltINV = 0.0;
	__assertion_error = 0;
	
	VerificationLoop();
}
