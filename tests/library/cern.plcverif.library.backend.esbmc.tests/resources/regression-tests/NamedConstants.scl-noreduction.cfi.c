#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Variables
bool MX0_0 = false;
bool MX0_1 = false;
bool MX0_2 = false;
bool namedconst_FC1_boolVar1 = false;
bool namedconst_FC1_boolVar2 = false;
int16_t namedconst_FC1_intVar1 = 0;
int16_t namedconst_FC1_intVar2 = 0;
int16_t namedconst_FC2_intVar = 0;
uint16_t __assertion_error = 0;
int16_t namedconst_FC2_arr1_1 = 5;
int16_t namedconst_FC2_arr1_2 = 5;
int16_t namedconst_FC2_arr1_3 = 5;
int16_t namedconst_FC2_arr1_4 = 5;
int16_t namedconst_FC2_arr1_5 = 5;
int16_t namedconst_FC2_arr1_6 = 5;
int16_t namedconst_FC2_arr1_7 = 5;
int16_t namedconst_FC2_arr1_8 = 5;
int16_t namedconst_FC2_arr1_9 = 5;
int16_t namedconst_FC2_arr1_10 = 5;
bool namedconst_FC2_arr2_1 = true;
bool namedconst_FC2_arr2_2 = true;
bool namedconst_FC2_arr2_3 = true;
bool namedconst_FC2_arr2_4 = true;
bool namedconst_FC2_arr2_5 = true;
bool namedconst_FC2_arr2_6 = true;
bool namedconst_FC2_arr2_7 = true;
bool namedconst_FC2_arr2_8 = true;
bool namedconst_FC2_arr2_9 = true;
bool namedconst_FC2_arr2_10 = true;
bool namedconst_FC2_arr3_1 = true;
bool namedconst_FC2_arr3_2 = true;
bool namedconst_FC2_arr3_3 = true;
bool namedconst_FC2_arr3_4 = true;
bool namedconst_FC2_arr3_5 = true;
bool namedconst_FC2_arr3_6 = true;
bool namedconst_FC2_arr3_7 = true;
bool namedconst_FC2_arr3_8 = true;
bool namedconst_FC2_arr3_9 = true;
bool namedconst_FC2_arr3_10 = true;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			goto verificationLoop_VerificationLoop_namedconst_OB1_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			goto verificationLoop_VerificationLoop_namedconst_OB1_init1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
			goto callEnd;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_namedconst_OB1_init: {
			namedconst_FC1_intVar1 = 1;
			goto verificationLoop_VerificationLoop_namedconst_OB1_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_namedconst_OB1_l1: {
			namedconst_FC1_intVar1 = (1 + namedconst_FC1_intVar2);
			goto verificationLoop_VerificationLoop_namedconst_OB1_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_namedconst_OB1_l2: {
			namedconst_FC1_intVar1 = (1 + 2);
			goto verificationLoop_VerificationLoop_namedconst_OB1_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_namedconst_OB1_l3: {
			namedconst_FC1_boolVar1 = true;
			goto verificationLoop_VerificationLoop_namedconst_OB1_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_namedconst_OB1_l4: {
			namedconst_FC1_boolVar1 = (true || namedconst_FC1_boolVar2);
			goto verificationLoop_VerificationLoop_namedconst_OB1_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_namedconst_OB1_l5: {
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_namedconst_OB1_init1: {
		if ((namedconst_FC2_intVar == 1)) {
			goto verificationLoop_VerificationLoop_namedconst_OB1_l21;
		}
		if ((! (namedconst_FC2_intVar == 1))) {
			goto verificationLoop_VerificationLoop_namedconst_OB1_l31;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_namedconst_OB1_l11: {
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_namedconst_OB1_l21: {
			MX0_0 = true;
			goto verificationLoop_VerificationLoop_namedconst_OB1_l41;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_namedconst_OB1_l31: {
		if ((((namedconst_FC2_intVar >= 2) && (namedconst_FC2_intVar <= 3)) || (namedconst_FC2_intVar == 10))) {
			goto verificationLoop_VerificationLoop_namedconst_OB1_l51;
		}
		if ((! (((namedconst_FC2_intVar >= 2) && (namedconst_FC2_intVar <= 3)) || (namedconst_FC2_intVar == 10)))) {
			goto verificationLoop_VerificationLoop_namedconst_OB1_l6;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_namedconst_OB1_l41: {
			goto verificationLoop_VerificationLoop_namedconst_OB1_l11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_namedconst_OB1_l51: {
			MX0_1 = true;
			goto verificationLoop_VerificationLoop_namedconst_OB1_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_namedconst_OB1_l6: {
			MX0_2 = true;
			goto verificationLoop_VerificationLoop_namedconst_OB1_l8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_namedconst_OB1_l7: {
			goto verificationLoop_VerificationLoop_namedconst_OB1_l11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_namedconst_OB1_l8: {
			goto verificationLoop_VerificationLoop_namedconst_OB1_l11;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
