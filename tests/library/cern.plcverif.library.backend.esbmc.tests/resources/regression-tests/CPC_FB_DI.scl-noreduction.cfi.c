#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Variables
bool R_EDGE_new = false;
bool R_EDGE_old = false;
bool R_EDGE_RET_VAL = false;
uint16_t instance_Manreg01 = 0;
uint16_t instance_Stsreg01 = 0;
int16_t instance_Perst_FEType = 0;
int16_t instance_Perst_index = 0;
int16_t instance_Perst_DBnum = 0;
int16_t instance_Perst_perByte = 0;
int16_t instance_Perst_perBit = 0;
int16_t instance_Perst_DBnumIoError = 0;
int16_t instance_Perst_DBposIoError = 0;
int16_t instance_Perst_DBbitIoError = 0;
bool instance_Perst_AuIhFoMo = false;
bool instance_Perst_PosSt = false;
bool instance_Perst_MPosRSt = false;
bool instance_Perst_HFSt = false;
bool instance_Perst_AuMoSt = false;
bool instance_Perst_FoMoSt = false;
bool instance_Perst_IOErrorW = false;
bool instance_Perst_IOSimuW = false;
bool instance_Perst_IOError = false;
bool instance_Perst_IOSimu = false;
bool instance_Perst_FoDiProW = false;
bool instance_Perst_HFPos = false;
bool instance_Perst_MIOErBRSt = false;
bool instance_Perst_MIOErBSetRst_old = false;
bool instance_E_MIOErBSetRst = false;
uint16_t instance_TempStsreg01 = 0;
uint16_t __assertion_error = 0;
bool instance_Manreg01b_0 = false;
bool instance_Manreg01b_1 = false;
bool instance_Manreg01b_2 = false;
bool instance_Manreg01b_3 = false;
bool instance_Manreg01b_4 = false;
bool instance_Manreg01b_5 = false;
bool instance_Manreg01b_6 = false;
bool instance_Manreg01b_7 = false;
bool instance_Manreg01b_8 = false;
bool instance_Manreg01b_9 = false;
bool instance_Manreg01b_10 = false;
bool instance_Manreg01b_11 = false;
bool instance_Manreg01b_12 = false;
bool instance_Manreg01b_13 = false;
bool instance_Manreg01b_14 = false;
bool instance_Manreg01b_15 = false;
bool instance_Stsreg01b_0 = false;
bool instance_Stsreg01b_1 = false;
bool instance_Stsreg01b_2 = false;
bool instance_Stsreg01b_3 = false;
bool instance_Stsreg01b_4 = false;
bool instance_Stsreg01b_5 = false;
bool instance_Stsreg01b_6 = false;
bool instance_Stsreg01b_7 = false;
bool instance_Stsreg01b_8 = false;
bool instance_Stsreg01b_9 = false;
bool instance_Stsreg01b_10 = false;
bool instance_Stsreg01b_11 = false;
bool instance_Stsreg01b_12 = false;
bool instance_Stsreg01b_13 = false;
bool instance_Stsreg01b_14 = false;
bool instance_Stsreg01b_15 = false;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			instance_Manreg01 = nondet_uint16_t();
			instance_Manreg01b_0 = nondet_bool();
			instance_Manreg01b_10 = nondet_bool();
			instance_Manreg01b_11 = nondet_bool();
			instance_Manreg01b_12 = nondet_bool();
			instance_Manreg01b_13 = nondet_bool();
			instance_Manreg01b_14 = nondet_bool();
			instance_Manreg01b_15 = nondet_bool();
			instance_Manreg01b_1 = nondet_bool();
			instance_Manreg01b_2 = nondet_bool();
			instance_Manreg01b_3 = nondet_bool();
			instance_Manreg01b_4 = nondet_bool();
			instance_Manreg01b_5 = nondet_bool();
			instance_Manreg01b_6 = nondet_bool();
			instance_Manreg01b_7 = nondet_bool();
			instance_Manreg01b_8 = nondet_bool();
			instance_Manreg01b_9 = nondet_bool();
			instance_Perst_AuIhFoMo = nondet_bool();
			instance_Perst_AuMoSt = nondet_bool();
			instance_Perst_DBbitIoError = nondet_int16_t();
			instance_Perst_DBnum = nondet_int16_t();
			instance_Perst_DBnumIoError = nondet_int16_t();
			instance_Perst_DBposIoError = nondet_int16_t();
			instance_Perst_FEType = nondet_int16_t();
			instance_Perst_FoDiProW = nondet_bool();
			instance_Perst_FoMoSt = nondet_bool();
			instance_Perst_HFPos = nondet_bool();
			instance_Perst_HFSt = nondet_bool();
			instance_Perst_IOError = nondet_bool();
			instance_Perst_IOErrorW = nondet_bool();
			instance_Perst_IOSimu = nondet_bool();
			instance_Perst_IOSimuW = nondet_bool();
			instance_Perst_MIOErBRSt = nondet_bool();
			instance_Perst_MIOErBSetRst_old = nondet_bool();
			instance_Perst_MPosRSt = nondet_bool();
			instance_Perst_PosSt = nondet_bool();
			instance_Perst_index = nondet_int16_t();
			instance_Perst_perBit = nondet_int16_t();
			instance_Perst_perByte = nondet_int16_t();
			instance_Stsreg01 = nondet_uint16_t();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			instance_Manreg01b_0 = ((instance_Manreg01 & 256) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			instance_TempStsreg01 = instance_Stsreg01;
			goto verificationLoop_VerificationLoop_x2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
			R_EDGE_new = instance_Manreg01b_2;
			R_EDGE_old = instance_Perst_MIOErBSetRst_old;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DI_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l3: {
		if ((instance_Manreg01b_8 || instance_Perst_AuIhFoMo)) {
			goto verificationLoop_VerificationLoop_l4;
		}
		if ((! (instance_Manreg01b_8 || instance_Perst_AuIhFoMo))) {
			goto verificationLoop_VerificationLoop_l6;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l4: {
			instance_Perst_FoMoSt = false;
			goto verificationLoop_VerificationLoop_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l5: {
			goto verificationLoop_VerificationLoop_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l6: {
			goto verificationLoop_VerificationLoop_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l7: {
		if ((instance_Manreg01b_10 && (! instance_Perst_AuIhFoMo))) {
			goto verificationLoop_VerificationLoop_l8;
		}
		if ((! (instance_Manreg01b_10 && (! instance_Perst_AuIhFoMo)))) {
			goto verificationLoop_VerificationLoop_l10;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l8: {
			instance_Perst_FoMoSt = true;
			goto verificationLoop_VerificationLoop_l9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l9: {
			goto verificationLoop_VerificationLoop_l11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l10: {
			goto verificationLoop_VerificationLoop_l11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l11: {
			instance_Perst_AuMoSt = (! instance_Perst_FoMoSt);
			goto verificationLoop_VerificationLoop_l12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l12: {
		if (instance_E_MIOErBSetRst) {
			goto verificationLoop_VerificationLoop_l13;
		}
		if ((! instance_E_MIOErBSetRst)) {
			goto verificationLoop_VerificationLoop_l15;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l13: {
			instance_Perst_MIOErBRSt = (! instance_Perst_MIOErBRSt);
			goto verificationLoop_VerificationLoop_l14;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l14: {
			goto verificationLoop_VerificationLoop_l16;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l15: {
			goto verificationLoop_VerificationLoop_l16;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l16: {
			instance_Perst_IOErrorW = (instance_Perst_IOError && (! instance_Perst_MIOErBRSt));
			goto verificationLoop_VerificationLoop_l17;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l17: {
			instance_Perst_IOSimuW = instance_Perst_IOSimu;
			goto verificationLoop_VerificationLoop_l18;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l18: {
		if (instance_Manreg01b_13) {
			goto verificationLoop_VerificationLoop_l19;
		}
		if ((! instance_Manreg01b_13)) {
			goto verificationLoop_VerificationLoop_l21;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l19: {
			instance_Perst_MPosRSt = false;
			goto verificationLoop_VerificationLoop_l20;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l20: {
			goto verificationLoop_VerificationLoop_l22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l21: {
			goto verificationLoop_VerificationLoop_l22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l22: {
		if (instance_Manreg01b_12) {
			goto verificationLoop_VerificationLoop_l23;
		}
		if ((! instance_Manreg01b_12)) {
			goto verificationLoop_VerificationLoop_l25;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l23: {
			instance_Perst_MPosRSt = true;
			goto verificationLoop_VerificationLoop_l24;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l24: {
			goto verificationLoop_VerificationLoop_l26;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l25: {
			goto verificationLoop_VerificationLoop_l26;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l26: {
		if (instance_Perst_FoMoSt) {
			goto verificationLoop_VerificationLoop_l27;
		}
		if ((! instance_Perst_FoMoSt)) {
			goto verificationLoop_VerificationLoop_l30;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l27: {
			instance_Perst_PosSt = instance_Perst_MPosRSt;
			goto verificationLoop_VerificationLoop_l28;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l28: {
			instance_Perst_FoDiProW = (instance_Perst_HFPos != instance_Perst_MPosRSt);
			goto verificationLoop_VerificationLoop_l29;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l29: {
			goto verificationLoop_VerificationLoop_l34;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l30: {
			instance_Perst_MPosRSt = instance_Perst_HFPos;
			goto verificationLoop_VerificationLoop_l31;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l31: {
			instance_Perst_PosSt = instance_Perst_HFPos;
			goto verificationLoop_VerificationLoop_l32;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l32: {
			instance_Perst_FoDiProW = false;
			goto verificationLoop_VerificationLoop_l33;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l33: {
			goto verificationLoop_VerificationLoop_l34;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l34: {
			instance_Perst_HFSt = instance_Perst_HFPos;
			goto verificationLoop_VerificationLoop_l35;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l35: {
			instance_Stsreg01b_8 = instance_Perst_PosSt;
			goto verificationLoop_VerificationLoop_x3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l36: {
			instance_Stsreg01b_9 = false;
			goto verificationLoop_VerificationLoop_x4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l37: {
			instance_Stsreg01b_10 = instance_Perst_AuMoSt;
			goto verificationLoop_VerificationLoop_x5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l38: {
			instance_Stsreg01b_11 = false;
			goto verificationLoop_VerificationLoop_x6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l39: {
			instance_Stsreg01b_12 = instance_Perst_FoMoSt;
			goto verificationLoop_VerificationLoop_x7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l40: {
			instance_Stsreg01b_13 = false;
			goto verificationLoop_VerificationLoop_x8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l41: {
			instance_Stsreg01b_14 = instance_Perst_IOErrorW;
			goto verificationLoop_VerificationLoop_x9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l42: {
			instance_Stsreg01b_15 = instance_Perst_IOSimuW;
			goto verificationLoop_VerificationLoop_x10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l43: {
			instance_Stsreg01b_0 = instance_Perst_FoDiProW;
			goto verificationLoop_VerificationLoop_x11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l44: {
			instance_Stsreg01b_1 = instance_Perst_MIOErBRSt;
			goto verificationLoop_VerificationLoop_x12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l45: {
			instance_Stsreg01b_2 = false;
			goto verificationLoop_VerificationLoop_x13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l46: {
			instance_Stsreg01b_3 = false;
			goto verificationLoop_VerificationLoop_x14;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l47: {
			instance_Stsreg01b_4 = false;
			goto verificationLoop_VerificationLoop_x15;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l48: {
			instance_Stsreg01b_5 = instance_Perst_AuIhFoMo;
			goto verificationLoop_VerificationLoop_x16;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l49: {
			instance_Stsreg01b_6 = instance_Perst_HFSt;
			goto verificationLoop_VerificationLoop_x17;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l50: {
			instance_Stsreg01b_7 = instance_Perst_MPosRSt;
			goto verificationLoop_VerificationLoop_x18;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l51: {
			instance_Stsreg01 = instance_TempStsreg01;
			goto verificationLoop_VerificationLoop_l52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l52: {
			goto callEnd;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x: {
			instance_E_MIOErBSetRst = false;
			instance_TempStsreg01 = 0;
			goto verificationLoop_VerificationLoop_x1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh: {
			instance_Stsreg01b_0 = ((instance_TempStsreg01 & 256) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh16;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh1: {
			instance_Manreg01b_1 = ((instance_Manreg01 & 512) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh2: {
			instance_Manreg01b_2 = ((instance_Manreg01 & 1024) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh3: {
			instance_Manreg01b_3 = ((instance_Manreg01 & 2048) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh4: {
			instance_Manreg01b_4 = ((instance_Manreg01 & 4096) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh5: {
			instance_Manreg01b_5 = ((instance_Manreg01 & 8192) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh6: {
			instance_Manreg01b_6 = ((instance_Manreg01 & 16384) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh7: {
			instance_Manreg01b_7 = ((instance_Manreg01 & 32768) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh8: {
			instance_Manreg01b_8 = ((instance_Manreg01 & 1) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh9: {
			instance_Manreg01b_9 = ((instance_Manreg01 & 2) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh10: {
			instance_Manreg01b_10 = ((instance_Manreg01 & 4) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh11: {
			instance_Manreg01b_11 = ((instance_Manreg01 & 8) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh12: {
			instance_Manreg01b_12 = ((instance_Manreg01 & 16) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh13: {
			instance_Manreg01b_13 = ((instance_Manreg01 & 32) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh14;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh14: {
			instance_Manreg01b_14 = ((instance_Manreg01 & 64) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh15;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh15: {
			instance_Manreg01b_15 = ((instance_Manreg01 & 128) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh16: {
			instance_Stsreg01b_1 = ((instance_TempStsreg01 & 512) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh17;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh17: {
			instance_Stsreg01b_2 = ((instance_TempStsreg01 & 1024) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh18;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh18: {
			instance_Stsreg01b_3 = ((instance_TempStsreg01 & 2048) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh19;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh19: {
			instance_Stsreg01b_4 = ((instance_TempStsreg01 & 4096) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh20;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh20: {
			instance_Stsreg01b_5 = ((instance_TempStsreg01 & 8192) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh21: {
			instance_Stsreg01b_6 = ((instance_TempStsreg01 & 16384) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh22: {
			instance_Stsreg01b_7 = ((instance_TempStsreg01 & 32768) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh23;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh23: {
			instance_Stsreg01b_8 = ((instance_TempStsreg01 & 1) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh24;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh24: {
			instance_Stsreg01b_9 = ((instance_TempStsreg01 & 2) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh25;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh25: {
			instance_Stsreg01b_10 = ((instance_TempStsreg01 & 4) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh26;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh26: {
			instance_Stsreg01b_11 = ((instance_TempStsreg01 & 8) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh27;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh27: {
			instance_Stsreg01b_12 = ((instance_TempStsreg01 & 16) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh28;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh28: {
			instance_Stsreg01b_13 = ((instance_TempStsreg01 & 32) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh29;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh29: {
			instance_Stsreg01b_14 = ((instance_TempStsreg01 & 64) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh30;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh30: {
			instance_Stsreg01b_15 = ((instance_TempStsreg01 & 128) != 0);
			goto verificationLoop_VerificationLoop_x;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x1: {
			instance_Stsreg01b_0 = ((instance_TempStsreg01 & 256) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh31;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh31: {
			instance_Stsreg01b_1 = ((instance_TempStsreg01 & 512) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh32;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh32: {
			instance_Stsreg01b_2 = ((instance_TempStsreg01 & 1024) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh33;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh33: {
			instance_Stsreg01b_3 = ((instance_TempStsreg01 & 2048) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh34;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh34: {
			instance_Stsreg01b_4 = ((instance_TempStsreg01 & 4096) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh35;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh35: {
			instance_Stsreg01b_5 = ((instance_TempStsreg01 & 8192) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh36;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh36: {
			instance_Stsreg01b_6 = ((instance_TempStsreg01 & 16384) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh37;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh37: {
			instance_Stsreg01b_7 = ((instance_TempStsreg01 & 32768) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh38;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh38: {
			instance_Stsreg01b_8 = ((instance_TempStsreg01 & 1) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh39;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh39: {
			instance_Stsreg01b_9 = ((instance_TempStsreg01 & 2) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh40;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh40: {
			instance_Stsreg01b_10 = ((instance_TempStsreg01 & 4) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh41;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh41: {
			instance_Stsreg01b_11 = ((instance_TempStsreg01 & 8) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh42;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh42: {
			instance_Stsreg01b_12 = ((instance_TempStsreg01 & 16) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh43;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh43: {
			instance_Stsreg01b_13 = ((instance_TempStsreg01 & 32) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh44;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh44: {
			instance_Stsreg01b_14 = ((instance_TempStsreg01 & 64) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh45;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh45: {
			instance_Stsreg01b_15 = ((instance_TempStsreg01 & 128) != 0);
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x2: {
			instance_Stsreg01b_0 = ((instance_TempStsreg01 & 256) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh46;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh46: {
			instance_Stsreg01b_1 = ((instance_TempStsreg01 & 512) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh47;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh47: {
			instance_Stsreg01b_2 = ((instance_TempStsreg01 & 1024) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh48;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh48: {
			instance_Stsreg01b_3 = ((instance_TempStsreg01 & 2048) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh49;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh49: {
			instance_Stsreg01b_4 = ((instance_TempStsreg01 & 4096) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh50;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh50: {
			instance_Stsreg01b_5 = ((instance_TempStsreg01 & 8192) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh51;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh51: {
			instance_Stsreg01b_6 = ((instance_TempStsreg01 & 16384) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh52: {
			instance_Stsreg01b_7 = ((instance_TempStsreg01 & 32768) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh53;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh53: {
			instance_Stsreg01b_8 = ((instance_TempStsreg01 & 1) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh54;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh54: {
			instance_Stsreg01b_9 = ((instance_TempStsreg01 & 2) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh55;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh55: {
			instance_Stsreg01b_10 = ((instance_TempStsreg01 & 4) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh56;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh56: {
			instance_Stsreg01b_11 = ((instance_TempStsreg01 & 8) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh57;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh57: {
			instance_Stsreg01b_12 = ((instance_TempStsreg01 & 16) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh58;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh58: {
			instance_Stsreg01b_13 = ((instance_TempStsreg01 & 32) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh59;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh59: {
			instance_Stsreg01b_14 = ((instance_TempStsreg01 & 64) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh60;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh60: {
			instance_Stsreg01b_15 = ((instance_TempStsreg01 & 128) != 0);
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x3: {
		if (instance_Stsreg01b_8) {
			instance_TempStsreg01 = (instance_TempStsreg01 | 1);
			goto verificationLoop_VerificationLoop_l36;
		}
		if ((! instance_Stsreg01b_8)) {
			instance_TempStsreg01 = (instance_TempStsreg01 & 65534);
			goto verificationLoop_VerificationLoop_l36;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x4: {
		if (instance_Stsreg01b_9) {
			instance_TempStsreg01 = (instance_TempStsreg01 | 2);
			goto verificationLoop_VerificationLoop_l37;
		}
		if ((! instance_Stsreg01b_9)) {
			instance_TempStsreg01 = (instance_TempStsreg01 & 65533);
			goto verificationLoop_VerificationLoop_l37;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x5: {
		if (instance_Stsreg01b_10) {
			instance_TempStsreg01 = (instance_TempStsreg01 | 4);
			goto verificationLoop_VerificationLoop_l38;
		}
		if ((! instance_Stsreg01b_10)) {
			instance_TempStsreg01 = (instance_TempStsreg01 & 65531);
			goto verificationLoop_VerificationLoop_l38;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x6: {
		if (instance_Stsreg01b_11) {
			instance_TempStsreg01 = (instance_TempStsreg01 | 8);
			goto verificationLoop_VerificationLoop_l39;
		}
		if ((! instance_Stsreg01b_11)) {
			instance_TempStsreg01 = (instance_TempStsreg01 & 65527);
			goto verificationLoop_VerificationLoop_l39;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x7: {
		if (instance_Stsreg01b_12) {
			instance_TempStsreg01 = (instance_TempStsreg01 | 16);
			goto verificationLoop_VerificationLoop_l40;
		}
		if ((! instance_Stsreg01b_12)) {
			instance_TempStsreg01 = (instance_TempStsreg01 & 65519);
			goto verificationLoop_VerificationLoop_l40;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x8: {
		if (instance_Stsreg01b_13) {
			instance_TempStsreg01 = (instance_TempStsreg01 | 32);
			goto verificationLoop_VerificationLoop_l41;
		}
		if ((! instance_Stsreg01b_13)) {
			instance_TempStsreg01 = (instance_TempStsreg01 & 65503);
			goto verificationLoop_VerificationLoop_l41;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x9: {
		if (instance_Stsreg01b_14) {
			instance_TempStsreg01 = (instance_TempStsreg01 | 64);
			goto verificationLoop_VerificationLoop_l42;
		}
		if ((! instance_Stsreg01b_14)) {
			instance_TempStsreg01 = (instance_TempStsreg01 & 65471);
			goto verificationLoop_VerificationLoop_l42;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x10: {
		if (instance_Stsreg01b_15) {
			instance_TempStsreg01 = (instance_TempStsreg01 | 128);
			goto verificationLoop_VerificationLoop_l43;
		}
		if ((! instance_Stsreg01b_15)) {
			instance_TempStsreg01 = (instance_TempStsreg01 & 65407);
			goto verificationLoop_VerificationLoop_l43;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x11: {
		if (instance_Stsreg01b_0) {
			instance_TempStsreg01 = (instance_TempStsreg01 | 256);
			goto verificationLoop_VerificationLoop_l44;
		}
		if ((! instance_Stsreg01b_0)) {
			instance_TempStsreg01 = (instance_TempStsreg01 & 65279);
			goto verificationLoop_VerificationLoop_l44;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x12: {
		if (instance_Stsreg01b_1) {
			instance_TempStsreg01 = (instance_TempStsreg01 | 512);
			goto verificationLoop_VerificationLoop_l45;
		}
		if ((! instance_Stsreg01b_1)) {
			instance_TempStsreg01 = (instance_TempStsreg01 & 65023);
			goto verificationLoop_VerificationLoop_l45;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x13: {
		if (instance_Stsreg01b_2) {
			instance_TempStsreg01 = (instance_TempStsreg01 | 1024);
			goto verificationLoop_VerificationLoop_l46;
		}
		if ((! instance_Stsreg01b_2)) {
			instance_TempStsreg01 = (instance_TempStsreg01 & 64511);
			goto verificationLoop_VerificationLoop_l46;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x14: {
		if (instance_Stsreg01b_3) {
			instance_TempStsreg01 = (instance_TempStsreg01 | 2048);
			goto verificationLoop_VerificationLoop_l47;
		}
		if ((! instance_Stsreg01b_3)) {
			instance_TempStsreg01 = (instance_TempStsreg01 & 63487);
			goto verificationLoop_VerificationLoop_l47;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x15: {
		if (instance_Stsreg01b_4) {
			instance_TempStsreg01 = (instance_TempStsreg01 | 4096);
			goto verificationLoop_VerificationLoop_l48;
		}
		if ((! instance_Stsreg01b_4)) {
			instance_TempStsreg01 = (instance_TempStsreg01 & 61439);
			goto verificationLoop_VerificationLoop_l48;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x16: {
		if (instance_Stsreg01b_5) {
			instance_TempStsreg01 = (instance_TempStsreg01 | 8192);
			goto verificationLoop_VerificationLoop_l49;
		}
		if ((! instance_Stsreg01b_5)) {
			instance_TempStsreg01 = (instance_TempStsreg01 & 57343);
			goto verificationLoop_VerificationLoop_l49;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x17: {
		if (instance_Stsreg01b_6) {
			instance_TempStsreg01 = (instance_TempStsreg01 | 16384);
			goto verificationLoop_VerificationLoop_l50;
		}
		if ((! instance_Stsreg01b_6)) {
			instance_TempStsreg01 = (instance_TempStsreg01 & 49151);
			goto verificationLoop_VerificationLoop_l50;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x18: {
		if (instance_Stsreg01b_7) {
			instance_TempStsreg01 = (instance_TempStsreg01 | 32768);
			goto verificationLoop_VerificationLoop_l51;
		}
		if ((! instance_Stsreg01b_7)) {
			instance_TempStsreg01 = (instance_TempStsreg01 & 32767);
			goto verificationLoop_VerificationLoop_l51;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DI_init: {
		if (((R_EDGE_new == true) && (R_EDGE_old == false))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DI_l1;
		}
		if ((! ((R_EDGE_new == true) && (R_EDGE_old == false)))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DI_l4;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DI_l1: {
			R_EDGE_RET_VAL = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DI_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DI_l2: {
			R_EDGE_old = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DI_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DI_l3: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DI_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DI_l4: {
			R_EDGE_RET_VAL = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DI_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DI_l5: {
			R_EDGE_old = R_EDGE_new;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DI_l6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DI_l6: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_DI_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_DI_l7: {
			instance_Perst_MIOErBSetRst_old = R_EDGE_old;
			instance_E_MIOErBSetRst = R_EDGE_RET_VAL;
			goto verificationLoop_VerificationLoop_l3;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
