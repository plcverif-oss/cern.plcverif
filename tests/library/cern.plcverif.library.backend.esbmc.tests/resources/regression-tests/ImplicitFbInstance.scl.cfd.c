#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Root data structure
typedef struct {
	bool a;
	bool b;
} __implInstFb1;

// Global variables
__implInstFb1 instance;
uint16_t __assertion_error;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Automata declarations
void implInstFb1(__implInstFb1 *__context);
void VerificationLoop();

// Automata
void implInstFb1(__implInstFb1 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			__context->b = __context->a;
			goto l1;
		//assert(false);
		return;  			}
	l1: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			instance.a = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			implInstFb1(&instance);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
int main() {
	// Initial values
	instance.a = false;
	instance.b = false;
	__assertion_error = 0;
	
	VerificationLoop();
}
