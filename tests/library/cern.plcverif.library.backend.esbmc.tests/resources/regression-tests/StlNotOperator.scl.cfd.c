#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Root data structure
typedef struct {
	uint16_t in;
	bool inbool;
	uint16_t out;
	bool outbool;
} __NotTest;

// Global variables
__NotTest NotTest1;
bool __RLO;
bool __NFC;
bool __BR;
bool __STA;
bool __OR;
bool __CC0;
bool __CC1;
bool __OS;
bool __OV;
int32_t __ACCU1;
int32_t __ACCU2;
uint16_t __assertion_error;
uint16_t __assertion_error;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Automata declarations
void NotTest(__NotTest *__context);
void VerificationLoop();
void VerificationLoop1();

// Automata
void NotTest(__NotTest *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			__ACCU2 = __ACCU1;
			goto l1;
		//assert(false);
		return;  			}
	l1: {
			__ACCU1 = ((int32_t) __context->in);
			goto l2;
		//assert(false);
		return;  			}
	l2: {
			__ACCU2 = __ACCU1;
			goto l3;
		//assert(false);
		return;  			}
	l3: {
			__ACCU1 = ((int32_t) 65535);
			goto l4;
		//assert(false);
		return;  			}
	l4: {
			__ACCU1 = (((__ACCU1 & 65535) ^ (__ACCU2 & 65535)) | (__ACCU1 & -65536));
			goto l5;
		//assert(false);
		return;  			}
	l5: {
			__CC0 = false;
			__OV = false;
			__CC1 = ((__ACCU1 & 65535) == 0);
			__context->out = ((uint16_t) __ACCU1);
			goto l7;
		//assert(false);
		return;  			}
	l7: {
		if ((! (((uint16_t)~ __context->in) == __context->out))) {
			__assertion_error = 1;
			goto end;
		}
		if (((! (! (((uint16_t)~ __context->in) == __context->out))) && (! (((uint16_t)~ (__context->in & __context->in)) == __context->out)))) {
			__assertion_error = 2;
			goto end;
		}
		if (((! (! (((uint16_t)~ __context->in) == __context->out))) && (! (! (((uint16_t)~ (__context->in & __context->in)) == __context->out))))) {
			__ACCU2 = __ACCU1;
			goto l8;
		}
		//assert(false);
		return;  			}
	l8: {
			__ACCU1 = ((int32_t) 0);
			goto l9;
		//assert(false);
		return;  			}
	l9: {
			__context->out = ((uint16_t) __ACCU1);
			goto l10;
		//assert(false);
		return;  			}
	l10: {
		if ((! (((uint16_t)~ __context->out) == 65535))) {
			__assertion_error = 3;
			goto end;
		}
		if ((! (! (((uint16_t)~ __context->out) == 65535)))) {
			__RLO = true;
			__STA = true;
			__OR = false;
			__NFC = false;
			goto l11;
		}
		//assert(false);
		return;  			}
	l11: {
			__RLO = (((! __NFC) || __RLO) && (__context->inbool || __OR));
			goto l12;
		//assert(false);
		return;  			}
	l12: {
			__OR = (__OR && __NFC);
			goto l13;
		//assert(false);
		return;  			}
	l13: {
			__STA = __context->inbool;
			__NFC = true;
			goto l14;
		//assert(false);
		return;  			}
	l14: {
			__RLO = ((__NFC && __RLO) != true);
			__OR = false;
			goto l16;
		//assert(false);
		return;  			}
	l16: {
			__STA = true;
			__NFC = true;
			__context->outbool = __RLO;
			goto l18;
		//assert(false);
		return;  			}
	l18: {
			__OR = false;
			__STA = __RLO;
			__NFC = false;
			goto l21;
		//assert(false);
		return;  			}
	l21: {
		if ((! ((! __context->inbool) == __context->outbool))) {
			__assertion_error = 4;
			goto end;
		}
		if (((! (! ((! __context->inbool) == __context->outbool))) && (! ((! (__context->inbool && __context->inbool)) == __context->outbool)))) {
			__assertion_error = 5;
			goto end;
		}
		if (((! (! ((! __context->inbool) == __context->outbool))) && (! (! ((! (__context->inbool && __context->inbool)) == __context->outbool))))) {
			__RLO = true;
			__STA = true;
			__OR = false;
			__NFC = false;
			goto l22;
		}
		//assert(false);
		return;  			}
	l22: {
			__RLO = (((! __NFC) || __RLO) && (false || __OR));
			goto l23;
		//assert(false);
		return;  			}
	l23: {
			__OR = (__OR && __NFC);
			goto l24;
		//assert(false);
		return;  			}
	l24: {
			__STA = false;
			__NFC = true;
			__context->outbool = __RLO;
			goto l26;
		//assert(false);
		return;  			}
	l26: {
			__OR = false;
			__STA = __RLO;
			__NFC = false;
			goto l29;
		//assert(false);
		return;  			}
	l29: {
		if ((! ((! __context->outbool) == true))) {
			__assertion_error = 6;
			goto end;
		}
		if ((! (! ((! __context->outbool) == true)))) {
			goto end;
		}
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			goto loop_start;
		//assert(false);
		return;  			}
	end1: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			NotTest1.in = nondet_uint16_t();
			NotTest1.inbool = nondet_bool();
			goto l_main_call;
		if (false) {
			goto end1;
		}
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			NotTest(&NotTest1);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop1() {
	// Temporary variables
	
	// Start with initial location
	goto init2;
	init2: {
			goto loop_start1;
		//assert(false);
		return;  			}
	end2: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start1: {
			goto prepare_BoC;
		if (false) {
			goto end2;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call1;
		//assert(false);
		return;  			}
	l_main_call1: {
			VerificationLoop();
			goto callEnd1;
		//assert(false);
		return;  			}
	callEnd1: {
			goto prepare_EoC1;
		//assert(false);
		return;  			}
	prepare_EoC1: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start1;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
int main() {
	// Initial values
	NotTest1.in = 0;
	NotTest1.inbool = false;
	NotTest1.out = 0;
	NotTest1.outbool = false;
	__RLO = false;
	__NFC = false;
	__BR = false;
	__STA = false;
	__OR = false;
	__CC0 = false;
	__CC1 = false;
	__OS = false;
	__OV = false;
	__ACCU1 = 0;
	__ACCU2 = 0;
	__assertion_error = 0;
	
	VerificationLoop1();
}
