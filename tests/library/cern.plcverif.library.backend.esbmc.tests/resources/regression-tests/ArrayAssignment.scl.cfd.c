#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Root data structure
typedef struct {
	int16_t a[3];
	int16_t b[3];
} __pv186;

// Global variables
__pv186 instance;
uint16_t __assertion_error;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Automata declarations
void pv186(__pv186 *__context);
void VerificationLoop();

// Automata
void pv186(__pv186 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			__context->b[1] = __context->a[1];
			goto l1;
		//assert(false);
		return;  			}
	l1: {
			memmove(&(__context->b), &(__context->a), sizeof __context->b);
			goto l2;
		//assert(false);
		return;  			}
	l2: {
		if ((! (__context->a[1] == __context->b[1]))) {
			__assertion_error = 1;
			goto end;
		}
		if ((! (! (__context->a[1] == __context->b[1])))) {
			goto end;
		}
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			goto loop_start;
		//assert(false);
		return;  			}
	end1: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end1;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			pv186(&instance);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
int main() {
	// Initial values
	instance.a[1] = 1;
	instance.a[2] = 2;
	instance.b[1] = 0;
	instance.b[2] = 0;
	__assertion_error = 0;
	
	VerificationLoop();
}
