#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Root data structure
typedef struct {
	uint16_t ParReg;
} __CPC_LOCAL_PARAM;
typedef struct {
	bool new;
	bool old;
	bool RET_VAL;
} __R_EDGE;
typedef struct {
	bool ParRegb[16];
} __anonymous_type;
typedef struct {
	bool HFOn;
	bool HFOff;
	bool IOError;
	bool IOSimu;
	uint16_t Manreg01;
	bool Manreg01b[16];
	__CPC_LOCAL_PARAM PLocal;
	__anonymous_type PLocalb;
	uint16_t Stsreg01;
	bool Stsreg01b[16];
	bool OnSt;
	bool OffSt;
	bool IOErrorW;
	bool IOSimuW;
	bool PosAlSt;
	bool PosW;
	bool E_PosAlSt;
	bool PosAlSt_old;
	bool PHFOn;
	bool PHFOff;
	bool PPosAlE;
	bool PAnim;
} __CPC_FB_LOCAL;

// Global variables
__R_EDGE R_EDGE1;
__CPC_FB_LOCAL instance;
uint16_t __assertion_error;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Automata declarations
void CPC_FB_LOCAL(__CPC_FB_LOCAL *__context);
void R_EDGE(__R_EDGE *__context);
void VerificationLoop();

// Automata
void CPC_FB_LOCAL(__CPC_FB_LOCAL *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			__context->Manreg01b[0] = ((__context->Manreg01 & 256) != 0);
			__context->Manreg01b[1] = ((__context->Manreg01 & 512) != 0);
			__context->Manreg01b[2] = ((__context->Manreg01 & 1024) != 0);
			__context->Manreg01b[3] = ((__context->Manreg01 & 2048) != 0);
			__context->Manreg01b[4] = ((__context->Manreg01 & 4096) != 0);
			__context->Manreg01b[5] = ((__context->Manreg01 & 8192) != 0);
			__context->Manreg01b[6] = ((__context->Manreg01 & 16384) != 0);
			__context->Manreg01b[7] = ((__context->Manreg01 & 32768) != 0);
			__context->Manreg01b[8] = ((__context->Manreg01 & 1) != 0);
			__context->Manreg01b[9] = ((__context->Manreg01 & 2) != 0);
			__context->Manreg01b[10] = ((__context->Manreg01 & 4) != 0);
			__context->Manreg01b[11] = ((__context->Manreg01 & 8) != 0);
			__context->Manreg01b[12] = ((__context->Manreg01 & 16) != 0);
			__context->Manreg01b[13] = ((__context->Manreg01 & 32) != 0);
			__context->Manreg01b[14] = ((__context->Manreg01 & 64) != 0);
			__context->Manreg01b[15] = ((__context->Manreg01 & 128) != 0);
			__context->PLocalb.ParRegb[0] = ((__context->PLocal.ParReg & 256) != 0);
			__context->PLocalb.ParRegb[1] = ((__context->PLocal.ParReg & 512) != 0);
			__context->PLocalb.ParRegb[2] = ((__context->PLocal.ParReg & 1024) != 0);
			__context->PLocalb.ParRegb[3] = ((__context->PLocal.ParReg & 2048) != 0);
			__context->PLocalb.ParRegb[4] = ((__context->PLocal.ParReg & 4096) != 0);
			__context->PLocalb.ParRegb[5] = ((__context->PLocal.ParReg & 8192) != 0);
			__context->PLocalb.ParRegb[6] = ((__context->PLocal.ParReg & 16384) != 0);
			__context->PLocalb.ParRegb[7] = ((__context->PLocal.ParReg & 32768) != 0);
			__context->PLocalb.ParRegb[8] = ((__context->PLocal.ParReg & 1) != 0);
			__context->PLocalb.ParRegb[9] = ((__context->PLocal.ParReg & 2) != 0);
			__context->PLocalb.ParRegb[10] = ((__context->PLocal.ParReg & 4) != 0);
			__context->PLocalb.ParRegb[11] = ((__context->PLocal.ParReg & 8) != 0);
			__context->PLocalb.ParRegb[12] = ((__context->PLocal.ParReg & 16) != 0);
			__context->PLocalb.ParRegb[13] = ((__context->PLocal.ParReg & 32) != 0);
			__context->PLocalb.ParRegb[14] = ((__context->PLocal.ParReg & 64) != 0);
			__context->PLocalb.ParRegb[15] = ((__context->PLocal.ParReg & 128) != 0);
			__context->Stsreg01b[0] = ((__context->Stsreg01 & 256) != 0);
			goto varview_refresh64;
		//assert(false);
		return;  			}
	l4: {
			__context->OnSt = ((__context->HFOn && __context->PHFOn) || (((! __context->PHFOn) && __context->PAnim) && (! __context->HFOff)));
			__context->OffSt = ((__context->HFOff && __context->PHFOff) || (((! __context->PHFOff) && __context->PAnim) && (! __context->HFOn)));
			__context->IOErrorW = __context->IOError;
			__context->IOSimuW = __context->IOSimu;
			goto l8;
		//assert(false);
		return;  			}
	l8: {
			__context->PosW = (((__context->OnSt == __context->OffSt) && __context->PHFOn) && __context->PHFOff);
			__context->PosAlSt = ((__context->PPosAlE && (__context->PHFOn != __context->PHFOff)) && ((__context->PHFOn && (! __context->HFOn)) || (__context->PHFOff && (! __context->HFOff))));
			goto l10;
		//assert(false);
		return;  			}
	l10: {
			// Assign inputs
			R_EDGE1.new = __context->PosAlSt;
			R_EDGE1.old = __context->PosAlSt_old;
			R_EDGE(&R_EDGE1);
			// Assign outputs
			__context->PosAlSt_old = R_EDGE1.old;
			__context->E_PosAlSt = R_EDGE1.RET_VAL;
			goto l11;
		//assert(false);
		return;  			}
	l11: {
			__context->Stsreg01b[8] = __context->OnSt;
			goto x1;
		//assert(false);
		return;  			}
	l12: {
			__context->Stsreg01b[9] = __context->OffSt;
			goto x2;
		//assert(false);
		return;  			}
	l13: {
			__context->Stsreg01b[10] = false;
			goto x3;
		//assert(false);
		return;  			}
	l14: {
			__context->Stsreg01b[11] = false;
			goto x4;
		//assert(false);
		return;  			}
	l15: {
			__context->Stsreg01b[12] = false;
			goto x5;
		//assert(false);
		return;  			}
	l16: {
			__context->Stsreg01b[13] = false;
			goto x6;
		//assert(false);
		return;  			}
	l17: {
			__context->Stsreg01b[14] = __context->IOError;
			goto x7;
		//assert(false);
		return;  			}
	l18: {
			__context->Stsreg01b[15] = __context->IOSimu;
			goto x8;
		//assert(false);
		return;  			}
	l19: {
			__context->Stsreg01b[0] = false;
			goto x9;
		//assert(false);
		return;  			}
	l20: {
			__context->Stsreg01b[1] = __context->PosW;
			goto x10;
		//assert(false);
		return;  			}
	l21: {
			__context->Stsreg01b[2] = __context->PosAlSt;
			goto x11;
		//assert(false);
		return;  			}
	l22: {
			__context->Stsreg01b[3] = false;
			goto x12;
		//assert(false);
		return;  			}
	l23: {
			__context->Stsreg01b[4] = false;
			goto x13;
		//assert(false);
		return;  			}
	l24: {
			__context->Stsreg01b[5] = false;
			goto x14;
		//assert(false);
		return;  			}
	l25: {
			__context->Stsreg01b[6] = false;
			goto x15;
		//assert(false);
		return;  			}
	l26: {
			__context->Stsreg01b[7] = false;
			goto x16;
		//assert(false);
		return;  			}
	l27: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	varview_refresh64: {
			__context->Stsreg01b[1] = ((__context->Stsreg01 & 512) != 0);
			__context->Stsreg01b[2] = ((__context->Stsreg01 & 1024) != 0);
			__context->Stsreg01b[3] = ((__context->Stsreg01 & 2048) != 0);
			__context->Stsreg01b[4] = ((__context->Stsreg01 & 4096) != 0);
			__context->Stsreg01b[5] = ((__context->Stsreg01 & 8192) != 0);
			__context->Stsreg01b[6] = ((__context->Stsreg01 & 16384) != 0);
			__context->Stsreg01b[7] = ((__context->Stsreg01 & 32768) != 0);
			__context->Stsreg01b[8] = ((__context->Stsreg01 & 1) != 0);
			__context->Stsreg01b[9] = ((__context->Stsreg01 & 2) != 0);
			__context->Stsreg01b[10] = ((__context->Stsreg01 & 4) != 0);
			__context->Stsreg01b[11] = ((__context->Stsreg01 & 8) != 0);
			__context->Stsreg01b[12] = ((__context->Stsreg01 & 16) != 0);
			__context->Stsreg01b[13] = ((__context->Stsreg01 & 32) != 0);
			__context->Stsreg01b[14] = ((__context->Stsreg01 & 64) != 0);
			__context->Stsreg01b[15] = ((__context->Stsreg01 & 128) != 0);
			__context->PHFOn = __context->PLocalb.ParRegb[9];
			__context->PHFOff = __context->PLocalb.ParRegb[10];
			__context->PPosAlE = __context->PLocalb.ParRegb[2];
			__context->PAnim = __context->PLocalb.ParRegb[14];
			goto l4;
		//assert(false);
		return;  			}
	x1: {
		if (__context->Stsreg01b[8]) {
			__context->Stsreg01 = (__context->Stsreg01 | 1);
			goto l12;
		}
		if ((! __context->Stsreg01b[8])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65534);
			goto l12;
		}
		//assert(false);
		return;  			}
	x2: {
		if (__context->Stsreg01b[9]) {
			__context->Stsreg01 = (__context->Stsreg01 | 2);
			goto l13;
		}
		if ((! __context->Stsreg01b[9])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65533);
			goto l13;
		}
		//assert(false);
		return;  			}
	x3: {
		if (__context->Stsreg01b[10]) {
			__context->Stsreg01 = (__context->Stsreg01 | 4);
			goto l14;
		}
		if ((! __context->Stsreg01b[10])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65531);
			goto l14;
		}
		//assert(false);
		return;  			}
	x4: {
		if (__context->Stsreg01b[11]) {
			__context->Stsreg01 = (__context->Stsreg01 | 8);
			goto l15;
		}
		if ((! __context->Stsreg01b[11])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65527);
			goto l15;
		}
		//assert(false);
		return;  			}
	x5: {
		if (__context->Stsreg01b[12]) {
			__context->Stsreg01 = (__context->Stsreg01 | 16);
			goto l16;
		}
		if ((! __context->Stsreg01b[12])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65519);
			goto l16;
		}
		//assert(false);
		return;  			}
	x6: {
		if (__context->Stsreg01b[13]) {
			__context->Stsreg01 = (__context->Stsreg01 | 32);
			goto l17;
		}
		if ((! __context->Stsreg01b[13])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65503);
			goto l17;
		}
		//assert(false);
		return;  			}
	x7: {
		if (__context->Stsreg01b[14]) {
			__context->Stsreg01 = (__context->Stsreg01 | 64);
			goto l18;
		}
		if ((! __context->Stsreg01b[14])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65471);
			goto l18;
		}
		//assert(false);
		return;  			}
	x8: {
		if (__context->Stsreg01b[15]) {
			__context->Stsreg01 = (__context->Stsreg01 | 128);
			goto l19;
		}
		if ((! __context->Stsreg01b[15])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65407);
			goto l19;
		}
		//assert(false);
		return;  			}
	x9: {
		if (__context->Stsreg01b[0]) {
			__context->Stsreg01 = (__context->Stsreg01 | 256);
			goto l20;
		}
		if ((! __context->Stsreg01b[0])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65279);
			goto l20;
		}
		//assert(false);
		return;  			}
	x10: {
		if (__context->Stsreg01b[1]) {
			__context->Stsreg01 = (__context->Stsreg01 | 512);
			goto l21;
		}
		if ((! __context->Stsreg01b[1])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65023);
			goto l21;
		}
		//assert(false);
		return;  			}
	x11: {
		if (__context->Stsreg01b[2]) {
			__context->Stsreg01 = (__context->Stsreg01 | 1024);
			goto l22;
		}
		if ((! __context->Stsreg01b[2])) {
			__context->Stsreg01 = (__context->Stsreg01 & 64511);
			goto l22;
		}
		//assert(false);
		return;  			}
	x12: {
		if (__context->Stsreg01b[3]) {
			__context->Stsreg01 = (__context->Stsreg01 | 2048);
			goto l23;
		}
		if ((! __context->Stsreg01b[3])) {
			__context->Stsreg01 = (__context->Stsreg01 & 63487);
			goto l23;
		}
		//assert(false);
		return;  			}
	x13: {
		if (__context->Stsreg01b[4]) {
			__context->Stsreg01 = (__context->Stsreg01 | 4096);
			goto l24;
		}
		if ((! __context->Stsreg01b[4])) {
			__context->Stsreg01 = (__context->Stsreg01 & 61439);
			goto l24;
		}
		//assert(false);
		return;  			}
	x14: {
		if (__context->Stsreg01b[5]) {
			__context->Stsreg01 = (__context->Stsreg01 | 8192);
			goto l25;
		}
		if ((! __context->Stsreg01b[5])) {
			__context->Stsreg01 = (__context->Stsreg01 & 57343);
			goto l25;
		}
		//assert(false);
		return;  			}
	x15: {
		if (__context->Stsreg01b[6]) {
			__context->Stsreg01 = (__context->Stsreg01 | 16384);
			goto l26;
		}
		if ((! __context->Stsreg01b[6])) {
			__context->Stsreg01 = (__context->Stsreg01 & 49151);
			goto l26;
		}
		//assert(false);
		return;  			}
	x16: {
		if (__context->Stsreg01b[7]) {
			__context->Stsreg01 = (__context->Stsreg01 | 32768);
			goto l27;
		}
		if ((! __context->Stsreg01b[7])) {
			__context->Stsreg01 = (__context->Stsreg01 & 32767);
			goto l27;
		}
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void R_EDGE(__R_EDGE *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
		if (((__context->new == true) && (__context->old == false))) {
			__context->RET_VAL = true;
			__context->old = true;
			goto l7;
		}
		if ((! ((__context->new == true) && (__context->old == false)))) {
			__context->RET_VAL = false;
			__context->old = __context->new;
			goto l7;
		}
		//assert(false);
		return;  			}
	l7: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init2;
	init2: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			instance.HFOff = nondet_bool();
			instance.HFOn = nondet_bool();
			instance.IOError = nondet_bool();
			instance.IOSimu = nondet_bool();
			instance.Manreg01 = nondet_uint16_t();
			instance.Manreg01b[0] = nondet_bool();
			instance.Manreg01b[10] = nondet_bool();
			instance.Manreg01b[11] = nondet_bool();
			instance.Manreg01b[12] = nondet_bool();
			instance.Manreg01b[13] = nondet_bool();
			instance.Manreg01b[14] = nondet_bool();
			instance.Manreg01b[15] = nondet_bool();
			instance.Manreg01b[1] = nondet_bool();
			instance.Manreg01b[2] = nondet_bool();
			instance.Manreg01b[3] = nondet_bool();
			instance.Manreg01b[4] = nondet_bool();
			instance.Manreg01b[5] = nondet_bool();
			instance.Manreg01b[6] = nondet_bool();
			instance.Manreg01b[7] = nondet_bool();
			instance.Manreg01b[8] = nondet_bool();
			instance.Manreg01b[9] = nondet_bool();
			instance.PLocal.ParReg = nondet_uint16_t();
			instance.PLocalb.ParRegb[0] = nondet_bool();
			instance.PLocalb.ParRegb[10] = nondet_bool();
			instance.PLocalb.ParRegb[11] = nondet_bool();
			instance.PLocalb.ParRegb[12] = nondet_bool();
			instance.PLocalb.ParRegb[13] = nondet_bool();
			instance.PLocalb.ParRegb[14] = nondet_bool();
			instance.PLocalb.ParRegb[15] = nondet_bool();
			instance.PLocalb.ParRegb[1] = nondet_bool();
			instance.PLocalb.ParRegb[2] = nondet_bool();
			instance.PLocalb.ParRegb[3] = nondet_bool();
			instance.PLocalb.ParRegb[4] = nondet_bool();
			instance.PLocalb.ParRegb[5] = nondet_bool();
			instance.PLocalb.ParRegb[6] = nondet_bool();
			instance.PLocalb.ParRegb[7] = nondet_bool();
			instance.PLocalb.ParRegb[8] = nondet_bool();
			instance.PLocalb.ParRegb[9] = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			CPC_FB_LOCAL(&instance);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
int main() {
	// Initial values
	R_EDGE1.new = false;
	R_EDGE1.old = false;
	R_EDGE1.RET_VAL = false;
	instance.HFOn = false;
	instance.HFOff = false;
	instance.IOError = false;
	instance.IOSimu = false;
	instance.Manreg01 = 0;
	instance.Manreg01b[0] = false;
	instance.Manreg01b[1] = false;
	instance.Manreg01b[2] = false;
	instance.Manreg01b[3] = false;
	instance.Manreg01b[4] = false;
	instance.Manreg01b[5] = false;
	instance.Manreg01b[6] = false;
	instance.Manreg01b[7] = false;
	instance.Manreg01b[8] = false;
	instance.Manreg01b[9] = false;
	instance.Manreg01b[10] = false;
	instance.Manreg01b[11] = false;
	instance.Manreg01b[12] = false;
	instance.Manreg01b[13] = false;
	instance.Manreg01b[14] = false;
	instance.Manreg01b[15] = false;
	instance.PLocal.ParReg = 0;
	instance.PLocalb.ParRegb[0] = false;
	instance.PLocalb.ParRegb[1] = false;
	instance.PLocalb.ParRegb[2] = false;
	instance.PLocalb.ParRegb[3] = false;
	instance.PLocalb.ParRegb[4] = false;
	instance.PLocalb.ParRegb[5] = false;
	instance.PLocalb.ParRegb[6] = false;
	instance.PLocalb.ParRegb[7] = false;
	instance.PLocalb.ParRegb[8] = false;
	instance.PLocalb.ParRegb[9] = false;
	instance.PLocalb.ParRegb[10] = false;
	instance.PLocalb.ParRegb[11] = false;
	instance.PLocalb.ParRegb[12] = false;
	instance.PLocalb.ParRegb[13] = false;
	instance.PLocalb.ParRegb[14] = false;
	instance.PLocalb.ParRegb[15] = false;
	instance.Stsreg01 = 0;
	instance.Stsreg01b[0] = false;
	instance.Stsreg01b[1] = false;
	instance.Stsreg01b[2] = false;
	instance.Stsreg01b[3] = false;
	instance.Stsreg01b[4] = false;
	instance.Stsreg01b[5] = false;
	instance.Stsreg01b[6] = false;
	instance.Stsreg01b[7] = false;
	instance.Stsreg01b[8] = false;
	instance.Stsreg01b[9] = false;
	instance.Stsreg01b[10] = false;
	instance.Stsreg01b[11] = false;
	instance.Stsreg01b[12] = false;
	instance.Stsreg01b[13] = false;
	instance.Stsreg01b[14] = false;
	instance.Stsreg01b[15] = false;
	instance.OnSt = false;
	instance.OffSt = false;
	instance.IOErrorW = false;
	instance.IOSimuW = false;
	instance.PosAlSt = false;
	instance.PosW = false;
	instance.E_PosAlSt = false;
	instance.PosAlSt_old = false;
	instance.PHFOn = false;
	instance.PHFOff = false;
	instance.PPosAlE = false;
	instance.PAnim = false;
	__assertion_error = 0;
	
	VerificationLoop();
}
