#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Root data structure
typedef struct {
	bool PIWDef;
	bool ReadByte;
	int16_t index;
	int16_t FEType;
	int16_t InterfaceParam1;
	int16_t InterfaceParam2;
	uint16_t AuPosR;
	uint16_t PosSt;
} __CPC_DB_WS;
typedef struct {
	__CPC_DB_WS Perst;
} __CPC_FB_WS;

// Global variables
__CPC_FB_WS instance;
uint16_t __assertion_error;
bool __esbmc_boc_marker;
bool __esbmc_eoc_marker;

// Automata declarations
void CPC_FB_WS(__CPC_FB_WS *__context);
void VerificationLoop();

// Automata
void CPC_FB_WS(__CPC_FB_WS *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			__context->Perst.PosSt = __context->Perst.AuPosR;
			goto l1;
		//assert(false);
		return;  			}
	l1: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			instance.Perst.AuPosR = nondet_uint16_t();
			instance.Perst.FEType = nondet_int16_t();
			instance.Perst.InterfaceParam1 = nondet_int16_t();
			instance.Perst.InterfaceParam2 = nondet_int16_t();
			instance.Perst.PIWDef = nondet_bool();
			instance.Perst.PosSt = nondet_uint16_t();
			instance.Perst.ReadByte = nondet_bool();
			instance.Perst.index = nondet_int16_t();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__esbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__esbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			CPC_FB_WS(&instance);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		__ESBMC_assert((__assertion_error == 0), "assertion error");
		__esbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__esbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
int main() {
	// Initial values
	instance.Perst.PIWDef = false;
	instance.Perst.ReadByte = false;
	instance.Perst.index = 0;
	instance.Perst.FEType = 0;
	instance.Perst.InterfaceParam1 = 0;
	instance.Perst.InterfaceParam2 = 0;
	instance.Perst.AuPosR = 0;
	instance.Perst.PosSt = 0;
	__assertion_error = 0;
	
	VerificationLoop();
}
