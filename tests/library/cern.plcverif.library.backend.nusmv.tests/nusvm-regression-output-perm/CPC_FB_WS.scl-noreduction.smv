-- Model
MODULE main
	VAR
		loc : {init_pv, end, loop_start, prepare_BoC, l_main_call, callEnd, prepare_EoC, verificationLoop_VerificationLoop_init, verificationLoop_VerificationLoop_l1};
		instance_Perst_PIWDef : boolean;
		instance_Perst_ReadByte : boolean;
		instance_Perst_index : signed word[16];
		instance_Perst_FEType : signed word[16];
		instance_Perst_InterfaceParam1 : signed word[16];
		instance_Perst_InterfaceParam2 : signed word[16];
		instance_Perst_AuPosR : unsigned word[16];
		instance_Perst_PosSt : unsigned word[16];
		__assertion_error : unsigned word[16]; -- frozen
		EoC : boolean; -- frozen
		BoC : boolean; -- frozen
		-- Random vars for nondeterministic INTs
		__random_instance_Perst_AuPosR : unsigned word[16];
		__random_instance_Perst_FEType : signed word[16];
		__random_instance_Perst_InterfaceParam1 : signed word[16];
		__random_instance_Perst_InterfaceParam2 : signed word[16];
		__random_instance_Perst_PosSt : unsigned word[16];
		__random_instance_Perst_index : signed word[16];
	
	ASSIGN
		-- CFA structure (loc)
		init(loc) := init_pv;
		next(loc) := case
			loc = init_pv & (TRUE) : loop_start;
			loc = loop_start & (TRUE) : prepare_BoC;
			loc = prepare_BoC & (TRUE) : l_main_call;
			loc = callEnd & (TRUE) : prepare_EoC;
			loc = prepare_EoC & (TRUE) : loop_start;
			loc = loop_start & (FALSE) : end;
			loc = verificationLoop_VerificationLoop_init & (TRUE) : verificationLoop_VerificationLoop_l1;
			loc = l_main_call & (TRUE) : verificationLoop_VerificationLoop_init;
			loc = verificationLoop_VerificationLoop_l1 & (TRUE) : callEnd;
			TRUE: loc;
		esac;
		
		init(instance_Perst_PIWDef) := FALSE;
		next(instance_Perst_PIWDef) := case
			loc = loop_start & (TRUE) : {TRUE, FALSE};
			TRUE  : instance_Perst_PIWDef;
		esac;
		init(instance_Perst_ReadByte) := FALSE;
		next(instance_Perst_ReadByte) := case
			loc = loop_start & (TRUE) : {TRUE, FALSE};
			TRUE  : instance_Perst_ReadByte;
		esac;
		init(instance_Perst_index) := 0sd16_0;
		next(instance_Perst_index) := case
			loc = loop_start & (TRUE) : __random_instance_Perst_index; -- Nondeterministic
			TRUE  : instance_Perst_index;
		esac;
		init(instance_Perst_FEType) := 0sd16_0;
		next(instance_Perst_FEType) := case
			loc = loop_start & (TRUE) : __random_instance_Perst_FEType; -- Nondeterministic
			TRUE  : instance_Perst_FEType;
		esac;
		init(instance_Perst_InterfaceParam1) := 0sd16_0;
		next(instance_Perst_InterfaceParam1) := case
			loc = loop_start & (TRUE) : __random_instance_Perst_InterfaceParam1; -- Nondeterministic
			TRUE  : instance_Perst_InterfaceParam1;
		esac;
		init(instance_Perst_InterfaceParam2) := 0sd16_0;
		next(instance_Perst_InterfaceParam2) := case
			loc = loop_start & (TRUE) : __random_instance_Perst_InterfaceParam2; -- Nondeterministic
			TRUE  : instance_Perst_InterfaceParam2;
		esac;
		init(instance_Perst_AuPosR) := 0ud16_0;
		next(instance_Perst_AuPosR) := case
			loc = loop_start & (TRUE) : __random_instance_Perst_AuPosR; -- Nondeterministic
			TRUE  : instance_Perst_AuPosR;
		esac;
		init(instance_Perst_PosSt) := 0ud16_0;
		next(instance_Perst_PosSt) := case
			loc = loop_start & (TRUE) : __random_instance_Perst_PosSt; -- Nondeterministic
			loc = verificationLoop_VerificationLoop_init & (TRUE) : instance_Perst_AuPosR;
			TRUE  : instance_Perst_PosSt;
		esac;
		init(__assertion_error) := 0ud16_0;
		next(__assertion_error) := case
			TRUE  : __assertion_error;
		esac;
		init(EoC) := FALSE;
		next(EoC) := case
			loc = callEnd & (TRUE) : TRUE;
			loc = prepare_EoC & (TRUE) : FALSE;
			TRUE  : EoC;
		esac;
		init(BoC) := FALSE;
		next(BoC) := case
			loc = loop_start & (TRUE) : TRUE;
			loc = prepare_BoC & (TRUE) : FALSE;
			TRUE  : BoC;
		esac;

-- Requirement
CTLSPEC AG((EoC) -> ((__assertion_error) = (0ud16_0)));
