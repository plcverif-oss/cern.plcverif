-- Model
MODULE main
	VAR
		loc : {init_pv, end, loop_start, prepare_BoC, l_main_call, callEnd, prepare_EoC, verificationLoop_VerificationLoop_init, verificationLoop_VerificationLoop_l1, verificationLoop_VerificationLoop_l2, verificationLoop_VerificationLoop_l3, verificationLoop_VerificationLoop_l4, verificationLoop_VerificationLoop_l5, verificationLoop_VerificationLoop_l6, verificationLoop_VerificationLoop_l7, verificationLoop_VerificationLoop_l8, verificationLoop_VerificationLoop_l9, verificationLoop_VerificationLoop_l10, verificationLoop_VerificationLoop_l11, verificationLoop_VerificationLoop_l12, verificationLoop_VerificationLoop_l13};
		typeconv_db1_in_int : signed word[16];
		typeconv_db1_in_dint : signed word[32];
		typeconv_db1_in_real : signed word[64];
		typeconv_db1_out_int : signed word[16];
		typeconv_db1_out_dint : signed word[32];
		typeconv_db1_out_real : signed word[64];
		__assertion_error : unsigned word[16]; -- frozen
		EoC : boolean; -- frozen
		BoC : boolean; -- frozen
		-- Random vars for nondeterministic INTs
		__random_typeconv_db1_in_dint : signed word[32];
		__random_typeconv_db1_in_int : signed word[16];
		__random_typeconv_db1_in_real : signed word[64];
	
	ASSIGN
		-- CFA structure (loc)
		init(loc) := init_pv;
		next(loc) := case
			loc = init_pv & (TRUE) : loop_start;
			loc = loop_start & (TRUE) : prepare_BoC;
			loc = prepare_BoC & (TRUE) : l_main_call;
			loc = callEnd & (TRUE) : prepare_EoC;
			loc = prepare_EoC & (TRUE) : loop_start;
			loc = loop_start & (FALSE) : end;
			loc = verificationLoop_VerificationLoop_init & (TRUE) : verificationLoop_VerificationLoop_l1;
			loc = verificationLoop_VerificationLoop_l1 & (TRUE) : verificationLoop_VerificationLoop_l2;
			loc = verificationLoop_VerificationLoop_l2 & (TRUE) : verificationLoop_VerificationLoop_l3;
			loc = verificationLoop_VerificationLoop_l3 & (TRUE) : verificationLoop_VerificationLoop_l4;
			loc = verificationLoop_VerificationLoop_l4 & (TRUE) : verificationLoop_VerificationLoop_l5;
			loc = verificationLoop_VerificationLoop_l5 & (TRUE) : verificationLoop_VerificationLoop_l6;
			loc = verificationLoop_VerificationLoop_l6 & (TRUE) : verificationLoop_VerificationLoop_l7;
			loc = verificationLoop_VerificationLoop_l7 & (TRUE) : verificationLoop_VerificationLoop_l8;
			loc = verificationLoop_VerificationLoop_l8 & (TRUE) : verificationLoop_VerificationLoop_l9;
			loc = verificationLoop_VerificationLoop_l9 & (TRUE) : verificationLoop_VerificationLoop_l10;
			loc = verificationLoop_VerificationLoop_l10 & (TRUE) : verificationLoop_VerificationLoop_l11;
			loc = verificationLoop_VerificationLoop_l11 & (TRUE) : verificationLoop_VerificationLoop_l12;
			loc = verificationLoop_VerificationLoop_l12 & (TRUE) : verificationLoop_VerificationLoop_l13;
			loc = l_main_call & (TRUE) : verificationLoop_VerificationLoop_init;
			loc = verificationLoop_VerificationLoop_l13 & (TRUE) : callEnd;
			TRUE: loc;
		esac;
		
		init(typeconv_db1_in_int) := 0sd16_0;
		next(typeconv_db1_in_int) := case
			loc = loop_start & (TRUE) : __random_typeconv_db1_in_int; -- Nondeterministic
			TRUE  : typeconv_db1_in_int;
		esac;
		init(typeconv_db1_in_dint) := 0sd32_0;
		next(typeconv_db1_in_dint) := case
			loc = loop_start & (TRUE) : __random_typeconv_db1_in_dint; -- Nondeterministic
			TRUE  : typeconv_db1_in_dint;
		esac;
		init(typeconv_db1_in_real) := 0sd64_0;
		next(typeconv_db1_in_real) := case
			loc = loop_start & (TRUE) : __random_typeconv_db1_in_real; -- Nondeterministic
			TRUE  : typeconv_db1_in_real;
		esac;
		init(typeconv_db1_out_int) := 0sd16_0;
		next(typeconv_db1_out_int) := case
			loc = verificationLoop_VerificationLoop_init & (TRUE) : typeconv_db1_in_int;
			loc = verificationLoop_VerificationLoop_l5 & (TRUE) : signed(resize(unsigned(typeconv_db1_in_dint), 16));
			loc = verificationLoop_VerificationLoop_l6 & (TRUE) : signed(resize(unsigned((typeconv_db1_in_real) / (0sd64_1000)), 16));
			TRUE  : typeconv_db1_out_int;
		esac;
		init(typeconv_db1_out_dint) := 0sd32_0;
		next(typeconv_db1_out_dint) := case
			loc = verificationLoop_VerificationLoop_l1 & (TRUE) : typeconv_db1_in_dint;
			loc = verificationLoop_VerificationLoop_l4 & (TRUE) : resize((typeconv_db1_in_int), 32);
			loc = verificationLoop_VerificationLoop_l7 & (TRUE) : signed(resize(unsigned((typeconv_db1_in_real) / (0sd64_1000)), 32));
			loc = verificationLoop_VerificationLoop_l8 & (TRUE) : 0sd32_56;
			loc = verificationLoop_VerificationLoop_l9 & (TRUE) : 0sd32_567;
			loc = verificationLoop_VerificationLoop_l10 & (TRUE) : 0sd32_567;
			loc = verificationLoop_VerificationLoop_l11 & (TRUE) : 0sd32_567;
			loc = verificationLoop_VerificationLoop_l12 & (TRUE) : (resize((typeconv_db1_in_int), 32)) * (0sd32_10);
			TRUE  : typeconv_db1_out_dint;
		esac;
		init(typeconv_db1_out_real) := 0sd64_0;
		next(typeconv_db1_out_real) := case
			loc = verificationLoop_VerificationLoop_l2 & (TRUE) : typeconv_db1_in_real;
			loc = verificationLoop_VerificationLoop_l3 & (TRUE) : (resize((typeconv_db1_in_int), 64)) * (0sd64_1000);
			TRUE  : typeconv_db1_out_real;
		esac;
		init(__assertion_error) := 0ud16_0;
		next(__assertion_error) := case
			TRUE  : __assertion_error;
		esac;
		init(EoC) := FALSE;
		next(EoC) := case
			loc = callEnd & (TRUE) : TRUE;
			loc = prepare_EoC & (TRUE) : FALSE;
			TRUE  : EoC;
		esac;
		init(BoC) := FALSE;
		next(BoC) := case
			loc = loop_start & (TRUE) : TRUE;
			loc = prepare_BoC & (TRUE) : FALSE;
			TRUE  : BoC;
		esac;

-- Requirement
CTLSPEC AG((EoC) -> ((__assertion_error) = (0ud16_0)));
