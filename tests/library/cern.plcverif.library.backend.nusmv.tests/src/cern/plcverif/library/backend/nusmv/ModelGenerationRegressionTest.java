/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.nusmv;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.cfa.transformation.ElseEliminator;
import cern.plcverif.base.models.cfa.validation.CfaValidation;
import cern.plcverif.library.backend.nusmv.model.NusmvModelBuilder;
import cern.plcverif.library.backend.nusmv.model.RemoveFloatsFromCfa;
import cern.plcverif.library.requirement.assertion.AssertionRequirementExtension;
import cern.plcverif.library.testmodels.TestModel;
import cern.plcverif.library.testmodels.TestModelCollection;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.VerificationResult;
import cern.plcverif.verif.jobmock.MockVerificationJob;
import cern.plcverif.verif.jobmock.MockVerificationJob.MockVerificationJobConfig;

/**
 * Regression tests relying on the MockVerificationJob, using the whole
 * Verification job except for backend execution.
 */
public class ModelGenerationRegressionTest {
	public static final boolean CONTINUE_ON_ASSERT_VIOLATION = false; // NOPMD
	public static boolean writeActualOutputs = CONTINUE_ON_ASSERT_VIOLATION;
	public static final String FILE_OUTPUT_DIR = "nusvm-regression-output-perm";

	public static Path outputDir;

	@BeforeClass
	public static void setup() throws IOException {
		outputDir = Files.createTempDirectory("NuSMV-regression");
	}

	@Test
	public void modelGenerationRegressionTests() throws IOException, SettingsParserException {
		for (TestModel testModel : TestModelCollection.getInstance().getAllModels()) {
			modelGenerationRegressionTest(testModel, String.format("/regression-tests/%s.smv", testModel.getModelId()),
					true);
			modelGenerationRegressionTest(testModel,
					String.format("/regression-tests/%s-noreduction.smv", testModel.getModelId()), false);
		}
	}

	private void modelGenerationRegressionTest(TestModel testModel, String expectedSmvPath, boolean reductions)
			throws IOException, SettingsParserException {
		String modelId = reductions ? testModel.getModelId() : testModel.getModelId() + "-noreduction";
		System.out.println(
				String.format("=== %s (NuSMV model generation regression test with verification loop) ===", modelId));

		// Generate the CFI using the VerificationJob workflow
		MockVerificationJobConfig config = reductions ? new MockVerificationJobConfig()
				: MockVerificationJobConfig.noReduction();
		config.setBackendExtension(new NusmvBackendExtension());
		config.setRequirementExtension(new AssertionRequirementExtension());
		config.setReporterExtension(null);
		
		CfaNetworkDeclaration cfd = testModel.getCfd();
		CfaValidation.validate(cfd);
		VerificationResult result = MockVerificationJob.run(cfd, modelId, outputDir, config, false);

		VerificationProblem verifProblem = result.getVerifProblem().get();
		CfaNetworkInstance cfi = (CfaNetworkInstance) verifProblem.getModel();

		// The NuSMV-specific CFI transformations need to be done here as this
		// part is skipped from the mock job
		new ElseEliminator().transformCfi(cfi);
		new RemoveFloatsFromCfa().transformCfi(cfi);

		CharSequence actualSmvModel = new NusmvModelBuilder(verifProblem, result, null).represent(cfi,
				verifProblem.getRequirement());
		Assert.assertNotNull(actualSmvModel);

				
		if (writeActualOutputs) {
			File directory = new File(FILE_OUTPUT_DIR);
		    if (! directory.exists()){
		        directory.mkdir();
		    }
			
			File modelFile = new File(directory, String.format("%s.smv", modelId));
			IoUtils.writeAllContent(modelFile, actualSmvModel);
		}

		// ASSERT
		// Load expected
		String expectedSmvModel = IoUtils.readResourceFile(expectedSmvPath, this.getClass().getClassLoader());

		if (!CONTINUE_ON_ASSERT_VIOLATION) {
			Assert.assertEquals(expectedSmvModel.trim(), actualSmvModel.toString().trim());
		}
	}
	
	@Test
	public void sentinel() {
		Assert.assertFalse(CONTINUE_ON_ASSERT_VIOLATION);
	}
}
