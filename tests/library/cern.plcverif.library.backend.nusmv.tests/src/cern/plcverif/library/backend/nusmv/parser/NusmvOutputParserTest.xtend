/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.library.backend.nusmv.parser

import cern.plcverif.base.common.utils.IoUtils
import cern.plcverif.base.models.cfa.CfaInstanceSafeFactory
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance
import cern.plcverif.base.models.cfa.cfainstance.Variable
import cern.plcverif.library.backend.nusmv.NusmvContext
import cern.plcverif.library.backend.nusmv.exception.NusmvParsingException
import cern.plcverif.library.backend.nusmv.model.NusmvVariableTrace
import cern.plcverif.verif.interfaces.data.ResultEnum
import cern.plcverif.verif.interfaces.data.VerificationProblem
import cern.plcverif.verif.interfaces.data.VerificationResult
import java.io.File
import java.util.Arrays
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.util.EcoreUtil
import org.junit.Assert
import org.junit.Test
import cern.plcverif.library.backend.nusmv.NusmvSettings

class NusmvOutputParserTest {
	static final CfaInstanceSafeFactory FACTORY = CfaInstanceSafeFactory.INSTANCE;
	CfaNetworkInstance cfaNetwork = null;

	@Test
	def void invalidCexFileTest1() {
		val VerificationResult result = parse("Hello");
		Assert.assertEquals(ResultEnum.Unknown, result.result);
		Assert.assertFalse(result.declarationCounterexample.isPresent);
		Assert.assertFalse(result.instanceCounterexample.isPresent);
	}

	@Test
	def void invalidCexFileTest2() {
		val VerificationResult result = parse("");
		Assert.assertEquals(ResultEnum.Unknown, result.result);
		Assert.assertFalse(result.declarationCounterexample.isPresent);
		Assert.assertFalse(result.instanceCounterexample.isPresent);
	}

	@Test
	def void invalidCexFileTest3() {
		val VerificationResult result = parse("-- specification XYZ is aaaaaa");
		Assert.assertEquals(ResultEnum.Unknown, result.result);
		Assert.assertFalse(result.declarationCounterexample.isPresent);
		Assert.assertFalse(result.instanceCounterexample.isPresent);
	}

	@Test
	def void onlyResultCexFileTest1() {
		val VerificationResult result = parse("-- specification XYZ is true");
		Assert.assertEquals(ResultEnum.Satisfied, result.result);
		Assert.assertFalse(result.declarationCounterexample.isPresent);
		Assert.assertFalse(result.instanceCounterexample.isPresent);
	}

	@Test
	def void onlyResultCexFileTest2() {
		val VerificationResult result = parse("-- specification XYZ is false");
		Assert.assertEquals(ResultEnum.Violated, result.result);
		Assert.assertFalse(result.declarationCounterexample.isPresent);
		Assert.assertFalse(result.instanceCounterexample.isPresent);
	}

	@Test
	def void cexFileTest1() {
		val cexContent = '''
			-- specification is false
			-- as demonstrated by the following execution sequence
			Trace Description: CTL Counterexample 
			Trace Type: Counterexample 
			  -> State: 1.1 <-
			    loc = loc1
			    int16Var1 = 0sd16_123
			    uint16Var2 = 0ud16_123
			    boolVar3 = FALSE
			    unknownVar = FALSE
			    EoC = FALSE
		'''
		val VerificationResult result = parse(cexContent);
		Assert.assertEquals(ResultEnum.Violated, result.result);
		Assert.assertTrue(result.instanceCounterexample.isPresent);

		Assert.assertEquals(0, result.instanceCounterexample.get.stepsCount);
	}

	@Test
	def void cexFileTest2() {
		val cexContent = '''
			-- specification is false
			-- as demonstrated by the following execution sequence
			Trace Description: CTL Counterexample 
			Trace Type: Counterexample 
			  -> State: 1.1 <-
			    loc = loc1
			    int16Var1 = 0sd16_123
			    uint16Var2 = 0ud16_0
			    boolVar3 = FALSE
			    EoC = TRUE
			  -> State: 1.2 <-
			    loc = loc1
			    int16Var1 = 0sd16_234
			    uint16Var2 = 0ud16_345
			    boolVar3 = TRUE
			    EoC = TRUE
		'''
		val VerificationResult result = parse(cexContent);
		Assert.assertEquals(ResultEnum.Violated, result.result);
		Assert.assertTrue(result.instanceCounterexample.isPresent);

		Assert.assertEquals(2, result.instanceCounterexample.get.stepsCount);

		val step1 = result.instanceCounterexample.get.steps.get(0);
		val step2 = result.instanceCounterexample.get.steps.get(1);

		Assert.assertEquals(3, step1.valuations.size);
		Assert.assertEquals(3, step2.valuations.size);

		assertEmfEquals(FACTORY.createIntLiteral(123, true, 16),
			step1.getValue(
				FACTORY.createVariableRef(cfaNetwork.variables.findFirst[it|it.name == "int16Var1"] as Variable)));
		assertEmfEquals(FACTORY.createIntLiteral(0, false, 16),
			step1.getValue(
				FACTORY.createVariableRef(cfaNetwork.variables.findFirst[it|it.name == "uint16Var2"] as Variable)));
		assertEmfEquals(FACTORY.createBoolLiteral(false),
			step1.getValue(
				FACTORY.createVariableRef(cfaNetwork.variables.findFirst[it|it.name == "boolVar3"] as Variable)));

		assertEmfEquals(FACTORY.createIntLiteral(234, true, 16),
			step2.getValue(
				FACTORY.createVariableRef(cfaNetwork.variables.findFirst[it|it.name == "int16Var1"] as Variable)));
		assertEmfEquals(FACTORY.createIntLiteral(345, false, 16),
			step2.getValue(
				FACTORY.createVariableRef(cfaNetwork.variables.findFirst[it|it.name == "uint16Var2"] as Variable)));
		assertEmfEquals(FACTORY.createBoolLiteral(true),
			step2.getValue(
				FACTORY.createVariableRef(cfaNetwork.variables.findFirst[it|it.name == "boolVar3"] as Variable)));
	}

	@Test(expected=typeof(NusmvParsingException))
	def void unknownVarExceptionTest() {
		val cexContent = '''
			"-- specification is false"
			-- as demonstrated by the following execution sequence
			Trace Description: CTL Counterexample 
			Trace Type: Counterexample 
			  -> State: 1.1 <-
			    loc = loc1
			    int16Var1 = 0sd16_123
			    uint16Var2 = 0ud16_0
			    unknownVar = FALSE
			    boolVar3 = FALSE
			    EoC = TRUE
		'''
		val VerificationResult result = parse(cexContent);
		Assert.assertNotNull(result.instanceCounterexample);
	}

	private def createTestContext() {
		val NusmvContext context = NusmvContext.create(new VerificationProblem(), new VerificationResult(),
			File.createTempFile("model", ".test"), new NusmvSettings());
		context.startStage("test");

		val NusmvVariableTrace varTrace = new NusmvVariableTrace();
		context.varTrace = varTrace;

		cfaNetwork = FACTORY.createCfaNetworkInstance("network");
		val var1 = FACTORY.createVariable(Arrays.asList("int16Var1"), cfaNetwork, FACTORY.createIntType(true, 16));
		val var2 = FACTORY.createVariable(Arrays.asList("uint16Var2"), cfaNetwork, FACTORY.createIntType(false, 16));
		val var3 = FACTORY.createVariable(Arrays.asList("boolVar3"), cfaNetwork, FACTORY.createBoolType);
		val var4 = FACTORY.createVariable(Arrays.asList("EoC"), cfaNetwork, FACTORY.createBoolType);

		varTrace.toName(var1);
		varTrace.toName(var2);
		varTrace.toName(var3);
		varTrace.toName(var4);

		return context;
	}

	private def VerificationResult parse(CharSequence content) {
		val NusmvContext context = createTestContext();
		parse(content, context);
		return context.verifResult;
	}

	private def parse(CharSequence content, NusmvContext context) {
		val cexFile = context.cexFile;
		IoUtils.writeAllContent(cexFile, content);

		NusmvOutputParser.parseCounterexample(context);
	}

	private def assertEmfEquals(EObject e1, EObject e2) {
		Assert.assertTrue(String.format("%s does not equal to %s", e1, e2), EcoreUtil.equals(e1, e2));
	}

}
