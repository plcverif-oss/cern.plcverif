/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.nusmv.model;

import java.io.IOException;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.common.emf.textual.EmfModelPrinter;
import cern.plcverif.base.common.emf.textual.SimpleEObjectToText;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.expr.FloatType;
import cern.plcverif.library.testmodels.TestModel;
import cern.plcverif.library.testmodels.TestModelCollection;

public class RemoveFloatsTest {
	@Test
	public void regressionTest() {
		regressionTest(TestModelCollection.getInstance().getModel("FloatType.scl"));
	}

	@Test
	public void basicTest() {
		// They only check whether there are remaining FloatType objects in the
		// CFA.
		basicTest(TestModelCollection.getInstance().getModel("FloatType.scl"));
	}

	private void regressionTest(TestModel testModel) {
		String expectedPath = String.format("/removefloats/%s.expected.txt", testModel.getModelId());

		System.out.println(String.format("=== %s ===", testModel.getModelId()));
		try {
			// Load and get instance model
			CfaNetworkInstance cfi = testModel.getCfi(true);

			RemoveFloatsFromCfa.execute(cfi, 100);

			String expectedContent = IoUtils.readResourceFile(expectedPath, this.getClass().getClassLoader());

			Assert.assertEquals("The generated CFI does not match the expected.", expectedContent.trim(),
					EmfModelPrinter.print(cfi, SimpleEObjectToText.INSTANCE).toString().trim());
		} catch (IOException e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	private void basicTest(TestModel testModel) {
		System.out.println(String.format("=== %s ===", testModel.getModelId()));

		try {
			// Load and get instance model
			CfaNetworkInstance cfi = testModel.getCfi(true);

			// Remove floats
			RemoveFloatsFromCfa.execute(cfi, 100);

			// Check if there are remaining float types
			Collection<FloatType> remainingFloatTypes = EmfHelper.getAllContentsOfType(cfi, FloatType.class, false);
			Assert.assertEquals("There are remaining float types.", 0, remainingFloatTypes.size());
		} catch (IOException e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

}
