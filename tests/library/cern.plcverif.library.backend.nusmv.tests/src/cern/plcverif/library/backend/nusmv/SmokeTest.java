/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Vince Molnar - further improvements and additions
 *******************************************************************************/
package cern.plcverif.library.backend.nusmv;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.cfa.transformation.ElseEliminator;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.base.models.expr.ExpressionSafeFactory;
import cern.plcverif.base.models.expr.UnaryCtlOperator;
import cern.plcverif.library.backend.nusmv.model.NusmvModelBuilder;
import cern.plcverif.library.backend.nusmv.model.RemoveFloatsFromCfa;
import cern.plcverif.library.testmodels.TestModel;
import cern.plcverif.library.testmodels.TestModelCollection;

public class SmokeTest {
	@Test
	public void batchSmokeTest() {
		for (TestModel testModel : TestModelCollection.getInstance().getAllModels()) {
			smokeTest(testModel);
		}
	}
	
	public void smokeTest(TestModel testModel) {
		System.out.println(String.format("=== %s (NuSMV backend smoke test) ===", testModel.getModelId()));
		
		try {
			// Load and get instance model; perform needed transformations
			CfaNetworkInstance cfi = testModel.getInlinedCfi(true, 
				new ElseEliminator(),
				new RemoveFloatsFromCfa()
					);

			// No reductions!
			
			// dummy requirement
			Expression req = ExpressionSafeFactory.INSTANCE.createUnaryCtlExpression(
					ExpressionSafeFactory.INSTANCE.trueLiteral()
					, UnaryCtlOperator.AG);
			
			// Generate Nusmv representation
			CharSequence result = new NusmvModelBuilder(null, null, null).represent(cfi, req);
			Assert.assertNotNull(result);
		} catch (IOException e) {
			e.printStackTrace();
			Assert.fail();
		}
	}
}
