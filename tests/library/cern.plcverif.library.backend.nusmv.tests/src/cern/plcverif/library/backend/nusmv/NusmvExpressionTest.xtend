package cern.plcverif.library.backend.nusmv

import cern.plcverif.base.models.cfa.CfaInstanceSafeFactory
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance
import cern.plcverif.base.models.cfa.cfainstance.Variable
import cern.plcverif.base.models.expr.BinaryCtlOperator
import cern.plcverif.base.models.expr.ComparisonOperator
import cern.plcverif.base.models.expr.Expression
import cern.plcverif.base.models.expr.UnaryCtlOperator
import cern.plcverif.library.backend.nusmv.exception.NusmvModelGenerationException
import cern.plcverif.library.backend.nusmv.model.NusmvExpression
import java.util.Arrays
import org.eclipse.emf.ecore.util.EcoreUtil
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class NusmvExpressionTest {
	@Extension
	static CfaInstanceSafeFactory factory = CfaInstanceSafeFactory.INSTANCE;

	NusmvExpression nusmvExprTranslator = new NusmvExpression();

	CfaNetworkInstance cfaNetwork;
	Variable var1;
	String var1Name;
	Variable var2;
	String var2Name;

	@Before
	def void createVariables() {
		cfaNetwork = createCfaNetworkInstance("cfa");

		var1 = createVariable(Arrays.asList("instance", "var1"), cfaNetwork, createIntType(true, 16));
		var1Name = nusmvExprTranslator.varTrace.toName(var1);

		var2 = createVariable(Arrays.asList("instance", "var2"), cfaNetwork, createBoolType);
		var2Name = nusmvExprTranslator.varTrace.toName(var2);
	}

	@Test
	def void toNusmvExpressionTest1() {
		toNusmvExpressionTest("TRUE", createBoolLiteral(true));
		toNusmvExpressionTest("FALSE", createBoolLiteral(false));
		toNusmvExpressionTest("0sd8_123", createIntLiteral(123, true, 8));
		toNusmvExpressionTest("-0sd8_123", createIntLiteral(-123, true, 8));
		toNusmvExpressionTest(var1Name, createVariableRef(var1));
		toNusmvExpressionTest('''(«var1Name») + (0sd16_2)''', plus(createVariableRef(var1), createIntLiteral(2, true, 16)));
		toNusmvExpressionTest('''(«var1Name») - (0sd16_2)''',
			minus(createVariableRef(var1), createIntLiteral(2, true, 16)));
		toNusmvExpressionTest('''(0sd16_5) * (0sd16_2)''',
			multiply(createIntLiteral(5, true, 16), createIntLiteral(2, true, 16)));
		toNusmvExpressionTest('''(0sd16_5) / (0sd16_2)''',
			divide(createIntLiteral(5, true, 16), createIntLiteral(2, true, 16)));
		toNusmvExpressionTest('''(«var1Name») + (resize((0sd8_2), 16))''',
			plus(createVariableRef(var1), createTypeConversion(createIntLiteral(2, true, 8), EcoreUtil.copy(var1.type))));
			
		toNusmvExpressionTest('signed(resize(unsigned(0sd16_2),8))',
			createTypeConversion(createIntLiteral(2, true, 16), createIntType(true, 8)));
		toNusmvExpressionTest('resize(unsigned(0sd16_2),8)',
			createTypeConversion(createIntLiteral(2, true, 16), createIntType(false, 8)));

		// Jira PV-63
		toNusmvExpressionTest("0ud16_32767", createIntLiteral(32767, false, 16));
		toNusmvExpressionTest("-0sd16_32767", createIntLiteral(-32767, true, 16));
		toNusmvExpressionTest("-0sd16_32768", createIntLiteral(-32768, true, 16));
		toNusmvExpressionTest('resize(signed(0ud17_32767), 16)',
			createTypeConversion(createIntLiteral(32767, false, 16), createIntType(true, 16)));
		toNusmvExpressionTest('signed(resize(unsigned(resize(signed(0ud17_32767), 32)), 16))',
			createTypeConversion(
				createTypeConversion(
					createIntLiteral(32767, createIntType(false, 16)), 
					createIntType(true, 32)
				),
				createIntType(true, 16)
			)
		);
		toNusmvExpressionTest('signed(resize(unsigned(resize(signed(0ud6_20), 32)), 16))',
			createTypeConversion(
				createTypeConversion(
					createIntLiteral(20, createIntType(false, 5)), 
					createIntType(true, 32)
				),
				createIntType(true, 16)
			)
		);
	}

	@Test(expected=NusmvModelGenerationException)
	def void toNusmvExpressionTest2() {
		nusmvExprTranslator.toNusmvExpression(null);
	}
	
	@Test
	def void toNusmvExpressionComparisonTest() {
		toNusmvComparisonOperatorTest("<", ComparisonOperator.LESS_THAN);
		toNusmvComparisonOperatorTest("<=", ComparisonOperator.LESS_EQ);
		toNusmvComparisonOperatorTest(">", ComparisonOperator.GREATER_THAN);
		toNusmvComparisonOperatorTest(">=", ComparisonOperator.GREATER_EQ);
		toNusmvComparisonOperatorTest("=", ComparisonOperator.EQUALS);
		toNusmvComparisonOperatorTest("!=", ComparisonOperator.NOT_EQUALS);
	}
	
	@Test
	def void toNusmvExpressionUnaryCtlTest() {
		toNusmvUnaryCtlOperatorTest("AG", UnaryCtlOperator.AG);
		toNusmvUnaryCtlOperatorTest("AF", UnaryCtlOperator.AF);
		toNusmvUnaryCtlOperatorTest("AX", UnaryCtlOperator.AX);
		toNusmvUnaryCtlOperatorTest("EG", UnaryCtlOperator.EG);
		toNusmvUnaryCtlOperatorTest("EF", UnaryCtlOperator.EF);
		toNusmvUnaryCtlOperatorTest("EX", UnaryCtlOperator.EX);
	}
	
	@Test
	def void toNusmvExpressionBinaryCtlTest() {
		toNusmvExpressionTest("A[(TRUE) U (FALSE)]", createBinaryCtlExpression(trueLiteral, falseLiteral, BinaryCtlOperator.AU));
		toNusmvExpressionTest("E[(TRUE) U (FALSE)]", createBinaryCtlExpression(trueLiteral, falseLiteral, BinaryCtlOperator.EU));
	}

	private def void toNusmvComparisonOperatorTest(String expectedOpStr, ComparisonOperator op) {
		toNusmvExpressionTest('''(0sd16_5) «expectedOpStr» (0sd16_2)''',
			createComparisonExpression(createIntLiteral(5, true, 16), createIntLiteral(2, true, 16), op));
	}
	
	private def void toNusmvUnaryCtlOperatorTest(String expectedOpStr, UnaryCtlOperator op) {
		toNusmvExpressionTest('''«op» (TRUE)''',
			createUnaryCtlExpression(trueLiteral, op));
	}


	/**
	 * Compares the textual representation of 'expr' with the expected value, without checking whitespace differences.
	 */
	private def void toNusmvExpressionTest(String expected, Expression expr) {
		Assert.assertEquals(expected.replaceAll("\\s", ""),
			nusmvExprTranslator.toNusmvExpression(expr).toString.replaceAll("\\s", ""));
	}
}
