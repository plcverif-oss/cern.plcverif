main process network {
	// Variables
	// ----------------------------------------
	var MX1_1 : bool
	var IX1_2 : bool
	var MX1_3 : bool
	var MX2_1 : bool
	var IX2_2 : bool
	var IX2_3 : bool
	var MX2_4 : bool
	var MX3_1 : bool
	var IX3_2 : bool
	var IX3_3 : bool
	var IX3_4 : bool
	var MX3_4 : bool
	var MX4_1 : bool
	var IX4_2 : bool
	var IX4_3 : bool
	var IX4_4 : bool
	var MX4_5 : bool
	var __assertion_error : int
	
	// Locations
	// ----------------------------------------
	init loc theta_init // Extra location for setting initial values of variables
	final loc end_pv
	error loc theta_error
	loc init_pv
	loc loop_start
	loc prepare_BoC
	loc l_main_call
	loc callEnd
	loc prepare_EoC
	loc verificationLoop_VerificationLoop_init
	loc verificationLoop_VerificationLoop_l1
	loc verificationLoop_VerificationLoop_l2
	loc verificationLoop_VerificationLoop_l3
	loc verificationLoop_VerificationLoop_l4
	loc verificationLoop_VerificationLoop_assertFunction_OB1_init
	loc verificationLoop_VerificationLoop_assertFunction_OB1_l1
	loc verificationLoop_VerificationLoop_assertFunction_OB1_l2
	loc verificationLoop_VerificationLoop_assertFunction_OB1_init1
	loc verificationLoop_VerificationLoop_assertFunction_OB1_l11
	loc verificationLoop_VerificationLoop_assertFunction_OB1_l21
	loc verificationLoop_VerificationLoop_assertFunction_OB1_init2
	loc verificationLoop_VerificationLoop_assertFunction_OB1_l12
	loc verificationLoop_VerificationLoop_assertFunction_OB1_l22
	loc verificationLoop_VerificationLoop_assertFunction_OB1_init3
	loc verificationLoop_VerificationLoop_assertFunction_OB1_l13
	loc verificationLoop_VerificationLoop_assertFunction_OB1_l23
	
	// Set the initial values of variables and go to the original initial location
	// ----------------------------------------
	theta_init -> init_pv {
		MX1_1 := false
		IX1_2 := false
		MX1_3 := false
		MX2_1 := false
		IX2_2 := false
		IX2_3 := false
		MX2_4 := false
		MX3_1 := false
		IX3_2 := false
		IX3_3 := false
		IX3_4 := false
		MX3_4 := false
		MX4_1 := false
		IX4_2 := false
		IX4_3 := false
		IX4_4 := false
		MX4_5 := false
		__assertion_error := 0
	}

	// Transitions
	// ----------------------------------------
	
	// t_params
	init_pv -> loop_start {
	}
	
	// set_BoC
	loop_start -> prepare_BoC {
		havoc IX1_2
		havoc IX2_2
		havoc IX2_3
		havoc IX3_2
		havoc IX3_3
		havoc IX3_4
		havoc IX4_2
		havoc IX4_3
		havoc IX4_4
	}
	
	// t_inputs
	prepare_BoC -> l_main_call {
	}
	
	// set_EoC
	callEnd -> prepare_EoC {
	}
	
	// restart
	prepare_EoC -> loop_start {
		assume (__assertion_error) = (0) // Requirement holds
	}
	
	// end1
	loop_start -> end_pv {
		assume false
	}
	
	// verificationLoop_VerificationLoop_assertFunction_OB1_t1
	verificationLoop_VerificationLoop_assertFunction_OB1_init -> verificationLoop_VerificationLoop_assertFunction_OB1_l1 {
		MX1_1 := true
	}
	
	// verificationLoop_VerificationLoop_assertFunction_OB1_t2
	verificationLoop_VerificationLoop_assertFunction_OB1_l1 -> verificationLoop_VerificationLoop_assertFunction_OB1_l2 {
		assume not (not (IX1_2))
		MX1_3 := true
	}
	
	// verificationLoop_VerificationLoop_assertFunction_OB1_fail
	verificationLoop_VerificationLoop_assertFunction_OB1_l1 -> verificationLoop_VerificationLoop_assertFunction_OB1_l2 {
		assume not (IX1_2)
		__assertion_error := 1
	}
	
	// verificationLoop_VerificationLoop_assertFunction_OB1_inputs
	verificationLoop_VerificationLoop_init -> verificationLoop_VerificationLoop_assertFunction_OB1_init {
	}
	
	// verificationLoop_VerificationLoop_assertFunction_OB1_outputs
	verificationLoop_VerificationLoop_assertFunction_OB1_l2 -> verificationLoop_VerificationLoop_l1 {
	}
	
	// verificationLoop_VerificationLoop_assertFunction_OB1_t11
	verificationLoop_VerificationLoop_assertFunction_OB1_init1 -> verificationLoop_VerificationLoop_assertFunction_OB1_l11 {
		MX2_1 := true
	}
	
	// verificationLoop_VerificationLoop_assertFunction_OB1_t21
	verificationLoop_VerificationLoop_assertFunction_OB1_l11 -> verificationLoop_VerificationLoop_assertFunction_OB1_l21 {
		assume not (not ((IX2_2)  or  (IX2_3)))
		MX2_4 := true
	}
	
	// verificationLoop_VerificationLoop_assertFunction_OB1_fail1
	verificationLoop_VerificationLoop_assertFunction_OB1_l11 -> verificationLoop_VerificationLoop_assertFunction_OB1_l21 {
		assume not ((IX2_2)  or  (IX2_3))
		__assertion_error := 2
	}
	
	// verificationLoop_VerificationLoop_assertFunction_OB1_inputs1
	verificationLoop_VerificationLoop_l1 -> verificationLoop_VerificationLoop_assertFunction_OB1_init1 {
	}
	
	// verificationLoop_VerificationLoop_assertFunction_OB1_outputs1
	verificationLoop_VerificationLoop_assertFunction_OB1_l21 -> verificationLoop_VerificationLoop_l2 {
	}
	
	// verificationLoop_VerificationLoop_assertFunction_OB1_t12
	verificationLoop_VerificationLoop_assertFunction_OB1_init2 -> verificationLoop_VerificationLoop_assertFunction_OB1_l12 {
		MX3_1 := true
	}
	
	// verificationLoop_VerificationLoop_assertFunction_OB1_t22
	verificationLoop_VerificationLoop_assertFunction_OB1_l12 -> verificationLoop_VerificationLoop_assertFunction_OB1_l22 {
		assume not (not (((IX3_2)  or  (IX3_3))  and  (IX3_4)))
		MX3_4 := true
	}
	
	// verificationLoop_VerificationLoop_assertFunction_OB1_fail2
	verificationLoop_VerificationLoop_assertFunction_OB1_l12 -> verificationLoop_VerificationLoop_assertFunction_OB1_l22 {
		assume not (((IX3_2)  or  (IX3_3))  and  (IX3_4))
		__assertion_error := 3
	}
	
	// verificationLoop_VerificationLoop_assertFunction_OB1_inputs2
	verificationLoop_VerificationLoop_l2 -> verificationLoop_VerificationLoop_assertFunction_OB1_init2 {
	}
	
	// verificationLoop_VerificationLoop_assertFunction_OB1_outputs2
	verificationLoop_VerificationLoop_assertFunction_OB1_l22 -> verificationLoop_VerificationLoop_l3 {
	}
	
	// verificationLoop_VerificationLoop_assertFunction_OB1_t13
	verificationLoop_VerificationLoop_assertFunction_OB1_init3 -> verificationLoop_VerificationLoop_assertFunction_OB1_l13 {
		MX4_1 := true
	}
	
	// verificationLoop_VerificationLoop_assertFunction_OB1_t23
	verificationLoop_VerificationLoop_assertFunction_OB1_l13 -> verificationLoop_VerificationLoop_assertFunction_OB1_l23 {
		assume not (not ((IX4_2)  imply  ((IX4_3)  imply  (IX4_4))))
		MX4_5 := true
	}
	
	// verificationLoop_VerificationLoop_assertFunction_OB1_fail3
	verificationLoop_VerificationLoop_assertFunction_OB1_l13 -> verificationLoop_VerificationLoop_assertFunction_OB1_l23 {
		assume not ((IX4_2)  imply  ((IX4_3)  imply  (IX4_4)))
		__assertion_error := 4
	}
	
	// verificationLoop_VerificationLoop_assertFunction_OB1_inputs3
	verificationLoop_VerificationLoop_l3 -> verificationLoop_VerificationLoop_assertFunction_OB1_init3 {
	}
	
	// verificationLoop_VerificationLoop_assertFunction_OB1_outputs3
	verificationLoop_VerificationLoop_assertFunction_OB1_l23 -> verificationLoop_VerificationLoop_l4 {
	}
	
	// verificationLoop_VerificationLoop_inputs
	l_main_call -> verificationLoop_VerificationLoop_init {
	}
	
	// verificationLoop_VerificationLoop_outputs
	verificationLoop_VerificationLoop_l4 -> callEnd {
	}
	
	// Requirement violation
	// ----------------------------------------
	prepare_EoC -> theta_error { 
		assume not ((__assertion_error) = (0))
	}
}
