/*******************************************************************************
 * (C) Copyright CERN 2017-2021. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *   Mihaly Dobos-Kovacs - integer representation, array representation support
 *******************************************************************************/
package cern.plcverif.library.backend.theta;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.collect.ImmutableSet;

import cern.plcverif.base.common.emf.EmfHelper;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.base.interfaces.data.result.JobStage;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.cfa.transformation.ElseEliminator;
import cern.plcverif.base.models.expr.BinaryArithmeticExpression;
import cern.plcverif.base.models.expr.BinaryArithmeticOperator;
import cern.plcverif.base.models.expr.FloatType;
import cern.plcverif.library.backend.theta.exception.ThetaException;
import cern.plcverif.library.backend.theta.model.ThetaCfaTransformer;
import cern.plcverif.library.backend.theta.model.TryRemoveBitwiseOps;
import cern.plcverif.library.requirement.assertion.AssertionRequirementExtension;
import cern.plcverif.library.testmodels.TestModel;
import cern.plcverif.library.testmodels.TestModel.TestModelTag;
import cern.plcverif.library.testmodels.TestModelCollection;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.VerificationResult;
import cern.plcverif.verif.jobmock.MockVerificationJob;
import cern.plcverif.verif.jobmock.MockVerificationJob.MockVerificationJobConfig;

public class RegressionTest {
	public static Path outputDir;
	
	public static final boolean CONTINUE_ON_ASSERT_VIOLATION = false; // NOPMD
	public static boolean writeActualOutputs = CONTINUE_ON_ASSERT_VIOLATION;
	
	private static String fileOutputDir;

	@BeforeClass
	public static void setup() throws IOException {
		outputDir = Files.createTempDirectory("Theta-regression");
		fileOutputDir = Files.createDirectories(Path.of("theta-regression-output")).toString();
	}

	@Test
	public void modelGenerationRegressionTests1() throws IOException, ThetaException, SettingsParserException {
		for (TestModel testModel : TestModelCollection.getInstance()
				.getModels(it -> it.hasTag(TestModelTag.SCL_TO_CFA_SUITE) || it.hasTag(TestModelTag.STL_TO_CFA_SUITE)
						|| it.hasTag(TestModelTag.UNICOS_CPC))) {
			modelGenerationRegressionTest(testModel,
					String.format("/regression-tests/%s-noreduction.theta", testModel.getModelId()), false, false);
		}
	}

	@Test
	public void modelGenerationRegressionTests2() throws IOException, ThetaException, SettingsParserException {
		for (TestModel testModel : TestModelCollection.getInstance()
				.getModels(it -> it.hasTag(TestModelTag.CONTAINS_ASSERTION))) {
			modelGenerationRegressionTest(testModel,
					String.format("/regression-tests/%s.theta", testModel.getModelId()), true, false);
		}
	}
	
	@Test
	public void modelWithArrayGenerationRegressionTest() throws IOException, ThetaException, SettingsParserException {
		final String folder = "arrays";
		final TestModelTag[] tags = {};
		List<TestModel> testModels = new ArrayList<>();
		testModels.add(new TestModel("Array1.scl", folder, tags));
		
		for (TestModel testModel : testModels) {
			modelGenerationRegressionTest(testModel,
					String.format("/regression-tests/%s.theta", testModel.getModelId()), true, true);
		}
	}

	private void modelGenerationRegressionTest(TestModel testModel, String expectedThetaPath, boolean reductions, boolean arrays)
			throws IOException, ThetaException, SettingsParserException {
		String modelId = reductions ? testModel.getModelId() : testModel.getModelId() + "-noreduction";

		System.out.println(String.format("=== %s (Theta backend regression test) ===", modelId));

		// Generate the CFI using the VerificationJob workflow
		MockVerificationJobConfig config = reductions ? new MockVerificationJobConfig()
				: MockVerificationJobConfig.noReduction();
		config.setBackendExtension(new ThetaBackendExtension());
		config.setRequirementExtension(new AssertionRequirementExtension());
		config.setReporterExtension(null);
		config.setSettings(arrays ? "-job.backend.array-representation=DYNAMIC" : "");
		VerificationResult result = MockVerificationJob.run(testModel.getCfd(), modelId, outputDir, config, false);

		VerificationProblem verifProblem = result.getVerifProblem().get();
		CfaNetworkInstance cfi = (CfaNetworkInstance) verifProblem.getModel();

		// The Theta-specific CFI transformations need to be done here as this
		// part is skipped from the mock job
		new ElseEliminator().transformCfi(cfi);
		try {
			new TryRemoveBitwiseOps(new JobStage("")).transformCfi(cfi);
		} catch (RuntimeException e) {
			System.out.println(e);
			return;
		}
		
		if (!supportedByTheta(cfi)) {
			System.out.printf("Model '%s' skipped, as it contains feature unsupported by Theta.%n", modelId);
			return;
		}

		// Generate Theta representation
		ThetaSettings settings = new ThetaSettings();
		settings.setIntegerRepresentation("UNBOUNDED");
		settings.setArrayRepresentation(arrays ? "DYNAMIC" : "ENUMERATED");
		ThetaContext context = new ThetaContext(verifProblem, result,
				File.createTempFile("theta", "theta").toPath().getParent(), settings);
		ThetaCfaTransformer.transform(context);
		CharSequence actualThetaModel = IoUtils.readFile(context.getModelFile());
		Assert.assertNotNull(actualThetaModel);

		
	
		if (writeActualOutputs) {
			File modelFile = new File( fileOutputDir, String.format("%s.theta" , modelId));
			IoUtils.writeAllContent(modelFile, actualThetaModel);
		}
	
		// ASSERT
		if (!CONTINUE_ON_ASSERT_VIOLATION) {
			// Load expected
			String expectedThetaModel = IoUtils.readResourceFile(expectedThetaPath, this.getClass().getClassLoader());
			Assert.assertEquals(expectedThetaModel.trim(), actualThetaModel.toString().trim());
		}
	}

	@Test
	public void sentinel() {
		Assert.assertFalse(CONTINUE_ON_ASSERT_VIOLATION);
	}

	private boolean supportedByTheta(CfaNetworkInstance cfi) {
		if (EmfHelper.getAllContentsOfType(cfi, FloatType.class, false).isEmpty() == false) {
			// contains FloatType
			return false;
		}

		Set<BinaryArithmeticOperator> unsupportedOperators = ImmutableSet.of(
				/* BinaryArithmeticOperator.POWER, */ BinaryArithmeticOperator.BITROTATE,
				BinaryArithmeticOperator.BITWISE_OR, BinaryArithmeticOperator.BITWISE_AND,
				BinaryArithmeticOperator.BITWISE_XOR);
		if (EmfHelper.getAllContentsOfType(cfi, BinaryArithmeticExpression.class, false,
				it -> unsupportedOperators.contains(it.getOperator())).isEmpty() == false) {
			// contains unsupported binary arithmetic operator
			return false;
		}

		return true;
	}
}
