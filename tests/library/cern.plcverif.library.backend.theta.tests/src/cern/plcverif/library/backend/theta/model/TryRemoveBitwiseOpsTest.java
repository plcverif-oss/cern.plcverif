/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.theta.model;

import static cern.plcverif.library.backend.theta.model.TryRemoveBitwiseOps.isExactlyOneOne;
import static cern.plcverif.library.backend.theta.model.TryRemoveBitwiseOps.isExactlyOneZero;
import static cern.plcverif.library.backend.theta.model.TryRemoveBitwiseOps.lowestOneBitIndex;
import static cern.plcverif.library.backend.theta.model.TryRemoveBitwiseOps.onesInBinary;
import static cern.plcverif.library.backend.theta.model.TryRemoveBitwiseOps.toBinaryString;

import org.junit.Assert;
import org.junit.Test;

import cern.plcverif.base.models.expr.ExpressionSafeFactory;
import cern.plcverif.base.models.expr.IntLiteral;

public class TryRemoveBitwiseOpsTest {
	@Test
	public void isExactlyOneOneTest() {
		Assert.assertEquals(false, isExactlyOneOne("00000011"));
		Assert.assertEquals(false, isExactlyOneOne("00000000"));
		Assert.assertEquals(true, isExactlyOneOne("00000001"));
		Assert.assertEquals(true, isExactlyOneOne("10000000"));
		Assert.assertEquals(true, isExactlyOneOne("00001000"));
	}
	
	@Test
	public void isExactlyOneZeroTest() {
		Assert.assertEquals(false, isExactlyOneZero("11111100"));
		Assert.assertEquals(false, isExactlyOneZero("11111111"));
		Assert.assertEquals(true, isExactlyOneZero("11111110"));
		Assert.assertEquals(true, isExactlyOneZero("01111111"));
		Assert.assertEquals(true, isExactlyOneZero("11110111"));
	}
	
	@Test
	public void lowestOneBitIndexTest() {
		Assert.assertEquals(-1, lowestOneBitIndex("00000000"));
		Assert.assertEquals(0, lowestOneBitIndex("00000001"));
		Assert.assertEquals(1, lowestOneBitIndex("00000010"));
		Assert.assertEquals(2, lowestOneBitIndex("00000100"));
		Assert.assertEquals(6, lowestOneBitIndex("01000000"));
		Assert.assertEquals(7, lowestOneBitIndex("10000000"));
		
		Assert.assertEquals(-1, lowestOneBitIndex("00000000"));
		Assert.assertEquals(0, lowestOneBitIndex("00010001"));
		Assert.assertEquals(1, lowestOneBitIndex("01000010"));
		Assert.assertEquals(2, lowestOneBitIndex("11111100"));
		Assert.assertEquals(6, lowestOneBitIndex("11000000"));
	}
	
	@Test
	public void onesInBinaryTest() {
		Assert.assertEquals(0, onesInBinary("00000000"));
		Assert.assertEquals(1, onesInBinary("00100000"));
		Assert.assertEquals(1, onesInBinary("00000001"));
		Assert.assertEquals(1, onesInBinary("10000000"));
		Assert.assertEquals(2, onesInBinary("10000001"));
		Assert.assertEquals(4, onesInBinary("11100001"));
		Assert.assertEquals(8, onesInBinary("11111111"));
	}
	
	@Test
	public void toBinaryStringTest1() {
		IntLiteral intLiteral5 = ExpressionSafeFactory.INSTANCE.createIntLiteral(5, false, 8);
		Assert.assertEquals("00000101", toBinaryString(intLiteral5));
	}
	
	@Test
	public void toBinaryStringTest2() {
		IntLiteral intLiteral5 = ExpressionSafeFactory.INSTANCE.createIntLiteral(5, true, 8);
		Assert.assertEquals("00000101", toBinaryString(intLiteral5));
	}
	
	@Test
	public void toBinaryStringTest3() {
		IntLiteral intLiteral5 = ExpressionSafeFactory.INSTANCE.createIntLiteral(5, false, 16);
		Assert.assertEquals("0000000000000101", toBinaryString(intLiteral5));
	}
}
