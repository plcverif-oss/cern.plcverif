package cern.plcverif.library.testing;

import org.junit.Assert;

import cern.plcverif.base.common.logging.PlcverifSeverity;
import cern.plcverif.base.interfaces.data.result.JobStage;
import cern.plcverif.base.interfaces.data.result.LogItem;
import cern.plcverif.verif.interfaces.data.VerificationResult;

/**
 * Utility class for CFA testing.
 */
public final class TestingUtil {
	private TestingUtil() {
		// Utility class.
	}

	/**
	 * Asserts that there is no log item with {@link PlcverifSeverity#Error}
	 * severity in the given verification result for any of the stages.
	 * 
	 * @param verifResult
	 *            Verification result to check.
	 */
	public static void assertNoErrorInLog(VerificationResult verifResult) {
		for (JobStage stageLog : verifResult.getAllStages()) {
			for (LogItem logItem : stageLog.getLogItems()) {
				if (logItem.severityAtLeast(PlcverifSeverity.Error)) {
					Assert.fail("There is a log item with at least Error severity: " + logItem.getMessage());
				}
			}
		}
	}
}
