/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.testmodels;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import cern.plcverif.library.testmodels.TestModel.TestModelTag;

public final class TestModelCollection {
	private List<TestModel> testModels = new ArrayList<>();
	private static TestModelCollection singletonInstance = null;

	public static TestModelCollection getInstance() {
		if (singletonInstance == null) {
			singletonInstance = new TestModelCollection();
			singletonInstance.fill();
		}
		return singletonInstance;
	}

	private TestModelCollection() {
		// Singleton.
	}

	public Collection<TestModel> getAllModels() {
		return Collections.unmodifiableList(testModels);
	}

	public Collection<TestModel> getAllModelsWithTag(TestModelTag tag) {
		return testModels.stream().filter(it -> it.hasTag(tag)).collect(Collectors.toList());
	}

	public Collection<TestModel> getModels(Predicate<TestModel> predicate) {
		return testModels.stream().filter(it -> predicate.test(it)).collect(Collectors.toList());
	}

	public TestModel getModel(String modelId) {
		List<TestModel> result = testModels.stream().filter(it -> it.getModelId().equalsIgnoreCase(modelId))
				.collect(Collectors.toList());
		if (result.isEmpty()) {
			throw new IllegalArgumentException("No model found with the following model ID: " + modelId);
		}
		if (result.size() > 1) {
			throw new AssertionError(
					"Invalid state: TestModelCollection contains more than one model with ID " + modelId);
		}

		return result.get(0);
	}

	private void fill() {
		fillSclToCfaSuite();
		fillStlToCfaSuite();
		fillUcpcSuite();
	}

	private void fillSclToCfaSuite() {
		final String folder = "/scl-to-cfa-suite/";
		final TestModelTag tags[] = { TestModelTag.SCL_FILE, TestModelTag.VALID, TestModelTag.SCL_TO_CFA_SUITE };
		//@formatter:off
		final String[] modelIds = {
				"ArrayAssignment.scl",
				"BasicCode.scl",
				"CompoundDataStructures.scl",
				"ExtractNestedCalls.scl",
				"FloatType.scl",
				"ImplicitFbInstance.scl",
				"NamedConstants.scl",
				"SclCaseStatement.scl",
				"SclExpressions.scl",
				"SclForStatement.scl",
				"SclGotoStatement.scl",
				"SclIfStatement.scl",
				"SclRepeatStatement.scl",
				"SclReturnStatement.scl",
				"SclSubroutineCall.scl",
				"SclSubroutineCallParams.scl",
				"SclWhileStatement.scl",
				"SimpleCode.scl",
				"SimpleCounterexample.scl",
				"TimeHandling.scl",
				"TypeConversion.scl",
				"Variables.scl",
				"VerificationAssertion.scl"
		};
		//@formatter:on

		for (String modelId : modelIds) {
			testModels.add(new TestModel(modelId, folder, tags));
		}
	}

	private void fillStlToCfaSuite() {
		final String folder = "/stl-to-cfa-suite/";
		final TestModelTag tags[] = { TestModelTag.STL_FILE, TestModelTag.VALID, TestModelTag.STL_TO_CFA_SUITE };
		//@formatter:off
		final String[] modelIds = {
				"StlBasicLogicStatements.scl",
				"StlComparisonStatements.scl",
				"StlJumps.scl",
				"StlNestingStack.scl",
				"StlSubroutineCall.scl",
				"StlNotOperator.scl"
		};
		//@formatter:on

		for (String modelId : modelIds) {
			testModels.add(new TestModel(modelId, folder, tags));
		}
	}

	private void fillUcpcSuite() {
		final String folder = "/unicos-cpc/";
		final TestModelTag tags[] = { TestModelTag.SCL_FILE, TestModelTag.VALID, TestModelTag.UNICOS_CPC };
		//@formatter:off
		final String[] modelIds = {
				"CPC_DIF.scl",
				"CPC_FB_AA.scl",
				"CPC_FB_AI.scl",
				"CPC_FB_AIR.scl",
				"CPC_FB_ANADIG.scl",
				"CPC_FB_ANADO.scl",
				"CPC_FB_ANALOG.scl",
				"CPC_FB_AO.scl",
				"CPC_FB_AOR.scl",
				"CPC_FB_APAR.scl",
				"CPC_FB_AS.scl",
				"CPC_FB_DA.scl",
				"CPC_FB_DI.scl",
				"CPC_FB_DO.scl",
				"CPC_FB_DPAR.scl",
				"CPC_FB_LOCAL.scl",
				"CPC_FB_ONOFF.scl",
				"CPC_FB_PID.scl",
				"CPC_FB_WPAR.scl",
				"CPC_FB_WS.scl",
				"CPC_RAMP.scl",
		};
		//@formatter:on

		for (String modelId : modelIds) {
			testModels.add(new TestModel(modelId, folder, tags));
		}
	}

}
