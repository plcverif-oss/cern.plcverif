/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.testmodels;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;

import org.eclipse.emf.ecore.util.EcoreUtil;

import cern.plcverif.base.common.emf.textual.EmfModelPrinter;
import cern.plcverif.base.common.emf.textual.SimpleEObjectToText;
import cern.plcverif.base.common.formattedstring.BasicFormattedString;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.base.interfaces.data.JobMetadata;
import cern.plcverif.base.models.cfa.cfabase.CfaNetworkBase;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.cfa.serialization.XmiToCfa;
import cern.plcverif.base.models.cfa.trace.CfaInstantiationTrace;
import cern.plcverif.base.models.cfa.transformation.CallInliner;
import cern.plcverif.base.models.cfa.transformation.CfaInstantiator;
import cern.plcverif.base.models.cfa.transformation.ICfaInstanceTransformation;
import cern.plcverif.base.models.cfa.visualization.CfaToDot;
import cern.plcverif.base.models.expr.Expression;
import cern.plcverif.verif.interfaces.data.RequirementRepresentationStrategy;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.VerificationResult;

public class TestModel {
	private static final boolean CACHING = true;

	public enum TestModelTag {
		UNICOS_CPC, CONTAINS_ASSERTION, SCL_FILE, STL_FILE, VALID, SCL_TO_CFA_SUITE, STL_TO_CFA_SUITE
	}

	private String modelId;

	private Set<TestModelTag> tags;

	/**
	 * Relative path of the CFD file inside the 'resources' folder. It should
	 * not contain the name of the file, which shall be {@code modelId}.
	 */
	private String directory;

	private CfaNetworkDeclaration cachedCfd = null;

	/**
	 * 
	 * 
	 * The CFD model is assumed to be at the following location inside the
	 * resources folder: {@code /<directory>/<modelId>.cfd}
	 * 
	 * @param modelId
	 * @param directory
	 * @param tags
	 */
	public TestModel(String modelId, String directory, TestModelTag... tags) {
		this.modelId = modelId;
		this.directory = directory;
		this.tags = EnumSet.noneOf(TestModelTag.class);
		for (TestModelTag tag : tags) {
			this.tags.add(tag);
		}
	}

	public String getModelId() {
		return modelId;
	}

	/**
	 * Returns an unmodifiable set of the model's tags.
	 */
	public Set<TestModelTag> getTags() {
		return Collections.unmodifiableSet(tags);
	}

	/**
	 * Returns true iff the model has the given tag.
	 */
	public boolean hasTag(TestModelTag tag) {
		return tags.contains(tag);
	}

	public JobMetadata getJobMetadata() {
		JobMetadata ret = new JobMetadata();
		ret.setId(this.getModelId());
		ret.setName(this.getModelId());
		ret.setDescription("N/A");
		return ret;
	}

	/**
	 * Returns a verification result ({@link VerificationResult}) object that is
	 * correctly initialized with the {@link VerificationProblem} and
	 * {@link JobMetadata} corresponding to the CFD of the current test model.
	 * 
	 * @return Initialized verification result
	 */
	public VerificationResult getVerifResultWithVerifProblem(CfaNetworkBase cfa) {
		VerificationResult result = new VerificationResult();
		result.setJobMetadata(this.getJobMetadata());
		
		VerificationProblem verifProblem = new VerificationProblem();
		verifProblem.setModel(cfa);
		result.setVerifProblem(verifProblem);
		return result;
	}

	/**
	 * Returns the CFD of the current test model.
	 * Each call returns a fresh copy of the automaton, even if caching is enabled.
	 */
	public CfaNetworkDeclaration getCfd() throws IOException {
		if (CACHING && cachedCfd != null) {
			return EcoreUtil.copy(cachedCfd);
		}

		// Load and get declaration model
		String path = String.format("/%s/%s.cfd", directory, modelId).replaceAll("//", "/");
		String cfdContent = IoUtils.readResourceFile(path, this.getClass().getClassLoader());
		CfaNetworkDeclaration cfd = XmiToCfa.deserializeFromString(cfdContent);

		if (CACHING) {
			cachedCfd = EcoreUtil.copy(cfd);
		}
		return cfd;
	}

	// public CfaNetworkInstance getCfi() throws IOException {
	// return getCfi(true);
	// }

	public CfaNetworkInstance getCfi(boolean enumerateArrays) throws IOException {
		CfaNetworkDeclaration cfd = getCfd();
		CfaInstantiationTrace traceModel = CfaInstantiator.transformCfaNetwork(cfd, enumerateArrays);
		CfaNetworkInstance cfi = traceModel.getInstance();
		return cfi;
	}

	public CfaNetworkInstance getInlinedCfi(boolean enumerateArrays, ICfaInstanceTransformation... transformations)
			throws IOException {
		CfaNetworkInstance cfi = getCfi(enumerateArrays);
		CallInliner.transform(cfi);
		for (ICfaInstanceTransformation transformation : transformations) {
			transformation.transformCfi(cfi);
		}
		return cfi;
	}

	public VerificationProblem getCfiVerifProblem(Expression requirement,
			RequirementRepresentationStrategy requirementRepresentation, boolean enumerateArrays) throws IOException {
		VerificationProblem problem = new VerificationProblem();
		problem.setModel(getCfi(enumerateArrays));
		problem.setRequirementDescription(new BasicFormattedString("Mock requirement."));
		problem.setRequirement(requirement, requirementRepresentation);
		return problem;
	}

	public static CharSequence getCfiEmfText(CfaNetworkInstance cfi) {
		return EmfModelPrinter.print(cfi, SimpleEObjectToText.INSTANCE);
	}

	public static void saveCfiEmfText(CfaNetworkInstance cfi, Path path) throws IOException {
		CharSequence content = getCfiEmfText(cfi);
		IoUtils.writeAllContent(path, content);
	}

	public static CharSequence getCfiDot(CfaNetworkInstance cfi) {
		return CfaToDot.representCfi(cfi);
	}

	public static void saveCfiDot(CfaNetworkInstance cfi, Path path) throws IOException {
		CharSequence content = getCfiDot(cfi);
		IoUtils.writeAllContent(path, content);
	}
}
