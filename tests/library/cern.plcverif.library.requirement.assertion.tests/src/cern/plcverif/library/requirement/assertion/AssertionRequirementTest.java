/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.requirement.assertion;

import java.io.IOException;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import cern.plcverif.base.common.emf.textual.EmfModelPrinter;
import cern.plcverif.base.common.emf.textual.SimpleEObjectToText;
import cern.plcverif.base.common.settings.SettingsElement;
import cern.plcverif.base.common.settings.SettingsSerializer;
import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.base.interfaces.IAstReference;
import cern.plcverif.base.interfaces.IParserLazyResult;
import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.interfaces.data.result.JobStage;
import cern.plcverif.base.interfaces.data.result.LogItem;
import cern.plcverif.base.interfaces.exceptions.AtomParsingException;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.OriginalDataTypeFieldAnnotation;
import cern.plcverif.base.models.cfa.serialization.XmiToCfa;
import cern.plcverif.base.models.cfa.textual.CfaToString;
import cern.plcverif.base.models.expr.AtomicExpression;
import cern.plcverif.library.testing.TestingUtil;
import cern.plcverif.verif.interfaces.data.RequirementRepresentationStrategy;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.VerificationResult;

public class AssertionRequirementTest {
	AssertionRequirementExtension reqExtension = new AssertionRequirementExtension();

	private CfaNetworkDeclaration cfd;
	private IParserLazyResult parserResultMock;
	private VerificationProblem verifProblem;
	private VerificationResult verifResult;

	@Before
	public void loadTestModel() throws IOException, AtomParsingException {
		this.cfd = XmiToCfa.deserializeFromString(
				IoUtils.readResourceFile("/AssertionRepresentation1.scl.cfd", this.getClass().getClassLoader()));
		// LOWPRI use mocking framework (but Mockito didn't work as Eclipse
		// dependency)
		parserResultMock = new IParserLazyResult() {

			@Override
			public String serializeAtom(AtomicExpression atom) {
				return null;
			}
			
			@Override
			public String serializeAtom(AtomicExpression atom, OriginalDataTypeFieldAnnotation hint) {
				return null;
			}

			@Override
			public AtomicExpression parseAtom(String atom) throws AtomParsingException {
				throw new AtomParsingException("The parserResultMock was not able to parse: " + atom);
			}
			
			@Override
			public Set<String> getFileNames() {
				return null;
			}

			@Override
			public IAstReference<?> getAst() {
				return null;
			}

			@Override
			public CfaNetworkDeclaration generateCfa(JobResult result) {
				return null;
			}
		};

		verifProblem = new VerificationProblem();
		verifProblem.setModel(cfd);

		verifResult = new VerificationResult();
		verifResult.switchToStage("Requirement representation");
	}

	@Test
	public void testIntErrorField_AllAssertions() throws SettingsParserException { // NOPMD
		testIntErrorField("*", 0, "AssertionRepresentation1_IntErrorField_AllAssertions.txt");
	}

	@Test
	public void testIntErrorField_Tag1Assertions() throws SettingsParserException { // NOPMD
		testIntErrorField("tag1", 5 - 2, "AssertionRepresentation1_IntErrorField_Tag1Assertions.txt");
	}

	@Test
	public void testIntErrorField_Tag1Tag2Assertions() throws SettingsParserException { // NOPMD
		testIntErrorField("{tag1,tag2}", 5 - 3, "AssertionRepresentation1_IntErrorField_Tag1Tag2Assertions.txt");
	}

	@Test
	public void testBoolErrorField_AllAssertions() throws SettingsParserException { // NOPMD
		testBoolErrorField("*", 0, "AssertionRepresentation1_BoolErrorField_AllAssertions.txt");
	}

	@Test
	public void testBoolErrorField_Tag1Assertions() throws SettingsParserException { // NOPMD
		testBoolErrorField("tag1", 5 - 2, "AssertionRepresentation1_BoolErrorField_Tag1Assertions.txt");
	}

	@Test
	public void testBoolErrorField_Tag1Tag2Assertions() throws SettingsParserException { // NOPMD
		testBoolErrorField("{tag1,tag2}", 5 - 3, "AssertionRepresentation1_BoolErrorField_Tag1Tag2Assertions.txt");
	}

	private void testIntErrorField(String assertionsToCheck, int expectedAssertRemovals, String expectedResultTextFile)
			throws SettingsParserException {
		test(AssertRepresentationStrategy.INTEGER, "(AG (EoC --> (__assertion_error = 0)))", assertionsToCheck,
				expectedAssertRemovals, expectedResultTextFile);
	}

	private void testBoolErrorField(String assertionsToCheck, int expectedAssertRemovals, String expectedResultTextFile)
			throws SettingsParserException {
		test(AssertRepresentationStrategy.BOOLEAN, "(AG (EoC --> (__assertion_error = false)))", assertionsToCheck,
				expectedAssertRemovals, expectedResultTextFile);
	}

	/**
	 * Represents the desired assertion requirement using the given strategy and
	 * checks whether it was as expected.
	 * 
	 * @param strategy
	 *            Assertion representation strategy to be used
	 * @param expectedReqString
	 *            The expected requirement expression (as serialized by
	 *            {@link CfaToString#toDiagString(org.eclipse.emf.ecore.EObject)})
	 * @param assertionsToCheck
	 *            The assertions to be checked, in the format required by the
	 *            corresponding settings.
	 * @param expectedAssertRemovals
	 *            Number of assertion removal entries expected in the log
	 * @param expectedResultTextFile
	 *            Path to expected CFA result file (serialized using
	 *            {@link EmfModelPrinter})
	 * @throws SettingsParserException
	 *             if it is not possible to parse the generated assertion
	 *             requirement settings
	 */
	private void test(AssertRepresentationStrategy strategy, String expectedReqString, String assertionsToCheck,
			int expectedAssertRemovals, String expectedResultTextFile) throws SettingsParserException {
		AssertionRequirement req = reqExtension.createRequirement(createSettings(strategy, assertionsToCheck));

		req.fillVerificationProblem(verifProblem, parserResultMock, verifResult,
				RequirementRepresentationStrategy.EXPLICIT_WITH_VARIABLE);

		Assert.assertNotNull(verifProblem);
		Assert.assertNotNull(verifProblem.getRequirement());
		Assert.assertEquals(expectedReqString, CfaToString.toDiagString(verifProblem.getRequirement()));
		Assert.assertEquals(verifProblem.getRequirementRepresentation(),
				RequirementRepresentationStrategy.EXPLICIT_WITH_VARIABLE);
		TestingUtil.assertNoErrorInLog(verifResult);
		Assert.assertEquals(expectedAssertRemovals, countRemovedCfaAssertionsInLog(verifResult));

		// Check if the result CFA is as expected
		String actualResultEmfModel = EmfModelPrinter.print(cfd, SimpleEObjectToText.INSTANCE).toString();
		String expectedResultEmfModel = IoUtils.readResourceFile(expectedResultTextFile,
				this.getClass().getClassLoader());

		// try {
		// IoUtils.writeAllContent(new
		// File("c:\\temp\\assertion-regression\\" + expectedResultTextFile
		// + ".dot"), CfaToDot.represent(cfa));
		// } catch (IOException e) { }

		Assert.assertEquals(expectedResultEmfModel.trim(), actualResultEmfModel.trim());
	}

	// Helper methods

	private SettingsElement createSettings(AssertRepresentationStrategy strategy, String assertionsToCheck)
			throws SettingsParserException {
		String settingsStr = String.format("-%s = %s%n -%s = %s",
				AssertionRequirementSettings.ASSERT_REPRESENTATION_STRATEGY, strategy.toString(),
				AssertionRequirementSettings.ASSERTION_TO_CHECK, assertionsToCheck);
		return SettingsSerializer.parseSettingsFromFile(settingsStr);
	}

	private static int countRemovedCfaAssertionsInLog(VerificationResult verifResult) {
		int cntr = 0;
		for (JobStage stageLog : verifResult.getAllStages()) {
			for (LogItem logItem : stageLog.getLogItems()) {
				if (logItem.getMessage().matches("Assertion .+ has been removed from the CFA.")) {
					cntr++;
				}
			}
		}

		return cntr;
	}
}
