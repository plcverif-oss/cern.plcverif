{CfaNetworkDeclaration name=network displayName=network mainAutomaton:VerificationLoop}
 ├──automata: 
 │  {AutomatonDeclaration name=assrepr_fb1 displayName=assrepr_fb1 initialLocation:init endLocation:end container:network}
 │   ├──locations: 
 │   │  {Location name=init displayName=init frozen=true parentAutomaton:assrepr_fb1 incoming:[] outgoing:[t1]}
 │   │   └──annotations: 
 │   │      {LineNumberAnnotation fileName=__synthetic0.SCL lineNumber=10 parentLocation:init}
 │   │  {Location name=l1 displayName=l1 frozen=true parentAutomaton:assrepr_fb1 incoming:[t1] outgoing:[fail,fail1,t2]}
 │   │   └──annotations: 
 │   │      {LineNumberAnnotation fileName=__synthetic0.SCL lineNumber=11 parentLocation:l1}
 │   │      {AssertionAnnotation name=tag1 parentLocation:l1}
 │   │       └──invariant: 
 │   │          {BinaryLogicExpression operator=IMPLIES}
 │   │           ├──leftOperand: 
 │   │           │  {FieldRef frozen=false field:out1}
 │   │           │   └──prefix: <null>
 │   │           └──rightOperand: 
 │   │              {FieldRef frozen=false field:in1}
 │   │               └──prefix: <null>
 │   │      {LineNumberAnnotation fileName=__synthetic0.SCL lineNumber=12 parentLocation:l1}
 │   │      {AssertionAnnotation name=tag1 parentLocation:l1}
 │   │       └──invariant: 
 │   │          {ComparisonExpression operator=EQUALS}
 │   │           ├──leftOperand: 
 │   │           │  {FieldRef frozen=false field:out1}
 │   │           │   └──prefix: <null>
 │   │           ├──rightOperand: 
 │   │           │  {FieldRef frozen=false field:in1}
 │   │           │   └──prefix: <null>
 │   │           └──type: 
 │   │              {BoolType}
 │   │      {LineNumberAnnotation fileName=__synthetic0.SCL lineNumber=14 parentLocation:l1}
 │   │      {IfLocationAnnotation parentLocation:l1 endsAt:end then:fail}
 │   │  {Location name=l2 displayName=l2 frozen=false parentAutomaton:assrepr_fb1 incoming:[t2] outgoing:[t3]}
 │   │   └──annotations: 
 │   │      {LineNumberAnnotation fileName=__synthetic0.SCL lineNumber=15 parentLocation:l2}
 │   │      {LineNumberAnnotation fileName=__synthetic0.SCL lineNumber=17 parentLocation:l2}
 │   │  {Location name=l3 displayName=l3 frozen=false parentAutomaton:assrepr_fb1 incoming:[t3] outgoing:[t4]}
 │   │   └──annotations: 
 │   │      {LineNumberAnnotation fileName=__synthetic0.SCL lineNumber=18 parentLocation:l3}
 │   │      {LineNumberAnnotation fileName=__synthetic0.SCL lineNumber=20 parentLocation:l3}
 │   │  {Location name=l4 displayName=l4 frozen=false parentAutomaton:assrepr_fb1 incoming:[t4] outgoing:[t5]}
 │   │   └──annotations: 
 │   │      {LineNumberAnnotation fileName=__synthetic0.SCL lineNumber=21 parentLocation:l4}
 │   │  {Location name=end displayName=end frozen=true parentAutomaton:assrepr_fb1 incoming:[t5,fail,fail1] outgoing:[]}
 │   │   └──annotations: <empty>
 │   ├──transitions: 
 │   │  {AssignmentTransition name=t1 displayName=t1 frozen=false parentAutomaton:assrepr_fb1 source:init target:l1}
 │   │   ├──condition: 
 │   │   │  {BoolLiteral value=true}
 │   │   │   └──type: 
 │   │   │      {BoolType}
 │   │   ├──annotations: <empty>
 │   │   ├──assignments: 
 │   │   │  {VariableAssignment frozen=false}
 │   │   │   ├──leftValue: 
 │   │   │   │  {FieldRef frozen=false field:out1}
 │   │   │   │   └──prefix: <null>
 │   │   │   └──rightValue: 
 │   │   │      {FieldRef frozen=false field:in1}
 │   │   │       └──prefix: <null>
 │   │   └──assumeBounds: <empty>
 │   │  {AssignmentTransition name=t2 displayName=t2 frozen=false parentAutomaton:assrepr_fb1 source:l1 target:l2}
 │   │   ├──condition: 
 │   │   │  {BinaryLogicExpression operator=AND}
 │   │   │   ├──leftOperand: 
 │   │   │   │  {UnaryLogicExpression operator=NEG}
 │   │   │   │   └──operand: 
 │   │   │   │      {UnaryLogicExpression operator=NEG}
 │   │   │   │       └──operand: 
 │   │   │   │          {BinaryLogicExpression operator=IMPLIES}
 │   │   │   │           ├──leftOperand: 
 │   │   │   │           │  {FieldRef frozen=false field:out1}
 │   │   │   │           │   └──prefix: <null>
 │   │   │   │           └──rightOperand: 
 │   │   │   │              {FieldRef frozen=false field:in1}
 │   │   │   │               └──prefix: <null>
 │   │   │   └──rightOperand: 
 │   │   │      {UnaryLogicExpression operator=NEG}
 │   │   │       └──operand: 
 │   │   │          {UnaryLogicExpression operator=NEG}
 │   │   │           └──operand: 
 │   │   │              {ComparisonExpression operator=EQUALS}
 │   │   │               ├──leftOperand: 
 │   │   │               │  {FieldRef frozen=false field:out1}
 │   │   │               │   └──prefix: <null>
 │   │   │               ├──rightOperand: 
 │   │   │               │  {FieldRef frozen=false field:in1}
 │   │   │               │   └──prefix: <null>
 │   │   │               └──type: 
 │   │   │                  {BoolType}
 │   │   ├──annotations: <empty>
 │   │   ├──assignments: 
 │   │   │  {VariableAssignment frozen=false}
 │   │   │   ├──leftValue: 
 │   │   │   │  {FieldRef frozen=false field:out2}
 │   │   │   │   └──prefix: <null>
 │   │   │   └──rightValue: 
 │   │   │      {BinaryLogicExpression operator=AND}
 │   │   │       ├──leftOperand: 
 │   │   │       │  {FieldRef frozen=false field:in1}
 │   │   │       │   └──prefix: <null>
 │   │   │       └──rightOperand: 
 │   │   │          {FieldRef frozen=false field:in2}
 │   │   │           └──prefix: <null>
 │   │   └──assumeBounds: <empty>
 │   │  {AssignmentTransition name=t3 displayName=t3 frozen=false parentAutomaton:assrepr_fb1 source:l2 target:l3}
 │   │   ├──condition: 
 │   │   │  {BoolLiteral value=true}
 │   │   │   └──type: 
 │   │   │      {BoolType}
 │   │   ├──annotations: <empty>
 │   │   ├──assignments: 
 │   │   │  {VariableAssignment frozen=false}
 │   │   │   ├──leftValue: 
 │   │   │   │  {FieldRef frozen=false field:out3}
 │   │   │   │   └──prefix: <null>
 │   │   │   └──rightValue: 
 │   │   │      {BinaryLogicExpression operator=OR}
 │   │   │       ├──leftOperand: 
 │   │   │       │  {FieldRef frozen=false field:in1}
 │   │   │       │   └──prefix: <null>
 │   │   │       └──rightOperand: 
 │   │   │          {FieldRef frozen=false field:in2}
 │   │   │           └──prefix: <null>
 │   │   └──assumeBounds: <empty>
 │   │  {AssignmentTransition name=t4 displayName=t4 frozen=false parentAutomaton:assrepr_fb1 source:l3 target:l4}
 │   │   ├──condition: 
 │   │   │  {BoolLiteral value=true}
 │   │   │   └──type: 
 │   │   │      {BoolType}
 │   │   ├──annotations: <empty>
 │   │   ├──assignments: 
 │   │   │  {VariableAssignment frozen=false}
 │   │   │   ├──leftValue: 
 │   │   │   │  {FieldRef frozen=false field:out4}
 │   │   │   │   └──prefix: <null>
 │   │   │   └──rightValue: 
 │   │   │      {BinaryLogicExpression operator=XOR}
 │   │   │       ├──leftOperand: 
 │   │   │       │  {FieldRef frozen=false field:in1}
 │   │   │       │   └──prefix: <null>
 │   │   │       └──rightOperand: 
 │   │   │          {FieldRef frozen=false field:in2}
 │   │   │           └──prefix: <null>
 │   │   └──assumeBounds: <empty>
 │   │  {AssignmentTransition name=t5 displayName=t5 frozen=false parentAutomaton:assrepr_fb1 source:l4 target:end}
 │   │   ├──condition: 
 │   │   │  {BoolLiteral value=true}
 │   │   │   └──type: 
 │   │   │      {BoolType}
 │   │   ├──annotations: <empty>
 │   │   ├──assignments: <empty>
 │   │   └──assumeBounds: <empty>
 │   │  {AssignmentTransition name=fail displayName=fail frozen=false parentAutomaton:assrepr_fb1 source:l1 target:end}
 │   │   ├──condition: 
 │   │   │  {UnaryLogicExpression operator=NEG}
 │   │   │   └──operand: 
 │   │   │      {BinaryLogicExpression operator=IMPLIES}
 │   │   │       ├──leftOperand: 
 │   │   │       │  {FieldRef frozen=false field:out1}
 │   │   │       │   └──prefix: <null>
 │   │   │       └──rightOperand: 
 │   │   │          {FieldRef frozen=false field:in1}
 │   │   │           └──prefix: <null>
 │   │   ├──annotations: <empty>
 │   │   ├──assignments: 
 │   │   │  {VariableAssignment frozen=false}
 │   │   │   ├──leftValue: 
 │   │   │   │  {FieldRef frozen=false field:__assertion_error}
 │   │   │   │   └──prefix: <null>
 │   │   │   └──rightValue: 
 │   │   │      {IntLiteral value=1}
 │   │   │       └──type: 
 │   │   │          {IntType signed=false bits=16}
 │   │   └──assumeBounds: <empty>
 │   │  {AssignmentTransition name=fail1 displayName=fail frozen=false parentAutomaton:assrepr_fb1 source:l1 target:end}
 │   │   ├──condition: 
 │   │   │  {BinaryLogicExpression operator=AND}
 │   │   │   ├──leftOperand: 
 │   │   │   │  {UnaryLogicExpression operator=NEG}
 │   │   │   │   └──operand: 
 │   │   │   │      {UnaryLogicExpression operator=NEG}
 │   │   │   │       └──operand: 
 │   │   │   │          {BinaryLogicExpression operator=IMPLIES}
 │   │   │   │           ├──leftOperand: 
 │   │   │   │           │  {FieldRef frozen=false field:out1}
 │   │   │   │           │   └──prefix: <null>
 │   │   │   │           └──rightOperand: 
 │   │   │   │              {FieldRef frozen=false field:in1}
 │   │   │   │               └──prefix: <null>
 │   │   │   └──rightOperand: 
 │   │   │      {UnaryLogicExpression operator=NEG}
 │   │   │       └──operand: 
 │   │   │          {ComparisonExpression operator=EQUALS}
 │   │   │           ├──leftOperand: 
 │   │   │           │  {FieldRef frozen=false field:out1}
 │   │   │           │   └──prefix: <null>
 │   │   │           ├──rightOperand: 
 │   │   │           │  {FieldRef frozen=false field:in1}
 │   │   │           │   └──prefix: <null>
 │   │   │           └──type: 
 │   │   │              {BoolType}
 │   │   ├──annotations: <empty>
 │   │   ├──assignments: 
 │   │   │  {VariableAssignment frozen=false}
 │   │   │   ├──leftValue: 
 │   │   │   │  {FieldRef frozen=false field:__assertion_error}
 │   │   │   │   └──prefix: <null>
 │   │   │   └──rightValue: 
 │   │   │      {IntLiteral value=2}
 │   │   │       └──type: 
 │   │   │          {IntType signed=false bits=16}
 │   │   └──assumeBounds: <empty>
 │   ├──annotations: 
 │   │  {BlockAutomatonAnnotation blockType=FunctionBlock name=assrepr_fb1 stateful=true parentAutomaton:assrepr_fb1}
 │   └──localDataStructure: 
 │      {DataStructureRef definition:assrepr_fb1}
 │  {AutomatonDeclaration name=VerificationLoop displayName=VerificationLoop initialLocation:init endLocation:end container:network}
 │   ├──locations: 
 │   │  {Location name=init displayName=init frozen=true parentAutomaton:VerificationLoop incoming:[] outgoing:[t_params]}
 │   │   └──annotations: <empty>
 │   │  {Location name=end displayName=end frozen=true parentAutomaton:VerificationLoop incoming:[end1] outgoing:[]}
 │   │   └──annotations: <empty>
 │   │  {Location name=loop_start displayName=loop_start frozen=false parentAutomaton:VerificationLoop incoming:[t_params,restart] outgoing:[set_BoC,end1]}
 │   │   └──annotations: 
 │   │      {WhileLoopLocationAnnotation parentLocation:loop_start loopBodyEntry:set_BoC loopNextCycle:restart loopExit:end1}
 │   │  {Location name=prepare_BoC displayName=prepare_BoC frozen=true parentAutomaton:VerificationLoop incoming:[set_BoC] outgoing:[t_inputs]}
 │   │   └──annotations: <empty>
 │   │  {Location name=l_main_call displayName=l_main_call frozen=false parentAutomaton:VerificationLoop incoming:[t_inputs] outgoing:[main_call]}
 │   │   └──annotations: <empty>
 │   │  {Location name=callEnd displayName=callEnd frozen=false parentAutomaton:VerificationLoop incoming:[main_call] outgoing:[set_EoC]}
 │   │   └──annotations: <empty>
 │   │  {Location name=prepare_EoC displayName=prepare_EoC frozen=true parentAutomaton:VerificationLoop incoming:[set_EoC] outgoing:[restart]}
 │   │   └──annotations: <empty>
 │   ├──transitions: 
 │   │  {AssignmentTransition name=t_params displayName=t_params frozen=false parentAutomaton:VerificationLoop source:init target:loop_start}
 │   │   ├──condition: 
 │   │   │  {BoolLiteral value=true}
 │   │   │   └──type: 
 │   │   │      {BoolType}
 │   │   ├──annotations: <empty>
 │   │   ├──assignments: <empty>
 │   │   └──assumeBounds: <empty>
 │   │  {AssignmentTransition name=set_BoC displayName=set_BoC frozen=true parentAutomaton:VerificationLoop source:loop_start target:prepare_BoC}
 │   │   ├──condition: 
 │   │   │  {BoolLiteral value=true}
 │   │   │   └──type: 
 │   │   │      {BoolType}
 │   │   ├──annotations: <empty>
 │   │   ├──assignments: 
 │   │   │  {VariableAssignment frozen=false}
 │   │   │   ├──leftValue: 
 │   │   │   │  {FieldRef frozen=false field:in1}
 │   │   │   │   └──prefix: 
 │   │   │   │      {FieldRef frozen=false field:instance}
 │   │   │   │       └──prefix: <null>
 │   │   │   └──rightValue: 
 │   │   │      {Nondeterministic}
 │   │   │       └──type: 
 │   │   │          {BoolType}
 │   │   │  {VariableAssignment frozen=false}
 │   │   │   ├──leftValue: 
 │   │   │   │  {FieldRef frozen=false field:in2}
 │   │   │   │   └──prefix: 
 │   │   │   │      {FieldRef frozen=false field:instance}
 │   │   │   │       └──prefix: <null>
 │   │   │   └──rightValue: 
 │   │   │      {Nondeterministic}
 │   │   │       └──type: 
 │   │   │          {BoolType}
 │   │   │  {VariableAssignment frozen=true}
 │   │   │   ├──leftValue: 
 │   │   │   │  {FieldRef frozen=true field:BoC}
 │   │   │   │   └──prefix: <null>
 │   │   │   └──rightValue: 
 │   │   │      {BoolLiteral value=true}
 │   │   │       └──type: 
 │   │   │          {BoolType}
 │   │   └──assumeBounds: <empty>
 │   │  {AssignmentTransition name=t_inputs displayName=t_inputs frozen=true parentAutomaton:VerificationLoop source:prepare_BoC target:l_main_call}
 │   │   ├──condition: 
 │   │   │  {BoolLiteral value=true}
 │   │   │   └──type: 
 │   │   │      {BoolType}
 │   │   ├──annotations: <empty>
 │   │   ├──assignments: 
 │   │   │  {VariableAssignment frozen=true}
 │   │   │   ├──leftValue: 
 │   │   │   │  {FieldRef frozen=true field:BoC}
 │   │   │   │   └──prefix: <null>
 │   │   │   └──rightValue: 
 │   │   │      {BoolLiteral value=false}
 │   │   │       └──type: 
 │   │   │          {BoolType}
 │   │   └──assumeBounds: <empty>
 │   │  {CallTransition name=main_call displayName=main_call frozen=false parentAutomaton:VerificationLoop source:l_main_call target:callEnd}
 │   │   ├──condition: 
 │   │   │  {BoolLiteral value=true}
 │   │   │   └──type: 
 │   │   │      {BoolType}
 │   │   ├──annotations: <empty>
 │   │   └──calls: 
 │   │      {Call parentTransition:main_call calledAutomaton:assrepr_fb1}
 │   │       ├──calleeContext: 
 │   │       │  {FieldRef frozen=false field:instance}
 │   │       │   └──prefix: <null>
 │   │       ├──inputAssignments: <empty>
 │   │       └──outputAssignments: <empty>
 │   │  {AssignmentTransition name=set_EoC displayName=set_EoC frozen=true parentAutomaton:VerificationLoop source:callEnd target:prepare_EoC}
 │   │   ├──condition: 
 │   │   │  {BoolLiteral value=true}
 │   │   │   └──type: 
 │   │   │      {BoolType}
 │   │   ├──annotations: <empty>
 │   │   ├──assignments: 
 │   │   │  {VariableAssignment frozen=true}
 │   │   │   ├──leftValue: 
 │   │   │   │  {FieldRef frozen=true field:EoC}
 │   │   │   │   └──prefix: <null>
 │   │   │   └──rightValue: 
 │   │   │      {BoolLiteral value=true}
 │   │   │       └──type: 
 │   │   │          {BoolType}
 │   │   └──assumeBounds: <empty>
 │   │  {AssignmentTransition name=restart displayName=restart frozen=true parentAutomaton:VerificationLoop source:prepare_EoC target:loop_start}
 │   │   ├──condition: 
 │   │   │  {BoolLiteral value=true}
 │   │   │   └──type: 
 │   │   │      {BoolType}
 │   │   ├──annotations: <empty>
 │   │   ├──assignments: 
 │   │   │  {VariableAssignment frozen=true}
 │   │   │   ├──leftValue: 
 │   │   │   │  {FieldRef frozen=true field:EoC}
 │   │   │   │   └──prefix: <null>
 │   │   │   └──rightValue: 
 │   │   │      {BoolLiteral value=false}
 │   │   │       └──type: 
 │   │   │          {BoolType}
 │   │   └──assumeBounds: <empty>
 │   │  {AssignmentTransition name=end1 displayName=end frozen=false parentAutomaton:VerificationLoop source:loop_start target:end}
 │   │   ├──condition: 
 │   │   │  {BoolLiteral value=false}
 │   │   │   └──type: 
 │   │   │      {BoolType}
 │   │   ├──annotations: <empty>
 │   │   ├──assignments: <empty>
 │   │   └──assumeBounds: <empty>
 │   ├──annotations: <empty>
 │   └──localDataStructure: 
 │      {DataStructureRef definition:VerificationLoopDS}
 ├──rootDataStructure: 
 │  {DataStructure name=_global displayName=_global enclosingType:<null>}
 │   ├──fields: 
 │   │  {Field name=instance displayName=instance frozen=false enclosingType:_global}
 │   │   ├──type: 
 │   │   │  {DataStructureRef definition:assrepr_fb1}
 │   │   ├──initialAssignments: <empty>
 │   │   ├──bounds: <empty>
 │   │   └──annotations: <empty>
 │   │  {Field name=__assertion_error displayName=__assertion_error frozen=true enclosingType:_global}
 │   │   ├──type: 
 │   │   │  {IntType signed=false bits=16}
 │   │   ├──initialAssignments: 
 │   │   │  {VariableAssignment frozen=false}
 │   │   │   ├──leftValue: 
 │   │   │   │  {FieldRef frozen=false field:__assertion_error}
 │   │   │   │   └──prefix: <null>
 │   │   │   └──rightValue: 
 │   │   │      {IntLiteral value=0}
 │   │   │       └──type: 
 │   │   │          {IntType signed=false bits=16}
 │   │   ├──bounds: <empty>
 │   │   └──annotations: 
 │   │      {InternalGeneratedFieldAnnotation parentField:__assertion_error}
 │   │  {Field name=verificationLoop displayName=verificationLoop frozen=false enclosingType:_global}
 │   │   ├──type: 
 │   │   │  {DataStructureRef definition:VerificationLoopDS}
 │   │   ├──initialAssignments: <empty>
 │   │   ├──bounds: <empty>
 │   │   └──annotations: <empty>
 │   │  {Field name=EoC displayName=EoC frozen=true enclosingType:_global}
 │   │   ├──type: 
 │   │   │  {BoolType}
 │   │   ├──initialAssignments: 
 │   │   │  {VariableAssignment frozen=false}
 │   │   │   ├──leftValue: 
 │   │   │   │  {FieldRef frozen=false field:EoC}
 │   │   │   │   └──prefix: <null>
 │   │   │   └──rightValue: 
 │   │   │      {BoolLiteral value=false}
 │   │   │       └──type: 
 │   │   │          {BoolType}
 │   │   ├──bounds: <empty>
 │   │   └──annotations: 
 │   │      {InternalGeneratedFieldAnnotation parentField:EoC}
 │   │  {Field name=BoC displayName=BoC frozen=true enclosingType:_global}
 │   │   ├──type: 
 │   │   │  {BoolType}
 │   │   ├──initialAssignments: 
 │   │   │  {VariableAssignment frozen=false}
 │   │   │   ├──leftValue: 
 │   │   │   │  {FieldRef frozen=false field:BoC}
 │   │   │   │   └──prefix: <null>
 │   │   │   └──rightValue: 
 │   │   │      {BoolLiteral value=false}
 │   │   │       └──type: 
 │   │   │          {BoolType}
 │   │   ├──bounds: <empty>
 │   │   └──annotations: 
 │   │      {InternalGeneratedFieldAnnotation parentField:BoC}
 │   ├──complexTypes: 
 │   │  {DataStructure name=assrepr_fb1 displayName=assrepr_fb1 enclosingType:_global}
 │   │   ├──fields: 
 │   │   │  {Field name=in1 displayName=in1 frozen=true enclosingType:assrepr_fb1}
 │   │   │   ├──type: 
 │   │   │   │  {BoolType}
 │   │   │   ├──initialAssignments: 
 │   │   │   │  {VariableAssignment frozen=false}
 │   │   │   │   ├──leftValue: 
 │   │   │   │   │  {FieldRef frozen=false field:in1}
 │   │   │   │   │   └──prefix: <null>
 │   │   │   │   └──rightValue: 
 │   │   │   │      {BoolLiteral value=false}
 │   │   │   │       └──type: 
 │   │   │   │          {BoolType}
 │   │   │   ├──bounds: <empty>
 │   │   │   └──annotations: 
 │   │   │      {DirectionFieldAnnotation direction=INPUT parentField:in1}
 │   │   │      {OriginalDataTypeFieldAnnotation plcDataType=BOOL parentField:in1}
 │   │   │  {Field name=in2 displayName=in2 frozen=false enclosingType:assrepr_fb1}
 │   │   │   ├──type: 
 │   │   │   │  {BoolType}
 │   │   │   ├──initialAssignments: 
 │   │   │   │  {VariableAssignment frozen=false}
 │   │   │   │   ├──leftValue: 
 │   │   │   │   │  {FieldRef frozen=false field:in2}
 │   │   │   │   │   └──prefix: <null>
 │   │   │   │   └──rightValue: 
 │   │   │   │      {BoolLiteral value=false}
 │   │   │   │       └──type: 
 │   │   │   │          {BoolType}
 │   │   │   ├──bounds: <empty>
 │   │   │   └──annotations: 
 │   │   │      {DirectionFieldAnnotation direction=INPUT parentField:in2}
 │   │   │      {OriginalDataTypeFieldAnnotation plcDataType=BOOL parentField:in2}
 │   │   │  {Field name=out1 displayName=out1 frozen=true enclosingType:assrepr_fb1}
 │   │   │   ├──type: 
 │   │   │   │  {BoolType}
 │   │   │   ├──initialAssignments: 
 │   │   │   │  {VariableAssignment frozen=false}
 │   │   │   │   ├──leftValue: 
 │   │   │   │   │  {FieldRef frozen=false field:out1}
 │   │   │   │   │   └──prefix: <null>
 │   │   │   │   └──rightValue: 
 │   │   │   │      {BoolLiteral value=false}
 │   │   │   │       └──type: 
 │   │   │   │          {BoolType}
 │   │   │   ├──bounds: <empty>
 │   │   │   └──annotations: 
 │   │   │      {DirectionFieldAnnotation direction=OUTPUT parentField:out1}
 │   │   │      {OriginalDataTypeFieldAnnotation plcDataType=BOOL parentField:out1}
 │   │   │  {Field name=out2 displayName=out2 frozen=false enclosingType:assrepr_fb1}
 │   │   │   ├──type: 
 │   │   │   │  {BoolType}
 │   │   │   ├──initialAssignments: 
 │   │   │   │  {VariableAssignment frozen=false}
 │   │   │   │   ├──leftValue: 
 │   │   │   │   │  {FieldRef frozen=false field:out2}
 │   │   │   │   │   └──prefix: <null>
 │   │   │   │   └──rightValue: 
 │   │   │   │      {BoolLiteral value=false}
 │   │   │   │       └──type: 
 │   │   │   │          {BoolType}
 │   │   │   ├──bounds: <empty>
 │   │   │   └──annotations: 
 │   │   │      {DirectionFieldAnnotation direction=OUTPUT parentField:out2}
 │   │   │      {OriginalDataTypeFieldAnnotation plcDataType=BOOL parentField:out2}
 │   │   │  {Field name=out3 displayName=out3 frozen=false enclosingType:assrepr_fb1}
 │   │   │   ├──type: 
 │   │   │   │  {BoolType}
 │   │   │   ├──initialAssignments: 
 │   │   │   │  {VariableAssignment frozen=false}
 │   │   │   │   ├──leftValue: 
 │   │   │   │   │  {FieldRef frozen=false field:out3}
 │   │   │   │   │   └──prefix: <null>
 │   │   │   │   └──rightValue: 
 │   │   │   │      {BoolLiteral value=false}
 │   │   │   │       └──type: 
 │   │   │   │          {BoolType}
 │   │   │   ├──bounds: <empty>
 │   │   │   └──annotations: 
 │   │   │      {DirectionFieldAnnotation direction=OUTPUT parentField:out3}
 │   │   │      {OriginalDataTypeFieldAnnotation plcDataType=BOOL parentField:out3}
 │   │   │  {Field name=out4 displayName=out4 frozen=false enclosingType:assrepr_fb1}
 │   │   │   ├──type: 
 │   │   │   │  {BoolType}
 │   │   │   ├──initialAssignments: 
 │   │   │   │  {VariableAssignment frozen=false}
 │   │   │   │   ├──leftValue: 
 │   │   │   │   │  {FieldRef frozen=false field:out4}
 │   │   │   │   │   └──prefix: <null>
 │   │   │   │   └──rightValue: 
 │   │   │   │      {BoolLiteral value=false}
 │   │   │   │       └──type: 
 │   │   │   │          {BoolType}
 │   │   │   ├──bounds: <empty>
 │   │   │   └──annotations: 
 │   │   │      {DirectionFieldAnnotation direction=OUTPUT parentField:out4}
 │   │   │      {OriginalDataTypeFieldAnnotation plcDataType=BOOL parentField:out4}
 │   │   ├──complexTypes: <empty>
 │   │   └──annotations: <empty>
 │   │  {DataStructure name=VerificationLoopDS displayName=VerificationLoopDS enclosingType:_global}
 │   │   ├──fields: <empty>
 │   │   ├──complexTypes: <empty>
 │   │   └──annotations: <empty>
 │   └──annotations: <empty>
 └──mainContext: 
    {FieldRef frozen=false field:verificationLoop}
     └──prefix: <null>