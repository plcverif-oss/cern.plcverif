package cern.plcverif.library.reporter.html.common

import java.util.List
import java.util.regex.Pattern
import org.junit.Assert
import org.junit.Test

class PathRecognitionTest {
	@Test
	def testWindowsPaths1() {
		val input = '''
			Lorem ipsum dolor c:\windows\a.txt sit amet.
		'''
		val expectedOutput = #['''c:\windows\a.txt'''];
		
		Assert.assertEquals(expectedOutput, findMatches(CommonHtmlReportDesign.FILEPATH_PATTERN_WINDOWS, input)); 
		Assert.assertEquals(#[], findMatches(CommonHtmlReportDesign.FILEPATH_PATTERN_WINDOWS_QUOTED, input)); 
	}
	
	@Test
	def testWindowsPaths2() {
		val input = '''
			Lorem ipsum dolor c:\windows\a.txt c:\windows\b.txt c:\windows\c sit amet.
		'''
		val expectedOutput = #['''c:\windows\a.txt''', '''c:\windows\b.txt''', '''c:\windows\c'''];
		
		Assert.assertEquals(expectedOutput, findMatches(CommonHtmlReportDesign.FILEPATH_PATTERN_WINDOWS, input)); 
		Assert.assertEquals(#[], findMatches(CommonHtmlReportDesign.FILEPATH_PATTERN_WINDOWS_QUOTED, input)); 
	}
	
	@Test
	def testWindowsPathsWithWhitespace1() {
		val input = '''
			Lorem ipsum dolor "c:\program files\a.txt" sit amet.
		'''
		val expectedOutput = #['''c:\program files\a.txt'''];
		
		Assert.assertEquals(#[], findMatches(CommonHtmlReportDesign.FILEPATH_PATTERN_WINDOWS, input)); 
		Assert.assertEquals(expectedOutput, findMatches(CommonHtmlReportDesign.FILEPATH_PATTERN_WINDOWS_QUOTED, input)); 
	}
	
	@Test
	def testWindowsPathsWithWhitespace2() {
		val input = '''
			Lorem ipsum dolor "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvars64.bat" sit amet.
		'''
		val expectedOutput = #['''C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvars64.bat'''];
		
		Assert.assertEquals(#[], findMatches(CommonHtmlReportDesign.FILEPATH_PATTERN_WINDOWS, input)); 
		Assert.assertEquals(expectedOutput, findMatches(CommonHtmlReportDesign.FILEPATH_PATTERN_WINDOWS_QUOTED, input)); 
	}
	
	@Test
	def testWindowsPathsWithWhitespace3() {
		val input = '''
			Lorem ipsum dolor &quot;c:\program files\a.txt&quot; sit amet.
		'''
		val expectedOutput = #['''c:\program files\a.txt'''];
		
		Assert.assertEquals(#[], findMatches(CommonHtmlReportDesign.FILEPATH_PATTERN_WINDOWS, input)); 
		Assert.assertEquals(expectedOutput, findMatches(CommonHtmlReportDesign.FILEPATH_PATTERN_WINDOWS_QUOTED, input)); 
	}
	
	@Test
	def testWindowsPathsWithWhitespace4() {
		val input = '''
			Lorem ipsum dolor &quot;C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvars64.bat&quot; sit amet.
		'''
		val expectedOutput = #['''C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvars64.bat'''];
		
		Assert.assertEquals(#[], findMatches(CommonHtmlReportDesign.FILEPATH_PATTERN_WINDOWS, input)); 
		Assert.assertEquals(expectedOutput, findMatches(CommonHtmlReportDesign.FILEPATH_PATTERN_WINDOWS_QUOTED, input)); 
	}
	
	@Test
	def testWindowsPathsWithWhitespace5() {
		val input = '''
			Lorem ipsum 
			-sourcefiles.0 = "C:\workspaces\runtime-plcverif.gui.app.product\.builtin_Siemens S7-300\builtin.scl"
			-sourcefiles.1 = C:\workspaces\runtime-plcverif.gui.app.product\demoabs2\source.scl
			sit amet.
		'''
		val expectedQuoted = #['''C:\workspaces\runtime-plcverif.gui.app.product\.builtin_Siemens S7-300\builtin.scl'''];
		val expectedNonquoted = #['''C:\workspaces\runtime-plcverif.gui.app.product\demoabs2\source.scl''']
		
		Assert.assertEquals(expectedNonquoted, findMatches(CommonHtmlReportDesign.FILEPATH_PATTERN_WINDOWS, input)); 
		Assert.assertEquals(expectedQuoted, findMatches(CommonHtmlReportDesign.FILEPATH_PATTERN_WINDOWS_QUOTED, input)); 
	}
	
	
	@Test
	def testUnixPaths1() {
		val input = '''
			Lorem ipsum dolor /tmp/dir1/file_0123.ext sit amet.
		'''
		val expectedOutput = #['''/tmp/dir1/file_0123.ext'''];
		
		Assert.assertEquals(expectedOutput, findMatches(CommonHtmlReportDesign.FILEPATH_PATTERN_LINUX, input)); 
	}
	
	@Test
	def testUnixPaths2() {
		val input = '''
			Lorem ipsum dolor /tmp/dir1/file-0123.ext sit /usr/.hidden amet.
		'''
		val expectedOutput = #['''/tmp/dir1/file-0123.ext''', '''/usr/.hidden'''];
		
		Assert.assertEquals(expectedOutput, findMatches(CommonHtmlReportDesign.FILEPATH_PATTERN_LINUX, input)); 
	}
	
	@Test
	def testHttpUrl1() {
		val input = '''
			Lorem ipsum dolor http://cern.ch/index.html amet.
		'''
		val expectedOutput = #['''http://cern.ch/index.html'''];
		
		Assert.assertEquals(expectedOutput, findMatches(CommonHtmlReportDesign.HTTP_URL_PATTERN, input)); 
	}
	
	@Test
	def testHttpUrl2() {
		val input = '''
			Lorem ipsum dolor (http://cern.ch/index.html) amet.
		'''
		val expectedOutput = #['''http://cern.ch/index.html'''];
		
		Assert.assertEquals(expectedOutput, findMatches(CommonHtmlReportDesign.HTTP_URL_PATTERN, input)); 
	}
	
	@Test
	def testHttpUrl3() {
		val input = '''
			Lorem ipsum
			dolor (http://cern.ch/index.html#a=b&c=d&e=f) sit 
			For more information see <http://nusmv.fbk.eu> 
			amet.
		'''
		val expectedOutput = #['http://cern.ch/index.html#a=b', 'http://nusmv.fbk.eu'];
		
		Assert.assertEquals(expectedOutput, findMatches(CommonHtmlReportDesign.HTTP_URL_PATTERN, input)); 
	}
	
	private def List<CharSequence> findMatches(String pattern, CharSequence text) {
		val matcher = Pattern.compile(pattern).matcher(text);
		val ret = newArrayList();
		while (matcher.find) {
			ret.add(matcher.group(1));
		}
		return ret;
	}
	
}