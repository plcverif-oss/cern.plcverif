#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Root data structure
typedef struct {
	bool boolVar1;
	bool boolVar2;
	int16_t intVar1;
	int16_t intVar2;
} __namedconst_FC1;
typedef struct {
	int16_t arr1[11];
	bool arr2[11];
	bool arr3[11];
	int16_t intVar;
} __namedconst_FC2;

// Global variables
bool MX0_0;
bool MX0_1;
bool MX0_2;
__namedconst_FC1 namedconst_FC11;
__namedconst_FC2 namedconst_FC21;
uint16_t __assertion_error;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Automata declarations
void namedconst_OB1();
void namedconst_FC1(__namedconst_FC1 *__context);
void namedconst_FC2(__namedconst_FC2 *__context);
void VerificationLoop();

// Automata
void namedconst_OB1() {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			// Assign inputs
			namedconst_FC1(&namedconst_FC11);
			// Assign outputs
			goto l1;
		//assert(false);
		return;  			}
	l1: {
			// Assign inputs
			namedconst_FC2(&namedconst_FC21);
			// Assign outputs
			goto l2;
		//assert(false);
		return;  			}
	l2: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void namedconst_FC1(__namedconst_FC1 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			__context->intVar1 = 1;
			goto l11;
		//assert(false);
		return;  			}
	l11: {
			__context->intVar1 = (1 + __context->intVar2);
			goto l21;
		//assert(false);
		return;  			}
	l21: {
			__context->intVar1 = (1 + 2);
			__context->boolVar1 = true;
			goto l4;
		//assert(false);
		return;  			}
	l4: {
			__context->boolVar1 = (true || __context->boolVar2);
			goto l5;
		//assert(false);
		return;  			}
	l5: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void namedconst_FC2(__namedconst_FC2 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init2;
	init2: {
		if ((__context->intVar == 1)) {
			MX0_0 = true;
			goto l12;
		}
		if ((! (__context->intVar == 1))) {
			goto l3;
		}
		//assert(false);
		return;  			}
	l12: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	l3: {
		if ((((__context->intVar >= 2) && (__context->intVar <= 3)) || (__context->intVar == 10))) {
			MX0_1 = true;
			goto l12;
		}
		if ((! (((__context->intVar >= 2) && (__context->intVar <= 3)) || (__context->intVar == 10)))) {
			MX0_2 = true;
			goto l12;
		}
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init3;
	init3: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			namedconst_OB1();
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
void main() {
	// Initial values
	MX0_0 = false;
	MX0_1 = false;
	MX0_2 = false;
	namedconst_FC11.boolVar1 = false;
	namedconst_FC11.boolVar2 = false;
	namedconst_FC11.intVar1 = 0;
	namedconst_FC11.intVar2 = 0;
	namedconst_FC21.arr1[1] = 5;
	namedconst_FC21.arr1[2] = 5;
	namedconst_FC21.arr1[3] = 5;
	namedconst_FC21.arr1[4] = 5;
	namedconst_FC21.arr1[5] = 5;
	namedconst_FC21.arr1[6] = 5;
	namedconst_FC21.arr1[7] = 5;
	namedconst_FC21.arr1[8] = 5;
	namedconst_FC21.arr1[9] = 5;
	namedconst_FC21.arr1[10] = 5;
	namedconst_FC21.arr2[1] = true;
	namedconst_FC21.arr2[2] = true;
	namedconst_FC21.arr2[3] = true;
	namedconst_FC21.arr2[4] = true;
	namedconst_FC21.arr2[5] = true;
	namedconst_FC21.arr2[6] = true;
	namedconst_FC21.arr2[7] = true;
	namedconst_FC21.arr2[8] = true;
	namedconst_FC21.arr2[9] = true;
	namedconst_FC21.arr2[10] = true;
	namedconst_FC21.arr3[1] = true;
	namedconst_FC21.arr3[2] = true;
	namedconst_FC21.arr3[3] = true;
	namedconst_FC21.arr3[4] = true;
	namedconst_FC21.arr3[5] = true;
	namedconst_FC21.arr3[6] = true;
	namedconst_FC21.arr3[7] = true;
	namedconst_FC21.arr3[8] = true;
	namedconst_FC21.arr3[9] = true;
	namedconst_FC21.arr3[10] = true;
	namedconst_FC21.intVar = 0;
	__assertion_error = 0;
	
	VerificationLoop();
}
