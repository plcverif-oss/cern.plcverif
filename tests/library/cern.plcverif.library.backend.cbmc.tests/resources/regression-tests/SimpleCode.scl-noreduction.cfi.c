#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Variables
bool MX123_7 = false;
int16_t function1_out1 = 0;
bool function2_in1 = false;
int16_t function2_in2 = 0;
int16_t function2_RET_VAL = 0;
uint16_t __assertion_error = 0;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			goto verificationLoop_VerificationLoop_simplecode_OB1_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			goto callEnd;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_simplecode_OB1_init: {
			function2_in1 = MX123_7;
			function2_in2 = 123;
			goto verificationLoop_VerificationLoop_simplecode_OB1_function1_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_simplecode_OB1_l1: {
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_simplecode_OB1_function1_init: {
		if (function2_in1) {
			goto verificationLoop_VerificationLoop_simplecode_OB1_function1_l1;
		}
		if ((! function2_in1)) {
			goto verificationLoop_VerificationLoop_simplecode_OB1_function1_l3;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_simplecode_OB1_function1_l1: {
			function2_RET_VAL = function2_in2;
			goto verificationLoop_VerificationLoop_simplecode_OB1_function1_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_simplecode_OB1_function1_l2: {
			goto verificationLoop_VerificationLoop_simplecode_OB1_function1_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_simplecode_OB1_function1_l3: {
			function2_RET_VAL = (- function2_in2);
			goto verificationLoop_VerificationLoop_simplecode_OB1_function1_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_simplecode_OB1_function1_l4: {
			goto verificationLoop_VerificationLoop_simplecode_OB1_function1_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_simplecode_OB1_function1_l5: {
			function1_out1 = function2_RET_VAL;
			goto verificationLoop_VerificationLoop_simplecode_OB1_l1;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
