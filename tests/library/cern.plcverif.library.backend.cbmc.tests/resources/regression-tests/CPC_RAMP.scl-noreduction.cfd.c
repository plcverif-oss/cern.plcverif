#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Root data structure
typedef struct {
	float INV;
	float InSpd;
	float DeSpd;
	float H_LM;
	float L_LM;
	float PV;
	float DF_OUTV;
	bool DFOUT_ON;
	bool TRACK;
	bool MAN_ON;
	bool COM_RST;
	int32_t CYCLE;
	float OUTV;
	bool DONE;
	float INV_sat;
} __RAMP;

// Global variables
__RAMP instance;
uint16_t __assertion_error;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Automata declarations
void RAMP(__RAMP *__context);
void VerificationLoop();

// Automata
void RAMP(__RAMP *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
		if ((__context->INV > __context->H_LM)) {
			goto l1;
		}
		if (((! (__context->INV > __context->H_LM)) && (__context->INV < __context->L_LM))) {
			goto l3;
		}
		if (((! (__context->INV > __context->H_LM)) && (! ((! (__context->INV > __context->H_LM)) && (__context->INV < __context->L_LM))))) {
			goto l5;
		}
		//assert(false);
		return;  			}
	l1: {
			__context->INV_sat = __context->H_LM;
			goto l2;
		//assert(false);
		return;  			}
	l2: {
			goto l7;
		//assert(false);
		return;  			}
	l3: {
			__context->INV_sat = __context->L_LM;
			goto l4;
		//assert(false);
		return;  			}
	l4: {
			goto l7;
		//assert(false);
		return;  			}
	l5: {
			__context->INV_sat = __context->INV;
			goto l6;
		//assert(false);
		return;  			}
	l6: {
			goto l7;
		//assert(false);
		return;  			}
	l7: {
		if (__context->COM_RST) {
			goto l8;
		}
		if (((! __context->COM_RST) && __context->TRACK)) {
			goto l11;
		}
		if ((((! __context->COM_RST) && (! __context->TRACK)) && __context->MAN_ON)) {
			goto l20;
		}
		if ((((! __context->COM_RST) && ((! __context->TRACK) && (! __context->MAN_ON))) && __context->DFOUT_ON)) {
			goto l29;
		}
		if (((! __context->COM_RST) && ((! ((! __context->COM_RST) && __context->TRACK)) && ((! (((! __context->COM_RST) && (! __context->TRACK)) && __context->MAN_ON)) && (! (((! __context->COM_RST) && ((! __context->TRACK) && (! __context->MAN_ON))) && __context->DFOUT_ON)))))) {
			goto l38;
		}
		//assert(false);
		return;  			}
	l8: {
			__context->OUTV = 0.0;
			goto l9;
		//assert(false);
		return;  			}
	l9: {
			__context->DONE = false;
			goto l10;
		//assert(false);
		return;  			}
	l10: {
			goto l53;
		//assert(false);
		return;  			}
	l11: {
		if ((__context->INV > __context->H_LM)) {
			goto l12;
		}
		if (((! (__context->INV > __context->H_LM)) && (__context->INV < __context->L_LM))) {
			goto l14;
		}
		if (((! (__context->INV > __context->H_LM)) && (! ((! (__context->INV > __context->H_LM)) && (__context->INV < __context->L_LM))))) {
			goto l16;
		}
		//assert(false);
		return;  			}
	l12: {
			__context->OUTV = __context->H_LM;
			goto l13;
		//assert(false);
		return;  			}
	l13: {
			goto l18;
		//assert(false);
		return;  			}
	l14: {
			__context->OUTV = __context->L_LM;
			goto l15;
		//assert(false);
		return;  			}
	l15: {
			goto l18;
		//assert(false);
		return;  			}
	l16: {
			__context->OUTV = __context->INV;
			goto l17;
		//assert(false);
		return;  			}
	l17: {
			goto l18;
		//assert(false);
		return;  			}
	l18: {
			__context->DONE = false;
			goto l19;
		//assert(false);
		return;  			}
	l19: {
			goto l53;
		//assert(false);
		return;  			}
	l20: {
		if ((__context->PV > __context->H_LM)) {
			goto l21;
		}
		if (((! (__context->PV > __context->H_LM)) && (__context->PV < __context->L_LM))) {
			goto l23;
		}
		if (((! (__context->PV > __context->H_LM)) && (! ((! (__context->PV > __context->H_LM)) && (__context->PV < __context->L_LM))))) {
			goto l25;
		}
		//assert(false);
		return;  			}
	l21: {
			__context->OUTV = __context->H_LM;
			goto l22;
		//assert(false);
		return;  			}
	l22: {
			goto l27;
		//assert(false);
		return;  			}
	l23: {
			__context->OUTV = __context->L_LM;
			goto l24;
		//assert(false);
		return;  			}
	l24: {
			goto l27;
		//assert(false);
		return;  			}
	l25: {
			__context->OUTV = __context->PV;
			goto l26;
		//assert(false);
		return;  			}
	l26: {
			goto l27;
		//assert(false);
		return;  			}
	l27: {
			__context->DONE = false;
			goto l28;
		//assert(false);
		return;  			}
	l28: {
			goto l53;
		//assert(false);
		return;  			}
	l29: {
		if ((__context->DF_OUTV > __context->H_LM)) {
			goto l30;
		}
		if (((! (__context->DF_OUTV > __context->H_LM)) && (__context->DF_OUTV < __context->L_LM))) {
			goto l32;
		}
		if (((! (__context->DF_OUTV > __context->H_LM)) && (! ((! (__context->DF_OUTV > __context->H_LM)) && (__context->DF_OUTV < __context->L_LM))))) {
			goto l34;
		}
		//assert(false);
		return;  			}
	l30: {
			__context->OUTV = __context->H_LM;
			goto l31;
		//assert(false);
		return;  			}
	l31: {
			goto l36;
		//assert(false);
		return;  			}
	l32: {
			__context->OUTV = __context->L_LM;
			goto l33;
		//assert(false);
		return;  			}
	l33: {
			goto l36;
		//assert(false);
		return;  			}
	l34: {
			__context->OUTV = __context->DF_OUTV;
			goto l35;
		//assert(false);
		return;  			}
	l35: {
			goto l36;
		//assert(false);
		return;  			}
	l36: {
			__context->DONE = false;
			goto l37;
		//assert(false);
		return;  			}
	l37: {
			goto l53;
		//assert(false);
		return;  			}
	l38: {
		if ((__context->OUTV < (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))) {
			goto l39;
		}
		if (((! (__context->OUTV < (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))) && ((__context->OUTV < __context->INV_sat) && (__context->OUTV > (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))))) {
			goto l42;
		}
		if ((((! (__context->OUTV < (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))) && (! ((__context->OUTV < __context->INV_sat) && (__context->OUTV > (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))))) && (__context->OUTV > (__context->INV_sat + ((__context->DeSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0))))) {
			goto l45;
		}
		if ((((! (__context->OUTV < (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))) && ((! ((__context->OUTV < __context->INV_sat) && (__context->OUTV > (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0))))) && (! (__context->OUTV > (__context->INV_sat + ((__context->DeSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))))) && ((__context->OUTV > __context->INV_sat) && (__context->OUTV < (__context->INV_sat + ((__context->DeSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))))) {
			goto l48;
		}
		if (((! (__context->OUTV < (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))) && ((! ((! (__context->OUTV < (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))) && ((__context->OUTV < __context->INV_sat) && (__context->OUTV > (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))))) && ((! (((! (__context->OUTV < (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))) && (! ((__context->OUTV < __context->INV_sat) && (__context->OUTV > (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))))) && (__context->OUTV > (__context->INV_sat + ((__context->DeSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0))))) && (! (((! (__context->OUTV < (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))) && ((! ((__context->OUTV < __context->INV_sat) && (__context->OUTV > (__context->INV_sat - ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0))))) && (! (__context->OUTV > (__context->INV_sat + ((__context->DeSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))))) && ((__context->OUTV > __context->INV_sat) && (__context->OUTV < (__context->INV_sat + ((__context->DeSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0)))))))))) {
			goto l51;
		}
		//assert(false);
		return;  			}
	l39: {
			__context->OUTV = (__context->OUTV + ((__context->InSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0));
			goto l40;
		//assert(false);
		return;  			}
	l40: {
			__context->DONE = false;
			goto l41;
		//assert(false);
		return;  			}
	l41: {
			goto l52;
		//assert(false);
		return;  			}
	l42: {
			__context->OUTV = __context->INV_sat;
			goto l43;
		//assert(false);
		return;  			}
	l43: {
			__context->DONE = true;
			goto l44;
		//assert(false);
		return;  			}
	l44: {
			goto l52;
		//assert(false);
		return;  			}
	l45: {
			__context->OUTV = (__context->OUTV - ((__context->DeSpd * ((float) ((int32_t) __context->CYCLE))) / 1000.0));
			goto l46;
		//assert(false);
		return;  			}
	l46: {
			__context->DONE = false;
			goto l47;
		//assert(false);
		return;  			}
	l47: {
			goto l52;
		//assert(false);
		return;  			}
	l48: {
			__context->OUTV = __context->INV_sat;
			goto l49;
		//assert(false);
		return;  			}
	l49: {
			__context->DONE = true;
			goto l50;
		//assert(false);
		return;  			}
	l50: {
			goto l52;
		//assert(false);
		return;  			}
	l51: {
			goto l52;
		//assert(false);
		return;  			}
	l52: {
			goto l53;
		//assert(false);
		return;  			}
	l53: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			instance.COM_RST = nondet_bool();
			instance.CYCLE = nondet_int32_t();
			instance.DFOUT_ON = nondet_bool();
			instance.DF_OUTV = nondet_float();
			instance.DeSpd = nondet_float();
			instance.H_LM = nondet_float();
			instance.INV = nondet_float();
			instance.InSpd = nondet_float();
			instance.L_LM = nondet_float();
			instance.MAN_ON = nondet_bool();
			instance.PV = nondet_float();
			instance.TRACK = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			RAMP(&instance);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
void main() {
	// Initial values
	instance.INV = 0.0;
	instance.InSpd = 0.0;
	instance.DeSpd = 0.0;
	instance.H_LM = 0.0;
	instance.L_LM = 0.0;
	instance.PV = 0.0;
	instance.DF_OUTV = 0.0;
	instance.DFOUT_ON = false;
	instance.TRACK = false;
	instance.MAN_ON = false;
	instance.COM_RST = false;
	instance.CYCLE = 0;
	instance.OUTV = 0.0;
	instance.DONE = false;
	instance.INV_sat = 0.0;
	__assertion_error = 0;
	
	VerificationLoop();
}
