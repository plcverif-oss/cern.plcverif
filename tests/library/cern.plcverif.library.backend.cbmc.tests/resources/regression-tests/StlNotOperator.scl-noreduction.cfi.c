#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Variables
uint16_t NotTest_in = 0;
bool NotTest_inbool = false;
uint16_t NotTest_out = 0;
bool NotTest_outbool = false;
bool __RLO = false;
bool __NFC = false;
bool __BR = false;
bool __STA = false;
bool __OR = false;
bool __CC0 = false;
bool __CC1 = false;
bool __OS = false;
bool __OV = false;
int32_t __ACCU1 = 0;
int32_t __ACCU2 = 0;
uint16_t __assertion_error = 0;
uint16_t __assertion_error1 = 0;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error1 == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			goto verificationLoop_VerificationLoop_loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_end: {
			goto callEnd;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_loop_start: {
			NotTest_in = nondet_uint16_t();
			NotTest_inbool = nondet_bool();
			goto verificationLoop_VerificationLoop_l_main_call;
		if (false) {
			goto verificationLoop_VerificationLoop_end;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l_main_call: {
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_callEnd: {
			goto verificationLoop_VerificationLoop_prepare_EoC;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_prepare_EoC: {
			goto verificationLoop_VerificationLoop_loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_init: {
			__ACCU2 = __ACCU1;
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l1: {
			__ACCU1 = ((int32_t) NotTest_in);
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l2: {
			__ACCU2 = __ACCU1;
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l3: {
			__ACCU1 = ((int32_t) 65535);
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l4: {
			__ACCU1 = (((__ACCU1 & 65535) ^ (__ACCU2 & 65535)) | (__ACCU1 & -65536));
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l5: {
			__CC0 = false;
			__OV = false;
			__CC1 = ((__ACCU1 & 65535) == 0);
			NotTest_out = ((uint16_t) __ACCU1);
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l7: {
		if (((! (! (((uint16_t)~ NotTest_in) == NotTest_out))) && (! (! (((uint16_t)~ (NotTest_in & NotTest_in)) == NotTest_out))))) {
			__ACCU2 = __ACCU1;
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l8;
		}
		if ((! (((uint16_t)~ NotTest_in) == NotTest_out))) {
			__assertion_error = 1;
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_end;
		}
		if (((! (! (((uint16_t)~ NotTest_in) == NotTest_out))) && (! (((uint16_t)~ (NotTest_in & NotTest_in)) == NotTest_out)))) {
			__assertion_error = 2;
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_end;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l8: {
			__ACCU1 = ((int32_t) 0);
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l9: {
			NotTest_out = ((uint16_t) __ACCU1);
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l10: {
		if ((! (! (((uint16_t)~ NotTest_out) == 65535)))) {
			__RLO = true;
			__STA = true;
			__OR = false;
			__NFC = false;
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l11;
		}
		if ((! (((uint16_t)~ NotTest_out) == 65535))) {
			__assertion_error = 3;
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_end;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l11: {
			__RLO = (((! __NFC) || __RLO) && (NotTest_inbool || __OR));
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l12: {
			__OR = (__OR && __NFC);
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l13: {
			__STA = NotTest_inbool;
			__NFC = true;
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l14;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l14: {
			__RLO = ((__NFC && __RLO) != true);
			__OR = false;
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l16;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l16: {
			__STA = true;
			__NFC = true;
			NotTest_outbool = __RLO;
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l18;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l18: {
			__OR = false;
			__STA = __RLO;
			__NFC = false;
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l21: {
		if (((! (! ((! NotTest_inbool) == NotTest_outbool))) && (! (! ((! (NotTest_inbool && NotTest_inbool)) == NotTest_outbool))))) {
			__RLO = true;
			__STA = true;
			__OR = false;
			__NFC = false;
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l22;
		}
		if ((! ((! NotTest_inbool) == NotTest_outbool))) {
			__assertion_error = 4;
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_end;
		}
		if (((! (! ((! NotTest_inbool) == NotTest_outbool))) && (! ((! (NotTest_inbool && NotTest_inbool)) == NotTest_outbool)))) {
			__assertion_error = 5;
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_end;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l22: {
			__RLO = (((! __NFC) || __RLO) && (false || __OR));
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l23;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l23: {
			__OR = (__OR && __NFC);
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l24;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l24: {
			__STA = false;
			__NFC = true;
			NotTest_outbool = __RLO;
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l26;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l26: {
			__OR = false;
			__STA = __RLO;
			__NFC = false;
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l29;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_l29: {
		if ((! (! ((! NotTest_outbool) == true)))) {
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_end;
		}
		if ((! ((! NotTest_outbool) == true))) {
			__assertion_error = 6;
			goto verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_end;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_verificationLoop_VerificationLoop_end: {
			goto verificationLoop_VerificationLoop_callEnd;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
