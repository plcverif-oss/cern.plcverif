#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Root data structure
typedef struct {
	int16_t FEType;
	int16_t index;
	int16_t DBnum;
	int16_t perByte;
	int16_t perBit;
	int16_t DBnumIoError;
	int16_t DBposIoError;
	int16_t DBbitIoError;
	bool AuIhFoMo;
	bool PosSt;
	bool MPosRSt;
	bool HFSt;
	bool AuMoSt;
	bool FoMoSt;
	bool IOErrorW;
	bool IOSimuW;
	bool IOError;
	bool IOSimu;
	bool FoDiProW;
	bool HFPos;
	bool MIOErBRSt;
	bool MIOErBSetRst_old;
} __CPC_DB_DI;
typedef struct {
	uint16_t Manreg01;
	bool Manreg01b[16];
	uint16_t Stsreg01;
	__CPC_DB_DI Perst;
} __CPC_FB_DI;
typedef struct {
	bool new;
	bool old;
	bool RET_VAL;
} __R_EDGE;

// Global variables
__R_EDGE R_EDGE1;
__CPC_FB_DI instance;
uint16_t __assertion_error;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Automata declarations
void CPC_FB_DI(__CPC_FB_DI *__context);
void R_EDGE(__R_EDGE *__context);
void VerificationLoop();

// Automata
void CPC_FB_DI(__CPC_FB_DI *__context) {
	// Temporary variables
	bool E_MIOErBSetRst;
	uint16_t TempStsreg01;
	bool Stsreg01b[16];
	
	// Start with initial location
	goto init;
	init: {
			__context->Manreg01b[0] = ((__context->Manreg01 & 256) != 0);
			goto varview_refresh1;
		//assert(false);
		return;  			}
	l1: {
			TempStsreg01 = __context->Stsreg01;
			goto x2;
		//assert(false);
		return;  			}
	l2: {
			// Assign inputs
			R_EDGE1.new = __context->Manreg01b[2];
			R_EDGE1.old = __context->Perst.MIOErBSetRst_old;
			R_EDGE(&R_EDGE1);
			// Assign outputs
			__context->Perst.MIOErBSetRst_old = R_EDGE1.old;
			E_MIOErBSetRst = R_EDGE1.RET_VAL;
			goto l3;
		//assert(false);
		return;  			}
	l3: {
		if ((__context->Manreg01b[8] || __context->Perst.AuIhFoMo)) {
			goto l4;
		}
		if ((! (__context->Manreg01b[8] || __context->Perst.AuIhFoMo))) {
			goto l6;
		}
		//assert(false);
		return;  			}
	l4: {
			__context->Perst.FoMoSt = false;
			goto l5;
		//assert(false);
		return;  			}
	l5: {
			goto l7;
		//assert(false);
		return;  			}
	l6: {
			goto l7;
		//assert(false);
		return;  			}
	l7: {
		if ((__context->Manreg01b[10] && (! __context->Perst.AuIhFoMo))) {
			goto l8;
		}
		if ((! (__context->Manreg01b[10] && (! __context->Perst.AuIhFoMo)))) {
			goto l10;
		}
		//assert(false);
		return;  			}
	l8: {
			__context->Perst.FoMoSt = true;
			goto l9;
		//assert(false);
		return;  			}
	l9: {
			goto l11;
		//assert(false);
		return;  			}
	l10: {
			goto l11;
		//assert(false);
		return;  			}
	l11: {
			__context->Perst.AuMoSt = (! __context->Perst.FoMoSt);
			goto l12;
		//assert(false);
		return;  			}
	l12: {
		if (E_MIOErBSetRst) {
			goto l13;
		}
		if ((! E_MIOErBSetRst)) {
			goto l15;
		}
		//assert(false);
		return;  			}
	l13: {
			__context->Perst.MIOErBRSt = (! __context->Perst.MIOErBRSt);
			goto l14;
		//assert(false);
		return;  			}
	l14: {
			goto l16;
		//assert(false);
		return;  			}
	l15: {
			goto l16;
		//assert(false);
		return;  			}
	l16: {
			__context->Perst.IOErrorW = (__context->Perst.IOError && (! __context->Perst.MIOErBRSt));
			goto l17;
		//assert(false);
		return;  			}
	l17: {
			__context->Perst.IOSimuW = __context->Perst.IOSimu;
			goto l18;
		//assert(false);
		return;  			}
	l18: {
		if (__context->Manreg01b[13]) {
			goto l19;
		}
		if ((! __context->Manreg01b[13])) {
			goto l21;
		}
		//assert(false);
		return;  			}
	l19: {
			__context->Perst.MPosRSt = false;
			goto l20;
		//assert(false);
		return;  			}
	l20: {
			goto l22;
		//assert(false);
		return;  			}
	l21: {
			goto l22;
		//assert(false);
		return;  			}
	l22: {
		if (__context->Manreg01b[12]) {
			goto l23;
		}
		if ((! __context->Manreg01b[12])) {
			goto l25;
		}
		//assert(false);
		return;  			}
	l23: {
			__context->Perst.MPosRSt = true;
			goto l24;
		//assert(false);
		return;  			}
	l24: {
			goto l26;
		//assert(false);
		return;  			}
	l25: {
			goto l26;
		//assert(false);
		return;  			}
	l26: {
		if (__context->Perst.FoMoSt) {
			goto l27;
		}
		if ((! __context->Perst.FoMoSt)) {
			goto l30;
		}
		//assert(false);
		return;  			}
	l27: {
			__context->Perst.PosSt = __context->Perst.MPosRSt;
			goto l28;
		//assert(false);
		return;  			}
	l28: {
			__context->Perst.FoDiProW = (__context->Perst.HFPos != __context->Perst.MPosRSt);
			goto l29;
		//assert(false);
		return;  			}
	l29: {
			goto l34;
		//assert(false);
		return;  			}
	l30: {
			__context->Perst.MPosRSt = __context->Perst.HFPos;
			goto l31;
		//assert(false);
		return;  			}
	l31: {
			__context->Perst.PosSt = __context->Perst.HFPos;
			goto l32;
		//assert(false);
		return;  			}
	l32: {
			__context->Perst.FoDiProW = false;
			goto l33;
		//assert(false);
		return;  			}
	l33: {
			goto l34;
		//assert(false);
		return;  			}
	l34: {
			__context->Perst.HFSt = __context->Perst.HFPos;
			goto l35;
		//assert(false);
		return;  			}
	l35: {
			Stsreg01b[8] = __context->Perst.PosSt;
			goto x3;
		//assert(false);
		return;  			}
	l36: {
			Stsreg01b[9] = false;
			goto x4;
		//assert(false);
		return;  			}
	l37: {
			Stsreg01b[10] = __context->Perst.AuMoSt;
			goto x5;
		//assert(false);
		return;  			}
	l38: {
			Stsreg01b[11] = false;
			goto x6;
		//assert(false);
		return;  			}
	l39: {
			Stsreg01b[12] = __context->Perst.FoMoSt;
			goto x7;
		//assert(false);
		return;  			}
	l40: {
			Stsreg01b[13] = false;
			goto x8;
		//assert(false);
		return;  			}
	l41: {
			Stsreg01b[14] = __context->Perst.IOErrorW;
			goto x9;
		//assert(false);
		return;  			}
	l42: {
			Stsreg01b[15] = __context->Perst.IOSimuW;
			goto x10;
		//assert(false);
		return;  			}
	l43: {
			Stsreg01b[0] = __context->Perst.FoDiProW;
			goto x11;
		//assert(false);
		return;  			}
	l44: {
			Stsreg01b[1] = __context->Perst.MIOErBRSt;
			goto x12;
		//assert(false);
		return;  			}
	l45: {
			Stsreg01b[2] = false;
			goto x13;
		//assert(false);
		return;  			}
	l46: {
			Stsreg01b[3] = false;
			goto x14;
		//assert(false);
		return;  			}
	l47: {
			Stsreg01b[4] = false;
			goto x15;
		//assert(false);
		return;  			}
	l48: {
			Stsreg01b[5] = __context->Perst.AuIhFoMo;
			goto x16;
		//assert(false);
		return;  			}
	l49: {
			Stsreg01b[6] = __context->Perst.HFSt;
			goto x17;
		//assert(false);
		return;  			}
	l50: {
			Stsreg01b[7] = __context->Perst.MPosRSt;
			goto x18;
		//assert(false);
		return;  			}
	l51: {
			__context->Stsreg01 = TempStsreg01;
			goto l52;
		//assert(false);
		return;  			}
	l52: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	x: {
			E_MIOErBSetRst = false;
			TempStsreg01 = 0;
			goto x1;
		//assert(false);
		return;  			}
	varview_refresh: {
			Stsreg01b[0] = ((TempStsreg01 & 256) != 0);
			goto varview_refresh32;
		//assert(false);
		return;  			}
	varview_refresh1: {
			__context->Manreg01b[1] = ((__context->Manreg01 & 512) != 0);
			goto varview_refresh3;
		//assert(false);
		return;  			}
	varview_refresh3: {
			__context->Manreg01b[2] = ((__context->Manreg01 & 1024) != 0);
			goto varview_refresh5;
		//assert(false);
		return;  			}
	varview_refresh5: {
			__context->Manreg01b[3] = ((__context->Manreg01 & 2048) != 0);
			goto varview_refresh7;
		//assert(false);
		return;  			}
	varview_refresh7: {
			__context->Manreg01b[4] = ((__context->Manreg01 & 4096) != 0);
			goto varview_refresh9;
		//assert(false);
		return;  			}
	varview_refresh9: {
			__context->Manreg01b[5] = ((__context->Manreg01 & 8192) != 0);
			goto varview_refresh11;
		//assert(false);
		return;  			}
	varview_refresh11: {
			__context->Manreg01b[6] = ((__context->Manreg01 & 16384) != 0);
			goto varview_refresh13;
		//assert(false);
		return;  			}
	varview_refresh13: {
			__context->Manreg01b[7] = ((__context->Manreg01 & 32768) != 0);
			goto varview_refresh15;
		//assert(false);
		return;  			}
	varview_refresh15: {
			__context->Manreg01b[8] = ((__context->Manreg01 & 1) != 0);
			goto varview_refresh17;
		//assert(false);
		return;  			}
	varview_refresh17: {
			__context->Manreg01b[9] = ((__context->Manreg01 & 2) != 0);
			goto varview_refresh19;
		//assert(false);
		return;  			}
	varview_refresh19: {
			__context->Manreg01b[10] = ((__context->Manreg01 & 4) != 0);
			goto varview_refresh21;
		//assert(false);
		return;  			}
	varview_refresh21: {
			__context->Manreg01b[11] = ((__context->Manreg01 & 8) != 0);
			goto varview_refresh23;
		//assert(false);
		return;  			}
	varview_refresh23: {
			__context->Manreg01b[12] = ((__context->Manreg01 & 16) != 0);
			goto varview_refresh25;
		//assert(false);
		return;  			}
	varview_refresh25: {
			__context->Manreg01b[13] = ((__context->Manreg01 & 32) != 0);
			goto varview_refresh27;
		//assert(false);
		return;  			}
	varview_refresh27: {
			__context->Manreg01b[14] = ((__context->Manreg01 & 64) != 0);
			goto varview_refresh29;
		//assert(false);
		return;  			}
	varview_refresh29: {
			__context->Manreg01b[15] = ((__context->Manreg01 & 128) != 0);
			goto varview_refresh;
		//assert(false);
		return;  			}
	varview_refresh32: {
			Stsreg01b[1] = ((TempStsreg01 & 512) != 0);
			goto varview_refresh34;
		//assert(false);
		return;  			}
	varview_refresh34: {
			Stsreg01b[2] = ((TempStsreg01 & 1024) != 0);
			goto varview_refresh36;
		//assert(false);
		return;  			}
	varview_refresh36: {
			Stsreg01b[3] = ((TempStsreg01 & 2048) != 0);
			goto varview_refresh38;
		//assert(false);
		return;  			}
	varview_refresh38: {
			Stsreg01b[4] = ((TempStsreg01 & 4096) != 0);
			goto varview_refresh40;
		//assert(false);
		return;  			}
	varview_refresh40: {
			Stsreg01b[5] = ((TempStsreg01 & 8192) != 0);
			goto varview_refresh42;
		//assert(false);
		return;  			}
	varview_refresh42: {
			Stsreg01b[6] = ((TempStsreg01 & 16384) != 0);
			goto varview_refresh44;
		//assert(false);
		return;  			}
	varview_refresh44: {
			Stsreg01b[7] = ((TempStsreg01 & 32768) != 0);
			goto varview_refresh46;
		//assert(false);
		return;  			}
	varview_refresh46: {
			Stsreg01b[8] = ((TempStsreg01 & 1) != 0);
			goto varview_refresh48;
		//assert(false);
		return;  			}
	varview_refresh48: {
			Stsreg01b[9] = ((TempStsreg01 & 2) != 0);
			goto varview_refresh50;
		//assert(false);
		return;  			}
	varview_refresh50: {
			Stsreg01b[10] = ((TempStsreg01 & 4) != 0);
			goto varview_refresh52;
		//assert(false);
		return;  			}
	varview_refresh52: {
			Stsreg01b[11] = ((TempStsreg01 & 8) != 0);
			goto varview_refresh54;
		//assert(false);
		return;  			}
	varview_refresh54: {
			Stsreg01b[12] = ((TempStsreg01 & 16) != 0);
			goto varview_refresh56;
		//assert(false);
		return;  			}
	varview_refresh56: {
			Stsreg01b[13] = ((TempStsreg01 & 32) != 0);
			goto varview_refresh58;
		//assert(false);
		return;  			}
	varview_refresh58: {
			Stsreg01b[14] = ((TempStsreg01 & 64) != 0);
			goto varview_refresh60;
		//assert(false);
		return;  			}
	varview_refresh60: {
			Stsreg01b[15] = ((TempStsreg01 & 128) != 0);
			goto x;
		//assert(false);
		return;  			}
	x1: {
			Stsreg01b[0] = ((TempStsreg01 & 256) != 0);
			goto varview_refresh63;
		//assert(false);
		return;  			}
	varview_refresh63: {
			Stsreg01b[1] = ((TempStsreg01 & 512) != 0);
			goto varview_refresh65;
		//assert(false);
		return;  			}
	varview_refresh65: {
			Stsreg01b[2] = ((TempStsreg01 & 1024) != 0);
			goto varview_refresh67;
		//assert(false);
		return;  			}
	varview_refresh67: {
			Stsreg01b[3] = ((TempStsreg01 & 2048) != 0);
			goto varview_refresh69;
		//assert(false);
		return;  			}
	varview_refresh69: {
			Stsreg01b[4] = ((TempStsreg01 & 4096) != 0);
			goto varview_refresh71;
		//assert(false);
		return;  			}
	varview_refresh71: {
			Stsreg01b[5] = ((TempStsreg01 & 8192) != 0);
			goto varview_refresh73;
		//assert(false);
		return;  			}
	varview_refresh73: {
			Stsreg01b[6] = ((TempStsreg01 & 16384) != 0);
			goto varview_refresh75;
		//assert(false);
		return;  			}
	varview_refresh75: {
			Stsreg01b[7] = ((TempStsreg01 & 32768) != 0);
			goto varview_refresh77;
		//assert(false);
		return;  			}
	varview_refresh77: {
			Stsreg01b[8] = ((TempStsreg01 & 1) != 0);
			goto varview_refresh79;
		//assert(false);
		return;  			}
	varview_refresh79: {
			Stsreg01b[9] = ((TempStsreg01 & 2) != 0);
			goto varview_refresh81;
		//assert(false);
		return;  			}
	varview_refresh81: {
			Stsreg01b[10] = ((TempStsreg01 & 4) != 0);
			goto varview_refresh83;
		//assert(false);
		return;  			}
	varview_refresh83: {
			Stsreg01b[11] = ((TempStsreg01 & 8) != 0);
			goto varview_refresh85;
		//assert(false);
		return;  			}
	varview_refresh85: {
			Stsreg01b[12] = ((TempStsreg01 & 16) != 0);
			goto varview_refresh87;
		//assert(false);
		return;  			}
	varview_refresh87: {
			Stsreg01b[13] = ((TempStsreg01 & 32) != 0);
			goto varview_refresh89;
		//assert(false);
		return;  			}
	varview_refresh89: {
			Stsreg01b[14] = ((TempStsreg01 & 64) != 0);
			goto varview_refresh91;
		//assert(false);
		return;  			}
	varview_refresh91: {
			Stsreg01b[15] = ((TempStsreg01 & 128) != 0);
			goto l1;
		//assert(false);
		return;  			}
	x2: {
			Stsreg01b[0] = ((TempStsreg01 & 256) != 0);
			goto varview_refresh94;
		//assert(false);
		return;  			}
	varview_refresh94: {
			Stsreg01b[1] = ((TempStsreg01 & 512) != 0);
			goto varview_refresh96;
		//assert(false);
		return;  			}
	varview_refresh96: {
			Stsreg01b[2] = ((TempStsreg01 & 1024) != 0);
			goto varview_refresh98;
		//assert(false);
		return;  			}
	varview_refresh98: {
			Stsreg01b[3] = ((TempStsreg01 & 2048) != 0);
			goto varview_refresh100;
		//assert(false);
		return;  			}
	varview_refresh100: {
			Stsreg01b[4] = ((TempStsreg01 & 4096) != 0);
			goto varview_refresh102;
		//assert(false);
		return;  			}
	varview_refresh102: {
			Stsreg01b[5] = ((TempStsreg01 & 8192) != 0);
			goto varview_refresh104;
		//assert(false);
		return;  			}
	varview_refresh104: {
			Stsreg01b[6] = ((TempStsreg01 & 16384) != 0);
			goto varview_refresh106;
		//assert(false);
		return;  			}
	varview_refresh106: {
			Stsreg01b[7] = ((TempStsreg01 & 32768) != 0);
			goto varview_refresh108;
		//assert(false);
		return;  			}
	varview_refresh108: {
			Stsreg01b[8] = ((TempStsreg01 & 1) != 0);
			goto varview_refresh110;
		//assert(false);
		return;  			}
	varview_refresh110: {
			Stsreg01b[9] = ((TempStsreg01 & 2) != 0);
			goto varview_refresh112;
		//assert(false);
		return;  			}
	varview_refresh112: {
			Stsreg01b[10] = ((TempStsreg01 & 4) != 0);
			goto varview_refresh114;
		//assert(false);
		return;  			}
	varview_refresh114: {
			Stsreg01b[11] = ((TempStsreg01 & 8) != 0);
			goto varview_refresh116;
		//assert(false);
		return;  			}
	varview_refresh116: {
			Stsreg01b[12] = ((TempStsreg01 & 16) != 0);
			goto varview_refresh118;
		//assert(false);
		return;  			}
	varview_refresh118: {
			Stsreg01b[13] = ((TempStsreg01 & 32) != 0);
			goto varview_refresh120;
		//assert(false);
		return;  			}
	varview_refresh120: {
			Stsreg01b[14] = ((TempStsreg01 & 64) != 0);
			goto varview_refresh122;
		//assert(false);
		return;  			}
	varview_refresh122: {
			Stsreg01b[15] = ((TempStsreg01 & 128) != 0);
			goto l2;
		//assert(false);
		return;  			}
	x3: {
		if (Stsreg01b[8]) {
			TempStsreg01 = (TempStsreg01 | 1);
			goto l36;
		}
		if ((! Stsreg01b[8])) {
			TempStsreg01 = (TempStsreg01 & 65534);
			goto l36;
		}
		//assert(false);
		return;  			}
	x4: {
		if (Stsreg01b[9]) {
			TempStsreg01 = (TempStsreg01 | 2);
			goto l37;
		}
		if ((! Stsreg01b[9])) {
			TempStsreg01 = (TempStsreg01 & 65533);
			goto l37;
		}
		//assert(false);
		return;  			}
	x5: {
		if (Stsreg01b[10]) {
			TempStsreg01 = (TempStsreg01 | 4);
			goto l38;
		}
		if ((! Stsreg01b[10])) {
			TempStsreg01 = (TempStsreg01 & 65531);
			goto l38;
		}
		//assert(false);
		return;  			}
	x6: {
		if (Stsreg01b[11]) {
			TempStsreg01 = (TempStsreg01 | 8);
			goto l39;
		}
		if ((! Stsreg01b[11])) {
			TempStsreg01 = (TempStsreg01 & 65527);
			goto l39;
		}
		//assert(false);
		return;  			}
	x7: {
		if (Stsreg01b[12]) {
			TempStsreg01 = (TempStsreg01 | 16);
			goto l40;
		}
		if ((! Stsreg01b[12])) {
			TempStsreg01 = (TempStsreg01 & 65519);
			goto l40;
		}
		//assert(false);
		return;  			}
	x8: {
		if (Stsreg01b[13]) {
			TempStsreg01 = (TempStsreg01 | 32);
			goto l41;
		}
		if ((! Stsreg01b[13])) {
			TempStsreg01 = (TempStsreg01 & 65503);
			goto l41;
		}
		//assert(false);
		return;  			}
	x9: {
		if (Stsreg01b[14]) {
			TempStsreg01 = (TempStsreg01 | 64);
			goto l42;
		}
		if ((! Stsreg01b[14])) {
			TempStsreg01 = (TempStsreg01 & 65471);
			goto l42;
		}
		//assert(false);
		return;  			}
	x10: {
		if (Stsreg01b[15]) {
			TempStsreg01 = (TempStsreg01 | 128);
			goto l43;
		}
		if ((! Stsreg01b[15])) {
			TempStsreg01 = (TempStsreg01 & 65407);
			goto l43;
		}
		//assert(false);
		return;  			}
	x11: {
		if (Stsreg01b[0]) {
			TempStsreg01 = (TempStsreg01 | 256);
			goto l44;
		}
		if ((! Stsreg01b[0])) {
			TempStsreg01 = (TempStsreg01 & 65279);
			goto l44;
		}
		//assert(false);
		return;  			}
	x12: {
		if (Stsreg01b[1]) {
			TempStsreg01 = (TempStsreg01 | 512);
			goto l45;
		}
		if ((! Stsreg01b[1])) {
			TempStsreg01 = (TempStsreg01 & 65023);
			goto l45;
		}
		//assert(false);
		return;  			}
	x13: {
		if (Stsreg01b[2]) {
			TempStsreg01 = (TempStsreg01 | 1024);
			goto l46;
		}
		if ((! Stsreg01b[2])) {
			TempStsreg01 = (TempStsreg01 & 64511);
			goto l46;
		}
		//assert(false);
		return;  			}
	x14: {
		if (Stsreg01b[3]) {
			TempStsreg01 = (TempStsreg01 | 2048);
			goto l47;
		}
		if ((! Stsreg01b[3])) {
			TempStsreg01 = (TempStsreg01 & 63487);
			goto l47;
		}
		//assert(false);
		return;  			}
	x15: {
		if (Stsreg01b[4]) {
			TempStsreg01 = (TempStsreg01 | 4096);
			goto l48;
		}
		if ((! Stsreg01b[4])) {
			TempStsreg01 = (TempStsreg01 & 61439);
			goto l48;
		}
		//assert(false);
		return;  			}
	x16: {
		if (Stsreg01b[5]) {
			TempStsreg01 = (TempStsreg01 | 8192);
			goto l49;
		}
		if ((! Stsreg01b[5])) {
			TempStsreg01 = (TempStsreg01 & 57343);
			goto l49;
		}
		//assert(false);
		return;  			}
	x17: {
		if (Stsreg01b[6]) {
			TempStsreg01 = (TempStsreg01 | 16384);
			goto l50;
		}
		if ((! Stsreg01b[6])) {
			TempStsreg01 = (TempStsreg01 & 49151);
			goto l50;
		}
		//assert(false);
		return;  			}
	x18: {
		if (Stsreg01b[7]) {
			TempStsreg01 = (TempStsreg01 | 32768);
			goto l51;
		}
		if ((! Stsreg01b[7])) {
			TempStsreg01 = (TempStsreg01 & 32767);
			goto l51;
		}
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void R_EDGE(__R_EDGE *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
		if (((__context->new == true) && (__context->old == false))) {
			goto l110;
		}
		if ((! ((__context->new == true) && (__context->old == false)))) {
			goto l410;
		}
		//assert(false);
		return;  			}
	l110: {
			__context->RET_VAL = true;
			goto l210;
		//assert(false);
		return;  			}
	l210: {
			__context->old = true;
			goto l310;
		//assert(false);
		return;  			}
	l310: {
			goto l71;
		//assert(false);
		return;  			}
	l410: {
			__context->RET_VAL = false;
			goto l53;
		//assert(false);
		return;  			}
	l53: {
			__context->old = __context->new;
			goto l61;
		//assert(false);
		return;  			}
	l61: {
			goto l71;
		//assert(false);
		return;  			}
	l71: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init2;
	init2: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			instance.Manreg01 = nondet_uint16_t();
			instance.Manreg01b[0] = nondet_bool();
			instance.Manreg01b[10] = nondet_bool();
			instance.Manreg01b[11] = nondet_bool();
			instance.Manreg01b[12] = nondet_bool();
			instance.Manreg01b[13] = nondet_bool();
			instance.Manreg01b[14] = nondet_bool();
			instance.Manreg01b[15] = nondet_bool();
			instance.Manreg01b[1] = nondet_bool();
			instance.Manreg01b[2] = nondet_bool();
			instance.Manreg01b[3] = nondet_bool();
			instance.Manreg01b[4] = nondet_bool();
			instance.Manreg01b[5] = nondet_bool();
			instance.Manreg01b[6] = nondet_bool();
			instance.Manreg01b[7] = nondet_bool();
			instance.Manreg01b[8] = nondet_bool();
			instance.Manreg01b[9] = nondet_bool();
			instance.Perst.AuIhFoMo = nondet_bool();
			instance.Perst.AuMoSt = nondet_bool();
			instance.Perst.DBbitIoError = nondet_int16_t();
			instance.Perst.DBnum = nondet_int16_t();
			instance.Perst.DBnumIoError = nondet_int16_t();
			instance.Perst.DBposIoError = nondet_int16_t();
			instance.Perst.FEType = nondet_int16_t();
			instance.Perst.FoDiProW = nondet_bool();
			instance.Perst.FoMoSt = nondet_bool();
			instance.Perst.HFPos = nondet_bool();
			instance.Perst.HFSt = nondet_bool();
			instance.Perst.IOError = nondet_bool();
			instance.Perst.IOErrorW = nondet_bool();
			instance.Perst.IOSimu = nondet_bool();
			instance.Perst.IOSimuW = nondet_bool();
			instance.Perst.MIOErBRSt = nondet_bool();
			instance.Perst.MIOErBSetRst_old = nondet_bool();
			instance.Perst.MPosRSt = nondet_bool();
			instance.Perst.PosSt = nondet_bool();
			instance.Perst.index = nondet_int16_t();
			instance.Perst.perBit = nondet_int16_t();
			instance.Perst.perByte = nondet_int16_t();
			instance.Stsreg01 = nondet_uint16_t();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			CPC_FB_DI(&instance);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
void main() {
	// Initial values
	R_EDGE1.new = false;
	R_EDGE1.old = false;
	R_EDGE1.RET_VAL = false;
	instance.Manreg01 = 0;
	instance.Manreg01b[0] = false;
	instance.Manreg01b[1] = false;
	instance.Manreg01b[2] = false;
	instance.Manreg01b[3] = false;
	instance.Manreg01b[4] = false;
	instance.Manreg01b[5] = false;
	instance.Manreg01b[6] = false;
	instance.Manreg01b[7] = false;
	instance.Manreg01b[8] = false;
	instance.Manreg01b[9] = false;
	instance.Manreg01b[10] = false;
	instance.Manreg01b[11] = false;
	instance.Manreg01b[12] = false;
	instance.Manreg01b[13] = false;
	instance.Manreg01b[14] = false;
	instance.Manreg01b[15] = false;
	instance.Stsreg01 = 0;
	instance.Perst.FEType = 0;
	instance.Perst.index = 0;
	instance.Perst.DBnum = 0;
	instance.Perst.perByte = 0;
	instance.Perst.perBit = 0;
	instance.Perst.DBnumIoError = 0;
	instance.Perst.DBposIoError = 0;
	instance.Perst.DBbitIoError = 0;
	instance.Perst.AuIhFoMo = false;
	instance.Perst.PosSt = false;
	instance.Perst.MPosRSt = false;
	instance.Perst.HFSt = false;
	instance.Perst.AuMoSt = false;
	instance.Perst.FoMoSt = false;
	instance.Perst.IOErrorW = false;
	instance.Perst.IOSimuW = false;
	instance.Perst.IOError = false;
	instance.Perst.IOSimu = false;
	instance.Perst.FoDiProW = false;
	instance.Perst.HFPos = false;
	instance.Perst.MIOErBRSt = false;
	instance.Perst.MIOErBSetRst_old = false;
	__assertion_error = 0;
	
	VerificationLoop();
}
