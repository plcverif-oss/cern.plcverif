#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Root data structure
typedef struct {
	bool __RLO;
	bool __NFC;
	bool __BR;
	bool __STA;
	bool __OR;
	bool __CC0;
	bool __CC1;
} __jumps_fb1;

// Global variables
bool MX0_0;
bool MX0_1;
bool MX0_2;
__jumps_fb1 instance;
uint16_t __assertion_error;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Automata declarations
void jumps_fb1(__jumps_fb1 *__context);
void VerificationLoop();

// Automata
void jumps_fb1(__jumps_fb1 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			__context->__OR = false;
			__context->__STA = true;
			__context->__RLO = true;
			__context->__CC0 = false;
			__context->__CC1 = false;
			__context->__BR = false;
			__context->__NFC = false;
			goto l1;
		//assert(false);
		return;  			}
	l1: {
			__context->__RLO = (((! __context->__NFC) || __context->__RLO) && (MX0_0 || __context->__OR));
			goto l2;
		//assert(false);
		return;  			}
	l2: {
			__context->__STA = MX0_0;
			goto l3;
		//assert(false);
		return;  			}
	l3: {
			__context->__NFC = true;
			goto l4;
		//assert(false);
		return;  			}
	l4: {
			goto l12;
		//assert(false);
		return;  			}
	l5: {
			__context->__RLO = (((! __context->__NFC) || __context->__RLO) && (MX0_1 || __context->__OR));
			goto l6;
		//assert(false);
		return;  			}
	l6: {
			__context->__STA = MX0_1;
			goto l7;
		//assert(false);
		return;  			}
	l7: {
			__context->__NFC = true;
			goto l8;
		//assert(false);
		return;  			}
	l8: {
		if (__context->__RLO) {
			goto l9;
		}
		if ((! __context->__RLO)) {
			goto l10;
		}
		//assert(false);
		return;  			}
	l9: {
			goto l10;
		//assert(false);
		return;  			}
	l10: {
		if ((! __context->__RLO)) {
			goto l11;
		}
		if ((! (! __context->__RLO))) {
			goto l12;
		}
		//assert(false);
		return;  			}
	l11: {
			goto l4;
		//assert(false);
		return;  			}
	l12: {
			__context->__RLO = (((! __context->__NFC) || __context->__RLO) && (MX0_2 || __context->__OR));
			goto l13;
		//assert(false);
		return;  			}
	l13: {
			__context->__STA = MX0_2;
			goto l14;
		//assert(false);
		return;  			}
	l14: {
			__context->__NFC = true;
			goto l15;
		//assert(false);
		return;  			}
	l15: {
			goto l5;
		//assert(false);
		return;  			}
	l16: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			jumps_fb1(&instance);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
void main() {
	// Initial values
	MX0_0 = false;
	MX0_1 = false;
	MX0_2 = false;
	instance.__RLO = false;
	instance.__NFC = false;
	instance.__BR = false;
	instance.__STA = false;
	instance.__OR = false;
	instance.__CC0 = false;
	instance.__CC1 = false;
	__assertion_error = 0;
	
	VerificationLoop();
}
