#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Root data structure
typedef struct {
	float real1;
	float real2;
	int16_t int1;
} __float_fc1;

// Global variables
__float_fc1 float_fc11;
uint16_t __assertion_error;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Automata declarations
void float_fc1(__float_fc1 *__context);
void VerificationLoop();

// Automata
void float_fc1(__float_fc1 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			__context->real1 = 12.3;
			goto l1;
		//assert(false);
		return;  			}
	l1: {
			__context->real1 = 23.0;
			goto l2;
		//assert(false);
		return;  			}
	l2: {
			__context->real1 = 34.0;
			goto l3;
		//assert(false);
		return;  			}
	l3: {
			__context->real1 = (45.6 + 7.0);
			goto l4;
		//assert(false);
		return;  			}
	l4: {
			__context->real1 = (__context->real1 + 8.0);
			goto l5;
		//assert(false);
		return;  			}
	l5: {
			__context->real1 = (91.2 + 92.3);
			goto l6;
		//assert(false);
		return;  			}
	l6: {
			__context->real1 = (__context->real1 + 92.4);
			goto l7;
		//assert(false);
		return;  			}
	l7: {
			__context->real1 = (__context->real1 + __context->real2);
			goto l8;
		//assert(false);
		return;  			}
	l8: {
			__context->real1 = (51.1 * 3.0);
			goto l9;
		//assert(false);
		return;  			}
	l9: {
			__context->real1 = (__context->real1 * 4.0);
			goto l10;
		//assert(false);
		return;  			}
	l10: {
			__context->real1 = (52.1 * 53.1);
			goto l11;
		//assert(false);
		return;  			}
	l11: {
			__context->real1 = (__context->real1 * 54.1);
			goto l12;
		//assert(false);
		return;  			}
	l12: {
			__context->real1 = (__context->real1 * __context->real2);
			goto l13;
		//assert(false);
		return;  			}
	l13: {
			__context->real1 = (60.1 / 20.3);
			goto l14;
		//assert(false);
		return;  			}
	l14: {
			__context->real1 = (__context->real1 / 20.4);
			goto l15;
		//assert(false);
		return;  			}
	l15: {
			__context->real1 = (__context->real1 / __context->real2);
			goto l16;
		//assert(false);
		return;  			}
	l16: {
			__context->real1 = (12.6 / 2.0);
			goto l17;
		//assert(false);
		return;  			}
	l17: {
			__context->real1 = (__context->real2 / 3.0);
			goto l18;
		//assert(false);
		return;  			}
	l18: {
			__context->real1 = (30.0 / 2.5);
			goto l19;
		//assert(false);
		return;  			}
	l19: {
			__context->real1 = (31.0 / __context->real2);
			goto l20;
		//assert(false);
		return;  			}
	l20: {
			__context->int1 = ((int16_t) __context->real1);
			goto l21;
		//assert(false);
		return;  			}
	l21: {
			__context->int1 = (((int16_t) __context->real1) + 1);
			goto l22;
		//assert(false);
		return;  			}
	l22: {
			__context->int1 = (((int16_t) __context->real1) * 2);
			goto l23;
		//assert(false);
		return;  			}
	l23: {
			__context->int1 = (((int16_t) __context->real1) * ((int16_t) __context->real2));
			goto l24;
		//assert(false);
		return;  			}
	l24: {
			__context->real1 = (((float) ((int16_t) __context->real2)) * 0.1);
			goto l25;
		//assert(false);
		return;  			}
	l25: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			float_fc1(&float_fc11);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
void main() {
	// Initial values
	float_fc11.real1 = 0.0;
	float_fc11.real2 = 0.0;
	float_fc11.int1 = 0;
	__assertion_error = 0;
	
	VerificationLoop();
}
