#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Root data structure
typedef struct {
	int16_t int1;
	int16_t int2;
	int32_t dint1;
	int32_t dint2;
	bool __RLO;
	bool __NFC;
	bool __BR;
	bool __STA;
	bool __OR;
	bool __CC0;
	bool __CC1;
	int32_t __ACCU1;
	int32_t __ACCU2;
} __compTest;

// Global variables
bool MX0_0;
__compTest instance;
uint16_t __assertion_error;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Automata declarations
void compTest(__compTest *__context);
void VerificationLoop();

// Automata
void compTest(__compTest *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			__context->__OR = false;
			__context->__STA = true;
			__context->__RLO = true;
			__context->__CC0 = false;
			__context->__CC1 = false;
			__context->__BR = false;
			__context->__NFC = false;
			goto l1;
		//assert(false);
		return;  			}
	l1: {
			__context->__ACCU2 = __context->__ACCU1;
			goto l2;
		//assert(false);
		return;  			}
	l2: {
			__context->__ACCU1 = ((int32_t) __context->int1);
			goto l3;
		//assert(false);
		return;  			}
	l3: {
			__context->__ACCU2 = __context->__ACCU1;
			goto l4;
		//assert(false);
		return;  			}
	l4: {
			__context->__ACCU1 = ((int32_t) __context->int2);
			goto l5;
		//assert(false);
		return;  			}
	l5: {
			__context->__RLO = (__context->__ACCU2 < __context->__ACCU1);
			goto l6;
		//assert(false);
		return;  			}
	l6: {
			__context->__CC0 = (__context->__ACCU1 > __context->__ACCU2);
			__context->__CC1 = (__context->__ACCU1 < __context->__ACCU2);
			__context->__OR = false;
			__context->__NFC = true;
			__context->__STA = __context->__RLO;
			goto l7;
		//assert(false);
		return;  			}
	l7: {
			__context->__RLO = (__context->__ACCU2 <= __context->__ACCU1);
			goto l8;
		//assert(false);
		return;  			}
	l8: {
			__context->__CC0 = (__context->__ACCU1 > __context->__ACCU2);
			__context->__CC1 = (__context->__ACCU1 < __context->__ACCU2);
			__context->__OR = false;
			__context->__NFC = true;
			__context->__STA = __context->__RLO;
			goto l9;
		//assert(false);
		return;  			}
	l9: {
			__context->__RLO = (__context->__ACCU2 >= __context->__ACCU1);
			goto l10;
		//assert(false);
		return;  			}
	l10: {
			__context->__CC0 = (__context->__ACCU1 > __context->__ACCU2);
			__context->__CC1 = (__context->__ACCU1 < __context->__ACCU2);
			__context->__OR = false;
			__context->__NFC = true;
			__context->__STA = __context->__RLO;
			goto l11;
		//assert(false);
		return;  			}
	l11: {
			__context->__RLO = (__context->__ACCU2 > __context->__ACCU1);
			goto l12;
		//assert(false);
		return;  			}
	l12: {
			__context->__CC0 = (__context->__ACCU1 > __context->__ACCU2);
			__context->__CC1 = (__context->__ACCU1 < __context->__ACCU2);
			__context->__OR = false;
			__context->__NFC = true;
			__context->__STA = __context->__RLO;
			goto l13;
		//assert(false);
		return;  			}
	l13: {
			__context->__RLO = (__context->__ACCU2 == __context->__ACCU1);
			goto l14;
		//assert(false);
		return;  			}
	l14: {
			__context->__CC0 = (__context->__ACCU1 > __context->__ACCU2);
			__context->__CC1 = (__context->__ACCU1 < __context->__ACCU2);
			__context->__OR = false;
			__context->__NFC = true;
			__context->__STA = __context->__RLO;
			goto l15;
		//assert(false);
		return;  			}
	l15: {
			__context->__RLO = (__context->__ACCU2 != __context->__ACCU1);
			goto l16;
		//assert(false);
		return;  			}
	l16: {
			__context->__CC0 = (__context->__ACCU1 > __context->__ACCU2);
			__context->__CC1 = (__context->__ACCU1 < __context->__ACCU2);
			__context->__OR = false;
			__context->__NFC = true;
			__context->__STA = __context->__RLO;
			goto l17;
		//assert(false);
		return;  			}
	l17: {
			__context->__ACCU2 = __context->__ACCU1;
			goto l18;
		//assert(false);
		return;  			}
	l18: {
			__context->__ACCU1 = __context->dint1;
			goto l19;
		//assert(false);
		return;  			}
	l19: {
			__context->__ACCU2 = __context->__ACCU1;
			goto l20;
		//assert(false);
		return;  			}
	l20: {
			__context->__ACCU1 = __context->dint2;
			goto l21;
		//assert(false);
		return;  			}
	l21: {
			__context->__RLO = (__context->__ACCU2 < __context->__ACCU1);
			goto l22;
		//assert(false);
		return;  			}
	l22: {
			__context->__CC0 = (__context->__ACCU1 > __context->__ACCU2);
			__context->__CC1 = (__context->__ACCU1 < __context->__ACCU2);
			__context->__OR = false;
			__context->__NFC = true;
			__context->__STA = __context->__RLO;
			goto l23;
		//assert(false);
		return;  			}
	l23: {
			__context->__RLO = (__context->__ACCU2 <= __context->__ACCU1);
			goto l24;
		//assert(false);
		return;  			}
	l24: {
			__context->__CC0 = (__context->__ACCU1 > __context->__ACCU2);
			__context->__CC1 = (__context->__ACCU1 < __context->__ACCU2);
			__context->__OR = false;
			__context->__NFC = true;
			__context->__STA = __context->__RLO;
			goto l25;
		//assert(false);
		return;  			}
	l25: {
			__context->__RLO = (__context->__ACCU2 >= __context->__ACCU1);
			goto l26;
		//assert(false);
		return;  			}
	l26: {
			__context->__CC0 = (__context->__ACCU1 > __context->__ACCU2);
			__context->__CC1 = (__context->__ACCU1 < __context->__ACCU2);
			__context->__OR = false;
			__context->__NFC = true;
			__context->__STA = __context->__RLO;
			goto l27;
		//assert(false);
		return;  			}
	l27: {
			__context->__RLO = (__context->__ACCU2 > __context->__ACCU1);
			goto l28;
		//assert(false);
		return;  			}
	l28: {
			__context->__CC0 = (__context->__ACCU1 > __context->__ACCU2);
			__context->__CC1 = (__context->__ACCU1 < __context->__ACCU2);
			__context->__OR = false;
			__context->__NFC = true;
			__context->__STA = __context->__RLO;
			goto l29;
		//assert(false);
		return;  			}
	l29: {
			__context->__RLO = (__context->__ACCU2 == __context->__ACCU1);
			goto l30;
		//assert(false);
		return;  			}
	l30: {
			__context->__CC0 = (__context->__ACCU1 > __context->__ACCU2);
			__context->__CC1 = (__context->__ACCU1 < __context->__ACCU2);
			__context->__OR = false;
			__context->__NFC = true;
			__context->__STA = __context->__RLO;
			goto l31;
		//assert(false);
		return;  			}
	l31: {
			__context->__RLO = (__context->__ACCU2 != __context->__ACCU1);
			goto l32;
		//assert(false);
		return;  			}
	l32: {
			__context->__CC0 = (__context->__ACCU1 > __context->__ACCU2);
			__context->__CC1 = (__context->__ACCU1 < __context->__ACCU2);
			__context->__OR = false;
			__context->__NFC = true;
			__context->__STA = __context->__RLO;
			goto l33;
		//assert(false);
		return;  			}
	l33: {
			MX0_0 = __context->__RLO;
			goto l34;
		//assert(false);
		return;  			}
	l34: {
			__context->__OR = false;
			goto l35;
		//assert(false);
		return;  			}
	l35: {
			__context->__STA = __context->__RLO;
			goto l36;
		//assert(false);
		return;  			}
	l36: {
			__context->__NFC = false;
			goto l37;
		//assert(false);
		return;  			}
	l37: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			instance.dint1 = nondet_int32_t();
			instance.dint2 = nondet_int32_t();
			instance.int1 = nondet_int16_t();
			instance.int2 = nondet_int16_t();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			compTest(&instance);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
void main() {
	// Initial values
	MX0_0 = false;
	instance.int1 = 0;
	instance.int2 = 0;
	instance.dint1 = 0;
	instance.dint2 = 0;
	instance.__RLO = false;
	instance.__NFC = false;
	instance.__BR = false;
	instance.__STA = false;
	instance.__OR = false;
	instance.__CC0 = false;
	instance.__CC1 = false;
	instance.__ACCU1 = 0;
	instance.__ACCU2 = 0;
	__assertion_error = 0;
	
	VerificationLoop();
}
