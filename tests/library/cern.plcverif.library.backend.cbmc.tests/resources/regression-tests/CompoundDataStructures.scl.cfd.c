#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Root data structure
typedef struct {
	bool x;
	bool y;
} __anonymous_type;
typedef struct {
	bool a;
} __anonymous_type1;
typedef struct {
	bool b;
	bool c;
} __anonymous_type2;
typedef struct {
	bool d;
	bool e;
} __anonymous_type4;
typedef struct {
	__anonymous_type4 s3a;
} __anonymous_type3;
typedef struct {
	bool s4arr[11];
} __anonymous_type41;
typedef struct {
	int16_t c1;
	bool c2;
} __compound_udt1;
typedef struct {
	bool in1;
	int16_t in2;
	bool arr1[4];
	int16_t arr2[4][8];
	__anonymous_type arr3[11];
	__compound_udt1 arr4[11];
	__anonymous_type1 s1;
	__anonymous_type2 s2;
	__anonymous_type3 s3;
	__anonymous_type41 s4;
	__compound_udt1 s5;
} __compound_fb1;

// Global variables
__compound_fb1 compound_db1;
__compound_udt1 compound_db2;
__compound_fb1 compound_db3;
__compound_fb1 compound_db1_inlined_1;
__compound_fb1 compound_db3_inlined_2;
uint16_t __assertion_error;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Automata declarations
void compound_OB1();
void compound_fb1(__compound_fb1 *__context);
void VerificationLoop();

// Automata
void compound_OB1() {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			// Assign inputs
			compound_fb1(&compound_db1_inlined_1);
			// Assign outputs
			goto l1;
		//assert(false);
		return;  			}
	l1: {
			// Assign inputs
			compound_fb1(&compound_db3_inlined_2);
			// Assign outputs
			goto l2;
		//assert(false);
		return;  			}
	l2: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void compound_fb1(__compound_fb1 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			__context->arr4[8].c1 = __context->in2;
			goto l11;
		//assert(false);
		return;  			}
	l11: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init2;
	init2: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			compound_OB1();
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
void main() {
	// Initial values
	compound_db1.in1 = false;
	compound_db1.in2 = 0;
	compound_db1.arr1[1] = false;
	compound_db1.arr1[2] = false;
	compound_db1.arr1[3] = false;
	compound_db1.arr2[1][5] = 0;
	compound_db1.arr2[1][6] = 0;
	compound_db1.arr2[1][7] = 0;
	compound_db1.arr2[2][5] = 0;
	compound_db1.arr2[2][6] = 0;
	compound_db1.arr2[2][7] = 0;
	compound_db1.arr2[3][5] = 0;
	compound_db1.arr2[3][6] = 0;
	compound_db1.arr2[3][7] = 0;
	compound_db1.s1.a = false;
	compound_db1.s2.b = false;
	compound_db1.s2.c = false;
	compound_db1.s3.s3a.d = false;
	compound_db1.s3.s3a.e = false;
	compound_db1.s4.s4arr[1] = false;
	compound_db1.s4.s4arr[2] = false;
	compound_db1.s4.s4arr[3] = false;
	compound_db1.s4.s4arr[4] = false;
	compound_db1.s4.s4arr[5] = false;
	compound_db1.s4.s4arr[6] = false;
	compound_db1.s4.s4arr[7] = false;
	compound_db1.s4.s4arr[8] = false;
	compound_db1.s4.s4arr[9] = false;
	compound_db1.s4.s4arr[10] = false;
	compound_db1.s5.c1 = 0;
	compound_db1.s5.c2 = false;
	compound_db2.c1 = 333;
	compound_db2.c2 = true;
	compound_db3.in2 = 987;
	compound_db3.in1 = false;
	compound_db3.arr1[1] = false;
	compound_db3.arr1[2] = false;
	compound_db3.arr1[3] = false;
	compound_db3.arr2[1][5] = 0;
	compound_db3.arr2[1][6] = 0;
	compound_db3.arr2[1][7] = 0;
	compound_db3.arr2[2][5] = 0;
	compound_db3.arr2[2][6] = 0;
	compound_db3.arr2[2][7] = 0;
	compound_db3.arr2[3][5] = 0;
	compound_db3.arr2[3][6] = 0;
	compound_db3.arr2[3][7] = 0;
	compound_db3.s1.a = false;
	compound_db3.s2.b = false;
	compound_db3.s2.c = false;
	compound_db3.s3.s3a.d = false;
	compound_db3.s3.s3a.e = false;
	compound_db3.s4.s4arr[1] = false;
	compound_db3.s4.s4arr[2] = false;
	compound_db3.s4.s4arr[3] = false;
	compound_db3.s4.s4arr[4] = false;
	compound_db3.s4.s4arr[5] = false;
	compound_db3.s4.s4arr[6] = false;
	compound_db3.s4.s4arr[7] = false;
	compound_db3.s4.s4arr[8] = false;
	compound_db3.s4.s4arr[9] = false;
	compound_db3.s4.s4arr[10] = false;
	compound_db3.s5.c1 = 0;
	compound_db3.s5.c2 = false;
	compound_db1_inlined_1.in1 = false;
	compound_db1_inlined_1.in2 = 0;
	compound_db1_inlined_1.arr1[1] = false;
	compound_db1_inlined_1.arr1[2] = false;
	compound_db1_inlined_1.arr1[3] = false;
	compound_db1_inlined_1.arr2[1][5] = 0;
	compound_db1_inlined_1.arr2[1][6] = 0;
	compound_db1_inlined_1.arr2[1][7] = 0;
	compound_db1_inlined_1.arr2[2][5] = 0;
	compound_db1_inlined_1.arr2[2][6] = 0;
	compound_db1_inlined_1.arr2[2][7] = 0;
	compound_db1_inlined_1.arr2[3][5] = 0;
	compound_db1_inlined_1.arr2[3][6] = 0;
	compound_db1_inlined_1.arr2[3][7] = 0;
	compound_db1_inlined_1.s1.a = false;
	compound_db1_inlined_1.s2.b = false;
	compound_db1_inlined_1.s2.c = false;
	compound_db1_inlined_1.s3.s3a.d = false;
	compound_db1_inlined_1.s3.s3a.e = false;
	compound_db1_inlined_1.s4.s4arr[1] = false;
	compound_db1_inlined_1.s4.s4arr[2] = false;
	compound_db1_inlined_1.s4.s4arr[3] = false;
	compound_db1_inlined_1.s4.s4arr[4] = false;
	compound_db1_inlined_1.s4.s4arr[5] = false;
	compound_db1_inlined_1.s4.s4arr[6] = false;
	compound_db1_inlined_1.s4.s4arr[7] = false;
	compound_db1_inlined_1.s4.s4arr[8] = false;
	compound_db1_inlined_1.s4.s4arr[9] = false;
	compound_db1_inlined_1.s4.s4arr[10] = false;
	compound_db1_inlined_1.s5.c1 = 0;
	compound_db1_inlined_1.s5.c2 = false;
	compound_db3_inlined_2.in2 = 987;
	compound_db3_inlined_2.in1 = false;
	compound_db3_inlined_2.arr1[1] = false;
	compound_db3_inlined_2.arr1[2] = false;
	compound_db3_inlined_2.arr1[3] = false;
	compound_db3_inlined_2.arr2[1][5] = 0;
	compound_db3_inlined_2.arr2[1][6] = 0;
	compound_db3_inlined_2.arr2[1][7] = 0;
	compound_db3_inlined_2.arr2[2][5] = 0;
	compound_db3_inlined_2.arr2[2][6] = 0;
	compound_db3_inlined_2.arr2[2][7] = 0;
	compound_db3_inlined_2.arr2[3][5] = 0;
	compound_db3_inlined_2.arr2[3][6] = 0;
	compound_db3_inlined_2.arr2[3][7] = 0;
	compound_db3_inlined_2.s1.a = false;
	compound_db3_inlined_2.s2.b = false;
	compound_db3_inlined_2.s2.c = false;
	compound_db3_inlined_2.s3.s3a.d = false;
	compound_db3_inlined_2.s3.s3a.e = false;
	compound_db3_inlined_2.s4.s4arr[1] = false;
	compound_db3_inlined_2.s4.s4arr[2] = false;
	compound_db3_inlined_2.s4.s4arr[3] = false;
	compound_db3_inlined_2.s4.s4arr[4] = false;
	compound_db3_inlined_2.s4.s4arr[5] = false;
	compound_db3_inlined_2.s4.s4arr[6] = false;
	compound_db3_inlined_2.s4.s4arr[7] = false;
	compound_db3_inlined_2.s4.s4arr[8] = false;
	compound_db3_inlined_2.s4.s4arr[9] = false;
	compound_db3_inlined_2.s4.s4arr[10] = false;
	compound_db3_inlined_2.s5.c1 = 0;
	compound_db3_inlined_2.s5.c2 = false;
	__assertion_error = 0;
	
	VerificationLoop();
}
