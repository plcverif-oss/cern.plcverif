#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Root data structure
typedef struct {
	int16_t a[3];
	int16_t b[3];
} __pv186;

// Global variables
__pv186 instance;
uint16_t __assertion_error;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Automata declarations
void pv186(__pv186 *__context);
void VerificationLoop();

// Automata
void pv186(__pv186 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			__context->b[1] = __context->a[1];
			goto l1;
		//assert(false);
		return;  			}
	l1: {
			memmove(&(__context->b), &(__context->a), sizeof __context->b);
			goto l2;
		//assert(false);
		return;  			}
	l2: {
		if ((! (__context->a[1] == __context->b[1]))) {
			__assertion_error = 1;
			goto end;
		}
		if ((! (! (__context->a[1] == __context->b[1])))) {
			goto end;
		}
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			goto loop_start;
		//assert(false);
		return;  			}
	end1: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end1;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			pv186(&instance);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
void main() {
	// Initial values
	instance.a[1] = 1;
	instance.a[2] = 2;
	instance.b[1] = 0;
	instance.b[2] = 0;
	__assertion_error = 0;
	
	VerificationLoop();
}
