#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Variables
bool R_EDGE_new = false;
bool R_EDGE_old = false;
bool R_EDGE_RET_VAL = false;
bool instance_HFOn = false;
bool instance_HFOff = false;
bool instance_IOError = false;
bool instance_IOSimu = false;
uint16_t instance_Manreg01 = 0;
uint16_t instance_PLocal_ParReg = 0;
uint16_t instance_Stsreg01 = 0;
bool instance_OnSt = false;
bool instance_OffSt = false;
bool instance_IOErrorW = false;
bool instance_IOSimuW = false;
bool instance_PosAlSt = false;
bool instance_PosW = false;
bool instance_E_PosAlSt = false;
bool instance_PosAlSt_old = false;
bool instance_PHFOn = false;
bool instance_PHFOff = false;
bool instance_PPosAlE = false;
bool instance_PAnim = false;
uint16_t __assertion_error = 0;
bool instance_Manreg01b_0 = false;
bool instance_Manreg01b_1 = false;
bool instance_Manreg01b_2 = false;
bool instance_Manreg01b_3 = false;
bool instance_Manreg01b_4 = false;
bool instance_Manreg01b_5 = false;
bool instance_Manreg01b_6 = false;
bool instance_Manreg01b_7 = false;
bool instance_Manreg01b_8 = false;
bool instance_Manreg01b_9 = false;
bool instance_Manreg01b_10 = false;
bool instance_Manreg01b_11 = false;
bool instance_Manreg01b_12 = false;
bool instance_Manreg01b_13 = false;
bool instance_Manreg01b_14 = false;
bool instance_Manreg01b_15 = false;
bool instance_PLocalb_ParRegb_0 = false;
bool instance_PLocalb_ParRegb_1 = false;
bool instance_PLocalb_ParRegb_2 = false;
bool instance_PLocalb_ParRegb_3 = false;
bool instance_PLocalb_ParRegb_4 = false;
bool instance_PLocalb_ParRegb_5 = false;
bool instance_PLocalb_ParRegb_6 = false;
bool instance_PLocalb_ParRegb_7 = false;
bool instance_PLocalb_ParRegb_8 = false;
bool instance_PLocalb_ParRegb_9 = false;
bool instance_PLocalb_ParRegb_10 = false;
bool instance_PLocalb_ParRegb_11 = false;
bool instance_PLocalb_ParRegb_12 = false;
bool instance_PLocalb_ParRegb_13 = false;
bool instance_PLocalb_ParRegb_14 = false;
bool instance_PLocalb_ParRegb_15 = false;
bool instance_Stsreg01b_0 = false;
bool instance_Stsreg01b_1 = false;
bool instance_Stsreg01b_2 = false;
bool instance_Stsreg01b_3 = false;
bool instance_Stsreg01b_4 = false;
bool instance_Stsreg01b_5 = false;
bool instance_Stsreg01b_6 = false;
bool instance_Stsreg01b_7 = false;
bool instance_Stsreg01b_8 = false;
bool instance_Stsreg01b_9 = false;
bool instance_Stsreg01b_10 = false;
bool instance_Stsreg01b_11 = false;
bool instance_Stsreg01b_12 = false;
bool instance_Stsreg01b_13 = false;
bool instance_Stsreg01b_14 = false;
bool instance_Stsreg01b_15 = false;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			instance_HFOff = nondet_bool();
			instance_HFOn = nondet_bool();
			instance_IOError = nondet_bool();
			instance_IOSimu = nondet_bool();
			instance_Manreg01 = nondet_uint16_t();
			instance_Manreg01b_0 = nondet_bool();
			instance_Manreg01b_10 = nondet_bool();
			instance_Manreg01b_11 = nondet_bool();
			instance_Manreg01b_12 = nondet_bool();
			instance_Manreg01b_13 = nondet_bool();
			instance_Manreg01b_14 = nondet_bool();
			instance_Manreg01b_15 = nondet_bool();
			instance_Manreg01b_1 = nondet_bool();
			instance_Manreg01b_2 = nondet_bool();
			instance_Manreg01b_3 = nondet_bool();
			instance_Manreg01b_4 = nondet_bool();
			instance_Manreg01b_5 = nondet_bool();
			instance_Manreg01b_6 = nondet_bool();
			instance_Manreg01b_7 = nondet_bool();
			instance_Manreg01b_8 = nondet_bool();
			instance_Manreg01b_9 = nondet_bool();
			instance_PLocal_ParReg = nondet_uint16_t();
			instance_PLocalb_ParRegb_0 = nondet_bool();
			instance_PLocalb_ParRegb_10 = nondet_bool();
			instance_PLocalb_ParRegb_11 = nondet_bool();
			instance_PLocalb_ParRegb_12 = nondet_bool();
			instance_PLocalb_ParRegb_13 = nondet_bool();
			instance_PLocalb_ParRegb_14 = nondet_bool();
			instance_PLocalb_ParRegb_15 = nondet_bool();
			instance_PLocalb_ParRegb_1 = nondet_bool();
			instance_PLocalb_ParRegb_2 = nondet_bool();
			instance_PLocalb_ParRegb_3 = nondet_bool();
			instance_PLocalb_ParRegb_4 = nondet_bool();
			instance_PLocalb_ParRegb_5 = nondet_bool();
			instance_PLocalb_ParRegb_6 = nondet_bool();
			instance_PLocalb_ParRegb_7 = nondet_bool();
			instance_PLocalb_ParRegb_8 = nondet_bool();
			instance_PLocalb_ParRegb_9 = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			instance_Manreg01b_0 = ((instance_Manreg01 & 256) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			instance_PHFOff = instance_PLocalb_ParRegb_10;
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
			instance_PPosAlE = instance_PLocalb_ParRegb_2;
			goto verificationLoop_VerificationLoop_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l3: {
			instance_PAnim = instance_PLocalb_ParRegb_14;
			goto verificationLoop_VerificationLoop_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l4: {
			instance_OnSt = ((instance_HFOn && instance_PHFOn) || (((! instance_PHFOn) && instance_PAnim) && (! instance_HFOff)));
			goto verificationLoop_VerificationLoop_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l5: {
			instance_OffSt = ((instance_HFOff && instance_PHFOff) || (((! instance_PHFOff) && instance_PAnim) && (! instance_HFOn)));
			goto verificationLoop_VerificationLoop_l6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l6: {
			instance_IOErrorW = instance_IOError;
			goto verificationLoop_VerificationLoop_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l7: {
			instance_IOSimuW = instance_IOSimu;
			goto verificationLoop_VerificationLoop_l8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l8: {
			instance_PosW = (((instance_OnSt == instance_OffSt) && instance_PHFOn) && instance_PHFOff);
			goto verificationLoop_VerificationLoop_l9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l9: {
			instance_PosAlSt = ((instance_PPosAlE && (instance_PHFOn != instance_PHFOff)) && ((instance_PHFOn && (! instance_HFOn)) || (instance_PHFOff && (! instance_HFOff))));
			goto verificationLoop_VerificationLoop_l10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l10: {
			R_EDGE_new = instance_PosAlSt;
			R_EDGE_old = instance_PosAlSt_old;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_LOCAL_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l11: {
			instance_Stsreg01b_8 = instance_OnSt;
			goto verificationLoop_VerificationLoop_x1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l12: {
			instance_Stsreg01b_9 = instance_OffSt;
			goto verificationLoop_VerificationLoop_x2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l13: {
			instance_Stsreg01b_10 = false;
			goto verificationLoop_VerificationLoop_x3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l14: {
			instance_Stsreg01b_11 = false;
			goto verificationLoop_VerificationLoop_x4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l15: {
			instance_Stsreg01b_12 = false;
			goto verificationLoop_VerificationLoop_x5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l16: {
			instance_Stsreg01b_13 = false;
			goto verificationLoop_VerificationLoop_x6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l17: {
			instance_Stsreg01b_14 = instance_IOError;
			goto verificationLoop_VerificationLoop_x7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l18: {
			instance_Stsreg01b_15 = instance_IOSimu;
			goto verificationLoop_VerificationLoop_x8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l19: {
			instance_Stsreg01b_0 = false;
			goto verificationLoop_VerificationLoop_x9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l20: {
			instance_Stsreg01b_1 = instance_PosW;
			goto verificationLoop_VerificationLoop_x10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l21: {
			instance_Stsreg01b_2 = instance_PosAlSt;
			goto verificationLoop_VerificationLoop_x11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l22: {
			instance_Stsreg01b_3 = false;
			goto verificationLoop_VerificationLoop_x12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l23: {
			instance_Stsreg01b_4 = false;
			goto verificationLoop_VerificationLoop_x13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l24: {
			instance_Stsreg01b_5 = false;
			goto verificationLoop_VerificationLoop_x14;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l25: {
			instance_Stsreg01b_6 = false;
			goto verificationLoop_VerificationLoop_x15;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l26: {
			instance_Stsreg01b_7 = false;
			goto verificationLoop_VerificationLoop_x16;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l27: {
			goto callEnd;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x: {
			instance_PHFOn = instance_PLocalb_ParRegb_9;
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh: {
			instance_PLocalb_ParRegb_0 = ((instance_PLocal_ParReg & 256) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh17;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh1: {
			instance_Manreg01b_1 = ((instance_Manreg01 & 512) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh2: {
			instance_Manreg01b_2 = ((instance_Manreg01 & 1024) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh3: {
			instance_Manreg01b_3 = ((instance_Manreg01 & 2048) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh4: {
			instance_Manreg01b_4 = ((instance_Manreg01 & 4096) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh5: {
			instance_Manreg01b_5 = ((instance_Manreg01 & 8192) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh6: {
			instance_Manreg01b_6 = ((instance_Manreg01 & 16384) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh7: {
			instance_Manreg01b_7 = ((instance_Manreg01 & 32768) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh8: {
			instance_Manreg01b_8 = ((instance_Manreg01 & 1) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh9: {
			instance_Manreg01b_9 = ((instance_Manreg01 & 2) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh10: {
			instance_Manreg01b_10 = ((instance_Manreg01 & 4) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh11: {
			instance_Manreg01b_11 = ((instance_Manreg01 & 8) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh12: {
			instance_Manreg01b_12 = ((instance_Manreg01 & 16) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh13: {
			instance_Manreg01b_13 = ((instance_Manreg01 & 32) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh14;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh14: {
			instance_Manreg01b_14 = ((instance_Manreg01 & 64) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh15;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh15: {
			instance_Manreg01b_15 = ((instance_Manreg01 & 128) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh16: {
			instance_Stsreg01b_0 = ((instance_Stsreg01 & 256) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh32;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh17: {
			instance_PLocalb_ParRegb_1 = ((instance_PLocal_ParReg & 512) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh18;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh18: {
			instance_PLocalb_ParRegb_2 = ((instance_PLocal_ParReg & 1024) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh19;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh19: {
			instance_PLocalb_ParRegb_3 = ((instance_PLocal_ParReg & 2048) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh20;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh20: {
			instance_PLocalb_ParRegb_4 = ((instance_PLocal_ParReg & 4096) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh21: {
			instance_PLocalb_ParRegb_5 = ((instance_PLocal_ParReg & 8192) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh22: {
			instance_PLocalb_ParRegb_6 = ((instance_PLocal_ParReg & 16384) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh23;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh23: {
			instance_PLocalb_ParRegb_7 = ((instance_PLocal_ParReg & 32768) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh24;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh24: {
			instance_PLocalb_ParRegb_8 = ((instance_PLocal_ParReg & 1) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh25;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh25: {
			instance_PLocalb_ParRegb_9 = ((instance_PLocal_ParReg & 2) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh26;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh26: {
			instance_PLocalb_ParRegb_10 = ((instance_PLocal_ParReg & 4) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh27;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh27: {
			instance_PLocalb_ParRegb_11 = ((instance_PLocal_ParReg & 8) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh28;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh28: {
			instance_PLocalb_ParRegb_12 = ((instance_PLocal_ParReg & 16) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh29;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh29: {
			instance_PLocalb_ParRegb_13 = ((instance_PLocal_ParReg & 32) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh30;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh30: {
			instance_PLocalb_ParRegb_14 = ((instance_PLocal_ParReg & 64) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh31;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh31: {
			instance_PLocalb_ParRegb_15 = ((instance_PLocal_ParReg & 128) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh16;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh32: {
			instance_Stsreg01b_1 = ((instance_Stsreg01 & 512) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh33;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh33: {
			instance_Stsreg01b_2 = ((instance_Stsreg01 & 1024) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh34;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh34: {
			instance_Stsreg01b_3 = ((instance_Stsreg01 & 2048) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh35;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh35: {
			instance_Stsreg01b_4 = ((instance_Stsreg01 & 4096) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh36;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh36: {
			instance_Stsreg01b_5 = ((instance_Stsreg01 & 8192) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh37;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh37: {
			instance_Stsreg01b_6 = ((instance_Stsreg01 & 16384) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh38;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh38: {
			instance_Stsreg01b_7 = ((instance_Stsreg01 & 32768) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh39;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh39: {
			instance_Stsreg01b_8 = ((instance_Stsreg01 & 1) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh40;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh40: {
			instance_Stsreg01b_9 = ((instance_Stsreg01 & 2) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh41;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh41: {
			instance_Stsreg01b_10 = ((instance_Stsreg01 & 4) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh42;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh42: {
			instance_Stsreg01b_11 = ((instance_Stsreg01 & 8) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh43;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh43: {
			instance_Stsreg01b_12 = ((instance_Stsreg01 & 16) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh44;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh44: {
			instance_Stsreg01b_13 = ((instance_Stsreg01 & 32) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh45;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh45: {
			instance_Stsreg01b_14 = ((instance_Stsreg01 & 64) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh46;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh46: {
			instance_Stsreg01b_15 = ((instance_Stsreg01 & 128) != 0);
			goto verificationLoop_VerificationLoop_x;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x1: {
		if (instance_Stsreg01b_8) {
			instance_Stsreg01 = (instance_Stsreg01 | 1);
			goto verificationLoop_VerificationLoop_l12;
		}
		if ((! instance_Stsreg01b_8)) {
			instance_Stsreg01 = (instance_Stsreg01 & 65534);
			goto verificationLoop_VerificationLoop_l12;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x2: {
		if (instance_Stsreg01b_9) {
			instance_Stsreg01 = (instance_Stsreg01 | 2);
			goto verificationLoop_VerificationLoop_l13;
		}
		if ((! instance_Stsreg01b_9)) {
			instance_Stsreg01 = (instance_Stsreg01 & 65533);
			goto verificationLoop_VerificationLoop_l13;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x3: {
		if (instance_Stsreg01b_10) {
			instance_Stsreg01 = (instance_Stsreg01 | 4);
			goto verificationLoop_VerificationLoop_l14;
		}
		if ((! instance_Stsreg01b_10)) {
			instance_Stsreg01 = (instance_Stsreg01 & 65531);
			goto verificationLoop_VerificationLoop_l14;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x4: {
		if (instance_Stsreg01b_11) {
			instance_Stsreg01 = (instance_Stsreg01 | 8);
			goto verificationLoop_VerificationLoop_l15;
		}
		if ((! instance_Stsreg01b_11)) {
			instance_Stsreg01 = (instance_Stsreg01 & 65527);
			goto verificationLoop_VerificationLoop_l15;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x5: {
		if (instance_Stsreg01b_12) {
			instance_Stsreg01 = (instance_Stsreg01 | 16);
			goto verificationLoop_VerificationLoop_l16;
		}
		if ((! instance_Stsreg01b_12)) {
			instance_Stsreg01 = (instance_Stsreg01 & 65519);
			goto verificationLoop_VerificationLoop_l16;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x6: {
		if (instance_Stsreg01b_13) {
			instance_Stsreg01 = (instance_Stsreg01 | 32);
			goto verificationLoop_VerificationLoop_l17;
		}
		if ((! instance_Stsreg01b_13)) {
			instance_Stsreg01 = (instance_Stsreg01 & 65503);
			goto verificationLoop_VerificationLoop_l17;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x7: {
		if (instance_Stsreg01b_14) {
			instance_Stsreg01 = (instance_Stsreg01 | 64);
			goto verificationLoop_VerificationLoop_l18;
		}
		if ((! instance_Stsreg01b_14)) {
			instance_Stsreg01 = (instance_Stsreg01 & 65471);
			goto verificationLoop_VerificationLoop_l18;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x8: {
		if (instance_Stsreg01b_15) {
			instance_Stsreg01 = (instance_Stsreg01 | 128);
			goto verificationLoop_VerificationLoop_l19;
		}
		if ((! instance_Stsreg01b_15)) {
			instance_Stsreg01 = (instance_Stsreg01 & 65407);
			goto verificationLoop_VerificationLoop_l19;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x9: {
		if (instance_Stsreg01b_0) {
			instance_Stsreg01 = (instance_Stsreg01 | 256);
			goto verificationLoop_VerificationLoop_l20;
		}
		if ((! instance_Stsreg01b_0)) {
			instance_Stsreg01 = (instance_Stsreg01 & 65279);
			goto verificationLoop_VerificationLoop_l20;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x10: {
		if (instance_Stsreg01b_1) {
			instance_Stsreg01 = (instance_Stsreg01 | 512);
			goto verificationLoop_VerificationLoop_l21;
		}
		if ((! instance_Stsreg01b_1)) {
			instance_Stsreg01 = (instance_Stsreg01 & 65023);
			goto verificationLoop_VerificationLoop_l21;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x11: {
		if (instance_Stsreg01b_2) {
			instance_Stsreg01 = (instance_Stsreg01 | 1024);
			goto verificationLoop_VerificationLoop_l22;
		}
		if ((! instance_Stsreg01b_2)) {
			instance_Stsreg01 = (instance_Stsreg01 & 64511);
			goto verificationLoop_VerificationLoop_l22;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x12: {
		if (instance_Stsreg01b_3) {
			instance_Stsreg01 = (instance_Stsreg01 | 2048);
			goto verificationLoop_VerificationLoop_l23;
		}
		if ((! instance_Stsreg01b_3)) {
			instance_Stsreg01 = (instance_Stsreg01 & 63487);
			goto verificationLoop_VerificationLoop_l23;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x13: {
		if (instance_Stsreg01b_4) {
			instance_Stsreg01 = (instance_Stsreg01 | 4096);
			goto verificationLoop_VerificationLoop_l24;
		}
		if ((! instance_Stsreg01b_4)) {
			instance_Stsreg01 = (instance_Stsreg01 & 61439);
			goto verificationLoop_VerificationLoop_l24;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x14: {
		if (instance_Stsreg01b_5) {
			instance_Stsreg01 = (instance_Stsreg01 | 8192);
			goto verificationLoop_VerificationLoop_l25;
		}
		if ((! instance_Stsreg01b_5)) {
			instance_Stsreg01 = (instance_Stsreg01 & 57343);
			goto verificationLoop_VerificationLoop_l25;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x15: {
		if (instance_Stsreg01b_6) {
			instance_Stsreg01 = (instance_Stsreg01 | 16384);
			goto verificationLoop_VerificationLoop_l26;
		}
		if ((! instance_Stsreg01b_6)) {
			instance_Stsreg01 = (instance_Stsreg01 & 49151);
			goto verificationLoop_VerificationLoop_l26;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x16: {
		if (instance_Stsreg01b_7) {
			instance_Stsreg01 = (instance_Stsreg01 | 32768);
			goto verificationLoop_VerificationLoop_l27;
		}
		if ((! instance_Stsreg01b_7)) {
			instance_Stsreg01 = (instance_Stsreg01 & 32767);
			goto verificationLoop_VerificationLoop_l27;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_LOCAL_init: {
		if (((R_EDGE_new == true) && (R_EDGE_old == false))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_LOCAL_l1;
		}
		if ((! ((R_EDGE_new == true) && (R_EDGE_old == false)))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_LOCAL_l4;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_LOCAL_l1: {
			R_EDGE_RET_VAL = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_LOCAL_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_LOCAL_l2: {
			R_EDGE_old = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_LOCAL_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_LOCAL_l3: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_LOCAL_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_LOCAL_l4: {
			R_EDGE_RET_VAL = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_LOCAL_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_LOCAL_l5: {
			R_EDGE_old = R_EDGE_new;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_LOCAL_l6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_LOCAL_l6: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_LOCAL_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_LOCAL_l7: {
			instance_PosAlSt_old = R_EDGE_old;
			instance_E_PosAlSt = R_EDGE_RET_VAL;
			goto verificationLoop_VerificationLoop_l11;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
