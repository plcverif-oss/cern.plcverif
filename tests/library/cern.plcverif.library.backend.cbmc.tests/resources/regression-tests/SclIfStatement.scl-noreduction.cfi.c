#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Variables
bool MX1_1 = false;
bool MX1_2 = false;
bool MX1_3 = false;
bool MX1_4 = false;
bool MX2_1 = false;
bool MX2_2 = false;
bool MX2_3 = false;
bool MX2_4 = false;
bool MX2_5 = false;
bool MX3_1 = false;
bool MX3_2 = false;
bool MX3_3 = false;
bool MX3_4 = false;
bool MX3_5 = false;
bool MX3_6 = false;
bool MX3_7 = false;
bool MX4_1 = false;
bool MX4_2 = false;
bool MX4_3 = false;
bool MX4_4 = false;
bool MX4_5 = false;
bool MX4_6 = false;
bool MX4_7 = false;
bool MX4_0 = false;
bool MX5_1 = false;
bool MX5_2 = false;
bool MX5_3 = false;
bool MX5_4 = false;
bool MX5_5 = false;
bool MX5_6 = false;
bool MX5_7 = false;
uint16_t __assertion_error = 0;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_init1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_init2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l3: {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_init3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l4: {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_init4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l5: {
			goto callEnd;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_init: {
			MX1_1 = true;
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l1: {
		if (MX1_2) {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l2;
		}
		if ((! MX1_2)) {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l4;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l2: {
			MX1_3 = true;
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l3: {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l4: {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l5: {
			MX1_4 = true;
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l6: {
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_init1: {
			MX2_1 = true;
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l11: {
		if (MX2_2) {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l21;
		}
		if ((! MX2_2)) {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l41;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l21: {
			MX2_3 = true;
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l31;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l31: {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l61;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l41: {
			MX2_4 = true;
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l51;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l51: {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l61;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l61: {
			MX2_5 = true;
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l7: {
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_init2: {
			MX3_1 = true;
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l12: {
		if (MX3_2) {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l22;
		}
		if (((! MX3_2) && MX3_4)) {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l42;
		}
		if (((! MX3_2) && (! ((! MX3_2) && MX3_4)))) {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l62;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l22: {
			MX3_3 = true;
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l32;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l32: {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l42: {
			MX3_5 = true;
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l52: {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l62: {
			MX3_6 = true;
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l71;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l71: {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l8: {
			MX3_7 = true;
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l9: {
			goto verificationLoop_VerificationLoop_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_init3: {
			MX4_1 = true;
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l13: {
		if (MX4_2) {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l23;
		}
		if (((! MX4_2) && MX4_4)) {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l43;
		}
		if ((((! MX4_2) && (! MX4_4)) && MX4_6)) {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l63;
		}
		if (((! MX4_2) && ((! ((! MX4_2) && MX4_4)) && (! (((! MX4_2) && (! MX4_4)) && MX4_6))))) {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l81;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l23: {
			MX4_3 = true;
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l33;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l33: {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l43: {
			MX4_5 = true;
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l53;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l53: {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l63: {
			MX4_7 = true;
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l72;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l72: {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l81: {
			MX4_0 = true;
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l91;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l91: {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l10: {
			MX4_1 = true;
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l111;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l111: {
			goto verificationLoop_VerificationLoop_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_init4: {
			MX5_1 = true;
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l14;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l14: {
		if (MX5_2) {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l24;
		}
		if ((! MX5_2)) {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l44;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l24: {
			MX5_3 = true;
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l34;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l34: {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l101;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l44: {
			MX5_4 = true;
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l54;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l54: {
		if (MX5_5) {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l64;
		}
		if ((! MX5_5)) {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l82;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l64: {
			MX5_6 = true;
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l73;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l73: {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l92;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l82: {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l92;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l92: {
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l101;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l101: {
			MX5_7 = true;
			goto verificationLoop_VerificationLoop_ifFunction_OB1_l112;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_ifFunction_OB1_l112: {
			goto verificationLoop_VerificationLoop_l5;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
