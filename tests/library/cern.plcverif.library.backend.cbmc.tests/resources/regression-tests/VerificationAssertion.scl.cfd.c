#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Root data structure

// Global variables
bool MX1_1;
bool IX1_2;
bool MX1_3;
bool MX2_1;
bool IX2_2;
bool IX2_3;
bool MX2_4;
bool MX3_1;
bool IX3_2;
bool IX3_3;
bool IX3_4;
bool MX3_4;
bool MX4_1;
bool IX4_2;
bool IX4_3;
bool IX4_4;
bool MX4_5;
uint16_t __assertion_error;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Automata declarations
void assertFunction1();
void assertFunction2();
void assertFunction3();
void assertFunction4();
void assertFunction_OB1();
void VerificationLoop();

// Automata
void assertFunction1() {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			MX1_1 = true;
			goto l1;
		//assert(false);
		return;  			}
	l1: {
		if ((! IX1_2)) {
			__assertion_error = 1;
			goto l2;
		}
		if ((! (! IX1_2))) {
			MX1_3 = true;
			goto l2;
		}
		//assert(false);
		return;  			}
	l2: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void assertFunction2() {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			MX2_1 = true;
			goto l11;
		//assert(false);
		return;  			}
	l11: {
		if ((! (IX2_2 || IX2_3))) {
			__assertion_error = 2;
			goto l21;
		}
		if ((! (! (IX2_2 || IX2_3)))) {
			MX2_4 = true;
			goto l21;
		}
		//assert(false);
		return;  			}
	l21: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void assertFunction3() {
	// Temporary variables
	
	// Start with initial location
	goto init2;
	init2: {
			MX3_1 = true;
			goto l12;
		//assert(false);
		return;  			}
	l12: {
		if ((! ((IX3_2 || IX3_3) && IX3_4))) {
			__assertion_error = 3;
			goto l22;
		}
		if ((! (! ((IX3_2 || IX3_3) && IX3_4)))) {
			MX3_4 = true;
			goto l22;
		}
		//assert(false);
		return;  			}
	l22: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void assertFunction4() {
	// Temporary variables
	
	// Start with initial location
	goto init3;
	init3: {
			MX4_1 = true;
			goto l13;
		//assert(false);
		return;  			}
	l13: {
		if ((! (!(IX4_2) || (!(IX4_3) || IX4_4)))) {
			__assertion_error = 4;
			goto l23;
		}
		if ((! (! (!(IX4_2) || (!(IX4_3) || IX4_4))))) {
			MX4_5 = true;
			goto l23;
		}
		//assert(false);
		return;  			}
	l23: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void assertFunction_OB1() {
	// Temporary variables
	
	// Start with initial location
	goto init4;
	init4: {
			assertFunction1();
			goto l14;
		//assert(false);
		return;  			}
	l14: {
			assertFunction2();
			goto l24;
		//assert(false);
		return;  			}
	l24: {
			assertFunction3();
			goto l3;
		//assert(false);
		return;  			}
	l3: {
			assertFunction4();
			goto l4;
		//assert(false);
		return;  			}
	l4: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init5;
	init5: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			IX1_2 = nondet_bool();
			IX2_2 = nondet_bool();
			IX2_3 = nondet_bool();
			IX3_2 = nondet_bool();
			IX3_3 = nondet_bool();
			IX3_4 = nondet_bool();
			IX4_2 = nondet_bool();
			IX4_3 = nondet_bool();
			IX4_4 = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			assertFunction_OB1();
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
void main() {
	// Initial values
	MX1_1 = false;
	IX1_2 = false;
	MX1_3 = false;
	MX2_1 = false;
	IX2_2 = false;
	IX2_3 = false;
	MX2_4 = false;
	MX3_1 = false;
	IX3_2 = false;
	IX3_3 = false;
	IX3_4 = false;
	MX3_4 = false;
	MX4_1 = false;
	IX4_2 = false;
	IX4_3 = false;
	IX4_4 = false;
	MX4_5 = false;
	__assertion_error = 0;
	
	VerificationLoop();
}
