#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Root data structure
typedef struct {
	bool b;
	bool w;
} __call_FB5;
typedef struct {
	bool a;
	bool q;
	__call_FB5 c;
} __call_FB4;
typedef struct {
	bool in1;
} __call_FC2;
typedef struct {
	bool in1;
	int16_t in2;
	int16_t RET_VAL;
} __call_FC3;
typedef struct {
} __call_OB1;

// Global variables
bool IX0_0;
bool MX0_0;
bool MX1_0;
bool MX2_0;
bool MX3_0;
bool MX3_1;
bool MX3_2;
bool MX4_0;
__call_OB1 call_OB11;
__call_FC2 call_FC21;
__call_FC3 call_FC31;
__call_FB4 call_DB4;
uint16_t __assertion_error;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Automata declarations
void call_OB1(__call_OB1 *__context);
void call_FC1();
void call_FC2(__call_FC2 *__context);
void call_FC3(__call_FC3 *__context);
void call_FB4(__call_FB4 *__context);
void call_FB5(__call_FB5 *__context);
void VerificationLoop();

// Automata
void call_OB1(__call_OB1 *__context) {
	// Temporary variables
	int16_t r;
	
	// Start with initial location
	goto init;
	init: {
			r = 0;
			goto l1;
		//assert(false);
		return;  			}
	l1: {
			call_FC1();
			goto l2;
		//assert(false);
		return;  			}
	l2: {
			// Assign inputs
			call_FC21.in1 = true;
			call_FC2(&call_FC21);
			// Assign outputs
			goto l3;
		//assert(false);
		return;  			}
	l3: {
			// Assign inputs
			call_FC31.in1 = IX0_0;
			call_FC31.in2 = 100;
			call_FC3(&call_FC31);
			// Assign outputs
			r = call_FC31.RET_VAL;
			goto l4;
		//assert(false);
		return;  			}
	l4: {
			call_DB4.a = true;
			goto l5;
		//assert(false);
		return;  			}
	l5: {
			// Assign inputs
			call_FB4(&call_DB4);
			// Assign outputs
			goto l6;
		//assert(false);
		return;  			}
	l6: {
			MX0_0 = call_DB4.q;
			goto l7;
		//assert(false);
		return;  			}
	l7: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void call_FC1() {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			MX1_0 = true;
			goto l11;
		//assert(false);
		return;  			}
	l11: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void call_FC2(__call_FC2 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init2;
	init2: {
			MX2_0 = (! __context->in1);
			goto l12;
		//assert(false);
		return;  			}
	l12: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void call_FC3(__call_FC3 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init3;
	init3: {
		if ((__context->in2 > 100)) {
			MX3_0 = true;
			goto l51;
		}
		if ((! (__context->in2 > 100))) {
			MX3_1 = false;
			goto l51;
		}
		//assert(false);
		return;  			}
	l51: {
			MX3_2 = true;
			__context->RET_VAL = __context->in2;
			goto l71;
		//assert(false);
		return;  			}
	l71: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void call_FB4(__call_FB4 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init4;
	init4: {
			__context->q = (! __context->a);
			__context->c.b = __context->a;
			goto l21;
		//assert(false);
		return;  			}
	l21: {
			// Assign inputs
			call_FB5(&__context->c);
			// Assign outputs
			goto l31;
		//assert(false);
		return;  			}
	l31: {
			MX4_0 = __context->c.w;
			goto l41;
		//assert(false);
		return;  			}
	l41: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void call_FB5(__call_FB5 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init5;
	init5: {
			__context->w = __context->b;
			goto l13;
		//assert(false);
		return;  			}
	l13: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init6;
	init6: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			IX0_0 = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			call_OB1(&call_OB11);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
void main() {
	// Initial values
	IX0_0 = false;
	MX0_0 = false;
	MX1_0 = false;
	MX2_0 = false;
	MX3_0 = false;
	MX3_1 = false;
	MX3_2 = false;
	MX4_0 = false;
	call_FC21.in1 = false;
	call_FC31.in1 = false;
	call_FC31.in2 = 0;
	call_FC31.RET_VAL = 0;
	call_DB4.a = false;
	call_DB4.q = false;
	call_DB4.c.b = false;
	call_DB4.c.w = false;
	__assertion_error = 0;
	
	VerificationLoop();
}
