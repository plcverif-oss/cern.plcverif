#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Variables
uint8_t T_CYCLE = 0;
int32_t __GLOBAL_TIME = 0;
bool R_EDGE_new = false;
bool R_EDGE_old = false;
bool R_EDGE_RET_VAL = false;
float ABS_REAL_in = 0.0;
float ABS_REAL_RET_VAL = 0.0;
bool DETECT_EDGE_new = false;
bool DETECT_EDGE_old = false;
bool DETECT_EDGE_re = false;
bool DETECT_EDGE_fe = false;
float instance_HFPos = 0.0;
float instance_HAOut = 0.0;
float instance_AuPosR = 0.0;
float instance_AuInSpd = 0.0;
float instance_AuDeSpd = 0.0;
float instance_MPosR = 0.0;
float instance_PliOff = 0.0;
float instance_PliOn = 0.0;
uint16_t instance_Manreg01 = 0;
bool instance_HFOn = false;
bool instance_HFOff = false;
bool instance_HLD = false;
bool instance_IOError = false;
bool instance_IOSimu = false;
bool instance_AlB = false;
bool instance_StartI = false;
bool instance_TStopI = false;
bool instance_FuStopI = false;
bool instance_Al = false;
bool instance_AuOnR = false;
bool instance_AuOffR = false;
bool instance_AuAuMoR = false;
bool instance_AuIhMMo = false;
bool instance_AuIhFoMo = false;
bool instance_AuAlAck = false;
bool instance_IhAuMRW = false;
bool instance_AuRstart = false;
uint16_t instance_PAnalog_ParReg = 0;
float instance_PAnalog_PMaxRan = 0.0;
float instance_PAnalog_PMinRan = 0.0;
float instance_PAnalog_PMStpInV = 0.0;
float instance_PAnalog_PMStpDeV = 0.0;
float instance_PAnalog_PMInSpd = 0.0;
float instance_PAnalog_PMDeSpd = 0.0;
int32_t instance_PAnalog_PWDt = 0;
float instance_PAnalog_PWDb = 0.0;
float instance_PAnalogb_PMaxRan = 0.0;
float instance_PAnalogb_PMinRan = 0.0;
float instance_PAnalogb_PMStpInV = 0.0;
float instance_PAnalogb_PMStpDeV = 0.0;
float instance_PAnalogb_PMInSpd = 0.0;
float instance_PAnalogb_PMDeSpd = 0.0;
int32_t instance_PAnalogb_PWDt = 0;
float instance_PAnalogb_PWDb = 0.0;
uint16_t instance_Stsreg01 = 0;
uint16_t instance_Stsreg02 = 0;
float instance_OutOV = 0.0;
float instance_PosSt = 0.0;
float instance_AuPosRSt = 0.0;
float instance_MPosRSt = 0.0;
float instance_PosRSt = 0.0;
bool instance_OnSt = false;
bool instance_OffSt = false;
bool instance_AuMoSt = false;
bool instance_MMoSt = false;
bool instance_LDSt = false;
bool instance_SoftLDSt = false;
bool instance_FoMoSt = false;
bool instance_IOErrorW = false;
bool instance_IOSimuW = false;
bool instance_AuMRW = false;
bool instance_PosW = false;
bool instance_StartISt = false;
bool instance_TStopISt = false;
bool instance_FuStopISt = false;
bool instance_AlSt = false;
bool instance_AlUnAck = false;
bool instance_AlBW = false;
bool instance_EnRstartSt = true;
bool instance_RdyStartSt = false;
bool instance_E_MAuMoR = false;
bool instance_E_MMMoR = false;
bool instance_E_MFoMoR = false;
bool instance_E_MOnR = false;
bool instance_E_MOffR = false;
bool instance_E_MAlAckR = false;
bool instance_E_StartI = false;
bool instance_E_TStopI = false;
bool instance_E_FuStopI = false;
bool instance_E_Al = false;
bool instance_E_AuAuMoR = false;
bool instance_E_AuAlAckR = false;
bool instance_E_MNewPosR = false;
bool instance_E_MStpInR = false;
bool instance_E_MStpDeR = false;
bool instance_E_MSoftLDR = false;
bool instance_E_MEnRstartR = false;
bool instance_RE_AlUnAck = false;
bool instance_FE_AlUnAck = false;
bool instance_MAuMoR_old = false;
bool instance_MMMoR_old = false;
bool instance_MFoMoR_old = false;
bool instance_MOnR_old = false;
bool instance_MOffR_old = false;
bool instance_MAlAckR_old = false;
bool instance_AuAuMoR_old = false;
bool instance_AuAlAckR_old = false;
bool instance_StartI_old = false;
bool instance_TStopI_old = false;
bool instance_FuStopI_old = false;
bool instance_Al_old = false;
bool instance_MNewPosR_old = false;
bool instance_MStpInR_old = false;
bool instance_MStpDeR_old = false;
bool instance_AlUnAck_old = false;
bool instance_MSoftLDR_old = false;
bool instance_MEnRstartR_old = false;
float instance_PosR = 0.0;
bool instance_PFsPosOn = false;
bool instance_PHFOn = false;
bool instance_PHFOff = false;
bool instance_PHFPos = false;
bool instance_PPulse = false;
bool instance_PHLD = false;
bool instance_PHLDCmd = false;
bool instance_PFsNOut = false;
bool instance_PEnRstart = false;
bool instance_PRstartFS = false;
bool instance_AuMoSt_aux = false;
bool instance_MMoSt_aux = false;
bool instance_FoMoSt_aux = false;
bool instance_SoftLDSt_aux = false;
bool instance_fullNotAcknowledged = false;
bool instance_InterlockR = false;
float instance_Ramp_parameters_inc_rate = 0.0;
float instance_Ramp_parameters_dec_rate = 0.0;
float instance_ROC_LIM_INV = 0.0;
float instance_ROC_LIM_UPRLM_P = 10.0;
float instance_ROC_LIM_DNRLM_P = 10.0;
float instance_ROC_LIM_UPRLM_N = 10.0;
float instance_ROC_LIM_DNRLM_N = 10.0;
float instance_ROC_LIM_H_LM = 100.0;
float instance_ROC_LIM_L_LM = 0.0;
float instance_ROC_LIM_PV = 0.0;
float instance_ROC_LIM_DF_OUTV = 0.0;
bool instance_ROC_LIM_DFOUT_ON = false;
bool instance_ROC_LIM_TRACK = false;
bool instance_ROC_LIM_MAN_ON = false;
bool instance_ROC_LIM_COM_RST = false;
int32_t instance_ROC_LIM_CYCLE = 1000;
float instance_ROC_LIM_OUTV = 0.0;
bool instance_ROC_LIM_QUPRLM_P = false;
bool instance_ROC_LIM_QDNRLM_P = false;
bool instance_ROC_LIM_QUPRLM_N = false;
bool instance_ROC_LIM_QDNRLM_N = false;
bool instance_ROC_LIM_QH_LM = false;
bool instance_ROC_LIM_QL_LM = false;
int32_t instance_Time_Warning = 0;
int32_t instance_Timer_Warning_PT = 0;
bool instance_Timer_Warning_IN = false;
bool instance_Timer_Warning_Q = false;
int32_t instance_Timer_Warning_ET = 0;
bool instance_Timer_Warning_running = false;
int32_t instance_Timer_Warning_start = 0;
float instance_PulseWidth = 0.0;
int16_t instance_FSIinc = 0;
int16_t instance_TSIinc = 0;
int16_t instance_SIinc = 0;
int16_t instance_Alinc = 0;
bool instance_WAlSt = false;
bool instance_WFuStopISt = false;
bool instance_WTStopISt = false;
bool instance_WStartISt = false;
float instance____nested_ret_val2 = 0.0;
float instance____nested_ret_val3 = 0.0;
float instance____nested_ret_val4 = 0.0;
float instance____nested_ret_val5 = 0.0;
bool R_EDGE_inlined_1_new = false;
bool R_EDGE_inlined_1_old = false;
bool R_EDGE_inlined_1_RET_VAL = false;
bool R_EDGE_inlined_2_new = false;
bool R_EDGE_inlined_2_old = false;
bool R_EDGE_inlined_2_RET_VAL = false;
bool R_EDGE_inlined_3_new = false;
bool R_EDGE_inlined_3_old = false;
bool R_EDGE_inlined_3_RET_VAL = false;
bool R_EDGE_inlined_4_new = false;
bool R_EDGE_inlined_4_old = false;
bool R_EDGE_inlined_4_RET_VAL = false;
bool R_EDGE_inlined_5_new = false;
bool R_EDGE_inlined_5_old = false;
bool R_EDGE_inlined_5_RET_VAL = false;
bool R_EDGE_inlined_6_new = false;
bool R_EDGE_inlined_6_old = false;
bool R_EDGE_inlined_6_RET_VAL = false;
bool R_EDGE_inlined_7_new = false;
bool R_EDGE_inlined_7_old = false;
bool R_EDGE_inlined_7_RET_VAL = false;
bool R_EDGE_inlined_8_new = false;
bool R_EDGE_inlined_8_old = false;
bool R_EDGE_inlined_8_RET_VAL = false;
bool R_EDGE_inlined_9_new = false;
bool R_EDGE_inlined_9_old = false;
bool R_EDGE_inlined_9_RET_VAL = false;
bool R_EDGE_inlined_10_new = false;
bool R_EDGE_inlined_10_old = false;
bool R_EDGE_inlined_10_RET_VAL = false;
bool R_EDGE_inlined_11_new = false;
bool R_EDGE_inlined_11_old = false;
bool R_EDGE_inlined_11_RET_VAL = false;
bool R_EDGE_inlined_12_new = false;
bool R_EDGE_inlined_12_old = false;
bool R_EDGE_inlined_12_RET_VAL = false;
bool R_EDGE_inlined_13_new = false;
bool R_EDGE_inlined_13_old = false;
bool R_EDGE_inlined_13_RET_VAL = false;
bool R_EDGE_inlined_14_new = false;
bool R_EDGE_inlined_14_old = false;
bool R_EDGE_inlined_14_RET_VAL = false;
bool R_EDGE_inlined_15_new = false;
bool R_EDGE_inlined_15_old = false;
bool R_EDGE_inlined_15_RET_VAL = false;
bool R_EDGE_inlined_16_new = false;
bool R_EDGE_inlined_16_old = false;
bool R_EDGE_inlined_16_RET_VAL = false;
bool R_EDGE_inlined_17_new = false;
bool R_EDGE_inlined_17_old = false;
bool R_EDGE_inlined_17_RET_VAL = false;
float ABS_REAL_inlined_18_in = 0.0;
float ABS_REAL_inlined_18_RET_VAL = 0.0;
float ABS_REAL_inlined_19_in = 0.0;
float ABS_REAL_inlined_19_RET_VAL = 0.0;
float ABS_REAL_inlined_20_in = 0.0;
float ABS_REAL_inlined_20_RET_VAL = 0.0;
float ABS_REAL_inlined_21_in = 0.0;
float ABS_REAL_inlined_21_RET_VAL = 0.0;
uint16_t __assertion_error = 0;
bool instance_Manreg01b_0 = false;
bool instance_Manreg01b_1 = false;
bool instance_Manreg01b_2 = false;
bool instance_Manreg01b_3 = false;
bool instance_Manreg01b_4 = false;
bool instance_Manreg01b_5 = false;
bool instance_Manreg01b_6 = false;
bool instance_Manreg01b_7 = false;
bool instance_Manreg01b_8 = false;
bool instance_Manreg01b_9 = false;
bool instance_Manreg01b_10 = false;
bool instance_Manreg01b_11 = false;
bool instance_Manreg01b_12 = false;
bool instance_Manreg01b_13 = false;
bool instance_Manreg01b_14 = false;
bool instance_Manreg01b_15 = false;
bool instance_PAnalogb_ParRegb_0 = false;
bool instance_PAnalogb_ParRegb_1 = false;
bool instance_PAnalogb_ParRegb_2 = false;
bool instance_PAnalogb_ParRegb_3 = false;
bool instance_PAnalogb_ParRegb_4 = false;
bool instance_PAnalogb_ParRegb_5 = false;
bool instance_PAnalogb_ParRegb_6 = false;
bool instance_PAnalogb_ParRegb_7 = false;
bool instance_PAnalogb_ParRegb_8 = false;
bool instance_PAnalogb_ParRegb_9 = false;
bool instance_PAnalogb_ParRegb_10 = false;
bool instance_PAnalogb_ParRegb_11 = false;
bool instance_PAnalogb_ParRegb_12 = false;
bool instance_PAnalogb_ParRegb_13 = false;
bool instance_PAnalogb_ParRegb_14 = false;
bool instance_PAnalogb_ParRegb_15 = false;
bool instance_Stsreg01b_0 = false;
bool instance_Stsreg01b_1 = false;
bool instance_Stsreg01b_2 = false;
bool instance_Stsreg01b_3 = false;
bool instance_Stsreg01b_4 = false;
bool instance_Stsreg01b_5 = false;
bool instance_Stsreg01b_6 = false;
bool instance_Stsreg01b_7 = false;
bool instance_Stsreg01b_8 = false;
bool instance_Stsreg01b_9 = false;
bool instance_Stsreg01b_10 = false;
bool instance_Stsreg01b_11 = false;
bool instance_Stsreg01b_12 = false;
bool instance_Stsreg01b_13 = false;
bool instance_Stsreg01b_14 = false;
bool instance_Stsreg01b_15 = false;
bool instance_Stsreg02b_0 = false;
bool instance_Stsreg02b_1 = false;
bool instance_Stsreg02b_2 = false;
bool instance_Stsreg02b_3 = false;
bool instance_Stsreg02b_4 = false;
bool instance_Stsreg02b_5 = false;
bool instance_Stsreg02b_6 = false;
bool instance_Stsreg02b_7 = false;
bool instance_Stsreg02b_8 = false;
bool instance_Stsreg02b_9 = false;
bool instance_Stsreg02b_10 = false;
bool instance_Stsreg02b_11 = false;
bool instance_Stsreg02b_12 = false;
bool instance_Stsreg02b_13 = false;
bool instance_Stsreg02b_14 = false;
bool instance_Stsreg02b_15 = false;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			T_CYCLE = nondet_uint8_t();
			instance_Al = nondet_bool();
			instance_AlB = nondet_bool();
			instance_AuAlAck = nondet_bool();
			instance_AuAuMoR = nondet_bool();
			instance_AuDeSpd = nondet_float();
			instance_AuIhFoMo = nondet_bool();
			instance_AuIhMMo = nondet_bool();
			instance_AuInSpd = nondet_float();
			instance_AuOffR = nondet_bool();
			instance_AuOnR = nondet_bool();
			instance_AuPosR = nondet_float();
			instance_AuRstart = nondet_bool();
			instance_FuStopI = nondet_bool();
			instance_HAOut = nondet_float();
			instance_HFOff = nondet_bool();
			instance_HFOn = nondet_bool();
			instance_HFPos = nondet_float();
			instance_HLD = nondet_bool();
			instance_IOError = nondet_bool();
			instance_IOSimu = nondet_bool();
			instance_IhAuMRW = nondet_bool();
			instance_MPosR = nondet_float();
			instance_Manreg01 = nondet_uint16_t();
			instance_Manreg01b_0 = nondet_bool();
			instance_Manreg01b_10 = nondet_bool();
			instance_Manreg01b_11 = nondet_bool();
			instance_Manreg01b_12 = nondet_bool();
			instance_Manreg01b_13 = nondet_bool();
			instance_Manreg01b_14 = nondet_bool();
			instance_Manreg01b_15 = nondet_bool();
			instance_Manreg01b_1 = nondet_bool();
			instance_Manreg01b_2 = nondet_bool();
			instance_Manreg01b_3 = nondet_bool();
			instance_Manreg01b_4 = nondet_bool();
			instance_Manreg01b_5 = nondet_bool();
			instance_Manreg01b_6 = nondet_bool();
			instance_Manreg01b_7 = nondet_bool();
			instance_Manreg01b_8 = nondet_bool();
			instance_Manreg01b_9 = nondet_bool();
			instance_PAnalog_PMDeSpd = nondet_float();
			instance_PAnalog_PMInSpd = nondet_float();
			instance_PAnalog_PMStpDeV = nondet_float();
			instance_PAnalog_PMStpInV = nondet_float();
			instance_PAnalog_PMaxRan = nondet_float();
			instance_PAnalog_PMinRan = nondet_float();
			instance_PAnalog_PWDb = nondet_float();
			instance_PAnalog_PWDt = nondet_int32_t();
			instance_PAnalog_ParReg = nondet_uint16_t();
			instance_PAnalogb_PMDeSpd = nondet_float();
			instance_PAnalogb_PMInSpd = nondet_float();
			instance_PAnalogb_PMStpDeV = nondet_float();
			instance_PAnalogb_PMStpInV = nondet_float();
			instance_PAnalogb_PMaxRan = nondet_float();
			instance_PAnalogb_PMinRan = nondet_float();
			instance_PAnalogb_PWDb = nondet_float();
			instance_PAnalogb_PWDt = nondet_int32_t();
			instance_PAnalogb_ParRegb_0 = nondet_bool();
			instance_PAnalogb_ParRegb_10 = nondet_bool();
			instance_PAnalogb_ParRegb_11 = nondet_bool();
			instance_PAnalogb_ParRegb_12 = nondet_bool();
			instance_PAnalogb_ParRegb_13 = nondet_bool();
			instance_PAnalogb_ParRegb_14 = nondet_bool();
			instance_PAnalogb_ParRegb_15 = nondet_bool();
			instance_PAnalogb_ParRegb_1 = nondet_bool();
			instance_PAnalogb_ParRegb_2 = nondet_bool();
			instance_PAnalogb_ParRegb_3 = nondet_bool();
			instance_PAnalogb_ParRegb_4 = nondet_bool();
			instance_PAnalogb_ParRegb_5 = nondet_bool();
			instance_PAnalogb_ParRegb_6 = nondet_bool();
			instance_PAnalogb_ParRegb_7 = nondet_bool();
			instance_PAnalogb_ParRegb_8 = nondet_bool();
			instance_PAnalogb_ParRegb_9 = nondet_bool();
			instance_PliOff = nondet_float();
			instance_PliOn = nondet_float();
			instance_StartI = nondet_bool();
			instance_TStopI = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			__GLOBAL_TIME = (__GLOBAL_TIME + ((int32_t) T_CYCLE));
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			instance_Manreg01b_0 = ((instance_Manreg01 & 256) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			R_EDGE_inlined_1_new = instance_Manreg01b_8;
			R_EDGE_inlined_1_old = instance_MAuMoR_old;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
			R_EDGE_inlined_2_new = instance_Manreg01b_9;
			R_EDGE_inlined_2_old = instance_MMMoR_old;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l3: {
			R_EDGE_inlined_3_new = instance_Manreg01b_10;
			R_EDGE_inlined_3_old = instance_MFoMoR_old;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l4: {
			R_EDGE_inlined_4_new = instance_Manreg01b_11;
			R_EDGE_inlined_4_old = instance_MSoftLDR_old;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l5: {
			R_EDGE_inlined_5_new = instance_Manreg01b_12;
			R_EDGE_inlined_5_old = instance_MOnR_old;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l6: {
			R_EDGE_inlined_6_new = instance_Manreg01b_13;
			R_EDGE_inlined_6_old = instance_MOffR_old;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l7: {
			R_EDGE_inlined_7_new = instance_Manreg01b_14;
			R_EDGE_inlined_7_old = instance_MNewPosR_old;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l8: {
			R_EDGE_inlined_8_new = instance_Manreg01b_15;
			R_EDGE_inlined_8_old = instance_MStpInR_old;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l9: {
			R_EDGE_inlined_9_new = instance_Manreg01b_0;
			R_EDGE_inlined_9_old = instance_MStpDeR_old;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l10: {
			R_EDGE_inlined_10_new = instance_Manreg01b_1;
			R_EDGE_inlined_10_old = instance_MEnRstartR_old;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l11: {
			R_EDGE_inlined_11_new = instance_Manreg01b_7;
			R_EDGE_inlined_11_old = instance_MAlAckR_old;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l12: {
			instance_PFsPosOn = instance_PAnalogb_ParRegb_8;
			goto verificationLoop_VerificationLoop_l13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l13: {
			instance_PHFOn = instance_PAnalogb_ParRegb_9;
			goto verificationLoop_VerificationLoop_l14;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l14: {
			instance_PHFOff = instance_PAnalogb_ParRegb_10;
			goto verificationLoop_VerificationLoop_l15;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l15: {
			instance_PHFPos = instance_PAnalogb_ParRegb_11;
			goto verificationLoop_VerificationLoop_l16;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l16: {
			instance_PHLD = instance_PAnalogb_ParRegb_12;
			goto verificationLoop_VerificationLoop_l17;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l17: {
			instance_PHLDCmd = instance_PAnalogb_ParRegb_13;
			goto verificationLoop_VerificationLoop_l18;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l18: {
			instance_PFsNOut = instance_PAnalogb_ParRegb_14;
			goto verificationLoop_VerificationLoop_l19;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l19: {
			instance_PEnRstart = instance_PAnalogb_ParRegb_0;
			goto verificationLoop_VerificationLoop_l20;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l20: {
			instance_PRstartFS = instance_PAnalogb_ParRegb_1;
			goto verificationLoop_VerificationLoop_l21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l21: {
			R_EDGE_inlined_12_new = instance_AuAuMoR;
			R_EDGE_inlined_12_old = instance_AuAuMoR_old;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l22: {
			R_EDGE_inlined_13_new = instance_AuAlAck;
			R_EDGE_inlined_13_old = instance_AuAlAckR_old;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l23: {
			R_EDGE_inlined_14_new = instance_StartI;
			R_EDGE_inlined_14_old = instance_StartI_old;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l24: {
			R_EDGE_inlined_15_new = instance_TStopI;
			R_EDGE_inlined_15_old = instance_TStopI_old;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init14;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l25: {
			R_EDGE_inlined_16_new = instance_FuStopI;
			R_EDGE_inlined_16_old = instance_FuStopI_old;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init15;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l26: {
			R_EDGE_inlined_17_new = instance_Al;
			R_EDGE_inlined_17_old = instance_Al_old;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init16;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l27: {
			instance_StartISt = instance_StartI;
			goto verificationLoop_VerificationLoop_l28;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l28: {
			instance_TStopISt = instance_TStopI;
			goto verificationLoop_VerificationLoop_l29;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l29: {
			instance_FuStopISt = instance_FuStopI;
			goto verificationLoop_VerificationLoop_l30;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l30: {
		if ((instance_E_MAlAckR || instance_E_AuAlAckR)) {
			goto verificationLoop_VerificationLoop_l31;
		}
		if (((! (instance_E_MAlAckR || instance_E_AuAlAckR)) && (((instance_E_TStopI || instance_E_StartI) || instance_E_FuStopI) || instance_E_Al))) {
			goto verificationLoop_VerificationLoop_l34;
		}
		if (((! (instance_E_MAlAckR || instance_E_AuAlAckR)) && (! ((! (instance_E_MAlAckR || instance_E_AuAlAckR)) && (((instance_E_TStopI || instance_E_StartI) || instance_E_FuStopI) || instance_E_Al))))) {
			goto verificationLoop_VerificationLoop_l36;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l31: {
			instance_fullNotAcknowledged = false;
			goto verificationLoop_VerificationLoop_l32;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l32: {
			instance_AlUnAck = false;
			goto verificationLoop_VerificationLoop_l33;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l33: {
			goto verificationLoop_VerificationLoop_l37;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l34: {
			instance_AlUnAck = true;
			goto verificationLoop_VerificationLoop_l35;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l35: {
			goto verificationLoop_VerificationLoop_l37;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l36: {
			goto verificationLoop_VerificationLoop_l37;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l37: {
		if (((((instance_PEnRstart && (instance_E_MEnRstartR || instance_AuRstart)) && (! instance_FuStopISt)) || ((instance_PEnRstart && instance_PRstartFS) && (instance_E_MEnRstartR || instance_AuRstart))) && (! instance_fullNotAcknowledged))) {
			goto verificationLoop_VerificationLoop_l38;
		}
		if ((! ((((instance_PEnRstart && (instance_E_MEnRstartR || instance_AuRstart)) && (! instance_FuStopISt)) || ((instance_PEnRstart && instance_PRstartFS) && (instance_E_MEnRstartR || instance_AuRstart))) && (! instance_fullNotAcknowledged)))) {
			goto verificationLoop_VerificationLoop_l40;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l38: {
			instance_EnRstartSt = true;
			goto verificationLoop_VerificationLoop_l39;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l39: {
			goto verificationLoop_VerificationLoop_l41;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l40: {
			goto verificationLoop_VerificationLoop_l41;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l41: {
			instance_InterlockR = (((((instance_TStopISt || instance_FuStopISt) || instance_fullNotAcknowledged) || (! instance_EnRstartSt)) || (((! instance_PFsPosOn) && instance_StartISt) && (instance_PosRSt <= instance_PAnalog_PMinRan))) || ((instance_PFsPosOn && instance_StartISt) && (instance_PosRSt >= instance_PAnalog_PMaxRan)));
			goto verificationLoop_VerificationLoop_l42;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l42: {
		if ((! (instance_HLD && instance_PHLD))) {
			goto verificationLoop_VerificationLoop_l43;
		}
		if ((! (! (instance_HLD && instance_PHLD)))) {
			goto verificationLoop_VerificationLoop_l77;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l43: {
		if (((((instance_AuMoSt_aux || instance_MMoSt_aux) || instance_SoftLDSt_aux) && instance_E_MFoMoR) && (! instance_AuIhFoMo))) {
			goto verificationLoop_VerificationLoop_l44;
		}
		if ((! ((((instance_AuMoSt_aux || instance_MMoSt_aux) || instance_SoftLDSt_aux) && instance_E_MFoMoR) && (! instance_AuIhFoMo)))) {
			goto verificationLoop_VerificationLoop_l49;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l44: {
			instance_AuMoSt_aux = false;
			goto verificationLoop_VerificationLoop_l45;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l45: {
			instance_MMoSt_aux = false;
			goto verificationLoop_VerificationLoop_l46;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l46: {
			instance_FoMoSt_aux = true;
			goto verificationLoop_VerificationLoop_l47;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l47: {
			instance_SoftLDSt_aux = false;
			goto verificationLoop_VerificationLoop_l48;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l48: {
			goto verificationLoop_VerificationLoop_l50;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l49: {
			goto verificationLoop_VerificationLoop_l50;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l50: {
		if (((((instance_AuMoSt_aux || instance_FoMoSt_aux) || instance_SoftLDSt_aux) && instance_E_MMMoR) && (! instance_AuIhMMo))) {
			goto verificationLoop_VerificationLoop_l51;
		}
		if ((! ((((instance_AuMoSt_aux || instance_FoMoSt_aux) || instance_SoftLDSt_aux) && instance_E_MMMoR) && (! instance_AuIhMMo)))) {
			goto verificationLoop_VerificationLoop_l56;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l51: {
			instance_AuMoSt_aux = false;
			goto verificationLoop_VerificationLoop_l52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l52: {
			instance_MMoSt_aux = true;
			goto verificationLoop_VerificationLoop_l53;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l53: {
			instance_FoMoSt_aux = false;
			goto verificationLoop_VerificationLoop_l54;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l54: {
			instance_SoftLDSt_aux = false;
			goto verificationLoop_VerificationLoop_l55;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l55: {
			goto verificationLoop_VerificationLoop_l57;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l56: {
			goto verificationLoop_VerificationLoop_l57;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l57: {
		if ((((((((instance_MMoSt_aux && (instance_E_MAuMoR || instance_E_AuAuMoR)) || (instance_FoMoSt_aux && instance_E_MAuMoR)) || (instance_SoftLDSt_aux && instance_E_MAuMoR)) || (instance_MMoSt_aux && instance_AuIhMMo)) || (instance_FoMoSt_aux && instance_AuIhFoMo)) || (instance_SoftLDSt_aux && instance_AuIhFoMo)) || (! (((instance_AuMoSt_aux || instance_MMoSt_aux) || instance_FoMoSt_aux) || instance_SoftLDSt_aux)))) {
			goto verificationLoop_VerificationLoop_l58;
		}
		if ((! (((((((instance_MMoSt_aux && (instance_E_MAuMoR || instance_E_AuAuMoR)) || (instance_FoMoSt_aux && instance_E_MAuMoR)) || (instance_SoftLDSt_aux && instance_E_MAuMoR)) || (instance_MMoSt_aux && instance_AuIhMMo)) || (instance_FoMoSt_aux && instance_AuIhFoMo)) || (instance_SoftLDSt_aux && instance_AuIhFoMo)) || (! (((instance_AuMoSt_aux || instance_MMoSt_aux) || instance_FoMoSt_aux) || instance_SoftLDSt_aux))))) {
			goto verificationLoop_VerificationLoop_l63;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l58: {
			instance_AuMoSt_aux = true;
			goto verificationLoop_VerificationLoop_l59;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l59: {
			instance_MMoSt_aux = false;
			goto verificationLoop_VerificationLoop_l60;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l60: {
			instance_FoMoSt_aux = false;
			goto verificationLoop_VerificationLoop_l61;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l61: {
			instance_SoftLDSt_aux = false;
			goto verificationLoop_VerificationLoop_l62;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l62: {
			goto verificationLoop_VerificationLoop_l64;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l63: {
			goto verificationLoop_VerificationLoop_l64;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l64: {
		if ((((instance_AuMoSt_aux || instance_MMoSt_aux) && instance_E_MSoftLDR) && (! instance_AuIhFoMo))) {
			goto verificationLoop_VerificationLoop_l65;
		}
		if ((! (((instance_AuMoSt_aux || instance_MMoSt_aux) && instance_E_MSoftLDR) && (! instance_AuIhFoMo)))) {
			goto verificationLoop_VerificationLoop_l70;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l65: {
			instance_AuMoSt_aux = false;
			goto verificationLoop_VerificationLoop_l66;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l66: {
			instance_MMoSt_aux = false;
			goto verificationLoop_VerificationLoop_l67;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l67: {
			instance_FoMoSt_aux = false;
			goto verificationLoop_VerificationLoop_l68;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l68: {
			instance_SoftLDSt_aux = true;
			goto verificationLoop_VerificationLoop_l69;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l69: {
			goto verificationLoop_VerificationLoop_l71;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l70: {
			goto verificationLoop_VerificationLoop_l71;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l71: {
			instance_LDSt = false;
			goto verificationLoop_VerificationLoop_l72;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l72: {
			instance_AuMoSt = instance_AuMoSt_aux;
			goto verificationLoop_VerificationLoop_l73;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l73: {
			instance_MMoSt = instance_MMoSt_aux;
			goto verificationLoop_VerificationLoop_l74;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l74: {
			instance_FoMoSt = instance_FoMoSt_aux;
			goto verificationLoop_VerificationLoop_l75;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l75: {
			instance_SoftLDSt = instance_SoftLDSt_aux;
			goto verificationLoop_VerificationLoop_l76;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l76: {
			goto verificationLoop_VerificationLoop_l83;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l77: {
			instance_AuMoSt = false;
			goto verificationLoop_VerificationLoop_l78;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l78: {
			instance_MMoSt = false;
			goto verificationLoop_VerificationLoop_l79;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l79: {
			instance_FoMoSt = false;
			goto verificationLoop_VerificationLoop_l80;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l80: {
			instance_LDSt = true;
			goto verificationLoop_VerificationLoop_l81;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l81: {
			instance_SoftLDSt = false;
			goto verificationLoop_VerificationLoop_l82;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l82: {
			goto verificationLoop_VerificationLoop_l83;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l83: {
		if (instance_PHFPos) {
			goto verificationLoop_VerificationLoop_l84;
		}
		if (((! instance_PHFPos) && (instance_PHFOn && instance_HFOn))) {
			goto verificationLoop_VerificationLoop_l86;
		}
		if ((((! instance_PHFPos) && (! (instance_PHFOn && instance_HFOn))) && (instance_PHFOff && instance_HFOff))) {
			goto verificationLoop_VerificationLoop_l88;
		}
		if ((((! instance_PHFPos) && ((! (instance_PHFOn && instance_HFOn)) && (! (instance_PHFOff && instance_HFOff)))) && instance_PHLDCmd)) {
			goto verificationLoop_VerificationLoop_l90;
		}
		if (((! instance_PHFPos) && ((! ((! instance_PHFPos) && (instance_PHFOn && instance_HFOn))) && ((! (((! instance_PHFPos) && (! (instance_PHFOn && instance_HFOn))) && (instance_PHFOff && instance_HFOff))) && (! (((! instance_PHFPos) && ((! (instance_PHFOn && instance_HFOn)) && (! (instance_PHFOff && instance_HFOff)))) && instance_PHLDCmd)))))) {
			goto verificationLoop_VerificationLoop_l92;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l84: {
			instance_PosSt = instance_HFPos;
			goto verificationLoop_VerificationLoop_l85;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l85: {
			goto verificationLoop_VerificationLoop_l94;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l86: {
			instance_PosSt = instance_PAnalog_PMaxRan;
			goto verificationLoop_VerificationLoop_l87;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l87: {
			goto verificationLoop_VerificationLoop_l94;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l88: {
			instance_PosSt = instance_PAnalog_PMinRan;
			goto verificationLoop_VerificationLoop_l89;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l89: {
			goto verificationLoop_VerificationLoop_l94;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l90: {
			instance_PosSt = instance_HAOut;
			goto verificationLoop_VerificationLoop_l91;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l91: {
			goto verificationLoop_VerificationLoop_l94;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l92: {
			instance_PosSt = instance_PosRSt;
			goto verificationLoop_VerificationLoop_l93;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l93: {
			goto verificationLoop_VerificationLoop_l94;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l94: {
			instance_OnSt = (instance_PosSt >= instance_PliOn);
			goto verificationLoop_VerificationLoop_l95;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l95: {
			instance_OffSt = (instance_PosSt <= instance_PliOff);
			goto verificationLoop_VerificationLoop_l96;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l96: {
		if (instance_EnRstartSt) {
			goto verificationLoop_VerificationLoop_l97;
		}
		if ((! instance_EnRstartSt)) {
			goto verificationLoop_VerificationLoop_l120;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l97: {
		if (instance_E_MNewPosR) {
			goto verificationLoop_VerificationLoop_l98;
		}
		if ((! instance_E_MNewPosR)) {
			goto verificationLoop_VerificationLoop_l100;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l98: {
			instance_MPosRSt = instance_MPosR;
			goto verificationLoop_VerificationLoop_l99;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l99: {
			goto verificationLoop_VerificationLoop_l101;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l100: {
			goto verificationLoop_VerificationLoop_l101;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l101: {
		if (instance_E_MOnR) {
			goto verificationLoop_VerificationLoop_l102;
		}
		if ((! instance_E_MOnR)) {
			goto verificationLoop_VerificationLoop_l104;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l102: {
			instance_MPosRSt = instance_PAnalog_PMaxRan;
			goto verificationLoop_VerificationLoop_l103;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l103: {
			goto verificationLoop_VerificationLoop_l105;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l104: {
			goto verificationLoop_VerificationLoop_l105;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l105: {
		if (instance_E_MStpInR) {
			goto verificationLoop_VerificationLoop_l106;
		}
		if ((! instance_E_MStpInR)) {
			goto verificationLoop_VerificationLoop_l109;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l106: {
			ABS_REAL_inlined_18_in = ((instance_PAnalog_PMaxRan - instance_PAnalog_PMinRan) / 100.0);
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init17;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l107: {
			instance_MPosRSt = (instance_MPosRSt + (instance_PAnalog_PMStpInV * instance____nested_ret_val2));
			goto verificationLoop_VerificationLoop_l108;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l108: {
			goto verificationLoop_VerificationLoop_l110;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l109: {
			goto verificationLoop_VerificationLoop_l110;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l110: {
		if (instance_E_MStpDeR) {
			goto verificationLoop_VerificationLoop_l111;
		}
		if ((! instance_E_MStpDeR)) {
			goto verificationLoop_VerificationLoop_l114;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l111: {
			ABS_REAL_inlined_19_in = ((instance_PAnalog_PMaxRan - instance_PAnalog_PMinRan) / 100.0);
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init18;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l112: {
			instance_MPosRSt = (instance_MPosRSt - (instance_PAnalog_PMStpDeV * instance____nested_ret_val3));
			goto verificationLoop_VerificationLoop_l113;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l113: {
			goto verificationLoop_VerificationLoop_l115;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l114: {
			goto verificationLoop_VerificationLoop_l115;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l115: {
		if (instance_E_MOffR) {
			goto verificationLoop_VerificationLoop_l116;
		}
		if ((! instance_E_MOffR)) {
			goto verificationLoop_VerificationLoop_l118;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l116: {
			instance_MPosRSt = instance_PAnalog_PMinRan;
			goto verificationLoop_VerificationLoop_l117;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l117: {
			goto verificationLoop_VerificationLoop_l119;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l118: {
			goto verificationLoop_VerificationLoop_l119;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l119: {
			goto verificationLoop_VerificationLoop_l129;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l120: {
		if (((! instance_PFsPosOn) && instance_E_MOffR)) {
			goto verificationLoop_VerificationLoop_l121;
		}
		if ((! ((! instance_PFsPosOn) && instance_E_MOffR))) {
			goto verificationLoop_VerificationLoop_l123;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l121: {
			instance_MPosRSt = instance_PAnalog_PMinRan;
			goto verificationLoop_VerificationLoop_l122;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l122: {
			goto verificationLoop_VerificationLoop_l124;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l123: {
			goto verificationLoop_VerificationLoop_l124;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l124: {
		if ((instance_PFsPosOn && instance_E_MOnR)) {
			goto verificationLoop_VerificationLoop_l125;
		}
		if ((! (instance_PFsPosOn && instance_E_MOnR))) {
			goto verificationLoop_VerificationLoop_l127;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l125: {
			instance_MPosRSt = instance_PAnalog_PMaxRan;
			goto verificationLoop_VerificationLoop_l126;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l126: {
			goto verificationLoop_VerificationLoop_l128;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l127: {
			goto verificationLoop_VerificationLoop_l128;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l128: {
			goto verificationLoop_VerificationLoop_l129;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l129: {
		if ((instance_MPosRSt > instance_PAnalog_PMaxRan)) {
			goto verificationLoop_VerificationLoop_l130;
		}
		if ((! (instance_MPosRSt > instance_PAnalog_PMaxRan))) {
			goto verificationLoop_VerificationLoop_l132;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l130: {
			instance_MPosRSt = instance_PAnalog_PMaxRan;
			goto verificationLoop_VerificationLoop_l131;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l131: {
			goto verificationLoop_VerificationLoop_l133;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l132: {
			goto verificationLoop_VerificationLoop_l133;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l133: {
		if ((instance_MPosRSt < instance_PAnalog_PMinRan)) {
			goto verificationLoop_VerificationLoop_l134;
		}
		if ((! (instance_MPosRSt < instance_PAnalog_PMinRan))) {
			goto verificationLoop_VerificationLoop_l136;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l134: {
			instance_MPosRSt = instance_PAnalog_PMinRan;
			goto verificationLoop_VerificationLoop_l135;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l135: {
			goto verificationLoop_VerificationLoop_l137;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l136: {
			goto verificationLoop_VerificationLoop_l137;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l137: {
			instance_AuPosRSt = instance_AuPosR;
			goto verificationLoop_VerificationLoop_l138;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l138: {
		if (instance_AuOnR) {
			goto verificationLoop_VerificationLoop_l139;
		}
		if ((! instance_AuOnR)) {
			goto verificationLoop_VerificationLoop_l141;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l139: {
			instance_AuPosRSt = instance_PAnalog_PMaxRan;
			goto verificationLoop_VerificationLoop_l140;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l140: {
			goto verificationLoop_VerificationLoop_l142;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l141: {
			goto verificationLoop_VerificationLoop_l142;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l142: {
		if (instance_AuOffR) {
			goto verificationLoop_VerificationLoop_l143;
		}
		if ((! instance_AuOffR)) {
			goto verificationLoop_VerificationLoop_l145;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l143: {
			instance_AuPosRSt = instance_PAnalog_PMinRan;
			goto verificationLoop_VerificationLoop_l144;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l144: {
			goto verificationLoop_VerificationLoop_l146;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l145: {
			goto verificationLoop_VerificationLoop_l146;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l146: {
		if ((instance_AuPosRSt > instance_PAnalog_PMaxRan)) {
			goto verificationLoop_VerificationLoop_l147;
		}
		if ((! (instance_AuPosRSt > instance_PAnalog_PMaxRan))) {
			goto verificationLoop_VerificationLoop_l149;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l147: {
			instance_AuPosRSt = instance_PAnalog_PMaxRan;
			goto verificationLoop_VerificationLoop_l148;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l148: {
			goto verificationLoop_VerificationLoop_l150;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l149: {
			goto verificationLoop_VerificationLoop_l150;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l150: {
		if ((instance_AuPosRSt < instance_PAnalog_PMinRan)) {
			goto verificationLoop_VerificationLoop_l151;
		}
		if ((! (instance_AuPosRSt < instance_PAnalog_PMinRan))) {
			goto verificationLoop_VerificationLoop_l153;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l151: {
			instance_AuPosRSt = instance_PAnalog_PMinRan;
			goto verificationLoop_VerificationLoop_l152;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l152: {
			goto verificationLoop_VerificationLoop_l154;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l153: {
			goto verificationLoop_VerificationLoop_l154;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l154: {
		if (instance_AuMoSt) {
			goto verificationLoop_VerificationLoop_l155;
		}
		if (((! instance_AuMoSt) && ((instance_MMoSt || instance_FoMoSt) || instance_SoftLDSt))) {
			goto verificationLoop_VerificationLoop_l158;
		}
		if (((! instance_AuMoSt) && (! ((! instance_AuMoSt) && ((instance_MMoSt || instance_FoMoSt) || instance_SoftLDSt))))) {
			goto verificationLoop_VerificationLoop_l160;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l155: {
			instance_PosR = instance_AuPosRSt;
			goto verificationLoop_VerificationLoop_l156;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l156: {
			instance_MPosRSt = instance_AuPosRSt;
			goto verificationLoop_VerificationLoop_l157;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l157: {
			goto verificationLoop_VerificationLoop_l170;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l158: {
			instance_PosR = instance_MPosRSt;
			goto verificationLoop_VerificationLoop_l159;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l159: {
			goto verificationLoop_VerificationLoop_l170;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l160: {
		if ((instance_PHLDCmd && instance_PHLD)) {
			goto verificationLoop_VerificationLoop_l161;
		}
		if ((! (instance_PHLDCmd && instance_PHLD))) {
			goto verificationLoop_VerificationLoop_l164;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l161: {
			instance_MPosRSt = instance_HAOut;
			goto verificationLoop_VerificationLoop_l162;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l162: {
			instance_PosR = instance_HAOut;
			goto verificationLoop_VerificationLoop_l163;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l163: {
			goto verificationLoop_VerificationLoop_l167;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l164: {
			instance_MPosRSt = instance_PosSt;
			goto verificationLoop_VerificationLoop_l165;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l165: {
			instance_PosR = instance_PosSt;
			goto verificationLoop_VerificationLoop_l166;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l166: {
			goto verificationLoop_VerificationLoop_l167;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l167: {
			instance_Ramp_parameters_inc_rate = 0.0;
			goto verificationLoop_VerificationLoop_l168;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l168: {
			instance_Ramp_parameters_dec_rate = 0.0;
			goto verificationLoop_VerificationLoop_l169;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l169: {
			goto verificationLoop_VerificationLoop_l170;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l170: {
		if (instance_E_FuStopI) {
			goto verificationLoop_VerificationLoop_l171;
		}
		if ((! instance_E_FuStopI)) {
			goto verificationLoop_VerificationLoop_l185;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l171: {
			instance_fullNotAcknowledged = true;
			goto verificationLoop_VerificationLoop_l172;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l172: {
		if ((! instance_AuMoSt)) {
			goto verificationLoop_VerificationLoop_l173;
		}
		if ((! (! instance_AuMoSt))) {
			goto verificationLoop_VerificationLoop_l179;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l173: {
		if ((! instance_PFsPosOn)) {
			goto verificationLoop_VerificationLoop_l174;
		}
		if ((! (! instance_PFsPosOn))) {
			goto verificationLoop_VerificationLoop_l176;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l174: {
			instance_MPosRSt = instance_PAnalog_PMinRan;
			goto verificationLoop_VerificationLoop_l175;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l175: {
			goto verificationLoop_VerificationLoop_l178;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l176: {
			instance_MPosRSt = instance_PAnalog_PMaxRan;
			goto verificationLoop_VerificationLoop_l177;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l177: {
			goto verificationLoop_VerificationLoop_l178;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l178: {
			goto verificationLoop_VerificationLoop_l180;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l179: {
			goto verificationLoop_VerificationLoop_l180;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l180: {
		if (instance_PEnRstart) {
			goto verificationLoop_VerificationLoop_l181;
		}
		if ((! instance_PEnRstart)) {
			goto verificationLoop_VerificationLoop_l183;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l181: {
			instance_EnRstartSt = false;
			goto verificationLoop_VerificationLoop_l182;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l182: {
			goto verificationLoop_VerificationLoop_l184;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l183: {
			goto verificationLoop_VerificationLoop_l184;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l184: {
			goto verificationLoop_VerificationLoop_l186;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l185: {
			goto verificationLoop_VerificationLoop_l186;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l186: {
		if ((! instance_PFsPosOn)) {
			goto verificationLoop_VerificationLoop_l187;
		}
		if ((! (! instance_PFsPosOn))) {
			goto verificationLoop_VerificationLoop_l192;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l187: {
		if (instance_InterlockR) {
			goto verificationLoop_VerificationLoop_l188;
		}
		if ((! instance_InterlockR)) {
			goto verificationLoop_VerificationLoop_l190;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l188: {
			instance_PosR = instance_PAnalog_PMinRan;
			goto verificationLoop_VerificationLoop_l189;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l189: {
			goto verificationLoop_VerificationLoop_l191;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l190: {
			goto verificationLoop_VerificationLoop_l191;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l191: {
			goto verificationLoop_VerificationLoop_l197;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l192: {
		if (instance_InterlockR) {
			goto verificationLoop_VerificationLoop_l193;
		}
		if ((! instance_InterlockR)) {
			goto verificationLoop_VerificationLoop_l195;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l193: {
			instance_PosR = instance_PAnalog_PMaxRan;
			goto verificationLoop_VerificationLoop_l194;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l194: {
			goto verificationLoop_VerificationLoop_l196;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l195: {
			goto verificationLoop_VerificationLoop_l196;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l196: {
			goto verificationLoop_VerificationLoop_l197;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l197: {
			instance_AlSt = instance_Al;
			goto verificationLoop_VerificationLoop_l198;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l198: {
		if (instance_AuMoSt) {
			goto verificationLoop_VerificationLoop_l199;
		}
		if (((! instance_AuMoSt) && ((instance_MMoSt || instance_FoMoSt) || instance_SoftLDSt))) {
			goto verificationLoop_VerificationLoop_l202;
		}
		if (((! instance_AuMoSt) && (! ((! instance_AuMoSt) && ((instance_MMoSt || instance_FoMoSt) || instance_SoftLDSt))))) {
			goto verificationLoop_VerificationLoop_l205;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l199: {
			instance_Ramp_parameters_inc_rate = instance_AuInSpd;
			goto verificationLoop_VerificationLoop_l200;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l200: {
			instance_Ramp_parameters_dec_rate = instance_AuDeSpd;
			goto verificationLoop_VerificationLoop_l201;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l201: {
			goto verificationLoop_VerificationLoop_l206;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l202: {
			instance_Ramp_parameters_inc_rate = instance_PAnalog_PMInSpd;
			goto verificationLoop_VerificationLoop_l203;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l203: {
			instance_Ramp_parameters_dec_rate = instance_PAnalog_PMDeSpd;
			goto verificationLoop_VerificationLoop_l204;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l204: {
			goto verificationLoop_VerificationLoop_l206;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l205: {
			goto verificationLoop_VerificationLoop_l206;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l206: {
		if ((instance_TStopI || instance_FuStopI)) {
			goto verificationLoop_VerificationLoop_l207;
		}
		if ((! (instance_TStopI || instance_FuStopI))) {
			goto verificationLoop_VerificationLoop_l210;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l207: {
			instance_Ramp_parameters_inc_rate = instance_AuInSpd;
			goto verificationLoop_VerificationLoop_l208;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l208: {
			instance_Ramp_parameters_dec_rate = instance_AuDeSpd;
			goto verificationLoop_VerificationLoop_l209;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l209: {
			goto verificationLoop_VerificationLoop_l211;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l210: {
			goto verificationLoop_VerificationLoop_l211;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l211: {
			instance_ROC_LIM_INV = instance_PosR;
			instance_ROC_LIM_UPRLM_P = instance_Ramp_parameters_inc_rate;
			instance_ROC_LIM_DNRLM_P = instance_Ramp_parameters_dec_rate;
			instance_ROC_LIM_UPRLM_N = instance_Ramp_parameters_inc_rate;
			instance_ROC_LIM_DNRLM_N = instance_Ramp_parameters_dec_rate;
			instance_ROC_LIM_DFOUT_ON = false;
			instance_ROC_LIM_DF_OUTV = instance_PosRSt;
			instance_ROC_LIM_H_LM = instance_PAnalog_PMaxRan;
			instance_ROC_LIM_L_LM = instance_PAnalog_PMinRan;
			instance_ROC_LIM_CYCLE = ((int32_t) T_CYCLE);
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init19;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l212: {
			instance_PosRSt = instance_ROC_LIM_OUTV;
			goto verificationLoop_VerificationLoop_l213;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l213: {
			instance_RdyStartSt = (! instance_InterlockR);
			goto verificationLoop_VerificationLoop_l214;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l214: {
			instance_IOErrorW = instance_IOError;
			goto verificationLoop_VerificationLoop_l215;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l215: {
			instance_IOSimuW = instance_IOSimu;
			goto verificationLoop_VerificationLoop_l216;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l216: {
			ABS_REAL_inlined_20_in = (instance_AuPosRSt - instance_MPosRSt);
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init20;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l217: {
			instance_AuMRW = ((((instance_MMoSt || instance_FoMoSt) || instance_SoftLDSt) && (instance____nested_ret_val4 > instance_PAnalog_PWDb)) && (! instance_IhAuMRW));
			goto verificationLoop_VerificationLoop_l218;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l218: {
			ABS_REAL_inlined_21_in = (instance_HFPos - instance_MPosRSt);
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l219: {
			instance_Timer_Warning_IN = (instance____nested_ret_val5 > instance_PAnalog_PWDb);
			instance_Timer_Warning_PT = instance_PAnalog_PWDt;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l220: {
		if (instance_PHFPos) {
			goto verificationLoop_VerificationLoop_l221;
		}
		if ((! instance_PHFPos)) {
			goto verificationLoop_VerificationLoop_l223;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l221: {
			instance_PosW = instance_Timer_Warning_Q;
			goto verificationLoop_VerificationLoop_l222;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l222: {
			goto verificationLoop_VerificationLoop_l225;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l223: {
			instance_PosW = false;
			goto verificationLoop_VerificationLoop_l224;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l224: {
			goto verificationLoop_VerificationLoop_l225;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l225: {
			instance_Time_Warning = instance_Timer_Warning_ET;
			goto verificationLoop_VerificationLoop_l226;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l226: {
		if ((instance_PFsPosOn && (! instance_PFsNOut))) {
			goto verificationLoop_VerificationLoop_l227;
		}
		if ((! (instance_PFsPosOn && (! instance_PFsNOut)))) {
			goto verificationLoop_VerificationLoop_l229;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l227: {
			instance_OutOV = ((instance_PAnalog_PMaxRan + instance_PAnalog_PMinRan) - instance_PosRSt);
			goto verificationLoop_VerificationLoop_l228;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l228: {
			goto verificationLoop_VerificationLoop_l231;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l229: {
			instance_OutOV = instance_PosRSt;
			goto verificationLoop_VerificationLoop_l230;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l230: {
			goto verificationLoop_VerificationLoop_l231;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l231: {
			instance_AlBW = instance_AlB;
			goto verificationLoop_VerificationLoop_l232;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l232: {
			instance_PulseWidth = (1500.0 / ((float) ((int32_t) ((int32_t) T_CYCLE))));
			goto verificationLoop_VerificationLoop_l233;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l233: {
		if ((instance_FuStopISt || (instance_FSIinc > 0))) {
			goto verificationLoop_VerificationLoop_l234;
		}
		if ((! (instance_FuStopISt || (instance_FSIinc > 0)))) {
			goto verificationLoop_VerificationLoop_l237;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l234: {
			instance_FSIinc = (instance_FSIinc + 1);
			goto verificationLoop_VerificationLoop_l235;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l235: {
			instance_WFuStopISt = true;
			goto verificationLoop_VerificationLoop_l236;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l236: {
			goto verificationLoop_VerificationLoop_l238;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l237: {
			goto verificationLoop_VerificationLoop_l238;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l238: {
		if (((((float) instance_FSIinc) > instance_PulseWidth) || ((! instance_FuStopISt) && (instance_FSIinc == 0)))) {
			goto verificationLoop_VerificationLoop_l239;
		}
		if ((! ((((float) instance_FSIinc) > instance_PulseWidth) || ((! instance_FuStopISt) && (instance_FSIinc == 0))))) {
			goto verificationLoop_VerificationLoop_l242;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l239: {
			instance_FSIinc = 0;
			goto verificationLoop_VerificationLoop_l240;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l240: {
			instance_WFuStopISt = instance_FuStopISt;
			goto verificationLoop_VerificationLoop_l241;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l241: {
			goto verificationLoop_VerificationLoop_l243;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l242: {
			goto verificationLoop_VerificationLoop_l243;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l243: {
		if ((instance_TStopISt || (instance_TSIinc > 0))) {
			goto verificationLoop_VerificationLoop_l244;
		}
		if ((! (instance_TStopISt || (instance_TSIinc > 0)))) {
			goto verificationLoop_VerificationLoop_l247;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l244: {
			instance_TSIinc = (instance_TSIinc + 1);
			goto verificationLoop_VerificationLoop_l245;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l245: {
			instance_WTStopISt = true;
			goto verificationLoop_VerificationLoop_l246;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l246: {
			goto verificationLoop_VerificationLoop_l248;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l247: {
			goto verificationLoop_VerificationLoop_l248;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l248: {
		if (((((float) instance_TSIinc) > instance_PulseWidth) || ((! instance_TStopISt) && (instance_TSIinc == 0)))) {
			goto verificationLoop_VerificationLoop_l249;
		}
		if ((! ((((float) instance_TSIinc) > instance_PulseWidth) || ((! instance_TStopISt) && (instance_TSIinc == 0))))) {
			goto verificationLoop_VerificationLoop_l252;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l249: {
			instance_TSIinc = 0;
			goto verificationLoop_VerificationLoop_l250;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l250: {
			instance_WTStopISt = instance_TStopISt;
			goto verificationLoop_VerificationLoop_l251;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l251: {
			goto verificationLoop_VerificationLoop_l253;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l252: {
			goto verificationLoop_VerificationLoop_l253;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l253: {
		if ((instance_StartISt || (instance_SIinc > 0))) {
			goto verificationLoop_VerificationLoop_l254;
		}
		if ((! (instance_StartISt || (instance_SIinc > 0)))) {
			goto verificationLoop_VerificationLoop_l257;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l254: {
			instance_SIinc = (instance_SIinc + 1);
			goto verificationLoop_VerificationLoop_l255;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l255: {
			instance_WStartISt = true;
			goto verificationLoop_VerificationLoop_l256;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l256: {
			goto verificationLoop_VerificationLoop_l258;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l257: {
			goto verificationLoop_VerificationLoop_l258;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l258: {
		if (((((float) instance_SIinc) > instance_PulseWidth) || ((! instance_StartISt) && (instance_SIinc == 0)))) {
			goto verificationLoop_VerificationLoop_l259;
		}
		if ((! ((((float) instance_SIinc) > instance_PulseWidth) || ((! instance_StartISt) && (instance_SIinc == 0))))) {
			goto verificationLoop_VerificationLoop_l262;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l259: {
			instance_SIinc = 0;
			goto verificationLoop_VerificationLoop_l260;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l260: {
			instance_WStartISt = instance_StartISt;
			goto verificationLoop_VerificationLoop_l261;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l261: {
			goto verificationLoop_VerificationLoop_l263;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l262: {
			goto verificationLoop_VerificationLoop_l263;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l263: {
		if ((instance_AlSt || (instance_Alinc > 0))) {
			goto verificationLoop_VerificationLoop_l264;
		}
		if ((! (instance_AlSt || (instance_Alinc > 0)))) {
			goto verificationLoop_VerificationLoop_l267;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l264: {
			instance_Alinc = (instance_Alinc + 1);
			goto verificationLoop_VerificationLoop_l265;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l265: {
			instance_WAlSt = true;
			goto verificationLoop_VerificationLoop_l266;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l266: {
			goto verificationLoop_VerificationLoop_l268;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l267: {
			goto verificationLoop_VerificationLoop_l268;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l268: {
		if (((((float) instance_Alinc) > instance_PulseWidth) || ((! instance_AlSt) && (instance_Alinc == 0)))) {
			goto verificationLoop_VerificationLoop_l269;
		}
		if ((! ((((float) instance_Alinc) > instance_PulseWidth) || ((! instance_AlSt) && (instance_Alinc == 0))))) {
			goto verificationLoop_VerificationLoop_l272;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l269: {
			instance_Alinc = 0;
			goto verificationLoop_VerificationLoop_l270;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l270: {
			instance_WAlSt = instance_AlSt;
			goto verificationLoop_VerificationLoop_l271;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l271: {
			goto verificationLoop_VerificationLoop_l273;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l272: {
			goto verificationLoop_VerificationLoop_l273;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l273: {
			instance_Stsreg01b_8 = instance_OnSt;
			goto verificationLoop_VerificationLoop_x1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l274: {
			instance_Stsreg01b_9 = instance_OffSt;
			goto verificationLoop_VerificationLoop_x2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l275: {
			instance_Stsreg01b_10 = instance_AuMoSt;
			goto verificationLoop_VerificationLoop_x3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l276: {
			instance_Stsreg01b_11 = instance_MMoSt;
			goto verificationLoop_VerificationLoop_x4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l277: {
			instance_Stsreg01b_12 = instance_FoMoSt;
			goto verificationLoop_VerificationLoop_x5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l278: {
			instance_Stsreg01b_13 = instance_LDSt;
			goto verificationLoop_VerificationLoop_x6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l279: {
			instance_Stsreg01b_14 = instance_IOErrorW;
			goto verificationLoop_VerificationLoop_x7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l280: {
			instance_Stsreg01b_15 = instance_IOSimuW;
			goto verificationLoop_VerificationLoop_x8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l281: {
			instance_Stsreg01b_0 = instance_AuMRW;
			goto verificationLoop_VerificationLoop_x9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l282: {
			instance_Stsreg01b_1 = instance_PosW;
			goto verificationLoop_VerificationLoop_x10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l283: {
			instance_Stsreg01b_2 = instance_WStartISt;
			goto verificationLoop_VerificationLoop_x11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l284: {
			instance_Stsreg01b_3 = instance_WTStopISt;
			goto verificationLoop_VerificationLoop_x12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l285: {
			instance_Stsreg01b_4 = instance_AlUnAck;
			goto verificationLoop_VerificationLoop_x13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l286: {
			instance_Stsreg01b_5 = instance_AuIhFoMo;
			goto verificationLoop_VerificationLoop_x14;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l287: {
			instance_Stsreg01b_6 = instance_WAlSt;
			goto verificationLoop_VerificationLoop_x15;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l288: {
			instance_Stsreg01b_7 = instance_AuIhMMo;
			goto verificationLoop_VerificationLoop_x16;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l289: {
			instance_Stsreg02b_8 = false;
			goto verificationLoop_VerificationLoop_x17;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l290: {
			instance_Stsreg02b_9 = false;
			goto verificationLoop_VerificationLoop_x18;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l291: {
			instance_Stsreg02b_10 = false;
			goto verificationLoop_VerificationLoop_x19;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l292: {
			instance_Stsreg02b_11 = false;
			goto verificationLoop_VerificationLoop_x20;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l293: {
			instance_Stsreg02b_12 = false;
			goto verificationLoop_VerificationLoop_x21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l294: {
			instance_Stsreg02b_13 = false;
			goto verificationLoop_VerificationLoop_x22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l295: {
			instance_Stsreg02b_14 = false;
			goto verificationLoop_VerificationLoop_x23;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l296: {
			instance_Stsreg02b_15 = false;
			goto verificationLoop_VerificationLoop_x24;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l297: {
			instance_Stsreg02b_0 = false;
			goto verificationLoop_VerificationLoop_x25;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l298: {
			instance_Stsreg02b_1 = false;
			goto verificationLoop_VerificationLoop_x26;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l299: {
			instance_Stsreg02b_2 = instance_WFuStopISt;
			goto verificationLoop_VerificationLoop_x27;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l300: {
			instance_Stsreg02b_3 = instance_EnRstartSt;
			goto verificationLoop_VerificationLoop_x28;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l301: {
			instance_Stsreg02b_4 = instance_SoftLDSt;
			goto verificationLoop_VerificationLoop_x29;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l302: {
			instance_Stsreg02b_5 = instance_AlBW;
			goto verificationLoop_VerificationLoop_x30;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l303: {
			instance_Stsreg02b_6 = false;
			goto verificationLoop_VerificationLoop_x31;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l304: {
			instance_Stsreg02b_7 = false;
			goto verificationLoop_VerificationLoop_x32;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l305: {
			DETECT_EDGE_new = instance_AlUnAck;
			DETECT_EDGE_old = instance_AlUnAck_old;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init23;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l306: {
			goto callEnd;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x: {
			instance____nested_ret_val2 = 0.0;
			instance____nested_ret_val3 = 0.0;
			instance____nested_ret_val4 = 0.0;
			instance____nested_ret_val5 = 0.0;
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh: {
			instance_PAnalogb_ParRegb_0 = ((instance_PAnalog_ParReg & 256) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh18;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh1: {
			instance_Manreg01b_1 = ((instance_Manreg01 & 512) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh2: {
			instance_Manreg01b_2 = ((instance_Manreg01 & 1024) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh3: {
			instance_Manreg01b_3 = ((instance_Manreg01 & 2048) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh4: {
			instance_Manreg01b_4 = ((instance_Manreg01 & 4096) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh5: {
			instance_Manreg01b_5 = ((instance_Manreg01 & 8192) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh6: {
			instance_Manreg01b_6 = ((instance_Manreg01 & 16384) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh7: {
			instance_Manreg01b_7 = ((instance_Manreg01 & 32768) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh8: {
			instance_Manreg01b_8 = ((instance_Manreg01 & 1) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh9: {
			instance_Manreg01b_9 = ((instance_Manreg01 & 2) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh10: {
			instance_Manreg01b_10 = ((instance_Manreg01 & 4) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh11: {
			instance_Manreg01b_11 = ((instance_Manreg01 & 8) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh12: {
			instance_Manreg01b_12 = ((instance_Manreg01 & 16) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh13: {
			instance_Manreg01b_13 = ((instance_Manreg01 & 32) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh14;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh14: {
			instance_Manreg01b_14 = ((instance_Manreg01 & 64) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh15;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh15: {
			instance_Manreg01b_15 = ((instance_Manreg01 & 128) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh16: {
			instance_Stsreg01b_0 = ((instance_Stsreg01 & 256) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh41;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh17: {
			instance_PAnalog_PMaxRan = instance_PAnalogb_PMaxRan;
			goto verificationLoop_VerificationLoop_varview_refresh33;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh18: {
			instance_PAnalogb_ParRegb_1 = ((instance_PAnalog_ParReg & 512) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh19;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh19: {
			instance_PAnalogb_ParRegb_2 = ((instance_PAnalog_ParReg & 1024) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh20;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh20: {
			instance_PAnalogb_ParRegb_3 = ((instance_PAnalog_ParReg & 2048) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh21: {
			instance_PAnalogb_ParRegb_4 = ((instance_PAnalog_ParReg & 4096) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh22: {
			instance_PAnalogb_ParRegb_5 = ((instance_PAnalog_ParReg & 8192) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh23;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh23: {
			instance_PAnalogb_ParRegb_6 = ((instance_PAnalog_ParReg & 16384) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh24;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh24: {
			instance_PAnalogb_ParRegb_7 = ((instance_PAnalog_ParReg & 32768) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh25;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh25: {
			instance_PAnalogb_ParRegb_8 = ((instance_PAnalog_ParReg & 1) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh26;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh26: {
			instance_PAnalogb_ParRegb_9 = ((instance_PAnalog_ParReg & 2) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh27;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh27: {
			instance_PAnalogb_ParRegb_10 = ((instance_PAnalog_ParReg & 4) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh28;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh28: {
			instance_PAnalogb_ParRegb_11 = ((instance_PAnalog_ParReg & 8) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh29;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh29: {
			instance_PAnalogb_ParRegb_12 = ((instance_PAnalog_ParReg & 16) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh30;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh30: {
			instance_PAnalogb_ParRegb_13 = ((instance_PAnalog_ParReg & 32) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh31;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh31: {
			instance_PAnalogb_ParRegb_14 = ((instance_PAnalog_ParReg & 64) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh32;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh32: {
			instance_PAnalogb_ParRegb_15 = ((instance_PAnalog_ParReg & 128) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh17;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh33: {
			instance_PAnalog_PMinRan = instance_PAnalogb_PMinRan;
			goto verificationLoop_VerificationLoop_varview_refresh34;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh34: {
			instance_PAnalog_PMStpInV = instance_PAnalogb_PMStpInV;
			goto verificationLoop_VerificationLoop_varview_refresh35;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh35: {
			instance_PAnalog_PMStpDeV = instance_PAnalogb_PMStpDeV;
			goto verificationLoop_VerificationLoop_varview_refresh36;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh36: {
			instance_PAnalog_PMInSpd = instance_PAnalogb_PMInSpd;
			goto verificationLoop_VerificationLoop_varview_refresh37;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh37: {
			instance_PAnalog_PMDeSpd = instance_PAnalogb_PMDeSpd;
			goto verificationLoop_VerificationLoop_varview_refresh38;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh38: {
			instance_PAnalog_PWDt = instance_PAnalogb_PWDt;
			goto verificationLoop_VerificationLoop_varview_refresh39;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh39: {
			instance_PAnalog_PWDb = instance_PAnalogb_PWDb;
			goto verificationLoop_VerificationLoop_varview_refresh16;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh40: {
			instance_Stsreg02b_0 = ((instance_Stsreg02 & 256) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh56;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh41: {
			instance_Stsreg01b_1 = ((instance_Stsreg01 & 512) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh42;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh42: {
			instance_Stsreg01b_2 = ((instance_Stsreg01 & 1024) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh43;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh43: {
			instance_Stsreg01b_3 = ((instance_Stsreg01 & 2048) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh44;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh44: {
			instance_Stsreg01b_4 = ((instance_Stsreg01 & 4096) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh45;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh45: {
			instance_Stsreg01b_5 = ((instance_Stsreg01 & 8192) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh46;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh46: {
			instance_Stsreg01b_6 = ((instance_Stsreg01 & 16384) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh47;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh47: {
			instance_Stsreg01b_7 = ((instance_Stsreg01 & 32768) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh48;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh48: {
			instance_Stsreg01b_8 = ((instance_Stsreg01 & 1) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh49;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh49: {
			instance_Stsreg01b_9 = ((instance_Stsreg01 & 2) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh50;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh50: {
			instance_Stsreg01b_10 = ((instance_Stsreg01 & 4) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh51;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh51: {
			instance_Stsreg01b_11 = ((instance_Stsreg01 & 8) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh52: {
			instance_Stsreg01b_12 = ((instance_Stsreg01 & 16) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh53;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh53: {
			instance_Stsreg01b_13 = ((instance_Stsreg01 & 32) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh54;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh54: {
			instance_Stsreg01b_14 = ((instance_Stsreg01 & 64) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh55;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh55: {
			instance_Stsreg01b_15 = ((instance_Stsreg01 & 128) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh40;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh56: {
			instance_Stsreg02b_1 = ((instance_Stsreg02 & 512) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh57;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh57: {
			instance_Stsreg02b_2 = ((instance_Stsreg02 & 1024) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh58;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh58: {
			instance_Stsreg02b_3 = ((instance_Stsreg02 & 2048) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh59;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh59: {
			instance_Stsreg02b_4 = ((instance_Stsreg02 & 4096) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh60;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh60: {
			instance_Stsreg02b_5 = ((instance_Stsreg02 & 8192) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh61;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh61: {
			instance_Stsreg02b_6 = ((instance_Stsreg02 & 16384) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh62;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh62: {
			instance_Stsreg02b_7 = ((instance_Stsreg02 & 32768) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh63;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh63: {
			instance_Stsreg02b_8 = ((instance_Stsreg02 & 1) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh64;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh64: {
			instance_Stsreg02b_9 = ((instance_Stsreg02 & 2) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh65;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh65: {
			instance_Stsreg02b_10 = ((instance_Stsreg02 & 4) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh66;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh66: {
			instance_Stsreg02b_11 = ((instance_Stsreg02 & 8) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh67;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh67: {
			instance_Stsreg02b_12 = ((instance_Stsreg02 & 16) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh68;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh68: {
			instance_Stsreg02b_13 = ((instance_Stsreg02 & 32) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh69;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh69: {
			instance_Stsreg02b_14 = ((instance_Stsreg02 & 64) != 0);
			goto verificationLoop_VerificationLoop_varview_refresh70;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_varview_refresh70: {
			instance_Stsreg02b_15 = ((instance_Stsreg02 & 128) != 0);
			goto verificationLoop_VerificationLoop_x;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x1: {
		if (instance_Stsreg01b_8) {
			instance_Stsreg01 = (instance_Stsreg01 | 1);
			goto verificationLoop_VerificationLoop_l274;
		}
		if ((! instance_Stsreg01b_8)) {
			instance_Stsreg01 = (instance_Stsreg01 & 65534);
			goto verificationLoop_VerificationLoop_l274;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x2: {
		if (instance_Stsreg01b_9) {
			instance_Stsreg01 = (instance_Stsreg01 | 2);
			goto verificationLoop_VerificationLoop_l275;
		}
		if ((! instance_Stsreg01b_9)) {
			instance_Stsreg01 = (instance_Stsreg01 & 65533);
			goto verificationLoop_VerificationLoop_l275;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x3: {
		if (instance_Stsreg01b_10) {
			instance_Stsreg01 = (instance_Stsreg01 | 4);
			goto verificationLoop_VerificationLoop_l276;
		}
		if ((! instance_Stsreg01b_10)) {
			instance_Stsreg01 = (instance_Stsreg01 & 65531);
			goto verificationLoop_VerificationLoop_l276;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x4: {
		if (instance_Stsreg01b_11) {
			instance_Stsreg01 = (instance_Stsreg01 | 8);
			goto verificationLoop_VerificationLoop_l277;
		}
		if ((! instance_Stsreg01b_11)) {
			instance_Stsreg01 = (instance_Stsreg01 & 65527);
			goto verificationLoop_VerificationLoop_l277;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x5: {
		if (instance_Stsreg01b_12) {
			instance_Stsreg01 = (instance_Stsreg01 | 16);
			goto verificationLoop_VerificationLoop_l278;
		}
		if ((! instance_Stsreg01b_12)) {
			instance_Stsreg01 = (instance_Stsreg01 & 65519);
			goto verificationLoop_VerificationLoop_l278;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x6: {
		if (instance_Stsreg01b_13) {
			instance_Stsreg01 = (instance_Stsreg01 | 32);
			goto verificationLoop_VerificationLoop_l279;
		}
		if ((! instance_Stsreg01b_13)) {
			instance_Stsreg01 = (instance_Stsreg01 & 65503);
			goto verificationLoop_VerificationLoop_l279;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x7: {
		if (instance_Stsreg01b_14) {
			instance_Stsreg01 = (instance_Stsreg01 | 64);
			goto verificationLoop_VerificationLoop_l280;
		}
		if ((! instance_Stsreg01b_14)) {
			instance_Stsreg01 = (instance_Stsreg01 & 65471);
			goto verificationLoop_VerificationLoop_l280;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x8: {
		if (instance_Stsreg01b_15) {
			instance_Stsreg01 = (instance_Stsreg01 | 128);
			goto verificationLoop_VerificationLoop_l281;
		}
		if ((! instance_Stsreg01b_15)) {
			instance_Stsreg01 = (instance_Stsreg01 & 65407);
			goto verificationLoop_VerificationLoop_l281;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x9: {
		if (instance_Stsreg01b_0) {
			instance_Stsreg01 = (instance_Stsreg01 | 256);
			goto verificationLoop_VerificationLoop_l282;
		}
		if ((! instance_Stsreg01b_0)) {
			instance_Stsreg01 = (instance_Stsreg01 & 65279);
			goto verificationLoop_VerificationLoop_l282;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x10: {
		if (instance_Stsreg01b_1) {
			instance_Stsreg01 = (instance_Stsreg01 | 512);
			goto verificationLoop_VerificationLoop_l283;
		}
		if ((! instance_Stsreg01b_1)) {
			instance_Stsreg01 = (instance_Stsreg01 & 65023);
			goto verificationLoop_VerificationLoop_l283;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x11: {
		if (instance_Stsreg01b_2) {
			instance_Stsreg01 = (instance_Stsreg01 | 1024);
			goto verificationLoop_VerificationLoop_l284;
		}
		if ((! instance_Stsreg01b_2)) {
			instance_Stsreg01 = (instance_Stsreg01 & 64511);
			goto verificationLoop_VerificationLoop_l284;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x12: {
		if (instance_Stsreg01b_3) {
			instance_Stsreg01 = (instance_Stsreg01 | 2048);
			goto verificationLoop_VerificationLoop_l285;
		}
		if ((! instance_Stsreg01b_3)) {
			instance_Stsreg01 = (instance_Stsreg01 & 63487);
			goto verificationLoop_VerificationLoop_l285;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x13: {
		if (instance_Stsreg01b_4) {
			instance_Stsreg01 = (instance_Stsreg01 | 4096);
			goto verificationLoop_VerificationLoop_l286;
		}
		if ((! instance_Stsreg01b_4)) {
			instance_Stsreg01 = (instance_Stsreg01 & 61439);
			goto verificationLoop_VerificationLoop_l286;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x14: {
		if (instance_Stsreg01b_5) {
			instance_Stsreg01 = (instance_Stsreg01 | 8192);
			goto verificationLoop_VerificationLoop_l287;
		}
		if ((! instance_Stsreg01b_5)) {
			instance_Stsreg01 = (instance_Stsreg01 & 57343);
			goto verificationLoop_VerificationLoop_l287;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x15: {
		if (instance_Stsreg01b_6) {
			instance_Stsreg01 = (instance_Stsreg01 | 16384);
			goto verificationLoop_VerificationLoop_l288;
		}
		if ((! instance_Stsreg01b_6)) {
			instance_Stsreg01 = (instance_Stsreg01 & 49151);
			goto verificationLoop_VerificationLoop_l288;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x16: {
		if (instance_Stsreg01b_7) {
			instance_Stsreg01 = (instance_Stsreg01 | 32768);
			goto verificationLoop_VerificationLoop_l289;
		}
		if ((! instance_Stsreg01b_7)) {
			instance_Stsreg01 = (instance_Stsreg01 & 32767);
			goto verificationLoop_VerificationLoop_l289;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x17: {
		if (instance_Stsreg02b_8) {
			instance_Stsreg02 = (instance_Stsreg02 | 1);
			goto verificationLoop_VerificationLoop_l290;
		}
		if ((! instance_Stsreg02b_8)) {
			instance_Stsreg02 = (instance_Stsreg02 & 65534);
			goto verificationLoop_VerificationLoop_l290;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x18: {
		if (instance_Stsreg02b_9) {
			instance_Stsreg02 = (instance_Stsreg02 | 2);
			goto verificationLoop_VerificationLoop_l291;
		}
		if ((! instance_Stsreg02b_9)) {
			instance_Stsreg02 = (instance_Stsreg02 & 65533);
			goto verificationLoop_VerificationLoop_l291;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x19: {
		if (instance_Stsreg02b_10) {
			instance_Stsreg02 = (instance_Stsreg02 | 4);
			goto verificationLoop_VerificationLoop_l292;
		}
		if ((! instance_Stsreg02b_10)) {
			instance_Stsreg02 = (instance_Stsreg02 & 65531);
			goto verificationLoop_VerificationLoop_l292;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x20: {
		if (instance_Stsreg02b_11) {
			instance_Stsreg02 = (instance_Stsreg02 | 8);
			goto verificationLoop_VerificationLoop_l293;
		}
		if ((! instance_Stsreg02b_11)) {
			instance_Stsreg02 = (instance_Stsreg02 & 65527);
			goto verificationLoop_VerificationLoop_l293;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x21: {
		if (instance_Stsreg02b_12) {
			instance_Stsreg02 = (instance_Stsreg02 | 16);
			goto verificationLoop_VerificationLoop_l294;
		}
		if ((! instance_Stsreg02b_12)) {
			instance_Stsreg02 = (instance_Stsreg02 & 65519);
			goto verificationLoop_VerificationLoop_l294;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x22: {
		if (instance_Stsreg02b_13) {
			instance_Stsreg02 = (instance_Stsreg02 | 32);
			goto verificationLoop_VerificationLoop_l295;
		}
		if ((! instance_Stsreg02b_13)) {
			instance_Stsreg02 = (instance_Stsreg02 & 65503);
			goto verificationLoop_VerificationLoop_l295;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x23: {
		if (instance_Stsreg02b_14) {
			instance_Stsreg02 = (instance_Stsreg02 | 64);
			goto verificationLoop_VerificationLoop_l296;
		}
		if ((! instance_Stsreg02b_14)) {
			instance_Stsreg02 = (instance_Stsreg02 & 65471);
			goto verificationLoop_VerificationLoop_l296;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x24: {
		if (instance_Stsreg02b_15) {
			instance_Stsreg02 = (instance_Stsreg02 | 128);
			goto verificationLoop_VerificationLoop_l297;
		}
		if ((! instance_Stsreg02b_15)) {
			instance_Stsreg02 = (instance_Stsreg02 & 65407);
			goto verificationLoop_VerificationLoop_l297;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x25: {
		if (instance_Stsreg02b_0) {
			instance_Stsreg02 = (instance_Stsreg02 | 256);
			goto verificationLoop_VerificationLoop_l298;
		}
		if ((! instance_Stsreg02b_0)) {
			instance_Stsreg02 = (instance_Stsreg02 & 65279);
			goto verificationLoop_VerificationLoop_l298;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x26: {
		if (instance_Stsreg02b_1) {
			instance_Stsreg02 = (instance_Stsreg02 | 512);
			goto verificationLoop_VerificationLoop_l299;
		}
		if ((! instance_Stsreg02b_1)) {
			instance_Stsreg02 = (instance_Stsreg02 & 65023);
			goto verificationLoop_VerificationLoop_l299;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x27: {
		if (instance_Stsreg02b_2) {
			instance_Stsreg02 = (instance_Stsreg02 | 1024);
			goto verificationLoop_VerificationLoop_l300;
		}
		if ((! instance_Stsreg02b_2)) {
			instance_Stsreg02 = (instance_Stsreg02 & 64511);
			goto verificationLoop_VerificationLoop_l300;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x28: {
		if (instance_Stsreg02b_3) {
			instance_Stsreg02 = (instance_Stsreg02 | 2048);
			goto verificationLoop_VerificationLoop_l301;
		}
		if ((! instance_Stsreg02b_3)) {
			instance_Stsreg02 = (instance_Stsreg02 & 63487);
			goto verificationLoop_VerificationLoop_l301;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x29: {
		if (instance_Stsreg02b_4) {
			instance_Stsreg02 = (instance_Stsreg02 | 4096);
			goto verificationLoop_VerificationLoop_l302;
		}
		if ((! instance_Stsreg02b_4)) {
			instance_Stsreg02 = (instance_Stsreg02 & 61439);
			goto verificationLoop_VerificationLoop_l302;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x30: {
		if (instance_Stsreg02b_5) {
			instance_Stsreg02 = (instance_Stsreg02 | 8192);
			goto verificationLoop_VerificationLoop_l303;
		}
		if ((! instance_Stsreg02b_5)) {
			instance_Stsreg02 = (instance_Stsreg02 & 57343);
			goto verificationLoop_VerificationLoop_l303;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x31: {
		if (instance_Stsreg02b_6) {
			instance_Stsreg02 = (instance_Stsreg02 | 16384);
			goto verificationLoop_VerificationLoop_l304;
		}
		if ((! instance_Stsreg02b_6)) {
			instance_Stsreg02 = (instance_Stsreg02 & 49151);
			goto verificationLoop_VerificationLoop_l304;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_x32: {
		if (instance_Stsreg02b_7) {
			instance_Stsreg02 = (instance_Stsreg02 | 32768);
			goto verificationLoop_VerificationLoop_l305;
		}
		if ((! instance_Stsreg02b_7)) {
			instance_Stsreg02 = (instance_Stsreg02 & 32767);
			goto verificationLoop_VerificationLoop_l305;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init: {
		if (((R_EDGE_inlined_1_new == true) && (R_EDGE_inlined_1_old == false))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l1;
		}
		if ((! ((R_EDGE_inlined_1_new == true) && (R_EDGE_inlined_1_old == false)))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l4;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l1: {
			R_EDGE_inlined_1_RET_VAL = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l2: {
			R_EDGE_inlined_1_old = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l3: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l4: {
			R_EDGE_inlined_1_RET_VAL = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l5: {
			R_EDGE_inlined_1_old = R_EDGE_inlined_1_new;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l6: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l7: {
			instance_MAuMoR_old = R_EDGE_inlined_1_old;
			instance_E_MAuMoR = R_EDGE_inlined_1_RET_VAL;
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init1: {
		if (((R_EDGE_inlined_2_new == true) && (R_EDGE_inlined_2_old == false))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l11;
		}
		if ((! ((R_EDGE_inlined_2_new == true) && (R_EDGE_inlined_2_old == false)))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l41;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l11: {
			R_EDGE_inlined_2_RET_VAL = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l21: {
			R_EDGE_inlined_2_old = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l31;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l31: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l71;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l41: {
			R_EDGE_inlined_2_RET_VAL = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l51;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l51: {
			R_EDGE_inlined_2_old = R_EDGE_inlined_2_new;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l61;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l61: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l71;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l71: {
			instance_MMMoR_old = R_EDGE_inlined_2_old;
			instance_E_MMMoR = R_EDGE_inlined_2_RET_VAL;
			goto verificationLoop_VerificationLoop_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init2: {
		if (((R_EDGE_inlined_3_new == true) && (R_EDGE_inlined_3_old == false))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l12;
		}
		if ((! ((R_EDGE_inlined_3_new == true) && (R_EDGE_inlined_3_old == false)))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l42;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l12: {
			R_EDGE_inlined_3_RET_VAL = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l22: {
			R_EDGE_inlined_3_old = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l32;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l32: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l72;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l42: {
			R_EDGE_inlined_3_RET_VAL = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l52: {
			R_EDGE_inlined_3_old = R_EDGE_inlined_3_new;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l62;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l62: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l72;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l72: {
			instance_MFoMoR_old = R_EDGE_inlined_3_old;
			instance_E_MFoMoR = R_EDGE_inlined_3_RET_VAL;
			goto verificationLoop_VerificationLoop_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init3: {
		if (((R_EDGE_inlined_4_new == true) && (R_EDGE_inlined_4_old == false))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l13;
		}
		if ((! ((R_EDGE_inlined_4_new == true) && (R_EDGE_inlined_4_old == false)))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l43;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l13: {
			R_EDGE_inlined_4_RET_VAL = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l23;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l23: {
			R_EDGE_inlined_4_old = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l33;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l33: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l73;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l43: {
			R_EDGE_inlined_4_RET_VAL = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l53;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l53: {
			R_EDGE_inlined_4_old = R_EDGE_inlined_4_new;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l63;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l63: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l73;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l73: {
			instance_MSoftLDR_old = R_EDGE_inlined_4_old;
			instance_E_MSoftLDR = R_EDGE_inlined_4_RET_VAL;
			goto verificationLoop_VerificationLoop_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init4: {
		if (((R_EDGE_inlined_5_new == true) && (R_EDGE_inlined_5_old == false))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l14;
		}
		if ((! ((R_EDGE_inlined_5_new == true) && (R_EDGE_inlined_5_old == false)))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l44;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l14: {
			R_EDGE_inlined_5_RET_VAL = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l24;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l24: {
			R_EDGE_inlined_5_old = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l34;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l34: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l74;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l44: {
			R_EDGE_inlined_5_RET_VAL = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l54;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l54: {
			R_EDGE_inlined_5_old = R_EDGE_inlined_5_new;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l64;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l64: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l74;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l74: {
			instance_MOnR_old = R_EDGE_inlined_5_old;
			instance_E_MOnR = R_EDGE_inlined_5_RET_VAL;
			goto verificationLoop_VerificationLoop_l6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init5: {
		if (((R_EDGE_inlined_6_new == true) && (R_EDGE_inlined_6_old == false))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l15;
		}
		if ((! ((R_EDGE_inlined_6_new == true) && (R_EDGE_inlined_6_old == false)))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l45;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l15: {
			R_EDGE_inlined_6_RET_VAL = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l25;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l25: {
			R_EDGE_inlined_6_old = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l35;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l35: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l75;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l45: {
			R_EDGE_inlined_6_RET_VAL = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l55;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l55: {
			R_EDGE_inlined_6_old = R_EDGE_inlined_6_new;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l65;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l65: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l75;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l75: {
			instance_MOffR_old = R_EDGE_inlined_6_old;
			instance_E_MOffR = R_EDGE_inlined_6_RET_VAL;
			goto verificationLoop_VerificationLoop_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init6: {
		if (((R_EDGE_inlined_7_new == true) && (R_EDGE_inlined_7_old == false))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l16;
		}
		if ((! ((R_EDGE_inlined_7_new == true) && (R_EDGE_inlined_7_old == false)))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l46;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l16: {
			R_EDGE_inlined_7_RET_VAL = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l26;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l26: {
			R_EDGE_inlined_7_old = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l36;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l36: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l76;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l46: {
			R_EDGE_inlined_7_RET_VAL = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l56;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l56: {
			R_EDGE_inlined_7_old = R_EDGE_inlined_7_new;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l66;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l66: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l76;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l76: {
			instance_MNewPosR_old = R_EDGE_inlined_7_old;
			instance_E_MNewPosR = R_EDGE_inlined_7_RET_VAL;
			goto verificationLoop_VerificationLoop_l8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init7: {
		if (((R_EDGE_inlined_8_new == true) && (R_EDGE_inlined_8_old == false))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l17;
		}
		if ((! ((R_EDGE_inlined_8_new == true) && (R_EDGE_inlined_8_old == false)))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l47;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l17: {
			R_EDGE_inlined_8_RET_VAL = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l27;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l27: {
			R_EDGE_inlined_8_old = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l37;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l37: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l77;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l47: {
			R_EDGE_inlined_8_RET_VAL = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l57;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l57: {
			R_EDGE_inlined_8_old = R_EDGE_inlined_8_new;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l67;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l67: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l77;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l77: {
			instance_MStpInR_old = R_EDGE_inlined_8_old;
			instance_E_MStpInR = R_EDGE_inlined_8_RET_VAL;
			goto verificationLoop_VerificationLoop_l9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init8: {
		if (((R_EDGE_inlined_9_new == true) && (R_EDGE_inlined_9_old == false))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l18;
		}
		if ((! ((R_EDGE_inlined_9_new == true) && (R_EDGE_inlined_9_old == false)))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l48;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l18: {
			R_EDGE_inlined_9_RET_VAL = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l28;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l28: {
			R_EDGE_inlined_9_old = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l38;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l38: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l78;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l48: {
			R_EDGE_inlined_9_RET_VAL = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l58;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l58: {
			R_EDGE_inlined_9_old = R_EDGE_inlined_9_new;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l68;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l68: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l78;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l78: {
			instance_MStpDeR_old = R_EDGE_inlined_9_old;
			instance_E_MStpDeR = R_EDGE_inlined_9_RET_VAL;
			goto verificationLoop_VerificationLoop_l10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init9: {
		if (((R_EDGE_inlined_10_new == true) && (R_EDGE_inlined_10_old == false))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l19;
		}
		if ((! ((R_EDGE_inlined_10_new == true) && (R_EDGE_inlined_10_old == false)))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l49;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l19: {
			R_EDGE_inlined_10_RET_VAL = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l29;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l29: {
			R_EDGE_inlined_10_old = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l39;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l39: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l79;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l49: {
			R_EDGE_inlined_10_RET_VAL = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l59;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l59: {
			R_EDGE_inlined_10_old = R_EDGE_inlined_10_new;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l69;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l69: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l79;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l79: {
			instance_MEnRstartR_old = R_EDGE_inlined_10_old;
			instance_E_MEnRstartR = R_EDGE_inlined_10_RET_VAL;
			goto verificationLoop_VerificationLoop_l11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init10: {
		if (((R_EDGE_inlined_11_new == true) && (R_EDGE_inlined_11_old == false))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l110;
		}
		if ((! ((R_EDGE_inlined_11_new == true) && (R_EDGE_inlined_11_old == false)))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l410;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l110: {
			R_EDGE_inlined_11_RET_VAL = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l210;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l210: {
			R_EDGE_inlined_11_old = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l310;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l310: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l710;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l410: {
			R_EDGE_inlined_11_RET_VAL = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l510;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l510: {
			R_EDGE_inlined_11_old = R_EDGE_inlined_11_new;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l610;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l610: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l710;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l710: {
			instance_MAlAckR_old = R_EDGE_inlined_11_old;
			instance_E_MAlAckR = R_EDGE_inlined_11_RET_VAL;
			goto verificationLoop_VerificationLoop_l12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init11: {
		if (((R_EDGE_inlined_12_new == true) && (R_EDGE_inlined_12_old == false))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l111;
		}
		if ((! ((R_EDGE_inlined_12_new == true) && (R_EDGE_inlined_12_old == false)))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l411;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l111: {
			R_EDGE_inlined_12_RET_VAL = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l211;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l211: {
			R_EDGE_inlined_12_old = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l311;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l311: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l711;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l411: {
			R_EDGE_inlined_12_RET_VAL = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l511;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l511: {
			R_EDGE_inlined_12_old = R_EDGE_inlined_12_new;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l611;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l611: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l711;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l711: {
			instance_AuAuMoR_old = R_EDGE_inlined_12_old;
			instance_E_AuAuMoR = R_EDGE_inlined_12_RET_VAL;
			goto verificationLoop_VerificationLoop_l22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init12: {
		if (((R_EDGE_inlined_13_new == true) && (R_EDGE_inlined_13_old == false))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l112;
		}
		if ((! ((R_EDGE_inlined_13_new == true) && (R_EDGE_inlined_13_old == false)))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l412;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l112: {
			R_EDGE_inlined_13_RET_VAL = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l212;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l212: {
			R_EDGE_inlined_13_old = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l312;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l312: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l712;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l412: {
			R_EDGE_inlined_13_RET_VAL = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l512;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l512: {
			R_EDGE_inlined_13_old = R_EDGE_inlined_13_new;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l612;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l612: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l712;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l712: {
			instance_AuAlAckR_old = R_EDGE_inlined_13_old;
			instance_E_AuAlAckR = R_EDGE_inlined_13_RET_VAL;
			goto verificationLoop_VerificationLoop_l23;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init13: {
		if (((R_EDGE_inlined_14_new == true) && (R_EDGE_inlined_14_old == false))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l113;
		}
		if ((! ((R_EDGE_inlined_14_new == true) && (R_EDGE_inlined_14_old == false)))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l413;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l113: {
			R_EDGE_inlined_14_RET_VAL = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l213;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l213: {
			R_EDGE_inlined_14_old = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l313;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l313: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l713;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l413: {
			R_EDGE_inlined_14_RET_VAL = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l513;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l513: {
			R_EDGE_inlined_14_old = R_EDGE_inlined_14_new;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l613;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l613: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l713;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l713: {
			instance_StartI_old = R_EDGE_inlined_14_old;
			instance_E_StartI = R_EDGE_inlined_14_RET_VAL;
			goto verificationLoop_VerificationLoop_l24;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init14: {
		if (((R_EDGE_inlined_15_new == true) && (R_EDGE_inlined_15_old == false))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l114;
		}
		if ((! ((R_EDGE_inlined_15_new == true) && (R_EDGE_inlined_15_old == false)))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l414;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l114: {
			R_EDGE_inlined_15_RET_VAL = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l214;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l214: {
			R_EDGE_inlined_15_old = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l314;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l314: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l714;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l414: {
			R_EDGE_inlined_15_RET_VAL = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l514;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l514: {
			R_EDGE_inlined_15_old = R_EDGE_inlined_15_new;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l614;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l614: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l714;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l714: {
			instance_TStopI_old = R_EDGE_inlined_15_old;
			instance_E_TStopI = R_EDGE_inlined_15_RET_VAL;
			goto verificationLoop_VerificationLoop_l25;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init15: {
		if (((R_EDGE_inlined_16_new == true) && (R_EDGE_inlined_16_old == false))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l115;
		}
		if ((! ((R_EDGE_inlined_16_new == true) && (R_EDGE_inlined_16_old == false)))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l415;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l115: {
			R_EDGE_inlined_16_RET_VAL = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l215;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l215: {
			R_EDGE_inlined_16_old = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l315;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l315: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l715;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l415: {
			R_EDGE_inlined_16_RET_VAL = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l515;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l515: {
			R_EDGE_inlined_16_old = R_EDGE_inlined_16_new;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l615;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l615: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l715;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l715: {
			instance_FuStopI_old = R_EDGE_inlined_16_old;
			instance_E_FuStopI = R_EDGE_inlined_16_RET_VAL;
			goto verificationLoop_VerificationLoop_l26;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init16: {
		if (((R_EDGE_inlined_17_new == true) && (R_EDGE_inlined_17_old == false))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l116;
		}
		if ((! ((R_EDGE_inlined_17_new == true) && (R_EDGE_inlined_17_old == false)))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l416;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l116: {
			R_EDGE_inlined_17_RET_VAL = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l216;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l216: {
			R_EDGE_inlined_17_old = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l316;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l316: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l716;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l416: {
			R_EDGE_inlined_17_RET_VAL = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l516;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l516: {
			R_EDGE_inlined_17_old = R_EDGE_inlined_17_new;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l616;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l616: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l716;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l716: {
			instance_Al_old = R_EDGE_inlined_17_old;
			instance_E_Al = R_EDGE_inlined_17_RET_VAL;
			goto verificationLoop_VerificationLoop_l27;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init17: {
		if ((ABS_REAL_inlined_18_in > 0.0)) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l117;
		}
		if ((! (ABS_REAL_inlined_18_in > 0.0))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l317;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l117: {
			ABS_REAL_inlined_18_RET_VAL = ABS_REAL_inlined_18_in;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l217;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l217: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l517;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l317: {
			ABS_REAL_inlined_18_RET_VAL = ((- 1.0) * ABS_REAL_inlined_18_in);
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l417;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l417: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l517;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l517: {
			instance____nested_ret_val2 = ABS_REAL_inlined_18_RET_VAL;
			goto verificationLoop_VerificationLoop_l107;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init18: {
		if ((ABS_REAL_inlined_19_in > 0.0)) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l118;
		}
		if ((! (ABS_REAL_inlined_19_in > 0.0))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l318;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l118: {
			ABS_REAL_inlined_19_RET_VAL = ABS_REAL_inlined_19_in;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l218;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l218: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l518;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l318: {
			ABS_REAL_inlined_19_RET_VAL = ((- 1.0) * ABS_REAL_inlined_19_in);
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l418;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l418: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l518;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l518: {
			instance____nested_ret_val3 = ABS_REAL_inlined_19_RET_VAL;
			goto verificationLoop_VerificationLoop_l112;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init19: {
			instance_ROC_LIM_OUTV = instance_ROC_LIM_INV;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l119;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l119: {
		if ((instance_ROC_LIM_OUTV > instance_ROC_LIM_H_LM)) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l219;
		}
		if (((! (instance_ROC_LIM_OUTV > instance_ROC_LIM_H_LM)) && (instance_ROC_LIM_OUTV < instance_ROC_LIM_L_LM))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l419;
		}
		if (((! (instance_ROC_LIM_OUTV > instance_ROC_LIM_H_LM)) && (! ((! (instance_ROC_LIM_OUTV > instance_ROC_LIM_H_LM)) && (instance_ROC_LIM_OUTV < instance_ROC_LIM_L_LM))))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l617;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l219: {
			instance_ROC_LIM_OUTV = instance_ROC_LIM_H_LM;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l319;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l319: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l717;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l419: {
			instance_ROC_LIM_OUTV = instance_ROC_LIM_L_LM;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l519;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l519: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l717;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l617: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l717;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l717: {
			goto verificationLoop_VerificationLoop_l212;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init20: {
		if ((ABS_REAL_inlined_20_in > 0.0)) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l120;
		}
		if ((! (ABS_REAL_inlined_20_in > 0.0))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l320;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l120: {
			ABS_REAL_inlined_20_RET_VAL = ABS_REAL_inlined_20_in;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l220;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l220: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l520;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l320: {
			ABS_REAL_inlined_20_RET_VAL = ((- 1.0) * ABS_REAL_inlined_20_in);
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l420;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l420: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l520;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l520: {
			instance____nested_ret_val4 = ABS_REAL_inlined_20_RET_VAL;
			goto verificationLoop_VerificationLoop_l217;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init21: {
		if ((ABS_REAL_inlined_21_in > 0.0)) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l121;
		}
		if ((! (ABS_REAL_inlined_21_in > 0.0))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l321;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l121: {
			ABS_REAL_inlined_21_RET_VAL = ABS_REAL_inlined_21_in;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l221;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l221: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l521;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l321: {
			ABS_REAL_inlined_21_RET_VAL = ((- 1.0) * ABS_REAL_inlined_21_in);
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l421;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l421: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l521;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l521: {
			instance____nested_ret_val5 = ABS_REAL_inlined_21_RET_VAL;
			goto verificationLoop_VerificationLoop_l219;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init22: {
		if ((instance_Timer_Warning_IN == false)) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l122;
		}
		if ((! (instance_Timer_Warning_IN == false))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l522;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l122: {
			instance_Timer_Warning_Q = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l222;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l222: {
			instance_Timer_Warning_ET = 0;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l322;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l322: {
			instance_Timer_Warning_running = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l422;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l422: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l251;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l522: {
		if ((instance_Timer_Warning_running == false)) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l618;
		}
		if ((! (instance_Timer_Warning_running == false))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l141;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l618: {
			instance_Timer_Warning_start = __GLOBAL_TIME;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l718;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l718: {
			instance_Timer_Warning_running = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l8: {
			instance_Timer_Warning_ET = 0;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l9: {
		if ((instance_Timer_Warning_PT == 0)) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l10;
		}
		if ((! (instance_Timer_Warning_PT == 0))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l123;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l10: {
			instance_Timer_Warning_Q = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l1110;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l1110: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l131;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l123: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l131;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l131: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l241;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l141: {
		if ((! ((__GLOBAL_TIME - (instance_Timer_Warning_start + instance_Timer_Warning_PT)) >= 0))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l151;
		}
		if ((! (! ((__GLOBAL_TIME - (instance_Timer_Warning_start + instance_Timer_Warning_PT)) >= 0)))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l20;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l151: {
		if ((! instance_Timer_Warning_Q)) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l161;
		}
		if ((! (! instance_Timer_Warning_Q))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l181;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l161: {
			instance_Timer_Warning_ET = (__GLOBAL_TIME - instance_Timer_Warning_start);
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l171;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l171: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l191;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l181: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l191;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l191: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l231;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l20: {
			instance_Timer_Warning_Q = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l2110;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l2110: {
			instance_Timer_Warning_ET = instance_Timer_Warning_PT;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l223;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l223: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l231;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l231: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l241;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l241: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l251;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l251: {
			goto verificationLoop_VerificationLoop_l220;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_init23: {
		if ((DETECT_EDGE_new != DETECT_EDGE_old)) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l124;
		}
		if ((! (DETECT_EDGE_new != DETECT_EDGE_old))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l101;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l124: {
		if ((DETECT_EDGE_new == true)) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l224;
		}
		if ((! (DETECT_EDGE_new == true))) {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l523;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l224: {
			DETECT_EDGE_re = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l323;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l323: {
			DETECT_EDGE_fe = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l423;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l423: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l81;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l523: {
			DETECT_EDGE_re = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l619;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l619: {
			DETECT_EDGE_fe = true;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l719;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l719: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l81;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l81: {
			DETECT_EDGE_old = DETECT_EDGE_new;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l91;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l91: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l132;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l101: {
			DETECT_EDGE_re = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l1111;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l1111: {
			DETECT_EDGE_fe = false;
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l125;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l125: {
			goto verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l132;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_instance_CPC_FB_ANALOG_l132: {
			instance_AlUnAck_old = DETECT_EDGE_old;
			instance_RE_AlUnAck = DETECT_EDGE_re;
			instance_FE_AlUnAck = DETECT_EDGE_fe;
			goto verificationLoop_VerificationLoop_l306;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
