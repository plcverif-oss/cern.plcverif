#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Variables
int16_t extractNested1_j = 0;
int16_t extractNested1____nested_ret_val1 = 0;
int16_t extractNested2_j = 0;
int16_t extractNested2____nested_ret_val2 = 0;
int16_t extractNested2____nested_ret_val3 = 0;
int16_t extractNested3_j = 0;
int16_t extractNested3____nested_ret_val4 = 0;
int16_t extractNested3____nested_ret_val5 = 0;
int16_t extractNested4_j = 0;
int16_t extractNested4____nested_ret_val6 = 0;
int16_t MAX0_in1 = 0;
int16_t MAX0_in2 = 0;
int16_t MAX0_RET_VAL = 0;
int16_t MAX0_inlined_1_in1 = 0;
int16_t MAX0_inlined_1_in2 = 0;
int16_t MAX0_inlined_1_RET_VAL = 0;
int16_t MAX0_inlined_2_in1 = 0;
int16_t MAX0_inlined_2_in2 = 0;
int16_t MAX0_inlined_2_RET_VAL = 0;
int16_t MAX0_inlined_3_in1 = 0;
int16_t MAX0_inlined_3_in2 = 0;
int16_t MAX0_inlined_3_RET_VAL = 0;
int16_t MAX0_inlined_4_in1 = 0;
int16_t MAX0_inlined_4_in2 = 0;
int16_t MAX0_inlined_4_RET_VAL = 0;
int16_t MAX0_inlined_5_in1 = 0;
int16_t MAX0_inlined_5_in2 = 0;
int16_t MAX0_inlined_5_RET_VAL = 0;
int16_t MAX0_inlined_6_in1 = 0;
int16_t MAX0_inlined_6_in2 = 0;
int16_t MAX0_inlined_6_RET_VAL = 0;
int16_t MAX0_inlined_7_in1 = 0;
int16_t MAX0_inlined_7_in2 = 0;
int16_t MAX0_inlined_7_RET_VAL = 0;
int16_t MAX0_inlined_8_in1 = 0;
int16_t MAX0_inlined_8_in2 = 0;
int16_t MAX0_inlined_8_RET_VAL = 0;
int16_t MAX0_inlined_9_in1 = 0;
int16_t MAX0_inlined_9_in2 = 0;
int16_t MAX0_inlined_9_RET_VAL = 0;
uint16_t __assertion_error = 0;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			goto verificationLoop_VerificationLoop_extractNested_OB1_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			goto verificationLoop_VerificationLoop_extractNested_OB1_init1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
			goto verificationLoop_VerificationLoop_extractNested_OB1_init2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l3: {
			goto verificationLoop_VerificationLoop_extractNested_OB1_init3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l4: {
			goto callEnd;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_init: {
			extractNested1____nested_ret_val1 = 0;
			goto verificationLoop_VerificationLoop_extractNested_OB1_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_l1: {
			MAX0_inlined_1_in1 = 2;
			MAX0_inlined_1_in2 = 3;
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested1_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_l2: {
			MAX0_inlined_2_in1 = 1;
			MAX0_inlined_2_in2 = extractNested1____nested_ret_val1;
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested1_init1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_l3: {
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested1_init: {
		if ((MAX0_inlined_1_in1 > MAX0_inlined_1_in2)) {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested1_l1;
		}
		if ((! (MAX0_inlined_1_in1 > MAX0_inlined_1_in2))) {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested1_l3;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested1_l1: {
			MAX0_inlined_1_RET_VAL = MAX0_inlined_1_in1;
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested1_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested1_l2: {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested1_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested1_l3: {
			MAX0_inlined_1_RET_VAL = MAX0_inlined_1_in2;
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested1_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested1_l4: {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested1_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested1_l5: {
			extractNested1____nested_ret_val1 = MAX0_inlined_1_RET_VAL;
			goto verificationLoop_VerificationLoop_extractNested_OB1_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested1_init1: {
		if ((MAX0_inlined_2_in1 > MAX0_inlined_2_in2)) {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested1_l11;
		}
		if ((! (MAX0_inlined_2_in1 > MAX0_inlined_2_in2))) {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested1_l31;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested1_l11: {
			MAX0_inlined_2_RET_VAL = MAX0_inlined_2_in1;
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested1_l21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested1_l21: {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested1_l51;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested1_l31: {
			MAX0_inlined_2_RET_VAL = MAX0_inlined_2_in2;
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested1_l41;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested1_l41: {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested1_l51;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested1_l51: {
			extractNested1_j = MAX0_inlined_2_RET_VAL;
			goto verificationLoop_VerificationLoop_extractNested_OB1_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_init1: {
			extractNested2____nested_ret_val2 = 0;
			extractNested2____nested_ret_val3 = 0;
			goto verificationLoop_VerificationLoop_extractNested_OB1_l11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_l11: {
			MAX0_inlined_3_in1 = 3;
			MAX0_inlined_3_in2 = 4;
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_l21: {
			MAX0_inlined_4_in1 = 2;
			MAX0_inlined_4_in2 = ((2 * extractNested2____nested_ret_val3) + 6);
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_init1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_l31: {
			MAX0_inlined_5_in1 = 1;
			MAX0_inlined_5_in2 = (extractNested2____nested_ret_val2 + 5);
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_init2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_l4: {
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_init: {
		if ((MAX0_inlined_3_in1 > MAX0_inlined_3_in2)) {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l1;
		}
		if ((! (MAX0_inlined_3_in1 > MAX0_inlined_3_in2))) {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l3;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l1: {
			MAX0_inlined_3_RET_VAL = MAX0_inlined_3_in1;
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l2: {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l3: {
			MAX0_inlined_3_RET_VAL = MAX0_inlined_3_in2;
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l4: {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l5: {
			extractNested2____nested_ret_val3 = MAX0_inlined_3_RET_VAL;
			goto verificationLoop_VerificationLoop_extractNested_OB1_l21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_init1: {
		if ((MAX0_inlined_4_in1 > MAX0_inlined_4_in2)) {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l11;
		}
		if ((! (MAX0_inlined_4_in1 > MAX0_inlined_4_in2))) {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l31;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l11: {
			MAX0_inlined_4_RET_VAL = MAX0_inlined_4_in1;
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l21: {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l51;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l31: {
			MAX0_inlined_4_RET_VAL = MAX0_inlined_4_in2;
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l41;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l41: {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l51;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l51: {
			extractNested2____nested_ret_val2 = MAX0_inlined_4_RET_VAL;
			goto verificationLoop_VerificationLoop_extractNested_OB1_l31;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_init2: {
		if ((MAX0_inlined_5_in1 > MAX0_inlined_5_in2)) {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l12;
		}
		if ((! (MAX0_inlined_5_in1 > MAX0_inlined_5_in2))) {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l32;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l12: {
			MAX0_inlined_5_RET_VAL = MAX0_inlined_5_in1;
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l22: {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l32: {
			MAX0_inlined_5_RET_VAL = MAX0_inlined_5_in2;
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l42;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l42: {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested2_l52: {
			extractNested2_j = MAX0_inlined_5_RET_VAL;
			goto verificationLoop_VerificationLoop_extractNested_OB1_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_init2: {
			extractNested3____nested_ret_val4 = 0;
			extractNested3____nested_ret_val5 = 0;
			goto verificationLoop_VerificationLoop_extractNested_OB1_l12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_l12: {
			MAX0_inlined_6_in1 = 11;
			MAX0_inlined_6_in2 = 22;
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_l22: {
			MAX0_inlined_7_in1 = 33;
			MAX0_inlined_7_in2 = 44;
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_init1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_l32: {
			MAX0_inlined_8_in1 = extractNested3____nested_ret_val4;
			MAX0_inlined_8_in2 = extractNested3____nested_ret_val5;
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_init2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_l41: {
			goto verificationLoop_VerificationLoop_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_init: {
		if ((MAX0_inlined_6_in1 > MAX0_inlined_6_in2)) {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l1;
		}
		if ((! (MAX0_inlined_6_in1 > MAX0_inlined_6_in2))) {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l3;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l1: {
			MAX0_inlined_6_RET_VAL = MAX0_inlined_6_in1;
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l2: {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l3: {
			MAX0_inlined_6_RET_VAL = MAX0_inlined_6_in2;
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l4: {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l5: {
			extractNested3____nested_ret_val4 = MAX0_inlined_6_RET_VAL;
			goto verificationLoop_VerificationLoop_extractNested_OB1_l22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_init1: {
		if ((MAX0_inlined_7_in1 > MAX0_inlined_7_in2)) {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l11;
		}
		if ((! (MAX0_inlined_7_in1 > MAX0_inlined_7_in2))) {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l31;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l11: {
			MAX0_inlined_7_RET_VAL = MAX0_inlined_7_in1;
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l21: {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l51;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l31: {
			MAX0_inlined_7_RET_VAL = MAX0_inlined_7_in2;
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l41;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l41: {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l51;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l51: {
			extractNested3____nested_ret_val5 = MAX0_inlined_7_RET_VAL;
			goto verificationLoop_VerificationLoop_extractNested_OB1_l32;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_init2: {
		if ((MAX0_inlined_8_in1 > MAX0_inlined_8_in2)) {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l12;
		}
		if ((! (MAX0_inlined_8_in1 > MAX0_inlined_8_in2))) {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l32;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l12: {
			MAX0_inlined_8_RET_VAL = MAX0_inlined_8_in1;
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l22: {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l32: {
			MAX0_inlined_8_RET_VAL = MAX0_inlined_8_in2;
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l42;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l42: {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested3_l52: {
			extractNested3_j = MAX0_inlined_8_RET_VAL;
			goto verificationLoop_VerificationLoop_extractNested_OB1_l41;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_init3: {
			extractNested4____nested_ret_val6 = 0;
			goto verificationLoop_VerificationLoop_extractNested_OB1_l13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_l13: {
			MAX0_inlined_9_in1 = 0;
			MAX0_inlined_9_in2 = 1;
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested4_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_l23: {
			extractNested4_j = (extractNested4____nested_ret_val6 + 10);
			goto verificationLoop_VerificationLoop_extractNested_OB1_l33;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_l33: {
			goto verificationLoop_VerificationLoop_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested4_init: {
		if ((MAX0_inlined_9_in1 > MAX0_inlined_9_in2)) {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested4_l1;
		}
		if ((! (MAX0_inlined_9_in1 > MAX0_inlined_9_in2))) {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested4_l3;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested4_l1: {
			MAX0_inlined_9_RET_VAL = MAX0_inlined_9_in1;
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested4_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested4_l2: {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested4_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested4_l3: {
			MAX0_inlined_9_RET_VAL = MAX0_inlined_9_in2;
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested4_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested4_l4: {
			goto verificationLoop_VerificationLoop_extractNested_OB1_extractNested4_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_extractNested_OB1_extractNested4_l5: {
			extractNested4____nested_ret_val6 = MAX0_inlined_9_RET_VAL;
			goto verificationLoop_VerificationLoop_extractNested_OB1_l23;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
