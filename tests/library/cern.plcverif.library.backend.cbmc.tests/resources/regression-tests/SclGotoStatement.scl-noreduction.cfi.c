#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Variables
bool MX0_0 = false;
bool MX0_1 = false;
bool MX0_2 = false;
bool IX0_3 = false;
bool IX0_4 = false;
uint16_t __assertion_error = 0;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			IX0_3 = nondet_bool();
			IX0_4 = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			MX0_0 = true;
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			MX0_1 = true;
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
			MX0_2 = true;
			goto verificationLoop_VerificationLoop_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l3: {
		if (IX0_3) {
			goto verificationLoop_VerificationLoop_l4;
		}
		if (((! IX0_3) && IX0_4)) {
			goto verificationLoop_VerificationLoop_l6;
		}
		if (((! IX0_3) && (! ((! IX0_3) && IX0_4)))) {
			goto verificationLoop_VerificationLoop_l8;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l4: {
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l5: {
			goto verificationLoop_VerificationLoop_l9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l6: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l7: {
			goto verificationLoop_VerificationLoop_l9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l8: {
			goto verificationLoop_VerificationLoop_l9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l9: {
			goto callEnd;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
