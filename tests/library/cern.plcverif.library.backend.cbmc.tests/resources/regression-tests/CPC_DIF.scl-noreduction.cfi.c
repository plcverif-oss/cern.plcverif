#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Variables
float instance_INV = 0.0;
int32_t instance_Td = 0;
int32_t instance_Tds = 0;
int32_t instance_CYCLE = 0;
int32_t instance_TM_LAG = 0;
float instance_OUTV = 0.0;
float instance_FiltINV = 0.0;
float instance_LastFiltINV = 0.0;
uint16_t __assertion_error = 0;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			instance_CYCLE = nondet_int32_t();
			instance_INV = nondet_float();
			instance_TM_LAG = nondet_int32_t();
			instance_Td = nondet_int32_t();
			instance_Tds = nondet_int32_t();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			instance_FiltINV = (((((float) ((int32_t) instance_CYCLE)) * instance_INV) + (((float) ((int32_t) instance_Tds)) * instance_LastFiltINV)) / (((float) ((int32_t) instance_CYCLE)) + ((float) ((int32_t) instance_Tds))));
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			instance_OUTV = ((((float) ((int32_t) instance_Td)) * (instance_FiltINV - instance_LastFiltINV)) / ((float) ((int32_t) instance_CYCLE)));
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
			instance_LastFiltINV = instance_FiltINV;
			goto verificationLoop_VerificationLoop_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l3: {
			goto callEnd;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
