#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Root data structure
typedef struct {
	float in;
	float RET_VAL;
} __ABS_REAL;
typedef struct {
	uint16_t ParReg;
	float PMaxRan;
	float PMinRan;
	float PMStpInV;
	float PMStpDeV;
	float PMInSpd;
	float PMDeSpd;
	int32_t PWDt;
	float PWDb;
} __CPC_ANALOG_PARAM;
typedef struct {
	bool new;
	bool old;
	bool re;
	bool fe;
} __DETECT_EDGE;
typedef struct {
	float INV;
	float UPRLM_P;
	float DNRLM_P;
	float UPRLM_N;
	float DNRLM_N;
	float H_LM;
	float L_LM;
	float PV;
	float DF_OUTV;
	bool DFOUT_ON;
	bool TRACK;
	bool MAN_ON;
	bool COM_RST;
	int32_t CYCLE;
	float OUTV;
	bool QUPRLM_P;
	bool QDNRLM_P;
	bool QUPRLM_N;
	bool QDNRLM_N;
	bool QH_LM;
	bool QL_LM;
} __ROC_LIM;
typedef struct {
	bool new;
	bool old;
	bool RET_VAL;
} __R_EDGE;
typedef struct {
	int32_t PT;
	bool IN;
	bool Q;
	int32_t ET;
	bool running;
	int32_t start;
} __TON;
typedef struct {
	bool ParRegb[16];
	float PMaxRan;
	float PMinRan;
	float PMStpInV;
	float PMStpDeV;
	float PMInSpd;
	float PMDeSpd;
	int32_t PWDt;
	float PWDb;
} __anonymous_type;
typedef struct {
	float inc_rate;
	float dec_rate;
} __anonymous_type1;
typedef struct {
	float HFPos;
	float HAOut;
	float AuPosR;
	float AuInSpd;
	float AuDeSpd;
	float PliOff;
	float PliOn;
	float MPosR;
	uint16_t Manreg01;
	bool Manreg01b[16];
	bool HLD;
	bool HFOn;
	bool HOnR;
	bool HOffR;
	bool IOError;
	bool IOSimu;
	bool AlB;
	bool StartI;
	bool TStopI;
	bool FuStopI;
	bool Al;
	bool AuOnR;
	bool AuOffR;
	bool AuAuMoR;
	bool AuIhMMo;
	bool AuIhFoMo;
	bool AuAlAck;
	bool IhAuMRW;
	bool AuRstart;
	__CPC_ANALOG_PARAM PAnalog;
	__anonymous_type PAnalogb;
	uint16_t Stsreg01;
	bool Stsreg01b[16];
	uint16_t Stsreg02;
	bool Stsreg02b[16];
	float OutOV;
	float AuPosRSt;
	float MPosRSt;
	float PosSt;
	float PosRSt;
	bool OutOnOV;
	bool OutOnOVSt;
	bool MOnRSt;
	bool MOffRSt;
	bool AuOnRSt;
	bool AuOffRSt;
	bool HOnRSt;
	bool HOffRSt;
	bool OnSt;
	bool AuMoSt;
	bool MMoSt;
	bool LDSt;
	bool SoftLDSt;
	bool FoMoSt;
	bool IOErrorW;
	bool IOSimuW;
	bool AuMRW;
	bool PosW;
	bool StartISt;
	bool TStopISt;
	bool FuStopISt;
	bool AlSt;
	bool AlUnAck;
	bool AlBW;
	bool EnRstartSt;
	bool RdyStartSt;
	bool E_MAuMoR;
	bool E_MMMoR;
	bool E_MFoMoR;
	bool E_MOnR;
	bool E_MOffR;
	bool E_MAlAckR;
	bool E_StartI;
	bool E_TStopI;
	bool E_FuStopI;
	bool E_Al;
	bool E_AuAuMoR;
	bool E_AuAlAckR;
	bool E_MNewPosR;
	bool E_MStpInR;
	bool E_MStpDeR;
	bool E_MSoftLDR;
	bool E_MEnRstartR;
	bool RE_AlUnAck;
	bool FE_AlUnAck;
	bool MAuMoR_old;
	bool MMMoR_old;
	bool MFoMoR_old;
	bool MOnR_old;
	bool MOffR_old;
	bool MAlAckR_old;
	bool AuAuMoR_old;
	bool AuAlAckR_old;
	bool StartI_old;
	bool TStopI_old;
	bool FuStopI_old;
	bool Al_old;
	bool MNewPosR_old;
	bool MStpInR_old;
	bool MStpDeR_old;
	bool AlUnAck_old;
	bool MSoftLDR_old;
	bool MEnRstartR_old;
	float PosR;
	bool PFsPosOn;
	bool PHFOn;
	bool PHFPos;
	bool PPulse;
	bool PHLD;
	bool PHLDCmd;
	bool PEnRstart;
	bool PRstartFS;
	bool AnalogOnSt;
	bool AnalogOffSt;
	bool AuMoSt_aux;
	bool MMoSt_aux;
	bool FoMoSt_aux;
	bool SoftLDSt_aux;
	bool fullNotAcknowledged;
	bool InterlockR;
	__anonymous_type1 Ramp_parameters;
	__ROC_LIM ROC_LIM;
	int32_t Time_Warning;
	__TON Timer_Warning;
	float PulseWidth;
	int16_t FSIinc;
	int16_t TSIinc;
	int16_t SIinc;
	int16_t Alinc;
	bool WAlSt;
	bool WFuStopISt;
	bool WTStopISt;
	bool WStartISt;
} __CPC_FB_ANADO;

// Global variables
uint8_t T_CYCLE;
int32_t __GLOBAL_TIME;
__R_EDGE R_EDGE1;
__ABS_REAL ABS#REAL1;
__DETECT_EDGE DETECT_EDGE1;
__CPC_FB_ANADO instance;
__R_EDGE R_EDGE1_inlined_1;
__R_EDGE R_EDGE1_inlined_2;
__R_EDGE R_EDGE1_inlined_3;
__R_EDGE R_EDGE1_inlined_4;
__R_EDGE R_EDGE1_inlined_5;
__R_EDGE R_EDGE1_inlined_6;
__R_EDGE R_EDGE1_inlined_7;
__R_EDGE R_EDGE1_inlined_8;
__R_EDGE R_EDGE1_inlined_9;
__R_EDGE R_EDGE1_inlined_10;
__R_EDGE R_EDGE1_inlined_11;
__R_EDGE R_EDGE1_inlined_12;
__R_EDGE R_EDGE1_inlined_13;
__R_EDGE R_EDGE1_inlined_14;
__R_EDGE R_EDGE1_inlined_15;
__R_EDGE R_EDGE1_inlined_16;
__R_EDGE R_EDGE1_inlined_17;
__ABS_REAL ABS#REAL1_inlined_18;
__ABS_REAL ABS#REAL1_inlined_19;
__ABS_REAL ABS#REAL1_inlined_20;
__ABS_REAL ABS#REAL1_inlined_21;
__ABS_REAL ABS#REAL1_inlined_22;
uint16_t __assertion_error;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Automata declarations
void CPC_FB_ANADO(__CPC_FB_ANADO *__context);
void ROC_LIM(__ROC_LIM *__context);
void TON(__TON *__context);
void R_EDGE(__R_EDGE *__context);
void ABS_REAL(__ABS_REAL *__context);
void DETECT_EDGE(__DETECT_EDGE *__context);
void VerificationLoop();

// Automata
void CPC_FB_ANADO(__CPC_FB_ANADO *__context) {
	// Temporary variables
	float ___nested_ret_val1;
	float ___nested_ret_val2;
	float ___nested_ret_val3;
	float ___nested_ret_val4;
	float ___nested_ret_val5;
	
	// Start with initial location
	goto init;
	init: {
			__context->Manreg01b[0] = ((__context->Manreg01 & 256) != 0);
			goto varview_refresh1;
		//assert(false);
		return;  			}
	l1: {
			// Assign inputs
			R_EDGE1_inlined_1.new = __context->Manreg01b[8];
			R_EDGE1_inlined_1.old = __context->MAuMoR_old;
			R_EDGE(&R_EDGE1_inlined_1);
			// Assign outputs
			__context->MAuMoR_old = R_EDGE1_inlined_1.old;
			__context->E_MAuMoR = R_EDGE1_inlined_1.RET_VAL;
			goto l2;
		//assert(false);
		return;  			}
	l2: {
			// Assign inputs
			R_EDGE1_inlined_2.new = __context->Manreg01b[9];
			R_EDGE1_inlined_2.old = __context->MMMoR_old;
			R_EDGE(&R_EDGE1_inlined_2);
			// Assign outputs
			__context->MMMoR_old = R_EDGE1_inlined_2.old;
			__context->E_MMMoR = R_EDGE1_inlined_2.RET_VAL;
			goto l3;
		//assert(false);
		return;  			}
	l3: {
			// Assign inputs
			R_EDGE1_inlined_3.new = __context->Manreg01b[10];
			R_EDGE1_inlined_3.old = __context->MFoMoR_old;
			R_EDGE(&R_EDGE1_inlined_3);
			// Assign outputs
			__context->MFoMoR_old = R_EDGE1_inlined_3.old;
			__context->E_MFoMoR = R_EDGE1_inlined_3.RET_VAL;
			goto l4;
		//assert(false);
		return;  			}
	l4: {
			// Assign inputs
			R_EDGE1_inlined_4.new = __context->Manreg01b[11];
			R_EDGE1_inlined_4.old = __context->MSoftLDR_old;
			R_EDGE(&R_EDGE1_inlined_4);
			// Assign outputs
			__context->MSoftLDR_old = R_EDGE1_inlined_4.old;
			__context->E_MSoftLDR = R_EDGE1_inlined_4.RET_VAL;
			goto l5;
		//assert(false);
		return;  			}
	l5: {
			// Assign inputs
			R_EDGE1_inlined_5.new = __context->Manreg01b[12];
			R_EDGE1_inlined_5.old = __context->MOnR_old;
			R_EDGE(&R_EDGE1_inlined_5);
			// Assign outputs
			__context->MOnR_old = R_EDGE1_inlined_5.old;
			__context->E_MOnR = R_EDGE1_inlined_5.RET_VAL;
			goto l6;
		//assert(false);
		return;  			}
	l6: {
			// Assign inputs
			R_EDGE1_inlined_6.new = __context->Manreg01b[13];
			R_EDGE1_inlined_6.old = __context->MOffR_old;
			R_EDGE(&R_EDGE1_inlined_6);
			// Assign outputs
			__context->MOffR_old = R_EDGE1_inlined_6.old;
			__context->E_MOffR = R_EDGE1_inlined_6.RET_VAL;
			goto l7;
		//assert(false);
		return;  			}
	l7: {
			// Assign inputs
			R_EDGE1_inlined_7.new = __context->Manreg01b[14];
			R_EDGE1_inlined_7.old = __context->MNewPosR_old;
			R_EDGE(&R_EDGE1_inlined_7);
			// Assign outputs
			__context->MNewPosR_old = R_EDGE1_inlined_7.old;
			__context->E_MNewPosR = R_EDGE1_inlined_7.RET_VAL;
			goto l8;
		//assert(false);
		return;  			}
	l8: {
			// Assign inputs
			R_EDGE1_inlined_8.new = __context->Manreg01b[15];
			R_EDGE1_inlined_8.old = __context->MStpInR_old;
			R_EDGE(&R_EDGE1_inlined_8);
			// Assign outputs
			__context->MStpInR_old = R_EDGE1_inlined_8.old;
			__context->E_MStpInR = R_EDGE1_inlined_8.RET_VAL;
			goto l9;
		//assert(false);
		return;  			}
	l9: {
			// Assign inputs
			R_EDGE1_inlined_9.new = __context->Manreg01b[0];
			R_EDGE1_inlined_9.old = __context->MStpDeR_old;
			R_EDGE(&R_EDGE1_inlined_9);
			// Assign outputs
			__context->MStpDeR_old = R_EDGE1_inlined_9.old;
			__context->E_MStpDeR = R_EDGE1_inlined_9.RET_VAL;
			goto l10;
		//assert(false);
		return;  			}
	l10: {
			// Assign inputs
			R_EDGE1_inlined_10.new = __context->Manreg01b[1];
			R_EDGE1_inlined_10.old = __context->MEnRstartR_old;
			R_EDGE(&R_EDGE1_inlined_10);
			// Assign outputs
			__context->MEnRstartR_old = R_EDGE1_inlined_10.old;
			__context->E_MEnRstartR = R_EDGE1_inlined_10.RET_VAL;
			goto l11;
		//assert(false);
		return;  			}
	l11: {
			// Assign inputs
			R_EDGE1_inlined_11.new = __context->Manreg01b[7];
			R_EDGE1_inlined_11.old = __context->MAlAckR_old;
			R_EDGE(&R_EDGE1_inlined_11);
			// Assign outputs
			__context->MAlAckR_old = R_EDGE1_inlined_11.old;
			__context->E_MAlAckR = R_EDGE1_inlined_11.RET_VAL;
			goto l12;
		//assert(false);
		return;  			}
	l12: {
			__context->PFsPosOn = __context->PAnalogb.ParRegb[8];
			goto l13;
		//assert(false);
		return;  			}
	l13: {
			__context->PHFOn = __context->PAnalogb.ParRegb[9];
			goto l14;
		//assert(false);
		return;  			}
	l14: {
			__context->PHFPos = __context->PAnalogb.ParRegb[11];
			goto l15;
		//assert(false);
		return;  			}
	l15: {
			__context->PHLD = __context->PAnalogb.ParRegb[12];
			goto l16;
		//assert(false);
		return;  			}
	l16: {
			__context->PHLDCmd = __context->PAnalogb.ParRegb[13];
			goto l17;
		//assert(false);
		return;  			}
	l17: {
			__context->PEnRstart = __context->PAnalogb.ParRegb[0];
			goto l18;
		//assert(false);
		return;  			}
	l18: {
			__context->PRstartFS = __context->PAnalogb.ParRegb[1];
			goto l19;
		//assert(false);
		return;  			}
	l19: {
			// Assign inputs
			R_EDGE1_inlined_12.new = __context->AuAuMoR;
			R_EDGE1_inlined_12.old = __context->AuAuMoR_old;
			R_EDGE(&R_EDGE1_inlined_12);
			// Assign outputs
			__context->AuAuMoR_old = R_EDGE1_inlined_12.old;
			__context->E_AuAuMoR = R_EDGE1_inlined_12.RET_VAL;
			goto l20;
		//assert(false);
		return;  			}
	l20: {
			// Assign inputs
			R_EDGE1_inlined_13.new = __context->AuAlAck;
			R_EDGE1_inlined_13.old = __context->AuAlAckR_old;
			R_EDGE(&R_EDGE1_inlined_13);
			// Assign outputs
			__context->AuAlAckR_old = R_EDGE1_inlined_13.old;
			__context->E_AuAlAckR = R_EDGE1_inlined_13.RET_VAL;
			goto l21;
		//assert(false);
		return;  			}
	l21: {
			// Assign inputs
			R_EDGE1_inlined_14.new = __context->StartI;
			R_EDGE1_inlined_14.old = __context->StartI_old;
			R_EDGE(&R_EDGE1_inlined_14);
			// Assign outputs
			__context->StartI_old = R_EDGE1_inlined_14.old;
			__context->E_StartI = R_EDGE1_inlined_14.RET_VAL;
			goto l22;
		//assert(false);
		return;  			}
	l22: {
			// Assign inputs
			R_EDGE1_inlined_15.new = __context->TStopI;
			R_EDGE1_inlined_15.old = __context->TStopI_old;
			R_EDGE(&R_EDGE1_inlined_15);
			// Assign outputs
			__context->TStopI_old = R_EDGE1_inlined_15.old;
			__context->E_TStopI = R_EDGE1_inlined_15.RET_VAL;
			goto l23;
		//assert(false);
		return;  			}
	l23: {
			// Assign inputs
			R_EDGE1_inlined_16.new = __context->FuStopI;
			R_EDGE1_inlined_16.old = __context->FuStopI_old;
			R_EDGE(&R_EDGE1_inlined_16);
			// Assign outputs
			__context->FuStopI_old = R_EDGE1_inlined_16.old;
			__context->E_FuStopI = R_EDGE1_inlined_16.RET_VAL;
			goto l24;
		//assert(false);
		return;  			}
	l24: {
			// Assign inputs
			R_EDGE1_inlined_17.new = __context->Al;
			R_EDGE1_inlined_17.old = __context->Al_old;
			R_EDGE(&R_EDGE1_inlined_17);
			// Assign outputs
			__context->Al_old = R_EDGE1_inlined_17.old;
			__context->E_Al = R_EDGE1_inlined_17.RET_VAL;
			goto l25;
		//assert(false);
		return;  			}
	l25: {
			__context->StartISt = __context->StartI;
			goto l26;
		//assert(false);
		return;  			}
	l26: {
			__context->TStopISt = __context->TStopI;
			goto l27;
		//assert(false);
		return;  			}
	l27: {
			__context->FuStopISt = __context->FuStopI;
			goto l28;
		//assert(false);
		return;  			}
	l28: {
		if (__context->E_FuStopI) {
			goto l29;
		}
		if ((! __context->E_FuStopI)) {
			goto l35;
		}
		//assert(false);
		return;  			}
	l29: {
			__context->fullNotAcknowledged = true;
			goto l30;
		//assert(false);
		return;  			}
	l30: {
		if (__context->PEnRstart) {
			goto l31;
		}
		if ((! __context->PEnRstart)) {
			goto l33;
		}
		//assert(false);
		return;  			}
	l31: {
			__context->EnRstartSt = false;
			goto l32;
		//assert(false);
		return;  			}
	l32: {
			goto l34;
		//assert(false);
		return;  			}
	l33: {
			goto l34;
		//assert(false);
		return;  			}
	l34: {
			goto l36;
		//assert(false);
		return;  			}
	l35: {
			goto l36;
		//assert(false);
		return;  			}
	l36: {
		if ((((__context->E_TStopI || __context->E_StartI) || __context->E_FuStopI) || __context->E_Al)) {
			goto l37;
		}
		if (((! (((__context->E_TStopI || __context->E_StartI) || __context->E_FuStopI) || __context->E_Al)) && (__context->E_MAlAckR || __context->E_AuAlAckR))) {
			goto l39;
		}
		if (((! (((__context->E_TStopI || __context->E_StartI) || __context->E_FuStopI) || __context->E_Al)) && (! ((! (((__context->E_TStopI || __context->E_StartI) || __context->E_FuStopI) || __context->E_Al)) && (__context->E_MAlAckR || __context->E_AuAlAckR))))) {
			goto l42;
		}
		//assert(false);
		return;  			}
	l37: {
			__context->AlUnAck = true;
			goto l38;
		//assert(false);
		return;  			}
	l38: {
			goto l43;
		//assert(false);
		return;  			}
	l39: {
			__context->fullNotAcknowledged = false;
			goto l40;
		//assert(false);
		return;  			}
	l40: {
			__context->AlUnAck = false;
			goto l41;
		//assert(false);
		return;  			}
	l41: {
			goto l43;
		//assert(false);
		return;  			}
	l42: {
			goto l43;
		//assert(false);
		return;  			}
	l43: {
		if (((((__context->PEnRstart && (__context->E_MEnRstartR || __context->AuRstart)) && (! __context->FuStopISt)) || ((__context->PEnRstart && __context->PRstartFS) && (__context->E_MEnRstartR || __context->AuRstart))) && (! __context->fullNotAcknowledged))) {
			goto l44;
		}
		if ((! ((((__context->PEnRstart && (__context->E_MEnRstartR || __context->AuRstart)) && (! __context->FuStopISt)) || ((__context->PEnRstart && __context->PRstartFS) && (__context->E_MEnRstartR || __context->AuRstart))) && (! __context->fullNotAcknowledged)))) {
			goto l46;
		}
		//assert(false);
		return;  			}
	l44: {
			__context->EnRstartSt = true;
			goto l45;
		//assert(false);
		return;  			}
	l45: {
			goto l47;
		//assert(false);
		return;  			}
	l46: {
			goto l47;
		//assert(false);
		return;  			}
	l47: {
			__context->InterlockR = (((((__context->TStopISt || __context->FuStopISt) || __context->fullNotAcknowledged) || (! __context->EnRstartSt)) || (((! __context->PFsPosOn) && __context->StartISt) && (! __context->OutOnOVSt))) || ((__context->PFsPosOn && __context->StartISt) && __context->OutOnOVSt));
			goto l48;
		//assert(false);
		return;  			}
	l48: {
		if ((! (__context->HLD && __context->PHLD))) {
			goto l49;
		}
		if ((! (! (__context->HLD && __context->PHLD)))) {
			goto l83;
		}
		//assert(false);
		return;  			}
	l49: {
		if (((((__context->AuMoSt_aux || __context->MMoSt_aux) || __context->SoftLDSt_aux) && __context->E_MFoMoR) && (! __context->AuIhFoMo))) {
			goto l50;
		}
		if ((! ((((__context->AuMoSt_aux || __context->MMoSt_aux) || __context->SoftLDSt_aux) && __context->E_MFoMoR) && (! __context->AuIhFoMo)))) {
			goto l55;
		}
		//assert(false);
		return;  			}
	l50: {
			__context->AuMoSt_aux = false;
			goto l51;
		//assert(false);
		return;  			}
	l51: {
			__context->MMoSt_aux = false;
			goto l52;
		//assert(false);
		return;  			}
	l52: {
			__context->FoMoSt_aux = true;
			goto l53;
		//assert(false);
		return;  			}
	l53: {
			__context->SoftLDSt_aux = false;
			goto l54;
		//assert(false);
		return;  			}
	l54: {
			goto l56;
		//assert(false);
		return;  			}
	l55: {
			goto l56;
		//assert(false);
		return;  			}
	l56: {
		if (((((__context->AuMoSt_aux || __context->FoMoSt_aux) || __context->SoftLDSt_aux) && __context->E_MMMoR) && (! __context->AuIhMMo))) {
			goto l57;
		}
		if ((! ((((__context->AuMoSt_aux || __context->FoMoSt_aux) || __context->SoftLDSt_aux) && __context->E_MMMoR) && (! __context->AuIhMMo)))) {
			goto l62;
		}
		//assert(false);
		return;  			}
	l57: {
			__context->AuMoSt_aux = false;
			goto l58;
		//assert(false);
		return;  			}
	l58: {
			__context->MMoSt_aux = true;
			goto l59;
		//assert(false);
		return;  			}
	l59: {
			__context->FoMoSt_aux = false;
			goto l60;
		//assert(false);
		return;  			}
	l60: {
			__context->SoftLDSt_aux = false;
			goto l61;
		//assert(false);
		return;  			}
	l61: {
			goto l63;
		//assert(false);
		return;  			}
	l62: {
			goto l63;
		//assert(false);
		return;  			}
	l63: {
		if ((((((((__context->MMoSt_aux && (__context->E_MAuMoR || __context->E_AuAuMoR)) || (__context->FoMoSt_aux && __context->E_MAuMoR)) || (__context->SoftLDSt_aux && __context->E_MAuMoR)) || (__context->MMoSt_aux && __context->AuIhMMo)) || (__context->FoMoSt_aux && __context->AuIhFoMo)) || (__context->SoftLDSt_aux && __context->AuIhFoMo)) || (! (((__context->AuMoSt_aux || __context->MMoSt_aux) || __context->FoMoSt_aux) || __context->SoftLDSt_aux)))) {
			goto l64;
		}
		if ((! (((((((__context->MMoSt_aux && (__context->E_MAuMoR || __context->E_AuAuMoR)) || (__context->FoMoSt_aux && __context->E_MAuMoR)) || (__context->SoftLDSt_aux && __context->E_MAuMoR)) || (__context->MMoSt_aux && __context->AuIhMMo)) || (__context->FoMoSt_aux && __context->AuIhFoMo)) || (__context->SoftLDSt_aux && __context->AuIhFoMo)) || (! (((__context->AuMoSt_aux || __context->MMoSt_aux) || __context->FoMoSt_aux) || __context->SoftLDSt_aux))))) {
			goto l69;
		}
		//assert(false);
		return;  			}
	l64: {
			__context->AuMoSt_aux = true;
			goto l65;
		//assert(false);
		return;  			}
	l65: {
			__context->MMoSt_aux = false;
			goto l66;
		//assert(false);
		return;  			}
	l66: {
			__context->FoMoSt_aux = false;
			goto l67;
		//assert(false);
		return;  			}
	l67: {
			__context->SoftLDSt_aux = false;
			goto l68;
		//assert(false);
		return;  			}
	l68: {
			goto l70;
		//assert(false);
		return;  			}
	l69: {
			goto l70;
		//assert(false);
		return;  			}
	l70: {
		if ((((__context->AuMoSt_aux || __context->MMoSt_aux) && __context->E_MSoftLDR) && (! __context->AuIhFoMo))) {
			goto l71;
		}
		if ((! (((__context->AuMoSt_aux || __context->MMoSt_aux) && __context->E_MSoftLDR) && (! __context->AuIhFoMo)))) {
			goto l76;
		}
		//assert(false);
		return;  			}
	l71: {
			__context->AuMoSt_aux = false;
			goto l72;
		//assert(false);
		return;  			}
	l72: {
			__context->MMoSt_aux = false;
			goto l73;
		//assert(false);
		return;  			}
	l73: {
			__context->FoMoSt_aux = false;
			goto l74;
		//assert(false);
		return;  			}
	l74: {
			__context->SoftLDSt_aux = true;
			goto l75;
		//assert(false);
		return;  			}
	l75: {
			goto l77;
		//assert(false);
		return;  			}
	l76: {
			goto l77;
		//assert(false);
		return;  			}
	l77: {
			__context->LDSt = false;
			goto l78;
		//assert(false);
		return;  			}
	l78: {
			__context->AuMoSt = __context->AuMoSt_aux;
			goto l79;
		//assert(false);
		return;  			}
	l79: {
			__context->MMoSt = __context->MMoSt_aux;
			goto l80;
		//assert(false);
		return;  			}
	l80: {
			__context->FoMoSt = __context->FoMoSt_aux;
			goto l81;
		//assert(false);
		return;  			}
	l81: {
			__context->SoftLDSt = __context->SoftLDSt_aux;
			goto l82;
		//assert(false);
		return;  			}
	l82: {
			goto l89;
		//assert(false);
		return;  			}
	l83: {
			__context->AuMoSt = false;
			goto l84;
		//assert(false);
		return;  			}
	l84: {
			__context->MMoSt = false;
			goto l85;
		//assert(false);
		return;  			}
	l85: {
			__context->FoMoSt = false;
			goto l86;
		//assert(false);
		return;  			}
	l86: {
			__context->LDSt = true;
			goto l87;
		//assert(false);
		return;  			}
	l87: {
			__context->SoftLDSt = false;
			goto l88;
		//assert(false);
		return;  			}
	l88: {
			goto l89;
		//assert(false);
		return;  			}
	l89: {
		if (__context->PHFPos) {
			goto l90;
		}
		if (((! __context->PHFPos) && __context->PHLDCmd)) {
			goto l92;
		}
		if (((! __context->PHFPos) && (! ((! __context->PHFPos) && __context->PHLDCmd)))) {
			goto l94;
		}
		//assert(false);
		return;  			}
	l90: {
			__context->PosSt = __context->HFPos;
			goto l91;
		//assert(false);
		return;  			}
	l91: {
			goto l96;
		//assert(false);
		return;  			}
	l92: {
			__context->PosSt = __context->HAOut;
			goto l93;
		//assert(false);
		return;  			}
	l93: {
			goto l96;
		//assert(false);
		return;  			}
	l94: {
			__context->PosSt = __context->PosRSt;
			goto l95;
		//assert(false);
		return;  			}
	l95: {
			goto l96;
		//assert(false);
		return;  			}
	l96: {
			__context->OnSt = ((__context->PHFOn && __context->HFOn) || ((! __context->PHFOn) && __context->OutOnOVSt));
			goto l97;
		//assert(false);
		return;  			}
	l97: {
			__context->AnalogOnSt = (__context->PosSt >= __context->PliOn);
			goto l98;
		//assert(false);
		return;  			}
	l98: {
			__context->AnalogOffSt = (__context->PosSt <= __context->PliOff);
			goto l99;
		//assert(false);
		return;  			}
	l99: {
		if (__context->AuOffR) {
			goto l100;
		}
		if (((! __context->AuOffR) && __context->AuOnR)) {
			goto l102;
		}
		if ((((! __context->AuOffR) && (! __context->AuOnR)) && ((__context->fullNotAcknowledged || __context->FuStopISt) || (! __context->EnRstartSt)))) {
			goto l104;
		}
		if (((! __context->AuOffR) && ((! ((! __context->AuOffR) && __context->AuOnR)) && (! (((! __context->AuOffR) && (! __context->AuOnR)) && ((__context->fullNotAcknowledged || __context->FuStopISt) || (! __context->EnRstartSt))))))) {
			goto l106;
		}
		//assert(false);
		return;  			}
	l100: {
			__context->AuOnRSt = false;
			goto l101;
		//assert(false);
		return;  			}
	l101: {
			goto l107;
		//assert(false);
		return;  			}
	l102: {
			__context->AuOnRSt = true;
			goto l103;
		//assert(false);
		return;  			}
	l103: {
			goto l107;
		//assert(false);
		return;  			}
	l104: {
			__context->AuOnRSt = __context->PFsPosOn;
			goto l105;
		//assert(false);
		return;  			}
	l105: {
			goto l107;
		//assert(false);
		return;  			}
	l106: {
			goto l107;
		//assert(false);
		return;  			}
	l107: {
			__context->AuOffRSt = (! __context->AuOnRSt);
			goto l108;
		//assert(false);
		return;  			}
	l108: {
		if ((((((__context->E_MOffR && ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt)) || (__context->AuOffRSt && __context->AuMoSt)) || ((__context->LDSt && __context->PHLDCmd) && __context->HOffRSt)) && __context->EnRstartSt) || (__context->E_FuStopI && (! __context->PFsPosOn)))) {
			goto l109;
		}
		if (((! (((((__context->E_MOffR && ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt)) || (__context->AuOffRSt && __context->AuMoSt)) || ((__context->LDSt && __context->PHLDCmd) && __context->HOffRSt)) && __context->EnRstartSt) || (__context->E_FuStopI && (! __context->PFsPosOn)))) && (((((__context->E_MOnR && ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt)) || (__context->AuOnRSt && __context->AuMoSt)) || ((__context->LDSt && __context->PHLDCmd) && __context->HOnRSt)) && __context->EnRstartSt) || (__context->E_FuStopI && __context->PFsPosOn)))) {
			goto l111;
		}
		if (((! (((((__context->E_MOffR && ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt)) || (__context->AuOffRSt && __context->AuMoSt)) || ((__context->LDSt && __context->PHLDCmd) && __context->HOffRSt)) && __context->EnRstartSt) || (__context->E_FuStopI && (! __context->PFsPosOn)))) && (! ((! (((((__context->E_MOffR && ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt)) || (__context->AuOffRSt && __context->AuMoSt)) || ((__context->LDSt && __context->PHLDCmd) && __context->HOffRSt)) && __context->EnRstartSt) || (__context->E_FuStopI && (! __context->PFsPosOn)))) && (((((__context->E_MOnR && ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt)) || (__context->AuOnRSt && __context->AuMoSt)) || ((__context->LDSt && __context->PHLDCmd) && __context->HOnRSt)) && __context->EnRstartSt) || (__context->E_FuStopI && __context->PFsPosOn)))))) {
			goto l113;
		}
		//assert(false);
		return;  			}
	l109: {
			__context->MOnRSt = false;
			goto l110;
		//assert(false);
		return;  			}
	l110: {
			goto l114;
		//assert(false);
		return;  			}
	l111: {
			__context->MOnRSt = true;
			goto l112;
		//assert(false);
		return;  			}
	l112: {
			goto l114;
		//assert(false);
		return;  			}
	l113: {
			goto l114;
		//assert(false);
		return;  			}
	l114: {
			__context->MOffRSt = (! __context->MOnRSt);
			goto l115;
		//assert(false);
		return;  			}
	l115: {
		if (__context->HOffR) {
			goto l116;
		}
		if ((! __context->HOffR)) {
			goto l118;
		}
		//assert(false);
		return;  			}
	l116: {
			__context->HOnRSt = false;
			goto l117;
		//assert(false);
		return;  			}
	l117: {
			goto l123;
		//assert(false);
		return;  			}
	l118: {
		if (__context->HOnR) {
			goto l119;
		}
		if ((! __context->HOnR)) {
			goto l121;
		}
		//assert(false);
		return;  			}
	l119: {
			__context->HOnRSt = true;
			goto l120;
		//assert(false);
		return;  			}
	l120: {
			goto l122;
		//assert(false);
		return;  			}
	l121: {
			goto l122;
		//assert(false);
		return;  			}
	l122: {
			goto l123;
		//assert(false);
		return;  			}
	l123: {
			__context->HOffRSt = (! __context->HOnRSt);
			goto l124;
		//assert(false);
		return;  			}
	l124: {
			__context->OutOnOVSt = (((__context->MOnRSt && ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt)) || (__context->AuOnRSt && __context->AuMoSt)) || ((__context->HOnRSt && __context->LDSt) && __context->PHLDCmd));
			goto l125;
		//assert(false);
		return;  			}
	l125: {
		if (__context->EnRstartSt) {
			goto l126;
		}
		if ((! __context->EnRstartSt)) {
			goto l145;
		}
		//assert(false);
		return;  			}
	l126: {
		if (__context->E_MNewPosR) {
			goto l127;
		}
		if ((! __context->E_MNewPosR)) {
			goto l129;
		}
		//assert(false);
		return;  			}
	l127: {
			__context->MPosRSt = __context->MPosR;
			goto l128;
		//assert(false);
		return;  			}
	l128: {
			goto l130;
		//assert(false);
		return;  			}
	l129: {
			goto l130;
		//assert(false);
		return;  			}
	l130: {
		if (__context->E_MStpInR) {
			goto l131;
		}
		if ((! __context->E_MStpInR)) {
			goto l134;
		}
		//assert(false);
		return;  			}
	l131: {
			// Assign inputs
			ABS#REAL1_inlined_18.in = ((__context->PAnalog.PMaxRan - __context->PAnalog.PMinRan) / 100.0);
			ABS_REAL(&ABS#REAL1_inlined_18);
			// Assign outputs
			___nested_ret_val1 = ABS#REAL1_inlined_18.RET_VAL;
			goto l132;
		//assert(false);
		return;  			}
	l132: {
			__context->MPosRSt = (__context->MPosRSt + (__context->PAnalog.PMStpInV * ___nested_ret_val1));
			goto l133;
		//assert(false);
		return;  			}
	l133: {
			goto l135;
		//assert(false);
		return;  			}
	l134: {
			goto l135;
		//assert(false);
		return;  			}
	l135: {
		if (__context->E_MStpDeR) {
			goto l136;
		}
		if ((! __context->E_MStpDeR)) {
			goto l139;
		}
		//assert(false);
		return;  			}
	l136: {
			// Assign inputs
			ABS#REAL1_inlined_19.in = ((__context->PAnalog.PMaxRan - __context->PAnalog.PMinRan) / 100.0);
			ABS_REAL(&ABS#REAL1_inlined_19);
			// Assign outputs
			___nested_ret_val2 = ABS#REAL1_inlined_19.RET_VAL;
			goto l137;
		//assert(false);
		return;  			}
	l137: {
			__context->MPosRSt = (__context->MPosRSt - (__context->PAnalog.PMStpDeV * ___nested_ret_val2));
			goto l138;
		//assert(false);
		return;  			}
	l138: {
			goto l140;
		//assert(false);
		return;  			}
	l139: {
			goto l140;
		//assert(false);
		return;  			}
	l140: {
		if (__context->E_MOffR) {
			goto l141;
		}
		if ((! __context->E_MOffR)) {
			goto l143;
		}
		//assert(false);
		return;  			}
	l141: {
			__context->MPosRSt = __context->PAnalog.PMinRan;
			goto l142;
		//assert(false);
		return;  			}
	l142: {
			goto l144;
		//assert(false);
		return;  			}
	l143: {
			goto l144;
		//assert(false);
		return;  			}
	l144: {
			goto l150;
		//assert(false);
		return;  			}
	l145: {
		if (((! __context->PFsPosOn) && __context->E_MOffR)) {
			goto l146;
		}
		if ((! ((! __context->PFsPosOn) && __context->E_MOffR))) {
			goto l148;
		}
		//assert(false);
		return;  			}
	l146: {
			__context->MPosRSt = __context->PAnalog.PMinRan;
			goto l147;
		//assert(false);
		return;  			}
	l147: {
			goto l149;
		//assert(false);
		return;  			}
	l148: {
			goto l149;
		//assert(false);
		return;  			}
	l149: {
			goto l150;
		//assert(false);
		return;  			}
	l150: {
		if ((__context->MPosRSt > __context->PAnalog.PMaxRan)) {
			goto l151;
		}
		if ((! (__context->MPosRSt > __context->PAnalog.PMaxRan))) {
			goto l153;
		}
		//assert(false);
		return;  			}
	l151: {
			__context->MPosRSt = __context->PAnalog.PMaxRan;
			goto l152;
		//assert(false);
		return;  			}
	l152: {
			goto l154;
		//assert(false);
		return;  			}
	l153: {
			goto l154;
		//assert(false);
		return;  			}
	l154: {
		if ((__context->MPosRSt < __context->PAnalog.PMinRan)) {
			goto l155;
		}
		if ((! (__context->MPosRSt < __context->PAnalog.PMinRan))) {
			goto l157;
		}
		//assert(false);
		return;  			}
	l155: {
			__context->MPosRSt = __context->PAnalog.PMinRan;
			goto l156;
		//assert(false);
		return;  			}
	l156: {
			goto l158;
		//assert(false);
		return;  			}
	l157: {
			goto l158;
		//assert(false);
		return;  			}
	l158: {
			__context->AuPosRSt = __context->AuPosR;
			goto l159;
		//assert(false);
		return;  			}
	l159: {
		if (__context->AuOffR) {
			goto l160;
		}
		if ((! __context->AuOffR)) {
			goto l162;
		}
		//assert(false);
		return;  			}
	l160: {
			__context->AuPosRSt = __context->PAnalog.PMinRan;
			goto l161;
		//assert(false);
		return;  			}
	l161: {
			goto l163;
		//assert(false);
		return;  			}
	l162: {
			goto l163;
		//assert(false);
		return;  			}
	l163: {
		if ((__context->AuPosRSt > __context->PAnalog.PMaxRan)) {
			goto l164;
		}
		if ((! (__context->AuPosRSt > __context->PAnalog.PMaxRan))) {
			goto l166;
		}
		//assert(false);
		return;  			}
	l164: {
			__context->AuPosRSt = __context->PAnalog.PMaxRan;
			goto l165;
		//assert(false);
		return;  			}
	l165: {
			goto l167;
		//assert(false);
		return;  			}
	l166: {
			goto l167;
		//assert(false);
		return;  			}
	l167: {
		if ((__context->AuPosRSt < __context->PAnalog.PMinRan)) {
			goto l168;
		}
		if ((! (__context->AuPosRSt < __context->PAnalog.PMinRan))) {
			goto l170;
		}
		//assert(false);
		return;  			}
	l168: {
			__context->AuPosRSt = __context->PAnalog.PMinRan;
			goto l169;
		//assert(false);
		return;  			}
	l169: {
			goto l171;
		//assert(false);
		return;  			}
	l170: {
			goto l171;
		//assert(false);
		return;  			}
	l171: {
		if (__context->AuMoSt) {
			goto l172;
		}
		if (((! __context->AuMoSt) && ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt))) {
			goto l175;
		}
		if (((! __context->AuMoSt) && (! ((! __context->AuMoSt) && ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt))))) {
			goto l177;
		}
		//assert(false);
		return;  			}
	l172: {
			__context->PosR = __context->AuPosRSt;
			goto l173;
		//assert(false);
		return;  			}
	l173: {
			__context->MPosRSt = __context->AuPosRSt;
			goto l174;
		//assert(false);
		return;  			}
	l174: {
			goto l187;
		//assert(false);
		return;  			}
	l175: {
			__context->PosR = __context->MPosRSt;
			goto l176;
		//assert(false);
		return;  			}
	l176: {
			goto l187;
		//assert(false);
		return;  			}
	l177: {
		if ((__context->PHLDCmd && __context->PHLD)) {
			goto l178;
		}
		if ((! (__context->PHLDCmd && __context->PHLD))) {
			goto l181;
		}
		//assert(false);
		return;  			}
	l178: {
			__context->MPosRSt = __context->HAOut;
			goto l179;
		//assert(false);
		return;  			}
	l179: {
			__context->PosR = __context->HAOut;
			goto l180;
		//assert(false);
		return;  			}
	l180: {
			goto l184;
		//assert(false);
		return;  			}
	l181: {
			__context->MPosRSt = __context->PosSt;
			goto l182;
		//assert(false);
		return;  			}
	l182: {
			__context->PosR = __context->PosSt;
			goto l183;
		//assert(false);
		return;  			}
	l183: {
			goto l184;
		//assert(false);
		return;  			}
	l184: {
			__context->Ramp_parameters.inc_rate = 0.0;
			goto l185;
		//assert(false);
		return;  			}
	l185: {
			__context->Ramp_parameters.dec_rate = 0.0;
			goto l186;
		//assert(false);
		return;  			}
	l186: {
			goto l187;
		//assert(false);
		return;  			}
	l187: {
		if (__context->E_FuStopI) {
			goto l188;
		}
		if ((! __context->E_FuStopI)) {
			goto l202;
		}
		//assert(false);
		return;  			}
	l188: {
			__context->fullNotAcknowledged = true;
			goto l189;
		//assert(false);
		return;  			}
	l189: {
		if ((! __context->AuMoSt)) {
			goto l190;
		}
		if ((! (! __context->AuMoSt))) {
			goto l196;
		}
		//assert(false);
		return;  			}
	l190: {
		if ((! __context->PFsPosOn)) {
			goto l191;
		}
		if ((! (! __context->PFsPosOn))) {
			goto l193;
		}
		//assert(false);
		return;  			}
	l191: {
			__context->MPosRSt = __context->PAnalog.PMinRan;
			goto l192;
		//assert(false);
		return;  			}
	l192: {
			goto l195;
		//assert(false);
		return;  			}
	l193: {
			__context->MPosRSt = __context->PAnalog.PMaxRan;
			goto l194;
		//assert(false);
		return;  			}
	l194: {
			goto l195;
		//assert(false);
		return;  			}
	l195: {
			goto l197;
		//assert(false);
		return;  			}
	l196: {
			goto l197;
		//assert(false);
		return;  			}
	l197: {
		if (__context->PEnRstart) {
			goto l198;
		}
		if ((! __context->PEnRstart)) {
			goto l200;
		}
		//assert(false);
		return;  			}
	l198: {
			__context->EnRstartSt = false;
			goto l199;
		//assert(false);
		return;  			}
	l199: {
			goto l201;
		//assert(false);
		return;  			}
	l200: {
			goto l201;
		//assert(false);
		return;  			}
	l201: {
			goto l203;
		//assert(false);
		return;  			}
	l202: {
			goto l203;
		//assert(false);
		return;  			}
	l203: {
		if ((! __context->PFsPosOn)) {
			goto l204;
		}
		if ((! (! __context->PFsPosOn))) {
			goto l210;
		}
		//assert(false);
		return;  			}
	l204: {
		if (__context->InterlockR) {
			goto l205;
		}
		if ((! __context->InterlockR)) {
			goto l208;
		}
		//assert(false);
		return;  			}
	l205: {
			__context->PosR = __context->PAnalog.PMinRan;
			goto l206;
		//assert(false);
		return;  			}
	l206: {
			__context->OutOnOVSt = false;
			goto l207;
		//assert(false);
		return;  			}
	l207: {
			goto l209;
		//assert(false);
		return;  			}
	l208: {
			goto l209;
		//assert(false);
		return;  			}
	l209: {
			goto l216;
		//assert(false);
		return;  			}
	l210: {
		if (__context->InterlockR) {
			goto l211;
		}
		if ((! __context->InterlockR)) {
			goto l214;
		}
		//assert(false);
		return;  			}
	l211: {
			__context->PosR = __context->PAnalog.PMaxRan;
			goto l212;
		//assert(false);
		return;  			}
	l212: {
			__context->OutOnOVSt = true;
			goto l213;
		//assert(false);
		return;  			}
	l213: {
			goto l215;
		//assert(false);
		return;  			}
	l214: {
			goto l215;
		//assert(false);
		return;  			}
	l215: {
			goto l216;
		//assert(false);
		return;  			}
	l216: {
			__context->AlSt = __context->Al;
			goto l217;
		//assert(false);
		return;  			}
	l217: {
		if (__context->AuMoSt) {
			goto l218;
		}
		if (((! __context->AuMoSt) && ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt))) {
			goto l221;
		}
		if (((! __context->AuMoSt) && (! ((! __context->AuMoSt) && ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt))))) {
			goto l224;
		}
		//assert(false);
		return;  			}
	l218: {
			__context->Ramp_parameters.inc_rate = __context->AuInSpd;
			goto l219;
		//assert(false);
		return;  			}
	l219: {
			__context->Ramp_parameters.dec_rate = __context->AuDeSpd;
			goto l220;
		//assert(false);
		return;  			}
	l220: {
			goto l225;
		//assert(false);
		return;  			}
	l221: {
			__context->Ramp_parameters.inc_rate = __context->PAnalog.PMInSpd;
			goto l222;
		//assert(false);
		return;  			}
	l222: {
			__context->Ramp_parameters.dec_rate = __context->PAnalog.PMDeSpd;
			goto l223;
		//assert(false);
		return;  			}
	l223: {
			goto l225;
		//assert(false);
		return;  			}
	l224: {
			goto l225;
		//assert(false);
		return;  			}
	l225: {
		if ((__context->TStopI || __context->FuStopI)) {
			goto l226;
		}
		if ((! (__context->TStopI || __context->FuStopI))) {
			goto l229;
		}
		//assert(false);
		return;  			}
	l226: {
			__context->Ramp_parameters.inc_rate = __context->AuInSpd;
			goto l227;
		//assert(false);
		return;  			}
	l227: {
			__context->Ramp_parameters.dec_rate = __context->AuDeSpd;
			goto l228;
		//assert(false);
		return;  			}
	l228: {
			goto l230;
		//assert(false);
		return;  			}
	l229: {
			goto l230;
		//assert(false);
		return;  			}
	l230: {
			// Assign inputs
			__context->ROC_LIM.INV = __context->PosR;
			__context->ROC_LIM.UPRLM_P = __context->Ramp_parameters.inc_rate;
			__context->ROC_LIM.DNRLM_P = __context->Ramp_parameters.dec_rate;
			__context->ROC_LIM.UPRLM_N = __context->Ramp_parameters.inc_rate;
			__context->ROC_LIM.DNRLM_N = __context->Ramp_parameters.dec_rate;
			__context->ROC_LIM.DFOUT_ON = false;
			__context->ROC_LIM.DF_OUTV = __context->PosRSt;
			__context->ROC_LIM.H_LM = __context->PAnalog.PMaxRan;
			__context->ROC_LIM.L_LM = __context->PAnalog.PMinRan;
			__context->ROC_LIM.CYCLE = ((int32_t) T_CYCLE);
			ROC_LIM(&__context->ROC_LIM);
			// Assign outputs
			goto l231;
		//assert(false);
		return;  			}
	l231: {
			__context->PosRSt = __context->ROC_LIM.OUTV;
			goto l232;
		//assert(false);
		return;  			}
	l232: {
			__context->RdyStartSt = (! __context->InterlockR);
			goto l233;
		//assert(false);
		return;  			}
	l233: {
			__context->IOErrorW = __context->IOError;
			goto l234;
		//assert(false);
		return;  			}
	l234: {
			__context->IOSimuW = __context->IOSimu;
			goto l235;
		//assert(false);
		return;  			}
	l235: {
			// Assign inputs
			ABS#REAL1_inlined_20.in = (__context->AuPosRSt - __context->MPosRSt);
			ABS_REAL(&ABS#REAL1_inlined_20);
			// Assign outputs
			___nested_ret_val3 = ABS#REAL1_inlined_20.RET_VAL;
			goto l236;
		//assert(false);
		return;  			}
	l236: {
			__context->AuMRW = (((((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt) && (___nested_ret_val3 > __context->PAnalog.PWDb)) && (! __context->IhAuMRW)) || ((((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt) && ((__context->AuOnRSt != __context->MOnRSt) || (__context->AuOffRSt != __context->MOffRSt))) && (! __context->IhAuMRW)));
			goto l237;
		//assert(false);
		return;  			}
	l237: {
		if ((__context->PHFPos && __context->PHFOn)) {
			goto l238;
		}
		if (((! (__context->PHFPos && __context->PHFOn)) && __context->PHFOn)) {
			goto l241;
		}
		if ((((! (__context->PHFPos && __context->PHFOn)) && (! __context->PHFOn)) && __context->PHFPos)) {
			goto l243;
		}
		if (((! (__context->PHFPos && __context->PHFOn)) && ((! ((! (__context->PHFPos && __context->PHFOn)) && __context->PHFOn)) && (! (((! (__context->PHFPos && __context->PHFOn)) && (! __context->PHFOn)) && __context->PHFPos))))) {
			goto l246;
		}
		//assert(false);
		return;  			}
	l238: {
			// Assign inputs
			ABS#REAL1_inlined_21.in = (__context->HFPos - __context->MPosRSt);
			ABS_REAL(&ABS#REAL1_inlined_21);
			// Assign outputs
			___nested_ret_val4 = ABS#REAL1_inlined_21.RET_VAL;
			goto l239;
		//assert(false);
		return;  			}
	l239: {
			// Assign inputs
			__context->Timer_Warning.IN = (((___nested_ret_val4 > __context->PAnalog.PWDb) || ((! __context->OutOnOVSt) && __context->OnSt)) || (__context->OutOnOVSt && (! __context->OnSt)));
			__context->Timer_Warning.PT = __context->PAnalog.PWDt;
			TON(&__context->Timer_Warning);
			// Assign outputs
			goto l240;
		//assert(false);
		return;  			}
	l240: {
			goto l248;
		//assert(false);
		return;  			}
	l241: {
			// Assign inputs
			__context->Timer_Warning.IN = (((! __context->OutOnOVSt) && __context->OnSt) || (__context->OutOnOVSt && (! __context->OnSt)));
			__context->Timer_Warning.PT = __context->PAnalog.PWDt;
			TON(&__context->Timer_Warning);
			// Assign outputs
			goto l242;
		//assert(false);
		return;  			}
	l242: {
			goto l248;
		//assert(false);
		return;  			}
	l243: {
			// Assign inputs
			ABS#REAL1_inlined_22.in = (__context->HFPos - __context->MPosRSt);
			ABS_REAL(&ABS#REAL1_inlined_22);
			// Assign outputs
			___nested_ret_val5 = ABS#REAL1_inlined_22.RET_VAL;
			goto l244;
		//assert(false);
		return;  			}
	l244: {
			// Assign inputs
			__context->Timer_Warning.IN = (___nested_ret_val5 > __context->PAnalog.PWDb);
			__context->Timer_Warning.PT = __context->PAnalog.PWDt;
			TON(&__context->Timer_Warning);
			// Assign outputs
			goto l245;
		//assert(false);
		return;  			}
	l245: {
			goto l248;
		//assert(false);
		return;  			}
	l246: {
			// Assign inputs
			__context->Timer_Warning.IN = false;
			__context->Timer_Warning.PT = __context->PAnalog.PWDt;
			TON(&__context->Timer_Warning);
			// Assign outputs
			goto l247;
		//assert(false);
		return;  			}
	l247: {
			goto l248;
		//assert(false);
		return;  			}
	l248: {
		if ((__context->PHFPos || __context->PHFOn)) {
			goto l249;
		}
		if ((! (__context->PHFPos || __context->PHFOn))) {
			goto l251;
		}
		//assert(false);
		return;  			}
	l249: {
			__context->PosW = __context->Timer_Warning.Q;
			goto l250;
		//assert(false);
		return;  			}
	l250: {
			goto l253;
		//assert(false);
		return;  			}
	l251: {
			__context->PosW = false;
			goto l252;
		//assert(false);
		return;  			}
	l252: {
			goto l253;
		//assert(false);
		return;  			}
	l253: {
			__context->Time_Warning = __context->Timer_Warning.ET;
			goto l254;
		//assert(false);
		return;  			}
	l254: {
		if (__context->PFsPosOn) {
			goto l255;
		}
		if ((! __context->PFsPosOn)) {
			goto l258;
		}
		//assert(false);
		return;  			}
	l255: {
			__context->OutOV = ((__context->PAnalog.PMaxRan + __context->PAnalog.PMinRan) - __context->PosRSt);
			goto l256;
		//assert(false);
		return;  			}
	l256: {
			__context->OutOnOV = (! __context->OutOnOVSt);
			goto l257;
		//assert(false);
		return;  			}
	l257: {
			goto l261;
		//assert(false);
		return;  			}
	l258: {
			__context->OutOV = __context->PosRSt;
			goto l259;
		//assert(false);
		return;  			}
	l259: {
			__context->OutOnOV = __context->OutOnOVSt;
			goto l260;
		//assert(false);
		return;  			}
	l260: {
			goto l261;
		//assert(false);
		return;  			}
	l261: {
			__context->AlBW = __context->AlB;
			goto l262;
		//assert(false);
		return;  			}
	l262: {
			__context->PulseWidth = (1500.0 / ((float) ((int32_t) ((int32_t) T_CYCLE))));
			goto l263;
		//assert(false);
		return;  			}
	l263: {
		if ((__context->FuStopISt || (__context->FSIinc > 0))) {
			goto l264;
		}
		if ((! (__context->FuStopISt || (__context->FSIinc > 0)))) {
			goto l267;
		}
		//assert(false);
		return;  			}
	l264: {
			__context->FSIinc = (__context->FSIinc + 1);
			goto l265;
		//assert(false);
		return;  			}
	l265: {
			__context->WFuStopISt = true;
			goto l266;
		//assert(false);
		return;  			}
	l266: {
			goto l268;
		//assert(false);
		return;  			}
	l267: {
			goto l268;
		//assert(false);
		return;  			}
	l268: {
		if (((((float) __context->FSIinc) > __context->PulseWidth) || ((! __context->FuStopISt) && (__context->FSIinc == 0)))) {
			goto l269;
		}
		if ((! ((((float) __context->FSIinc) > __context->PulseWidth) || ((! __context->FuStopISt) && (__context->FSIinc == 0))))) {
			goto l272;
		}
		//assert(false);
		return;  			}
	l269: {
			__context->FSIinc = 0;
			goto l270;
		//assert(false);
		return;  			}
	l270: {
			__context->WFuStopISt = __context->FuStopISt;
			goto l271;
		//assert(false);
		return;  			}
	l271: {
			goto l273;
		//assert(false);
		return;  			}
	l272: {
			goto l273;
		//assert(false);
		return;  			}
	l273: {
		if ((__context->TStopISt || (__context->TSIinc > 0))) {
			goto l274;
		}
		if ((! (__context->TStopISt || (__context->TSIinc > 0)))) {
			goto l277;
		}
		//assert(false);
		return;  			}
	l274: {
			__context->TSIinc = (__context->TSIinc + 1);
			goto l275;
		//assert(false);
		return;  			}
	l275: {
			__context->WTStopISt = true;
			goto l276;
		//assert(false);
		return;  			}
	l276: {
			goto l278;
		//assert(false);
		return;  			}
	l277: {
			goto l278;
		//assert(false);
		return;  			}
	l278: {
		if (((((float) __context->TSIinc) > __context->PulseWidth) || ((! __context->TStopISt) && (__context->TSIinc == 0)))) {
			goto l279;
		}
		if ((! ((((float) __context->TSIinc) > __context->PulseWidth) || ((! __context->TStopISt) && (__context->TSIinc == 0))))) {
			goto l282;
		}
		//assert(false);
		return;  			}
	l279: {
			__context->TSIinc = 0;
			goto l280;
		//assert(false);
		return;  			}
	l280: {
			__context->WTStopISt = __context->TStopISt;
			goto l281;
		//assert(false);
		return;  			}
	l281: {
			goto l283;
		//assert(false);
		return;  			}
	l282: {
			goto l283;
		//assert(false);
		return;  			}
	l283: {
		if ((__context->StartISt || (__context->SIinc > 0))) {
			goto l284;
		}
		if ((! (__context->StartISt || (__context->SIinc > 0)))) {
			goto l287;
		}
		//assert(false);
		return;  			}
	l284: {
			__context->SIinc = (__context->SIinc + 1);
			goto l285;
		//assert(false);
		return;  			}
	l285: {
			__context->WStartISt = true;
			goto l286;
		//assert(false);
		return;  			}
	l286: {
			goto l288;
		//assert(false);
		return;  			}
	l287: {
			goto l288;
		//assert(false);
		return;  			}
	l288: {
		if (((((float) __context->SIinc) > __context->PulseWidth) || ((! __context->StartISt) && (__context->SIinc == 0)))) {
			goto l289;
		}
		if ((! ((((float) __context->SIinc) > __context->PulseWidth) || ((! __context->StartISt) && (__context->SIinc == 0))))) {
			goto l292;
		}
		//assert(false);
		return;  			}
	l289: {
			__context->SIinc = 0;
			goto l290;
		//assert(false);
		return;  			}
	l290: {
			__context->WStartISt = __context->StartISt;
			goto l291;
		//assert(false);
		return;  			}
	l291: {
			goto l293;
		//assert(false);
		return;  			}
	l292: {
			goto l293;
		//assert(false);
		return;  			}
	l293: {
		if ((__context->AlSt || (__context->Alinc > 0))) {
			goto l294;
		}
		if ((! (__context->AlSt || (__context->Alinc > 0)))) {
			goto l297;
		}
		//assert(false);
		return;  			}
	l294: {
			__context->Alinc = (__context->Alinc + 1);
			goto l295;
		//assert(false);
		return;  			}
	l295: {
			__context->WAlSt = true;
			goto l296;
		//assert(false);
		return;  			}
	l296: {
			goto l298;
		//assert(false);
		return;  			}
	l297: {
			goto l298;
		//assert(false);
		return;  			}
	l298: {
		if (((((float) __context->Alinc) > __context->PulseWidth) || ((! __context->AlSt) && (__context->Alinc == 0)))) {
			goto l299;
		}
		if ((! ((((float) __context->Alinc) > __context->PulseWidth) || ((! __context->AlSt) && (__context->Alinc == 0))))) {
			goto l302;
		}
		//assert(false);
		return;  			}
	l299: {
			__context->Alinc = 0;
			goto l300;
		//assert(false);
		return;  			}
	l300: {
			__context->WAlSt = __context->AlSt;
			goto l301;
		//assert(false);
		return;  			}
	l301: {
			goto l303;
		//assert(false);
		return;  			}
	l302: {
			goto l303;
		//assert(false);
		return;  			}
	l303: {
			__context->Stsreg01b[8] = __context->OnSt;
			goto x1;
		//assert(false);
		return;  			}
	l304: {
			__context->Stsreg01b[9] = false;
			goto x2;
		//assert(false);
		return;  			}
	l305: {
			__context->Stsreg01b[10] = __context->AuMoSt;
			goto x3;
		//assert(false);
		return;  			}
	l306: {
			__context->Stsreg01b[11] = __context->MMoSt;
			goto x4;
		//assert(false);
		return;  			}
	l307: {
			__context->Stsreg01b[12] = __context->FoMoSt;
			goto x5;
		//assert(false);
		return;  			}
	l308: {
			__context->Stsreg01b[13] = __context->LDSt;
			goto x6;
		//assert(false);
		return;  			}
	l309: {
			__context->Stsreg01b[14] = __context->IOErrorW;
			goto x7;
		//assert(false);
		return;  			}
	l310: {
			__context->Stsreg01b[15] = __context->IOSimuW;
			goto x8;
		//assert(false);
		return;  			}
	l311: {
			__context->Stsreg01b[0] = __context->AuMRW;
			goto x9;
		//assert(false);
		return;  			}
	l312: {
			__context->Stsreg01b[1] = __context->PosW;
			goto x10;
		//assert(false);
		return;  			}
	l313: {
			__context->Stsreg01b[2] = __context->WStartISt;
			goto x11;
		//assert(false);
		return;  			}
	l314: {
			__context->Stsreg01b[3] = __context->WTStopISt;
			goto x12;
		//assert(false);
		return;  			}
	l315: {
			__context->Stsreg01b[4] = __context->AlUnAck;
			goto x13;
		//assert(false);
		return;  			}
	l316: {
			__context->Stsreg01b[5] = __context->AuIhFoMo;
			goto x14;
		//assert(false);
		return;  			}
	l317: {
			__context->Stsreg01b[6] = __context->WAlSt;
			goto x15;
		//assert(false);
		return;  			}
	l318: {
			__context->Stsreg01b[7] = __context->AuIhMMo;
			goto x16;
		//assert(false);
		return;  			}
	l319: {
			__context->Stsreg02b[8] = __context->OutOnOVSt;
			goto x17;
		//assert(false);
		return;  			}
	l320: {
			__context->Stsreg02b[9] = __context->AuOnRSt;
			goto x18;
		//assert(false);
		return;  			}
	l321: {
			__context->Stsreg02b[10] = __context->MOnRSt;
			goto x19;
		//assert(false);
		return;  			}
	l322: {
			__context->Stsreg02b[11] = __context->AuOffRSt;
			goto x20;
		//assert(false);
		return;  			}
	l323: {
			__context->Stsreg02b[12] = __context->MOffRSt;
			goto x21;
		//assert(false);
		return;  			}
	l324: {
			__context->Stsreg02b[13] = __context->HOnRSt;
			goto x22;
		//assert(false);
		return;  			}
	l325: {
			__context->Stsreg02b[14] = __context->HOffRSt;
			goto x23;
		//assert(false);
		return;  			}
	l326: {
			__context->Stsreg02b[15] = false;
			goto x24;
		//assert(false);
		return;  			}
	l327: {
			__context->Stsreg02b[0] = __context->AnalogOnSt;
			goto x25;
		//assert(false);
		return;  			}
	l328: {
			__context->Stsreg02b[1] = __context->AnalogOffSt;
			goto x26;
		//assert(false);
		return;  			}
	l329: {
			__context->Stsreg02b[2] = __context->WFuStopISt;
			goto x27;
		//assert(false);
		return;  			}
	l330: {
			__context->Stsreg02b[3] = __context->EnRstartSt;
			goto x28;
		//assert(false);
		return;  			}
	l331: {
			__context->Stsreg02b[4] = __context->SoftLDSt;
			goto x29;
		//assert(false);
		return;  			}
	l332: {
			__context->Stsreg02b[5] = __context->AlBW;
			goto x30;
		//assert(false);
		return;  			}
	l333: {
			__context->Stsreg02b[6] = false;
			goto x31;
		//assert(false);
		return;  			}
	l334: {
			__context->Stsreg02b[7] = false;
			goto x32;
		//assert(false);
		return;  			}
	l335: {
			// Assign inputs
			DETECT_EDGE1.new = __context->AlUnAck;
			DETECT_EDGE1.old = __context->AlUnAck_old;
			DETECT_EDGE(&DETECT_EDGE1);
			// Assign outputs
			__context->AlUnAck_old = DETECT_EDGE1.old;
			__context->RE_AlUnAck = DETECT_EDGE1.re;
			__context->FE_AlUnAck = DETECT_EDGE1.fe;
			goto l336;
		//assert(false);
		return;  			}
	l336: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	x: {
			___nested_ret_val1 = 0.0;
			___nested_ret_val2 = 0.0;
			___nested_ret_val3 = 0.0;
			___nested_ret_val4 = 0.0;
			___nested_ret_val5 = 0.0;
			goto l1;
		//assert(false);
		return;  			}
	varview_refresh: {
			__context->PAnalogb.ParRegb[0] = ((__context->PAnalog.ParReg & 256) != 0);
			goto varview_refresh34;
		//assert(false);
		return;  			}
	varview_refresh1: {
			__context->Manreg01b[1] = ((__context->Manreg01 & 512) != 0);
			goto varview_refresh3;
		//assert(false);
		return;  			}
	varview_refresh3: {
			__context->Manreg01b[2] = ((__context->Manreg01 & 1024) != 0);
			goto varview_refresh5;
		//assert(false);
		return;  			}
	varview_refresh5: {
			__context->Manreg01b[3] = ((__context->Manreg01 & 2048) != 0);
			goto varview_refresh7;
		//assert(false);
		return;  			}
	varview_refresh7: {
			__context->Manreg01b[4] = ((__context->Manreg01 & 4096) != 0);
			goto varview_refresh9;
		//assert(false);
		return;  			}
	varview_refresh9: {
			__context->Manreg01b[5] = ((__context->Manreg01 & 8192) != 0);
			goto varview_refresh11;
		//assert(false);
		return;  			}
	varview_refresh11: {
			__context->Manreg01b[6] = ((__context->Manreg01 & 16384) != 0);
			goto varview_refresh13;
		//assert(false);
		return;  			}
	varview_refresh13: {
			__context->Manreg01b[7] = ((__context->Manreg01 & 32768) != 0);
			goto varview_refresh15;
		//assert(false);
		return;  			}
	varview_refresh15: {
			__context->Manreg01b[8] = ((__context->Manreg01 & 1) != 0);
			goto varview_refresh17;
		//assert(false);
		return;  			}
	varview_refresh17: {
			__context->Manreg01b[9] = ((__context->Manreg01 & 2) != 0);
			goto varview_refresh19;
		//assert(false);
		return;  			}
	varview_refresh19: {
			__context->Manreg01b[10] = ((__context->Manreg01 & 4) != 0);
			goto varview_refresh21;
		//assert(false);
		return;  			}
	varview_refresh21: {
			__context->Manreg01b[11] = ((__context->Manreg01 & 8) != 0);
			goto varview_refresh23;
		//assert(false);
		return;  			}
	varview_refresh23: {
			__context->Manreg01b[12] = ((__context->Manreg01 & 16) != 0);
			goto varview_refresh25;
		//assert(false);
		return;  			}
	varview_refresh25: {
			__context->Manreg01b[13] = ((__context->Manreg01 & 32) != 0);
			goto varview_refresh27;
		//assert(false);
		return;  			}
	varview_refresh27: {
			__context->Manreg01b[14] = ((__context->Manreg01 & 64) != 0);
			goto varview_refresh29;
		//assert(false);
		return;  			}
	varview_refresh29: {
			__context->Manreg01b[15] = ((__context->Manreg01 & 128) != 0);
			goto varview_refresh;
		//assert(false);
		return;  			}
	varview_refresh32: {
			__context->Stsreg01b[0] = ((__context->Stsreg01 & 256) != 0);
			goto varview_refresh81;
		//assert(false);
		return;  			}
	varview_refresh33: {
			__context->PAnalog.PMaxRan = __context->PAnalogb.PMaxRan;
			goto varview_refresh65;
		//assert(false);
		return;  			}
	varview_refresh34: {
			__context->PAnalogb.ParRegb[1] = ((__context->PAnalog.ParReg & 512) != 0);
			goto varview_refresh36;
		//assert(false);
		return;  			}
	varview_refresh36: {
			__context->PAnalogb.ParRegb[2] = ((__context->PAnalog.ParReg & 1024) != 0);
			goto varview_refresh38;
		//assert(false);
		return;  			}
	varview_refresh38: {
			__context->PAnalogb.ParRegb[3] = ((__context->PAnalog.ParReg & 2048) != 0);
			goto varview_refresh40;
		//assert(false);
		return;  			}
	varview_refresh40: {
			__context->PAnalogb.ParRegb[4] = ((__context->PAnalog.ParReg & 4096) != 0);
			goto varview_refresh42;
		//assert(false);
		return;  			}
	varview_refresh42: {
			__context->PAnalogb.ParRegb[5] = ((__context->PAnalog.ParReg & 8192) != 0);
			goto varview_refresh44;
		//assert(false);
		return;  			}
	varview_refresh44: {
			__context->PAnalogb.ParRegb[6] = ((__context->PAnalog.ParReg & 16384) != 0);
			goto varview_refresh46;
		//assert(false);
		return;  			}
	varview_refresh46: {
			__context->PAnalogb.ParRegb[7] = ((__context->PAnalog.ParReg & 32768) != 0);
			goto varview_refresh48;
		//assert(false);
		return;  			}
	varview_refresh48: {
			__context->PAnalogb.ParRegb[8] = ((__context->PAnalog.ParReg & 1) != 0);
			goto varview_refresh50;
		//assert(false);
		return;  			}
	varview_refresh50: {
			__context->PAnalogb.ParRegb[9] = ((__context->PAnalog.ParReg & 2) != 0);
			goto varview_refresh52;
		//assert(false);
		return;  			}
	varview_refresh52: {
			__context->PAnalogb.ParRegb[10] = ((__context->PAnalog.ParReg & 4) != 0);
			goto varview_refresh54;
		//assert(false);
		return;  			}
	varview_refresh54: {
			__context->PAnalogb.ParRegb[11] = ((__context->PAnalog.ParReg & 8) != 0);
			goto varview_refresh56;
		//assert(false);
		return;  			}
	varview_refresh56: {
			__context->PAnalogb.ParRegb[12] = ((__context->PAnalog.ParReg & 16) != 0);
			goto varview_refresh58;
		//assert(false);
		return;  			}
	varview_refresh58: {
			__context->PAnalogb.ParRegb[13] = ((__context->PAnalog.ParReg & 32) != 0);
			goto varview_refresh60;
		//assert(false);
		return;  			}
	varview_refresh60: {
			__context->PAnalogb.ParRegb[14] = ((__context->PAnalog.ParReg & 64) != 0);
			goto varview_refresh62;
		//assert(false);
		return;  			}
	varview_refresh62: {
			__context->PAnalogb.ParRegb[15] = ((__context->PAnalog.ParReg & 128) != 0);
			goto varview_refresh33;
		//assert(false);
		return;  			}
	varview_refresh65: {
			__context->PAnalog.PMinRan = __context->PAnalogb.PMinRan;
			goto varview_refresh67;
		//assert(false);
		return;  			}
	varview_refresh67: {
			__context->PAnalog.PMStpInV = __context->PAnalogb.PMStpInV;
			goto varview_refresh69;
		//assert(false);
		return;  			}
	varview_refresh69: {
			__context->PAnalog.PMStpDeV = __context->PAnalogb.PMStpDeV;
			goto varview_refresh71;
		//assert(false);
		return;  			}
	varview_refresh71: {
			__context->PAnalog.PMInSpd = __context->PAnalogb.PMInSpd;
			goto varview_refresh73;
		//assert(false);
		return;  			}
	varview_refresh73: {
			__context->PAnalog.PMDeSpd = __context->PAnalogb.PMDeSpd;
			goto varview_refresh75;
		//assert(false);
		return;  			}
	varview_refresh75: {
			__context->PAnalog.PWDt = __context->PAnalogb.PWDt;
			goto varview_refresh77;
		//assert(false);
		return;  			}
	varview_refresh77: {
			__context->PAnalog.PWDb = __context->PAnalogb.PWDb;
			goto varview_refresh32;
		//assert(false);
		return;  			}
	varview_refresh80: {
			__context->Stsreg02b[0] = ((__context->Stsreg02 & 256) != 0);
			goto varview_refresh112;
		//assert(false);
		return;  			}
	varview_refresh81: {
			__context->Stsreg01b[1] = ((__context->Stsreg01 & 512) != 0);
			goto varview_refresh83;
		//assert(false);
		return;  			}
	varview_refresh83: {
			__context->Stsreg01b[2] = ((__context->Stsreg01 & 1024) != 0);
			goto varview_refresh85;
		//assert(false);
		return;  			}
	varview_refresh85: {
			__context->Stsreg01b[3] = ((__context->Stsreg01 & 2048) != 0);
			goto varview_refresh87;
		//assert(false);
		return;  			}
	varview_refresh87: {
			__context->Stsreg01b[4] = ((__context->Stsreg01 & 4096) != 0);
			goto varview_refresh89;
		//assert(false);
		return;  			}
	varview_refresh89: {
			__context->Stsreg01b[5] = ((__context->Stsreg01 & 8192) != 0);
			goto varview_refresh91;
		//assert(false);
		return;  			}
	varview_refresh91: {
			__context->Stsreg01b[6] = ((__context->Stsreg01 & 16384) != 0);
			goto varview_refresh93;
		//assert(false);
		return;  			}
	varview_refresh93: {
			__context->Stsreg01b[7] = ((__context->Stsreg01 & 32768) != 0);
			goto varview_refresh95;
		//assert(false);
		return;  			}
	varview_refresh95: {
			__context->Stsreg01b[8] = ((__context->Stsreg01 & 1) != 0);
			goto varview_refresh97;
		//assert(false);
		return;  			}
	varview_refresh97: {
			__context->Stsreg01b[9] = ((__context->Stsreg01 & 2) != 0);
			goto varview_refresh99;
		//assert(false);
		return;  			}
	varview_refresh99: {
			__context->Stsreg01b[10] = ((__context->Stsreg01 & 4) != 0);
			goto varview_refresh101;
		//assert(false);
		return;  			}
	varview_refresh101: {
			__context->Stsreg01b[11] = ((__context->Stsreg01 & 8) != 0);
			goto varview_refresh103;
		//assert(false);
		return;  			}
	varview_refresh103: {
			__context->Stsreg01b[12] = ((__context->Stsreg01 & 16) != 0);
			goto varview_refresh105;
		//assert(false);
		return;  			}
	varview_refresh105: {
			__context->Stsreg01b[13] = ((__context->Stsreg01 & 32) != 0);
			goto varview_refresh107;
		//assert(false);
		return;  			}
	varview_refresh107: {
			__context->Stsreg01b[14] = ((__context->Stsreg01 & 64) != 0);
			goto varview_refresh109;
		//assert(false);
		return;  			}
	varview_refresh109: {
			__context->Stsreg01b[15] = ((__context->Stsreg01 & 128) != 0);
			goto varview_refresh80;
		//assert(false);
		return;  			}
	varview_refresh112: {
			__context->Stsreg02b[1] = ((__context->Stsreg02 & 512) != 0);
			goto varview_refresh114;
		//assert(false);
		return;  			}
	varview_refresh114: {
			__context->Stsreg02b[2] = ((__context->Stsreg02 & 1024) != 0);
			goto varview_refresh116;
		//assert(false);
		return;  			}
	varview_refresh116: {
			__context->Stsreg02b[3] = ((__context->Stsreg02 & 2048) != 0);
			goto varview_refresh118;
		//assert(false);
		return;  			}
	varview_refresh118: {
			__context->Stsreg02b[4] = ((__context->Stsreg02 & 4096) != 0);
			goto varview_refresh120;
		//assert(false);
		return;  			}
	varview_refresh120: {
			__context->Stsreg02b[5] = ((__context->Stsreg02 & 8192) != 0);
			goto varview_refresh122;
		//assert(false);
		return;  			}
	varview_refresh122: {
			__context->Stsreg02b[6] = ((__context->Stsreg02 & 16384) != 0);
			goto varview_refresh124;
		//assert(false);
		return;  			}
	varview_refresh124: {
			__context->Stsreg02b[7] = ((__context->Stsreg02 & 32768) != 0);
			goto varview_refresh126;
		//assert(false);
		return;  			}
	varview_refresh126: {
			__context->Stsreg02b[8] = ((__context->Stsreg02 & 1) != 0);
			goto varview_refresh128;
		//assert(false);
		return;  			}
	varview_refresh128: {
			__context->Stsreg02b[9] = ((__context->Stsreg02 & 2) != 0);
			goto varview_refresh130;
		//assert(false);
		return;  			}
	varview_refresh130: {
			__context->Stsreg02b[10] = ((__context->Stsreg02 & 4) != 0);
			goto varview_refresh132;
		//assert(false);
		return;  			}
	varview_refresh132: {
			__context->Stsreg02b[11] = ((__context->Stsreg02 & 8) != 0);
			goto varview_refresh134;
		//assert(false);
		return;  			}
	varview_refresh134: {
			__context->Stsreg02b[12] = ((__context->Stsreg02 & 16) != 0);
			goto varview_refresh136;
		//assert(false);
		return;  			}
	varview_refresh136: {
			__context->Stsreg02b[13] = ((__context->Stsreg02 & 32) != 0);
			goto varview_refresh138;
		//assert(false);
		return;  			}
	varview_refresh138: {
			__context->Stsreg02b[14] = ((__context->Stsreg02 & 64) != 0);
			goto varview_refresh140;
		//assert(false);
		return;  			}
	varview_refresh140: {
			__context->Stsreg02b[15] = ((__context->Stsreg02 & 128) != 0);
			goto x;
		//assert(false);
		return;  			}
	x1: {
		if (__context->Stsreg01b[8]) {
			__context->Stsreg01 = (__context->Stsreg01 | 1);
			goto l304;
		}
		if ((! __context->Stsreg01b[8])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65534);
			goto l304;
		}
		//assert(false);
		return;  			}
	x2: {
		if (__context->Stsreg01b[9]) {
			__context->Stsreg01 = (__context->Stsreg01 | 2);
			goto l305;
		}
		if ((! __context->Stsreg01b[9])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65533);
			goto l305;
		}
		//assert(false);
		return;  			}
	x3: {
		if (__context->Stsreg01b[10]) {
			__context->Stsreg01 = (__context->Stsreg01 | 4);
			goto l306;
		}
		if ((! __context->Stsreg01b[10])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65531);
			goto l306;
		}
		//assert(false);
		return;  			}
	x4: {
		if (__context->Stsreg01b[11]) {
			__context->Stsreg01 = (__context->Stsreg01 | 8);
			goto l307;
		}
		if ((! __context->Stsreg01b[11])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65527);
			goto l307;
		}
		//assert(false);
		return;  			}
	x5: {
		if (__context->Stsreg01b[12]) {
			__context->Stsreg01 = (__context->Stsreg01 | 16);
			goto l308;
		}
		if ((! __context->Stsreg01b[12])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65519);
			goto l308;
		}
		//assert(false);
		return;  			}
	x6: {
		if (__context->Stsreg01b[13]) {
			__context->Stsreg01 = (__context->Stsreg01 | 32);
			goto l309;
		}
		if ((! __context->Stsreg01b[13])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65503);
			goto l309;
		}
		//assert(false);
		return;  			}
	x7: {
		if (__context->Stsreg01b[14]) {
			__context->Stsreg01 = (__context->Stsreg01 | 64);
			goto l310;
		}
		if ((! __context->Stsreg01b[14])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65471);
			goto l310;
		}
		//assert(false);
		return;  			}
	x8: {
		if (__context->Stsreg01b[15]) {
			__context->Stsreg01 = (__context->Stsreg01 | 128);
			goto l311;
		}
		if ((! __context->Stsreg01b[15])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65407);
			goto l311;
		}
		//assert(false);
		return;  			}
	x9: {
		if (__context->Stsreg01b[0]) {
			__context->Stsreg01 = (__context->Stsreg01 | 256);
			goto l312;
		}
		if ((! __context->Stsreg01b[0])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65279);
			goto l312;
		}
		//assert(false);
		return;  			}
	x10: {
		if (__context->Stsreg01b[1]) {
			__context->Stsreg01 = (__context->Stsreg01 | 512);
			goto l313;
		}
		if ((! __context->Stsreg01b[1])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65023);
			goto l313;
		}
		//assert(false);
		return;  			}
	x11: {
		if (__context->Stsreg01b[2]) {
			__context->Stsreg01 = (__context->Stsreg01 | 1024);
			goto l314;
		}
		if ((! __context->Stsreg01b[2])) {
			__context->Stsreg01 = (__context->Stsreg01 & 64511);
			goto l314;
		}
		//assert(false);
		return;  			}
	x12: {
		if (__context->Stsreg01b[3]) {
			__context->Stsreg01 = (__context->Stsreg01 | 2048);
			goto l315;
		}
		if ((! __context->Stsreg01b[3])) {
			__context->Stsreg01 = (__context->Stsreg01 & 63487);
			goto l315;
		}
		//assert(false);
		return;  			}
	x13: {
		if (__context->Stsreg01b[4]) {
			__context->Stsreg01 = (__context->Stsreg01 | 4096);
			goto l316;
		}
		if ((! __context->Stsreg01b[4])) {
			__context->Stsreg01 = (__context->Stsreg01 & 61439);
			goto l316;
		}
		//assert(false);
		return;  			}
	x14: {
		if (__context->Stsreg01b[5]) {
			__context->Stsreg01 = (__context->Stsreg01 | 8192);
			goto l317;
		}
		if ((! __context->Stsreg01b[5])) {
			__context->Stsreg01 = (__context->Stsreg01 & 57343);
			goto l317;
		}
		//assert(false);
		return;  			}
	x15: {
		if (__context->Stsreg01b[6]) {
			__context->Stsreg01 = (__context->Stsreg01 | 16384);
			goto l318;
		}
		if ((! __context->Stsreg01b[6])) {
			__context->Stsreg01 = (__context->Stsreg01 & 49151);
			goto l318;
		}
		//assert(false);
		return;  			}
	x16: {
		if (__context->Stsreg01b[7]) {
			__context->Stsreg01 = (__context->Stsreg01 | 32768);
			goto l319;
		}
		if ((! __context->Stsreg01b[7])) {
			__context->Stsreg01 = (__context->Stsreg01 & 32767);
			goto l319;
		}
		//assert(false);
		return;  			}
	x17: {
		if (__context->Stsreg02b[8]) {
			__context->Stsreg02 = (__context->Stsreg02 | 1);
			goto l320;
		}
		if ((! __context->Stsreg02b[8])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65534);
			goto l320;
		}
		//assert(false);
		return;  			}
	x18: {
		if (__context->Stsreg02b[9]) {
			__context->Stsreg02 = (__context->Stsreg02 | 2);
			goto l321;
		}
		if ((! __context->Stsreg02b[9])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65533);
			goto l321;
		}
		//assert(false);
		return;  			}
	x19: {
		if (__context->Stsreg02b[10]) {
			__context->Stsreg02 = (__context->Stsreg02 | 4);
			goto l322;
		}
		if ((! __context->Stsreg02b[10])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65531);
			goto l322;
		}
		//assert(false);
		return;  			}
	x20: {
		if (__context->Stsreg02b[11]) {
			__context->Stsreg02 = (__context->Stsreg02 | 8);
			goto l323;
		}
		if ((! __context->Stsreg02b[11])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65527);
			goto l323;
		}
		//assert(false);
		return;  			}
	x21: {
		if (__context->Stsreg02b[12]) {
			__context->Stsreg02 = (__context->Stsreg02 | 16);
			goto l324;
		}
		if ((! __context->Stsreg02b[12])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65519);
			goto l324;
		}
		//assert(false);
		return;  			}
	x22: {
		if (__context->Stsreg02b[13]) {
			__context->Stsreg02 = (__context->Stsreg02 | 32);
			goto l325;
		}
		if ((! __context->Stsreg02b[13])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65503);
			goto l325;
		}
		//assert(false);
		return;  			}
	x23: {
		if (__context->Stsreg02b[14]) {
			__context->Stsreg02 = (__context->Stsreg02 | 64);
			goto l326;
		}
		if ((! __context->Stsreg02b[14])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65471);
			goto l326;
		}
		//assert(false);
		return;  			}
	x24: {
		if (__context->Stsreg02b[15]) {
			__context->Stsreg02 = (__context->Stsreg02 | 128);
			goto l327;
		}
		if ((! __context->Stsreg02b[15])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65407);
			goto l327;
		}
		//assert(false);
		return;  			}
	x25: {
		if (__context->Stsreg02b[0]) {
			__context->Stsreg02 = (__context->Stsreg02 | 256);
			goto l328;
		}
		if ((! __context->Stsreg02b[0])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65279);
			goto l328;
		}
		//assert(false);
		return;  			}
	x26: {
		if (__context->Stsreg02b[1]) {
			__context->Stsreg02 = (__context->Stsreg02 | 512);
			goto l329;
		}
		if ((! __context->Stsreg02b[1])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65023);
			goto l329;
		}
		//assert(false);
		return;  			}
	x27: {
		if (__context->Stsreg02b[2]) {
			__context->Stsreg02 = (__context->Stsreg02 | 1024);
			goto l330;
		}
		if ((! __context->Stsreg02b[2])) {
			__context->Stsreg02 = (__context->Stsreg02 & 64511);
			goto l330;
		}
		//assert(false);
		return;  			}
	x28: {
		if (__context->Stsreg02b[3]) {
			__context->Stsreg02 = (__context->Stsreg02 | 2048);
			goto l331;
		}
		if ((! __context->Stsreg02b[3])) {
			__context->Stsreg02 = (__context->Stsreg02 & 63487);
			goto l331;
		}
		//assert(false);
		return;  			}
	x29: {
		if (__context->Stsreg02b[4]) {
			__context->Stsreg02 = (__context->Stsreg02 | 4096);
			goto l332;
		}
		if ((! __context->Stsreg02b[4])) {
			__context->Stsreg02 = (__context->Stsreg02 & 61439);
			goto l332;
		}
		//assert(false);
		return;  			}
	x30: {
		if (__context->Stsreg02b[5]) {
			__context->Stsreg02 = (__context->Stsreg02 | 8192);
			goto l333;
		}
		if ((! __context->Stsreg02b[5])) {
			__context->Stsreg02 = (__context->Stsreg02 & 57343);
			goto l333;
		}
		//assert(false);
		return;  			}
	x31: {
		if (__context->Stsreg02b[6]) {
			__context->Stsreg02 = (__context->Stsreg02 | 16384);
			goto l334;
		}
		if ((! __context->Stsreg02b[6])) {
			__context->Stsreg02 = (__context->Stsreg02 & 49151);
			goto l334;
		}
		//assert(false);
		return;  			}
	x32: {
		if (__context->Stsreg02b[7]) {
			__context->Stsreg02 = (__context->Stsreg02 | 32768);
			goto l335;
		}
		if ((! __context->Stsreg02b[7])) {
			__context->Stsreg02 = (__context->Stsreg02 & 32767);
			goto l335;
		}
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void ROC_LIM(__ROC_LIM *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			__context->OUTV = __context->INV;
			goto l1100;
		//assert(false);
		return;  			}
	l1100: {
		if ((__context->OUTV > __context->H_LM)) {
			goto l2100;
		}
		if (((! (__context->OUTV > __context->H_LM)) && (__context->OUTV < __context->L_LM))) {
			goto l410;
		}
		if (((! (__context->OUTV > __context->H_LM)) && (! ((! (__context->OUTV > __context->H_LM)) && (__context->OUTV < __context->L_LM))))) {
			goto l610;
		}
		//assert(false);
		return;  			}
	l2100: {
			__context->OUTV = __context->H_LM;
			goto l337;
		//assert(false);
		return;  			}
	l337: {
			goto l710;
		//assert(false);
		return;  			}
	l410: {
			__context->OUTV = __context->L_LM;
			goto l510;
		//assert(false);
		return;  			}
	l510: {
			goto l710;
		//assert(false);
		return;  			}
	l610: {
			goto l710;
		//assert(false);
		return;  			}
	l710: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void TON(__TON *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init2;
	init2: {
		if ((__context->IN == false)) {
			goto l1101;
		}
		if ((! (__context->IN == false))) {
			goto l511;
		}
		//assert(false);
		return;  			}
	l1101: {
			__context->Q = false;
			goto l2101;
		//assert(false);
		return;  			}
	l2101: {
			__context->ET = 0;
			goto l338;
		//assert(false);
		return;  			}
	l338: {
			__context->running = false;
			goto l411;
		//assert(false);
		return;  			}
	l411: {
			goto l2510;
		//assert(false);
		return;  			}
	l511: {
		if ((__context->running == false)) {
			goto l611;
		}
		if ((! (__context->running == false))) {
			goto l1410;
		}
		//assert(false);
		return;  			}
	l611: {
			__context->start = __GLOBAL_TIME;
			goto l711;
		//assert(false);
		return;  			}
	l711: {
			__context->running = true;
			goto l810;
		//assert(false);
		return;  			}
	l810: {
			__context->ET = 0;
			goto l910;
		//assert(false);
		return;  			}
	l910: {
		if ((__context->PT == 0)) {
			goto l1010;
		}
		if ((! (__context->PT == 0))) {
			goto l1210;
		}
		//assert(false);
		return;  			}
	l1010: {
			__context->Q = true;
			goto l1110;
		//assert(false);
		return;  			}
	l1110: {
			goto l1310;
		//assert(false);
		return;  			}
	l1210: {
			goto l1310;
		//assert(false);
		return;  			}
	l1310: {
			goto l2410;
		//assert(false);
		return;  			}
	l1410: {
		if ((! ((__GLOBAL_TIME - (__context->start + __context->PT)) >= 0))) {
			goto l1510;
		}
		if ((! (! ((__GLOBAL_TIME - (__context->start + __context->PT)) >= 0)))) {
			goto l2010;
		}
		//assert(false);
		return;  			}
	l1510: {
		if ((! __context->Q)) {
			goto l1610;
		}
		if ((! (! __context->Q))) {
			goto l1810;
		}
		//assert(false);
		return;  			}
	l1610: {
			__context->ET = (__GLOBAL_TIME - __context->start);
			goto l1710;
		//assert(false);
		return;  			}
	l1710: {
			goto l1910;
		//assert(false);
		return;  			}
	l1810: {
			goto l1910;
		//assert(false);
		return;  			}
	l1910: {
			goto l2310;
		//assert(false);
		return;  			}
	l2010: {
			__context->Q = true;
			goto l2110;
		//assert(false);
		return;  			}
	l2110: {
			__context->ET = __context->PT;
			goto l2210;
		//assert(false);
		return;  			}
	l2210: {
			goto l2310;
		//assert(false);
		return;  			}
	l2310: {
			goto l2410;
		//assert(false);
		return;  			}
	l2410: {
			goto l2510;
		//assert(false);
		return;  			}
	l2510: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void R_EDGE(__R_EDGE *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init3;
	init3: {
		if (((__context->new == true) && (__context->old == false))) {
			goto l1102;
		}
		if ((! ((__context->new == true) && (__context->old == false)))) {
			goto l412;
		}
		//assert(false);
		return;  			}
	l1102: {
			__context->RET_VAL = true;
			goto l2102;
		//assert(false);
		return;  			}
	l2102: {
			__context->old = true;
			goto l339;
		//assert(false);
		return;  			}
	l339: {
			goto l712;
		//assert(false);
		return;  			}
	l412: {
			__context->RET_VAL = false;
			goto l512;
		//assert(false);
		return;  			}
	l512: {
			__context->old = __context->new;
			goto l612;
		//assert(false);
		return;  			}
	l612: {
			goto l712;
		//assert(false);
		return;  			}
	l712: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void ABS_REAL(__ABS_REAL *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init4;
	init4: {
		if ((__context->in > 0.0)) {
			goto l1103;
		}
		if ((! (__context->in > 0.0))) {
			goto l340;
		}
		//assert(false);
		return;  			}
	l1103: {
			__context->RET_VAL = __context->in;
			goto l2103;
		//assert(false);
		return;  			}
	l2103: {
			goto l513;
		//assert(false);
		return;  			}
	l340: {
			__context->RET_VAL = ((- 1.0) * __context->in);
			goto l413;
		//assert(false);
		return;  			}
	l413: {
			goto l513;
		//assert(false);
		return;  			}
	l513: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void DETECT_EDGE(__DETECT_EDGE *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init5;
	init5: {
		if ((__context->new != __context->old)) {
			goto l1104;
		}
		if ((! (__context->new != __context->old))) {
			goto l1011;
		}
		//assert(false);
		return;  			}
	l1104: {
		if ((__context->new == true)) {
			goto l2104;
		}
		if ((! (__context->new == true))) {
			goto l514;
		}
		//assert(false);
		return;  			}
	l2104: {
			__context->re = true;
			goto l341;
		//assert(false);
		return;  			}
	l341: {
			__context->fe = false;
			goto l414;
		//assert(false);
		return;  			}
	l414: {
			goto l811;
		//assert(false);
		return;  			}
	l514: {
			__context->re = false;
			goto l613;
		//assert(false);
		return;  			}
	l613: {
			__context->fe = true;
			goto l713;
		//assert(false);
		return;  			}
	l713: {
			goto l811;
		//assert(false);
		return;  			}
	l811: {
			__context->old = __context->new;
			goto l911;
		//assert(false);
		return;  			}
	l911: {
			goto l1311;
		//assert(false);
		return;  			}
	l1011: {
			__context->re = false;
			goto l1111;
		//assert(false);
		return;  			}
	l1111: {
			__context->fe = false;
			goto l1211;
		//assert(false);
		return;  			}
	l1211: {
			goto l1311;
		//assert(false);
		return;  			}
	l1311: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init6;
	init6: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			T_CYCLE = nondet_uint8_t();
			instance.Al = nondet_bool();
			instance.AlB = nondet_bool();
			instance.AuAlAck = nondet_bool();
			instance.AuAuMoR = nondet_bool();
			instance.AuDeSpd = nondet_float();
			instance.AuIhFoMo = nondet_bool();
			instance.AuIhMMo = nondet_bool();
			instance.AuInSpd = nondet_float();
			instance.AuOffR = nondet_bool();
			instance.AuOnR = nondet_bool();
			instance.AuPosR = nondet_float();
			instance.AuRstart = nondet_bool();
			instance.FuStopI = nondet_bool();
			instance.HAOut = nondet_float();
			instance.HFOn = nondet_bool();
			instance.HFPos = nondet_float();
			instance.HLD = nondet_bool();
			instance.HOffR = nondet_bool();
			instance.HOnR = nondet_bool();
			instance.IOError = nondet_bool();
			instance.IOSimu = nondet_bool();
			instance.IhAuMRW = nondet_bool();
			instance.MPosR = nondet_float();
			instance.Manreg01 = nondet_uint16_t();
			instance.Manreg01b[0] = nondet_bool();
			instance.Manreg01b[10] = nondet_bool();
			instance.Manreg01b[11] = nondet_bool();
			instance.Manreg01b[12] = nondet_bool();
			instance.Manreg01b[13] = nondet_bool();
			instance.Manreg01b[14] = nondet_bool();
			instance.Manreg01b[15] = nondet_bool();
			instance.Manreg01b[1] = nondet_bool();
			instance.Manreg01b[2] = nondet_bool();
			instance.Manreg01b[3] = nondet_bool();
			instance.Manreg01b[4] = nondet_bool();
			instance.Manreg01b[5] = nondet_bool();
			instance.Manreg01b[6] = nondet_bool();
			instance.Manreg01b[7] = nondet_bool();
			instance.Manreg01b[8] = nondet_bool();
			instance.Manreg01b[9] = nondet_bool();
			instance.PAnalog.PMDeSpd = nondet_float();
			instance.PAnalog.PMInSpd = nondet_float();
			instance.PAnalog.PMStpDeV = nondet_float();
			instance.PAnalog.PMStpInV = nondet_float();
			instance.PAnalog.PMaxRan = nondet_float();
			instance.PAnalog.PMinRan = nondet_float();
			instance.PAnalog.PWDb = nondet_float();
			instance.PAnalog.PWDt = nondet_int32_t();
			instance.PAnalog.ParReg = nondet_uint16_t();
			instance.PAnalogb.PMDeSpd = nondet_float();
			instance.PAnalogb.PMInSpd = nondet_float();
			instance.PAnalogb.PMStpDeV = nondet_float();
			instance.PAnalogb.PMStpInV = nondet_float();
			instance.PAnalogb.PMaxRan = nondet_float();
			instance.PAnalogb.PMinRan = nondet_float();
			instance.PAnalogb.PWDb = nondet_float();
			instance.PAnalogb.PWDt = nondet_int32_t();
			instance.PAnalogb.ParRegb[0] = nondet_bool();
			instance.PAnalogb.ParRegb[10] = nondet_bool();
			instance.PAnalogb.ParRegb[11] = nondet_bool();
			instance.PAnalogb.ParRegb[12] = nondet_bool();
			instance.PAnalogb.ParRegb[13] = nondet_bool();
			instance.PAnalogb.ParRegb[14] = nondet_bool();
			instance.PAnalogb.ParRegb[15] = nondet_bool();
			instance.PAnalogb.ParRegb[1] = nondet_bool();
			instance.PAnalogb.ParRegb[2] = nondet_bool();
			instance.PAnalogb.ParRegb[3] = nondet_bool();
			instance.PAnalogb.ParRegb[4] = nondet_bool();
			instance.PAnalogb.ParRegb[5] = nondet_bool();
			instance.PAnalogb.ParRegb[6] = nondet_bool();
			instance.PAnalogb.ParRegb[7] = nondet_bool();
			instance.PAnalogb.ParRegb[8] = nondet_bool();
			instance.PAnalogb.ParRegb[9] = nondet_bool();
			instance.PliOff = nondet_float();
			instance.PliOn = nondet_float();
			instance.StartI = nondet_bool();
			instance.TStopI = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			CPC_FB_ANADO(&instance);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			__GLOBAL_TIME = (__GLOBAL_TIME + ((int32_t) T_CYCLE));
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
void main() {
	// Initial values
	T_CYCLE = 0;
	__GLOBAL_TIME = 0;
	R_EDGE1.new = false;
	R_EDGE1.old = false;
	R_EDGE1.RET_VAL = false;
	ABS#REAL1.in = 0.0;
	ABS#REAL1.RET_VAL = 0.0;
	DETECT_EDGE1.new = false;
	DETECT_EDGE1.old = false;
	DETECT_EDGE1.re = false;
	DETECT_EDGE1.fe = false;
	instance.HFPos = 0.0;
	instance.HAOut = 0.0;
	instance.AuPosR = 0.0;
	instance.AuInSpd = 0.0;
	instance.AuDeSpd = 0.0;
	instance.PliOff = 0.0;
	instance.PliOn = 0.0;
	instance.MPosR = 0.0;
	instance.Manreg01 = 0;
	instance.Manreg01b[0] = false;
	instance.Manreg01b[1] = false;
	instance.Manreg01b[2] = false;
	instance.Manreg01b[3] = false;
	instance.Manreg01b[4] = false;
	instance.Manreg01b[5] = false;
	instance.Manreg01b[6] = false;
	instance.Manreg01b[7] = false;
	instance.Manreg01b[8] = false;
	instance.Manreg01b[9] = false;
	instance.Manreg01b[10] = false;
	instance.Manreg01b[11] = false;
	instance.Manreg01b[12] = false;
	instance.Manreg01b[13] = false;
	instance.Manreg01b[14] = false;
	instance.Manreg01b[15] = false;
	instance.HLD = false;
	instance.HFOn = false;
	instance.HOnR = false;
	instance.HOffR = false;
	instance.IOError = false;
	instance.IOSimu = false;
	instance.AlB = false;
	instance.StartI = false;
	instance.TStopI = false;
	instance.FuStopI = false;
	instance.Al = false;
	instance.AuOnR = false;
	instance.AuOffR = false;
	instance.AuAuMoR = false;
	instance.AuIhMMo = false;
	instance.AuIhFoMo = false;
	instance.AuAlAck = false;
	instance.IhAuMRW = false;
	instance.AuRstart = false;
	instance.PAnalog.ParReg = 0;
	instance.PAnalog.PMaxRan = 0.0;
	instance.PAnalog.PMinRan = 0.0;
	instance.PAnalog.PMStpInV = 0.0;
	instance.PAnalog.PMStpDeV = 0.0;
	instance.PAnalog.PMInSpd = 0.0;
	instance.PAnalog.PMDeSpd = 0.0;
	instance.PAnalog.PWDt = 0;
	instance.PAnalog.PWDb = 0.0;
	instance.PAnalogb.ParRegb[0] = false;
	instance.PAnalogb.ParRegb[1] = false;
	instance.PAnalogb.ParRegb[2] = false;
	instance.PAnalogb.ParRegb[3] = false;
	instance.PAnalogb.ParRegb[4] = false;
	instance.PAnalogb.ParRegb[5] = false;
	instance.PAnalogb.ParRegb[6] = false;
	instance.PAnalogb.ParRegb[7] = false;
	instance.PAnalogb.ParRegb[8] = false;
	instance.PAnalogb.ParRegb[9] = false;
	instance.PAnalogb.ParRegb[10] = false;
	instance.PAnalogb.ParRegb[11] = false;
	instance.PAnalogb.ParRegb[12] = false;
	instance.PAnalogb.ParRegb[13] = false;
	instance.PAnalogb.ParRegb[14] = false;
	instance.PAnalogb.ParRegb[15] = false;
	instance.PAnalogb.PMaxRan = 0.0;
	instance.PAnalogb.PMinRan = 0.0;
	instance.PAnalogb.PMStpInV = 0.0;
	instance.PAnalogb.PMStpDeV = 0.0;
	instance.PAnalogb.PMInSpd = 0.0;
	instance.PAnalogb.PMDeSpd = 0.0;
	instance.PAnalogb.PWDt = 0;
	instance.PAnalogb.PWDb = 0.0;
	instance.Stsreg01 = 0;
	instance.Stsreg01b[0] = false;
	instance.Stsreg01b[1] = false;
	instance.Stsreg01b[2] = false;
	instance.Stsreg01b[3] = false;
	instance.Stsreg01b[4] = false;
	instance.Stsreg01b[5] = false;
	instance.Stsreg01b[6] = false;
	instance.Stsreg01b[7] = false;
	instance.Stsreg01b[8] = false;
	instance.Stsreg01b[9] = false;
	instance.Stsreg01b[10] = false;
	instance.Stsreg01b[11] = false;
	instance.Stsreg01b[12] = false;
	instance.Stsreg01b[13] = false;
	instance.Stsreg01b[14] = false;
	instance.Stsreg01b[15] = false;
	instance.Stsreg02 = 0;
	instance.Stsreg02b[0] = false;
	instance.Stsreg02b[1] = false;
	instance.Stsreg02b[2] = false;
	instance.Stsreg02b[3] = false;
	instance.Stsreg02b[4] = false;
	instance.Stsreg02b[5] = false;
	instance.Stsreg02b[6] = false;
	instance.Stsreg02b[7] = false;
	instance.Stsreg02b[8] = false;
	instance.Stsreg02b[9] = false;
	instance.Stsreg02b[10] = false;
	instance.Stsreg02b[11] = false;
	instance.Stsreg02b[12] = false;
	instance.Stsreg02b[13] = false;
	instance.Stsreg02b[14] = false;
	instance.Stsreg02b[15] = false;
	instance.OutOV = 0.0;
	instance.AuPosRSt = 0.0;
	instance.MPosRSt = 0.0;
	instance.PosSt = 0.0;
	instance.PosRSt = 0.0;
	instance.OutOnOV = false;
	instance.OutOnOVSt = false;
	instance.MOnRSt = false;
	instance.MOffRSt = false;
	instance.AuOnRSt = false;
	instance.AuOffRSt = false;
	instance.HOnRSt = false;
	instance.HOffRSt = false;
	instance.OnSt = false;
	instance.AuMoSt = false;
	instance.MMoSt = false;
	instance.LDSt = false;
	instance.SoftLDSt = false;
	instance.FoMoSt = false;
	instance.IOErrorW = false;
	instance.IOSimuW = false;
	instance.AuMRW = false;
	instance.PosW = false;
	instance.StartISt = false;
	instance.TStopISt = false;
	instance.FuStopISt = false;
	instance.AlSt = false;
	instance.AlUnAck = false;
	instance.AlBW = false;
	instance.EnRstartSt = true;
	instance.RdyStartSt = false;
	instance.E_MAuMoR = false;
	instance.E_MMMoR = false;
	instance.E_MFoMoR = false;
	instance.E_MOnR = false;
	instance.E_MOffR = false;
	instance.E_MAlAckR = false;
	instance.E_StartI = false;
	instance.E_TStopI = false;
	instance.E_FuStopI = false;
	instance.E_Al = false;
	instance.E_AuAuMoR = false;
	instance.E_AuAlAckR = false;
	instance.E_MNewPosR = false;
	instance.E_MStpInR = false;
	instance.E_MStpDeR = false;
	instance.E_MSoftLDR = false;
	instance.E_MEnRstartR = false;
	instance.RE_AlUnAck = false;
	instance.FE_AlUnAck = false;
	instance.MAuMoR_old = false;
	instance.MMMoR_old = false;
	instance.MFoMoR_old = false;
	instance.MOnR_old = false;
	instance.MOffR_old = false;
	instance.MAlAckR_old = false;
	instance.AuAuMoR_old = false;
	instance.AuAlAckR_old = false;
	instance.StartI_old = false;
	instance.TStopI_old = false;
	instance.FuStopI_old = false;
	instance.Al_old = false;
	instance.MNewPosR_old = false;
	instance.MStpInR_old = false;
	instance.MStpDeR_old = false;
	instance.AlUnAck_old = false;
	instance.MSoftLDR_old = false;
	instance.MEnRstartR_old = false;
	instance.PosR = 0.0;
	instance.PFsPosOn = false;
	instance.PHFOn = false;
	instance.PHFPos = false;
	instance.PPulse = false;
	instance.PHLD = false;
	instance.PHLDCmd = false;
	instance.PEnRstart = false;
	instance.PRstartFS = false;
	instance.AnalogOnSt = false;
	instance.AnalogOffSt = false;
	instance.AuMoSt_aux = false;
	instance.MMoSt_aux = false;
	instance.FoMoSt_aux = false;
	instance.SoftLDSt_aux = false;
	instance.fullNotAcknowledged = false;
	instance.InterlockR = false;
	instance.Ramp_parameters.inc_rate = 0.0;
	instance.Ramp_parameters.dec_rate = 0.0;
	instance.ROC_LIM.INV = 0.0;
	instance.ROC_LIM.UPRLM_P = 10.0;
	instance.ROC_LIM.DNRLM_P = 10.0;
	instance.ROC_LIM.UPRLM_N = 10.0;
	instance.ROC_LIM.DNRLM_N = 10.0;
	instance.ROC_LIM.H_LM = 100.0;
	instance.ROC_LIM.L_LM = 0.0;
	instance.ROC_LIM.PV = 0.0;
	instance.ROC_LIM.DF_OUTV = 0.0;
	instance.ROC_LIM.DFOUT_ON = false;
	instance.ROC_LIM.TRACK = false;
	instance.ROC_LIM.MAN_ON = false;
	instance.ROC_LIM.COM_RST = false;
	instance.ROC_LIM.CYCLE = 1000;
	instance.ROC_LIM.OUTV = 0.0;
	instance.ROC_LIM.QUPRLM_P = false;
	instance.ROC_LIM.QDNRLM_P = false;
	instance.ROC_LIM.QUPRLM_N = false;
	instance.ROC_LIM.QDNRLM_N = false;
	instance.ROC_LIM.QH_LM = false;
	instance.ROC_LIM.QL_LM = false;
	instance.Time_Warning = 0;
	instance.Timer_Warning.PT = 0;
	instance.Timer_Warning.IN = false;
	instance.Timer_Warning.Q = false;
	instance.Timer_Warning.ET = 0;
	instance.Timer_Warning.running = false;
	instance.Timer_Warning.start = 0;
	instance.PulseWidth = 0.0;
	instance.FSIinc = 0;
	instance.TSIinc = 0;
	instance.SIinc = 0;
	instance.Alinc = 0;
	instance.WAlSt = false;
	instance.WFuStopISt = false;
	instance.WTStopISt = false;
	instance.WStartISt = false;
	R_EDGE1_inlined_1.new = false;
	R_EDGE1_inlined_1.old = false;
	R_EDGE1_inlined_1.RET_VAL = false;
	R_EDGE1_inlined_2.new = false;
	R_EDGE1_inlined_2.old = false;
	R_EDGE1_inlined_2.RET_VAL = false;
	R_EDGE1_inlined_3.new = false;
	R_EDGE1_inlined_3.old = false;
	R_EDGE1_inlined_3.RET_VAL = false;
	R_EDGE1_inlined_4.new = false;
	R_EDGE1_inlined_4.old = false;
	R_EDGE1_inlined_4.RET_VAL = false;
	R_EDGE1_inlined_5.new = false;
	R_EDGE1_inlined_5.old = false;
	R_EDGE1_inlined_5.RET_VAL = false;
	R_EDGE1_inlined_6.new = false;
	R_EDGE1_inlined_6.old = false;
	R_EDGE1_inlined_6.RET_VAL = false;
	R_EDGE1_inlined_7.new = false;
	R_EDGE1_inlined_7.old = false;
	R_EDGE1_inlined_7.RET_VAL = false;
	R_EDGE1_inlined_8.new = false;
	R_EDGE1_inlined_8.old = false;
	R_EDGE1_inlined_8.RET_VAL = false;
	R_EDGE1_inlined_9.new = false;
	R_EDGE1_inlined_9.old = false;
	R_EDGE1_inlined_9.RET_VAL = false;
	R_EDGE1_inlined_10.new = false;
	R_EDGE1_inlined_10.old = false;
	R_EDGE1_inlined_10.RET_VAL = false;
	R_EDGE1_inlined_11.new = false;
	R_EDGE1_inlined_11.old = false;
	R_EDGE1_inlined_11.RET_VAL = false;
	R_EDGE1_inlined_12.new = false;
	R_EDGE1_inlined_12.old = false;
	R_EDGE1_inlined_12.RET_VAL = false;
	R_EDGE1_inlined_13.new = false;
	R_EDGE1_inlined_13.old = false;
	R_EDGE1_inlined_13.RET_VAL = false;
	R_EDGE1_inlined_14.new = false;
	R_EDGE1_inlined_14.old = false;
	R_EDGE1_inlined_14.RET_VAL = false;
	R_EDGE1_inlined_15.new = false;
	R_EDGE1_inlined_15.old = false;
	R_EDGE1_inlined_15.RET_VAL = false;
	R_EDGE1_inlined_16.new = false;
	R_EDGE1_inlined_16.old = false;
	R_EDGE1_inlined_16.RET_VAL = false;
	R_EDGE1_inlined_17.new = false;
	R_EDGE1_inlined_17.old = false;
	R_EDGE1_inlined_17.RET_VAL = false;
	ABS#REAL1_inlined_18.in = 0.0;
	ABS#REAL1_inlined_18.RET_VAL = 0.0;
	ABS#REAL1_inlined_19.in = 0.0;
	ABS#REAL1_inlined_19.RET_VAL = 0.0;
	ABS#REAL1_inlined_20.in = 0.0;
	ABS#REAL1_inlined_20.RET_VAL = 0.0;
	ABS#REAL1_inlined_21.in = 0.0;
	ABS#REAL1_inlined_21.RET_VAL = 0.0;
	ABS#REAL1_inlined_22.in = 0.0;
	ABS#REAL1_inlined_22.RET_VAL = 0.0;
	__assertion_error = 0;
	
	VerificationLoop();
}
