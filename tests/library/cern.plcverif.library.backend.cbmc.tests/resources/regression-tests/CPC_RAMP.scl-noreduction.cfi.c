#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Variables
float instance_INV = 0.0;
float instance_InSpd = 0.0;
float instance_DeSpd = 0.0;
float instance_H_LM = 0.0;
float instance_L_LM = 0.0;
float instance_PV = 0.0;
float instance_DF_OUTV = 0.0;
bool instance_DFOUT_ON = false;
bool instance_TRACK = false;
bool instance_MAN_ON = false;
bool instance_COM_RST = false;
int32_t instance_CYCLE = 0;
float instance_OUTV = 0.0;
bool instance_DONE = false;
float instance_INV_sat = 0.0;
uint16_t __assertion_error = 0;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			instance_COM_RST = nondet_bool();
			instance_CYCLE = nondet_int32_t();
			instance_DFOUT_ON = nondet_bool();
			instance_DF_OUTV = nondet_float();
			instance_DeSpd = nondet_float();
			instance_H_LM = nondet_float();
			instance_INV = nondet_float();
			instance_InSpd = nondet_float();
			instance_L_LM = nondet_float();
			instance_MAN_ON = nondet_bool();
			instance_PV = nondet_float();
			instance_TRACK = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
		if ((instance_INV > instance_H_LM)) {
			goto verificationLoop_VerificationLoop_l1;
		}
		if (((! (instance_INV > instance_H_LM)) && (instance_INV < instance_L_LM))) {
			goto verificationLoop_VerificationLoop_l3;
		}
		if (((! (instance_INV > instance_H_LM)) && (! ((! (instance_INV > instance_H_LM)) && (instance_INV < instance_L_LM))))) {
			goto verificationLoop_VerificationLoop_l5;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			instance_INV_sat = instance_H_LM;
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
			goto verificationLoop_VerificationLoop_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l3: {
			instance_INV_sat = instance_L_LM;
			goto verificationLoop_VerificationLoop_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l4: {
			goto verificationLoop_VerificationLoop_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l5: {
			instance_INV_sat = instance_INV;
			goto verificationLoop_VerificationLoop_l6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l6: {
			goto verificationLoop_VerificationLoop_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l7: {
		if (instance_COM_RST) {
			goto verificationLoop_VerificationLoop_l8;
		}
		if (((! instance_COM_RST) && instance_TRACK)) {
			goto verificationLoop_VerificationLoop_l11;
		}
		if ((((! instance_COM_RST) && (! instance_TRACK)) && instance_MAN_ON)) {
			goto verificationLoop_VerificationLoop_l20;
		}
		if ((((! instance_COM_RST) && ((! instance_TRACK) && (! instance_MAN_ON))) && instance_DFOUT_ON)) {
			goto verificationLoop_VerificationLoop_l29;
		}
		if (((! instance_COM_RST) && ((! ((! instance_COM_RST) && instance_TRACK)) && ((! (((! instance_COM_RST) && (! instance_TRACK)) && instance_MAN_ON)) && (! (((! instance_COM_RST) && ((! instance_TRACK) && (! instance_MAN_ON))) && instance_DFOUT_ON)))))) {
			goto verificationLoop_VerificationLoop_l38;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l8: {
			instance_OUTV = 0.0;
			goto verificationLoop_VerificationLoop_l9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l9: {
			instance_DONE = false;
			goto verificationLoop_VerificationLoop_l10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l10: {
			goto verificationLoop_VerificationLoop_l53;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l11: {
		if ((instance_INV > instance_H_LM)) {
			goto verificationLoop_VerificationLoop_l12;
		}
		if (((! (instance_INV > instance_H_LM)) && (instance_INV < instance_L_LM))) {
			goto verificationLoop_VerificationLoop_l14;
		}
		if (((! (instance_INV > instance_H_LM)) && (! ((! (instance_INV > instance_H_LM)) && (instance_INV < instance_L_LM))))) {
			goto verificationLoop_VerificationLoop_l16;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l12: {
			instance_OUTV = instance_H_LM;
			goto verificationLoop_VerificationLoop_l13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l13: {
			goto verificationLoop_VerificationLoop_l18;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l14: {
			instance_OUTV = instance_L_LM;
			goto verificationLoop_VerificationLoop_l15;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l15: {
			goto verificationLoop_VerificationLoop_l18;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l16: {
			instance_OUTV = instance_INV;
			goto verificationLoop_VerificationLoop_l17;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l17: {
			goto verificationLoop_VerificationLoop_l18;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l18: {
			instance_DONE = false;
			goto verificationLoop_VerificationLoop_l19;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l19: {
			goto verificationLoop_VerificationLoop_l53;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l20: {
		if ((instance_PV > instance_H_LM)) {
			goto verificationLoop_VerificationLoop_l21;
		}
		if (((! (instance_PV > instance_H_LM)) && (instance_PV < instance_L_LM))) {
			goto verificationLoop_VerificationLoop_l23;
		}
		if (((! (instance_PV > instance_H_LM)) && (! ((! (instance_PV > instance_H_LM)) && (instance_PV < instance_L_LM))))) {
			goto verificationLoop_VerificationLoop_l25;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l21: {
			instance_OUTV = instance_H_LM;
			goto verificationLoop_VerificationLoop_l22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l22: {
			goto verificationLoop_VerificationLoop_l27;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l23: {
			instance_OUTV = instance_L_LM;
			goto verificationLoop_VerificationLoop_l24;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l24: {
			goto verificationLoop_VerificationLoop_l27;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l25: {
			instance_OUTV = instance_PV;
			goto verificationLoop_VerificationLoop_l26;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l26: {
			goto verificationLoop_VerificationLoop_l27;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l27: {
			instance_DONE = false;
			goto verificationLoop_VerificationLoop_l28;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l28: {
			goto verificationLoop_VerificationLoop_l53;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l29: {
		if ((instance_DF_OUTV > instance_H_LM)) {
			goto verificationLoop_VerificationLoop_l30;
		}
		if (((! (instance_DF_OUTV > instance_H_LM)) && (instance_DF_OUTV < instance_L_LM))) {
			goto verificationLoop_VerificationLoop_l32;
		}
		if (((! (instance_DF_OUTV > instance_H_LM)) && (! ((! (instance_DF_OUTV > instance_H_LM)) && (instance_DF_OUTV < instance_L_LM))))) {
			goto verificationLoop_VerificationLoop_l34;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l30: {
			instance_OUTV = instance_H_LM;
			goto verificationLoop_VerificationLoop_l31;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l31: {
			goto verificationLoop_VerificationLoop_l36;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l32: {
			instance_OUTV = instance_L_LM;
			goto verificationLoop_VerificationLoop_l33;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l33: {
			goto verificationLoop_VerificationLoop_l36;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l34: {
			instance_OUTV = instance_DF_OUTV;
			goto verificationLoop_VerificationLoop_l35;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l35: {
			goto verificationLoop_VerificationLoop_l36;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l36: {
			instance_DONE = false;
			goto verificationLoop_VerificationLoop_l37;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l37: {
			goto verificationLoop_VerificationLoop_l53;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l38: {
		if ((instance_OUTV < (instance_INV_sat - ((instance_InSpd * ((float) ((int32_t) instance_CYCLE))) / 1000.0)))) {
			goto verificationLoop_VerificationLoop_l39;
		}
		if (((! (instance_OUTV < (instance_INV_sat - ((instance_InSpd * ((float) ((int32_t) instance_CYCLE))) / 1000.0)))) && ((instance_OUTV < instance_INV_sat) && (instance_OUTV > (instance_INV_sat - ((instance_InSpd * ((float) ((int32_t) instance_CYCLE))) / 1000.0)))))) {
			goto verificationLoop_VerificationLoop_l42;
		}
		if ((((! (instance_OUTV < (instance_INV_sat - ((instance_InSpd * ((float) ((int32_t) instance_CYCLE))) / 1000.0)))) && (! ((instance_OUTV < instance_INV_sat) && (instance_OUTV > (instance_INV_sat - ((instance_InSpd * ((float) ((int32_t) instance_CYCLE))) / 1000.0)))))) && (instance_OUTV > (instance_INV_sat + ((instance_DeSpd * ((float) ((int32_t) instance_CYCLE))) / 1000.0))))) {
			goto verificationLoop_VerificationLoop_l45;
		}
		if ((((! (instance_OUTV < (instance_INV_sat - ((instance_InSpd * ((float) ((int32_t) instance_CYCLE))) / 1000.0)))) && ((! ((instance_OUTV < instance_INV_sat) && (instance_OUTV > (instance_INV_sat - ((instance_InSpd * ((float) ((int32_t) instance_CYCLE))) / 1000.0))))) && (! (instance_OUTV > (instance_INV_sat + ((instance_DeSpd * ((float) ((int32_t) instance_CYCLE))) / 1000.0)))))) && ((instance_OUTV > instance_INV_sat) && (instance_OUTV < (instance_INV_sat + ((instance_DeSpd * ((float) ((int32_t) instance_CYCLE))) / 1000.0)))))) {
			goto verificationLoop_VerificationLoop_l48;
		}
		if (((! (instance_OUTV < (instance_INV_sat - ((instance_InSpd * ((float) ((int32_t) instance_CYCLE))) / 1000.0)))) && ((! ((! (instance_OUTV < (instance_INV_sat - ((instance_InSpd * ((float) ((int32_t) instance_CYCLE))) / 1000.0)))) && ((instance_OUTV < instance_INV_sat) && (instance_OUTV > (instance_INV_sat - ((instance_InSpd * ((float) ((int32_t) instance_CYCLE))) / 1000.0)))))) && ((! (((! (instance_OUTV < (instance_INV_sat - ((instance_InSpd * ((float) ((int32_t) instance_CYCLE))) / 1000.0)))) && (! ((instance_OUTV < instance_INV_sat) && (instance_OUTV > (instance_INV_sat - ((instance_InSpd * ((float) ((int32_t) instance_CYCLE))) / 1000.0)))))) && (instance_OUTV > (instance_INV_sat + ((instance_DeSpd * ((float) ((int32_t) instance_CYCLE))) / 1000.0))))) && (! (((! (instance_OUTV < (instance_INV_sat - ((instance_InSpd * ((float) ((int32_t) instance_CYCLE))) / 1000.0)))) && ((! ((instance_OUTV < instance_INV_sat) && (instance_OUTV > (instance_INV_sat - ((instance_InSpd * ((float) ((int32_t) instance_CYCLE))) / 1000.0))))) && (! (instance_OUTV > (instance_INV_sat + ((instance_DeSpd * ((float) ((int32_t) instance_CYCLE))) / 1000.0)))))) && ((instance_OUTV > instance_INV_sat) && (instance_OUTV < (instance_INV_sat + ((instance_DeSpd * ((float) ((int32_t) instance_CYCLE))) / 1000.0)))))))))) {
			goto verificationLoop_VerificationLoop_l51;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l39: {
			instance_OUTV = (instance_OUTV + ((instance_InSpd * ((float) ((int32_t) instance_CYCLE))) / 1000.0));
			goto verificationLoop_VerificationLoop_l40;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l40: {
			instance_DONE = false;
			goto verificationLoop_VerificationLoop_l41;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l41: {
			goto verificationLoop_VerificationLoop_l52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l42: {
			instance_OUTV = instance_INV_sat;
			goto verificationLoop_VerificationLoop_l43;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l43: {
			instance_DONE = true;
			goto verificationLoop_VerificationLoop_l44;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l44: {
			goto verificationLoop_VerificationLoop_l52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l45: {
			instance_OUTV = (instance_OUTV - ((instance_DeSpd * ((float) ((int32_t) instance_CYCLE))) / 1000.0));
			goto verificationLoop_VerificationLoop_l46;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l46: {
			instance_DONE = false;
			goto verificationLoop_VerificationLoop_l47;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l47: {
			goto verificationLoop_VerificationLoop_l52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l48: {
			instance_OUTV = instance_INV_sat;
			goto verificationLoop_VerificationLoop_l49;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l49: {
			instance_DONE = true;
			goto verificationLoop_VerificationLoop_l50;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l50: {
			goto verificationLoop_VerificationLoop_l52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l51: {
			goto verificationLoop_VerificationLoop_l52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l52: {
			goto verificationLoop_VerificationLoop_l53;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l53: {
			goto callEnd;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
