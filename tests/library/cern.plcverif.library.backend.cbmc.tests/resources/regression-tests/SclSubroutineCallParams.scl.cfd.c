#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Root data structure
typedef struct {
	bool RET_VAL;
} __callParamsFunc2;
typedef struct {
	bool IN;
	bool RET_VAL;
} __callParamsFunc3;
typedef struct {
	bool i1;
	bool i2;
	bool RET_VAL;
} __callParamsFunc4;
typedef struct {
	bool i1;
	bool i2;
	bool out1;
	bool RET_VAL;
} __callParamsFunc5;
typedef struct {
	bool i1;
	bool i2;
	bool inout1;
	bool RET_VAL;
} __callParamsFunc6;

// Global variables
bool MX2_0;
bool MX0_0;
bool MX3_0;
bool MX3_1;
bool MX1_0;
bool MX1_1;
bool MX4_0;
bool MX4_1;
bool MX5_4;
bool MX5_0;
bool MX5_5;
bool MX5_1;
bool MX5_6;
bool MX5_2;
bool MX6_4;
bool MX6_0;
bool MX6_5;
bool MX6_1;
bool MX6_6;
bool MX6_2;
__callParamsFunc2 callParamsFunc21;
__callParamsFunc3 callParamsFunc31;
__callParamsFunc4 callParamsFunc41;
__callParamsFunc5 callParamsFunc51;
__callParamsFunc6 callParamsFunc61;
__callParamsFunc3 callParamsFunc31_inlined_1;
__callParamsFunc3 callParamsFunc31_inlined_2;
__callParamsFunc4 callParamsFunc41_inlined_3;
__callParamsFunc4 callParamsFunc41_inlined_4;
__callParamsFunc5 callParamsFunc51_inlined_5;
__callParamsFunc5 callParamsFunc51_inlined_6;
__callParamsFunc5 callParamsFunc51_inlined_7;
__callParamsFunc6 callParamsFunc61_inlined_8;
__callParamsFunc6 callParamsFunc61_inlined_9;
__callParamsFunc6 callParamsFunc61_inlined_10;
uint16_t __assertion_error;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Automata declarations
void callParams_OB1();
void callParamsFunc1();
void callParamsFunc2(__callParamsFunc2 *__context);
void callParamsFunc3(__callParamsFunc3 *__context);
void callParamsFunc4(__callParamsFunc4 *__context);
void callParamsFunc5(__callParamsFunc5 *__context);
void callParamsFunc6(__callParamsFunc6 *__context);
void VerificationLoop();

// Automata
void callParams_OB1() {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			callParamsFunc1();
			goto l1;
		//assert(false);
		return;  			}
	l1: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void callParamsFunc1() {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			// Assign inputs
			callParamsFunc2(&callParamsFunc21);
			// Assign outputs
			MX2_0 = callParamsFunc21.RET_VAL;
			goto l11;
		//assert(false);
		return;  			}
	l11: {
			// Assign inputs
			callParamsFunc31_inlined_1.IN = MX0_0;
			callParamsFunc3(&callParamsFunc31_inlined_1);
			// Assign outputs
			MX3_0 = callParamsFunc31_inlined_1.RET_VAL;
			goto l2;
		//assert(false);
		return;  			}
	l2: {
			// Assign inputs
			callParamsFunc31_inlined_2.IN = MX0_0;
			callParamsFunc3(&callParamsFunc31_inlined_2);
			// Assign outputs
			MX3_1 = callParamsFunc31_inlined_2.RET_VAL;
			goto l3;
		//assert(false);
		return;  			}
	l3: {
			// Assign inputs
			callParamsFunc41_inlined_3.i1 = MX1_0;
			callParamsFunc41_inlined_3.i2 = MX1_1;
			callParamsFunc4(&callParamsFunc41_inlined_3);
			// Assign outputs
			MX4_0 = callParamsFunc41_inlined_3.RET_VAL;
			goto l4;
		//assert(false);
		return;  			}
	l4: {
			// Assign inputs
			callParamsFunc41_inlined_4.i2 = MX1_0;
			callParamsFunc41_inlined_4.i1 = MX1_1;
			callParamsFunc4(&callParamsFunc41_inlined_4);
			// Assign outputs
			MX4_1 = callParamsFunc41_inlined_4.RET_VAL;
			goto l5;
		//assert(false);
		return;  			}
	l5: {
			// Assign inputs
			callParamsFunc51_inlined_5.i1 = MX1_0;
			callParamsFunc51_inlined_5.i2 = MX1_1;
			callParamsFunc5(&callParamsFunc51_inlined_5);
			// Assign outputs
			MX5_4 = callParamsFunc51_inlined_5.out1;
			MX5_0 = callParamsFunc51_inlined_5.RET_VAL;
			goto l6;
		//assert(false);
		return;  			}
	l6: {
			// Assign inputs
			callParamsFunc51_inlined_6.i1 = MX1_0;
			callParamsFunc51_inlined_6.i2 = MX1_1;
			callParamsFunc5(&callParamsFunc51_inlined_6);
			// Assign outputs
			MX5_5 = callParamsFunc51_inlined_6.out1;
			MX5_1 = callParamsFunc51_inlined_6.RET_VAL;
			goto l7;
		//assert(false);
		return;  			}
	l7: {
			// Assign inputs
			callParamsFunc51_inlined_7.i1 = MX1_0;
			callParamsFunc51_inlined_7.i2 = MX1_1;
			callParamsFunc5(&callParamsFunc51_inlined_7);
			// Assign outputs
			MX5_6 = callParamsFunc51_inlined_7.out1;
			MX5_2 = callParamsFunc51_inlined_7.RET_VAL;
			goto l8;
		//assert(false);
		return;  			}
	l8: {
			// Assign inputs
			callParamsFunc61_inlined_8.i1 = MX1_0;
			callParamsFunc61_inlined_8.i2 = MX1_1;
			callParamsFunc61_inlined_8.inout1 = MX6_4;
			callParamsFunc6(&callParamsFunc61_inlined_8);
			// Assign outputs
			MX6_4 = callParamsFunc61_inlined_8.inout1;
			MX6_0 = callParamsFunc61_inlined_8.RET_VAL;
			goto l9;
		//assert(false);
		return;  			}
	l9: {
			// Assign inputs
			callParamsFunc61_inlined_9.inout1 = MX6_5;
			callParamsFunc61_inlined_9.i1 = MX1_0;
			callParamsFunc61_inlined_9.i2 = MX1_1;
			callParamsFunc6(&callParamsFunc61_inlined_9);
			// Assign outputs
			MX6_5 = callParamsFunc61_inlined_9.inout1;
			MX6_1 = callParamsFunc61_inlined_9.RET_VAL;
			goto l10;
		//assert(false);
		return;  			}
	l10: {
			// Assign inputs
			callParamsFunc61_inlined_10.i1 = MX1_0;
			callParamsFunc61_inlined_10.i2 = MX1_1;
			callParamsFunc61_inlined_10.inout1 = MX6_6;
			callParamsFunc6(&callParamsFunc61_inlined_10);
			// Assign outputs
			MX6_6 = callParamsFunc61_inlined_10.inout1;
			MX6_2 = callParamsFunc61_inlined_10.RET_VAL;
			goto l111;
		//assert(false);
		return;  			}
	l111: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void callParamsFunc2(__callParamsFunc2 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init2;
	init2: {
			__context->RET_VAL = false;
			goto l12;
		//assert(false);
		return;  			}
	l12: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void callParamsFunc3(__callParamsFunc3 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init3;
	init3: {
			__context->RET_VAL = (! __context->IN);
			goto l13;
		//assert(false);
		return;  			}
	l13: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void callParamsFunc4(__callParamsFunc4 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init4;
	init4: {
			__context->RET_VAL = (__context->i1 && __context->i2);
			goto l14;
		//assert(false);
		return;  			}
	l14: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void callParamsFunc5(__callParamsFunc5 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init5;
	init5: {
			__context->out1 = (__context->i1 || __context->i2);
			__context->RET_VAL = (__context->i1 && __context->i2);
			goto l21;
		//assert(false);
		return;  			}
	l21: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void callParamsFunc6(__callParamsFunc6 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init6;
	init6: {
			__context->inout1 = (__context->inout1 || __context->i2);
			__context->RET_VAL = (__context->i1 && __context->i2);
			goto l22;
		//assert(false);
		return;  			}
	l22: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init7;
	init7: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			callParams_OB1();
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
void main() {
	// Initial values
	MX2_0 = false;
	MX0_0 = false;
	MX3_0 = false;
	MX3_1 = false;
	MX1_0 = false;
	MX1_1 = false;
	MX4_0 = false;
	MX4_1 = false;
	MX5_4 = false;
	MX5_0 = false;
	MX5_5 = false;
	MX5_1 = false;
	MX5_6 = false;
	MX5_2 = false;
	MX6_4 = false;
	MX6_0 = false;
	MX6_5 = false;
	MX6_1 = false;
	MX6_6 = false;
	MX6_2 = false;
	callParamsFunc21.RET_VAL = false;
	callParamsFunc31.IN = false;
	callParamsFunc31.RET_VAL = false;
	callParamsFunc41.i1 = false;
	callParamsFunc41.i2 = false;
	callParamsFunc41.RET_VAL = false;
	callParamsFunc51.i1 = false;
	callParamsFunc51.i2 = false;
	callParamsFunc51.out1 = false;
	callParamsFunc51.RET_VAL = false;
	callParamsFunc61.i1 = false;
	callParamsFunc61.i2 = false;
	callParamsFunc61.inout1 = false;
	callParamsFunc61.RET_VAL = false;
	callParamsFunc31_inlined_1.IN = false;
	callParamsFunc31_inlined_1.RET_VAL = false;
	callParamsFunc31_inlined_2.IN = false;
	callParamsFunc31_inlined_2.RET_VAL = false;
	callParamsFunc41_inlined_3.i1 = false;
	callParamsFunc41_inlined_3.i2 = false;
	callParamsFunc41_inlined_3.RET_VAL = false;
	callParamsFunc41_inlined_4.i1 = false;
	callParamsFunc41_inlined_4.i2 = false;
	callParamsFunc41_inlined_4.RET_VAL = false;
	callParamsFunc51_inlined_5.i1 = false;
	callParamsFunc51_inlined_5.i2 = false;
	callParamsFunc51_inlined_5.out1 = false;
	callParamsFunc51_inlined_5.RET_VAL = false;
	callParamsFunc51_inlined_6.i1 = false;
	callParamsFunc51_inlined_6.i2 = false;
	callParamsFunc51_inlined_6.out1 = false;
	callParamsFunc51_inlined_6.RET_VAL = false;
	callParamsFunc51_inlined_7.i1 = false;
	callParamsFunc51_inlined_7.i2 = false;
	callParamsFunc51_inlined_7.out1 = false;
	callParamsFunc51_inlined_7.RET_VAL = false;
	callParamsFunc61_inlined_8.i1 = false;
	callParamsFunc61_inlined_8.i2 = false;
	callParamsFunc61_inlined_8.inout1 = false;
	callParamsFunc61_inlined_8.RET_VAL = false;
	callParamsFunc61_inlined_9.i1 = false;
	callParamsFunc61_inlined_9.i2 = false;
	callParamsFunc61_inlined_9.inout1 = false;
	callParamsFunc61_inlined_9.RET_VAL = false;
	callParamsFunc61_inlined_10.i1 = false;
	callParamsFunc61_inlined_10.i2 = false;
	callParamsFunc61_inlined_10.inout1 = false;
	callParamsFunc61_inlined_10.RET_VAL = false;
	__assertion_error = 0;
	
	VerificationLoop();
}
