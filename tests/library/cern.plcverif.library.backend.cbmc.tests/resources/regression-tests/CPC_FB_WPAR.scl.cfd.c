#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Root data structure
typedef struct {
	int16_t InterfaceParam1;
	int16_t InterfaceParam2;
	int16_t FEType;
	int16_t index;
	uint16_t PosSt;
	uint16_t MPosRSt;
	bool ArmRcp_old;
	bool ActRcp_old;
	bool MNewMR_old;
	bool ArmRcpSt;
	bool PQWDef;
} __CPC_DB_WPAR;
typedef struct {
	uint16_t Manreg01;
	bool Manreg01b[16];
	uint16_t MPosR;
	uint16_t StsReg01;
	__CPC_DB_WPAR Perst;
} __CPC_FB_WPAR;
typedef struct {
	bool new;
	bool old;
	bool RET_VAL;
} __R_EDGE;

// Global variables
__R_EDGE R_EDGE1;
__CPC_FB_WPAR instance;
__R_EDGE R_EDGE1_inlined_1;
__R_EDGE R_EDGE1_inlined_2;
__R_EDGE R_EDGE1_inlined_3;
uint16_t __assertion_error;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Automata declarations
void CPC_FB_WPAR(__CPC_FB_WPAR *__context);
void R_EDGE(__R_EDGE *__context);
void VerificationLoop();

// Automata
void CPC_FB_WPAR(__CPC_FB_WPAR *__context) {
	// Temporary variables
	bool E_ArmRcp;
	bool E_ActRcp;
	bool E_MNewMR;
	uint16_t TempStsReg01;
	bool StsReg01b[16];
	
	// Start with initial location
	goto init;
	init: {
			__context->Manreg01b[0] = ((__context->Manreg01 & 256) != 0);
			__context->Manreg01b[1] = ((__context->Manreg01 & 512) != 0);
			__context->Manreg01b[2] = ((__context->Manreg01 & 1024) != 0);
			__context->Manreg01b[3] = ((__context->Manreg01 & 2048) != 0);
			__context->Manreg01b[4] = ((__context->Manreg01 & 4096) != 0);
			__context->Manreg01b[5] = ((__context->Manreg01 & 8192) != 0);
			__context->Manreg01b[6] = ((__context->Manreg01 & 16384) != 0);
			__context->Manreg01b[7] = ((__context->Manreg01 & 32768) != 0);
			__context->Manreg01b[8] = ((__context->Manreg01 & 1) != 0);
			__context->Manreg01b[9] = ((__context->Manreg01 & 2) != 0);
			__context->Manreg01b[10] = ((__context->Manreg01 & 4) != 0);
			__context->Manreg01b[11] = ((__context->Manreg01 & 8) != 0);
			__context->Manreg01b[12] = ((__context->Manreg01 & 16) != 0);
			__context->Manreg01b[13] = ((__context->Manreg01 & 32) != 0);
			__context->Manreg01b[14] = ((__context->Manreg01 & 64) != 0);
			__context->Manreg01b[15] = ((__context->Manreg01 & 128) != 0);
			StsReg01b[0] = ((TempStsReg01 & 256) != 0);
			StsReg01b[1] = ((TempStsReg01 & 512) != 0);
			StsReg01b[2] = ((TempStsReg01 & 1024) != 0);
			StsReg01b[3] = ((TempStsReg01 & 2048) != 0);
			StsReg01b[4] = ((TempStsReg01 & 4096) != 0);
			StsReg01b[5] = ((TempStsReg01 & 8192) != 0);
			StsReg01b[6] = ((TempStsReg01 & 16384) != 0);
			StsReg01b[7] = ((TempStsReg01 & 32768) != 0);
			StsReg01b[8] = ((TempStsReg01 & 1) != 0);
			StsReg01b[9] = ((TempStsReg01 & 2) != 0);
			StsReg01b[10] = ((TempStsReg01 & 4) != 0);
			StsReg01b[11] = ((TempStsReg01 & 8) != 0);
			StsReg01b[12] = ((TempStsReg01 & 16) != 0);
			StsReg01b[13] = ((TempStsReg01 & 32) != 0);
			StsReg01b[14] = ((TempStsReg01 & 64) != 0);
			StsReg01b[15] = ((TempStsReg01 & 128) != 0);
			goto x;
		//assert(false);
		return;  			}
	l1: {
			TempStsReg01 = __context->StsReg01;
			goto x2;
		//assert(false);
		return;  			}
	l2: {
			// Assign inputs
			R_EDGE1_inlined_1.new = __context->Manreg01b[10];
			R_EDGE1_inlined_1.old = __context->Perst.ArmRcp_old;
			R_EDGE(&R_EDGE1_inlined_1);
			// Assign outputs
			__context->Perst.ArmRcp_old = R_EDGE1_inlined_1.old;
			E_ArmRcp = R_EDGE1_inlined_1.RET_VAL;
			goto l3;
		//assert(false);
		return;  			}
	l3: {
			// Assign inputs
			R_EDGE1_inlined_2.new = __context->Manreg01b[11];
			R_EDGE1_inlined_2.old = __context->Perst.ActRcp_old;
			R_EDGE(&R_EDGE1_inlined_2);
			// Assign outputs
			__context->Perst.ActRcp_old = R_EDGE1_inlined_2.old;
			E_ActRcp = R_EDGE1_inlined_2.RET_VAL;
			goto l4;
		//assert(false);
		return;  			}
	l4: {
			// Assign inputs
			R_EDGE1_inlined_3.new = __context->Manreg01b[14];
			R_EDGE1_inlined_3.old = __context->Perst.MNewMR_old;
			R_EDGE(&R_EDGE1_inlined_3);
			// Assign outputs
			__context->Perst.MNewMR_old = R_EDGE1_inlined_3.old;
			E_MNewMR = R_EDGE1_inlined_3.RET_VAL;
			goto l5;
		//assert(false);
		return;  			}
	l5: {
		if (E_ArmRcp) {
			__context->Perst.ArmRcpSt = true;
			goto l9;
		}
		if ((! E_ArmRcp)) {
			goto l9;
		}
		//assert(false);
		return;  			}
	l9: {
		if ((E_ArmRcp && E_ActRcp)) {
			__context->Perst.ArmRcpSt = false;
			goto l13;
		}
		if ((! (E_ArmRcp && E_ActRcp))) {
			goto l13;
		}
		//assert(false);
		return;  			}
	l13: {
			__context->Perst.MPosRSt = __context->MPosR;
			goto l14;
		//assert(false);
		return;  			}
	l14: {
		if (((E_MNewMR && (! __context->Perst.ArmRcpSt)) || ((E_MNewMR && __context->Perst.ArmRcpSt) && E_ActRcp))) {
			__context->Perst.PosSt = __context->MPosR;
			goto l18;
		}
		if ((! ((E_MNewMR && (! __context->Perst.ArmRcpSt)) || ((E_MNewMR && __context->Perst.ArmRcpSt) && E_ActRcp)))) {
			goto l18;
		}
		//assert(false);
		return;  			}
	l18: {
		if ((__context->Perst.ArmRcpSt && E_ActRcp)) {
			__context->Perst.ArmRcpSt = false;
			goto l22;
		}
		if ((! (__context->Perst.ArmRcpSt && E_ActRcp))) {
			goto l22;
		}
		//assert(false);
		return;  			}
	l22: {
			StsReg01b[8] = false;
			goto x3;
		//assert(false);
		return;  			}
	l23: {
			StsReg01b[9] = false;
			goto x4;
		//assert(false);
		return;  			}
	l24: {
			StsReg01b[10] = false;
			goto x5;
		//assert(false);
		return;  			}
	l25: {
			StsReg01b[11] = __context->Perst.ArmRcpSt;
			goto x6;
		//assert(false);
		return;  			}
	l26: {
			StsReg01b[12] = false;
			goto x7;
		//assert(false);
		return;  			}
	l27: {
			StsReg01b[13] = false;
			goto x8;
		//assert(false);
		return;  			}
	l28: {
			StsReg01b[14] = false;
			goto x9;
		//assert(false);
		return;  			}
	l29: {
			StsReg01b[15] = false;
			goto x10;
		//assert(false);
		return;  			}
	l30: {
			StsReg01b[0] = false;
			goto x11;
		//assert(false);
		return;  			}
	l31: {
			StsReg01b[1] = false;
			goto x12;
		//assert(false);
		return;  			}
	l32: {
			StsReg01b[2] = false;
			goto x13;
		//assert(false);
		return;  			}
	l33: {
			StsReg01b[3] = false;
			goto x14;
		//assert(false);
		return;  			}
	l34: {
			StsReg01b[4] = false;
			goto x15;
		//assert(false);
		return;  			}
	l35: {
			StsReg01b[5] = false;
			goto x16;
		//assert(false);
		return;  			}
	l36: {
			StsReg01b[6] = false;
			goto x17;
		//assert(false);
		return;  			}
	l37: {
			StsReg01b[7] = false;
			goto x18;
		//assert(false);
		return;  			}
	l38: {
			__context->StsReg01 = TempStsReg01;
			goto l39;
		//assert(false);
		return;  			}
	l39: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	x: {
			E_ActRcp = false;
			E_ArmRcp = false;
			E_MNewMR = false;
			TempStsReg01 = 0;
			goto x1;
		//assert(false);
		return;  			}
	x1: {
			StsReg01b[0] = ((TempStsReg01 & 256) != 0);
			StsReg01b[1] = ((TempStsReg01 & 512) != 0);
			StsReg01b[2] = ((TempStsReg01 & 1024) != 0);
			StsReg01b[3] = ((TempStsReg01 & 2048) != 0);
			StsReg01b[4] = ((TempStsReg01 & 4096) != 0);
			StsReg01b[5] = ((TempStsReg01 & 8192) != 0);
			StsReg01b[6] = ((TempStsReg01 & 16384) != 0);
			StsReg01b[7] = ((TempStsReg01 & 32768) != 0);
			StsReg01b[8] = ((TempStsReg01 & 1) != 0);
			StsReg01b[9] = ((TempStsReg01 & 2) != 0);
			StsReg01b[10] = ((TempStsReg01 & 4) != 0);
			StsReg01b[11] = ((TempStsReg01 & 8) != 0);
			StsReg01b[12] = ((TempStsReg01 & 16) != 0);
			StsReg01b[13] = ((TempStsReg01 & 32) != 0);
			StsReg01b[14] = ((TempStsReg01 & 64) != 0);
			StsReg01b[15] = ((TempStsReg01 & 128) != 0);
			goto l1;
		//assert(false);
		return;  			}
	x2: {
			StsReg01b[0] = ((TempStsReg01 & 256) != 0);
			StsReg01b[1] = ((TempStsReg01 & 512) != 0);
			StsReg01b[2] = ((TempStsReg01 & 1024) != 0);
			StsReg01b[3] = ((TempStsReg01 & 2048) != 0);
			StsReg01b[4] = ((TempStsReg01 & 4096) != 0);
			StsReg01b[5] = ((TempStsReg01 & 8192) != 0);
			StsReg01b[6] = ((TempStsReg01 & 16384) != 0);
			StsReg01b[7] = ((TempStsReg01 & 32768) != 0);
			StsReg01b[8] = ((TempStsReg01 & 1) != 0);
			StsReg01b[9] = ((TempStsReg01 & 2) != 0);
			StsReg01b[10] = ((TempStsReg01 & 4) != 0);
			StsReg01b[11] = ((TempStsReg01 & 8) != 0);
			StsReg01b[12] = ((TempStsReg01 & 16) != 0);
			StsReg01b[13] = ((TempStsReg01 & 32) != 0);
			StsReg01b[14] = ((TempStsReg01 & 64) != 0);
			StsReg01b[15] = ((TempStsReg01 & 128) != 0);
			goto l2;
		//assert(false);
		return;  			}
	x3: {
		if (StsReg01b[8]) {
			TempStsReg01 = (TempStsReg01 | 1);
			goto l23;
		}
		if ((! StsReg01b[8])) {
			TempStsReg01 = (TempStsReg01 & 65534);
			goto l23;
		}
		//assert(false);
		return;  			}
	x4: {
		if (StsReg01b[9]) {
			TempStsReg01 = (TempStsReg01 | 2);
			goto l24;
		}
		if ((! StsReg01b[9])) {
			TempStsReg01 = (TempStsReg01 & 65533);
			goto l24;
		}
		//assert(false);
		return;  			}
	x5: {
		if (StsReg01b[10]) {
			TempStsReg01 = (TempStsReg01 | 4);
			goto l25;
		}
		if ((! StsReg01b[10])) {
			TempStsReg01 = (TempStsReg01 & 65531);
			goto l25;
		}
		//assert(false);
		return;  			}
	x6: {
		if (StsReg01b[11]) {
			TempStsReg01 = (TempStsReg01 | 8);
			goto l26;
		}
		if ((! StsReg01b[11])) {
			TempStsReg01 = (TempStsReg01 & 65527);
			goto l26;
		}
		//assert(false);
		return;  			}
	x7: {
		if (StsReg01b[12]) {
			TempStsReg01 = (TempStsReg01 | 16);
			goto l27;
		}
		if ((! StsReg01b[12])) {
			TempStsReg01 = (TempStsReg01 & 65519);
			goto l27;
		}
		//assert(false);
		return;  			}
	x8: {
		if (StsReg01b[13]) {
			TempStsReg01 = (TempStsReg01 | 32);
			goto l28;
		}
		if ((! StsReg01b[13])) {
			TempStsReg01 = (TempStsReg01 & 65503);
			goto l28;
		}
		//assert(false);
		return;  			}
	x9: {
		if (StsReg01b[14]) {
			TempStsReg01 = (TempStsReg01 | 64);
			goto l29;
		}
		if ((! StsReg01b[14])) {
			TempStsReg01 = (TempStsReg01 & 65471);
			goto l29;
		}
		//assert(false);
		return;  			}
	x10: {
		if (StsReg01b[15]) {
			TempStsReg01 = (TempStsReg01 | 128);
			goto l30;
		}
		if ((! StsReg01b[15])) {
			TempStsReg01 = (TempStsReg01 & 65407);
			goto l30;
		}
		//assert(false);
		return;  			}
	x11: {
		if (StsReg01b[0]) {
			TempStsReg01 = (TempStsReg01 | 256);
			goto l31;
		}
		if ((! StsReg01b[0])) {
			TempStsReg01 = (TempStsReg01 & 65279);
			goto l31;
		}
		//assert(false);
		return;  			}
	x12: {
		if (StsReg01b[1]) {
			TempStsReg01 = (TempStsReg01 | 512);
			goto l32;
		}
		if ((! StsReg01b[1])) {
			TempStsReg01 = (TempStsReg01 & 65023);
			goto l32;
		}
		//assert(false);
		return;  			}
	x13: {
		if (StsReg01b[2]) {
			TempStsReg01 = (TempStsReg01 | 1024);
			goto l33;
		}
		if ((! StsReg01b[2])) {
			TempStsReg01 = (TempStsReg01 & 64511);
			goto l33;
		}
		//assert(false);
		return;  			}
	x14: {
		if (StsReg01b[3]) {
			TempStsReg01 = (TempStsReg01 | 2048);
			goto l34;
		}
		if ((! StsReg01b[3])) {
			TempStsReg01 = (TempStsReg01 & 63487);
			goto l34;
		}
		//assert(false);
		return;  			}
	x15: {
		if (StsReg01b[4]) {
			TempStsReg01 = (TempStsReg01 | 4096);
			goto l35;
		}
		if ((! StsReg01b[4])) {
			TempStsReg01 = (TempStsReg01 & 61439);
			goto l35;
		}
		//assert(false);
		return;  			}
	x16: {
		if (StsReg01b[5]) {
			TempStsReg01 = (TempStsReg01 | 8192);
			goto l36;
		}
		if ((! StsReg01b[5])) {
			TempStsReg01 = (TempStsReg01 & 57343);
			goto l36;
		}
		//assert(false);
		return;  			}
	x17: {
		if (StsReg01b[6]) {
			TempStsReg01 = (TempStsReg01 | 16384);
			goto l37;
		}
		if ((! StsReg01b[6])) {
			TempStsReg01 = (TempStsReg01 & 49151);
			goto l37;
		}
		//assert(false);
		return;  			}
	x18: {
		if (StsReg01b[7]) {
			TempStsReg01 = (TempStsReg01 | 32768);
			goto l38;
		}
		if ((! StsReg01b[7])) {
			TempStsReg01 = (TempStsReg01 & 32767);
			goto l38;
		}
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void R_EDGE(__R_EDGE *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
		if (((__context->new == true) && (__context->old == false))) {
			__context->RET_VAL = true;
			__context->old = true;
			goto l7;
		}
		if ((! ((__context->new == true) && (__context->old == false)))) {
			__context->RET_VAL = false;
			__context->old = __context->new;
			goto l7;
		}
		//assert(false);
		return;  			}
	l7: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init2;
	init2: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			instance.MPosR = nondet_uint16_t();
			instance.Manreg01 = nondet_uint16_t();
			instance.Manreg01b[0] = nondet_bool();
			instance.Manreg01b[10] = nondet_bool();
			instance.Manreg01b[11] = nondet_bool();
			instance.Manreg01b[12] = nondet_bool();
			instance.Manreg01b[13] = nondet_bool();
			instance.Manreg01b[14] = nondet_bool();
			instance.Manreg01b[15] = nondet_bool();
			instance.Manreg01b[1] = nondet_bool();
			instance.Manreg01b[2] = nondet_bool();
			instance.Manreg01b[3] = nondet_bool();
			instance.Manreg01b[4] = nondet_bool();
			instance.Manreg01b[5] = nondet_bool();
			instance.Manreg01b[6] = nondet_bool();
			instance.Manreg01b[7] = nondet_bool();
			instance.Manreg01b[8] = nondet_bool();
			instance.Manreg01b[9] = nondet_bool();
			instance.Perst.ActRcp_old = nondet_bool();
			instance.Perst.ArmRcpSt = nondet_bool();
			instance.Perst.ArmRcp_old = nondet_bool();
			instance.Perst.FEType = nondet_int16_t();
			instance.Perst.InterfaceParam1 = nondet_int16_t();
			instance.Perst.InterfaceParam2 = nondet_int16_t();
			instance.Perst.MNewMR_old = nondet_bool();
			instance.Perst.MPosRSt = nondet_uint16_t();
			instance.Perst.PQWDef = nondet_bool();
			instance.Perst.PosSt = nondet_uint16_t();
			instance.Perst.index = nondet_int16_t();
			instance.StsReg01 = nondet_uint16_t();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			CPC_FB_WPAR(&instance);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
void main() {
	// Initial values
	R_EDGE1.new = false;
	R_EDGE1.old = false;
	R_EDGE1.RET_VAL = false;
	instance.Manreg01 = 0;
	instance.Manreg01b[0] = false;
	instance.Manreg01b[1] = false;
	instance.Manreg01b[2] = false;
	instance.Manreg01b[3] = false;
	instance.Manreg01b[4] = false;
	instance.Manreg01b[5] = false;
	instance.Manreg01b[6] = false;
	instance.Manreg01b[7] = false;
	instance.Manreg01b[8] = false;
	instance.Manreg01b[9] = false;
	instance.Manreg01b[10] = false;
	instance.Manreg01b[11] = false;
	instance.Manreg01b[12] = false;
	instance.Manreg01b[13] = false;
	instance.Manreg01b[14] = false;
	instance.Manreg01b[15] = false;
	instance.MPosR = 0;
	instance.StsReg01 = 0;
	instance.Perst.InterfaceParam1 = 0;
	instance.Perst.InterfaceParam2 = 0;
	instance.Perst.FEType = 0;
	instance.Perst.index = 0;
	instance.Perst.PosSt = 0;
	instance.Perst.MPosRSt = 0;
	instance.Perst.ArmRcp_old = false;
	instance.Perst.ActRcp_old = false;
	instance.Perst.MNewMR_old = false;
	instance.Perst.ArmRcpSt = false;
	instance.Perst.PQWDef = false;
	R_EDGE1_inlined_1.new = false;
	R_EDGE1_inlined_1.old = false;
	R_EDGE1_inlined_1.RET_VAL = false;
	R_EDGE1_inlined_2.new = false;
	R_EDGE1_inlined_2.old = false;
	R_EDGE1_inlined_2.RET_VAL = false;
	R_EDGE1_inlined_3.new = false;
	R_EDGE1_inlined_3.old = false;
	R_EDGE1_inlined_3.RET_VAL = false;
	__assertion_error = 0;
	
	VerificationLoop();
}
