#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Root data structure

// Global variables
bool MX1_1;
bool MX1_2;
bool MX1_3;
bool MX1_4;
bool MX2_1;
bool MX2_2;
bool MX2_3;
bool MX2_4;
bool MX2_5;
bool MX3_1;
bool MX3_2;
bool MX3_3;
bool MX3_4;
bool MX3_5;
bool MX3_6;
bool MX3_7;
bool MX4_1;
bool MX4_2;
bool MX4_3;
bool MX4_4;
bool MX4_5;
bool MX4_6;
bool MX4_7;
bool MX4_0;
bool MX5_1;
bool MX5_2;
bool MX5_3;
bool MX5_4;
bool MX5_5;
bool MX5_6;
bool MX5_7;
uint16_t __assertion_error;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Automata declarations
void ifFunction1();
void ifFunction2();
void ifFunction3();
void ifFunction4();
void ifFunction5();
void ifFunction_OB1();
void VerificationLoop();

// Automata
void ifFunction1() {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			MX1_1 = true;
			goto l1;
		//assert(false);
		return;  			}
	l1: {
		if (MX1_2) {
			goto l2;
		}
		if ((! MX1_2)) {
			goto l4;
		}
		//assert(false);
		return;  			}
	l2: {
			MX1_3 = true;
			goto l3;
		//assert(false);
		return;  			}
	l3: {
			goto l5;
		//assert(false);
		return;  			}
	l4: {
			goto l5;
		//assert(false);
		return;  			}
	l5: {
			MX1_4 = true;
			goto l6;
		//assert(false);
		return;  			}
	l6: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void ifFunction2() {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			MX2_1 = true;
			goto l11;
		//assert(false);
		return;  			}
	l11: {
		if (MX2_2) {
			goto l21;
		}
		if ((! MX2_2)) {
			goto l41;
		}
		//assert(false);
		return;  			}
	l21: {
			MX2_3 = true;
			goto l31;
		//assert(false);
		return;  			}
	l31: {
			goto l61;
		//assert(false);
		return;  			}
	l41: {
			MX2_4 = true;
			goto l51;
		//assert(false);
		return;  			}
	l51: {
			goto l61;
		//assert(false);
		return;  			}
	l61: {
			MX2_5 = true;
			goto l7;
		//assert(false);
		return;  			}
	l7: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void ifFunction3() {
	// Temporary variables
	
	// Start with initial location
	goto init2;
	init2: {
			MX3_1 = true;
			goto l12;
		//assert(false);
		return;  			}
	l12: {
		if (MX3_2) {
			goto l22;
		}
		if (((! MX3_2) && MX3_4)) {
			goto l42;
		}
		if (((! MX3_2) && (! ((! MX3_2) && MX3_4)))) {
			goto l62;
		}
		//assert(false);
		return;  			}
	l22: {
			MX3_3 = true;
			goto l32;
		//assert(false);
		return;  			}
	l32: {
			goto l8;
		//assert(false);
		return;  			}
	l42: {
			MX3_5 = true;
			goto l52;
		//assert(false);
		return;  			}
	l52: {
			goto l8;
		//assert(false);
		return;  			}
	l62: {
			MX3_6 = true;
			goto l71;
		//assert(false);
		return;  			}
	l71: {
			goto l8;
		//assert(false);
		return;  			}
	l8: {
			MX3_7 = true;
			goto l9;
		//assert(false);
		return;  			}
	l9: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void ifFunction4() {
	// Temporary variables
	
	// Start with initial location
	goto init3;
	init3: {
			MX4_1 = true;
			goto l13;
		//assert(false);
		return;  			}
	l13: {
		if (MX4_2) {
			goto l23;
		}
		if (((! MX4_2) && MX4_4)) {
			goto l43;
		}
		if ((((! MX4_2) && (! MX4_4)) && MX4_6)) {
			goto l63;
		}
		if (((! MX4_2) && ((! ((! MX4_2) && MX4_4)) && (! (((! MX4_2) && (! MX4_4)) && MX4_6))))) {
			goto l81;
		}
		//assert(false);
		return;  			}
	l23: {
			MX4_3 = true;
			goto l33;
		//assert(false);
		return;  			}
	l33: {
			goto l10;
		//assert(false);
		return;  			}
	l43: {
			MX4_5 = true;
			goto l53;
		//assert(false);
		return;  			}
	l53: {
			goto l10;
		//assert(false);
		return;  			}
	l63: {
			MX4_7 = true;
			goto l72;
		//assert(false);
		return;  			}
	l72: {
			goto l10;
		//assert(false);
		return;  			}
	l81: {
			MX4_0 = true;
			goto l91;
		//assert(false);
		return;  			}
	l91: {
			goto l10;
		//assert(false);
		return;  			}
	l10: {
			MX4_1 = true;
			goto l111;
		//assert(false);
		return;  			}
	l111: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void ifFunction5() {
	// Temporary variables
	
	// Start with initial location
	goto init4;
	init4: {
			MX5_1 = true;
			goto l14;
		//assert(false);
		return;  			}
	l14: {
		if (MX5_2) {
			goto l24;
		}
		if ((! MX5_2)) {
			goto l44;
		}
		//assert(false);
		return;  			}
	l24: {
			MX5_3 = true;
			goto l34;
		//assert(false);
		return;  			}
	l34: {
			goto l101;
		//assert(false);
		return;  			}
	l44: {
			MX5_4 = true;
			goto l54;
		//assert(false);
		return;  			}
	l54: {
		if (MX5_5) {
			goto l64;
		}
		if ((! MX5_5)) {
			goto l82;
		}
		//assert(false);
		return;  			}
	l64: {
			MX5_6 = true;
			goto l73;
		//assert(false);
		return;  			}
	l73: {
			goto l92;
		//assert(false);
		return;  			}
	l82: {
			goto l92;
		//assert(false);
		return;  			}
	l92: {
			goto l101;
		//assert(false);
		return;  			}
	l101: {
			MX5_7 = true;
			goto l112;
		//assert(false);
		return;  			}
	l112: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void ifFunction_OB1() {
	// Temporary variables
	
	// Start with initial location
	goto init5;
	init5: {
			ifFunction1();
			goto l15;
		//assert(false);
		return;  			}
	l15: {
			ifFunction2();
			goto l25;
		//assert(false);
		return;  			}
	l25: {
			ifFunction3();
			goto l35;
		//assert(false);
		return;  			}
	l35: {
			ifFunction4();
			goto l45;
		//assert(false);
		return;  			}
	l45: {
			ifFunction5();
			goto l55;
		//assert(false);
		return;  			}
	l55: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init6;
	init6: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			ifFunction_OB1();
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
void main() {
	// Initial values
	MX1_1 = false;
	MX1_2 = false;
	MX1_3 = false;
	MX1_4 = false;
	MX2_1 = false;
	MX2_2 = false;
	MX2_3 = false;
	MX2_4 = false;
	MX2_5 = false;
	MX3_1 = false;
	MX3_2 = false;
	MX3_3 = false;
	MX3_4 = false;
	MX3_5 = false;
	MX3_6 = false;
	MX3_7 = false;
	MX4_1 = false;
	MX4_2 = false;
	MX4_3 = false;
	MX4_4 = false;
	MX4_5 = false;
	MX4_6 = false;
	MX4_7 = false;
	MX4_0 = false;
	MX5_1 = false;
	MX5_2 = false;
	MX5_3 = false;
	MX5_4 = false;
	MX5_5 = false;
	MX5_6 = false;
	MX5_7 = false;
	__assertion_error = 0;
	
	VerificationLoop();
}
