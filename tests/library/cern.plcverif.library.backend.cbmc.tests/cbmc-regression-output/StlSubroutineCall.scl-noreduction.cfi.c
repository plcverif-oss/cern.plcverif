#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Variables
bool IX0_0 = false;
bool MX0_0 = false;
bool MX1_0 = false;
bool MX2_0 = false;
bool MX4_0 = false;
int16_t call_OB1_r = 0;
bool call_OB1___RLO = false;
bool call_OB1___NFC = false;
bool call_OB1___BR = false;
bool call_OB1___STA = false;
bool call_OB1___OR = false;
bool call_OB1___CC0 = false;
bool call_OB1___CC1 = false;
bool call_FC1___RLO = false;
bool call_FC1___NFC = false;
bool call_FC1___BR = false;
bool call_FC1___STA = false;
bool call_FC1___OR = false;
bool call_FC1___CC0 = false;
bool call_FC1___CC1 = false;
bool call_FC2_in1 = false;
bool call_FC2___RLO = false;
bool call_FC2___NFC = false;
bool call_FC2___BR = false;
bool call_FC2___STA = false;
bool call_FC2___OR = false;
bool call_FC2___CC0 = false;
bool call_FC2___CC1 = false;
bool call_FC3_in1 = false;
int16_t call_FC3_in2 = 0;
int16_t call_FC3_RET_VAL = 0;
bool call_FC3___RLO = false;
bool call_FC3___NFC = false;
bool call_FC3___BR = false;
bool call_FC3___STA = false;
bool call_FC3___OR = false;
bool call_FC3___CC0 = false;
bool call_FC3___CC1 = false;
bool call_DB4_a = false;
bool call_DB4_q = false;
bool call_DB4_c_b = false;
bool call_DB4_c_w = false;
bool call_DB4_c___RLO = false;
bool call_DB4_c___NFC = false;
bool call_DB4_c___BR = false;
bool call_DB4_c___STA = false;
bool call_DB4_c___OR = false;
bool call_DB4_c___CC0 = false;
bool call_DB4_c___CC1 = false;
bool call_DB4___RLO = false;
bool call_DB4___NFC = false;
bool call_DB4___BR = false;
bool call_DB4___STA = false;
bool call_DB4___OR = false;
bool call_DB4___CC0 = false;
bool call_DB4___CC1 = false;
uint16_t __assertion_error = 0;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			IX0_0 = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			goto verificationLoop_VerificationLoop_init;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_init: {
			call_OB1_r = 0;
			goto verificationLoop_VerificationLoop_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l1: {
			call_OB1___OR = false;
			call_OB1___STA = true;
			call_OB1___RLO = true;
			call_OB1___CC0 = false;
			call_OB1___CC1 = false;
			call_OB1___BR = false;
			call_OB1___NFC = false;
			goto verificationLoop_VerificationLoop_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l2: {
			goto verificationLoop_VerificationLoop_call_OB1_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l3: {
			call_FC2_in1 = true;
			goto verificationLoop_VerificationLoop_call_OB1_init1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l4: {
			call_FC3_in1 = IX0_0;
			call_FC3_in2 = 100;
			goto verificationLoop_VerificationLoop_call_OB1_init2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l5: {
			call_DB4_a = (call_DB4_a || call_OB1___RLO);
			goto verificationLoop_VerificationLoop_l6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l6: {
			call_OB1___OR = false;
			goto verificationLoop_VerificationLoop_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l7: {
			call_OB1___STA = call_DB4_a;
			goto verificationLoop_VerificationLoop_l8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l8: {
			call_OB1___NFC = false;
			goto verificationLoop_VerificationLoop_l9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l9: {
			goto verificationLoop_VerificationLoop_call_OB1_init3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l10: {
			call_OB1___RLO = (((! call_OB1___NFC) || call_OB1___RLO) && (call_DB4_q || call_OB1___OR));
			goto verificationLoop_VerificationLoop_l11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l11: {
			call_OB1___STA = call_DB4_q;
			goto verificationLoop_VerificationLoop_l12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l12: {
			call_OB1___NFC = true;
			goto verificationLoop_VerificationLoop_l13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l13: {
			MX0_0 = call_OB1___RLO;
			goto verificationLoop_VerificationLoop_l14;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l14: {
			call_OB1___OR = false;
			goto verificationLoop_VerificationLoop_l15;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l15: {
			call_OB1___STA = call_OB1___RLO;
			goto verificationLoop_VerificationLoop_l16;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l16: {
			call_OB1___NFC = false;
			goto verificationLoop_VerificationLoop_l17;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l17: {
			goto callEnd;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_init: {
			call_FC1___OR = false;
			call_FC1___STA = true;
			call_FC1___RLO = true;
			call_FC1___CC0 = false;
			call_FC1___CC1 = false;
			call_FC1___BR = false;
			call_FC1___NFC = false;
			goto verificationLoop_VerificationLoop_call_OB1_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l1: {
			MX1_0 = (MX1_0 || call_FC1___RLO);
			goto verificationLoop_VerificationLoop_call_OB1_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l2: {
			call_FC1___OR = false;
			goto verificationLoop_VerificationLoop_call_OB1_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l3: {
			call_FC1___STA = MX1_0;
			goto verificationLoop_VerificationLoop_call_OB1_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l4: {
			call_FC1___NFC = false;
			goto verificationLoop_VerificationLoop_call_OB1_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l5: {
			goto verificationLoop_VerificationLoop_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_init1: {
			call_FC2___OR = false;
			call_FC2___STA = true;
			call_FC2___RLO = true;
			call_FC2___CC0 = false;
			call_FC2___CC1 = false;
			call_FC2___BR = false;
			call_FC2___NFC = false;
			goto verificationLoop_VerificationLoop_call_OB1_l11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l11: {
			call_FC2___RLO = (((! call_FC2___NFC) || call_FC2___RLO) && (call_FC2_in1 || call_FC2___OR));
			goto verificationLoop_VerificationLoop_call_OB1_l21;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l21: {
			call_FC2___STA = call_FC2_in1;
			goto verificationLoop_VerificationLoop_call_OB1_l31;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l31: {
			call_FC2___NFC = true;
			goto verificationLoop_VerificationLoop_call_OB1_l41;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l41: {
			call_FC2___RLO = (! call_FC2___RLO);
			goto verificationLoop_VerificationLoop_call_OB1_l51;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l51: {
			MX2_0 = call_FC2___RLO;
			goto verificationLoop_VerificationLoop_call_OB1_l6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l6: {
			call_FC2___OR = false;
			goto verificationLoop_VerificationLoop_call_OB1_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l7: {
			call_FC2___STA = call_FC2___RLO;
			goto verificationLoop_VerificationLoop_call_OB1_l8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l8: {
			call_FC2___NFC = false;
			goto verificationLoop_VerificationLoop_call_OB1_l9;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l9: {
			goto verificationLoop_VerificationLoop_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_init2: {
			call_FC3___OR = false;
			call_FC3___STA = true;
			call_FC3___RLO = true;
			call_FC3___CC0 = false;
			call_FC3___CC1 = false;
			call_FC3___BR = false;
			call_FC3___NFC = false;
			goto verificationLoop_VerificationLoop_call_OB1_l12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l12: {
			call_OB1_r = call_FC3_RET_VAL;
			goto verificationLoop_VerificationLoop_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_init3: {
			call_DB4___OR = false;
			call_DB4___STA = true;
			call_DB4___RLO = true;
			call_DB4___CC0 = false;
			call_DB4___CC1 = false;
			call_DB4___BR = false;
			call_DB4___NFC = false;
			goto verificationLoop_VerificationLoop_call_OB1_l13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l13: {
			call_DB4___RLO = (((! call_DB4___NFC) || call_DB4___RLO) && (call_DB4_a || call_DB4___OR));
			goto verificationLoop_VerificationLoop_call_OB1_l22;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l22: {
			call_DB4___STA = call_DB4_a;
			goto verificationLoop_VerificationLoop_call_OB1_l32;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l32: {
			call_DB4___NFC = true;
			goto verificationLoop_VerificationLoop_call_OB1_l42;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l42: {
			call_DB4___RLO = (! call_DB4___RLO);
			goto verificationLoop_VerificationLoop_call_OB1_l52;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l52: {
			call_DB4_q = call_DB4___RLO;
			goto verificationLoop_VerificationLoop_call_OB1_l61;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l61: {
			call_DB4___OR = false;
			goto verificationLoop_VerificationLoop_call_OB1_l71;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l71: {
			call_DB4___STA = call_DB4___RLO;
			goto verificationLoop_VerificationLoop_call_OB1_l81;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l81: {
			call_DB4___NFC = false;
			goto verificationLoop_VerificationLoop_call_OB1_l91;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l91: {
			call_DB4___RLO = (((! call_DB4___NFC) || call_DB4___RLO) && (call_DB4_a || call_DB4___OR));
			goto verificationLoop_VerificationLoop_call_OB1_l10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l10: {
			call_DB4___STA = call_DB4_a;
			goto verificationLoop_VerificationLoop_call_OB1_l111;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l111: {
			call_DB4___NFC = true;
			goto verificationLoop_VerificationLoop_call_OB1_l121;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l121: {
			call_DB4_c_b = call_DB4___RLO;
			goto verificationLoop_VerificationLoop_call_OB1_l131;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l131: {
			call_DB4___OR = false;
			goto verificationLoop_VerificationLoop_call_OB1_l14;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l14: {
			call_DB4___STA = call_DB4___RLO;
			goto verificationLoop_VerificationLoop_call_OB1_l15;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l15: {
			call_DB4___NFC = false;
			goto verificationLoop_VerificationLoop_call_OB1_l16;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l16: {
			goto verificationLoop_VerificationLoop_call_OB1_call_DB4_call_FB4_init;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l17: {
			call_DB4___RLO = (((! call_DB4___NFC) || call_DB4___RLO) && (call_DB4_c_w || call_DB4___OR));
			goto verificationLoop_VerificationLoop_call_OB1_l18;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l18: {
			call_DB4___STA = call_DB4_c_w;
			goto verificationLoop_VerificationLoop_call_OB1_l19;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l19: {
			call_DB4___NFC = true;
			goto verificationLoop_VerificationLoop_call_OB1_l20;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l20: {
			MX4_0 = call_DB4___RLO;
			goto verificationLoop_VerificationLoop_call_OB1_l211;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l211: {
			call_DB4___OR = false;
			goto verificationLoop_VerificationLoop_call_OB1_l221;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l221: {
			call_DB4___STA = call_DB4___RLO;
			goto verificationLoop_VerificationLoop_call_OB1_l23;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l23: {
			call_DB4___NFC = false;
			goto verificationLoop_VerificationLoop_call_OB1_l24;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_l24: {
			goto verificationLoop_VerificationLoop_l10;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_call_DB4_call_FB4_init: {
			call_DB4_c___OR = false;
			call_DB4_c___STA = true;
			call_DB4_c___RLO = true;
			call_DB4_c___CC0 = false;
			call_DB4_c___CC1 = false;
			call_DB4_c___BR = false;
			call_DB4_c___NFC = false;
			goto verificationLoop_VerificationLoop_call_OB1_call_DB4_call_FB4_l1;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_call_DB4_call_FB4_l1: {
			call_DB4_c___RLO = (((! call_DB4_c___NFC) || call_DB4_c___RLO) && (call_DB4_c_b || call_DB4_c___OR));
			goto verificationLoop_VerificationLoop_call_OB1_call_DB4_call_FB4_l2;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_call_DB4_call_FB4_l2: {
			call_DB4_c___STA = call_DB4_c_b;
			goto verificationLoop_VerificationLoop_call_OB1_call_DB4_call_FB4_l3;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_call_DB4_call_FB4_l3: {
			call_DB4_c___NFC = true;
			goto verificationLoop_VerificationLoop_call_OB1_call_DB4_call_FB4_l4;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_call_DB4_call_FB4_l4: {
			call_DB4_c_w = call_DB4_c___RLO;
			goto verificationLoop_VerificationLoop_call_OB1_call_DB4_call_FB4_l5;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_call_DB4_call_FB4_l5: {
			call_DB4_c___OR = false;
			goto verificationLoop_VerificationLoop_call_OB1_call_DB4_call_FB4_l6;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_call_DB4_call_FB4_l6: {
			call_DB4_c___STA = call_DB4_c___RLO;
			goto verificationLoop_VerificationLoop_call_OB1_call_DB4_call_FB4_l7;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_call_DB4_call_FB4_l7: {
			call_DB4_c___NFC = false;
			goto verificationLoop_VerificationLoop_call_OB1_call_DB4_call_FB4_l8;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_call_OB1_call_DB4_call_FB4_l8: {
			goto verificationLoop_VerificationLoop_call_OB1_l17;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
