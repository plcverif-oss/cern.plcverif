#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Root data structure
typedef struct {
	bool in1;
	bool in2;
	bool in3;
	bool out;
	bool __RLO;
	bool __NFC;
	bool __BR;
	bool __STA;
	bool __OR;
	bool __CC0;
	bool __CC1;
	bool __RLO_1;
	bool __RLO_2;
	bool __RLO_3;
	bool __RLO_4;
	bool __RLO_5;
	bool __RLO_6;
	bool __RLO_7;
	bool __BR_1;
	bool __BR_2;
	bool __BR_3;
	bool __BR_4;
	bool __BR_5;
	bool __BR_6;
	bool __BR_7;
	bool __OR_1;
	bool __OR_2;
	bool __OR_3;
	bool __OR_4;
	bool __OR_5;
	bool __OR_6;
	bool __OR_7;
	bool __FC0_1;
	bool __FC0_2;
	bool __FC0_3;
	bool __FC0_4;
	bool __FC0_5;
	bool __FC0_6;
	bool __FC0_7;
	bool __FC1_1;
	bool __FC1_2;
	bool __FC1_3;
	bool __FC1_4;
	bool __FC1_5;
	bool __FC1_6;
	bool __FC1_7;
	bool __FC2_1;
	bool __FC2_2;
	bool __FC2_3;
	bool __FC2_4;
	bool __FC2_5;
	bool __FC2_6;
	bool __FC2_7;
} __nestingStack_fb1;

// Global variables
__nestingStack_fb1 instance;
uint16_t __assertion_error;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Automata declarations
void nestingStack_fb1(__nestingStack_fb1 *__context);
void VerificationLoop();

// Automata
void nestingStack_fb1(__nestingStack_fb1 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			__context->__OR = false;
			__context->__STA = true;
			__context->__RLO = true;
			__context->__CC0 = false;
			__context->__CC1 = false;
			__context->__BR = false;
			__context->__NFC = false;
			goto l1;
		//assert(false);
		return;  			}
	l1: {
			__context->__RLO_7 = __context->__RLO_6;
			__context->__BR_7 = __context->__BR_6;
			__context->__OR_7 = __context->__OR_6;
			__context->__FC0_7 = __context->__FC0_6;
			__context->__FC1_7 = __context->__FC1_6;
			__context->__FC2_7 = __context->__FC2_6;
			goto l2;
		//assert(false);
		return;  			}
	l2: {
			__context->__RLO_6 = __context->__RLO_5;
			__context->__BR_6 = __context->__BR_5;
			__context->__OR_6 = __context->__OR_5;
			__context->__FC0_6 = __context->__FC0_5;
			__context->__FC1_6 = __context->__FC1_5;
			__context->__FC2_6 = __context->__FC2_5;
			goto l3;
		//assert(false);
		return;  			}
	l3: {
			__context->__RLO_5 = __context->__RLO_4;
			__context->__BR_5 = __context->__BR_4;
			__context->__OR_5 = __context->__OR_4;
			__context->__FC0_5 = __context->__FC0_4;
			__context->__FC1_5 = __context->__FC1_4;
			__context->__FC2_5 = __context->__FC2_4;
			goto l4;
		//assert(false);
		return;  			}
	l4: {
			__context->__RLO_4 = __context->__RLO_3;
			__context->__BR_4 = __context->__BR_3;
			__context->__OR_4 = __context->__OR_3;
			__context->__FC0_4 = __context->__FC0_3;
			__context->__FC1_4 = __context->__FC1_3;
			__context->__FC2_4 = __context->__FC2_3;
			goto l5;
		//assert(false);
		return;  			}
	l5: {
			__context->__RLO_3 = __context->__RLO_2;
			__context->__BR_3 = __context->__BR_2;
			__context->__OR_3 = __context->__OR_2;
			__context->__FC0_3 = __context->__FC0_2;
			__context->__FC1_3 = __context->__FC1_2;
			__context->__FC2_3 = __context->__FC2_2;
			goto l6;
		//assert(false);
		return;  			}
	l6: {
			__context->__RLO_2 = __context->__RLO_1;
			__context->__BR_2 = __context->__BR_1;
			__context->__OR_2 = __context->__OR_1;
			__context->__FC0_2 = __context->__FC0_1;
			__context->__FC1_2 = __context->__FC1_1;
			__context->__FC2_2 = __context->__FC2_1;
			goto l7;
		//assert(false);
		return;  			}
	l7: {
			__context->__RLO_1 = (__context->__RLO && __context->__NFC);
			__context->__OR_1 = false;
			__context->__BR_1 = __context->__BR;
			__context->__FC0_1 = false;
			__context->__FC1_1 = true;
			__context->__FC2_1 = false;
			goto l8;
		//assert(false);
		return;  			}
	l8: {
			__context->__OR = false;
			goto l9;
		//assert(false);
		return;  			}
	l9: {
			__context->__STA = __context->__RLO;
			goto l10;
		//assert(false);
		return;  			}
	l10: {
			__context->__NFC = false;
			goto l11;
		//assert(false);
		return;  			}
	l11: {
			__context->__RLO = (((! __context->__NFC) || __context->__RLO) && (__context->in1 || __context->__OR));
			goto l12;
		//assert(false);
		return;  			}
	l12: {
			__context->__STA = __context->in1;
			goto l13;
		//assert(false);
		return;  			}
	l13: {
			__context->__NFC = true;
			goto l14;
		//assert(false);
		return;  			}
	l14: {
			__context->__OR = __context->__OR_1;
			goto l15;
		//assert(false);
		return;  			}
	l15: {
			__context->__NFC = true;
			goto l16;
		//assert(false);
		return;  			}
	l16: {
			__context->__STA = true;
			goto l17;
		//assert(false);
		return;  			}
	l17: {
			__context->__BR = __context->__BR_1;
			goto l18;
		//assert(false);
		return;  			}
	l18: {
		if (((__context->__FC0_1 == false) && ((__context->__FC1_1 == false) && (__context->__FC2_1 == false)))) {
			goto l19;
		}
		if (((__context->__FC0_1 == true) && ((__context->__FC1_1 == false) && (__context->__FC2_1 == false)))) {
			goto l19;
		}
		if (((__context->__FC0_1 == false) && ((__context->__FC1_1 == true) && (__context->__FC2_1 == false)))) {
			goto l19;
		}
		if (((__context->__FC0_1 == true) && ((__context->__FC1_1 == true) && (__context->__FC2_1 == false)))) {
			goto l19;
		}
		if (((__context->__FC0_1 == false) && ((__context->__FC1_1 == false) && (__context->__FC2_1 == true)))) {
			goto l19;
		}
		if (((__context->__FC0_1 == false) && ((__context->__FC1_1 == false) && (__context->__FC2_1 == true)))) {
			goto l19;
		}
		//assert(false);
		return;  			}
	l19: {
			__context->__RLO_1 = __context->__RLO_2;
			__context->__BR_1 = __context->__BR_2;
			__context->__OR_1 = __context->__OR_2;
			__context->__FC0_1 = __context->__FC0_2;
			__context->__FC1_1 = __context->__FC1_2;
			__context->__FC2_1 = __context->__FC2_2;
			goto l20;
		//assert(false);
		return;  			}
	l20: {
			__context->__RLO_2 = __context->__RLO_3;
			__context->__BR_2 = __context->__BR_3;
			__context->__OR_2 = __context->__OR_3;
			__context->__FC0_2 = __context->__FC0_3;
			__context->__FC1_2 = __context->__FC1_3;
			__context->__FC2_2 = __context->__FC2_3;
			goto l21;
		//assert(false);
		return;  			}
	l21: {
			__context->__RLO_3 = __context->__RLO_4;
			__context->__BR_3 = __context->__BR_4;
			__context->__OR_3 = __context->__OR_4;
			__context->__FC0_3 = __context->__FC0_4;
			__context->__FC1_3 = __context->__FC1_4;
			__context->__FC2_3 = __context->__FC2_4;
			goto l22;
		//assert(false);
		return;  			}
	l22: {
			__context->__RLO_4 = __context->__RLO_5;
			__context->__BR_4 = __context->__BR_5;
			__context->__OR_4 = __context->__OR_5;
			__context->__FC0_4 = __context->__FC0_5;
			__context->__FC1_4 = __context->__FC1_5;
			__context->__FC2_4 = __context->__FC2_5;
			goto l23;
		//assert(false);
		return;  			}
	l23: {
			__context->__RLO_5 = __context->__RLO_6;
			__context->__BR_5 = __context->__BR_6;
			__context->__OR_5 = __context->__OR_6;
			__context->__FC0_5 = __context->__FC0_6;
			__context->__FC1_5 = __context->__FC1_6;
			__context->__FC2_5 = __context->__FC2_6;
			goto l24;
		//assert(false);
		return;  			}
	l24: {
			__context->__RLO_6 = __context->__RLO_7;
			__context->__BR_6 = __context->__BR_7;
			__context->__OR_6 = __context->__OR_7;
			__context->__FC0_6 = __context->__FC0_7;
			__context->__FC1_6 = __context->__FC1_7;
			__context->__FC2_6 = __context->__FC2_7;
			goto l25;
		//assert(false);
		return;  			}
	l25: {
			__context->__RLO_7 = false;
			__context->__BR_7 = false;
			__context->__OR_7 = false;
			__context->__FC0_7 = false;
			__context->__FC1_7 = false;
			__context->__FC2_7 = false;
			goto l26;
		//assert(false);
		return;  			}
	l26: {
			__context->__RLO_7 = __context->__RLO_6;
			__context->__BR_7 = __context->__BR_6;
			__context->__OR_7 = __context->__OR_6;
			__context->__FC0_7 = __context->__FC0_6;
			__context->__FC1_7 = __context->__FC1_6;
			__context->__FC2_7 = __context->__FC2_6;
			goto l27;
		//assert(false);
		return;  			}
	l27: {
			__context->__RLO_6 = __context->__RLO_5;
			__context->__BR_6 = __context->__BR_5;
			__context->__OR_6 = __context->__OR_5;
			__context->__FC0_6 = __context->__FC0_5;
			__context->__FC1_6 = __context->__FC1_5;
			__context->__FC2_6 = __context->__FC2_5;
			goto l28;
		//assert(false);
		return;  			}
	l28: {
			__context->__RLO_5 = __context->__RLO_4;
			__context->__BR_5 = __context->__BR_4;
			__context->__OR_5 = __context->__OR_4;
			__context->__FC0_5 = __context->__FC0_4;
			__context->__FC1_5 = __context->__FC1_4;
			__context->__FC2_5 = __context->__FC2_4;
			goto l29;
		//assert(false);
		return;  			}
	l29: {
			__context->__RLO_4 = __context->__RLO_3;
			__context->__BR_4 = __context->__BR_3;
			__context->__OR_4 = __context->__OR_3;
			__context->__FC0_4 = __context->__FC0_3;
			__context->__FC1_4 = __context->__FC1_3;
			__context->__FC2_4 = __context->__FC2_3;
			goto l30;
		//assert(false);
		return;  			}
	l30: {
			__context->__RLO_3 = __context->__RLO_2;
			__context->__BR_3 = __context->__BR_2;
			__context->__OR_3 = __context->__OR_2;
			__context->__FC0_3 = __context->__FC0_2;
			__context->__FC1_3 = __context->__FC1_2;
			__context->__FC2_3 = __context->__FC2_2;
			goto l31;
		//assert(false);
		return;  			}
	l31: {
			__context->__RLO_2 = __context->__RLO_1;
			__context->__BR_2 = __context->__BR_1;
			__context->__OR_2 = __context->__OR_1;
			__context->__FC0_2 = __context->__FC0_1;
			__context->__FC1_2 = __context->__FC1_1;
			__context->__FC2_2 = __context->__FC2_1;
			goto l32;
		//assert(false);
		return;  			}
	l32: {
			__context->__RLO_1 = (__context->__RLO && __context->__NFC);
			__context->__OR_1 = false;
			__context->__BR_1 = __context->__BR;
			__context->__FC0_1 = false;
			__context->__FC1_1 = true;
			__context->__FC2_1 = false;
			goto l33;
		//assert(false);
		return;  			}
	l33: {
			__context->__OR = false;
			goto l34;
		//assert(false);
		return;  			}
	l34: {
			__context->__STA = __context->__RLO;
			goto l35;
		//assert(false);
		return;  			}
	l35: {
			__context->__NFC = false;
			goto l36;
		//assert(false);
		return;  			}
	l36: {
			__context->__RLO = (((! __context->__NFC) || __context->__RLO) && (__context->in2 || __context->__OR));
			goto l37;
		//assert(false);
		return;  			}
	l37: {
			__context->__STA = __context->in2;
			goto l38;
		//assert(false);
		return;  			}
	l38: {
			__context->__NFC = true;
			goto l39;
		//assert(false);
		return;  			}
	l39: {
			__context->__OR = __context->__OR_1;
			goto l40;
		//assert(false);
		return;  			}
	l40: {
			__context->__NFC = true;
			goto l41;
		//assert(false);
		return;  			}
	l41: {
			__context->__STA = true;
			goto l42;
		//assert(false);
		return;  			}
	l42: {
			__context->__BR = __context->__BR_1;
			goto l43;
		//assert(false);
		return;  			}
	l43: {
		if (((__context->__FC0_1 == false) && ((__context->__FC1_1 == false) && (__context->__FC2_1 == false)))) {
			goto l44;
		}
		if (((__context->__FC0_1 == true) && ((__context->__FC1_1 == false) && (__context->__FC2_1 == false)))) {
			goto l44;
		}
		if (((__context->__FC0_1 == false) && ((__context->__FC1_1 == true) && (__context->__FC2_1 == false)))) {
			goto l44;
		}
		if (((__context->__FC0_1 == true) && ((__context->__FC1_1 == true) && (__context->__FC2_1 == false)))) {
			goto l44;
		}
		if (((__context->__FC0_1 == false) && ((__context->__FC1_1 == false) && (__context->__FC2_1 == true)))) {
			goto l44;
		}
		if (((__context->__FC0_1 == false) && ((__context->__FC1_1 == false) && (__context->__FC2_1 == true)))) {
			goto l44;
		}
		//assert(false);
		return;  			}
	l44: {
			__context->__RLO_1 = __context->__RLO_2;
			__context->__BR_1 = __context->__BR_2;
			__context->__OR_1 = __context->__OR_2;
			__context->__FC0_1 = __context->__FC0_2;
			__context->__FC1_1 = __context->__FC1_2;
			__context->__FC2_1 = __context->__FC2_2;
			goto l45;
		//assert(false);
		return;  			}
	l45: {
			__context->__RLO_2 = __context->__RLO_3;
			__context->__BR_2 = __context->__BR_3;
			__context->__OR_2 = __context->__OR_3;
			__context->__FC0_2 = __context->__FC0_3;
			__context->__FC1_2 = __context->__FC1_3;
			__context->__FC2_2 = __context->__FC2_3;
			goto l46;
		//assert(false);
		return;  			}
	l46: {
			__context->__RLO_3 = __context->__RLO_4;
			__context->__BR_3 = __context->__BR_4;
			__context->__OR_3 = __context->__OR_4;
			__context->__FC0_3 = __context->__FC0_4;
			__context->__FC1_3 = __context->__FC1_4;
			__context->__FC2_3 = __context->__FC2_4;
			goto l47;
		//assert(false);
		return;  			}
	l47: {
			__context->__RLO_4 = __context->__RLO_5;
			__context->__BR_4 = __context->__BR_5;
			__context->__OR_4 = __context->__OR_5;
			__context->__FC0_4 = __context->__FC0_5;
			__context->__FC1_4 = __context->__FC1_5;
			__context->__FC2_4 = __context->__FC2_5;
			goto l48;
		//assert(false);
		return;  			}
	l48: {
			__context->__RLO_5 = __context->__RLO_6;
			__context->__BR_5 = __context->__BR_6;
			__context->__OR_5 = __context->__OR_6;
			__context->__FC0_5 = __context->__FC0_6;
			__context->__FC1_5 = __context->__FC1_6;
			__context->__FC2_5 = __context->__FC2_6;
			goto l49;
		//assert(false);
		return;  			}
	l49: {
			__context->__RLO_6 = __context->__RLO_7;
			__context->__BR_6 = __context->__BR_7;
			__context->__OR_6 = __context->__OR_7;
			__context->__FC0_6 = __context->__FC0_7;
			__context->__FC1_6 = __context->__FC1_7;
			__context->__FC2_6 = __context->__FC2_7;
			goto l50;
		//assert(false);
		return;  			}
	l50: {
			__context->__RLO_7 = false;
			__context->__BR_7 = false;
			__context->__OR_7 = false;
			__context->__FC0_7 = false;
			__context->__FC1_7 = false;
			__context->__FC2_7 = false;
			goto l51;
		//assert(false);
		return;  			}
	l51: {
			__context->__RLO_7 = __context->__RLO_6;
			__context->__BR_7 = __context->__BR_6;
			__context->__OR_7 = __context->__OR_6;
			__context->__FC0_7 = __context->__FC0_6;
			__context->__FC1_7 = __context->__FC1_6;
			__context->__FC2_7 = __context->__FC2_6;
			goto l52;
		//assert(false);
		return;  			}
	l52: {
			__context->__RLO_6 = __context->__RLO_5;
			__context->__BR_6 = __context->__BR_5;
			__context->__OR_6 = __context->__OR_5;
			__context->__FC0_6 = __context->__FC0_5;
			__context->__FC1_6 = __context->__FC1_5;
			__context->__FC2_6 = __context->__FC2_5;
			goto l53;
		//assert(false);
		return;  			}
	l53: {
			__context->__RLO_5 = __context->__RLO_4;
			__context->__BR_5 = __context->__BR_4;
			__context->__OR_5 = __context->__OR_4;
			__context->__FC0_5 = __context->__FC0_4;
			__context->__FC1_5 = __context->__FC1_4;
			__context->__FC2_5 = __context->__FC2_4;
			goto l54;
		//assert(false);
		return;  			}
	l54: {
			__context->__RLO_4 = __context->__RLO_3;
			__context->__BR_4 = __context->__BR_3;
			__context->__OR_4 = __context->__OR_3;
			__context->__FC0_4 = __context->__FC0_3;
			__context->__FC1_4 = __context->__FC1_3;
			__context->__FC2_4 = __context->__FC2_3;
			goto l55;
		//assert(false);
		return;  			}
	l55: {
			__context->__RLO_3 = __context->__RLO_2;
			__context->__BR_3 = __context->__BR_2;
			__context->__OR_3 = __context->__OR_2;
			__context->__FC0_3 = __context->__FC0_2;
			__context->__FC1_3 = __context->__FC1_2;
			__context->__FC2_3 = __context->__FC2_2;
			goto l56;
		//assert(false);
		return;  			}
	l56: {
			__context->__RLO_2 = __context->__RLO_1;
			__context->__BR_2 = __context->__BR_1;
			__context->__OR_2 = __context->__OR_1;
			__context->__FC0_2 = __context->__FC0_1;
			__context->__FC1_2 = __context->__FC1_1;
			__context->__FC2_2 = __context->__FC2_1;
			goto l57;
		//assert(false);
		return;  			}
	l57: {
			__context->__RLO_1 = (__context->__RLO && __context->__NFC);
			__context->__OR_1 = false;
			__context->__BR_1 = __context->__BR;
			__context->__FC0_1 = false;
			__context->__FC1_1 = true;
			__context->__FC2_1 = false;
			goto l58;
		//assert(false);
		return;  			}
	l58: {
			__context->__OR = false;
			goto l59;
		//assert(false);
		return;  			}
	l59: {
			__context->__STA = __context->__RLO;
			goto l60;
		//assert(false);
		return;  			}
	l60: {
			__context->__NFC = false;
			goto l61;
		//assert(false);
		return;  			}
	l61: {
			__context->__RLO = (((! __context->__NFC) || __context->__RLO) && (__context->in3 || __context->__OR));
			goto l62;
		//assert(false);
		return;  			}
	l62: {
			__context->__STA = __context->in3;
			goto l63;
		//assert(false);
		return;  			}
	l63: {
			__context->__NFC = true;
			goto l64;
		//assert(false);
		return;  			}
	l64: {
			__context->__OR = __context->__OR_1;
			goto l65;
		//assert(false);
		return;  			}
	l65: {
			__context->__NFC = true;
			goto l66;
		//assert(false);
		return;  			}
	l66: {
			__context->__STA = true;
			goto l67;
		//assert(false);
		return;  			}
	l67: {
			__context->__BR = __context->__BR_1;
			goto l68;
		//assert(false);
		return;  			}
	l68: {
		if (((__context->__FC0_1 == false) && ((__context->__FC1_1 == false) && (__context->__FC2_1 == false)))) {
			goto l69;
		}
		if (((__context->__FC0_1 == true) && ((__context->__FC1_1 == false) && (__context->__FC2_1 == false)))) {
			goto l69;
		}
		if (((__context->__FC0_1 == false) && ((__context->__FC1_1 == true) && (__context->__FC2_1 == false)))) {
			goto l69;
		}
		if (((__context->__FC0_1 == true) && ((__context->__FC1_1 == true) && (__context->__FC2_1 == false)))) {
			goto l69;
		}
		if (((__context->__FC0_1 == false) && ((__context->__FC1_1 == false) && (__context->__FC2_1 == true)))) {
			goto l69;
		}
		if (((__context->__FC0_1 == false) && ((__context->__FC1_1 == false) && (__context->__FC2_1 == true)))) {
			goto l69;
		}
		//assert(false);
		return;  			}
	l69: {
			__context->__RLO_1 = __context->__RLO_2;
			__context->__BR_1 = __context->__BR_2;
			__context->__OR_1 = __context->__OR_2;
			__context->__FC0_1 = __context->__FC0_2;
			__context->__FC1_1 = __context->__FC1_2;
			__context->__FC2_1 = __context->__FC2_2;
			goto l70;
		//assert(false);
		return;  			}
	l70: {
			__context->__RLO_2 = __context->__RLO_3;
			__context->__BR_2 = __context->__BR_3;
			__context->__OR_2 = __context->__OR_3;
			__context->__FC0_2 = __context->__FC0_3;
			__context->__FC1_2 = __context->__FC1_3;
			__context->__FC2_2 = __context->__FC2_3;
			goto l71;
		//assert(false);
		return;  			}
	l71: {
			__context->__RLO_3 = __context->__RLO_4;
			__context->__BR_3 = __context->__BR_4;
			__context->__OR_3 = __context->__OR_4;
			__context->__FC0_3 = __context->__FC0_4;
			__context->__FC1_3 = __context->__FC1_4;
			__context->__FC2_3 = __context->__FC2_4;
			goto l72;
		//assert(false);
		return;  			}
	l72: {
			__context->__RLO_4 = __context->__RLO_5;
			__context->__BR_4 = __context->__BR_5;
			__context->__OR_4 = __context->__OR_5;
			__context->__FC0_4 = __context->__FC0_5;
			__context->__FC1_4 = __context->__FC1_5;
			__context->__FC2_4 = __context->__FC2_5;
			goto l73;
		//assert(false);
		return;  			}
	l73: {
			__context->__RLO_5 = __context->__RLO_6;
			__context->__BR_5 = __context->__BR_6;
			__context->__OR_5 = __context->__OR_6;
			__context->__FC0_5 = __context->__FC0_6;
			__context->__FC1_5 = __context->__FC1_6;
			__context->__FC2_5 = __context->__FC2_6;
			goto l74;
		//assert(false);
		return;  			}
	l74: {
			__context->__RLO_6 = __context->__RLO_7;
			__context->__BR_6 = __context->__BR_7;
			__context->__OR_6 = __context->__OR_7;
			__context->__FC0_6 = __context->__FC0_7;
			__context->__FC1_6 = __context->__FC1_7;
			__context->__FC2_6 = __context->__FC2_7;
			goto l75;
		//assert(false);
		return;  			}
	l75: {
			__context->__RLO_7 = false;
			__context->__BR_7 = false;
			__context->__OR_7 = false;
			__context->__FC0_7 = false;
			__context->__FC1_7 = false;
			__context->__FC2_7 = false;
			goto l76;
		//assert(false);
		return;  			}
	l76: {
			__context->out = __context->__RLO;
			goto l77;
		//assert(false);
		return;  			}
	l77: {
			__context->__OR = false;
			goto l78;
		//assert(false);
		return;  			}
	l78: {
			__context->__STA = __context->__RLO;
			goto l79;
		//assert(false);
		return;  			}
	l79: {
			__context->__NFC = false;
			goto l80;
		//assert(false);
		return;  			}
	l80: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			instance.in1 = nondet_bool();
			instance.in2 = nondet_bool();
			instance.in3 = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			nestingStack_fb1(&instance);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
void main() {
	// Initial values
	instance.in1 = false;
	instance.in2 = false;
	instance.in3 = false;
	instance.out = false;
	instance.__RLO = false;
	instance.__NFC = false;
	instance.__BR = false;
	instance.__STA = false;
	instance.__OR = false;
	instance.__CC0 = false;
	instance.__CC1 = false;
	instance.__RLO_1 = false;
	instance.__RLO_2 = false;
	instance.__RLO_3 = false;
	instance.__RLO_4 = false;
	instance.__RLO_5 = false;
	instance.__RLO_6 = false;
	instance.__RLO_7 = false;
	instance.__BR_1 = false;
	instance.__BR_2 = false;
	instance.__BR_3 = false;
	instance.__BR_4 = false;
	instance.__BR_5 = false;
	instance.__BR_6 = false;
	instance.__BR_7 = false;
	instance.__OR_1 = false;
	instance.__OR_2 = false;
	instance.__OR_3 = false;
	instance.__OR_4 = false;
	instance.__OR_5 = false;
	instance.__OR_6 = false;
	instance.__OR_7 = false;
	instance.__FC0_1 = false;
	instance.__FC0_2 = false;
	instance.__FC0_3 = false;
	instance.__FC0_4 = false;
	instance.__FC0_5 = false;
	instance.__FC0_6 = false;
	instance.__FC0_7 = false;
	instance.__FC1_1 = false;
	instance.__FC1_2 = false;
	instance.__FC1_3 = false;
	instance.__FC1_4 = false;
	instance.__FC1_5 = false;
	instance.__FC1_6 = false;
	instance.__FC1_7 = false;
	instance.__FC2_1 = false;
	instance.__FC2_2 = false;
	instance.__FC2_3 = false;
	instance.__FC2_4 = false;
	instance.__FC2_5 = false;
	instance.__FC2_6 = false;
	instance.__FC2_7 = false;
	__assertion_error = 0;
	
	VerificationLoop();
}
