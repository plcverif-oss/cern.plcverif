#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Root data structure

// Global variables
bool MX0_1;
bool MX0_2;
bool MX0_3;
bool MX0_4;
bool MX0_5;
bool MX1_0;
bool MX1_1;
bool MX1_2;
bool MX1_3;
bool MX1_4;
bool MX1_5;
bool MX1_6;
bool MX2_0;
bool MX2_1;
bool MX2_2;
bool MX2_3;
bool MX2_4;
bool MX2_5;
bool MX2_6;
uint16_t __assertion_error;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Automata declarations
void whileFunction1();
void whileFunction1_exit();
void whileFunction1_continue();
void whileFunction_OB1();
void VerificationLoop();

// Automata
void whileFunction1() {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			MX0_1 = true;
			goto l1;
		//assert(false);
		return;  			}
	l1: {
		if (MX0_2) {
			MX0_3 = true;
			MX0_4 = true;
			goto l1;
		}
		if ((! MX0_2)) {
			MX0_5 = true;
			goto l6;
		}
		//assert(false);
		return;  			}
	l6: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void whileFunction1_exit() {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			MX1_0 = true;
			goto l11;
		//assert(false);
		return;  			}
	l11: {
		if (MX1_1) {
			MX1_2 = true;
			goto l3;
		}
		if ((! MX1_1)) {
			goto l10;
		}
		//assert(false);
		return;  			}
	l3: {
		if (MX1_3) {
			MX1_4 = true;
			goto l10;
		}
		if ((! MX1_3)) {
			MX1_5 = true;
			goto l11;
		}
		//assert(false);
		return;  			}
	l10: {
			MX1_6 = true;
			goto l111;
		//assert(false);
		return;  			}
	l111: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void whileFunction1_continue() {
	// Temporary variables
	
	// Start with initial location
	goto init2;
	init2: {
			MX2_0 = true;
			goto l12;
		//assert(false);
		return;  			}
	l12: {
		if (MX2_1) {
			MX2_2 = true;
			goto l31;
		}
		if ((! MX2_1)) {
			MX2_6 = true;
			goto l112;
		}
		//assert(false);
		return;  			}
	l31: {
		if (MX2_3) {
			MX2_4 = true;
			goto l12;
		}
		if ((! MX2_3)) {
			MX2_5 = true;
			goto l12;
		}
		//assert(false);
		return;  			}
	l112: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void whileFunction_OB1() {
	// Temporary variables
	
	// Start with initial location
	goto init3;
	init3: {
			whileFunction1();
			goto l13;
		//assert(false);
		return;  			}
	l13: {
			whileFunction1_exit();
			goto l2;
		//assert(false);
		return;  			}
	l2: {
			whileFunction1_continue();
			goto l32;
		//assert(false);
		return;  			}
	l32: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init4;
	init4: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			whileFunction_OB1();
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
void main() {
	// Initial values
	MX0_1 = false;
	MX0_2 = false;
	MX0_3 = false;
	MX0_4 = false;
	MX0_5 = false;
	MX1_0 = false;
	MX1_1 = false;
	MX1_2 = false;
	MX1_3 = false;
	MX1_4 = false;
	MX1_5 = false;
	MX1_6 = false;
	MX2_0 = false;
	MX2_1 = false;
	MX2_2 = false;
	MX2_3 = false;
	MX2_4 = false;
	MX2_5 = false;
	MX2_6 = false;
	__assertion_error = 0;
	
	VerificationLoop();
}
