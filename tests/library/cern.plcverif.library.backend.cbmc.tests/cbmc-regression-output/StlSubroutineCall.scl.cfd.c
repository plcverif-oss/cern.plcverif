#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Root data structure
typedef struct {
	bool b;
	bool w;
	bool __RLO;
	bool __NFC;
	bool __BR;
	bool __STA;
	bool __OR;
	bool __CC0;
	bool __CC1;
} __call_FB5;
typedef struct {
	bool a;
	bool q;
	__call_FB5 c;
	bool __RLO;
	bool __NFC;
	bool __BR;
	bool __STA;
	bool __OR;
	bool __CC0;
	bool __CC1;
} __call_FB4;
typedef struct {
	bool __RLO;
	bool __NFC;
	bool __BR;
	bool __STA;
	bool __OR;
	bool __CC0;
	bool __CC1;
} __call_FC1;
typedef struct {
	bool in1;
	bool __RLO;
	bool __NFC;
	bool __BR;
	bool __STA;
	bool __OR;
	bool __CC0;
	bool __CC1;
} __call_FC2;
typedef struct {
	bool in1;
	int16_t in2;
	int16_t RET_VAL;
	bool __RLO;
	bool __NFC;
	bool __BR;
	bool __STA;
	bool __OR;
	bool __CC0;
	bool __CC1;
} __call_FC3;
typedef struct {
	bool __RLO;
	bool __NFC;
	bool __BR;
	bool __STA;
	bool __OR;
	bool __CC0;
	bool __CC1;
} __call_OB1;

// Global variables
bool IX0_0;
bool MX0_0;
bool MX1_0;
bool MX2_0;
bool MX4_0;
__call_OB1 call_OB11;
__call_FC1 call_FC11;
__call_FC2 call_FC21;
__call_FC3 call_FC31;
__call_FB4 call_DB4;
uint16_t __assertion_error;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Automata declarations
void call_OB1(__call_OB1 *__context);
void call_FC1(__call_FC1 *__context);
void call_FC2(__call_FC2 *__context);
void call_FC3(__call_FC3 *__context);
void call_FB4(__call_FB4 *__context);
void call_FB5(__call_FB5 *__context);
void VerificationLoop();

// Automata
void call_OB1(__call_OB1 *__context) {
	// Temporary variables
	int16_t r;
	
	// Start with initial location
	goto init;
	init: {
			r = 0;
			__context->__OR = false;
			__context->__STA = true;
			__context->__RLO = true;
			__context->__CC0 = false;
			__context->__CC1 = false;
			__context->__BR = false;
			__context->__NFC = false;
			goto l2;
		//assert(false);
		return;  			}
	l2: {
			// Assign inputs
			call_FC1(&call_FC11);
			// Assign outputs
			goto l3;
		//assert(false);
		return;  			}
	l3: {
			// Assign inputs
			call_FC21.in1 = true;
			call_FC2(&call_FC21);
			// Assign outputs
			goto l4;
		//assert(false);
		return;  			}
	l4: {
			// Assign inputs
			call_FC31.in1 = IX0_0;
			call_FC31.in2 = 100;
			call_FC3(&call_FC31);
			// Assign outputs
			r = call_FC31.RET_VAL;
			goto l5;
		//assert(false);
		return;  			}
	l5: {
			call_DB4.a = (call_DB4.a || __context->__RLO);
			__context->__OR = false;
			goto l7;
		//assert(false);
		return;  			}
	l7: {
			__context->__STA = call_DB4.a;
			__context->__NFC = false;
			goto l9;
		//assert(false);
		return;  			}
	l9: {
			// Assign inputs
			call_FB4(&call_DB4);
			// Assign outputs
			goto l10;
		//assert(false);
		return;  			}
	l10: {
			__context->__RLO = (((! __context->__NFC) || __context->__RLO) && (call_DB4.q || __context->__OR));
			__context->__STA = call_DB4.q;
			goto l12;
		//assert(false);
		return;  			}
	l12: {
			__context->__NFC = true;
			MX0_0 = __context->__RLO;
			__context->__OR = false;
			__context->__STA = __context->__RLO;
			goto l16;
		//assert(false);
		return;  			}
	l16: {
			__context->__NFC = false;
			goto l17;
		//assert(false);
		return;  			}
	l17: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void call_FC1(__call_FC1 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			__context->__OR = false;
			__context->__STA = true;
			__context->__RLO = true;
			__context->__CC0 = false;
			__context->__CC1 = false;
			__context->__BR = false;
			__context->__NFC = false;
			goto l1;
		//assert(false);
		return;  			}
	l1: {
			MX1_0 = (MX1_0 || __context->__RLO);
			__context->__OR = false;
			goto l31;
		//assert(false);
		return;  			}
	l31: {
			__context->__STA = MX1_0;
			__context->__NFC = false;
			goto l51;
		//assert(false);
		return;  			}
	l51: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void call_FC2(__call_FC2 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init2;
	init2: {
			__context->__OR = false;
			__context->__STA = true;
			__context->__RLO = true;
			__context->__CC0 = false;
			__context->__CC1 = false;
			__context->__BR = false;
			__context->__NFC = false;
			goto l11;
		//assert(false);
		return;  			}
	l11: {
			__context->__RLO = (((! __context->__NFC) || __context->__RLO) && (__context->in1 || __context->__OR));
			__context->__STA = __context->in1;
			goto l32;
		//assert(false);
		return;  			}
	l32: {
			__context->__NFC = true;
			__context->__RLO = (! __context->__RLO);
			goto l52;
		//assert(false);
		return;  			}
	l52: {
			MX2_0 = __context->__RLO;
			__context->__OR = false;
			__context->__STA = __context->__RLO;
			__context->__NFC = false;
			goto l91;
		//assert(false);
		return;  			}
	l91: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void call_FC3(__call_FC3 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init3;
	init3: {
			__context->__OR = false;
			__context->__STA = true;
			__context->__RLO = true;
			__context->__CC0 = false;
			__context->__CC1 = false;
			__context->__BR = false;
			__context->__NFC = false;
			goto l13;
		//assert(false);
		return;  			}
	l13: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void call_FB4(__call_FB4 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init4;
	init4: {
			__context->__OR = false;
			__context->__STA = true;
			__context->__RLO = true;
			__context->__CC0 = false;
			__context->__CC1 = false;
			__context->__BR = false;
			__context->__NFC = false;
			goto l14;
		//assert(false);
		return;  			}
	l14: {
			__context->__RLO = (((! __context->__NFC) || __context->__RLO) && (__context->a || __context->__OR));
			__context->__STA = __context->a;
			goto l33;
		//assert(false);
		return;  			}
	l33: {
			__context->__NFC = true;
			__context->__RLO = (! __context->__RLO);
			goto l53;
		//assert(false);
		return;  			}
	l53: {
			__context->q = __context->__RLO;
			__context->__OR = false;
			__context->__STA = __context->__RLO;
			__context->__NFC = false;
			goto l92;
		//assert(false);
		return;  			}
	l92: {
			__context->__RLO = (((! __context->__NFC) || __context->__RLO) && (__context->a || __context->__OR));
			__context->__STA = __context->a;
			goto l111;
		//assert(false);
		return;  			}
	l111: {
			__context->__NFC = true;
			__context->c.b = __context->__RLO;
			__context->__OR = false;
			__context->__STA = __context->__RLO;
			goto l15;
		//assert(false);
		return;  			}
	l15: {
			__context->__NFC = false;
			goto l161;
		//assert(false);
		return;  			}
	l161: {
			// Assign inputs
			call_FB5(&__context->c);
			// Assign outputs
			goto l171;
		//assert(false);
		return;  			}
	l171: {
			__context->__RLO = (((! __context->__NFC) || __context->__RLO) && (__context->c.w || __context->__OR));
			__context->__STA = __context->c.w;
			goto l19;
		//assert(false);
		return;  			}
	l19: {
			__context->__NFC = true;
			MX4_0 = __context->__RLO;
			__context->__OR = false;
			__context->__STA = __context->__RLO;
			goto l23;
		//assert(false);
		return;  			}
	l23: {
			__context->__NFC = false;
			goto l24;
		//assert(false);
		return;  			}
	l24: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void call_FB5(__call_FB5 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init5;
	init5: {
			__context->__OR = false;
			__context->__STA = true;
			__context->__RLO = true;
			__context->__CC0 = false;
			__context->__CC1 = false;
			__context->__BR = false;
			__context->__NFC = false;
			goto l18;
		//assert(false);
		return;  			}
	l18: {
			__context->__RLO = (((! __context->__NFC) || __context->__RLO) && (__context->b || __context->__OR));
			__context->__STA = __context->b;
			goto l34;
		//assert(false);
		return;  			}
	l34: {
			__context->__NFC = true;
			__context->w = __context->__RLO;
			__context->__OR = false;
			__context->__STA = __context->__RLO;
			goto l71;
		//assert(false);
		return;  			}
	l71: {
			__context->__NFC = false;
			goto l8;
		//assert(false);
		return;  			}
	l8: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init6;
	init6: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			IX0_0 = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			call_OB1(&call_OB11);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
void main() {
	// Initial values
	IX0_0 = false;
	MX0_0 = false;
	MX1_0 = false;
	MX2_0 = false;
	MX4_0 = false;
	call_OB11.__RLO = false;
	call_OB11.__NFC = false;
	call_OB11.__BR = false;
	call_OB11.__STA = false;
	call_OB11.__OR = false;
	call_OB11.__CC0 = false;
	call_OB11.__CC1 = false;
	call_FC11.__RLO = false;
	call_FC11.__NFC = false;
	call_FC11.__BR = false;
	call_FC11.__STA = false;
	call_FC11.__OR = false;
	call_FC11.__CC0 = false;
	call_FC11.__CC1 = false;
	call_FC21.in1 = false;
	call_FC21.__RLO = false;
	call_FC21.__NFC = false;
	call_FC21.__BR = false;
	call_FC21.__STA = false;
	call_FC21.__OR = false;
	call_FC21.__CC0 = false;
	call_FC21.__CC1 = false;
	call_FC31.in1 = false;
	call_FC31.in2 = 0;
	call_FC31.RET_VAL = 0;
	call_FC31.__RLO = false;
	call_FC31.__NFC = false;
	call_FC31.__BR = false;
	call_FC31.__STA = false;
	call_FC31.__OR = false;
	call_FC31.__CC0 = false;
	call_FC31.__CC1 = false;
	call_DB4.a = false;
	call_DB4.q = false;
	call_DB4.c.b = false;
	call_DB4.c.w = false;
	call_DB4.c.__RLO = false;
	call_DB4.c.__NFC = false;
	call_DB4.c.__BR = false;
	call_DB4.c.__STA = false;
	call_DB4.c.__OR = false;
	call_DB4.c.__CC0 = false;
	call_DB4.c.__CC1 = false;
	call_DB4.__RLO = false;
	call_DB4.__NFC = false;
	call_DB4.__BR = false;
	call_DB4.__STA = false;
	call_DB4.__OR = false;
	call_DB4.__CC0 = false;
	call_DB4.__CC1 = false;
	__assertion_error = 0;
	
	VerificationLoop();
}
