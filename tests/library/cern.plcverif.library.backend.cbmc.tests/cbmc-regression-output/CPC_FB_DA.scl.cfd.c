#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Root data structure
typedef struct {
	int16_t PAlDt;
	bool PAuAckAl;
	bool I;
	bool AuAlAck;
	bool ISt;
	bool AlUnAck;
	bool MAlBRSt;
	bool AuIhMB;
	bool IOErrorW;
	bool IOSimuW;
	bool IOError;
	bool IOSimu;
	bool AuEAl;
	bool MAlBSetRst_old;
	bool MAlAck_old;
	bool AuAlAck_old;
	bool Alarm_Cond_old;
	bool AlUnAck_old;
	bool AlarmPh1;
	bool AlarmPh2;
	bool WISt;
	int32_t TimeAlarm;
	int16_t Iinc;
} __CPC_DB_DA;
typedef struct {
	uint16_t Manreg01;
	bool Manreg01b[16];
	bool PAuAckAl;
	uint16_t StsReg01;
	__CPC_DB_DA Perst;
} __CPC_FB_DA;
typedef struct {
	bool new;
	bool old;
	bool re;
	bool fe;
} __DETECT_EDGE;
typedef struct {
	bool new;
	bool old;
	bool RET_VAL;
} __R_EDGE;

// Global variables
int32_t UNICOS_LiveCounter;
int32_t T_CYCLE;
__R_EDGE R_EDGE1;
__DETECT_EDGE DETECT_EDGE1;
__CPC_FB_DA instance;
__R_EDGE R_EDGE1_inlined_1;
__R_EDGE R_EDGE1_inlined_2;
__R_EDGE R_EDGE1_inlined_3;
__R_EDGE R_EDGE1_inlined_4;
uint16_t __assertion_error;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Automata declarations
void CPC_FB_DA(__CPC_FB_DA *__context);
void R_EDGE(__R_EDGE *__context);
void DETECT_EDGE(__DETECT_EDGE *__context);
void VerificationLoop();

// Automata
void CPC_FB_DA(__CPC_FB_DA *__context) {
	// Temporary variables
	bool E_MAlBSetRst;
	bool E_MAlAck;
	bool E_AuAlAck;
	bool E_Alarm_Cond;
	bool RE_AlUnAck;
	bool FE_AlUnAck;
	bool PosW;
	bool ConfigW;
	uint16_t TempStsReg01;
	bool StsReg01b[16];
	float PulseWidth;
	
	// Start with initial location
	goto init;
	init: {
			__context->Manreg01b[0] = ((__context->Manreg01 & 256) != 0);
			__context->Manreg01b[1] = ((__context->Manreg01 & 512) != 0);
			__context->Manreg01b[2] = ((__context->Manreg01 & 1024) != 0);
			__context->Manreg01b[3] = ((__context->Manreg01 & 2048) != 0);
			__context->Manreg01b[4] = ((__context->Manreg01 & 4096) != 0);
			__context->Manreg01b[5] = ((__context->Manreg01 & 8192) != 0);
			__context->Manreg01b[6] = ((__context->Manreg01 & 16384) != 0);
			__context->Manreg01b[7] = ((__context->Manreg01 & 32768) != 0);
			__context->Manreg01b[8] = ((__context->Manreg01 & 1) != 0);
			__context->Manreg01b[9] = ((__context->Manreg01 & 2) != 0);
			__context->Manreg01b[10] = ((__context->Manreg01 & 4) != 0);
			__context->Manreg01b[11] = ((__context->Manreg01 & 8) != 0);
			__context->Manreg01b[12] = ((__context->Manreg01 & 16) != 0);
			__context->Manreg01b[13] = ((__context->Manreg01 & 32) != 0);
			__context->Manreg01b[14] = ((__context->Manreg01 & 64) != 0);
			__context->Manreg01b[15] = ((__context->Manreg01 & 128) != 0);
			StsReg01b[0] = ((TempStsReg01 & 256) != 0);
			StsReg01b[1] = ((TempStsReg01 & 512) != 0);
			StsReg01b[2] = ((TempStsReg01 & 1024) != 0);
			StsReg01b[3] = ((TempStsReg01 & 2048) != 0);
			StsReg01b[4] = ((TempStsReg01 & 4096) != 0);
			StsReg01b[5] = ((TempStsReg01 & 8192) != 0);
			StsReg01b[6] = ((TempStsReg01 & 16384) != 0);
			StsReg01b[7] = ((TempStsReg01 & 32768) != 0);
			StsReg01b[8] = ((TempStsReg01 & 1) != 0);
			StsReg01b[9] = ((TempStsReg01 & 2) != 0);
			StsReg01b[10] = ((TempStsReg01 & 4) != 0);
			StsReg01b[11] = ((TempStsReg01 & 8) != 0);
			StsReg01b[12] = ((TempStsReg01 & 16) != 0);
			StsReg01b[13] = ((TempStsReg01 & 32) != 0);
			StsReg01b[14] = ((TempStsReg01 & 64) != 0);
			StsReg01b[15] = ((TempStsReg01 & 128) != 0);
			goto x;
		//assert(false);
		return;  			}
	l1: {
			TempStsReg01 = __context->StsReg01;
			goto x2;
		//assert(false);
		return;  			}
	l2: {
			// Assign inputs
			R_EDGE1_inlined_1.new = __context->Manreg01b[2];
			R_EDGE1_inlined_1.old = __context->Perst.MAlBSetRst_old;
			R_EDGE(&R_EDGE1_inlined_1);
			// Assign outputs
			__context->Perst.MAlBSetRst_old = R_EDGE1_inlined_1.old;
			E_MAlBSetRst = R_EDGE1_inlined_1.RET_VAL;
			goto l3;
		//assert(false);
		return;  			}
	l3: {
			// Assign inputs
			R_EDGE1_inlined_2.new = __context->Manreg01b[7];
			R_EDGE1_inlined_2.old = __context->Perst.MAlAck_old;
			R_EDGE(&R_EDGE1_inlined_2);
			// Assign outputs
			__context->Perst.MAlAck_old = R_EDGE1_inlined_2.old;
			E_MAlAck = R_EDGE1_inlined_2.RET_VAL;
			goto l4;
		//assert(false);
		return;  			}
	l4: {
			// Assign inputs
			R_EDGE1_inlined_3.new = __context->Perst.AuAlAck;
			R_EDGE1_inlined_3.old = __context->Perst.AuAlAck_old;
			R_EDGE(&R_EDGE1_inlined_3);
			// Assign outputs
			__context->Perst.AuAlAck_old = R_EDGE1_inlined_3.old;
			E_AuAlAck = R_EDGE1_inlined_3.RET_VAL;
			goto l5;
		//assert(false);
		return;  			}
	l5: {
		if (E_MAlBSetRst) {
			__context->Perst.MAlBRSt = (! __context->Perst.MAlBRSt);
			goto l9;
		}
		if ((! E_MAlBSetRst)) {
			goto l9;
		}
		//assert(false);
		return;  			}
	l9: {
		if (__context->Perst.AuIhMB) {
			__context->Perst.MAlBRSt = false;
			goto l13;
		}
		if ((! __context->Perst.AuIhMB)) {
			goto l13;
		}
		//assert(false);
		return;  			}
	l13: {
		if ((__context->Perst.I && __context->Perst.AuEAl)) {
			__context->Perst.AlarmPh1 = true;
			goto l19;
		}
		if ((! (__context->Perst.I && __context->Perst.AuEAl))) {
			__context->Perst.AlarmPh1 = false;
			goto l17;
		}
		//assert(false);
		return;  			}
	l17: {
			__context->Perst.AlarmPh2 = false;
			goto l19;
		//assert(false);
		return;  			}
	l19: {
			PosW = false;
			goto l20;
		//assert(false);
		return;  			}
	l20: {
		if (__context->Perst.AlarmPh1) {
			goto l21;
		}
		if ((! __context->Perst.AlarmPh1)) {
			__context->Perst.TimeAlarm = 0;
			goto l50;
		}
		//assert(false);
		return;  			}
	l21: {
		if ((! __context->Perst.AlarmPh2)) {
			__context->Perst.TimeAlarm = (UNICOS_LiveCounter + ((int32_t) __context->Perst.PAlDt));
			goto l23;
		}
		if ((! (! __context->Perst.AlarmPh2))) {
			goto l26;
		}
		//assert(false);
		return;  			}
	l23: {
			__context->Perst.AlarmPh2 = true;
			goto l26;
		//assert(false);
		return;  			}
	l26: {
		if (__context->Perst.AlarmPh2) {
			goto l27;
		}
		if ((! __context->Perst.AlarmPh2)) {
			goto l44;
		}
		//assert(false);
		return;  			}
	l27: {
		if ((UNICOS_LiveCounter >= __context->Perst.TimeAlarm)) {
			goto l28;
		}
		if (((! (UNICOS_LiveCounter >= __context->Perst.TimeAlarm)) && __context->Perst.MAlBRSt)) {
			__context->Perst.ISt = false;
			PosW = false;
			goto l42;
		}
		if (((! (UNICOS_LiveCounter >= __context->Perst.TimeAlarm)) && (! ((! (UNICOS_LiveCounter >= __context->Perst.TimeAlarm)) && __context->Perst.MAlBRSt)))) {
			__context->Perst.ISt = false;
			PosW = true;
			goto l42;
		}
		//assert(false);
		return;  			}
	l28: {
		if (__context->Perst.MAlBRSt) {
			__context->Perst.ISt = false;
			PosW = true;
			goto l35;
		}
		if ((! __context->Perst.MAlBRSt)) {
			__context->Perst.ISt = true;
			PosW = false;
			goto l35;
		}
		//assert(false);
		return;  			}
	l35: {
			goto l42;
		//assert(false);
		return;  			}
	l42: {
			goto l44;
		//assert(false);
		return;  			}
	l44: {
		if (((__context->Perst.TimeAlarm - ((int32_t) __context->Perst.PAlDt)) > UNICOS_LiveCounter)) {
			__context->Perst.TimeAlarm = (UNICOS_LiveCounter + ((int32_t) __context->Perst.PAlDt));
			goto l48;
		}
		if ((! ((__context->Perst.TimeAlarm - ((int32_t) __context->Perst.PAlDt)) > UNICOS_LiveCounter))) {
			goto l48;
		}
		//assert(false);
		return;  			}
	l48: {
			goto l52;
		//assert(false);
		return;  			}
	l50: {
			__context->Perst.ISt = false;
			goto l52;
		//assert(false);
		return;  			}
	l52: {
			// Assign inputs
			R_EDGE1_inlined_4.new = __context->Perst.ISt;
			R_EDGE1_inlined_4.old = __context->Perst.Alarm_Cond_old;
			R_EDGE(&R_EDGE1_inlined_4);
			// Assign outputs
			__context->Perst.Alarm_Cond_old = R_EDGE1_inlined_4.old;
			E_Alarm_Cond = R_EDGE1_inlined_4.RET_VAL;
			goto l53;
		//assert(false);
		return;  			}
	l53: {
		if ((((E_MAlAck || E_AuAlAck) || __context->Perst.MAlBRSt) || __context->Perst.PAuAckAl)) {
			__context->Perst.AlUnAck = false;
			goto l59;
		}
		if (((! (((E_MAlAck || E_AuAlAck) || __context->Perst.MAlBRSt) || __context->Perst.PAuAckAl)) && E_Alarm_Cond)) {
			__context->Perst.AlUnAck = true;
			goto l59;
		}
		if (((! (((E_MAlAck || E_AuAlAck) || __context->Perst.MAlBRSt) || __context->Perst.PAuAckAl)) && (! ((! (((E_MAlAck || E_AuAlAck) || __context->Perst.MAlBRSt) || __context->Perst.PAuAckAl)) && E_Alarm_Cond)))) {
			goto l59;
		}
		//assert(false);
		return;  			}
	l59: {
			ConfigW = (! __context->Perst.AuEAl);
			goto l60;
		//assert(false);
		return;  			}
	l60: {
			__context->Perst.IOErrorW = __context->Perst.IOError;
			goto l61;
		//assert(false);
		return;  			}
	l61: {
			__context->Perst.IOSimuW = __context->Perst.IOSimu;
			PulseWidth = (1500.0 / ((float) ((int32_t) T_CYCLE)));
			goto l63;
		//assert(false);
		return;  			}
	l63: {
		if ((__context->Perst.ISt || (__context->Perst.Iinc > 0))) {
			__context->Perst.Iinc = (__context->Perst.Iinc + 1);
			goto l65;
		}
		if ((! (__context->Perst.ISt || (__context->Perst.Iinc > 0)))) {
			goto l68;
		}
		//assert(false);
		return;  			}
	l65: {
			__context->Perst.WISt = true;
			goto l68;
		//assert(false);
		return;  			}
	l68: {
		if (((((float) __context->Perst.Iinc) > PulseWidth) || ((! __context->Perst.ISt) && (__context->Perst.Iinc == 0)))) {
			__context->Perst.Iinc = 0;
			goto l70;
		}
		if ((! ((((float) __context->Perst.Iinc) > PulseWidth) || ((! __context->Perst.ISt) && (__context->Perst.Iinc == 0))))) {
			goto l73;
		}
		//assert(false);
		return;  			}
	l70: {
			__context->Perst.WISt = __context->Perst.ISt;
			goto l73;
		//assert(false);
		return;  			}
	l73: {
			StsReg01b[8] = __context->Perst.WISt;
			goto x3;
		//assert(false);
		return;  			}
	l74: {
			StsReg01b[9] = false;
			goto x4;
		//assert(false);
		return;  			}
	l75: {
			StsReg01b[10] = false;
			goto x5;
		//assert(false);
		return;  			}
	l76: {
			StsReg01b[11] = false;
			goto x6;
		//assert(false);
		return;  			}
	l77: {
			StsReg01b[12] = false;
			goto x7;
		//assert(false);
		return;  			}
	l78: {
			StsReg01b[13] = ConfigW;
			goto x8;
		//assert(false);
		return;  			}
	l79: {
			StsReg01b[14] = __context->Perst.IOErrorW;
			goto x9;
		//assert(false);
		return;  			}
	l80: {
			StsReg01b[15] = __context->Perst.IOSimuW;
			goto x10;
		//assert(false);
		return;  			}
	l81: {
			StsReg01b[0] = __context->Perst.AuEAl;
			goto x11;
		//assert(false);
		return;  			}
	l82: {
			StsReg01b[1] = PosW;
			goto x12;
		//assert(false);
		return;  			}
	l83: {
			StsReg01b[2] = false;
			goto x13;
		//assert(false);
		return;  			}
	l84: {
			StsReg01b[3] = false;
			goto x14;
		//assert(false);
		return;  			}
	l85: {
			StsReg01b[4] = __context->Perst.AlUnAck;
			goto x15;
		//assert(false);
		return;  			}
	l86: {
			StsReg01b[5] = false;
			goto x16;
		//assert(false);
		return;  			}
	l87: {
			StsReg01b[6] = __context->Perst.MAlBRSt;
			goto x17;
		//assert(false);
		return;  			}
	l88: {
			StsReg01b[7] = __context->Perst.AuIhMB;
			goto x18;
		//assert(false);
		return;  			}
	l89: {
			// Assign inputs
			DETECT_EDGE1.new = __context->Perst.AlUnAck;
			DETECT_EDGE1.old = __context->Perst.AlUnAck_old;
			DETECT_EDGE(&DETECT_EDGE1);
			// Assign outputs
			__context->Perst.AlUnAck_old = DETECT_EDGE1.old;
			RE_AlUnAck = DETECT_EDGE1.re;
			FE_AlUnAck = DETECT_EDGE1.fe;
			goto l90;
		//assert(false);
		return;  			}
	l90: {
			__context->StsReg01 = TempStsReg01;
			goto l91;
		//assert(false);
		return;  			}
	l91: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	x: {
			ConfigW = false;
			E_Alarm_Cond = false;
			E_AuAlAck = false;
			E_MAlAck = false;
			E_MAlBSetRst = false;
			FE_AlUnAck = false;
			PosW = false;
			PulseWidth = 0.0;
			RE_AlUnAck = false;
			TempStsReg01 = 0;
			goto x1;
		//assert(false);
		return;  			}
	x1: {
			StsReg01b[0] = ((TempStsReg01 & 256) != 0);
			StsReg01b[1] = ((TempStsReg01 & 512) != 0);
			StsReg01b[2] = ((TempStsReg01 & 1024) != 0);
			StsReg01b[3] = ((TempStsReg01 & 2048) != 0);
			StsReg01b[4] = ((TempStsReg01 & 4096) != 0);
			StsReg01b[5] = ((TempStsReg01 & 8192) != 0);
			StsReg01b[6] = ((TempStsReg01 & 16384) != 0);
			StsReg01b[7] = ((TempStsReg01 & 32768) != 0);
			StsReg01b[8] = ((TempStsReg01 & 1) != 0);
			StsReg01b[9] = ((TempStsReg01 & 2) != 0);
			StsReg01b[10] = ((TempStsReg01 & 4) != 0);
			StsReg01b[11] = ((TempStsReg01 & 8) != 0);
			StsReg01b[12] = ((TempStsReg01 & 16) != 0);
			StsReg01b[13] = ((TempStsReg01 & 32) != 0);
			StsReg01b[14] = ((TempStsReg01 & 64) != 0);
			StsReg01b[15] = ((TempStsReg01 & 128) != 0);
			goto l1;
		//assert(false);
		return;  			}
	x2: {
			StsReg01b[0] = ((TempStsReg01 & 256) != 0);
			StsReg01b[1] = ((TempStsReg01 & 512) != 0);
			StsReg01b[2] = ((TempStsReg01 & 1024) != 0);
			StsReg01b[3] = ((TempStsReg01 & 2048) != 0);
			StsReg01b[4] = ((TempStsReg01 & 4096) != 0);
			StsReg01b[5] = ((TempStsReg01 & 8192) != 0);
			StsReg01b[6] = ((TempStsReg01 & 16384) != 0);
			StsReg01b[7] = ((TempStsReg01 & 32768) != 0);
			StsReg01b[8] = ((TempStsReg01 & 1) != 0);
			StsReg01b[9] = ((TempStsReg01 & 2) != 0);
			StsReg01b[10] = ((TempStsReg01 & 4) != 0);
			StsReg01b[11] = ((TempStsReg01 & 8) != 0);
			StsReg01b[12] = ((TempStsReg01 & 16) != 0);
			StsReg01b[13] = ((TempStsReg01 & 32) != 0);
			StsReg01b[14] = ((TempStsReg01 & 64) != 0);
			StsReg01b[15] = ((TempStsReg01 & 128) != 0);
			goto l2;
		//assert(false);
		return;  			}
	x3: {
		if (StsReg01b[8]) {
			TempStsReg01 = (TempStsReg01 | 1);
			goto l74;
		}
		if ((! StsReg01b[8])) {
			TempStsReg01 = (TempStsReg01 & 65534);
			goto l74;
		}
		//assert(false);
		return;  			}
	x4: {
		if (StsReg01b[9]) {
			TempStsReg01 = (TempStsReg01 | 2);
			goto l75;
		}
		if ((! StsReg01b[9])) {
			TempStsReg01 = (TempStsReg01 & 65533);
			goto l75;
		}
		//assert(false);
		return;  			}
	x5: {
		if (StsReg01b[10]) {
			TempStsReg01 = (TempStsReg01 | 4);
			goto l76;
		}
		if ((! StsReg01b[10])) {
			TempStsReg01 = (TempStsReg01 & 65531);
			goto l76;
		}
		//assert(false);
		return;  			}
	x6: {
		if (StsReg01b[11]) {
			TempStsReg01 = (TempStsReg01 | 8);
			goto l77;
		}
		if ((! StsReg01b[11])) {
			TempStsReg01 = (TempStsReg01 & 65527);
			goto l77;
		}
		//assert(false);
		return;  			}
	x7: {
		if (StsReg01b[12]) {
			TempStsReg01 = (TempStsReg01 | 16);
			goto l78;
		}
		if ((! StsReg01b[12])) {
			TempStsReg01 = (TempStsReg01 & 65519);
			goto l78;
		}
		//assert(false);
		return;  			}
	x8: {
		if (StsReg01b[13]) {
			TempStsReg01 = (TempStsReg01 | 32);
			goto l79;
		}
		if ((! StsReg01b[13])) {
			TempStsReg01 = (TempStsReg01 & 65503);
			goto l79;
		}
		//assert(false);
		return;  			}
	x9: {
		if (StsReg01b[14]) {
			TempStsReg01 = (TempStsReg01 | 64);
			goto l80;
		}
		if ((! StsReg01b[14])) {
			TempStsReg01 = (TempStsReg01 & 65471);
			goto l80;
		}
		//assert(false);
		return;  			}
	x10: {
		if (StsReg01b[15]) {
			TempStsReg01 = (TempStsReg01 | 128);
			goto l81;
		}
		if ((! StsReg01b[15])) {
			TempStsReg01 = (TempStsReg01 & 65407);
			goto l81;
		}
		//assert(false);
		return;  			}
	x11: {
		if (StsReg01b[0]) {
			TempStsReg01 = (TempStsReg01 | 256);
			goto l82;
		}
		if ((! StsReg01b[0])) {
			TempStsReg01 = (TempStsReg01 & 65279);
			goto l82;
		}
		//assert(false);
		return;  			}
	x12: {
		if (StsReg01b[1]) {
			TempStsReg01 = (TempStsReg01 | 512);
			goto l83;
		}
		if ((! StsReg01b[1])) {
			TempStsReg01 = (TempStsReg01 & 65023);
			goto l83;
		}
		//assert(false);
		return;  			}
	x13: {
		if (StsReg01b[2]) {
			TempStsReg01 = (TempStsReg01 | 1024);
			goto l84;
		}
		if ((! StsReg01b[2])) {
			TempStsReg01 = (TempStsReg01 & 64511);
			goto l84;
		}
		//assert(false);
		return;  			}
	x14: {
		if (StsReg01b[3]) {
			TempStsReg01 = (TempStsReg01 | 2048);
			goto l85;
		}
		if ((! StsReg01b[3])) {
			TempStsReg01 = (TempStsReg01 & 63487);
			goto l85;
		}
		//assert(false);
		return;  			}
	x15: {
		if (StsReg01b[4]) {
			TempStsReg01 = (TempStsReg01 | 4096);
			goto l86;
		}
		if ((! StsReg01b[4])) {
			TempStsReg01 = (TempStsReg01 & 61439);
			goto l86;
		}
		//assert(false);
		return;  			}
	x16: {
		if (StsReg01b[5]) {
			TempStsReg01 = (TempStsReg01 | 8192);
			goto l87;
		}
		if ((! StsReg01b[5])) {
			TempStsReg01 = (TempStsReg01 & 57343);
			goto l87;
		}
		//assert(false);
		return;  			}
	x17: {
		if (StsReg01b[6]) {
			TempStsReg01 = (TempStsReg01 | 16384);
			goto l88;
		}
		if ((! StsReg01b[6])) {
			TempStsReg01 = (TempStsReg01 & 49151);
			goto l88;
		}
		//assert(false);
		return;  			}
	x18: {
		if (StsReg01b[7]) {
			TempStsReg01 = (TempStsReg01 | 32768);
			goto l89;
		}
		if ((! StsReg01b[7])) {
			TempStsReg01 = (TempStsReg01 & 32767);
			goto l89;
		}
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void R_EDGE(__R_EDGE *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
		if (((__context->new == true) && (__context->old == false))) {
			__context->RET_VAL = true;
			__context->old = true;
			goto l7;
		}
		if ((! ((__context->new == true) && (__context->old == false)))) {
			__context->RET_VAL = false;
			__context->old = __context->new;
			goto l7;
		}
		//assert(false);
		return;  			}
	l7: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void DETECT_EDGE(__DETECT_EDGE *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init2;
	init2: {
		if ((__context->new != __context->old)) {
			goto l11;
		}
		if ((! (__context->new != __context->old))) {
			__context->re = false;
			__context->fe = false;
			goto l131;
		}
		//assert(false);
		return;  			}
	l11: {
		if ((__context->new == true)) {
			__context->re = true;
			__context->fe = false;
			goto l8;
		}
		if ((! (__context->new == true))) {
			__context->re = false;
			__context->fe = true;
			goto l8;
		}
		//assert(false);
		return;  			}
	l8: {
			__context->old = __context->new;
			goto l131;
		//assert(false);
		return;  			}
	l131: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init3;
	init3: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			instance.Manreg01 = nondet_uint16_t();
			instance.Manreg01b[0] = nondet_bool();
			instance.Manreg01b[10] = nondet_bool();
			instance.Manreg01b[11] = nondet_bool();
			instance.Manreg01b[12] = nondet_bool();
			instance.Manreg01b[13] = nondet_bool();
			instance.Manreg01b[14] = nondet_bool();
			instance.Manreg01b[15] = nondet_bool();
			instance.Manreg01b[1] = nondet_bool();
			instance.Manreg01b[2] = nondet_bool();
			instance.Manreg01b[3] = nondet_bool();
			instance.Manreg01b[4] = nondet_bool();
			instance.Manreg01b[5] = nondet_bool();
			instance.Manreg01b[6] = nondet_bool();
			instance.Manreg01b[7] = nondet_bool();
			instance.Manreg01b[8] = nondet_bool();
			instance.Manreg01b[9] = nondet_bool();
			instance.PAuAckAl = nondet_bool();
			instance.Perst.AlUnAck = nondet_bool();
			instance.Perst.AlUnAck_old = nondet_bool();
			instance.Perst.AlarmPh1 = nondet_bool();
			instance.Perst.AlarmPh2 = nondet_bool();
			instance.Perst.Alarm_Cond_old = nondet_bool();
			instance.Perst.AuAlAck = nondet_bool();
			instance.Perst.AuAlAck_old = nondet_bool();
			instance.Perst.AuEAl = nondet_bool();
			instance.Perst.AuIhMB = nondet_bool();
			instance.Perst.I = nondet_bool();
			instance.Perst.IOError = nondet_bool();
			instance.Perst.IOErrorW = nondet_bool();
			instance.Perst.IOSimu = nondet_bool();
			instance.Perst.IOSimuW = nondet_bool();
			instance.Perst.ISt = nondet_bool();
			instance.Perst.Iinc = nondet_int16_t();
			instance.Perst.MAlAck_old = nondet_bool();
			instance.Perst.MAlBRSt = nondet_bool();
			instance.Perst.MAlBSetRst_old = nondet_bool();
			instance.Perst.PAlDt = nondet_int16_t();
			instance.Perst.PAuAckAl = nondet_bool();
			instance.Perst.TimeAlarm = nondet_int32_t();
			instance.Perst.WISt = nondet_bool();
			instance.StsReg01 = nondet_uint16_t();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			CPC_FB_DA(&instance);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
void main() {
	// Initial values
	UNICOS_LiveCounter = 0;
	T_CYCLE = 0;
	R_EDGE1.new = false;
	R_EDGE1.old = false;
	R_EDGE1.RET_VAL = false;
	DETECT_EDGE1.new = false;
	DETECT_EDGE1.old = false;
	DETECT_EDGE1.re = false;
	DETECT_EDGE1.fe = false;
	instance.Manreg01 = 0;
	instance.Manreg01b[0] = false;
	instance.Manreg01b[1] = false;
	instance.Manreg01b[2] = false;
	instance.Manreg01b[3] = false;
	instance.Manreg01b[4] = false;
	instance.Manreg01b[5] = false;
	instance.Manreg01b[6] = false;
	instance.Manreg01b[7] = false;
	instance.Manreg01b[8] = false;
	instance.Manreg01b[9] = false;
	instance.Manreg01b[10] = false;
	instance.Manreg01b[11] = false;
	instance.Manreg01b[12] = false;
	instance.Manreg01b[13] = false;
	instance.Manreg01b[14] = false;
	instance.Manreg01b[15] = false;
	instance.PAuAckAl = false;
	instance.StsReg01 = 0;
	instance.Perst.PAlDt = 0;
	instance.Perst.PAuAckAl = false;
	instance.Perst.I = false;
	instance.Perst.AuAlAck = false;
	instance.Perst.ISt = false;
	instance.Perst.AlUnAck = false;
	instance.Perst.MAlBRSt = false;
	instance.Perst.AuIhMB = false;
	instance.Perst.IOErrorW = false;
	instance.Perst.IOSimuW = false;
	instance.Perst.IOError = false;
	instance.Perst.IOSimu = false;
	instance.Perst.AuEAl = true;
	instance.Perst.MAlBSetRst_old = false;
	instance.Perst.MAlAck_old = false;
	instance.Perst.AuAlAck_old = false;
	instance.Perst.Alarm_Cond_old = false;
	instance.Perst.AlUnAck_old = false;
	instance.Perst.AlarmPh1 = false;
	instance.Perst.AlarmPh2 = false;
	instance.Perst.WISt = false;
	instance.Perst.TimeAlarm = 0;
	instance.Perst.Iinc = 0;
	R_EDGE1_inlined_1.new = false;
	R_EDGE1_inlined_1.old = false;
	R_EDGE1_inlined_1.RET_VAL = false;
	R_EDGE1_inlined_2.new = false;
	R_EDGE1_inlined_2.old = false;
	R_EDGE1_inlined_2.RET_VAL = false;
	R_EDGE1_inlined_3.new = false;
	R_EDGE1_inlined_3.old = false;
	R_EDGE1_inlined_3.RET_VAL = false;
	R_EDGE1_inlined_4.new = false;
	R_EDGE1_inlined_4.old = false;
	R_EDGE1_inlined_4.RET_VAL = false;
	__assertion_error = 0;
	
	VerificationLoop();
}
