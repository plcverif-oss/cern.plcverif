#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Root data structure
typedef struct {
	uint16_t ParReg;
	int32_t PPulseLe;
	int32_t PWDt;
} __CPC_ONOFF_PARAM;
typedef struct {
	bool new;
	bool old;
	bool re;
	bool fe;
} __DETECT_EDGE;
typedef struct {
	bool new;
	bool old;
	bool RET_VAL;
} __F_EDGE;
typedef struct {
	bool new;
	bool old;
	bool RET_VAL;
} __R_EDGE;
typedef struct {
	int32_t PT;
	bool IN;
	bool Q;
	int32_t ET;
	bool running;
	int32_t start;
} __TON;
typedef struct {
	int32_t PT;
	bool IN;
	bool Q;
	int32_t ET;
	bool old_in;
	int32_t due;
} __TP;
typedef struct {
	bool ParRegb[16];
	int32_t PPulseLeb;
	int32_t PWDtb;
} __anonymous_type;
typedef struct {
	bool HFOn;
	bool HFOff;
	bool HLD;
	bool IOError;
	bool IOSimu;
	bool AlB;
	uint16_t Manreg01;
	bool Manreg01b[16];
	bool HOnR;
	bool HOffR;
	bool StartI;
	bool TStopI;
	bool FuStopI;
	bool Al;
	bool AuOnR;
	bool AuOffR;
	bool AuAuMoR;
	bool AuIhMMo;
	bool AuIhFoMo;
	bool AuAlAck;
	bool IhAuMRW;
	bool AuRstart;
	__CPC_ONOFF_PARAM POnOff;
	__anonymous_type POnOffb;
	uint16_t Stsreg01;
	bool Stsreg01b[16];
	uint16_t Stsreg02;
	bool Stsreg02b[16];
	bool OutOnOV;
	bool OutOffOV;
	bool OnSt;
	bool OffSt;
	bool AuMoSt;
	bool MMoSt;
	bool LDSt;
	bool SoftLDSt;
	bool FoMoSt;
	bool AuOnRSt;
	bool AuOffRSt;
	bool MOnRSt;
	bool MOffRSt;
	bool HOnRSt;
	bool HOffRSt;
	bool IOErrorW;
	bool IOSimuW;
	bool AuMRW;
	bool AlUnAck;
	bool PosW;
	bool StartISt;
	bool TStopISt;
	bool FuStopISt;
	bool AlSt;
	bool AlBW;
	bool EnRstartSt;
	bool RdyStartSt;
	bool E_MAuMoR;
	bool E_MMMoR;
	bool E_MFoMoR;
	bool E_MOnR;
	bool E_MOffR;
	bool E_MAlAckR;
	bool E_StartI;
	bool E_TStopI;
	bool E_FuStopI;
	bool E_Al;
	bool E_AuAuMoR;
	bool E_AuAlAckR;
	bool E_MSoftLDR;
	bool E_MEnRstartR;
	bool RE_AlUnAck;
	bool FE_AlUnAck;
	bool RE_PulseOn;
	bool FE_PulseOn;
	bool RE_PulseOff;
	bool RE_OutOVSt_aux;
	bool FE_OutOVSt_aux;
	bool FE_InterlockR;
	bool MAuMoR_old;
	bool MMMoR_old;
	bool MFoMoR_old;
	bool MOnR_old;
	bool MOffR_old;
	bool MAlAckR_old;
	bool AuAuMoR_old;
	bool AuAlAckR_old;
	bool StartI_old;
	bool TStopI_old;
	bool FuStopI_old;
	bool Al_old;
	bool AlUnAck_old;
	bool MSoftLDR_old;
	bool MEnRstartR_old;
	bool RE_PulseOn_old;
	bool FE_PulseOn_old;
	bool RE_PulseOff_old;
	bool RE_OutOVSt_aux_old;
	bool FE_OutOVSt_aux_old;
	bool FE_InterlockR_old;
	bool PFsPosOn;
	bool PFsPosOn2;
	bool PHFOn;
	bool PHFOff;
	bool PPulse;
	bool PPulseCste;
	bool PHLD;
	bool PHLDCmd;
	bool PAnim;
	bool POutOff;
	bool PEnRstart;
	bool PRstartFS;
	bool OutOnOVSt;
	bool OutOffOVSt;
	bool AuMoSt_aux;
	bool MMoSt_aux;
	bool FoMoSt_aux;
	bool SoftLDSt_aux;
	bool PulseOn;
	bool PulseOff;
	bool PosW_aux;
	bool OutOVSt_aux;
	bool fullNotAcknowledged;
	bool PulseOnR;
	bool PulseOffR;
	bool InterlockR;
	int32_t Time_Warning;
	__TP Timer_PulseOn;
	__TP Timer_PulseOff;
	__TON Timer_Warning;
	float PulseWidth;
	int16_t FSIinc;
	int16_t TSIinc;
	int16_t SIinc;
	int16_t Alinc;
	bool WTStopISt;
	bool WStartISt;
	bool WAlSt;
	bool WFuStopISt;
} __CPC_FB_ONOFF;

// Global variables
uint8_t T_CYCLE;
int32_t __GLOBAL_TIME;
__R_EDGE R_EDGE1;
__F_EDGE F_EDGE1;
__DETECT_EDGE DETECT_EDGE1;
__CPC_FB_ONOFF instance;
__R_EDGE R_EDGE1_inlined_1;
__R_EDGE R_EDGE1_inlined_2;
__R_EDGE R_EDGE1_inlined_3;
__R_EDGE R_EDGE1_inlined_4;
__R_EDGE R_EDGE1_inlined_5;
__R_EDGE R_EDGE1_inlined_6;
__R_EDGE R_EDGE1_inlined_7;
__R_EDGE R_EDGE1_inlined_8;
__R_EDGE R_EDGE1_inlined_9;
__R_EDGE R_EDGE1_inlined_10;
__R_EDGE R_EDGE1_inlined_11;
__R_EDGE R_EDGE1_inlined_12;
__R_EDGE R_EDGE1_inlined_13;
__R_EDGE R_EDGE1_inlined_14;
__F_EDGE F_EDGE1_inlined_15;
__R_EDGE R_EDGE1_inlined_16;
__F_EDGE F_EDGE1_inlined_17;
__R_EDGE R_EDGE1_inlined_18;
__R_EDGE R_EDGE1_inlined_19;
__F_EDGE F_EDGE1_inlined_20;
uint16_t __assertion_error;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Automata declarations
void CPC_FB_ONOFF(__CPC_FB_ONOFF *__context);
void TP(__TP *__context);
void TON(__TON *__context);
void R_EDGE(__R_EDGE *__context);
void F_EDGE(__F_EDGE *__context);
void DETECT_EDGE(__DETECT_EDGE *__context);
void VerificationLoop();

// Automata
void CPC_FB_ONOFF(__CPC_FB_ONOFF *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			__context->Manreg01b[0] = ((__context->Manreg01 & 256) != 0);
			__context->Manreg01b[1] = ((__context->Manreg01 & 512) != 0);
			__context->Manreg01b[2] = ((__context->Manreg01 & 1024) != 0);
			__context->Manreg01b[3] = ((__context->Manreg01 & 2048) != 0);
			__context->Manreg01b[4] = ((__context->Manreg01 & 4096) != 0);
			__context->Manreg01b[5] = ((__context->Manreg01 & 8192) != 0);
			__context->Manreg01b[6] = ((__context->Manreg01 & 16384) != 0);
			__context->Manreg01b[7] = ((__context->Manreg01 & 32768) != 0);
			__context->Manreg01b[8] = ((__context->Manreg01 & 1) != 0);
			__context->Manreg01b[9] = ((__context->Manreg01 & 2) != 0);
			__context->Manreg01b[10] = ((__context->Manreg01 & 4) != 0);
			__context->Manreg01b[11] = ((__context->Manreg01 & 8) != 0);
			__context->Manreg01b[12] = ((__context->Manreg01 & 16) != 0);
			__context->Manreg01b[13] = ((__context->Manreg01 & 32) != 0);
			__context->Manreg01b[14] = ((__context->Manreg01 & 64) != 0);
			__context->Manreg01b[15] = ((__context->Manreg01 & 128) != 0);
			__context->POnOffb.ParRegb[0] = ((__context->POnOff.ParReg & 256) != 0);
			__context->POnOffb.ParRegb[1] = ((__context->POnOff.ParReg & 512) != 0);
			__context->POnOffb.ParRegb[2] = ((__context->POnOff.ParReg & 1024) != 0);
			__context->POnOffb.ParRegb[3] = ((__context->POnOff.ParReg & 2048) != 0);
			__context->POnOffb.ParRegb[4] = ((__context->POnOff.ParReg & 4096) != 0);
			__context->POnOffb.ParRegb[5] = ((__context->POnOff.ParReg & 8192) != 0);
			__context->POnOffb.ParRegb[6] = ((__context->POnOff.ParReg & 16384) != 0);
			__context->POnOffb.ParRegb[7] = ((__context->POnOff.ParReg & 32768) != 0);
			__context->POnOffb.ParRegb[8] = ((__context->POnOff.ParReg & 1) != 0);
			__context->POnOffb.ParRegb[9] = ((__context->POnOff.ParReg & 2) != 0);
			__context->POnOffb.ParRegb[10] = ((__context->POnOff.ParReg & 4) != 0);
			__context->POnOffb.ParRegb[11] = ((__context->POnOff.ParReg & 8) != 0);
			__context->POnOffb.ParRegb[12] = ((__context->POnOff.ParReg & 16) != 0);
			__context->POnOffb.ParRegb[13] = ((__context->POnOff.ParReg & 32) != 0);
			__context->POnOffb.ParRegb[14] = ((__context->POnOff.ParReg & 64) != 0);
			__context->POnOffb.ParRegb[15] = ((__context->POnOff.ParReg & 128) != 0);
			goto varview_refresh33;
		//assert(false);
		return;  			}
	l1: {
			// Assign inputs
			R_EDGE1_inlined_2.new = __context->Manreg01b[9];
			R_EDGE1_inlined_2.old = __context->MMMoR_old;
			R_EDGE(&R_EDGE1_inlined_2);
			// Assign outputs
			__context->MMMoR_old = R_EDGE1_inlined_2.old;
			__context->E_MMMoR = R_EDGE1_inlined_2.RET_VAL;
			goto l2;
		//assert(false);
		return;  			}
	l2: {
			// Assign inputs
			R_EDGE1_inlined_3.new = __context->Manreg01b[10];
			R_EDGE1_inlined_3.old = __context->MFoMoR_old;
			R_EDGE(&R_EDGE1_inlined_3);
			// Assign outputs
			__context->MFoMoR_old = R_EDGE1_inlined_3.old;
			__context->E_MFoMoR = R_EDGE1_inlined_3.RET_VAL;
			goto l3;
		//assert(false);
		return;  			}
	l3: {
			// Assign inputs
			R_EDGE1_inlined_4.new = __context->Manreg01b[11];
			R_EDGE1_inlined_4.old = __context->MSoftLDR_old;
			R_EDGE(&R_EDGE1_inlined_4);
			// Assign outputs
			__context->MSoftLDR_old = R_EDGE1_inlined_4.old;
			__context->E_MSoftLDR = R_EDGE1_inlined_4.RET_VAL;
			goto l4;
		//assert(false);
		return;  			}
	l4: {
			// Assign inputs
			R_EDGE1_inlined_5.new = __context->Manreg01b[12];
			R_EDGE1_inlined_5.old = __context->MOnR_old;
			R_EDGE(&R_EDGE1_inlined_5);
			// Assign outputs
			__context->MOnR_old = R_EDGE1_inlined_5.old;
			__context->E_MOnR = R_EDGE1_inlined_5.RET_VAL;
			goto l5;
		//assert(false);
		return;  			}
	l5: {
			// Assign inputs
			R_EDGE1_inlined_6.new = __context->Manreg01b[13];
			R_EDGE1_inlined_6.old = __context->MOffR_old;
			R_EDGE(&R_EDGE1_inlined_6);
			// Assign outputs
			__context->MOffR_old = R_EDGE1_inlined_6.old;
			__context->E_MOffR = R_EDGE1_inlined_6.RET_VAL;
			goto l6;
		//assert(false);
		return;  			}
	l6: {
			// Assign inputs
			R_EDGE1_inlined_7.new = __context->Manreg01b[1];
			R_EDGE1_inlined_7.old = __context->MEnRstartR_old;
			R_EDGE(&R_EDGE1_inlined_7);
			// Assign outputs
			__context->MEnRstartR_old = R_EDGE1_inlined_7.old;
			__context->E_MEnRstartR = R_EDGE1_inlined_7.RET_VAL;
			goto l7;
		//assert(false);
		return;  			}
	l7: {
			// Assign inputs
			R_EDGE1_inlined_8.new = __context->Manreg01b[7];
			R_EDGE1_inlined_8.old = __context->MAlAckR_old;
			R_EDGE(&R_EDGE1_inlined_8);
			// Assign outputs
			__context->MAlAckR_old = R_EDGE1_inlined_8.old;
			__context->E_MAlAckR = R_EDGE1_inlined_8.RET_VAL;
			goto l8;
		//assert(false);
		return;  			}
	l8: {
			__context->PFsPosOn = __context->POnOffb.ParRegb[8];
			__context->PHFOn = __context->POnOffb.ParRegb[9];
			__context->PHFOff = __context->POnOffb.ParRegb[10];
			__context->PPulse = __context->POnOffb.ParRegb[11];
			__context->PHLD = __context->POnOffb.ParRegb[12];
			__context->PHLDCmd = __context->POnOffb.ParRegb[13];
			__context->PAnim = __context->POnOffb.ParRegb[14];
			__context->POutOff = __context->POnOffb.ParRegb[15];
			__context->PEnRstart = __context->POnOffb.ParRegb[0];
			__context->PRstartFS = __context->POnOffb.ParRegb[1];
			__context->PFsPosOn2 = __context->POnOffb.ParRegb[2];
			__context->PPulseCste = __context->POnOffb.ParRegb[3];
			goto l20;
		//assert(false);
		return;  			}
	l20: {
			// Assign inputs
			R_EDGE1_inlined_9.new = __context->AuAuMoR;
			R_EDGE1_inlined_9.old = __context->AuAuMoR_old;
			R_EDGE(&R_EDGE1_inlined_9);
			// Assign outputs
			__context->AuAuMoR_old = R_EDGE1_inlined_9.old;
			__context->E_AuAuMoR = R_EDGE1_inlined_9.RET_VAL;
			goto l21;
		//assert(false);
		return;  			}
	l21: {
			// Assign inputs
			R_EDGE1_inlined_10.new = __context->AuAlAck;
			R_EDGE1_inlined_10.old = __context->AuAlAckR_old;
			R_EDGE(&R_EDGE1_inlined_10);
			// Assign outputs
			__context->AuAlAckR_old = R_EDGE1_inlined_10.old;
			__context->E_AuAlAckR = R_EDGE1_inlined_10.RET_VAL;
			goto l22;
		//assert(false);
		return;  			}
	l22: {
			// Assign inputs
			R_EDGE1_inlined_11.new = __context->StartI;
			R_EDGE1_inlined_11.old = __context->StartI_old;
			R_EDGE(&R_EDGE1_inlined_11);
			// Assign outputs
			__context->StartI_old = R_EDGE1_inlined_11.old;
			__context->E_StartI = R_EDGE1_inlined_11.RET_VAL;
			goto l23;
		//assert(false);
		return;  			}
	l23: {
			// Assign inputs
			R_EDGE1_inlined_12.new = __context->TStopI;
			R_EDGE1_inlined_12.old = __context->TStopI_old;
			R_EDGE(&R_EDGE1_inlined_12);
			// Assign outputs
			__context->TStopI_old = R_EDGE1_inlined_12.old;
			__context->E_TStopI = R_EDGE1_inlined_12.RET_VAL;
			goto l24;
		//assert(false);
		return;  			}
	l24: {
			// Assign inputs
			R_EDGE1_inlined_13.new = __context->FuStopI;
			R_EDGE1_inlined_13.old = __context->FuStopI_old;
			R_EDGE(&R_EDGE1_inlined_13);
			// Assign outputs
			__context->FuStopI_old = R_EDGE1_inlined_13.old;
			__context->E_FuStopI = R_EDGE1_inlined_13.RET_VAL;
			goto l25;
		//assert(false);
		return;  			}
	l25: {
			// Assign inputs
			R_EDGE1_inlined_14.new = __context->Al;
			R_EDGE1_inlined_14.old = __context->Al_old;
			R_EDGE(&R_EDGE1_inlined_14);
			// Assign outputs
			__context->Al_old = R_EDGE1_inlined_14.old;
			__context->E_Al = R_EDGE1_inlined_14.RET_VAL;
			goto l26;
		//assert(false);
		return;  			}
	l26: {
			__context->StartISt = __context->StartI;
			__context->TStopISt = __context->TStopI;
			__context->FuStopISt = __context->FuStopI;
			goto l29;
		//assert(false);
		return;  			}
	l29: {
		if (__context->E_FuStopI) {
			__context->fullNotAcknowledged = true;
			goto l31;
		}
		if ((! __context->E_FuStopI)) {
			goto l37;
		}
		//assert(false);
		return;  			}
	l31: {
		if (__context->PEnRstart) {
			__context->EnRstartSt = false;
			goto l35;
		}
		if ((! __context->PEnRstart)) {
			goto l35;
		}
		//assert(false);
		return;  			}
	l35: {
			goto l37;
		//assert(false);
		return;  			}
	l37: {
		if ((((__context->E_TStopI || __context->E_StartI) || __context->E_FuStopI) || __context->E_Al)) {
			__context->AlUnAck = true;
			goto l44;
		}
		if (((! (((__context->E_TStopI || __context->E_StartI) || __context->E_FuStopI) || __context->E_Al)) && (__context->E_MAlAckR || __context->E_AuAlAckR))) {
			__context->fullNotAcknowledged = false;
			__context->AlUnAck = false;
			goto l44;
		}
		if (((! (((__context->E_TStopI || __context->E_StartI) || __context->E_FuStopI) || __context->E_Al)) && (! ((! (((__context->E_TStopI || __context->E_StartI) || __context->E_FuStopI) || __context->E_Al)) && (__context->E_MAlAckR || __context->E_AuAlAckR))))) {
			goto l44;
		}
		//assert(false);
		return;  			}
	l44: {
		if (((((__context->PEnRstart && (__context->E_MEnRstartR || __context->AuRstart)) && (! __context->FuStopISt)) || ((__context->PEnRstart && __context->PRstartFS) && (__context->E_MEnRstartR || __context->AuRstart))) && (! __context->fullNotAcknowledged))) {
			__context->EnRstartSt = true;
			goto l48;
		}
		if ((! ((((__context->PEnRstart && (__context->E_MEnRstartR || __context->AuRstart)) && (! __context->FuStopISt)) || ((__context->PEnRstart && __context->PRstartFS) && (__context->E_MEnRstartR || __context->AuRstart))) && (! __context->fullNotAcknowledged)))) {
			goto l48;
		}
		//assert(false);
		return;  			}
	l48: {
			__context->InterlockR = (((((__context->TStopISt || __context->FuStopISt) || __context->fullNotAcknowledged) || (! __context->EnRstartSt)) || ((__context->StartISt && (! __context->POutOff)) && (! __context->OutOnOV))) || ((__context->StartISt && __context->POutOff) && ((__context->PFsPosOn && __context->OutOVSt_aux) || ((! __context->PFsPosOn) && (! __context->OutOVSt_aux)))));
			goto l49;
		//assert(false);
		return;  			}
	l49: {
			// Assign inputs
			F_EDGE1_inlined_15.new = __context->InterlockR;
			F_EDGE1_inlined_15.old = __context->FE_InterlockR_old;
			F_EDGE(&F_EDGE1_inlined_15);
			// Assign outputs
			__context->FE_InterlockR_old = F_EDGE1_inlined_15.old;
			__context->FE_InterlockR = F_EDGE1_inlined_15.RET_VAL;
			goto l50;
		//assert(false);
		return;  			}
	l50: {
		if ((! (__context->HLD && __context->PHLD))) {
			goto l51;
		}
		if ((! (! (__context->HLD && __context->PHLD)))) {
			__context->AuMoSt = false;
			__context->MMoSt = false;
			__context->FoMoSt = false;
			__context->LDSt = true;
			__context->SoftLDSt = false;
			goto l91;
		}
		//assert(false);
		return;  			}
	l51: {
		if (((((__context->AuMoSt_aux || __context->MMoSt_aux) || __context->SoftLDSt_aux) && __context->E_MFoMoR) && (! __context->AuIhFoMo))) {
			__context->AuMoSt_aux = false;
			__context->MMoSt_aux = false;
			__context->FoMoSt_aux = true;
			__context->SoftLDSt_aux = false;
			goto l58;
		}
		if ((! ((((__context->AuMoSt_aux || __context->MMoSt_aux) || __context->SoftLDSt_aux) && __context->E_MFoMoR) && (! __context->AuIhFoMo)))) {
			goto l58;
		}
		//assert(false);
		return;  			}
	l58: {
		if (((((__context->AuMoSt_aux || __context->FoMoSt_aux) || __context->SoftLDSt_aux) && __context->E_MMMoR) && (! __context->AuIhMMo))) {
			__context->AuMoSt_aux = false;
			__context->MMoSt_aux = true;
			__context->FoMoSt_aux = false;
			__context->SoftLDSt_aux = false;
			goto l65;
		}
		if ((! ((((__context->AuMoSt_aux || __context->FoMoSt_aux) || __context->SoftLDSt_aux) && __context->E_MMMoR) && (! __context->AuIhMMo)))) {
			goto l65;
		}
		//assert(false);
		return;  			}
	l65: {
		if ((((((((__context->MMoSt_aux && (__context->E_MAuMoR || __context->E_AuAuMoR)) || (__context->FoMoSt_aux && __context->E_MAuMoR)) || (__context->SoftLDSt_aux && __context->E_MAuMoR)) || (__context->MMoSt_aux && __context->AuIhMMo)) || (__context->FoMoSt_aux && __context->AuIhFoMo)) || (__context->SoftLDSt_aux && __context->AuIhFoMo)) || (! (((__context->AuMoSt_aux || __context->MMoSt_aux) || __context->FoMoSt_aux) || __context->SoftLDSt_aux)))) {
			__context->AuMoSt_aux = true;
			__context->MMoSt_aux = false;
			__context->FoMoSt_aux = false;
			__context->SoftLDSt_aux = false;
			goto l72;
		}
		if ((! (((((((__context->MMoSt_aux && (__context->E_MAuMoR || __context->E_AuAuMoR)) || (__context->FoMoSt_aux && __context->E_MAuMoR)) || (__context->SoftLDSt_aux && __context->E_MAuMoR)) || (__context->MMoSt_aux && __context->AuIhMMo)) || (__context->FoMoSt_aux && __context->AuIhFoMo)) || (__context->SoftLDSt_aux && __context->AuIhFoMo)) || (! (((__context->AuMoSt_aux || __context->MMoSt_aux) || __context->FoMoSt_aux) || __context->SoftLDSt_aux))))) {
			goto l72;
		}
		//assert(false);
		return;  			}
	l72: {
		if ((((__context->AuMoSt_aux || __context->MMoSt_aux) && __context->E_MSoftLDR) && (! __context->AuIhFoMo))) {
			__context->AuMoSt_aux = false;
			__context->MMoSt_aux = false;
			__context->FoMoSt_aux = false;
			__context->SoftLDSt_aux = true;
			goto l79;
		}
		if ((! (((__context->AuMoSt_aux || __context->MMoSt_aux) && __context->E_MSoftLDR) && (! __context->AuIhFoMo)))) {
			goto l79;
		}
		//assert(false);
		return;  			}
	l79: {
			__context->LDSt = false;
			__context->AuMoSt = __context->AuMoSt_aux;
			__context->MMoSt = __context->MMoSt_aux;
			__context->FoMoSt = __context->FoMoSt_aux;
			__context->SoftLDSt = __context->SoftLDSt_aux;
			goto l91;
		//assert(false);
		return;  			}
	l91: {
			__context->OnSt = (((__context->HFOn && __context->PHFOn) || ((((! __context->PHFOn) && __context->PHFOff) && __context->PAnim) && (! __context->HFOff))) || (((! __context->PHFOn) && (! __context->PHFOff)) && __context->OutOVSt_aux));
			__context->OffSt = (((__context->HFOff && __context->PHFOff) || ((((! __context->PHFOff) && __context->PHFOn) && __context->PAnim) && (! __context->HFOn))) || (((! __context->PHFOn) && (! __context->PHFOff)) && (! __context->OutOVSt_aux)));
			goto l93;
		//assert(false);
		return;  			}
	l93: {
		if (__context->AuOffR) {
			__context->AuOnRSt = false;
			goto l101;
		}
		if (((! __context->AuOffR) && __context->AuOnR)) {
			__context->AuOnRSt = true;
			goto l101;
		}
		if ((((! __context->AuOffR) && (! __context->AuOnR)) && ((__context->fullNotAcknowledged || __context->FuStopISt) || (! __context->EnRstartSt)))) {
			__context->AuOnRSt = __context->PFsPosOn;
			goto l101;
		}
		if (((! __context->AuOffR) && ((! ((! __context->AuOffR) && __context->AuOnR)) && (! (((! __context->AuOffR) && (! __context->AuOnR)) && ((__context->fullNotAcknowledged || __context->FuStopISt) || (! __context->EnRstartSt))))))) {
			goto l101;
		}
		//assert(false);
		return;  			}
	l101: {
			__context->AuOffRSt = (! __context->AuOnRSt);
			goto l102;
		//assert(false);
		return;  			}
	l102: {
		if (((((((__context->E_MOffR && ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt)) || (__context->AuOffRSt && __context->AuMoSt)) || ((__context->LDSt && __context->PHLDCmd) && __context->HOffRSt)) || ((__context->FE_PulseOn && __context->PPulse) && (! __context->POutOff))) && __context->EnRstartSt) || (__context->E_FuStopI && (! __context->PFsPosOn)))) {
			__context->MOnRSt = false;
			goto l108;
		}
		if (((! ((((((__context->E_MOffR && ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt)) || (__context->AuOffRSt && __context->AuMoSt)) || ((__context->LDSt && __context->PHLDCmd) && __context->HOffRSt)) || ((__context->FE_PulseOn && __context->PPulse) && (! __context->POutOff))) && __context->EnRstartSt) || (__context->E_FuStopI && (! __context->PFsPosOn)))) && (((((__context->E_MOnR && ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt)) || (__context->AuOnRSt && __context->AuMoSt)) || ((__context->LDSt && __context->PHLDCmd) && __context->HOnRSt)) && __context->EnRstartSt) || (__context->E_FuStopI && __context->PFsPosOn)))) {
			__context->MOnRSt = true;
			goto l108;
		}
		if (((! ((((((__context->E_MOffR && ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt)) || (__context->AuOffRSt && __context->AuMoSt)) || ((__context->LDSt && __context->PHLDCmd) && __context->HOffRSt)) || ((__context->FE_PulseOn && __context->PPulse) && (! __context->POutOff))) && __context->EnRstartSt) || (__context->E_FuStopI && (! __context->PFsPosOn)))) && (! ((! ((((((__context->E_MOffR && ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt)) || (__context->AuOffRSt && __context->AuMoSt)) || ((__context->LDSt && __context->PHLDCmd) && __context->HOffRSt)) || ((__context->FE_PulseOn && __context->PPulse) && (! __context->POutOff))) && __context->EnRstartSt) || (__context->E_FuStopI && (! __context->PFsPosOn)))) && (((((__context->E_MOnR && ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt)) || (__context->AuOnRSt && __context->AuMoSt)) || ((__context->LDSt && __context->PHLDCmd) && __context->HOnRSt)) && __context->EnRstartSt) || (__context->E_FuStopI && __context->PFsPosOn)))))) {
			goto l108;
		}
		//assert(false);
		return;  			}
	l108: {
			__context->MOffRSt = (! __context->MOnRSt);
			goto l109;
		//assert(false);
		return;  			}
	l109: {
		if (__context->HOffR) {
			__context->HOnRSt = false;
			goto l117;
		}
		if ((! __context->HOffR)) {
			goto l112;
		}
		//assert(false);
		return;  			}
	l112: {
		if (__context->HOnR) {
			__context->HOnRSt = true;
			goto l116;
		}
		if ((! __context->HOnR)) {
			goto l116;
		}
		//assert(false);
		return;  			}
	l116: {
			goto l117;
		//assert(false);
		return;  			}
	l117: {
			__context->HOffRSt = (! __context->HOnRSt);
			goto l118;
		//assert(false);
		return;  			}
	l118: {
		if (__context->PPulse) {
			goto l119;
		}
		if ((! __context->PPulse)) {
			goto l159;
		}
		//assert(false);
		return;  			}
	l119: {
		if (__context->InterlockR) {
			__context->PulseOnR = ((__context->PFsPosOn && (! __context->PFsPosOn2)) || (__context->PFsPosOn && __context->PFsPosOn2));
			__context->PulseOffR = (((! __context->PFsPosOn) && (! __context->PFsPosOn2)) || (__context->PFsPosOn && __context->PFsPosOn2));
			goto l137;
		}
		if (((! __context->InterlockR) && __context->FE_InterlockR)) {
			__context->PulseOnR = false;
			__context->PulseOffR = false;
			goto l125;
		}
		if ((((! __context->InterlockR) && (! __context->FE_InterlockR)) && (((__context->MOffRSt && ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt)) || (__context->AuOffRSt && __context->AuMoSt)) || ((__context->HOffR && __context->LDSt) && __context->PHLDCmd)))) {
			__context->PulseOnR = false;
			__context->PulseOffR = true;
			goto l137;
		}
		if ((((! __context->InterlockR) && ((! __context->FE_InterlockR) && (! (((__context->MOffRSt && ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt)) || (__context->AuOffRSt && __context->AuMoSt)) || ((__context->HOffR && __context->LDSt) && __context->PHLDCmd))))) && (((__context->MOnRSt && ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt)) || (__context->AuOnRSt && __context->AuMoSt)) || ((__context->HOnR && __context->LDSt) && __context->PHLDCmd)))) {
			__context->PulseOnR = true;
			__context->PulseOffR = false;
			goto l137;
		}
		if (((! __context->InterlockR) && ((! ((! __context->InterlockR) && __context->FE_InterlockR)) && ((! (((! __context->InterlockR) && (! __context->FE_InterlockR)) && (((__context->MOffRSt && ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt)) || (__context->AuOffRSt && __context->AuMoSt)) || ((__context->HOffR && __context->LDSt) && __context->PHLDCmd)))) && (! (((! __context->InterlockR) && ((! __context->FE_InterlockR) && (! (((__context->MOffRSt && ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt)) || (__context->AuOffRSt && __context->AuMoSt)) || ((__context->HOffR && __context->LDSt) && __context->PHLDCmd))))) && (((__context->MOnRSt && ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt)) || (__context->AuOnRSt && __context->AuMoSt)) || ((__context->HOnR && __context->LDSt) && __context->PHLDCmd)))))))) {
			__context->PulseOnR = false;
			__context->PulseOffR = false;
			goto l137;
		}
		//assert(false);
		return;  			}
	l125: {
			// Assign inputs
			__context->Timer_PulseOn.IN = false;
			__context->Timer_PulseOn.PT = 0;
			TP(&__context->Timer_PulseOn);
			// Assign outputs
			goto l126;
		//assert(false);
		return;  			}
	l126: {
			// Assign inputs
			__context->Timer_PulseOff.IN = false;
			__context->Timer_PulseOff.PT = 0;
			TP(&__context->Timer_PulseOff);
			// Assign outputs
			goto l127;
		//assert(false);
		return;  			}
	l127: {
			goto l137;
		//assert(false);
		return;  			}
	l137: {
			// Assign inputs
			__context->Timer_PulseOn.IN = __context->PulseOnR;
			__context->Timer_PulseOn.PT = __context->POnOffb.PPulseLeb;
			TP(&__context->Timer_PulseOn);
			// Assign outputs
			goto l138;
		//assert(false);
		return;  			}
	l138: {
			// Assign inputs
			__context->Timer_PulseOff.IN = __context->PulseOffR;
			__context->Timer_PulseOff.PT = __context->POnOffb.PPulseLeb;
			TP(&__context->Timer_PulseOff);
			// Assign outputs
			goto l139;
		//assert(false);
		return;  			}
	l139: {
			// Assign inputs
			R_EDGE1_inlined_16.new = __context->PulseOn;
			R_EDGE1_inlined_16.old = __context->RE_PulseOn_old;
			R_EDGE(&R_EDGE1_inlined_16);
			// Assign outputs
			__context->RE_PulseOn_old = R_EDGE1_inlined_16.old;
			__context->RE_PulseOn = R_EDGE1_inlined_16.RET_VAL;
			goto l140;
		//assert(false);
		return;  			}
	l140: {
			// Assign inputs
			F_EDGE1_inlined_17.new = __context->PulseOn;
			F_EDGE1_inlined_17.old = __context->FE_PulseOn_old;
			F_EDGE(&F_EDGE1_inlined_17);
			// Assign outputs
			__context->FE_PulseOn_old = F_EDGE1_inlined_17.old;
			__context->FE_PulseOn = F_EDGE1_inlined_17.RET_VAL;
			goto l141;
		//assert(false);
		return;  			}
	l141: {
			// Assign inputs
			R_EDGE1_inlined_18.new = __context->PulseOff;
			R_EDGE1_inlined_18.old = __context->RE_PulseOff_old;
			R_EDGE(&R_EDGE1_inlined_18);
			// Assign outputs
			__context->RE_PulseOff_old = R_EDGE1_inlined_18.old;
			__context->RE_PulseOff = R_EDGE1_inlined_18.RET_VAL;
			goto l142;
		//assert(false);
		return;  			}
	l142: {
		if (__context->RE_PulseOn) {
			goto l143;
		}
		if ((! __context->RE_PulseOn)) {
			goto l146;
		}
		//assert(false);
		return;  			}
	l143: {
			// Assign inputs
			__context->Timer_PulseOff.IN = false;
			__context->Timer_PulseOff.PT = 0;
			TP(&__context->Timer_PulseOff);
			// Assign outputs
			goto l144;
		//assert(false);
		return;  			}
	l144: {
			goto l146;
		//assert(false);
		return;  			}
	l146: {
		if (__context->RE_PulseOff) {
			goto l147;
		}
		if ((! __context->RE_PulseOff)) {
			goto l150;
		}
		//assert(false);
		return;  			}
	l147: {
			// Assign inputs
			__context->Timer_PulseOn.IN = false;
			__context->Timer_PulseOn.PT = 0;
			TP(&__context->Timer_PulseOn);
			// Assign outputs
			goto l148;
		//assert(false);
		return;  			}
	l148: {
			goto l150;
		//assert(false);
		return;  			}
	l150: {
		if (__context->PPulseCste) {
			__context->PulseOn = (__context->Timer_PulseOn.Q && (! __context->PulseOffR));
			__context->PulseOff = (__context->Timer_PulseOff.Q && (! __context->PulseOnR));
			goto l157;
		}
		if ((! __context->PPulseCste)) {
			__context->PulseOn = ((__context->Timer_PulseOn.Q && (! __context->PulseOffR)) && ((! __context->PHFOn) || (__context->PHFOn && (! __context->HFOn))));
			__context->PulseOff = ((__context->Timer_PulseOff.Q && (! __context->PulseOnR)) && ((! __context->PHFOff) || (__context->PHFOff && (! __context->HFOff))));
			goto l157;
		}
		//assert(false);
		return;  			}
	l157: {
			goto l159;
		//assert(false);
		return;  			}
	l159: {
			__context->OutOnOVSt = ((__context->PPulse && __context->PulseOn) || ((! __context->PPulse) && (((__context->MOnRSt && ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt)) || (__context->AuOnRSt && __context->AuMoSt)) || ((__context->HOnRSt && __context->LDSt) && __context->PHLDCmd))));
			goto l160;
		//assert(false);
		return;  			}
	l160: {
		if (__context->POutOff) {
			__context->OutOffOVSt = ((__context->PulseOff && __context->PPulse) || ((! __context->PPulse) && (((__context->MOffRSt && ((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt)) || (__context->AuOffRSt && __context->AuMoSt)) || ((__context->HOffRSt && __context->LDSt) && __context->PHLDCmd))));
			goto l164;
		}
		if ((! __context->POutOff)) {
			goto l164;
		}
		//assert(false);
		return;  			}
	l164: {
		if (__context->POutOff) {
			goto l165;
		}
		if ((! __context->POutOff)) {
			goto l184;
		}
		//assert(false);
		return;  			}
	l165: {
		if (__context->InterlockR) {
			goto l166;
		}
		if ((! __context->InterlockR)) {
			goto l183;
		}
		//assert(false);
		return;  			}
	l166: {
		if ((__context->PPulse && (! __context->PFsPosOn2))) {
			goto l167;
		}
		if ((! (__context->PPulse && (! __context->PFsPosOn2)))) {
			__context->OutOnOVSt = ((__context->PFsPosOn && (! __context->PFsPosOn2)) || (__context->PFsPosOn && __context->PFsPosOn2));
			__context->OutOffOVSt = (((! __context->PFsPosOn) && (! __context->PFsPosOn2)) || (__context->PFsPosOn && __context->PFsPosOn2));
			__context->OutOVSt_aux = __context->PFsPosOn;
			goto l181;
		}
		//assert(false);
		return;  			}
	l167: {
		if (__context->PFsPosOn) {
			__context->OutOnOVSt = __context->PulseOn;
			__context->OutOffOVSt = false;
			__context->OutOVSt_aux = true;
			goto l176;
		}
		if ((! __context->PFsPosOn)) {
			__context->OutOnOVSt = false;
			__context->OutOffOVSt = __context->PulseOff;
			__context->OutOVSt_aux = false;
			goto l176;
		}
		//assert(false);
		return;  			}
	l176: {
			goto l181;
		//assert(false);
		return;  			}
	l181: {
			goto l183;
		//assert(false);
		return;  			}
	l183: {
			goto l190;
		//assert(false);
		return;  			}
	l184: {
		if (__context->InterlockR) {
			__context->OutOnOVSt = __context->PFsPosOn;
			__context->OutOVSt_aux = __context->PFsPosOn;
			goto l189;
		}
		if ((! __context->InterlockR)) {
			goto l189;
		}
		//assert(false);
		return;  			}
	l189: {
			goto l190;
		//assert(false);
		return;  			}
	l190: {
			__context->RdyStartSt = (! __context->InterlockR);
			__context->AlSt = __context->Al;
			__context->IOErrorW = __context->IOError;
			__context->IOSimuW = __context->IOSimu;
			__context->AuMRW = ((((__context->MMoSt || __context->FoMoSt) || __context->SoftLDSt) && ((__context->AuOnRSt != __context->MOnRSt) || (__context->AuOffRSt != __context->MOffRSt))) && (! __context->IhAuMRW));
			goto l195;
		//assert(false);
		return;  			}
	l195: {
		if ((! __context->POutOff)) {
			goto l196;
		}
		if ((! (! __context->POutOff))) {
			__context->OutOnOV = __context->OutOnOVSt;
			__context->OutOffOV = __context->OutOffOVSt;
			goto l205;
		}
		//assert(false);
		return;  			}
	l196: {
		if (__context->PFsPosOn) {
			__context->OutOnOV = (! __context->OutOnOVSt);
			goto l201;
		}
		if ((! __context->PFsPosOn)) {
			__context->OutOnOV = __context->OutOnOVSt;
			goto l201;
		}
		//assert(false);
		return;  			}
	l201: {
			goto l205;
		//assert(false);
		return;  			}
	l205: {
		if (((((__context->OutOffOVSt && __context->POutOff) || ((! __context->OutOnOVSt) && (! __context->POutOff))) || (__context->PPulse && __context->PulseOffR)) || ((__context->LDSt && (! __context->PHLDCmd)) && (! __context->PFsPosOn)))) {
			__context->OutOVSt_aux = false;
			goto l211;
		}
		if (((! ((((__context->OutOffOVSt && __context->POutOff) || ((! __context->OutOnOVSt) && (! __context->POutOff))) || (__context->PPulse && __context->PulseOffR)) || ((__context->LDSt && (! __context->PHLDCmd)) && (! __context->PFsPosOn)))) && ((__context->OutOnOVSt || (__context->PPulse && __context->PulseOnR)) || ((__context->LDSt && (! __context->PHLDCmd)) && __context->PFsPosOn)))) {
			__context->OutOVSt_aux = true;
			goto l211;
		}
		if (((! ((((__context->OutOffOVSt && __context->POutOff) || ((! __context->OutOnOVSt) && (! __context->POutOff))) || (__context->PPulse && __context->PulseOffR)) || ((__context->LDSt && (! __context->PHLDCmd)) && (! __context->PFsPosOn)))) && (! ((! ((((__context->OutOffOVSt && __context->POutOff) || ((! __context->OutOnOVSt) && (! __context->POutOff))) || (__context->PPulse && __context->PulseOffR)) || ((__context->LDSt && (! __context->PHLDCmd)) && (! __context->PFsPosOn)))) && ((__context->OutOnOVSt || (__context->PPulse && __context->PulseOnR)) || ((__context->LDSt && (! __context->PHLDCmd)) && __context->PFsPosOn)))))) {
			goto l211;
		}
		//assert(false);
		return;  			}
	l211: {
			// Assign inputs
			R_EDGE1_inlined_19.new = __context->OutOVSt_aux;
			R_EDGE1_inlined_19.old = __context->RE_OutOVSt_aux_old;
			R_EDGE(&R_EDGE1_inlined_19);
			// Assign outputs
			__context->RE_OutOVSt_aux_old = R_EDGE1_inlined_19.old;
			__context->RE_OutOVSt_aux = R_EDGE1_inlined_19.RET_VAL;
			goto l212;
		//assert(false);
		return;  			}
	l212: {
			// Assign inputs
			F_EDGE1_inlined_20.new = __context->OutOVSt_aux;
			F_EDGE1_inlined_20.old = __context->FE_OutOVSt_aux_old;
			F_EDGE(&F_EDGE1_inlined_20);
			// Assign outputs
			__context->FE_OutOVSt_aux_old = F_EDGE1_inlined_20.old;
			__context->FE_OutOVSt_aux = F_EDGE1_inlined_20.RET_VAL;
			goto l213;
		//assert(false);
		return;  			}
	l213: {
		if (((((__context->OutOVSt_aux && ((__context->PHFOn && (! __context->OnSt)) || (__context->PHFOff && __context->OffSt))) || ((! __context->OutOVSt_aux) && ((__context->PHFOff && (! __context->OffSt)) || (__context->PHFOn && __context->OnSt)))) || (__context->OffSt && __context->OnSt)) && ((! __context->PPulse) || (((__context->POutOff && __context->PPulse) && (! __context->OutOnOV)) && (! __context->OutOffOV))))) {
			__context->PosW_aux = true;
			goto l217;
		}
		if ((! ((((__context->OutOVSt_aux && ((__context->PHFOn && (! __context->OnSt)) || (__context->PHFOff && __context->OffSt))) || ((! __context->OutOVSt_aux) && ((__context->PHFOff && (! __context->OffSt)) || (__context->PHFOn && __context->OnSt)))) || (__context->OffSt && __context->OnSt)) && ((! __context->PPulse) || (((__context->POutOff && __context->PPulse) && (! __context->OutOnOV)) && (! __context->OutOffOV)))))) {
			goto l217;
		}
		//assert(false);
		return;  			}
	l217: {
		if ((((((! (((__context->OutOVSt_aux && ((__context->PHFOn && (! __context->OnSt)) || (__context->PHFOff && __context->OffSt))) || ((! __context->OutOVSt_aux) && ((__context->PHFOff && (! __context->OffSt)) || (__context->PHFOn && __context->OnSt)))) || (__context->OffSt && __context->OnSt))) || __context->RE_OutOVSt_aux) || __context->FE_OutOVSt_aux) || ((__context->PPulse && __context->POutOff) && __context->OutOnOV)) || ((__context->PPulse && __context->POutOff) && __context->OutOffOV))) {
			__context->PosW_aux = false;
			goto l221;
		}
		if ((! (((((! (((__context->OutOVSt_aux && ((__context->PHFOn && (! __context->OnSt)) || (__context->PHFOff && __context->OffSt))) || ((! __context->OutOVSt_aux) && ((__context->PHFOff && (! __context->OffSt)) || (__context->PHFOn && __context->OnSt)))) || (__context->OffSt && __context->OnSt))) || __context->RE_OutOVSt_aux) || __context->FE_OutOVSt_aux) || ((__context->PPulse && __context->POutOff) && __context->OutOnOV)) || ((__context->PPulse && __context->POutOff) && __context->OutOffOV)))) {
			goto l221;
		}
		//assert(false);
		return;  			}
	l221: {
			// Assign inputs
			__context->Timer_Warning.IN = __context->PosW_aux;
			__context->Timer_Warning.PT = __context->POnOffb.PWDtb;
			TON(&__context->Timer_Warning);
			// Assign outputs
			goto l222;
		//assert(false);
		return;  			}
	l222: {
			__context->PosW = __context->Timer_Warning.Q;
			__context->Time_Warning = __context->Timer_Warning.ET;
			__context->AlBW = __context->AlB;
			__context->PulseWidth = (1500.0 / ((float) ((int32_t) ((int32_t) T_CYCLE))));
			goto l226;
		//assert(false);
		return;  			}
	l226: {
		if ((__context->FuStopISt || (__context->FSIinc > 0))) {
			__context->FSIinc = (__context->FSIinc + 1);
			__context->WFuStopISt = true;
			goto l231;
		}
		if ((! (__context->FuStopISt || (__context->FSIinc > 0)))) {
			goto l231;
		}
		//assert(false);
		return;  			}
	l231: {
		if (((((float) __context->FSIinc) > __context->PulseWidth) || ((! __context->FuStopISt) && (__context->FSIinc == 0)))) {
			__context->FSIinc = 0;
			__context->WFuStopISt = __context->FuStopISt;
			goto l236;
		}
		if ((! ((((float) __context->FSIinc) > __context->PulseWidth) || ((! __context->FuStopISt) && (__context->FSIinc == 0))))) {
			goto l236;
		}
		//assert(false);
		return;  			}
	l236: {
		if ((__context->TStopISt || (__context->TSIinc > 0))) {
			__context->TSIinc = (__context->TSIinc + 1);
			__context->WTStopISt = true;
			goto l241;
		}
		if ((! (__context->TStopISt || (__context->TSIinc > 0)))) {
			goto l241;
		}
		//assert(false);
		return;  			}
	l241: {
		if (((((float) __context->TSIinc) > __context->PulseWidth) || ((! __context->TStopISt) && (__context->TSIinc == 0)))) {
			__context->TSIinc = 0;
			__context->WTStopISt = __context->TStopISt;
			goto l246;
		}
		if ((! ((((float) __context->TSIinc) > __context->PulseWidth) || ((! __context->TStopISt) && (__context->TSIinc == 0))))) {
			goto l246;
		}
		//assert(false);
		return;  			}
	l246: {
		if ((__context->StartISt || (__context->SIinc > 0))) {
			__context->SIinc = (__context->SIinc + 1);
			__context->WStartISt = true;
			goto l251;
		}
		if ((! (__context->StartISt || (__context->SIinc > 0)))) {
			goto l251;
		}
		//assert(false);
		return;  			}
	l251: {
		if (((((float) __context->SIinc) > __context->PulseWidth) || ((! __context->StartISt) && (__context->SIinc == 0)))) {
			__context->SIinc = 0;
			__context->WStartISt = __context->StartISt;
			goto l256;
		}
		if ((! ((((float) __context->SIinc) > __context->PulseWidth) || ((! __context->StartISt) && (__context->SIinc == 0))))) {
			goto l256;
		}
		//assert(false);
		return;  			}
	l256: {
		if ((__context->AlSt || (__context->Alinc > 0))) {
			__context->Alinc = (__context->Alinc + 1);
			__context->WAlSt = true;
			goto l261;
		}
		if ((! (__context->AlSt || (__context->Alinc > 0)))) {
			goto l261;
		}
		//assert(false);
		return;  			}
	l261: {
		if (((((float) __context->Alinc) > __context->PulseWidth) || ((! __context->AlSt) && (__context->Alinc == 0)))) {
			__context->Alinc = 0;
			__context->WAlSt = __context->AlSt;
			goto l266;
		}
		if ((! ((((float) __context->Alinc) > __context->PulseWidth) || ((! __context->AlSt) && (__context->Alinc == 0))))) {
			goto l266;
		}
		//assert(false);
		return;  			}
	l266: {
			__context->Stsreg01b[8] = __context->OnSt;
			goto x1;
		//assert(false);
		return;  			}
	l267: {
			__context->Stsreg01b[9] = __context->OffSt;
			goto x2;
		//assert(false);
		return;  			}
	l268: {
			__context->Stsreg01b[10] = __context->AuMoSt;
			goto x3;
		//assert(false);
		return;  			}
	l269: {
			__context->Stsreg01b[11] = __context->MMoSt;
			goto x4;
		//assert(false);
		return;  			}
	l270: {
			__context->Stsreg01b[12] = __context->FoMoSt;
			goto x5;
		//assert(false);
		return;  			}
	l271: {
			__context->Stsreg01b[13] = __context->LDSt;
			goto x6;
		//assert(false);
		return;  			}
	l272: {
			__context->Stsreg01b[14] = __context->IOErrorW;
			goto x7;
		//assert(false);
		return;  			}
	l273: {
			__context->Stsreg01b[15] = __context->IOSimuW;
			goto x8;
		//assert(false);
		return;  			}
	l274: {
			__context->Stsreg01b[0] = __context->AuMRW;
			goto x9;
		//assert(false);
		return;  			}
	l275: {
			__context->Stsreg01b[1] = __context->PosW;
			goto x10;
		//assert(false);
		return;  			}
	l276: {
			__context->Stsreg01b[2] = __context->WStartISt;
			goto x11;
		//assert(false);
		return;  			}
	l277: {
			__context->Stsreg01b[3] = __context->WTStopISt;
			goto x12;
		//assert(false);
		return;  			}
	l278: {
			__context->Stsreg01b[4] = __context->AlUnAck;
			goto x13;
		//assert(false);
		return;  			}
	l279: {
			__context->Stsreg01b[5] = __context->AuIhFoMo;
			goto x14;
		//assert(false);
		return;  			}
	l280: {
			__context->Stsreg01b[6] = __context->WAlSt;
			goto x15;
		//assert(false);
		return;  			}
	l281: {
			__context->Stsreg01b[7] = __context->AuIhMMo;
			goto x16;
		//assert(false);
		return;  			}
	l282: {
			__context->Stsreg02b[8] = __context->OutOnOVSt;
			goto x17;
		//assert(false);
		return;  			}
	l283: {
			__context->Stsreg02b[9] = __context->AuOnRSt;
			goto x18;
		//assert(false);
		return;  			}
	l284: {
			__context->Stsreg02b[10] = __context->MOnRSt;
			goto x19;
		//assert(false);
		return;  			}
	l285: {
			__context->Stsreg02b[11] = __context->AuOffRSt;
			goto x20;
		//assert(false);
		return;  			}
	l286: {
			__context->Stsreg02b[12] = __context->MOffRSt;
			goto x21;
		//assert(false);
		return;  			}
	l287: {
			__context->Stsreg02b[13] = __context->HOnRSt;
			goto x22;
		//assert(false);
		return;  			}
	l288: {
			__context->Stsreg02b[14] = __context->HOffRSt;
			goto x23;
		//assert(false);
		return;  			}
	l289: {
			__context->Stsreg02b[15] = false;
			goto x24;
		//assert(false);
		return;  			}
	l290: {
			__context->Stsreg02b[0] = false;
			goto x25;
		//assert(false);
		return;  			}
	l291: {
			__context->Stsreg02b[1] = false;
			goto x26;
		//assert(false);
		return;  			}
	l292: {
			__context->Stsreg02b[2] = __context->WFuStopISt;
			goto x27;
		//assert(false);
		return;  			}
	l293: {
			__context->Stsreg02b[3] = __context->EnRstartSt;
			goto x28;
		//assert(false);
		return;  			}
	l294: {
			__context->Stsreg02b[4] = __context->SoftLDSt;
			goto x29;
		//assert(false);
		return;  			}
	l295: {
			__context->Stsreg02b[5] = __context->AlBW;
			goto x30;
		//assert(false);
		return;  			}
	l296: {
			__context->Stsreg02b[6] = __context->OutOffOVSt;
			goto x31;
		//assert(false);
		return;  			}
	l297: {
			__context->Stsreg02b[7] = false;
			goto x32;
		//assert(false);
		return;  			}
	l298: {
			// Assign inputs
			DETECT_EDGE1.new = __context->AlUnAck;
			DETECT_EDGE1.old = __context->AlUnAck_old;
			DETECT_EDGE(&DETECT_EDGE1);
			// Assign outputs
			__context->AlUnAck_old = DETECT_EDGE1.old;
			__context->RE_AlUnAck = DETECT_EDGE1.re;
			__context->FE_AlUnAck = DETECT_EDGE1.fe;
			goto l299;
		//assert(false);
		return;  			}
	l299: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	x: {
			// Assign inputs
			R_EDGE1_inlined_1.new = __context->Manreg01b[8];
			R_EDGE1_inlined_1.old = __context->MAuMoR_old;
			R_EDGE(&R_EDGE1_inlined_1);
			// Assign outputs
			__context->MAuMoR_old = R_EDGE1_inlined_1.old;
			__context->E_MAuMoR = R_EDGE1_inlined_1.RET_VAL;
			goto l1;
		//assert(false);
		return;  			}
	varview_refresh33: {
			__context->POnOff.PPulseLe = __context->POnOffb.PPulseLeb;
			goto varview_refresh65;
		//assert(false);
		return;  			}
	varview_refresh65: {
			__context->POnOff.PWDt = __context->POnOffb.PWDtb;
			__context->Stsreg01b[0] = ((__context->Stsreg01 & 256) != 0);
			__context->Stsreg01b[1] = ((__context->Stsreg01 & 512) != 0);
			__context->Stsreg01b[2] = ((__context->Stsreg01 & 1024) != 0);
			__context->Stsreg01b[3] = ((__context->Stsreg01 & 2048) != 0);
			__context->Stsreg01b[4] = ((__context->Stsreg01 & 4096) != 0);
			__context->Stsreg01b[5] = ((__context->Stsreg01 & 8192) != 0);
			__context->Stsreg01b[6] = ((__context->Stsreg01 & 16384) != 0);
			__context->Stsreg01b[7] = ((__context->Stsreg01 & 32768) != 0);
			__context->Stsreg01b[8] = ((__context->Stsreg01 & 1) != 0);
			__context->Stsreg01b[9] = ((__context->Stsreg01 & 2) != 0);
			__context->Stsreg01b[10] = ((__context->Stsreg01 & 4) != 0);
			__context->Stsreg01b[11] = ((__context->Stsreg01 & 8) != 0);
			__context->Stsreg01b[12] = ((__context->Stsreg01 & 16) != 0);
			__context->Stsreg01b[13] = ((__context->Stsreg01 & 32) != 0);
			__context->Stsreg01b[14] = ((__context->Stsreg01 & 64) != 0);
			__context->Stsreg01b[15] = ((__context->Stsreg01 & 128) != 0);
			__context->Stsreg02b[0] = ((__context->Stsreg02 & 256) != 0);
			__context->Stsreg02b[1] = ((__context->Stsreg02 & 512) != 0);
			__context->Stsreg02b[2] = ((__context->Stsreg02 & 1024) != 0);
			__context->Stsreg02b[3] = ((__context->Stsreg02 & 2048) != 0);
			__context->Stsreg02b[4] = ((__context->Stsreg02 & 4096) != 0);
			__context->Stsreg02b[5] = ((__context->Stsreg02 & 8192) != 0);
			__context->Stsreg02b[6] = ((__context->Stsreg02 & 16384) != 0);
			__context->Stsreg02b[7] = ((__context->Stsreg02 & 32768) != 0);
			__context->Stsreg02b[8] = ((__context->Stsreg02 & 1) != 0);
			__context->Stsreg02b[9] = ((__context->Stsreg02 & 2) != 0);
			__context->Stsreg02b[10] = ((__context->Stsreg02 & 4) != 0);
			__context->Stsreg02b[11] = ((__context->Stsreg02 & 8) != 0);
			__context->Stsreg02b[12] = ((__context->Stsreg02 & 16) != 0);
			__context->Stsreg02b[13] = ((__context->Stsreg02 & 32) != 0);
			__context->Stsreg02b[14] = ((__context->Stsreg02 & 64) != 0);
			__context->Stsreg02b[15] = ((__context->Stsreg02 & 128) != 0);
			goto x;
		//assert(false);
		return;  			}
	x1: {
		if (__context->Stsreg01b[8]) {
			__context->Stsreg01 = (__context->Stsreg01 | 1);
			goto l267;
		}
		if ((! __context->Stsreg01b[8])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65534);
			goto l267;
		}
		//assert(false);
		return;  			}
	x2: {
		if (__context->Stsreg01b[9]) {
			__context->Stsreg01 = (__context->Stsreg01 | 2);
			goto l268;
		}
		if ((! __context->Stsreg01b[9])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65533);
			goto l268;
		}
		//assert(false);
		return;  			}
	x3: {
		if (__context->Stsreg01b[10]) {
			__context->Stsreg01 = (__context->Stsreg01 | 4);
			goto l269;
		}
		if ((! __context->Stsreg01b[10])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65531);
			goto l269;
		}
		//assert(false);
		return;  			}
	x4: {
		if (__context->Stsreg01b[11]) {
			__context->Stsreg01 = (__context->Stsreg01 | 8);
			goto l270;
		}
		if ((! __context->Stsreg01b[11])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65527);
			goto l270;
		}
		//assert(false);
		return;  			}
	x5: {
		if (__context->Stsreg01b[12]) {
			__context->Stsreg01 = (__context->Stsreg01 | 16);
			goto l271;
		}
		if ((! __context->Stsreg01b[12])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65519);
			goto l271;
		}
		//assert(false);
		return;  			}
	x6: {
		if (__context->Stsreg01b[13]) {
			__context->Stsreg01 = (__context->Stsreg01 | 32);
			goto l272;
		}
		if ((! __context->Stsreg01b[13])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65503);
			goto l272;
		}
		//assert(false);
		return;  			}
	x7: {
		if (__context->Stsreg01b[14]) {
			__context->Stsreg01 = (__context->Stsreg01 | 64);
			goto l273;
		}
		if ((! __context->Stsreg01b[14])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65471);
			goto l273;
		}
		//assert(false);
		return;  			}
	x8: {
		if (__context->Stsreg01b[15]) {
			__context->Stsreg01 = (__context->Stsreg01 | 128);
			goto l274;
		}
		if ((! __context->Stsreg01b[15])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65407);
			goto l274;
		}
		//assert(false);
		return;  			}
	x9: {
		if (__context->Stsreg01b[0]) {
			__context->Stsreg01 = (__context->Stsreg01 | 256);
			goto l275;
		}
		if ((! __context->Stsreg01b[0])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65279);
			goto l275;
		}
		//assert(false);
		return;  			}
	x10: {
		if (__context->Stsreg01b[1]) {
			__context->Stsreg01 = (__context->Stsreg01 | 512);
			goto l276;
		}
		if ((! __context->Stsreg01b[1])) {
			__context->Stsreg01 = (__context->Stsreg01 & 65023);
			goto l276;
		}
		//assert(false);
		return;  			}
	x11: {
		if (__context->Stsreg01b[2]) {
			__context->Stsreg01 = (__context->Stsreg01 | 1024);
			goto l277;
		}
		if ((! __context->Stsreg01b[2])) {
			__context->Stsreg01 = (__context->Stsreg01 & 64511);
			goto l277;
		}
		//assert(false);
		return;  			}
	x12: {
		if (__context->Stsreg01b[3]) {
			__context->Stsreg01 = (__context->Stsreg01 | 2048);
			goto l278;
		}
		if ((! __context->Stsreg01b[3])) {
			__context->Stsreg01 = (__context->Stsreg01 & 63487);
			goto l278;
		}
		//assert(false);
		return;  			}
	x13: {
		if (__context->Stsreg01b[4]) {
			__context->Stsreg01 = (__context->Stsreg01 | 4096);
			goto l279;
		}
		if ((! __context->Stsreg01b[4])) {
			__context->Stsreg01 = (__context->Stsreg01 & 61439);
			goto l279;
		}
		//assert(false);
		return;  			}
	x14: {
		if (__context->Stsreg01b[5]) {
			__context->Stsreg01 = (__context->Stsreg01 | 8192);
			goto l280;
		}
		if ((! __context->Stsreg01b[5])) {
			__context->Stsreg01 = (__context->Stsreg01 & 57343);
			goto l280;
		}
		//assert(false);
		return;  			}
	x15: {
		if (__context->Stsreg01b[6]) {
			__context->Stsreg01 = (__context->Stsreg01 | 16384);
			goto l281;
		}
		if ((! __context->Stsreg01b[6])) {
			__context->Stsreg01 = (__context->Stsreg01 & 49151);
			goto l281;
		}
		//assert(false);
		return;  			}
	x16: {
		if (__context->Stsreg01b[7]) {
			__context->Stsreg01 = (__context->Stsreg01 | 32768);
			goto l282;
		}
		if ((! __context->Stsreg01b[7])) {
			__context->Stsreg01 = (__context->Stsreg01 & 32767);
			goto l282;
		}
		//assert(false);
		return;  			}
	x17: {
		if (__context->Stsreg02b[8]) {
			__context->Stsreg02 = (__context->Stsreg02 | 1);
			goto l283;
		}
		if ((! __context->Stsreg02b[8])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65534);
			goto l283;
		}
		//assert(false);
		return;  			}
	x18: {
		if (__context->Stsreg02b[9]) {
			__context->Stsreg02 = (__context->Stsreg02 | 2);
			goto l284;
		}
		if ((! __context->Stsreg02b[9])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65533);
			goto l284;
		}
		//assert(false);
		return;  			}
	x19: {
		if (__context->Stsreg02b[10]) {
			__context->Stsreg02 = (__context->Stsreg02 | 4);
			goto l285;
		}
		if ((! __context->Stsreg02b[10])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65531);
			goto l285;
		}
		//assert(false);
		return;  			}
	x20: {
		if (__context->Stsreg02b[11]) {
			__context->Stsreg02 = (__context->Stsreg02 | 8);
			goto l286;
		}
		if ((! __context->Stsreg02b[11])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65527);
			goto l286;
		}
		//assert(false);
		return;  			}
	x21: {
		if (__context->Stsreg02b[12]) {
			__context->Stsreg02 = (__context->Stsreg02 | 16);
			goto l287;
		}
		if ((! __context->Stsreg02b[12])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65519);
			goto l287;
		}
		//assert(false);
		return;  			}
	x22: {
		if (__context->Stsreg02b[13]) {
			__context->Stsreg02 = (__context->Stsreg02 | 32);
			goto l288;
		}
		if ((! __context->Stsreg02b[13])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65503);
			goto l288;
		}
		//assert(false);
		return;  			}
	x23: {
		if (__context->Stsreg02b[14]) {
			__context->Stsreg02 = (__context->Stsreg02 | 64);
			goto l289;
		}
		if ((! __context->Stsreg02b[14])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65471);
			goto l289;
		}
		//assert(false);
		return;  			}
	x24: {
		if (__context->Stsreg02b[15]) {
			__context->Stsreg02 = (__context->Stsreg02 | 128);
			goto l290;
		}
		if ((! __context->Stsreg02b[15])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65407);
			goto l290;
		}
		//assert(false);
		return;  			}
	x25: {
		if (__context->Stsreg02b[0]) {
			__context->Stsreg02 = (__context->Stsreg02 | 256);
			goto l291;
		}
		if ((! __context->Stsreg02b[0])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65279);
			goto l291;
		}
		//assert(false);
		return;  			}
	x26: {
		if (__context->Stsreg02b[1]) {
			__context->Stsreg02 = (__context->Stsreg02 | 512);
			goto l292;
		}
		if ((! __context->Stsreg02b[1])) {
			__context->Stsreg02 = (__context->Stsreg02 & 65023);
			goto l292;
		}
		//assert(false);
		return;  			}
	x27: {
		if (__context->Stsreg02b[2]) {
			__context->Stsreg02 = (__context->Stsreg02 | 1024);
			goto l293;
		}
		if ((! __context->Stsreg02b[2])) {
			__context->Stsreg02 = (__context->Stsreg02 & 64511);
			goto l293;
		}
		//assert(false);
		return;  			}
	x28: {
		if (__context->Stsreg02b[3]) {
			__context->Stsreg02 = (__context->Stsreg02 | 2048);
			goto l294;
		}
		if ((! __context->Stsreg02b[3])) {
			__context->Stsreg02 = (__context->Stsreg02 & 63487);
			goto l294;
		}
		//assert(false);
		return;  			}
	x29: {
		if (__context->Stsreg02b[4]) {
			__context->Stsreg02 = (__context->Stsreg02 | 4096);
			goto l295;
		}
		if ((! __context->Stsreg02b[4])) {
			__context->Stsreg02 = (__context->Stsreg02 & 61439);
			goto l295;
		}
		//assert(false);
		return;  			}
	x30: {
		if (__context->Stsreg02b[5]) {
			__context->Stsreg02 = (__context->Stsreg02 | 8192);
			goto l296;
		}
		if ((! __context->Stsreg02b[5])) {
			__context->Stsreg02 = (__context->Stsreg02 & 57343);
			goto l296;
		}
		//assert(false);
		return;  			}
	x31: {
		if (__context->Stsreg02b[6]) {
			__context->Stsreg02 = (__context->Stsreg02 | 16384);
			goto l297;
		}
		if ((! __context->Stsreg02b[6])) {
			__context->Stsreg02 = (__context->Stsreg02 & 49151);
			goto l297;
		}
		//assert(false);
		return;  			}
	x32: {
		if (__context->Stsreg02b[7]) {
			__context->Stsreg02 = (__context->Stsreg02 | 32768);
			goto l298;
		}
		if ((! __context->Stsreg02b[7])) {
			__context->Stsreg02 = (__context->Stsreg02 & 32767);
			goto l298;
		}
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void TP(__TP *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
		if (((__context->IN && (! __context->old_in)) && (! __context->Q))) {
			__context->due = (__GLOBAL_TIME + __context->PT);
			goto l41;
		}
		if ((! ((__context->IN && (! __context->old_in)) && (! __context->Q)))) {
			goto l41;
		}
		//assert(false);
		return;  			}
	l41: {
		if ((__GLOBAL_TIME <= __context->due)) {
			__context->Q = true;
			__context->ET = (__context->PT - (__context->due - __GLOBAL_TIME));
			goto l15;
		}
		if ((! (__GLOBAL_TIME <= __context->due))) {
			__context->Q = false;
			goto l9;
		}
		//assert(false);
		return;  			}
	l9: {
		if (__context->IN) {
			__context->ET = __context->PT;
			goto l14;
		}
		if ((! __context->IN)) {
			__context->ET = 0;
			goto l14;
		}
		//assert(false);
		return;  			}
	l14: {
			goto l15;
		//assert(false);
		return;  			}
	l15: {
			__context->old_in = __context->IN;
			goto l16;
		//assert(false);
		return;  			}
	l16: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void TON(__TON *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init2;
	init2: {
		if ((__context->IN == false)) {
			__context->Q = false;
			__context->ET = 0;
			__context->running = false;
			goto l252;
		}
		if ((! (__context->IN == false))) {
			goto l52;
		}
		//assert(false);
		return;  			}
	l52: {
		if ((__context->running == false)) {
			__context->start = __GLOBAL_TIME;
			__context->running = true;
			__context->ET = 0;
			goto l92;
		}
		if ((! (__context->running == false))) {
			goto l145;
		}
		//assert(false);
		return;  			}
	l92: {
		if ((__context->PT == 0)) {
			__context->Q = true;
			goto l13;
		}
		if ((! (__context->PT == 0))) {
			goto l13;
		}
		//assert(false);
		return;  			}
	l13: {
			goto l242;
		//assert(false);
		return;  			}
	l145: {
		if ((! ((__GLOBAL_TIME - (__context->start + __context->PT)) >= 0))) {
			goto l151;
		}
		if ((! (! ((__GLOBAL_TIME - (__context->start + __context->PT)) >= 0)))) {
			__context->Q = true;
			__context->ET = __context->PT;
			goto l232;
		}
		//assert(false);
		return;  			}
	l151: {
		if ((! __context->Q)) {
			__context->ET = (__GLOBAL_TIME - __context->start);
			goto l19;
		}
		if ((! (! __context->Q))) {
			goto l19;
		}
		//assert(false);
		return;  			}
	l19: {
			goto l232;
		//assert(false);
		return;  			}
	l232: {
			goto l242;
		//assert(false);
		return;  			}
	l242: {
			goto l252;
		//assert(false);
		return;  			}
	l252: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void R_EDGE(__R_EDGE *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init3;
	init3: {
		if (((__context->new == true) && (__context->old == false))) {
			__context->RET_VAL = true;
			__context->old = true;
			goto l71;
		}
		if ((! ((__context->new == true) && (__context->old == false)))) {
			__context->RET_VAL = false;
			__context->old = __context->new;
			goto l71;
		}
		//assert(false);
		return;  			}
	l71: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void F_EDGE(__F_EDGE *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init4;
	init4: {
		if (((__context->new == false) && (__context->old == true))) {
			__context->RET_VAL = true;
			__context->old = false;
			goto l73;
		}
		if ((! ((__context->new == false) && (__context->old == true)))) {
			__context->RET_VAL = false;
			__context->old = __context->new;
			goto l73;
		}
		//assert(false);
		return;  			}
	l73: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void DETECT_EDGE(__DETECT_EDGE *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init5;
	init5: {
		if ((__context->new != __context->old)) {
			goto l11;
		}
		if ((! (__context->new != __context->old))) {
			__context->re = false;
			__context->fe = false;
			goto l131;
		}
		//assert(false);
		return;  			}
	l11: {
		if ((__context->new == true)) {
			__context->re = true;
			__context->fe = false;
			goto l81;
		}
		if ((! (__context->new == true))) {
			__context->re = false;
			__context->fe = true;
			goto l81;
		}
		//assert(false);
		return;  			}
	l81: {
			__context->old = __context->new;
			goto l131;
		//assert(false);
		return;  			}
	l131: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init6;
	init6: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			T_CYCLE = nondet_uint8_t();
			instance.Al = nondet_bool();
			instance.AlB = nondet_bool();
			instance.AuAlAck = nondet_bool();
			instance.AuAuMoR = nondet_bool();
			instance.AuIhFoMo = nondet_bool();
			instance.AuIhMMo = nondet_bool();
			instance.AuOffR = nondet_bool();
			instance.AuOnR = nondet_bool();
			instance.AuRstart = nondet_bool();
			instance.FuStopI = nondet_bool();
			instance.HFOff = nondet_bool();
			instance.HFOn = nondet_bool();
			instance.HLD = nondet_bool();
			instance.HOffR = nondet_bool();
			instance.HOnR = nondet_bool();
			instance.IOError = nondet_bool();
			instance.IOSimu = nondet_bool();
			instance.IhAuMRW = nondet_bool();
			instance.Manreg01 = nondet_uint16_t();
			instance.Manreg01b[0] = nondet_bool();
			instance.Manreg01b[10] = nondet_bool();
			instance.Manreg01b[11] = nondet_bool();
			instance.Manreg01b[12] = nondet_bool();
			instance.Manreg01b[13] = nondet_bool();
			instance.Manreg01b[14] = nondet_bool();
			instance.Manreg01b[15] = nondet_bool();
			instance.Manreg01b[1] = nondet_bool();
			instance.Manreg01b[2] = nondet_bool();
			instance.Manreg01b[3] = nondet_bool();
			instance.Manreg01b[4] = nondet_bool();
			instance.Manreg01b[5] = nondet_bool();
			instance.Manreg01b[6] = nondet_bool();
			instance.Manreg01b[7] = nondet_bool();
			instance.Manreg01b[8] = nondet_bool();
			instance.Manreg01b[9] = nondet_bool();
			instance.POnOff.PPulseLe = nondet_int32_t();
			instance.POnOff.PWDt = nondet_int32_t();
			instance.POnOff.ParReg = nondet_uint16_t();
			instance.POnOffb.PPulseLeb = nondet_int32_t();
			instance.POnOffb.PWDtb = nondet_int32_t();
			instance.POnOffb.ParRegb[0] = nondet_bool();
			instance.POnOffb.ParRegb[10] = nondet_bool();
			instance.POnOffb.ParRegb[11] = nondet_bool();
			instance.POnOffb.ParRegb[12] = nondet_bool();
			instance.POnOffb.ParRegb[13] = nondet_bool();
			instance.POnOffb.ParRegb[14] = nondet_bool();
			instance.POnOffb.ParRegb[15] = nondet_bool();
			instance.POnOffb.ParRegb[1] = nondet_bool();
			instance.POnOffb.ParRegb[2] = nondet_bool();
			instance.POnOffb.ParRegb[3] = nondet_bool();
			instance.POnOffb.ParRegb[4] = nondet_bool();
			instance.POnOffb.ParRegb[5] = nondet_bool();
			instance.POnOffb.ParRegb[6] = nondet_bool();
			instance.POnOffb.ParRegb[7] = nondet_bool();
			instance.POnOffb.ParRegb[8] = nondet_bool();
			instance.POnOffb.ParRegb[9] = nondet_bool();
			instance.StartI = nondet_bool();
			instance.TStopI = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			CPC_FB_ONOFF(&instance);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			__GLOBAL_TIME = (__GLOBAL_TIME + ((int32_t) T_CYCLE));
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
void main() {
	// Initial values
	T_CYCLE = 0;
	__GLOBAL_TIME = 0;
	R_EDGE1.new = false;
	R_EDGE1.old = false;
	R_EDGE1.RET_VAL = false;
	F_EDGE1.new = false;
	F_EDGE1.old = false;
	F_EDGE1.RET_VAL = false;
	DETECT_EDGE1.new = false;
	DETECT_EDGE1.old = false;
	DETECT_EDGE1.re = false;
	DETECT_EDGE1.fe = false;
	instance.HFOn = false;
	instance.HFOff = false;
	instance.HLD = false;
	instance.IOError = false;
	instance.IOSimu = false;
	instance.AlB = false;
	instance.Manreg01 = 0;
	instance.Manreg01b[0] = false;
	instance.Manreg01b[1] = false;
	instance.Manreg01b[2] = false;
	instance.Manreg01b[3] = false;
	instance.Manreg01b[4] = false;
	instance.Manreg01b[5] = false;
	instance.Manreg01b[6] = false;
	instance.Manreg01b[7] = false;
	instance.Manreg01b[8] = false;
	instance.Manreg01b[9] = false;
	instance.Manreg01b[10] = false;
	instance.Manreg01b[11] = false;
	instance.Manreg01b[12] = false;
	instance.Manreg01b[13] = false;
	instance.Manreg01b[14] = false;
	instance.Manreg01b[15] = false;
	instance.HOnR = false;
	instance.HOffR = false;
	instance.StartI = false;
	instance.TStopI = false;
	instance.FuStopI = false;
	instance.Al = false;
	instance.AuOnR = false;
	instance.AuOffR = false;
	instance.AuAuMoR = false;
	instance.AuIhMMo = false;
	instance.AuIhFoMo = false;
	instance.AuAlAck = false;
	instance.IhAuMRW = false;
	instance.AuRstart = false;
	instance.POnOff.ParReg = 0;
	instance.POnOff.PPulseLe = 0;
	instance.POnOff.PWDt = 0;
	instance.POnOffb.ParRegb[0] = false;
	instance.POnOffb.ParRegb[1] = false;
	instance.POnOffb.ParRegb[2] = false;
	instance.POnOffb.ParRegb[3] = false;
	instance.POnOffb.ParRegb[4] = false;
	instance.POnOffb.ParRegb[5] = false;
	instance.POnOffb.ParRegb[6] = false;
	instance.POnOffb.ParRegb[7] = false;
	instance.POnOffb.ParRegb[8] = false;
	instance.POnOffb.ParRegb[9] = false;
	instance.POnOffb.ParRegb[10] = false;
	instance.POnOffb.ParRegb[11] = false;
	instance.POnOffb.ParRegb[12] = false;
	instance.POnOffb.ParRegb[13] = false;
	instance.POnOffb.ParRegb[14] = false;
	instance.POnOffb.ParRegb[15] = false;
	instance.POnOffb.PPulseLeb = 0;
	instance.POnOffb.PWDtb = 0;
	instance.Stsreg01 = 0;
	instance.Stsreg01b[0] = false;
	instance.Stsreg01b[1] = false;
	instance.Stsreg01b[2] = false;
	instance.Stsreg01b[3] = false;
	instance.Stsreg01b[4] = false;
	instance.Stsreg01b[5] = false;
	instance.Stsreg01b[6] = false;
	instance.Stsreg01b[7] = false;
	instance.Stsreg01b[8] = false;
	instance.Stsreg01b[9] = false;
	instance.Stsreg01b[10] = false;
	instance.Stsreg01b[11] = false;
	instance.Stsreg01b[12] = false;
	instance.Stsreg01b[13] = false;
	instance.Stsreg01b[14] = false;
	instance.Stsreg01b[15] = false;
	instance.Stsreg02 = 0;
	instance.Stsreg02b[0] = false;
	instance.Stsreg02b[1] = false;
	instance.Stsreg02b[2] = false;
	instance.Stsreg02b[3] = false;
	instance.Stsreg02b[4] = false;
	instance.Stsreg02b[5] = false;
	instance.Stsreg02b[6] = false;
	instance.Stsreg02b[7] = false;
	instance.Stsreg02b[8] = false;
	instance.Stsreg02b[9] = false;
	instance.Stsreg02b[10] = false;
	instance.Stsreg02b[11] = false;
	instance.Stsreg02b[12] = false;
	instance.Stsreg02b[13] = false;
	instance.Stsreg02b[14] = false;
	instance.Stsreg02b[15] = false;
	instance.OutOnOV = false;
	instance.OutOffOV = false;
	instance.OnSt = false;
	instance.OffSt = false;
	instance.AuMoSt = false;
	instance.MMoSt = false;
	instance.LDSt = false;
	instance.SoftLDSt = false;
	instance.FoMoSt = false;
	instance.AuOnRSt = false;
	instance.AuOffRSt = false;
	instance.MOnRSt = false;
	instance.MOffRSt = false;
	instance.HOnRSt = false;
	instance.HOffRSt = false;
	instance.IOErrorW = false;
	instance.IOSimuW = false;
	instance.AuMRW = false;
	instance.AlUnAck = false;
	instance.PosW = false;
	instance.StartISt = false;
	instance.TStopISt = false;
	instance.FuStopISt = false;
	instance.AlSt = false;
	instance.AlBW = false;
	instance.EnRstartSt = true;
	instance.RdyStartSt = false;
	instance.E_MAuMoR = false;
	instance.E_MMMoR = false;
	instance.E_MFoMoR = false;
	instance.E_MOnR = false;
	instance.E_MOffR = false;
	instance.E_MAlAckR = false;
	instance.E_StartI = false;
	instance.E_TStopI = false;
	instance.E_FuStopI = false;
	instance.E_Al = false;
	instance.E_AuAuMoR = false;
	instance.E_AuAlAckR = false;
	instance.E_MSoftLDR = false;
	instance.E_MEnRstartR = false;
	instance.RE_AlUnAck = false;
	instance.FE_AlUnAck = false;
	instance.RE_PulseOn = false;
	instance.FE_PulseOn = false;
	instance.RE_PulseOff = false;
	instance.RE_OutOVSt_aux = false;
	instance.FE_OutOVSt_aux = false;
	instance.FE_InterlockR = false;
	instance.MAuMoR_old = false;
	instance.MMMoR_old = false;
	instance.MFoMoR_old = false;
	instance.MOnR_old = false;
	instance.MOffR_old = false;
	instance.MAlAckR_old = false;
	instance.AuAuMoR_old = false;
	instance.AuAlAckR_old = false;
	instance.StartI_old = false;
	instance.TStopI_old = false;
	instance.FuStopI_old = false;
	instance.Al_old = false;
	instance.AlUnAck_old = false;
	instance.MSoftLDR_old = false;
	instance.MEnRstartR_old = false;
	instance.RE_PulseOn_old = false;
	instance.FE_PulseOn_old = false;
	instance.RE_PulseOff_old = false;
	instance.RE_OutOVSt_aux_old = false;
	instance.FE_OutOVSt_aux_old = false;
	instance.FE_InterlockR_old = false;
	instance.PFsPosOn = false;
	instance.PFsPosOn2 = false;
	instance.PHFOn = false;
	instance.PHFOff = false;
	instance.PPulse = false;
	instance.PPulseCste = false;
	instance.PHLD = false;
	instance.PHLDCmd = false;
	instance.PAnim = false;
	instance.POutOff = false;
	instance.PEnRstart = false;
	instance.PRstartFS = false;
	instance.OutOnOVSt = false;
	instance.OutOffOVSt = false;
	instance.AuMoSt_aux = false;
	instance.MMoSt_aux = false;
	instance.FoMoSt_aux = false;
	instance.SoftLDSt_aux = false;
	instance.PulseOn = false;
	instance.PulseOff = false;
	instance.PosW_aux = false;
	instance.OutOVSt_aux = false;
	instance.fullNotAcknowledged = false;
	instance.PulseOnR = false;
	instance.PulseOffR = false;
	instance.InterlockR = false;
	instance.Time_Warning = 0;
	instance.Timer_PulseOn.PT = 0;
	instance.Timer_PulseOn.IN = false;
	instance.Timer_PulseOn.Q = false;
	instance.Timer_PulseOn.ET = 0;
	instance.Timer_PulseOn.old_in = false;
	instance.Timer_PulseOn.due = 0;
	instance.Timer_PulseOff.PT = 0;
	instance.Timer_PulseOff.IN = false;
	instance.Timer_PulseOff.Q = false;
	instance.Timer_PulseOff.ET = 0;
	instance.Timer_PulseOff.old_in = false;
	instance.Timer_PulseOff.due = 0;
	instance.Timer_Warning.PT = 0;
	instance.Timer_Warning.IN = false;
	instance.Timer_Warning.Q = false;
	instance.Timer_Warning.ET = 0;
	instance.Timer_Warning.running = false;
	instance.Timer_Warning.start = 0;
	instance.PulseWidth = 0.0;
	instance.FSIinc = 0;
	instance.TSIinc = 0;
	instance.SIinc = 0;
	instance.Alinc = 0;
	instance.WTStopISt = false;
	instance.WStartISt = false;
	instance.WAlSt = false;
	instance.WFuStopISt = false;
	R_EDGE1_inlined_1.new = false;
	R_EDGE1_inlined_1.old = false;
	R_EDGE1_inlined_1.RET_VAL = false;
	R_EDGE1_inlined_2.new = false;
	R_EDGE1_inlined_2.old = false;
	R_EDGE1_inlined_2.RET_VAL = false;
	R_EDGE1_inlined_3.new = false;
	R_EDGE1_inlined_3.old = false;
	R_EDGE1_inlined_3.RET_VAL = false;
	R_EDGE1_inlined_4.new = false;
	R_EDGE1_inlined_4.old = false;
	R_EDGE1_inlined_4.RET_VAL = false;
	R_EDGE1_inlined_5.new = false;
	R_EDGE1_inlined_5.old = false;
	R_EDGE1_inlined_5.RET_VAL = false;
	R_EDGE1_inlined_6.new = false;
	R_EDGE1_inlined_6.old = false;
	R_EDGE1_inlined_6.RET_VAL = false;
	R_EDGE1_inlined_7.new = false;
	R_EDGE1_inlined_7.old = false;
	R_EDGE1_inlined_7.RET_VAL = false;
	R_EDGE1_inlined_8.new = false;
	R_EDGE1_inlined_8.old = false;
	R_EDGE1_inlined_8.RET_VAL = false;
	R_EDGE1_inlined_9.new = false;
	R_EDGE1_inlined_9.old = false;
	R_EDGE1_inlined_9.RET_VAL = false;
	R_EDGE1_inlined_10.new = false;
	R_EDGE1_inlined_10.old = false;
	R_EDGE1_inlined_10.RET_VAL = false;
	R_EDGE1_inlined_11.new = false;
	R_EDGE1_inlined_11.old = false;
	R_EDGE1_inlined_11.RET_VAL = false;
	R_EDGE1_inlined_12.new = false;
	R_EDGE1_inlined_12.old = false;
	R_EDGE1_inlined_12.RET_VAL = false;
	R_EDGE1_inlined_13.new = false;
	R_EDGE1_inlined_13.old = false;
	R_EDGE1_inlined_13.RET_VAL = false;
	R_EDGE1_inlined_14.new = false;
	R_EDGE1_inlined_14.old = false;
	R_EDGE1_inlined_14.RET_VAL = false;
	F_EDGE1_inlined_15.new = false;
	F_EDGE1_inlined_15.old = false;
	F_EDGE1_inlined_15.RET_VAL = false;
	R_EDGE1_inlined_16.new = false;
	R_EDGE1_inlined_16.old = false;
	R_EDGE1_inlined_16.RET_VAL = false;
	F_EDGE1_inlined_17.new = false;
	F_EDGE1_inlined_17.old = false;
	F_EDGE1_inlined_17.RET_VAL = false;
	R_EDGE1_inlined_18.new = false;
	R_EDGE1_inlined_18.old = false;
	R_EDGE1_inlined_18.RET_VAL = false;
	R_EDGE1_inlined_19.new = false;
	R_EDGE1_inlined_19.old = false;
	R_EDGE1_inlined_19.RET_VAL = false;
	F_EDGE1_inlined_20.new = false;
	F_EDGE1_inlined_20.old = false;
	F_EDGE1_inlined_20.RET_VAL = false;
	__assertion_error = 0;
	
	VerificationLoop();
}
