#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Variables
bool QX0_0 = false;
bool QX0_1 = false;
bool QX0_2 = false;
bool QX0_3 = false;
bool QX0_4 = false;
bool simpletest_db1_a = false;
bool simpletest_db1_b = false;
bool simpletest_db1_c = false;
uint16_t __assertion_error = 0;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			simpletest_db1_a = nondet_bool();
			simpletest_db1_b = nondet_bool();
			simpletest_db1_c = nondet_bool();
			goto prepare_BoC;
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			QX0_0 = ((simpletest_db1_a || simpletest_db1_b) || simpletest_db1_c);
			goto verificationLoop_VerificationLoop_l17;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l17: {
		if ((! (! (QX0_0 == ((simpletest_db1_a || simpletest_db1_b) || simpletest_db1_c))))) {
			QX0_1 = ((simpletest_db1_a && simpletest_db1_b) && simpletest_db1_c);
			goto verificationLoop_VerificationLoop_l30;
		}
		if ((! (QX0_0 == ((simpletest_db1_a || simpletest_db1_b) || simpletest_db1_c)))) {
			__assertion_error = 1;
			goto verificationLoop_VerificationLoop_l70;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l30: {
		if ((! (! (QX0_1 == ((simpletest_db1_a && simpletest_db1_b) && simpletest_db1_c))))) {
			QX0_2 = true;
			goto verificationLoop_VerificationLoop_l35;
		}
		if ((! (QX0_1 == ((simpletest_db1_a && simpletest_db1_b) && simpletest_db1_c)))) {
			__assertion_error = 2;
			goto verificationLoop_VerificationLoop_l70;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l35: {
		if ((! (! QX0_2))) {
			QX0_3 = false;
			goto verificationLoop_VerificationLoop_l40;
		}
		if ((! QX0_2)) {
			__assertion_error = 3;
			goto verificationLoop_VerificationLoop_l70;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l40: {
		if ((! (! (QX0_3 == false)))) {
			QX0_4 = (! ((simpletest_db1_a && simpletest_db1_b) || simpletest_db1_c));
			goto verificationLoop_VerificationLoop_l55;
		}
		if ((! (QX0_3 == false))) {
			__assertion_error = 4;
			goto verificationLoop_VerificationLoop_l70;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l55: {
		if ((! (! (QX0_4 == (! ((simpletest_db1_a && simpletest_db1_b) || simpletest_db1_c)))))) {
			goto verificationLoop_VerificationLoop_l70;
		}
		if ((! (QX0_4 == (! ((simpletest_db1_a && simpletest_db1_b) || simpletest_db1_c))))) {
			__assertion_error = 5;
			goto verificationLoop_VerificationLoop_l70;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_l70: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
