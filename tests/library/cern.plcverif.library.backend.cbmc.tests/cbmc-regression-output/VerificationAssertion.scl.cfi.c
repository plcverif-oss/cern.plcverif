#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Variables
bool IX1_2 = false;
bool IX2_2 = false;
bool IX2_3 = false;
bool IX3_2 = false;
bool IX3_3 = false;
bool IX3_4 = false;
bool IX4_2 = false;
bool IX4_3 = false;
bool IX4_4 = false;
uint16_t __assertion_error = 0;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Main
void main() {
	// Start with initial location
	goto init;
	init: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			IX1_2 = nondet_bool();
			IX2_2 = nondet_bool();
			IX2_3 = nondet_bool();
			IX3_2 = nondet_bool();
			IX3_3 = nondet_bool();
			IX3_4 = nondet_bool();
			IX4_2 = nondet_bool();
			IX4_3 = nondet_bool();
			IX4_4 = nondet_bool();
			goto prepare_BoC;
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto verificationLoop_VerificationLoop_assertFunction_OB1_l1;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_assertFunction_OB1_l1: {
		if ((! (! IX1_2))) {
			goto verificationLoop_VerificationLoop_assertFunction_OB1_l2;
		}
		if ((! IX1_2)) {
			__assertion_error = 1;
			goto verificationLoop_VerificationLoop_assertFunction_OB1_l2;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_assertFunction_OB1_l2: {
			goto verificationLoop_VerificationLoop_assertFunction_OB1_l11;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_assertFunction_OB1_l11: {
		if ((! (! (IX2_2 || IX2_3)))) {
			goto verificationLoop_VerificationLoop_assertFunction_OB1_l21;
		}
		if ((! (IX2_2 || IX2_3))) {
			__assertion_error = 2;
			goto verificationLoop_VerificationLoop_assertFunction_OB1_l21;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_assertFunction_OB1_l21: {
			goto verificationLoop_VerificationLoop_assertFunction_OB1_l12;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_assertFunction_OB1_l12: {
		if ((! (! ((IX3_2 || IX3_3) && IX3_4)))) {
			goto verificationLoop_VerificationLoop_assertFunction_OB1_l22;
		}
		if ((! ((IX3_2 || IX3_3) && IX3_4))) {
			__assertion_error = 3;
			goto verificationLoop_VerificationLoop_assertFunction_OB1_l22;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_assertFunction_OB1_l22: {
			goto verificationLoop_VerificationLoop_assertFunction_OB1_l13;
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_assertFunction_OB1_l13: {
		if ((! (! (!(IX4_2) || (!(IX4_3) || IX4_4))))) {
			goto verificationLoop_VerificationLoop_assertFunction_OB1_l23;
		}
		if ((! (!(IX4_2) || (!(IX4_3) || IX4_4)))) {
			__assertion_error = 4;
			goto verificationLoop_VerificationLoop_assertFunction_OB1_l23;
		}
		//assert(false);
		return;  			}
	verificationLoop_VerificationLoop_assertFunction_OB1_l23: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
