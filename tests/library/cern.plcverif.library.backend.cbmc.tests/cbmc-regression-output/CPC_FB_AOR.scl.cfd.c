#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Root data structure
typedef struct {
	float in;
	float RET_VAL;
} __ABS_REAL;
typedef struct {
	int16_t FEType;
	int16_t index;
	int16_t DBnum;
	int16_t DBpos;
	int16_t DBnumIoError;
	int16_t DBposIoError;
	int16_t DBbitIoError;
	float AuPosR;
	float PMinRan;
	float PMaxRan;
	float PMinRaw;
	float PMaxRaw;
	float PosSt;
	float OutOV;
	float MPosRSt;
	float AuPosRSt;
	bool MNewMR_old;
	bool AuIhFoMo;
	bool AuMoSt;
	bool FoMoSt;
	bool IOErrorW;
	bool IOSimuW;
	bool IOError;
	bool IOSimu;
	bool AuMRW;
	bool MIOErBRSt;
	bool MIOErBSetRst_old;
	bool PQWDef;
} __CPC_DB_AOR;
typedef struct {
	uint16_t Manreg01;
	bool Manreg01b[16];
	float MPosR;
	uint16_t StsReg01;
	__CPC_DB_AOR Perst;
} __CPC_FB_AOR;
typedef struct {
	bool new;
	bool old;
	bool RET_VAL;
} __R_EDGE;

// Global variables
__R_EDGE R_EDGE1;
__ABS_REAL ABS#REAL1;
__CPC_FB_AOR instance;
__ABS_REAL ABS#REAL1_inlined_1;
__ABS_REAL ABS#REAL1_inlined_2;
uint16_t __assertion_error;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Automata declarations
void CPC_FB_AOR(__CPC_FB_AOR *__context);
void R_EDGE(__R_EDGE *__context);
void ABS_REAL(__ABS_REAL *__context);
void VerificationLoop();

// Automata
void CPC_FB_AOR(__CPC_FB_AOR *__context) {
	// Temporary variables
	float Outovold;
	bool E_MIOErBSetRst;
	uint16_t TempStsReg01;
	bool StsReg01b[16];
	float ___nested_ret_val1;
	float ___nested_ret_val2;
	float ___nested_ret_val3;
	
	// Start with initial location
	goto init;
	init: {
			__context->Manreg01b[0] = ((__context->Manreg01 & 256) != 0);
			__context->Manreg01b[1] = ((__context->Manreg01 & 512) != 0);
			__context->Manreg01b[2] = ((__context->Manreg01 & 1024) != 0);
			__context->Manreg01b[3] = ((__context->Manreg01 & 2048) != 0);
			__context->Manreg01b[4] = ((__context->Manreg01 & 4096) != 0);
			__context->Manreg01b[5] = ((__context->Manreg01 & 8192) != 0);
			__context->Manreg01b[6] = ((__context->Manreg01 & 16384) != 0);
			__context->Manreg01b[7] = ((__context->Manreg01 & 32768) != 0);
			__context->Manreg01b[8] = ((__context->Manreg01 & 1) != 0);
			__context->Manreg01b[9] = ((__context->Manreg01 & 2) != 0);
			__context->Manreg01b[10] = ((__context->Manreg01 & 4) != 0);
			__context->Manreg01b[11] = ((__context->Manreg01 & 8) != 0);
			__context->Manreg01b[12] = ((__context->Manreg01 & 16) != 0);
			__context->Manreg01b[13] = ((__context->Manreg01 & 32) != 0);
			__context->Manreg01b[14] = ((__context->Manreg01 & 64) != 0);
			__context->Manreg01b[15] = ((__context->Manreg01 & 128) != 0);
			StsReg01b[0] = ((TempStsReg01 & 256) != 0);
			StsReg01b[1] = ((TempStsReg01 & 512) != 0);
			StsReg01b[2] = ((TempStsReg01 & 1024) != 0);
			StsReg01b[3] = ((TempStsReg01 & 2048) != 0);
			StsReg01b[4] = ((TempStsReg01 & 4096) != 0);
			StsReg01b[5] = ((TempStsReg01 & 8192) != 0);
			StsReg01b[6] = ((TempStsReg01 & 16384) != 0);
			StsReg01b[7] = ((TempStsReg01 & 32768) != 0);
			StsReg01b[8] = ((TempStsReg01 & 1) != 0);
			StsReg01b[9] = ((TempStsReg01 & 2) != 0);
			StsReg01b[10] = ((TempStsReg01 & 4) != 0);
			StsReg01b[11] = ((TempStsReg01 & 8) != 0);
			StsReg01b[12] = ((TempStsReg01 & 16) != 0);
			StsReg01b[13] = ((TempStsReg01 & 32) != 0);
			StsReg01b[14] = ((TempStsReg01 & 64) != 0);
			StsReg01b[15] = ((TempStsReg01 & 128) != 0);
			goto x;
		//assert(false);
		return;  			}
	l1: {
			TempStsReg01 = __context->StsReg01;
			goto x2;
		//assert(false);
		return;  			}
	l2: {
			// Assign inputs
			R_EDGE1.new = __context->Manreg01b[2];
			R_EDGE1.old = __context->Perst.MIOErBSetRst_old;
			R_EDGE(&R_EDGE1);
			// Assign outputs
			__context->Perst.MIOErBSetRst_old = R_EDGE1.old;
			E_MIOErBSetRst = R_EDGE1.RET_VAL;
			goto l3;
		//assert(false);
		return;  			}
	l3: {
		if ((__context->Manreg01b[8] || __context->Perst.AuIhFoMo)) {
			__context->Perst.FoMoSt = false;
			goto l7;
		}
		if ((! (__context->Manreg01b[8] || __context->Perst.AuIhFoMo))) {
			goto l7;
		}
		//assert(false);
		return;  			}
	l7: {
		if ((__context->Manreg01b[10] && (! __context->Perst.AuIhFoMo))) {
			__context->Perst.FoMoSt = true;
			goto l11;
		}
		if ((! (__context->Manreg01b[10] && (! __context->Perst.AuIhFoMo)))) {
			goto l11;
		}
		//assert(false);
		return;  			}
	l11: {
			__context->Perst.AuMoSt = (! __context->Perst.FoMoSt);
			goto l12;
		//assert(false);
		return;  			}
	l12: {
		if ((__context->Manreg01b[14] && (__context->Perst.MNewMR_old != __context->Manreg01b[14]))) {
			__context->Perst.MPosRSt = __context->MPosR;
			goto l16;
		}
		if ((! (__context->Manreg01b[14] && (__context->Perst.MNewMR_old != __context->Manreg01b[14])))) {
			goto l16;
		}
		//assert(false);
		return;  			}
	l16: {
			__context->Perst.MNewMR_old = __context->Manreg01b[14];
			goto l17;
		//assert(false);
		return;  			}
	l17: {
			__context->Perst.AuPosRSt = __context->Perst.AuPosR;
			goto l18;
		//assert(false);
		return;  			}
	l18: {
		if ((__context->Perst.AuPosRSt > __context->Perst.PMaxRan)) {
			__context->Perst.AuPosRSt = __context->Perst.PMaxRan;
			goto l22;
		}
		if ((! (__context->Perst.AuPosRSt > __context->Perst.PMaxRan))) {
			goto l22;
		}
		//assert(false);
		return;  			}
	l22: {
		if ((__context->Perst.AuPosRSt < __context->Perst.PMinRan)) {
			__context->Perst.AuPosRSt = __context->Perst.PMinRan;
			goto l26;
		}
		if ((! (__context->Perst.AuPosRSt < __context->Perst.PMinRan))) {
			goto l26;
		}
		//assert(false);
		return;  			}
	l26: {
		if (__context->Perst.FoMoSt) {
			__context->Perst.PosSt = __context->Perst.MPosRSt;
			goto l28;
		}
		if ((! __context->Perst.FoMoSt)) {
			__context->Perst.PosSt = __context->Perst.AuPosRSt;
			goto l34;
		}
		//assert(false);
		return;  			}
	l28: {
			// Assign inputs
			ABS#REAL1_inlined_1.in = (__context->Perst.AuPosRSt - __context->Perst.MPosRSt);
			ABS_REAL(&ABS#REAL1_inlined_1);
			// Assign outputs
			___nested_ret_val1 = ABS#REAL1_inlined_1.RET_VAL;
			goto l29;
		//assert(false);
		return;  			}
	l29: {
			// Assign inputs
			ABS#REAL1_inlined_2.in = (__context->Perst.PMaxRan - __context->Perst.PMinRan);
			ABS_REAL(&ABS#REAL1_inlined_2);
			// Assign outputs
			___nested_ret_val3 = ABS#REAL1_inlined_2.RET_VAL;
			goto l30;
		//assert(false);
		return;  			}
	l30: {
			___nested_ret_val2 = ___nested_ret_val3;
			goto l31;
		//assert(false);
		return;  			}
	l31: {
			__context->Perst.AuMRW = ((___nested_ret_val1 / ___nested_ret_val2) > 0.01);
			goto l37;
		//assert(false);
		return;  			}
	l34: {
			__context->Perst.MPosRSt = __context->Perst.AuPosRSt;
			goto l35;
		//assert(false);
		return;  			}
	l35: {
			__context->Perst.AuMRW = false;
			goto l37;
		//assert(false);
		return;  			}
	l37: {
		if (E_MIOErBSetRst) {
			__context->Perst.MIOErBRSt = (! __context->Perst.MIOErBRSt);
			goto l41;
		}
		if ((! E_MIOErBSetRst)) {
			goto l41;
		}
		//assert(false);
		return;  			}
	l41: {
			__context->Perst.OutOV = ((((__context->Perst.PMaxRaw - __context->Perst.PMinRaw) / (__context->Perst.PMaxRan - __context->Perst.PMinRan)) * (__context->Perst.PosSt - __context->Perst.PMinRan)) + __context->Perst.PMinRaw);
			goto l42;
		//assert(false);
		return;  			}
	l42: {
			Outovold = __context->Perst.OutOV;
			goto l43;
		//assert(false);
		return;  			}
	l43: {
			__context->Perst.IOErrorW = (__context->Perst.IOError && (! __context->Perst.MIOErBRSt));
			goto l44;
		//assert(false);
		return;  			}
	l44: {
			__context->Perst.IOSimuW = __context->Perst.IOSimu;
			StsReg01b[8] = false;
			goto x3;
		//assert(false);
		return;  			}
	l46: {
			StsReg01b[9] = false;
			goto x4;
		//assert(false);
		return;  			}
	l47: {
			StsReg01b[10] = __context->Perst.AuMoSt;
			goto x5;
		//assert(false);
		return;  			}
	l48: {
			StsReg01b[11] = false;
			goto x6;
		//assert(false);
		return;  			}
	l49: {
			StsReg01b[12] = __context->Perst.FoMoSt;
			goto x7;
		//assert(false);
		return;  			}
	l50: {
			StsReg01b[13] = false;
			goto x8;
		//assert(false);
		return;  			}
	l51: {
			StsReg01b[14] = __context->Perst.IOErrorW;
			goto x9;
		//assert(false);
		return;  			}
	l52: {
			StsReg01b[15] = __context->Perst.IOSimuW;
			goto x10;
		//assert(false);
		return;  			}
	l53: {
			StsReg01b[0] = __context->Perst.AuMRW;
			goto x11;
		//assert(false);
		return;  			}
	l54: {
			StsReg01b[1] = __context->Perst.MIOErBRSt;
			goto x12;
		//assert(false);
		return;  			}
	l55: {
			StsReg01b[2] = false;
			goto x13;
		//assert(false);
		return;  			}
	l56: {
			StsReg01b[3] = false;
			goto x14;
		//assert(false);
		return;  			}
	l57: {
			StsReg01b[4] = false;
			goto x15;
		//assert(false);
		return;  			}
	l58: {
			StsReg01b[5] = __context->Perst.AuIhFoMo;
			goto x16;
		//assert(false);
		return;  			}
	l59: {
			StsReg01b[6] = false;
			goto x17;
		//assert(false);
		return;  			}
	l60: {
			StsReg01b[7] = false;
			goto x18;
		//assert(false);
		return;  			}
	l61: {
			__context->StsReg01 = TempStsReg01;
			goto l62;
		//assert(false);
		return;  			}
	l62: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	x: {
			E_MIOErBSetRst = false;
			Outovold = 0.0;
			TempStsReg01 = 0;
			___nested_ret_val1 = 0.0;
			___nested_ret_val2 = 0.0;
			___nested_ret_val3 = 0.0;
			goto x1;
		//assert(false);
		return;  			}
	x1: {
			StsReg01b[0] = ((TempStsReg01 & 256) != 0);
			StsReg01b[1] = ((TempStsReg01 & 512) != 0);
			StsReg01b[2] = ((TempStsReg01 & 1024) != 0);
			StsReg01b[3] = ((TempStsReg01 & 2048) != 0);
			StsReg01b[4] = ((TempStsReg01 & 4096) != 0);
			StsReg01b[5] = ((TempStsReg01 & 8192) != 0);
			StsReg01b[6] = ((TempStsReg01 & 16384) != 0);
			StsReg01b[7] = ((TempStsReg01 & 32768) != 0);
			StsReg01b[8] = ((TempStsReg01 & 1) != 0);
			StsReg01b[9] = ((TempStsReg01 & 2) != 0);
			StsReg01b[10] = ((TempStsReg01 & 4) != 0);
			StsReg01b[11] = ((TempStsReg01 & 8) != 0);
			StsReg01b[12] = ((TempStsReg01 & 16) != 0);
			StsReg01b[13] = ((TempStsReg01 & 32) != 0);
			StsReg01b[14] = ((TempStsReg01 & 64) != 0);
			StsReg01b[15] = ((TempStsReg01 & 128) != 0);
			goto l1;
		//assert(false);
		return;  			}
	x2: {
			StsReg01b[0] = ((TempStsReg01 & 256) != 0);
			StsReg01b[1] = ((TempStsReg01 & 512) != 0);
			StsReg01b[2] = ((TempStsReg01 & 1024) != 0);
			StsReg01b[3] = ((TempStsReg01 & 2048) != 0);
			StsReg01b[4] = ((TempStsReg01 & 4096) != 0);
			StsReg01b[5] = ((TempStsReg01 & 8192) != 0);
			StsReg01b[6] = ((TempStsReg01 & 16384) != 0);
			StsReg01b[7] = ((TempStsReg01 & 32768) != 0);
			StsReg01b[8] = ((TempStsReg01 & 1) != 0);
			StsReg01b[9] = ((TempStsReg01 & 2) != 0);
			StsReg01b[10] = ((TempStsReg01 & 4) != 0);
			StsReg01b[11] = ((TempStsReg01 & 8) != 0);
			StsReg01b[12] = ((TempStsReg01 & 16) != 0);
			StsReg01b[13] = ((TempStsReg01 & 32) != 0);
			StsReg01b[14] = ((TempStsReg01 & 64) != 0);
			StsReg01b[15] = ((TempStsReg01 & 128) != 0);
			goto l2;
		//assert(false);
		return;  			}
	x3: {
		if (StsReg01b[8]) {
			TempStsReg01 = (TempStsReg01 | 1);
			goto l46;
		}
		if ((! StsReg01b[8])) {
			TempStsReg01 = (TempStsReg01 & 65534);
			goto l46;
		}
		//assert(false);
		return;  			}
	x4: {
		if (StsReg01b[9]) {
			TempStsReg01 = (TempStsReg01 | 2);
			goto l47;
		}
		if ((! StsReg01b[9])) {
			TempStsReg01 = (TempStsReg01 & 65533);
			goto l47;
		}
		//assert(false);
		return;  			}
	x5: {
		if (StsReg01b[10]) {
			TempStsReg01 = (TempStsReg01 | 4);
			goto l48;
		}
		if ((! StsReg01b[10])) {
			TempStsReg01 = (TempStsReg01 & 65531);
			goto l48;
		}
		//assert(false);
		return;  			}
	x6: {
		if (StsReg01b[11]) {
			TempStsReg01 = (TempStsReg01 | 8);
			goto l49;
		}
		if ((! StsReg01b[11])) {
			TempStsReg01 = (TempStsReg01 & 65527);
			goto l49;
		}
		//assert(false);
		return;  			}
	x7: {
		if (StsReg01b[12]) {
			TempStsReg01 = (TempStsReg01 | 16);
			goto l50;
		}
		if ((! StsReg01b[12])) {
			TempStsReg01 = (TempStsReg01 & 65519);
			goto l50;
		}
		//assert(false);
		return;  			}
	x8: {
		if (StsReg01b[13]) {
			TempStsReg01 = (TempStsReg01 | 32);
			goto l51;
		}
		if ((! StsReg01b[13])) {
			TempStsReg01 = (TempStsReg01 & 65503);
			goto l51;
		}
		//assert(false);
		return;  			}
	x9: {
		if (StsReg01b[14]) {
			TempStsReg01 = (TempStsReg01 | 64);
			goto l52;
		}
		if ((! StsReg01b[14])) {
			TempStsReg01 = (TempStsReg01 & 65471);
			goto l52;
		}
		//assert(false);
		return;  			}
	x10: {
		if (StsReg01b[15]) {
			TempStsReg01 = (TempStsReg01 | 128);
			goto l53;
		}
		if ((! StsReg01b[15])) {
			TempStsReg01 = (TempStsReg01 & 65407);
			goto l53;
		}
		//assert(false);
		return;  			}
	x11: {
		if (StsReg01b[0]) {
			TempStsReg01 = (TempStsReg01 | 256);
			goto l54;
		}
		if ((! StsReg01b[0])) {
			TempStsReg01 = (TempStsReg01 & 65279);
			goto l54;
		}
		//assert(false);
		return;  			}
	x12: {
		if (StsReg01b[1]) {
			TempStsReg01 = (TempStsReg01 | 512);
			goto l55;
		}
		if ((! StsReg01b[1])) {
			TempStsReg01 = (TempStsReg01 & 65023);
			goto l55;
		}
		//assert(false);
		return;  			}
	x13: {
		if (StsReg01b[2]) {
			TempStsReg01 = (TempStsReg01 | 1024);
			goto l56;
		}
		if ((! StsReg01b[2])) {
			TempStsReg01 = (TempStsReg01 & 64511);
			goto l56;
		}
		//assert(false);
		return;  			}
	x14: {
		if (StsReg01b[3]) {
			TempStsReg01 = (TempStsReg01 | 2048);
			goto l57;
		}
		if ((! StsReg01b[3])) {
			TempStsReg01 = (TempStsReg01 & 63487);
			goto l57;
		}
		//assert(false);
		return;  			}
	x15: {
		if (StsReg01b[4]) {
			TempStsReg01 = (TempStsReg01 | 4096);
			goto l58;
		}
		if ((! StsReg01b[4])) {
			TempStsReg01 = (TempStsReg01 & 61439);
			goto l58;
		}
		//assert(false);
		return;  			}
	x16: {
		if (StsReg01b[5]) {
			TempStsReg01 = (TempStsReg01 | 8192);
			goto l59;
		}
		if ((! StsReg01b[5])) {
			TempStsReg01 = (TempStsReg01 & 57343);
			goto l59;
		}
		//assert(false);
		return;  			}
	x17: {
		if (StsReg01b[6]) {
			TempStsReg01 = (TempStsReg01 | 16384);
			goto l60;
		}
		if ((! StsReg01b[6])) {
			TempStsReg01 = (TempStsReg01 & 49151);
			goto l60;
		}
		//assert(false);
		return;  			}
	x18: {
		if (StsReg01b[7]) {
			TempStsReg01 = (TempStsReg01 | 32768);
			goto l61;
		}
		if ((! StsReg01b[7])) {
			TempStsReg01 = (TempStsReg01 & 32767);
			goto l61;
		}
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void R_EDGE(__R_EDGE *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
		if (((__context->new == true) && (__context->old == false))) {
			__context->RET_VAL = true;
			__context->old = true;
			goto l71;
		}
		if ((! ((__context->new == true) && (__context->old == false)))) {
			__context->RET_VAL = false;
			__context->old = __context->new;
			goto l71;
		}
		//assert(false);
		return;  			}
	l71: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void ABS_REAL(__ABS_REAL *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init2;
	init2: {
		if ((__context->in > 0.0)) {
			__context->RET_VAL = __context->in;
			goto l5;
		}
		if ((! (__context->in > 0.0))) {
			__context->RET_VAL = ((- 1.0) * __context->in);
			goto l5;
		}
		//assert(false);
		return;  			}
	l5: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init3;
	init3: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			instance.MPosR = nondet_float();
			instance.Manreg01 = nondet_uint16_t();
			instance.Manreg01b[0] = nondet_bool();
			instance.Manreg01b[10] = nondet_bool();
			instance.Manreg01b[11] = nondet_bool();
			instance.Manreg01b[12] = nondet_bool();
			instance.Manreg01b[13] = nondet_bool();
			instance.Manreg01b[14] = nondet_bool();
			instance.Manreg01b[15] = nondet_bool();
			instance.Manreg01b[1] = nondet_bool();
			instance.Manreg01b[2] = nondet_bool();
			instance.Manreg01b[3] = nondet_bool();
			instance.Manreg01b[4] = nondet_bool();
			instance.Manreg01b[5] = nondet_bool();
			instance.Manreg01b[6] = nondet_bool();
			instance.Manreg01b[7] = nondet_bool();
			instance.Manreg01b[8] = nondet_bool();
			instance.Manreg01b[9] = nondet_bool();
			instance.Perst.AuIhFoMo = nondet_bool();
			instance.Perst.AuMRW = nondet_bool();
			instance.Perst.AuMoSt = nondet_bool();
			instance.Perst.AuPosR = nondet_float();
			instance.Perst.AuPosRSt = nondet_float();
			instance.Perst.DBbitIoError = nondet_int16_t();
			instance.Perst.DBnum = nondet_int16_t();
			instance.Perst.DBnumIoError = nondet_int16_t();
			instance.Perst.DBpos = nondet_int16_t();
			instance.Perst.DBposIoError = nondet_int16_t();
			instance.Perst.FEType = nondet_int16_t();
			instance.Perst.FoMoSt = nondet_bool();
			instance.Perst.IOError = nondet_bool();
			instance.Perst.IOErrorW = nondet_bool();
			instance.Perst.IOSimu = nondet_bool();
			instance.Perst.IOSimuW = nondet_bool();
			instance.Perst.MIOErBRSt = nondet_bool();
			instance.Perst.MIOErBSetRst_old = nondet_bool();
			instance.Perst.MNewMR_old = nondet_bool();
			instance.Perst.MPosRSt = nondet_float();
			instance.Perst.OutOV = nondet_float();
			instance.Perst.PMaxRan = nondet_float();
			instance.Perst.PMaxRaw = nondet_float();
			instance.Perst.PMinRan = nondet_float();
			instance.Perst.PMinRaw = nondet_float();
			instance.Perst.PQWDef = nondet_bool();
			instance.Perst.PosSt = nondet_float();
			instance.Perst.index = nondet_int16_t();
			instance.StsReg01 = nondet_uint16_t();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			CPC_FB_AOR(&instance);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
void main() {
	// Initial values
	R_EDGE1.new = false;
	R_EDGE1.old = false;
	R_EDGE1.RET_VAL = false;
	ABS#REAL1.in = 0.0;
	ABS#REAL1.RET_VAL = 0.0;
	instance.Manreg01 = 0;
	instance.Manreg01b[0] = false;
	instance.Manreg01b[1] = false;
	instance.Manreg01b[2] = false;
	instance.Manreg01b[3] = false;
	instance.Manreg01b[4] = false;
	instance.Manreg01b[5] = false;
	instance.Manreg01b[6] = false;
	instance.Manreg01b[7] = false;
	instance.Manreg01b[8] = false;
	instance.Manreg01b[9] = false;
	instance.Manreg01b[10] = false;
	instance.Manreg01b[11] = false;
	instance.Manreg01b[12] = false;
	instance.Manreg01b[13] = false;
	instance.Manreg01b[14] = false;
	instance.Manreg01b[15] = false;
	instance.MPosR = 0.0;
	instance.StsReg01 = 0;
	instance.Perst.FEType = 0;
	instance.Perst.index = 0;
	instance.Perst.DBnum = 0;
	instance.Perst.DBpos = 0;
	instance.Perst.DBnumIoError = 0;
	instance.Perst.DBposIoError = 0;
	instance.Perst.DBbitIoError = 0;
	instance.Perst.AuPosR = 0.0;
	instance.Perst.PMinRan = 0.0;
	instance.Perst.PMaxRan = 0.0;
	instance.Perst.PMinRaw = 0.0;
	instance.Perst.PMaxRaw = 0.0;
	instance.Perst.PosSt = 0.0;
	instance.Perst.OutOV = 0.0;
	instance.Perst.MPosRSt = 0.0;
	instance.Perst.AuPosRSt = 0.0;
	instance.Perst.MNewMR_old = false;
	instance.Perst.AuIhFoMo = false;
	instance.Perst.AuMoSt = false;
	instance.Perst.FoMoSt = false;
	instance.Perst.IOErrorW = false;
	instance.Perst.IOSimuW = false;
	instance.Perst.IOError = false;
	instance.Perst.IOSimu = false;
	instance.Perst.AuMRW = false;
	instance.Perst.MIOErBRSt = false;
	instance.Perst.MIOErBSetRst_old = false;
	instance.Perst.PQWDef = false;
	ABS#REAL1_inlined_1.in = 0.0;
	ABS#REAL1_inlined_1.RET_VAL = 0.0;
	ABS#REAL1_inlined_2.in = 0.0;
	ABS#REAL1_inlined_2.RET_VAL = 0.0;
	__assertion_error = 0;
	
	VerificationLoop();
}
