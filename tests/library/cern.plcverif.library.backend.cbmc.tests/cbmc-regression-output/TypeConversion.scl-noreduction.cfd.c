#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Root data structure
typedef struct {
	int16_t in_int;
	int32_t in_dint;
	float in_real;
	int16_t out_int;
	int32_t out_dint;
	float out_real;
} __typeconv_fb1;

// Global variables
__typeconv_fb1 typeconv_db1;
uint16_t __assertion_error;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Automata declarations
void typeconv_fb1(__typeconv_fb1 *__context);
void VerificationLoop();

// Automata
void typeconv_fb1(__typeconv_fb1 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			__context->out_int = __context->in_int;
			goto l1;
		//assert(false);
		return;  			}
	l1: {
			__context->out_dint = __context->in_dint;
			goto l2;
		//assert(false);
		return;  			}
	l2: {
			__context->out_real = __context->in_real;
			goto l3;
		//assert(false);
		return;  			}
	l3: {
			__context->out_real = ((float) __context->in_int);
			goto l4;
		//assert(false);
		return;  			}
	l4: {
			__context->out_dint = ((int32_t) __context->in_int);
			goto l5;
		//assert(false);
		return;  			}
	l5: {
			__context->out_int = ((int16_t) __context->in_dint);
			goto l6;
		//assert(false);
		return;  			}
	l6: {
			__context->out_int = ((int16_t) __context->in_real);
			goto l7;
		//assert(false);
		return;  			}
	l7: {
			__context->out_dint = ((int32_t) __context->in_real);
			goto l8;
		//assert(false);
		return;  			}
	l8: {
			__context->out_dint = 56;
			goto l9;
		//assert(false);
		return;  			}
	l9: {
			__context->out_dint = 567;
			goto l10;
		//assert(false);
		return;  			}
	l10: {
			__context->out_dint = 567;
			goto l11;
		//assert(false);
		return;  			}
	l11: {
			__context->out_dint = 567;
			goto l12;
		//assert(false);
		return;  			}
	l12: {
			__context->out_dint = (((int32_t) __context->in_int) * 10);
			goto l13;
		//assert(false);
		return;  			}
	l13: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			typeconv_db1.in_dint = nondet_int32_t();
			typeconv_db1.in_int = nondet_int16_t();
			typeconv_db1.in_real = nondet_float();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			typeconv_fb1(&typeconv_db1);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
void main() {
	// Initial values
	typeconv_db1.in_int = 0;
	typeconv_db1.in_dint = 0;
	typeconv_db1.in_real = 0.0;
	typeconv_db1.out_int = 0;
	typeconv_db1.out_dint = 0;
	typeconv_db1.out_real = 0.0;
	__assertion_error = 0;
	
	VerificationLoop();
}
