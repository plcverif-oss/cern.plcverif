#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Root data structure
typedef struct {
	int16_t index;
	int16_t FEType;
	int16_t InterfaceParam1;
	int16_t InterfaceParam2;
	float AuPosR;
	float PosSt;
	bool PIWDef;
	bool FOFEn;
} __CPC_DB_AS;
typedef struct {
	__CPC_DB_AS Perst;
} __CPC_FB_AS;

// Global variables
__CPC_FB_AS instance;
uint16_t __assertion_error;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Automata declarations
void CPC_FB_AS(__CPC_FB_AS *__context);
void VerificationLoop();

// Automata
void CPC_FB_AS(__CPC_FB_AS *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
			__context->Perst.PosSt = __context->Perst.AuPosR;
			goto l1;
		//assert(false);
		return;  			}
	l1: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			goto loop_start;
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			instance.Perst.AuPosR = nondet_float();
			instance.Perst.FEType = nondet_int16_t();
			instance.Perst.FOFEn = nondet_bool();
			instance.Perst.InterfaceParam1 = nondet_int16_t();
			instance.Perst.InterfaceParam2 = nondet_int16_t();
			instance.Perst.PIWDef = nondet_bool();
			instance.Perst.PosSt = nondet_float();
			instance.Perst.index = nondet_int16_t();
			goto prepare_BoC;
		if (false) {
			goto end;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			CPC_FB_AS(&instance);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
void main() {
	// Initial values
	instance.Perst.index = 0;
	instance.Perst.FEType = 0;
	instance.Perst.InterfaceParam1 = 0;
	instance.Perst.InterfaceParam2 = 0;
	instance.Perst.AuPosR = 0.0;
	instance.Perst.PosSt = 0.0;
	instance.Perst.PIWDef = false;
	instance.Perst.FOFEn = false;
	__assertion_error = 0;
	
	VerificationLoop();
}
