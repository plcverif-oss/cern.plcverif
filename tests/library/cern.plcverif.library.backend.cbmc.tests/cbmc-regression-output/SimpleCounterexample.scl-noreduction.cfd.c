#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

// Declare nondet assignment functions
bool nondet_bool();
uint8_t nondet_uint8_t();
uint16_t nondet_uint16_t();
uint32_t nondet_uint32_t();
uint64_t nondet_uint64_t();
int8_t nondet_int8_t();
int16_t nondet_int16_t();
int32_t nondet_int32_t();
int64_t nondet_int64_t();
double nondet_float();
double nondet_double();

// Root data structure
typedef struct {
	bool in1;
	bool in2;
	int16_t out;
} __simple_cex_fb1;

// Global variables
__simple_cex_fb1 instance;
uint16_t __assertion_error;
bool __cbmc_boc_marker;
bool __cbmc_eoc_marker;

// Automata declarations
void simple_cex_fb1(__simple_cex_fb1 *__context);
void VerificationLoop();

// Automata
void simple_cex_fb1(__simple_cex_fb1 *__context) {
	// Temporary variables
	
	// Start with initial location
	goto init;
	init: {
		if ((__context->in1 && (! __context->in2))) {
			goto l1;
		}
		if ((! (__context->in1 && (! __context->in2)))) {
			goto l3;
		}
		//assert(false);
		return;  			}
	l1: {
			__context->out = (__context->out + 1);
			goto l2;
		//assert(false);
		return;  			}
	l2: {
			goto l4;
		//assert(false);
		return;  			}
	l3: {
			goto l4;
		//assert(false);
		return;  			}
	l4: {
		if ((! (__context->out < 3))) {
			__assertion_error = 1;
			goto end;
		}
		if ((! (! (__context->out < 3)))) {
			goto end;
		}
		//assert(false);
		return;  			}
	end: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}
void VerificationLoop() {
	// Temporary variables
	
	// Start with initial location
	goto init1;
	init1: {
			goto loop_start;
		//assert(false);
		return;  			}
	end1: {
		goto __end_of_automaton;
		//assert(false);
		return;  			}
	loop_start: {
			instance.in1 = nondet_bool();
			instance.in2 = nondet_bool();
			goto prepare_BoC;
		if (false) {
			goto end1;
		}
		//assert(false);
		return;  			}
	prepare_BoC: {
		__cbmc_boc_marker = true; // to indicate the beginning of the loop for the counterexample parser
		__cbmc_boc_marker = false;
			goto l_main_call;
		//assert(false);
		return;  			}
	l_main_call: {
			// Assign inputs
			simple_cex_fb1(&instance);
			// Assign outputs
			goto callEnd;
		//assert(false);
		return;  			}
	callEnd: {
			goto prepare_EoC;
		//assert(false);
		return;  			}
	prepare_EoC: {
		assert((__assertion_error == 0));
		__cbmc_eoc_marker = true; // to indicate the end of the loop for the counterexample parser
		__cbmc_eoc_marker = false;
			goto loop_start;
		//assert(false);
		return;  			}
	
	// End of automaton
	__end_of_automaton: ;
}

// Main
void main() {
	// Initial values
	instance.in1 = false;
	instance.in2 = false;
	instance.out = 0;
	__assertion_error = 0;
	
	VerificationLoop();
}
