/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.backend.cbmc;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import cern.plcverif.base.common.settings.exceptions.SettingsParserException;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.transformation.ElseEliminator;
import cern.plcverif.base.models.cfa.validation.CfaValidation;
import cern.plcverif.library.backend.cbmc.CbmcSettings.CbmcModelVariant;
import cern.plcverif.library.backend.cbmc.model.CbmcModelBuilderCfi;
import cern.plcverif.library.requirement.assertion.AssertionRequirementExtension;
import cern.plcverif.library.testmodels.TestModel;
import cern.plcverif.library.testmodels.TestModelCollection;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.VerificationResult;
import cern.plcverif.verif.jobmock.MockVerificationJob;
import cern.plcverif.verif.jobmock.MockVerificationJob.MockVerificationJobConfig;

/**
 * Regression tests relying on the MockVerificationJob, using the whole
 * Verification job except for backend execution.
 */
public class RegressionTest {
	public static final boolean CONTINUE_ON_ASSERT_VIOLATION = false; // NOPMD
	public static boolean writeActualOutputs = CONTINUE_ON_ASSERT_VIOLATION;
	
	private static String fileOutputDir;
	private static Path outputDir;

	@BeforeClass
	public static void setup() throws IOException {
		outputDir = Files.createTempDirectory("cbmc-regression");
		fileOutputDir = Files.createDirectories(Path.of("cbmc-regression-output")).toString();
	}
	
	@Test
	public void modelGenerationRegressionTests() throws IOException, SettingsParserException {
		for (TestModel testModel : TestModelCollection.getInstance().getAllModels()) {
			cbmcModelGenerationRegressionTest(testModel, String.format("/regression-tests/%s.cfi.c", testModel.getModelId()),
					true, CbmcModelVariant.CFI);
			cbmcModelGenerationRegressionTest(testModel,
					String.format("/regression-tests/%s-noreduction.cfi.c", testModel.getModelId()), false, CbmcModelVariant.CFI);
			
			cbmcModelGenerationRegressionTest(testModel, String.format("/regression-tests/%s.cfd.c", testModel.getModelId()),
					true, CbmcModelVariant.CFD);
			cbmcModelGenerationRegressionTest(testModel,
					String.format("/regression-tests/%s-noreduction.cfd.c", testModel.getModelId()), false, CbmcModelVariant.CFD);
		}
	}

	private void cbmcModelGenerationRegressionTest(TestModel testModel, String expectedSmvPath, boolean reductions, CbmcModelVariant modelVariant)
			throws IOException, SettingsParserException {
		String modelId = reductions ? testModel.getModelId() : testModel.getModelId() + "-noreduction";
		System.out.println(
				String.format("=== %s (CBMC-%s model generation regression test with verification loop) ===", modelId, modelVariant));
		

		CbmcSettings settings = new CbmcSettings();
		settings.setModelVariant(modelVariant);
		
		// Generate the CFI using the VerificationJob workflow
		MockVerificationJobConfig config = reductions ? new MockVerificationJobConfig()
				: MockVerificationJobConfig.noReduction();
		config.setBackendExtension(new CbmcBackendExtension());
		config.setRequirementExtension(new AssertionRequirementExtension());
		config.setReporterExtension(null);
		config.setSettings("-job.strict=true -job.backend." + CbmcSettings.MODEL_VARIANT + "=" + modelVariant.toString());
		
		CfaNetworkDeclaration cfd = testModel.getCfd();
		CfaValidation.validate(cfd);
		VerificationResult result = MockVerificationJob.run(cfd, modelId, outputDir, config, false);
		VerificationProblem verifProblem = result.getVerifProblem().get();
		
		// The CBMC-specific transformations need to be done here as this
		// part is skipped from the mock job
		ElseEliminator.transform(verifProblem.getModel());
		
		CharSequence actualCbmcModel = CbmcModelBuilderCfi.buildModel(verifProblem, result, settings);
		Assert.assertNotNull(actualCbmcModel);

		if (writeActualOutputs) {
			File modelFile = new File( fileOutputDir, String.format("%s.%s.c" , modelId, modelVariant.toString().toLowerCase()));
			IoUtils.writeAllContent(modelFile, actualCbmcModel);
		}

		// ASSERT
		if (!CONTINUE_ON_ASSERT_VIOLATION) {
			// Load expected
			String expectedCbmcModel = IoUtils.readResourceFile(expectedSmvPath, this.getClass().getClassLoader());
			Assert.assertEquals(expectedCbmcModel.trim(), actualCbmcModel.toString().trim());
		}
	}
	
	@Test
	public void sentinel() {
		Assert.assertFalse(CONTINUE_ON_ASSERT_VIOLATION);
	}
}
