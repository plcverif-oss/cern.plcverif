/******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *****************************************************************************/
package cern.plcverif.library.backend.cbmc.parser

import org.junit.Test
import org.junit.Assert
import cern.plcverif.verif.interfaces.data.ResultEnum
import java.util.Arrays
import cern.plcverif.library.backend.cbmc.CbmcSettings
import cern.plcverif.base.common.logging.IPlcverifLogger

class CbmcOutputParserTest {
	@Test
	def singleCycleTest() {
		val toBeParsed = '''
			CBMC version 5.8 64-bit x86_64 windows
			Parsing test.c
			test.c
			Converting
			Type-checking model_PlcPerf06-01_cbmc
			Generating GOTO Program
			Adding CPROVER library (x86_64)
			Removal of function pointers and virtual functions
			Partial Inlining
			Generic Property Instrumentation
			Starting Bounded Model Checking
			Passing problem to propositional reduction
			converting SSA
			Running propositional reduction
			37220 variables, 217192 clauses
			SAT checker: instance is SATISFIABLE
			Runtime decision procedure: 3.046s
			
			** Results:
			[VerificationLoop.assertion.1] assertion (__assertion_error == 0): FAILURE
			
			Trace for VerificationLoop.assertion.1:
			
			State 25 file test.c line 338 function VerificationLoop thread 0
			----------------------------------------------------
			  instance.in=TRUE (00000001)
			
			State 26 file test.c line 339 function VerificationLoop thread 0
			----------------------------------------------------
			  instance.pt_in=2147483148 (01111111111111111111111000001100)
			
			State 27 file test.c line 340 function VerificationLoop thread 0
			----------------------------------------------------
			  T_CYCLE=0 (00000000)
			
			State 30 file test.c line 347 function VerificationLoop thread 0
			----------------------------------------------------
			  context=&instance.in (0000001000000000000000000000000000000000000000000000000000000000)
			
			State 33 file test.c line 76 function PlcPerf06 thread 0
			----------------------------------------------------
			  instance.pt=2147483148 (01111111111111111111111000001100)
			
			State 34 file test.c line 83 function PlcPerf06 thread 0
			----------------------------------------------------
			  instance.timer1.IN=TRUE (00000001)
			
			State 35 file test.c line 84 function PlcPerf06 thread 0
			----------------------------------------------------
			  instance.timer1.PT=2147483148 (01111111111111111111111000001100)
			
			State 38 file test.c line 85 function PlcPerf06 thread 0
			----------------------------------------------------
			  context=&instance.timer1.PT (0000001000000000000000000000000000000000000000000000000000001100)
			
			State 44 file test.c line 211 function TON thread 0
			----------------------------------------------------
			  instance.timer1.start=0 (00000000000000000000000000000000)
			
			State 45 file test.c line 212 function TON thread 0
			----------------------------------------------------
			  instance.timer1.running=TRUE (00000001)
			
			State 46 file test.c line 213 function TON thread 0
			----------------------------------------------------
			  instance.timer1.ET=0 (00000000000000000000000000000000)
			
			State 55 file test.c line 93 function PlcPerf06 thread 0
			----------------------------------------------------
			  instance.timer2.IN=TRUE (00000001)
			
			State 56 file test.c line 94 function PlcPerf06 thread 0
			----------------------------------------------------
			  instance.timer2.PT=-2147483648 (10000000000000000000000000000000)
			
			State 59 file test.c line 95 function PlcPerf06 thread 0
			----------------------------------------------------
			  context=&instance.timer2.PT (0000001000000000000000000000000000000000000000000000000000100000)
			
			State 65 file test.c line 211 function TON thread 0
			----------------------------------------------------
			  instance.timer2.start=0 (00000000000000000000000000000000)
			
			State 66 file test.c line 212 function TON thread 0
			----------------------------------------------------
			  instance.timer2.running=TRUE (00000001)
			
			State 67 file test.c line 213 function TON thread 0
			----------------------------------------------------
			  instance.timer2.ET=0 (00000000000000000000000000000000)
			
			State 76 file test.c line 102 function PlcPerf06 thread 0
			----------------------------------------------------
			  instance.q1=FALSE (00000000)
			
			State 77 file test.c line 103 function PlcPerf06 thread 0
			----------------------------------------------------
			  instance.q2=FALSE (00000000)
			
			State 78 file test.c line 110 function PlcPerf06 thread 0
			----------------------------------------------------
			  instance.timer3.IN=FALSE (00000000)
			
			State 79 file test.c line 111 function PlcPerf06 thread 0
			----------------------------------------------------
			  instance.timer3.PT=2147483148 (01111111111111111111111000001100)
			
			State 82 file test.c line 112 function PlcPerf06 thread 0
			----------------------------------------------------
			  context=&instance.timer3.PT (0000001000000000000000000000000000000000000000000000000000110100)
			
			State 85 file test.c line 193 function TON thread 0
			----------------------------------------------------
			  instance.timer3.Q=FALSE (00000000)
			
			State 86 file test.c line 194 function TON thread 0
			----------------------------------------------------
			  instance.timer3.ET=0 (00000000000000000000000000000000)
			
			State 87 file test.c line 195 function TON thread 0
			----------------------------------------------------
			  instance.timer3.running=FALSE (00000000)
			
			State 92 file test.c line 120 function PlcPerf06 thread 0
			----------------------------------------------------
			  instance.tof.IN=FALSE (00000000)
			
			State 93 file test.c line 121 function PlcPerf06 thread 0
			----------------------------------------------------
			  instance.tof.PT=2147483148 (01111111111111111111111000001100)
			
			State 96 file test.c line 122 function PlcPerf06 thread 0
			----------------------------------------------------
			  context=&instance.tof.PT (0000001000000000000000000000000000000000000000000000000001001000)
			
			State 97 file test.c line 298 function TOF thread 0
			----------------------------------------------------
			  instance.tof.ton.PT=2147483148 (01111111111111111111111000001100)
			
			State 98 file test.c line 299 function TOF thread 0
			----------------------------------------------------
			  instance.tof.ton.IN=TRUE (00000001)
			
			State 101 file test.c line 300 function TOF thread 0
			----------------------------------------------------
			  context=&instance.tof.ton.PT (0000001000000000000000000000000000000000000000000000000001010100)
			
			State 107 file test.c line 211 function TON thread 0
			----------------------------------------------------
			  instance.tof.ton.start=0 (00000000000000000000000000000000)
			
			State 108 file test.c line 212 function TON thread 0
			----------------------------------------------------
			  instance.tof.ton.running=TRUE (00000001)
			
			State 109 file test.c line 213 function TON thread 0
			----------------------------------------------------
			  instance.tof.ton.ET=0 (00000000000000000000000000000000)
			
			State 118 file test.c line 307 function TOF thread 0
			----------------------------------------------------
			  instance.tof.ET=0 (00000000000000000000000000000000)
			
			State 119 file test.c line 308 function TOF thread 0
			----------------------------------------------------
			  instance.tof.Q=TRUE (00000001)
			
			State 172 file test.c line 142 function PlcPerf06 thread 0
			----------------------------------------------------
			  __assertion_error=3 (0000000000000011)
			
			Violated property:
			  file test.c line 359 function VerificationLoop
			  assertion (__assertion_error == 0)
			  FALSE
			
			
			** 1 of 1 failed (1 iteration)
			VERIFICATION FAILED
		''';

		val parser = new CbmcOutputParser(toBeParsed, new CbmcSettings(), IPlcverifLogger.NullPlcverifLogger.NULL_LOGGER);
		Assert.assertEquals(ResultEnum.Violated, parser.parseResult());
	
		val vvmaps = parser.parseCounterexampleToMaps();
		Assert.assertNotNull(vvmaps);
		Assert.assertEquals(1, vvmaps.size);

		val vvmap = vvmaps.get(0);
		Assert.assertNotNull(vvmap.get("instance.timer3.ET"));
		Assert.assertEquals("0", vvmap.get("instance.timer3.ET"));
		Assert.assertNotNull(vvmap.get("instance.q1"));
		Assert.assertEquals("FALSE", vvmap.get("instance.q1"));
	}

	@Test
	def multiCycleTest() {
		val toBeParsed = '''
		**********************************************************************
		** Visual Studio 2017 Developer Command Prompt v15.0.26430.14
		** Copyright (c) 2017 Microsoft Corporation
		**********************************************************************
		[vcvarsall.bat] Environment initialized for: 'x64'
		CBMC version 5.8 64-bit x86_64 windows
		Parsing test.c
		cbmc-cex-1.c
		Converting
		Type-checking cbmc-cex-1
		Generating GOTO Program
		Adding CPROVER library (x86_64)
		Removal of function pointers and virtual functions
		Partial Inlining
		Generic Property Instrumentation
		Starting Bounded Model Checking
		Passing problem to propositional reduction
		converting SSA
		Running propositional reduction
		641 variables, 2274 clauses
		SAT checker: instance is SATISFIABLE
		Runtime decision procedure: 0.009s
		
		** Results:
		[VerificationLoop.assertion.1] assertion (__assertion_error == 0): FAILURE
		
		Trace for VerificationLoop.assertion.1:
		
		State 24 file test.c line 92 function VerificationLoop thread 0
		----------------------------------------------------
		  instance.in1=FALSE (00000000)
		
		State 25 file test.c line 93 function VerificationLoop thread 0
		----------------------------------------------------
		  instance.in2=FALSE (00000000)
		
		State 28 file test.c line 100 function VerificationLoop thread 0
		----------------------------------------------------
		  context=&instance.in1 (0000001000000000000000000000000000000000000000000000000000000000)
		
		State 42 file test.c line 113 function VerificationLoop thread 0
		----------------------------------------------------
		  __cbmc_eoc_marker=TRUE (00000001)
		
		State 43 file test.c line 114 function VerificationLoop thread 0
		----------------------------------------------------
		  __cbmc_eoc_marker=FALSE (00000000)
		
		State 45 file test.c line 92 function VerificationLoop thread 0
		----------------------------------------------------
		  instance.in1=FALSE (00000000)
		
		State 46 file test.c line 93 function VerificationLoop thread 0
		----------------------------------------------------
		  instance.in2=FALSE (00000000)
		
		State 49 file test.c line 100 function VerificationLoop thread 0
		----------------------------------------------------
		  context=&instance.in1 (0000001000000000000000000000000000000000000000000000000000000000)
		
		State 63 file test.c line 113 function VerificationLoop thread 0
		----------------------------------------------------
		  __cbmc_eoc_marker=TRUE (00000001)
		
		State 64 file test.c line 114 function VerificationLoop thread 0
		----------------------------------------------------
		  __cbmc_eoc_marker=FALSE (00000000)
		
		State 66 file test.c line 92 function VerificationLoop thread 0
		----------------------------------------------------
		  instance.in1=TRUE (00000001)
		
		State 67 file test.c line 93 function VerificationLoop thread 0
		----------------------------------------------------
		  instance.in2=FALSE (00000000)
		
		State 70 file test.c line 100 function VerificationLoop thread 0
		----------------------------------------------------
		  context=&instance.in1 (0000001000000000000000000000000000000000000000000000000000000000)
		
		State 73 file test.c line 51 function cbmc_cex_parser_1 thread 0
		----------------------------------------------------
		  instance.out=1 (0000000000000001)
		
		State 82 file test.c line 113 function VerificationLoop thread 0
		----------------------------------------------------
		  __cbmc_eoc_marker=TRUE (00000001)
		
		State 83 file test.c line 114 function VerificationLoop thread 0
		----------------------------------------------------
		  __cbmc_eoc_marker=FALSE (00000000)
		
		State 85 file test.c line 92 function VerificationLoop thread 0
		----------------------------------------------------
		  instance.in1=FALSE (00000000)
		
		State 86 file test.c line 93 function VerificationLoop thread 0
		----------------------------------------------------
		  instance.in2=FALSE (00000000)
		
		State 89 file test.c line 100 function VerificationLoop thread 0
		----------------------------------------------------
		  context=&instance.in1 (0000001000000000000000000000000000000000000000000000000000000000)
		
		State 103 file test.c line 113 function VerificationLoop thread 0
		----------------------------------------------------
		  __cbmc_eoc_marker=TRUE (00000001)
		
		State 104 file test.c line 114 function VerificationLoop thread 0
		----------------------------------------------------
		  __cbmc_eoc_marker=FALSE (00000000)
		
		State 106 file test.c line 92 function VerificationLoop thread 0
		----------------------------------------------------
		  instance.in1=FALSE (00000000)
		
		State 107 file test.c line 93 function VerificationLoop thread 0
		----------------------------------------------------
		  instance.in2=FALSE (00000000)
		
		State 110 file test.c line 100 function VerificationLoop thread 0
		----------------------------------------------------
		  context=&instance.in1 (0000001000000000000000000000000000000000000000000000000000000000)
		
		State 124 file test.c line 113 function VerificationLoop thread 0
		----------------------------------------------------
		  __cbmc_eoc_marker=TRUE (00000001)
		
		State 125 file test.c line 114 function VerificationLoop thread 0
		----------------------------------------------------
		  __cbmc_eoc_marker=FALSE (00000000)
		
		State 127 file test.c line 92 function VerificationLoop thread 0
		----------------------------------------------------
		  instance.in1=TRUE (00000001)
		
		State 128 file test.c line 93 function VerificationLoop thread 0
		----------------------------------------------------
		  instance.in2=FALSE (00000000)
		
		State 131 file test.c line 100 function VerificationLoop thread 0
		----------------------------------------------------
		  context=&instance.in1 (0000001000000000000000000000000000000000000000000000000000000000)
		
		State 134 file test.c line 51 function cbmc_cex_parser_1 thread 0
		----------------------------------------------------
		  instance.out=2 (0000000000000010)
		
		State 143 file test.c line 113 function VerificationLoop thread 0
		----------------------------------------------------
		  __cbmc_eoc_marker=TRUE (00000001)
		
		State 144 file test.c line 114 function VerificationLoop thread 0
		----------------------------------------------------
		  __cbmc_eoc_marker=FALSE (00000000)
		
		State 146 file test.c line 92 function VerificationLoop thread 0
		----------------------------------------------------
		  instance.in1=TRUE (00000001)
		
		State 147 file test.c line 93 function VerificationLoop thread 0
		----------------------------------------------------
		  instance.in2=FALSE (00000000)
		
		State 150 file test.c line 100 function VerificationLoop thread 0
		----------------------------------------------------
		  context=&instance.in1 (0000001000000000000000000000000000000000000000000000000000000000)
		
		State 153 file test.c line 51 function cbmc_cex_parser_1 thread 0
		----------------------------------------------------
		  instance.out=3 (0000000000000011)
		
		State 156 file test.c line 61 function cbmc_cex_parser_1 thread 0
		----------------------------------------------------
		  __assertion_error=1 (0000000000000001)
		
		Violated property:
		  file test.c line 112 function VerificationLoop
		  assertion (__assertion_error == 0)
		  FALSE
		
		
		VERIFICATION FAILED
		** 1 of 1 failed (1 iteration)
		''';

		val parser = new CbmcOutputParser(toBeParsed, new CbmcSettings(), IPlcverifLogger.NullPlcverifLogger.NULL_LOGGER);
		Assert.assertEquals(ResultEnum.Violated, parser.parseResult());
	
		val vvmaps = parser.parseCounterexampleToMaps();
		Assert.assertNotNull(vvmaps);
		Assert.assertEquals(7, vvmaps.size);
		
		val in1_expectedValues = Arrays.asList("FALSE", "FALSE", "TRUE", "FALSE", "FALSE", "TRUE", "TRUE");
		val out_expectedValues = Arrays.asList(null, null, "1", "1", "1", "2", "3");
		val in2_expectedValue = "FALSE";
		for (i : 0..6) {
			val vvmap = vvmaps.get(i);
			Assert.assertEquals(in1_expectedValues.get(i), vvmap.get("instance.in1"));
			Assert.assertEquals(in2_expectedValue, vvmap.get("instance.in2"));
			Assert.assertEquals(out_expectedValues.get(i), vvmap.get("instance.out"));
		}
	}
}
