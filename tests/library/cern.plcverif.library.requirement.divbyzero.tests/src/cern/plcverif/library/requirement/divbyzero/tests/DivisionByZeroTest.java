/*******************************************************************************
 * (C) Copyright CERN 2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jean-Charles Tournier - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.requirement.divbyzero.tests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Set;

import cern.plcverif.base.common.emf.textual.EmfModelPrinter;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.base.interfaces.IAstReference;
import cern.plcverif.base.interfaces.IParserLazyResult;
import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.interfaces.exceptions.AtomParsingException;
import cern.plcverif.base.models.cfa.CfaDeclarationSafeFactory;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfadeclaration.OriginalDataTypeFieldAnnotation;
import cern.plcverif.base.models.cfa.serialization.CfaToXmi;
import cern.plcverif.base.models.cfa.serialization.XmiToCfa;
import cern.plcverif.base.models.cfa.textual.CfaEObjectToText;
import cern.plcverif.base.models.expr.AtomicExpression;
import cern.plcverif.library.requirement.divbyzero.DivByZeroRequirement;
import cern.plcverif.library.requirement.divbyzero.DivByZeroRequirementExtension;
import cern.plcverif.library.testing.TestingUtil;
import cern.plcverif.verif.interfaces.data.RequirementRepresentationStrategy;
import cern.plcverif.verif.interfaces.data.VerificationProblem;
import cern.plcverif.verif.interfaces.data.VerificationResult;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.junit.Before;
import org.junit.Test;

public class DivisionByZeroTest {

	private DivByZeroRequirementExtension reqExtension = new DivByZeroRequirementExtension();
	private static final CfaDeclarationSafeFactory FACTORY = CfaDeclarationSafeFactory.INSTANCE;

	private IParserLazyResult parserResultMock;
	private VerificationProblem verifProblem = new VerificationProblem();
	private VerificationResult verifResult = new VerificationResult();

	@Before
	public void loadTestModel() throws IOException, AtomParsingException {
		CfaNetworkDeclaration cfa = XmiToCfa
				.deserializeFromString(IoUtils.readResourceFile("cfa-orig.xmi", this.getClass().getClassLoader()));
		verifProblem.setModel(cfa);

		// LOWPRI use mocking framework (but Mockito didn't work as Eclipse
		// dependency)
		parserResultMock = new IParserLazyResult() {

			@Override
			public String serializeAtom(AtomicExpression atom) {
				return null;
			}

			@Override
			public AtomicExpression parseAtom(String atom) throws AtomParsingException {
				return FACTORY.trueLiteral();
			}

			@Override
			public Set<String> getFileNames() {
				return null;
			}

			@Override
			public IAstReference<?> getAst() {
				return null;
			}

			@Override
			public CfaNetworkDeclaration generateCfa(JobResult result) {
				return null;
			}

			@Override
			public String serializeAtom(AtomicExpression atom, OriginalDataTypeFieldAnnotation hint) {
				return null;
			}
		};
	}

	@Test
	public void testNoGenericErrorInLog() {
		DivByZeroRequirement req = reqExtension.createRequirement();

		req.fillVerificationProblem(verifProblem, parserResultMock, verifResult,
				RequirementRepresentationStrategy.EXPLICIT_WITH_VARIABLE);

		// First test that no error is in the log
		TestingUtil.assertNoErrorInLog(verifResult);
	}

	@Test
	public void testCfaTransformedAsExcepted() throws IOException {
		DivByZeroRequirement req = reqExtension.createRequirement();

		req.fillVerificationProblem(verifProblem, parserResultMock, verifResult,
				RequirementRepresentationStrategy.EXPLICIT_WITH_VARIABLE);

		CfaNetworkDeclaration expectedCfa = XmiToCfa
				.deserializeFromString(IoUtils.readResourceFile("/cfa-final.xmi", this.getClass().getClassLoader()));
		CfaNetworkDeclaration cfa = (CfaNetworkDeclaration) verifProblem.getModel();

		System.err.println(CfaToXmi.serializeToString(cfa));
		
		assertEquals("Comparing the two CFA string representations", EmfModelPrinter.print(expectedCfa, CfaEObjectToText.INSTANCE),
				EmfModelPrinter.print(cfa, CfaEObjectToText.INSTANCE));
		
		assertTrue( "Comparing the two CFA objects", EcoreUtil.equals(expectedCfa, cfa));
	}

}
