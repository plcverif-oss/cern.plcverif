/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic;

import static cern.plcverif.library.reduction.basic.ReductionTesting.test;

import org.junit.Test;

import cern.plcverif.library.reduction.basic.ReductionTesting.Exhaustive;
import cern.plcverif.library.reduction.basic.impl.cfd.OverlapAssignments;

public class DeclarationRegressionTest {

	@Test
	public void overlapAmtsCfdReductionTest() {
		test("OverlapVariableAssignments1.scl", new OverlapAssignments(), Exhaustive.EXHAUSTIVE);
		test("OverlapVariableAssignments2.scl", new OverlapAssignments(), Exhaustive.EXHAUSTIVE);
		test("OverlapAssignments3.scl", new OverlapAssignments(), Exhaustive.EXHAUSTIVE);
		test("OverlapAssignments3.scl.verif", new OverlapAssignments(), Exhaustive.EXHAUSTIVE);
	}
}
