/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic;

import static cern.plcverif.library.reduction.basic.ReductionTesting.test;

import org.junit.Assert;
import org.junit.Test;

import cern.plcverif.base.models.cfa.transformation.ElseEliminator;
import cern.plcverif.library.reduction.basic.ReductionTesting.Exhaustive;
import cern.plcverif.library.reduction.basic.impl.cfi.ConditionPushDown;
import cern.plcverif.library.reduction.basic.impl.cfi.ExpressionSimplification;
import cern.plcverif.library.reduction.basic.impl.cfi.OverlapVarAssignments;
import cern.plcverif.library.reduction.basic.impl.cfi.RemoveFalseTransitions;
import cern.plcverif.library.reduction.basic.impl.cfi.RemoveIdentityAssignments;
import cern.plcverif.library.reduction.basic.impl.cfi.RemoveUnreadVariables;
import cern.plcverif.library.reduction.basic.impl.cfi.SimplifyTrivialConditionalBranches;
import cern.plcverif.library.reduction.basic.impl.cfi.coi.CoiReduction;
import cern.plcverif.library.reduction.basic.impl.cfi.propagation.ExpressionPropagation;
import cern.plcverif.library.reduction.basic.impl.cfi.unreadamt.RemoveUnreadAssignments;

public class RegressionTest {
	@Test
	public void coiReductionTest() {
		test("CoiReduction1.scl", new CoiReduction(new BasicReductionSettings()), "coi_func1_d");
		test("CoiReduction2.scl", new CoiReduction(new BasicReductionSettings()), "coi_func2_d");
	}

	@Test
	public void expressionPropagationReductionTest() {
		test("ExpressionPropagation1.scl", new ExpressionPropagation(false, new BasicReductionSettings()), cfi -> ElseEliminator.transform(cfi));
		test("ExpressionPropagation2.scl", new ExpressionPropagation(false, new BasicReductionSettings()), cfi -> ElseEliminator.transform(cfi));
		test("ExpressionPropagation3.scl", new ExpressionPropagation(false, new BasicReductionSettings()), cfi -> ElseEliminator.transform(cfi));
	}

	@Test
	public void conditionPushDownReductionTest() {
		test("ConditionPushDown1.scl", new ConditionPushDown(), cfi -> ElseEliminator.transform(cfi));
	}

	@Test
	public void removeUnreadVarsReductionTest() {
		test("RemoveUnreadVariables1.scl", new RemoveUnreadVariables(), "instance_d");
	}

	@Test
	public void overlapVarAmtsReductionTest() {
		test("OverlapVariableAssignments1.scl", new OverlapVarAssignments(), Exhaustive.EXHAUSTIVE);
		test("OverlapVariableAssignments2.scl", new OverlapVarAssignments(), Exhaustive.EXHAUSTIVE);
		test("OverlapVariableAssignments4.scl", new OverlapVarAssignments(), Exhaustive.EXHAUSTIVE);
	}
	
	@Test
	public void removeUnreadAmtsReductionTest() {
		test("RemoveUnreadAssignments1.scl", new RemoveUnreadAssignments(), "instance_c", "instance_e");
	}
	
	@Test
	public void removeFalseTransitionsReductionTest() {
		test("RemoveFalseTransitions1.scl", new RemoveFalseTransitions());
	}
	
	@Test
	public void removeIdentityAssignmentsReductionTest() {
		test("RemoveIdentityAssignments1.scl", new RemoveIdentityAssignments());
	}
	
	@Test
	public void simplifyTrivialConditionalBranchesReductionTest() {
		test("SimplifyTrivialConditionalBranches1.scl", new SimplifyTrivialConditionalBranches(), 
				cfi -> {while(new OverlapVarAssignments().reduce(cfi, null)) {}});
		test("SimplifyTrivialConditionalBranches2.scl", new SimplifyTrivialConditionalBranches(), 
				Exhaustive.EXHAUSTIVE,
				cfi -> {while(new OverlapVarAssignments().reduce(cfi, null)) {}});
		test("SimplifyTrivialConditionalBranches3.scl", new SimplifyTrivialConditionalBranches(), 
				Exhaustive.EXHAUSTIVE,
				cfi -> {while(new OverlapVarAssignments().reduce(cfi, null)) {}});
		
		test("SimplifyTrivialConditionalBranches3.scl.verif", new SimplifyTrivialConditionalBranches(), 
				Exhaustive.EXHAUSTIVE,
				cfi -> {while(new OverlapVarAssignments().reduce(cfi, null)) {}});
	}
	
	@Test
	public void exprSimplificationReductionTest() {
		test("ExpressionSimplification1.scl", new ExpressionSimplification(), Exhaustive.EXHAUSTIVE);
		test("ExpressionSimplification2.scl", new ExpressionSimplification(), Exhaustive.EXHAUSTIVE);
		test("ExpressionSimplification3.scl", new ExpressionSimplification(), Exhaustive.EXHAUSTIVE);
		test("ExpressionSimplification4.scl", new ExpressionSimplification(), Exhaustive.EXHAUSTIVE);
		test("ExpressionSimplification5.scl", new ExpressionSimplification(), Exhaustive.EXHAUSTIVE);
		test("ExpressionSimplification6.scl", new ExpressionSimplification(), Exhaustive.EXHAUSTIVE);
		test("ExpressionSimplification7.scl", new ExpressionSimplification(), Exhaustive.EXHAUSTIVE);
	}

	@Test
	public void sentinel() {
		if (ReductionTesting.CONTINUE_ON_ASSERT_VIOLATION) {
			Assert.fail("Assertions were disabled. This feature shall not be enabled in production.");
		}
	}

	
}
