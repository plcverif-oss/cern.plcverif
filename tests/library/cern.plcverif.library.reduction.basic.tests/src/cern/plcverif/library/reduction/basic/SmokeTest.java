/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic;

import java.io.IOException;
import java.util.Arrays;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.junit.Test;

import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.cfa.utils.CfaUtils;
import cern.plcverif.library.testing.MockJobResult;
import cern.plcverif.library.testmodels.TestModel;
import cern.plcverif.library.testmodels.TestModelCollection;

public class SmokeTest {
	/**
	 * Try each CFI reduction with each known model. We are not checking the
	 * result, only if they throw an exception.
	 * 
	 * This should not be counted in the test coverage.
	 * 
	 * @throws IOException
	 */
	@Test
	public void smokeTestCfi() throws IOException {
		for (TestModel testModel : TestModelCollection.getInstance().getAllModels()) {
			for (boolean enumerateArrays : Arrays.asList(true, false)) {
				CfaNetworkInstance cfi = testModel.getCfi(enumerateArrays);
				CfaUtils.removeAllAnnotations(cfi);
				for (IInstanceReduction reduction : new BasicReductions().allInstanceReductions()) {
					CfaNetworkInstance cfiCopy = EcoreUtil.copy(cfi);

//					System.out.printf(
//							"reduction.basic.tests/SmokeTest.smokeTestCfi: %s reduction with model %s (enumerateArrays=%s)...%n",
//							reduction.getClass().getSimpleName(), testModel.getModelId(),
//							Boolean.toString(enumerateArrays));
					reduction.reduce(cfiCopy, new MockJobResult());
				}
			}
		}
	}

	/**
	 * Try each CFD reduction with each known model. We are not checking the
	 * result, only if they throw an exception.
	 * 
	 * This should not be counted in the test coverage.
	 * 
	 * @throws IOException
	 */
	@Test
	public void smokeTestCfd() throws IOException {
		for (TestModel testModel : TestModelCollection.getInstance().getAllModels()) {
			CfaNetworkDeclaration cfd = testModel.getCfd();
			for (IDeclarationReduction reduction : new BasicReductions().allDeclarationReductions()) {
				CfaNetworkDeclaration cfdCopy = EcoreUtil.copy(cfd);
//				System.out.printf("reduction.basic.tests/SmokeTest.smokeTestCfd: %s reduction with model %s...%n",
//						reduction.getClass().getSimpleName(), testModel.getModelId());
				reduction.reduce(cfdCopy, new MockJobResult());
			}
		}
	}
}
