/*******************************************************************************
 * (C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 * 
 * This program and the accompanying materials are made available under the 2
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Daniel Darvas - initial API and implementation
 *******************************************************************************/
package cern.plcverif.library.reduction.basic;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.eclipse.core.runtime.Platform;
import org.junit.Assert;

import cern.plcverif.base.common.emf.textual.EmfModelPrinter;
import cern.plcverif.base.common.emf.textual.SimpleEObjectToText;
import cern.plcverif.base.common.utils.IoUtils;
import cern.plcverif.base.interfaces.data.JobResult;
import cern.plcverif.base.interfaces.data.result.JobStage;
import cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration;
import cern.plcverif.base.models.cfa.cfainstance.AbstractVariable;
import cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance;
import cern.plcverif.base.models.cfa.serialization.XmiToCfa;
import cern.plcverif.base.models.cfa.textual.CfaEObjectToText;
import cern.plcverif.base.models.cfa.trace.CfaInstantiationTrace;
import cern.plcverif.base.models.cfa.transformation.CfaInstantiator;
import cern.plcverif.base.models.cfa.utils.CfaUtils;
import cern.plcverif.base.models.cfa.visualization.CfaToDot;
import cern.plcverif.library.testing.MockJobResult;

public class ReductionTesting {
	public static final String WRITE_ACTUAL_OUTPUTS_TRACE_OPTION = "cern.plcverif.library.reduction.basic.tests/debug/write_actual_outputs";
	public static final boolean CONTINUE_ON_ASSERT_VIOLATION = false; // NOPMD
	public static boolean writeActualOutputs = CONTINUE_ON_ASSERT_VIOLATION;
	public static final String FILE_OUTPUT_DIR = "regression_output_dir";

	public enum Exhaustive { EXHAUSTIVE, ONE_ITERATION };
	
	static {
		String debugOption = Platform.getDebugOption(WRITE_ACTUAL_OUTPUTS_TRACE_OPTION);
		if ("true".equalsIgnoreCase(debugOption)) {
			writeActualOutputs = true;
		}
	}
	
	public static void test(String modelName, IInstanceReduction reduction, String... frozenVars) {
		test(modelName, reduction, Exhaustive.ONE_ITERATION, it -> {}, frozenVars);
	}
	
	public static void test(String modelName, IInstanceReduction reduction, Consumer<CfaNetworkInstance> cfiModif, String... frozenVars) {
		test(modelName, reduction, Exhaustive.ONE_ITERATION, cfiModif, frozenVars);
	}
	
	public static void test(String modelName, IInstanceReduction reduction, Exhaustive exhaustive, String... frozenVars) {
		test(modelName, reduction, exhaustive, it -> {}, frozenVars);
	}
	
	public static void test(String modelName, IInstanceReduction reduction, Exhaustive exhaustive, Consumer<CfaNetworkInstance> cfiModif, String... frozenVars) {
		String modelPath = String.format("/%s.cfd", modelName);
		String expOutputPath = String.format("/%s.reduced-expected.txt", modelName);
		String expLogPath = String.format("/%s.log-expected.txt", modelName);

		reduction.setFineLogging(true);
		
		try {
			// Load and get instance model
			String cfdContent = IoUtils.readResourceFile(modelPath, ReductionTesting.class.getClassLoader());
			CfaNetworkDeclaration cfd = XmiToCfa.deserializeFromString(cfdContent);
			CfaInstantiationTrace traceModel = CfaInstantiator.transformCfaNetwork(cfd, false);
			CfaNetworkInstance cfi = traceModel.getInstance();

			for (String frozenVar : frozenVars) {
				Optional<AbstractVariable> var = cfi.getVariables().stream()
						.filter(it -> it.getName().equals(frozenVar)).findFirst();
				if (var.isPresent()) {
					var.get().setFrozen(true);
				} else {
					System.out.println("Available vars: " + cfi.getVariables().stream().map(it -> it.getName())
							.collect(Collectors.joining(", ", "[", "]")));
					Assert.fail("Unable to find the following variable: " + frozenVar);
				}
			}

			if (writeActualOutputs) {
				File directory = new File(FILE_OUTPUT_DIR);
			    if (! directory.exists()){
			        directory.mkdir();
			    }
			    File modelFile = new File( directory, String.format("%s.original.dot" , modelName));
				IoUtils.writeAllContent(modelFile, CfaToDot.representCfi(cfi));
			}

			CfaUtils.removeAllAnnotations(cfi);
			cfiModif.accept(cfi);
			JobResult result = new MockJobResult();
			result.switchToStage("Reductions test (CFI)");
			
			if (exhaustive == Exhaustive.ONE_ITERATION) {
				reduction.reduce(cfi, result);
			} else if (exhaustive == Exhaustive.EXHAUSTIVE) {
				while (reduction.reduce(cfi, result)) {
					cfiModif.accept(cfi);
				}
			} else {
				throw new UnsupportedOperationException();
			}

			String cfiActualText = EmfModelPrinter.print(cfi, SimpleEObjectToText.INSTANCE).toString();

			if (writeActualOutputs) {
				File directory = new File(FILE_OUTPUT_DIR);
			    if (! directory.exists()){
			        directory.mkdir();
			    }
				File modelFile = new File( directory, String.format("%s.reduced-expected.txt" , modelName));
				IoUtils.writeAllContent(modelFile, cfiActualText);
				File modelFileDot = new File( directory, String.format("%s.reduced.dot" , modelName));
				IoUtils.writeAllContent(modelFileDot, CfaToDot.representCfi(cfi));
			}

			// Load and get expected model
			String expected = readResourceFile(expOutputPath);
			assertEquals(expected, cfiActualText);

			// Check log contents
			checkLog(expLogPath, result.currentStage(), modelName);
		} catch (IOException ex) {
			ex.printStackTrace();
			Assert.fail(ex.getMessage());
		}
	}
	
	public static void test(String modelName, IDeclarationReduction reduction, Exhaustive exhaustive) {
		test(modelName, reduction, exhaustive, it -> {});
	}
	
	public static void test(String modelName, IDeclarationReduction reduction, Exhaustive exhaustive, Consumer<CfaNetworkDeclaration> cfdModif) {
		String modelPath = String.format("/%s.cfd", modelName);
		String expOutputPath = String.format("/%s.cfdreduced-expected.txt", modelName);
		String expLogPath = String.format("/%s.cfdlog-expected.txt", modelName);

		reduction.setFineLogging(true);
		
		try {
			// Load and get instance model
			String cfdContent = IoUtils.readResourceFile(modelPath, ReductionTesting.class.getClassLoader());
			CfaNetworkDeclaration cfd = XmiToCfa.deserializeFromString(cfdContent);

			if (writeActualOutputs) {
				File directory = new File(FILE_OUTPUT_DIR);
			    if (! directory.exists()){
			        directory.mkdir();
			    }
				File modelFile = new File( directory, String.format("%s.original.dot" , modelName));
				IoUtils.writeAllContent(modelFile, CfaToDot.representCfd(cfd));
			}

			cfdModif.accept(cfd);
			JobResult result = new MockJobResult();
			result.switchToStage("Reductions test (CFI)");
			
			if (exhaustive == Exhaustive.ONE_ITERATION) {
				reduction.reduce(cfd, result);
			} else if (exhaustive == Exhaustive.EXHAUSTIVE) {
				while (reduction.reduce(cfd, result)) {
					cfdModif.accept(cfd);
				}
			} else {
				throw new UnsupportedOperationException();
			}

			String cfdActualText = EmfModelPrinter.print(cfd, CfaEObjectToText.INSTANCE).toString();

			if (writeActualOutputs) {
				File directory = new File(FILE_OUTPUT_DIR);
			    if (! directory.exists()){
			        directory.mkdir();
			    }
				File modelFile = new File( directory, String.format("%s.cfdreduced.dot" , modelName));
				IoUtils.writeAllContent(modelFile, CfaToDot.representCfd(cfd));
				File modelFileDot = new File( directory, String.format("%s.cfdreduced-expected.txt" , modelName));
				IoUtils.writeAllContent(modelFileDot, cfdActualText);
			}

			// Load and get expected model
			String expected = readResourceFile(expOutputPath);
			assertEquals(expected, cfdActualText);

			// Check log contents
			checkLog(expLogPath, result.currentStage(), modelName);
		} catch (IOException ex) {
			ex.printStackTrace();
			Assert.fail(ex.getMessage());
		}
	}

	private static void checkLog(String expectedLogPath, JobStage currentStageLog, String modelName) {
		// Make String representation of current log
		String actualLog = currentStageLog.getLogItems().stream().map(it -> it.getMessage())
				.collect(Collectors.joining(System.lineSeparator()));

		if (writeActualOutputs) {
			System.out.println(actualLog);
			try {
				File directory = new File(FILE_OUTPUT_DIR);
			    if (! directory.exists()){
			        directory.mkdir();
			    }
			    File modelFile = new File( directory, String.format("%s.log-expected.txt" , modelName));
				IoUtils.writeAllContent(modelFile, actualLog);
			} catch (IOException e) {
				// best effort
				e.printStackTrace();
			}
		}

		// Load expected log
		String expectedLog = readResourceFile(expectedLogPath);

		// Compare
		assertEquals(expectedLog, actualLog);
	}

	private static String readResourceFile(String path) {
		try {
			return IoUtils.readResourceFile(path, ReductionTesting.class.getClassLoader());
		} catch (NullPointerException exception) {
			if (CONTINUE_ON_ASSERT_VIOLATION) {
				exception.printStackTrace();
				return null;
			} else {
				throw exception;
			}
		}
	}

	private static void assertEquals(String expected, String actual) {
		if (!CONTINUE_ON_ASSERT_VIOLATION) {
			Assert.assertEquals(expected.trim(), actual.trim());
		}
	}
}
