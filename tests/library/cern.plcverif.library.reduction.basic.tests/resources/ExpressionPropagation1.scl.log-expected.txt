ExpressionPropagation: 'exprprop_ob1/b1' has been replaced by 'true' on transition 't3'.
ExpressionSimplification: '(true && IX0.0)' has been simplified to 'IX0.0'. (called from ExpressionPropagation)
ExpressionPropagation: 'exprprop_ob1/b1' has been replaced by 'true' on transition 't4'.
ExpressionSimplification: '(true && exprprop_ob1/b2)' has been simplified to 'exprprop_ob1/b2'. (called from ExpressionPropagation)
ExpressionPropagation: 'exprprop_ob1/b2' has been replaced by 'IX0.0' on transition 't4'.
ExpressionPropagation: 'exprprop_ob1/i2' has been replaced by '0' on transition 't6'.
ExpressionSimplification: '(0 * 3)' has been simplified to '0'. (called from ExpressionPropagation)
ExpressionPropagation: 'exprprop_ob1/i1' has been replaced by '5' on transition 't7'.
ExpressionPropagation: 'exprprop_ob1/i2' has been replaced by '0' on transition 't7'.
ExpressionSimplification: '(5 + 0)' has been simplified to '5'. (called from ExpressionPropagation)