{CfaNetworkInstance name=network displayName=network mainAutomaton:es_func5}
 ├──variables: 
 │  {Variable name=es_func5_a displayName=es_func5/a frozen=false container:network}
 │   ├──type: 
 │   │  {BoolType}
 │   ├──fqn: 
 │   │  {FullyQualifiedName identifiers=[es_func5, a]}
 │   ├──annotations: <empty>
 │   └──initialValue: 
 │      {BoolLiteral value=false}
 │       └──type: 
 │          {BoolType}
 │  {Variable name=es_func5_i displayName=es_func5/i frozen=false container:network}
 │   ├──type: 
 │   │  {IntType signed=true bits=16}
 │   ├──fqn: 
 │   │  {FullyQualifiedName identifiers=[es_func5, i]}
 │   ├──annotations: <empty>
 │   └──initialValue: 
 │      {IntLiteral value=0}
 │       └──type: 
 │          {IntType signed=true bits=16}
 │  {Variable name=es_func5_j displayName=es_func5/j frozen=false container:network}
 │   ├──type: 
 │   │  {IntType signed=true bits=16}
 │   ├──fqn: 
 │   │  {FullyQualifiedName identifiers=[es_func5, j]}
 │   ├──annotations: <empty>
 │   └──initialValue: 
 │      {IntLiteral value=0}
 │       └──type: 
 │          {IntType signed=true bits=16}
 └──automata: 
    {AutomatonInstance name=es_func5 displayName=es_func5 initialLocation:init endLocation:l21 container:network}
     ├──locations: 
     │  {Location name=init displayName=init frozen=true parentAutomaton:es_func5 incoming:[] outgoing:[t1]}
     │   └──annotations: <empty>
     │  {Location name=l1 displayName=l1 frozen=false parentAutomaton:es_func5 incoming:[t1] outgoing:[t2]}
     │   └──annotations: <empty>
     │  {Location name=l2 displayName=l2 frozen=false parentAutomaton:es_func5 incoming:[t2] outgoing:[t3]}
     │   └──annotations: <empty>
     │  {Location name=l3 displayName=l3 frozen=false parentAutomaton:es_func5 incoming:[t3] outgoing:[t4]}
     │   └──annotations: <empty>
     │  {Location name=l4 displayName=l4 frozen=false parentAutomaton:es_func5 incoming:[t4] outgoing:[t5]}
     │   └──annotations: <empty>
     │  {Location name=l5 displayName=l5 frozen=false parentAutomaton:es_func5 incoming:[t5] outgoing:[t6]}
     │   └──annotations: <empty>
     │  {Location name=l6 displayName=l6 frozen=false parentAutomaton:es_func5 incoming:[t6] outgoing:[t7]}
     │   └──annotations: <empty>
     │  {Location name=l7 displayName=l7 frozen=false parentAutomaton:es_func5 incoming:[t7] outgoing:[t8]}
     │   └──annotations: <empty>
     │  {Location name=l8 displayName=l8 frozen=false parentAutomaton:es_func5 incoming:[t8] outgoing:[t9]}
     │   └──annotations: <empty>
     │  {Location name=l9 displayName=l9 frozen=false parentAutomaton:es_func5 incoming:[t9] outgoing:[t10]}
     │   └──annotations: <empty>
     │  {Location name=l10 displayName=l10 frozen=false parentAutomaton:es_func5 incoming:[t10] outgoing:[t11]}
     │   └──annotations: <empty>
     │  {Location name=l11 displayName=l11 frozen=false parentAutomaton:es_func5 incoming:[t11] outgoing:[t12]}
     │   └──annotations: <empty>
     │  {Location name=l12 displayName=l12 frozen=false parentAutomaton:es_func5 incoming:[t12] outgoing:[t13]}
     │   └──annotations: <empty>
     │  {Location name=l13 displayName=l13 frozen=false parentAutomaton:es_func5 incoming:[t13] outgoing:[t14]}
     │   └──annotations: <empty>
     │  {Location name=l14 displayName=l14 frozen=false parentAutomaton:es_func5 incoming:[t14] outgoing:[t15]}
     │   └──annotations: <empty>
     │  {Location name=l15 displayName=l15 frozen=false parentAutomaton:es_func5 incoming:[t15] outgoing:[t16]}
     │   └──annotations: <empty>
     │  {Location name=l16 displayName=l16 frozen=false parentAutomaton:es_func5 incoming:[t16] outgoing:[t17]}
     │   └──annotations: <empty>
     │  {Location name=l17 displayName=l17 frozen=false parentAutomaton:es_func5 incoming:[t17] outgoing:[t18]}
     │   └──annotations: <empty>
     │  {Location name=l18 displayName=l18 frozen=false parentAutomaton:es_func5 incoming:[t18] outgoing:[t19]}
     │   └──annotations: <empty>
     │  {Location name=l19 displayName=l19 frozen=false parentAutomaton:es_func5 incoming:[t19] outgoing:[t20]}
     │   └──annotations: <empty>
     │  {Location name=l20 displayName=l20 frozen=false parentAutomaton:es_func5 incoming:[t20] outgoing:[t21]}
     │   └──annotations: <empty>
     │  {Location name=l21 displayName=l21 frozen=false parentAutomaton:es_func5 incoming:[t21] outgoing:[]}
     │   └──annotations: <empty>
     ├──transitions: 
     │  {AssignmentTransition name=t1 displayName=t1 frozen=false parentAutomaton:es_func5 source:init target:l1}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func5_a}
     │   │   └──rightValue: 
     │   │      {BoolLiteral value=true}
     │   │       └──type: 
     │   │          {BoolType}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t2 displayName=t2 frozen=false parentAutomaton:es_func5 source:l1 target:l2}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func5_a}
     │   │   └──rightValue: 
     │   │      {BoolLiteral value=false}
     │   │       └──type: 
     │   │          {BoolType}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t3 displayName=t3 frozen=false parentAutomaton:es_func5 source:l2 target:l3}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func5_a}
     │   │   └──rightValue: 
     │   │      {BoolLiteral value=false}
     │   │       └──type: 
     │   │          {BoolType}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t4 displayName=t4 frozen=false parentAutomaton:es_func5 source:l3 target:l4}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func5_a}
     │   │   └──rightValue: 
     │   │      {BoolLiteral value=true}
     │   │       └──type: 
     │   │          {BoolType}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t5 displayName=t5 frozen=false parentAutomaton:es_func5 source:l4 target:l5}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func5_a}
     │   │   └──rightValue: 
     │   │      {BoolLiteral value=false}
     │   │       └──type: 
     │   │          {BoolType}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t6 displayName=t6 frozen=false parentAutomaton:es_func5 source:l5 target:l6}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func5_a}
     │   │   └──rightValue: 
     │   │      {BoolLiteral value=true}
     │   │       └──type: 
     │   │          {BoolType}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t7 displayName=t7 frozen=false parentAutomaton:es_func5 source:l6 target:l7}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func5_a}
     │   │   └──rightValue: 
     │   │      {BoolLiteral value=false}
     │   │       └──type: 
     │   │          {BoolType}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t8 displayName=t8 frozen=false parentAutomaton:es_func5 source:l7 target:l8}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func5_a}
     │   │   └──rightValue: 
     │   │      {BoolLiteral value=false}
     │   │       └──type: 
     │   │          {BoolType}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t9 displayName=t9 frozen=false parentAutomaton:es_func5 source:l8 target:l9}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func5_a}
     │   │   └──rightValue: 
     │   │      {BoolLiteral value=true}
     │   │       └──type: 
     │   │          {BoolType}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t10 displayName=t10 frozen=false parentAutomaton:es_func5 source:l9 target:l10}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func5_a}
     │   │   └──rightValue: 
     │   │      {BoolLiteral value=false}
     │   │       └──type: 
     │   │          {BoolType}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t11 displayName=t11 frozen=false parentAutomaton:es_func5 source:l10 target:l11}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func5_a}
     │   │   └──rightValue: 
     │   │      {BoolLiteral value=true}
     │   │       └──type: 
     │   │          {BoolType}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t12 displayName=t12 frozen=false parentAutomaton:es_func5 source:l11 target:l12}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func5_a}
     │   │   └──rightValue: 
     │   │      {BoolLiteral value=true}
     │   │       └──type: 
     │   │          {BoolType}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t13 displayName=t13 frozen=false parentAutomaton:es_func5 source:l12 target:l13}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func5_a}
     │   │   └──rightValue: 
     │   │      {BoolLiteral value=true}
     │   │       └──type: 
     │   │          {BoolType}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t14 displayName=t14 frozen=false parentAutomaton:es_func5 source:l13 target:l14}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func5_a}
     │   │   └──rightValue: 
     │   │      {BoolLiteral value=true}
     │   │       └──type: 
     │   │          {BoolType}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t15 displayName=t15 frozen=false parentAutomaton:es_func5 source:l14 target:l15}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func5_a}
     │   │   └──rightValue: 
     │   │      {BoolLiteral value=false}
     │   │       └──type: 
     │   │          {BoolType}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t16 displayName=t16 frozen=false parentAutomaton:es_func5 source:l15 target:l16}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func5_i}
     │   │   └──rightValue: 
     │   │      {IntLiteral value=6}
     │   │       └──type: 
     │   │          {IntType signed=true bits=16}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t17 displayName=t17 frozen=false parentAutomaton:es_func5 source:l16 target:l17}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func5_i}
     │   │   └──rightValue: 
     │   │      {BinaryArithmeticExpression operator=PLUS}
     │   │       ├──leftOperand: 
     │   │       │  {BinaryArithmeticExpression operator=PLUS}
     │   │       │   ├──leftOperand: 
     │   │       │   │  {BinaryArithmeticExpression operator=PLUS}
     │   │       │   │   ├──leftOperand: 
     │   │       │   │   │  {VariableRef frozen=false variable:es_func5_j}
     │   │       │   │   └──rightOperand: 
     │   │       │   │      {IntLiteral value=1}
     │   │       │   │       └──type: 
     │   │       │   │          {IntType signed=true bits=16}
     │   │       │   └──rightOperand: 
     │   │       │      {IntLiteral value=2}
     │   │       │       └──type: 
     │   │       │          {IntType signed=true bits=16}
     │   │       └──rightOperand: 
     │   │          {IntLiteral value=3}
     │   │           └──type: 
     │   │              {IntType signed=true bits=16}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t18 displayName=t18 frozen=false parentAutomaton:es_func5 source:l17 target:l18}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func5_i}
     │   │   └──rightValue: 
     │   │      {BinaryArithmeticExpression operator=PLUS}
     │   │       ├──leftOperand: 
     │   │       │  {VariableRef frozen=false variable:es_func5_j}
     │   │       └──rightOperand: 
     │   │          {IntLiteral value=6}
     │   │           └──type: 
     │   │              {IntType signed=true bits=16}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t19 displayName=t19 frozen=false parentAutomaton:es_func5 source:l18 target:l19}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func5_i}
     │   │   └──rightValue: 
     │   │      {IntLiteral value=11}
     │   │       └──type: 
     │   │          {IntType signed=true bits=16}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t20 displayName=t20 frozen=false parentAutomaton:es_func5 source:l19 target:l20}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func5_i}
     │   │   └──rightValue: 
     │   │      {IntLiteral value=16}
     │   │       └──type: 
     │   │          {IntType signed=true bits=16}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t21 displayName=t21 frozen=false parentAutomaton:es_func5 source:l20 target:l21}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func5_i}
     │   │   └──rightValue: 
     │   │      {IntLiteral value=-6}
     │   │       └──type: 
     │   │          {IntType signed=true bits=16}
     │   └──assumeBounds: <empty>
     └──annotations: <empty>
