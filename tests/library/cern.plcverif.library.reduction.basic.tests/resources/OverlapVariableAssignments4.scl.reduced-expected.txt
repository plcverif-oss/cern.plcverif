{CfaNetworkInstance name=network displayName=network mainAutomaton:ova_func4}
 ├──variables: 
 │  {Variable name=ova_func4_in1 displayName=ova_func4/in1 frozen=false container:network}
 │   ├──type: 
 │   │  {IntType signed=true bits=16}
 │   ├──fqn: 
 │   │  {FullyQualifiedName identifiers=[ova_func4, in1]}
 │   ├──annotations: <empty>
 │   └──initialValue: 
 │      {IntLiteral value=0}
 │       └──type: 
 │          {IntType signed=true bits=16}
 │  {Variable name=ova_func4_a displayName=ova_func4/a frozen=false container:network}
 │   ├──type: 
 │   │  {IntType signed=true bits=16}
 │   ├──fqn: 
 │   │  {FullyQualifiedName identifiers=[ova_func4, a]}
 │   ├──annotations: <empty>
 │   └──initialValue: 
 │      {IntLiteral value=0}
 │       └──type: 
 │          {IntType signed=true bits=16}
 │  {Variable name=ova_func4_b displayName=ova_func4/b frozen=false container:network}
 │   ├──type: 
 │   │  {IntType signed=true bits=16}
 │   ├──fqn: 
 │   │  {FullyQualifiedName identifiers=[ova_func4, b]}
 │   ├──annotations: <empty>
 │   └──initialValue: 
 │      {IntLiteral value=0}
 │       └──type: 
 │          {IntType signed=true bits=16}
 └──automata: 
    {AutomatonInstance name=ova_func4 displayName=ova_func4 initialLocation:init endLocation:l3 container:network}
     ├──locations: 
     │  {Location name=init displayName=init frozen=true parentAutomaton:ova_func4 incoming:[] outgoing:[t1]}
     │   └──annotations: <empty>
     │  {Location name=l1 displayName=l1 frozen=false parentAutomaton:ova_func4 incoming:[t1] outgoing:[t2]}
     │   └──annotations: <empty>
     │  {Location name=l3 displayName=l3 frozen=false parentAutomaton:ova_func4 incoming:[t2] outgoing:[]}
     │   └──annotations: <empty>
     ├──transitions: 
     │  {AssignmentTransition name=t1 displayName=t1 frozen=false parentAutomaton:ova_func4 source:init target:l1}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:ova_func4_a}
     │   │   └──rightValue: 
     │   │      {VariableRef frozen=false variable:ova_func4_in1}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t2 displayName=t2 frozen=false parentAutomaton:ova_func4 source:l1 target:l3}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:ova_func4_a}
     │   │   └──rightValue: 
     │   │      {BinaryArithmeticExpression operator=PLUS}
     │   │       ├──leftOperand: 
     │   │       │  {VariableRef frozen=false variable:ova_func4_a}
     │   │       └──rightOperand: 
     │   │          {IntLiteral value=1}
     │   │           └──type: 
     │   │              {IntType signed=true bits=16}
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:ova_func4_b}
     │   │   └──rightValue: 
     │   │      {IntLiteral value=0}
     │   │       └──type: 
     │   │          {IntType signed=true bits=16}
     │   └──assumeBounds: <empty>
     └──annotations: <empty>
