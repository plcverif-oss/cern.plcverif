ExpressionPropagation: 'IX0.0' has been replaced by 'true' on transition 't3'.
ExpressionPropagation: 'IX0.1' has been replaced by 'true' on transition 't8'.
ExpressionSimplification: '(exprprop2_ob1/b1 || true)' has been simplified to 'true'. (called from ExpressionPropagation)
ExpressionPropagation: 'IX0.0' has been replaced by 'true' on transition 't14'.
ExpressionPropagation: 'IX0.2' has been replaced by 'true' on transition 't19'.
ExpressionPropagation: 'IX0.3' has been replaced by 'false' on transition 't20'.
ExpressionPropagation: 'IX0.4' has been replaced by 'false' on transition 't21'.