{CfaNetworkInstance name=network displayName=network mainAutomaton:stcb_func2}
 ├──variables: 
 │  {Variable name=MX0_0 displayName=MX0.0 frozen=false container:network}
 │   ├──type: 
 │   │  {BoolType}
 │   ├──fqn: 
 │   │  {FullyQualifiedName identifiers=[MX0.0]}
 │   ├──annotations: <empty>
 │   └──initialValue: 
 │      {BoolLiteral value=false}
 │       └──type: 
 │          {BoolType}
 │  {Variable name=stcb_func2_a displayName=stcb_func2/a frozen=false container:network}
 │   ├──type: 
 │   │  {BoolType}
 │   ├──fqn: 
 │   │  {FullyQualifiedName identifiers=[stcb_func2, a]}
 │   ├──annotations: <empty>
 │   └──initialValue: 
 │      {BoolLiteral value=false}
 │       └──type: 
 │          {BoolType}
 │  {Variable name=stcb_func2_b displayName=stcb_func2/b frozen=false container:network}
 │   ├──type: 
 │   │  {BoolType}
 │   ├──fqn: 
 │   │  {FullyQualifiedName identifiers=[stcb_func2, b]}
 │   ├──annotations: <empty>
 │   └──initialValue: 
 │      {BoolLiteral value=false}
 │       └──type: 
 │          {BoolType}
 │  {Variable name=stcb_func2_c displayName=stcb_func2/c frozen=false container:network}
 │   ├──type: 
 │   │  {BoolType}
 │   ├──fqn: 
 │   │  {FullyQualifiedName identifiers=[stcb_func2, c]}
 │   ├──annotations: <empty>
 │   └──initialValue: 
 │      {BoolLiteral value=false}
 │       └──type: 
 │          {BoolType}
 └──automata: 
    {AutomatonInstance name=stcb_func2 displayName=stcb_func2 initialLocation:init endLocation:l16 container:network}
     ├──locations: 
     │  {Location name=init displayName=init frozen=true parentAutomaton:stcb_func2 incoming:[] outgoing:[t1]}
     │   └──annotations: <empty>
     │  {Location name=l1 displayName=l1 frozen=false parentAutomaton:stcb_func2 incoming:[t1] outgoing:[t2,t5]}
     │   └──annotations: <empty>
     │  {Location name=l6 displayName=l6 frozen=false parentAutomaton:stcb_func2 incoming:[x7] outgoing:[t8,t11]}
     │   └──annotations: <empty>
     │  {Location name=l11 displayName=l11 frozen=false parentAutomaton:stcb_func2 incoming:[t11,t8] outgoing:[t14,t17]}
     │   └──annotations: <empty>
     │  {Location name=l13 displayName=l13 frozen=false parentAutomaton:stcb_func2 incoming:[t14] outgoing:[t16]}
     │   └──annotations: <empty>
     │  {Location name=l16 displayName=l16 frozen=false parentAutomaton:stcb_func2 incoming:[t16,t17] outgoing:[]}
     │   └──annotations: <empty>
     │  {Location name=x4 displayName=x frozen=false parentAutomaton:stcb_func2 incoming:[t5,t2] outgoing:[x5]}
     │   └──annotations: <empty>
     │  {Location name=x6 displayName=x frozen=false parentAutomaton:stcb_func2 incoming:[x5] outgoing:[x7]}
     │   └──annotations: <empty>
     ├──transitions: 
     │  {AssignmentTransition name=t1 displayName=t1 frozen=false parentAutomaton:stcb_func2 source:init target:l1}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:MX0_0}
     │   │   └──rightValue: 
     │   │      {BoolLiteral value=true}
     │   │       └──type: 
     │   │          {BoolType}
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:stcb_func2_b}
     │   │   └──rightValue: 
     │   │      {BinaryLogicExpression operator=OR}
     │   │       ├──leftOperand: 
     │   │       │  {BinaryLogicExpression operator=AND}
     │   │       │   ├──leftOperand: 
     │   │       │   │  {VariableRef frozen=false variable:stcb_func2_a}
     │   │       │   └──rightOperand: 
     │   │       │      {VariableRef frozen=false variable:stcb_func2_a}
     │   │       └──rightOperand: 
     │   │          {BinaryLogicExpression operator=AND}
     │   │           ├──leftOperand: 
     │   │           │  {UnaryLogicExpression operator=NEG}
     │   │           │   └──operand: 
     │   │           │      {VariableRef frozen=false variable:stcb_func2_a}
     │   │           └──rightOperand: 
     │   │              {VariableRef frozen=false variable:stcb_func2_b}
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:stcb_func2_c}
     │   │   └──rightValue: 
     │   │      {BinaryLogicExpression operator=OR}
     │   │       ├──leftOperand: 
     │   │       │  {BinaryLogicExpression operator=AND}
     │   │       │   ├──leftOperand: 
     │   │       │   │  {VariableRef frozen=false variable:stcb_func2_a}
     │   │       │   └──rightOperand: 
     │   │       │      {VariableRef frozen=false variable:stcb_func2_a}
     │   │       └──rightOperand: 
     │   │          {BinaryLogicExpression operator=AND}
     │   │           ├──leftOperand: 
     │   │           │  {UnaryLogicExpression operator=NEG}
     │   │           │   └──operand: 
     │   │           │      {VariableRef frozen=false variable:stcb_func2_a}
     │   │           └──rightOperand: 
     │   │              {VariableRef frozen=false variable:stcb_func2_c}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t2 displayName=t2 frozen=false parentAutomaton:stcb_func2 source:l1 target:x4}
     │   ├──condition: 
     │   │  {VariableRef frozen=false variable:stcb_func2_a}
     │   ├──annotations: <empty>
     │   ├──assignments: <empty>
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t5 displayName=t5 frozen=false parentAutomaton:stcb_func2 source:l1 target:x4}
     │   ├──condition: 
     │   │  {ElseExpression}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: <empty>
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t8 displayName=t8 frozen=false parentAutomaton:stcb_func2 source:l6 target:l11}
     │   ├──condition: 
     │   │  {VariableRef frozen=false variable:stcb_func2_a}
     │   ├──annotations: <empty>
     │   ├──assignments: <empty>
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t11 displayName=t11 frozen=false parentAutomaton:stcb_func2 source:l6 target:l11}
     │   ├──condition: 
     │   │  {ElseExpression}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: <empty>
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t14 displayName=t14 frozen=false parentAutomaton:stcb_func2 source:l11 target:l13}
     │   ├──condition: 
     │   │  {VariableRef frozen=false variable:stcb_func2_b}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:stcb_func2_b}
     │   │   └──rightValue: 
     │   │      {VariableRef frozen=false variable:stcb_func2_a}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t16 displayName=t16 frozen=false parentAutomaton:stcb_func2 source:l13 target:l16}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:stcb_func2_c}
     │   │   └──rightValue: 
     │   │      {VariableRef frozen=false variable:stcb_func2_b}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t17 displayName=t17 frozen=false parentAutomaton:stcb_func2 source:l11 target:l16}
     │   ├──condition: 
     │   │  {ElseExpression}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: <empty>
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=x5 displayName=x frozen=false parentAutomaton:stcb_func2 source:x4 target:x6}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:stcb_func2_b}
     │   │   └──rightValue: 
     │   │      {BinaryLogicExpression operator=OR}
     │   │       ├──leftOperand: 
     │   │       │  {BinaryLogicExpression operator=AND}
     │   │       │   ├──leftOperand: 
     │   │       │   │  {VariableRef frozen=false variable:stcb_func2_a}
     │   │       │   └──rightOperand: 
     │   │       │      {VariableRef frozen=false variable:stcb_func2_a}
     │   │       └──rightOperand: 
     │   │          {BinaryLogicExpression operator=AND}
     │   │           ├──leftOperand: 
     │   │           │  {UnaryLogicExpression operator=NEG}
     │   │           │   └──operand: 
     │   │           │      {VariableRef frozen=false variable:stcb_func2_a}
     │   │           └──rightOperand: 
     │   │              {VariableRef frozen=false variable:stcb_func2_b}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=x7 displayName=x frozen=false parentAutomaton:stcb_func2 source:x6 target:l6}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:stcb_func2_c}
     │   │   └──rightValue: 
     │   │      {BinaryLogicExpression operator=OR}
     │   │       ├──leftOperand: 
     │   │       │  {BinaryLogicExpression operator=AND}
     │   │       │   ├──leftOperand: 
     │   │       │   │  {VariableRef frozen=false variable:stcb_func2_a}
     │   │       │   └──rightOperand: 
     │   │       │      {BinaryLogicExpression operator=OR}
     │   │       │       ├──leftOperand: 
     │   │       │       │  {VariableRef frozen=false variable:stcb_func2_b}
     │   │       │       └──rightOperand: 
     │   │       │          {VariableRef frozen=false variable:stcb_func2_a}
     │   │       └──rightOperand: 
     │   │          {BinaryLogicExpression operator=AND}
     │   │           ├──leftOperand: 
     │   │           │  {UnaryLogicExpression operator=NEG}
     │   │           │   └──operand: 
     │   │           │      {VariableRef frozen=false variable:stcb_func2_a}
     │   │           └──rightOperand: 
     │   │              {VariableRef frozen=false variable:stcb_func2_c}
     │   └──assumeBounds: <empty>
     └──annotations: <empty>
