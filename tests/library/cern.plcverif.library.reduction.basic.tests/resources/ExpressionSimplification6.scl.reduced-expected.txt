{CfaNetworkInstance name=network displayName=network mainAutomaton:es_func6}
 ├──variables: 
 │  {Variable name=es_func6_v displayName=es_func6/v frozen=false container:network}
 │   ├──type: 
 │   │  {BoolType}
 │   ├──fqn: 
 │   │  {FullyQualifiedName identifiers=[es_func6, v]}
 │   ├──annotations: <empty>
 │   └──initialValue: 
 │      {BoolLiteral value=false}
 │       └──type: 
 │          {BoolType}
 │  {Variable name=es_func6_a displayName=es_func6/a frozen=false container:network}
 │   ├──type: 
 │   │  {BoolType}
 │   ├──fqn: 
 │   │  {FullyQualifiedName identifiers=[es_func6, a]}
 │   ├──annotations: <empty>
 │   └──initialValue: 
 │      {BoolLiteral value=false}
 │       └──type: 
 │          {BoolType}
 │  {Variable name=es_func6_b displayName=es_func6/b frozen=false container:network}
 │   ├──type: 
 │   │  {BoolType}
 │   ├──fqn: 
 │   │  {FullyQualifiedName identifiers=[es_func6, b]}
 │   ├──annotations: <empty>
 │   └──initialValue: 
 │      {BoolLiteral value=false}
 │       └──type: 
 │          {BoolType}
 │  {Variable name=es_func6_c displayName=es_func6/c frozen=false container:network}
 │   ├──type: 
 │   │  {BoolType}
 │   ├──fqn: 
 │   │  {FullyQualifiedName identifiers=[es_func6, c]}
 │   ├──annotations: <empty>
 │   └──initialValue: 
 │      {BoolLiteral value=false}
 │       └──type: 
 │          {BoolType}
 └──automata: 
    {AutomatonInstance name=es_func6 displayName=es_func6 initialLocation:init endLocation:l11 container:network}
     ├──locations: 
     │  {Location name=init displayName=init frozen=true parentAutomaton:es_func6 incoming:[] outgoing:[t1]}
     │   └──annotations: <empty>
     │  {Location name=l1 displayName=l1 frozen=false parentAutomaton:es_func6 incoming:[t1] outgoing:[t2]}
     │   └──annotations: <empty>
     │  {Location name=l2 displayName=l2 frozen=false parentAutomaton:es_func6 incoming:[t2] outgoing:[t3]}
     │   └──annotations: <empty>
     │  {Location name=l3 displayName=l3 frozen=false parentAutomaton:es_func6 incoming:[t3] outgoing:[t4]}
     │   └──annotations: <empty>
     │  {Location name=l4 displayName=l4 frozen=false parentAutomaton:es_func6 incoming:[t4] outgoing:[t5]}
     │   └──annotations: <empty>
     │  {Location name=l5 displayName=l5 frozen=false parentAutomaton:es_func6 incoming:[t5] outgoing:[t6]}
     │   └──annotations: <empty>
     │  {Location name=l6 displayName=l6 frozen=false parentAutomaton:es_func6 incoming:[t6] outgoing:[t7]}
     │   └──annotations: <empty>
     │  {Location name=l7 displayName=l7 frozen=false parentAutomaton:es_func6 incoming:[t7] outgoing:[t8]}
     │   └──annotations: <empty>
     │  {Location name=l8 displayName=l8 frozen=false parentAutomaton:es_func6 incoming:[t8] outgoing:[t9]}
     │   └──annotations: <empty>
     │  {Location name=l9 displayName=l9 frozen=false parentAutomaton:es_func6 incoming:[t9] outgoing:[t10]}
     │   └──annotations: <empty>
     │  {Location name=l10 displayName=l10 frozen=false parentAutomaton:es_func6 incoming:[t10] outgoing:[t11]}
     │   └──annotations: <empty>
     │  {Location name=l11 displayName=l11 frozen=false parentAutomaton:es_func6 incoming:[t11] outgoing:[]}
     │   └──annotations: <empty>
     ├──transitions: 
     │  {AssignmentTransition name=t1 displayName=t1 frozen=false parentAutomaton:es_func6 source:init target:l1}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func6_v}
     │   │   └──rightValue: 
     │   │      {VariableRef frozen=false variable:es_func6_a}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t2 displayName=t2 frozen=false parentAutomaton:es_func6 source:l1 target:l2}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func6_v}
     │   │   └──rightValue: 
     │   │      {BoolLiteral value=false}
     │   │       └──type: 
     │   │          {BoolType}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t3 displayName=t3 frozen=false parentAutomaton:es_func6 source:l2 target:l3}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func6_v}
     │   │   └──rightValue: 
     │   │      {BoolLiteral value=false}
     │   │       └──type: 
     │   │          {BoolType}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t4 displayName=t4 frozen=false parentAutomaton:es_func6 source:l3 target:l4}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func6_v}
     │   │   └──rightValue: 
     │   │      {BinaryLogicExpression operator=OR}
     │   │       ├──leftOperand: 
     │   │       │  {VariableRef frozen=false variable:es_func6_a}
     │   │       └──rightOperand: 
     │   │          {BinaryLogicExpression operator=AND}
     │   │           ├──leftOperand: 
     │   │           │  {VariableRef frozen=false variable:es_func6_b}
     │   │           └──rightOperand: 
     │   │              {VariableRef frozen=false variable:es_func6_c}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t5 displayName=t5 frozen=false parentAutomaton:es_func6 source:l4 target:l5}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func6_v}
     │   │   └──rightValue: 
     │   │      {BinaryLogicExpression operator=OR}
     │   │       ├──leftOperand: 
     │   │       │  {VariableRef frozen=false variable:es_func6_a}
     │   │       └──rightOperand: 
     │   │          {BinaryLogicExpression operator=AND}
     │   │           ├──leftOperand: 
     │   │           │  {VariableRef frozen=false variable:es_func6_b}
     │   │           └──rightOperand: 
     │   │              {VariableRef frozen=false variable:es_func6_c}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t6 displayName=t6 frozen=false parentAutomaton:es_func6 source:l5 target:l6}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func6_v}
     │   │   └──rightValue: 
     │   │      {BinaryLogicExpression operator=OR}
     │   │       ├──leftOperand: 
     │   │       │  {VariableRef frozen=false variable:es_func6_b}
     │   │       └──rightOperand: 
     │   │          {BinaryLogicExpression operator=AND}
     │   │           ├──leftOperand: 
     │   │           │  {VariableRef frozen=false variable:es_func6_a}
     │   │           └──rightOperand: 
     │   │              {VariableRef frozen=false variable:es_func6_c}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t7 displayName=t7 frozen=false parentAutomaton:es_func6 source:l6 target:l7}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func6_v}
     │   │   └──rightValue: 
     │   │      {BinaryLogicExpression operator=OR}
     │   │       ├──leftOperand: 
     │   │       │  {VariableRef frozen=false variable:es_func6_b}
     │   │       └──rightOperand: 
     │   │          {BinaryLogicExpression operator=AND}
     │   │           ├──leftOperand: 
     │   │           │  {VariableRef frozen=false variable:es_func6_a}
     │   │           └──rightOperand: 
     │   │              {VariableRef frozen=false variable:es_func6_c}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t8 displayName=t8 frozen=false parentAutomaton:es_func6 source:l7 target:l8}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func6_v}
     │   │   └──rightValue: 
     │   │      {BinaryLogicExpression operator=AND}
     │   │       ├──leftOperand: 
     │   │       │  {BinaryLogicExpression operator=AND}
     │   │       │   ├──leftOperand: 
     │   │       │   │  {VariableRef frozen=false variable:es_func6_a}
     │   │       │   └──rightOperand: 
     │   │       │      {VariableRef frozen=false variable:es_func6_b}
     │   │       └──rightOperand: 
     │   │          {VariableRef frozen=false variable:es_func6_c}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t9 displayName=t9 frozen=false parentAutomaton:es_func6 source:l8 target:l9}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func6_v}
     │   │   └──rightValue: 
     │   │      {BinaryLogicExpression operator=AND}
     │   │       ├──leftOperand: 
     │   │       │  {BinaryLogicExpression operator=AND}
     │   │       │   ├──leftOperand: 
     │   │       │   │  {VariableRef frozen=false variable:es_func6_a}
     │   │       │   └──rightOperand: 
     │   │       │      {VariableRef frozen=false variable:es_func6_b}
     │   │       └──rightOperand: 
     │   │          {VariableRef frozen=false variable:es_func6_c}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t10 displayName=t10 frozen=false parentAutomaton:es_func6 source:l9 target:l10}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func6_v}
     │   │   └──rightValue: 
     │   │      {BinaryLogicExpression operator=AND}
     │   │       ├──leftOperand: 
     │   │       │  {BinaryLogicExpression operator=AND}
     │   │       │   ├──leftOperand: 
     │   │       │   │  {VariableRef frozen=false variable:es_func6_a}
     │   │       │   └──rightOperand: 
     │   │       │      {VariableRef frozen=false variable:es_func6_b}
     │   │       └──rightOperand: 
     │   │          {VariableRef frozen=false variable:es_func6_c}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t11 displayName=t11 frozen=false parentAutomaton:es_func6 source:l10 target:l11}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:es_func6_v}
     │   │   └──rightValue: 
     │   │      {BinaryLogicExpression operator=AND}
     │   │       ├──leftOperand: 
     │   │       │  {BinaryLogicExpression operator=AND}
     │   │       │   ├──leftOperand: 
     │   │       │   │  {VariableRef frozen=false variable:es_func6_a}
     │   │       │   └──rightOperand: 
     │   │       │      {VariableRef frozen=false variable:es_func6_b}
     │   │       └──rightOperand: 
     │   │          {VariableRef frozen=false variable:es_func6_c}
     │   └──assumeBounds: <empty>
     └──annotations: <empty>
