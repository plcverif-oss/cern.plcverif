{CfaNetworkInstance name=network displayName=network mainAutomaton:instance_ria_fb1}
 ├──variables: 
 │  {Variable name=ria_db1_a displayName=ria_db1/a frozen=false container:network}
 │   ├──type: 
 │   │  {BoolType}
 │   ├──fqn: 
 │   │  {FullyQualifiedName identifiers=[ria_db1, a]}
 │   ├──annotations: <empty>
 │   └──initialValue: 
 │      {BoolLiteral value=false}
 │       └──type: 
 │          {BoolType}
 │  {Variable name=instance_in1 displayName=instance/in1 frozen=false container:network}
 │   ├──type: 
 │   │  {BoolType}
 │   ├──fqn: 
 │   │  {FullyQualifiedName identifiers=[instance, in1]}
 │   ├──annotations: <empty>
 │   └──initialValue: 
 │      {BoolLiteral value=false}
 │       └──type: 
 │          {BoolType}
 │  {Variable name=instance_a displayName=instance/a frozen=false container:network}
 │   ├──type: 
 │   │  {BoolType}
 │   ├──fqn: 
 │   │  {FullyQualifiedName identifiers=[instance, a]}
 │   ├──annotations: <empty>
 │   └──initialValue: 
 │      {BoolLiteral value=false}
 │       └──type: 
 │          {BoolType}
 └──automata: 
    {AutomatonInstance name=instance_ria_fb1 displayName=instance.ria_fb1 initialLocation:init endLocation:l5 container:network}
     ├──locations: 
     │  {Location name=init displayName=init frozen=true parentAutomaton:instance_ria_fb1 incoming:[] outgoing:[t1]}
     │   └──annotations: <empty>
     │  {Location name=l1 displayName=l1 frozen=false parentAutomaton:instance_ria_fb1 incoming:[t1] outgoing:[t2]}
     │   └──annotations: <empty>
     │  {Location name=l2 displayName=l2 frozen=false parentAutomaton:instance_ria_fb1 incoming:[t2] outgoing:[t3]}
     │   └──annotations: <empty>
     │  {Location name=l3 displayName=l3 frozen=false parentAutomaton:instance_ria_fb1 incoming:[t3] outgoing:[t4]}
     │   └──annotations: <empty>
     │  {Location name=l4 displayName=l4 frozen=false parentAutomaton:instance_ria_fb1 incoming:[t4] outgoing:[t5]}
     │   └──annotations: <empty>
     │  {Location name=l5 displayName=l5 frozen=false parentAutomaton:instance_ria_fb1 incoming:[t5] outgoing:[]}
     │   └──annotations: <empty>
     ├──transitions: 
     │  {AssignmentTransition name=t1 displayName=t1 frozen=false parentAutomaton:instance_ria_fb1 source:init target:l1}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:instance_a}
     │   │   └──rightValue: 
     │   │      {VariableRef frozen=false variable:instance_in1}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t2 displayName=t2 frozen=false parentAutomaton:instance_ria_fb1 source:l1 target:l2}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: <empty>
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t3 displayName=t3 frozen=false parentAutomaton:instance_ria_fb1 source:l2 target:l3}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:instance_a}
     │   │   └──rightValue: 
     │   │      {UnaryLogicExpression operator=NEG}
     │   │       └──operand: 
     │   │          {VariableRef frozen=false variable:instance_a}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t4 displayName=t4 frozen=false parentAutomaton:instance_ria_fb1 source:l3 target:l4}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:instance_a}
     │   │   └──rightValue: 
     │   │      {BinaryLogicExpression operator=OR}
     │   │       ├──leftOperand: 
     │   │       │  {VariableRef frozen=false variable:instance_a}
     │   │       └──rightOperand: 
     │   │          {UnaryLogicExpression operator=NEG}
     │   │           └──operand: 
     │   │              {VariableRef frozen=false variable:instance_a}
     │   └──assumeBounds: <empty>
     │  {AssignmentTransition name=t5 displayName=t5 frozen=false parentAutomaton:instance_ria_fb1 source:l4 target:l5}
     │   ├──condition: 
     │   │  {BoolLiteral value=true}
     │   │   └──type: 
     │   │      {BoolType}
     │   ├──annotations: <empty>
     │   ├──assignments: 
     │   │  {VariableAssignment frozen=false}
     │   │   ├──leftValue: 
     │   │   │  {VariableRef frozen=false variable:instance_a}
     │   │   └──rightValue: 
     │   │      {VariableRef frozen=false variable:ria_db1_a}
     │   └──assumeBounds: <empty>
     └──annotations: <empty>
